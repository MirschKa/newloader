[LIT]
73
15	26	32	42	32
35	41	299	41	303
36	41	303	42	303
37	41	299	42	299
38	34	299	41	299
57	41	347	41	351
58	41	351	42	351
59	41	347	42	347
60	35	347	41	347
94	41	252	41	256
96	41	210	42	210
97	33	206	41	206
98	41	206	42	206
99	41	206	41	210
100	41	256	42	256
101	41	252	42	252
102	33	252	41	252
150	36	493	40	493
151	40	493	42	493
152	40	497	42	497
153	40	493	40	497
155	40	545	42	545
156	36	541	40	541
157	40	541	42	541
158	40	541	40	545
197	35	445	40	445
198	40	445	42	445
199	40	449	42	449
200	40	445	40	449
202	34	396	41	396
203	41	396	42	396
204	41	400	42	400
205	41	396	41	400
224	25	151	40	151
225	40	151	42	151
226	40	155	42	155
227	40	151	40	155
247	25	107	40	107
248	40	107	42	107
249	40	111	42	111
250	40	107	40	111
260	78	40	82	40
300	28	587	39	587
301	39	587	42	587
302	39	591	42	591
303	39	587	39	591
305	39	638	42	638
306	28	634	39	634
307	39	634	42	634
308	39	634	39	638
347	33	680	39	680
348	39	680	42	680
349	39	684	42	684
350	39	680	39	684
352	39	726	39	730
353	39	730	42	730
354	39	726	42	726
355	34	726	39	726
391	39	775	42	775
392	39	779	42	779
393	39	775	39	779
395	39	822	42	822
396	39	826	42	826
397	39	822	39	826
398	25	775	39	775
415	35	76	41	76
416	35	72	41	72
417	91	72	97	72
418	91	76	96	76
420	91	80	97	80
422	36	82	41	82
424	25	822	39	822
433	88	107	97	107

[TET]
109
2	4	31	26	33	4	5	SI_EmergencyStop
14	78	31	99	33	4	3	s_EmergencyStop
16	34	27	42	29	4	5	TRUE
17	29	35	42	37	4	5	SAFETRUE
18	28	39	42	41	4	5	SAFEFALSE
19	28	43	42	45	4	5	PLC_Reset
21	4	298	34	300	4	5	SI_InfeedDoorRightClosed
39	31	306	42	308	4	5	TIME#0s
40	29	310	42	312	4	5	SAFETRUE
41	29	314	42	316	4	5	SAFETRUE
42	88	298	117	300	4	3	s_InfeedDoorRightClosed
56	4	346	35	348	4	5	SI_InfeedDoorRightLocked
61	34	294	42	296	4	5	TRUE
62	34	342	42	344	4	5	TRUE
63	31	354	42	356	4	5	TIME#0s
64	29	358	42	360	4	5	SAFETRUE
65	29	362	42	364	4	5	SAFETRUE
93	4	251	33	253	4	5	SI_InfeedDoorLeftLocked
95	4	205	33	207	4	5	SI_InfeedDoorLeftClosed
103	34	201	42	203	4	5	TRUE
104	34	247	42	249	4	5	TRUE
105	31	213	42	215	4	5	TIME#0s
106	29	217	42	219	4	5	SAFETRUE
107	29	221	42	223	4	5	SAFETRUE
108	31	259	42	261	4	5	TIME#0s
109	29	263	42	265	4	5	SAFETRUE
110	29	267	42	269	4	5	SAFETRUE
111	88	346	117	348	4	3	s_InfeedDoorRightLocked
112	88	205	115	207	4	3	s_InfeedDoorLeftClosed
113	88	251	116	253	4	3	s_InfeedDoorLeftLocked
117	34	488	42	490	4	5	TRUE
118	34	536	42	538	4	5	TRUE
119	31	500	42	502	4	5	TIME#0s
120	29	504	42	506	4	5	SAFETRUE
121	29	508	42	510	4	5	SAFETRUE
122	31	548	42	550	4	5	TIME#0s
123	29	552	42	554	4	5	SAFETRUE
124	29	556	42	558	4	5	SAFETRUE
149	4	492	36	494	4	5	SI_OutfeedDoorRightClosed
154	4	540	36	542	4	5	SI_OutfeedDoorRightLocked
159	88	492	119	494	4	3	s_OutfeedDoorRightClosed
160	88	540	119	542	4	3	s_OutfeedDoorRightLocked
164	34	391	42	393	4	5	TRUE
165	34	440	42	442	4	5	TRUE
166	31	403	42	405	4	5	TIME#0s
167	29	407	42	409	4	5	SAFETRUE
168	29	411	42	413	4	5	SAFETRUE
169	31	452	42	454	4	5	TIME#0s
170	29	456	42	458	4	5	SAFETRUE
171	29	460	42	462	4	5	SAFETRUE
196	4	444	35	446	4	5	SI_OutfeedDoorLeftLocked
201	4	395	34	397	4	5	SI_OutfeedDoorLeftClosed
206	88	395	117	397	4	3	s_OutfeedDoorLeftClosed
207	88	444	118	446	4	3	s_OutfeedDoorLeftLocked
222	34	146	42	148	4	5	TRUE
223	4	150	25	152	4	5	SI_InfeedShutter
228	31	158	42	160	4	5	TIME#0s
229	29	162	42	164	4	5	SAFETRUE
230	29	166	42	168	4	5	SAFETRUE
231	88	150	108	152	4	3	s_InfeedShutter
246	4	106	25	108	4	5	SI_ExternalSafety
251	34	102	42	104	4	5	TRUE
252	31	114	42	116	4	5	TIME#0s
253	29	118	42	120	4	5	SAFETRUE
254	29	122	42	124	4	5	SAFETRUE
255	97	106	117	108	4	3	s_ExternalSafety
261	116	39	143	41	4	3	m_EStopResetRequired
264	34	582	42	584	4	5	TRUE
265	31	594	42	596	4	5	TIME#0s
266	29	598	42	600	4	5	SAFETRUE
267	29	602	42	604	4	5	SAFETRUE
268	88	586	120	588	4	3	s_AddInfeedDoorLeftClosed
281	0	586	28	588	4	5	SI_AddInfeedLeftClosed
283	34	629	42	631	4	5	TRUE
284	31	641	42	643	4	5	TIME#0s
285	29	645	42	647	4	5	SAFETRUE
286	29	649	42	651	4	5	SAFETRUE
287	88	633	120	635	4	3	s_AddInfeedDoorLeftLocked
304	0	633	28	635	4	5	SI_AddInfeedLeftLocked
311	34	675	42	677	4	5	TRUE
312	31	687	42	689	4	5	TIME#0s
313	29	691	42	693	4	5	SAFETRUE
314	29	695	42	697	4	5	SAFETRUE
315	88	679	121	681	4	3	s_AddInfeedDoorRightClosed
317	34	721	42	723	4	5	TRUE
318	31	733	42	735	4	5	TIME#0s
319	29	737	42	739	4	5	SAFETRUE
320	29	741	42	743	4	5	SAFETRUE
321	88	725	122	727	4	3	s_AddInfeedDoorRightLocked
346	4	679	33	681	4	5	SI_AddInfeedRightClosed
351	4	725	34	727	4	5	SI_AddInfeedRightLocked
382	34	817	42	819	4	5	TRUE
383	34	770	42	772	4	5	TRUE
384	31	782	42	784	4	5	TIME#0s
385	29	786	42	788	4	5	SAFETRUE
386	29	790	42	792	4	5	SAFETRUE
387	31	829	42	831	4	5	TIME#0s
388	29	833	42	835	4	5	SAFETRUE
389	29	837	42	839	4	5	SAFETRUE
390	2	774	25	776	4	5	SI_EndpiceDoorLeft
394	0	821	25	823	4	5	SI_EndpiceDoorRight
399	88	821	111	823	4	3	s_EndpiceDoorRight
400	88	774	110	776	4	3	s_EndpiceDoorLeft
402	22	81	36	83	4	5	PLC_Reset
403	96	75	128	77	4	3	m_AirPressureResetRequired
404	97	71	113	73	4	3	s_AirPressure
405	16	71	35	73	4	5	SI_AirPressure1
406	16	75	35	77	4	5	SI_AirPressure2
419	97	79	125	81	4	3	m_AirPressureEquivalent

[FBS]
19
12	44	24	76	48	0	SF_EmergencyStop_V1_00	EmergencyStop
33	44	291	86	323	0	SF_GuardMonitoring_V1_00	InfeedDoorRightClosed
54	44	339	86	371	0	SF_GuardMonitoring_V1_00	InfeedDoorRightLocked
78	44	198	86	230	0	SF_GuardMonitoring_V1_00	InfeedDoorLeftClosed
91	44	244	86	276	0	SF_GuardMonitoring_V1_00	InfeedDoorLeftLocked
147	44	485	86	517	0	SF_GuardMonitoring_V1_00	OutfeedDoorRightClosed
148	44	533	86	565	0	SF_GuardMonitoring_V1_00	OutfeedDoorRightLocked
194	44	388	86	420	0	SF_GuardMonitoring_V1_00	OutfeedDoorLeftClosed
195	44	437	86	469	0	SF_GuardMonitoring_V1_00	OutfeedDoorLeftLocked
220	44	143	86	175	0	SF_GuardMonitoring_V1_00	InfeedShutter
244	44	99	86	131	0	SF_GuardMonitoring_V1_00	ExternalSafety
258	84	36	114	44	0	EStopResetRequired	EStopResetRequired_1
280	44	579	86	611	0	SF_GuardMonitoring_V1_00	AddInfeedLeftClosed
299	44	626	86	658	0	SF_GuardMonitoring_V1_00	AddInfeedLeftLocked
344	44	672	86	704	0	SF_GuardMonitoring_V1_00	AddInfeedClosed
345	44	718	86	750	0	SF_GuardMonitoring_V1_00	AddInfeedLocked
367	44	814	86	846	0	SF_GuardMonitoring_V1_00	EndpiceDoorRight
380	44	767	86	799	0	SF_GuardMonitoring_V1_00	EndpiceDoorLeft
413	43	68	89	84	0	LogicalAirpressure	LogicalAirpressure

[FPT]
193
3	44	27	55	29	Activate	0	128	0	BOOL
4	44	31	58	33	S_EStopIn	0	128	0	SAFEBOOL
5	44	35	61	37	S_StartReset	0	128	0	SAFEBOOL
6	44	39	61	41	S_AutoReset	0	128	0	SAFEBOOL
7	44	43	53	45	Reset	0	128	0	BOOL
8	68	27	76	29	Ready	1	0	128	BOOL
9	61	31	76	33	S_EStopOut	1	0	128	SAFEBOOL
10	69	35	76	37	Error	1	0	128	BOOL
11	64	39	76	41	DiagCode	1	0	128	WORD
22	44	294	55	296	Activate	0	128	0	BOOL
23	44	298	64	300	S_GuardSwitch1	0	128	0	SAFEBOOL
24	44	302	64	304	S_GuardSwitch2	0	128	0	SAFEBOOL
25	44	306	64	308	DiscrepancyTime	0	128	0	TIME
26	44	310	61	312	S_StartReset	0	128	0	SAFEBOOL
27	44	314	61	316	S_AutoReset	0	128	0	SAFEBOOL
28	44	318	53	320	Reset	0	128	0	BOOL
29	78	294	86	296	Ready	1	0	128	BOOL
30	64	298	86	300	S_GuardMonitoring	1	0	128	SAFEBOOL
31	79	302	86	304	Error	1	0	128	BOOL
32	74	306	86	308	DiagCode	1	0	128	WORD
43	44	342	55	344	Activate	0	128	0	BOOL
44	44	346	64	348	S_GuardSwitch1	0	128	0	SAFEBOOL
45	44	350	64	352	S_GuardSwitch2	0	128	0	SAFEBOOL
46	44	354	64	356	DiscrepancyTime	0	128	0	TIME
47	44	358	61	360	S_StartReset	0	128	0	SAFEBOOL
48	44	362	61	364	S_AutoReset	0	128	0	SAFEBOOL
49	44	366	53	368	Reset	0	128	0	BOOL
50	78	342	86	344	Ready	1	0	128	BOOL
51	64	346	86	348	S_GuardMonitoring	1	0	128	SAFEBOOL
52	79	350	86	352	Error	1	0	128	BOOL
53	74	354	86	356	DiagCode	1	0	128	WORD
67	44	201	55	203	Activate	0	128	0	BOOL
68	44	205	64	207	S_GuardSwitch1	0	128	0	SAFEBOOL
69	44	209	64	211	S_GuardSwitch2	0	128	0	SAFEBOOL
70	44	213	64	215	DiscrepancyTime	0	128	0	TIME
71	44	217	61	219	S_StartReset	0	128	0	SAFEBOOL
72	44	221	61	223	S_AutoReset	0	128	0	SAFEBOOL
73	44	225	53	227	Reset	0	128	0	BOOL
74	78	201	86	203	Ready	1	0	128	BOOL
75	64	205	86	207	S_GuardMonitoring	1	0	128	SAFEBOOL
76	79	209	86	211	Error	1	0	128	BOOL
77	74	213	86	215	DiagCode	1	0	128	WORD
80	44	247	55	249	Activate	0	128	0	BOOL
81	44	251	64	253	S_GuardSwitch1	0	128	0	SAFEBOOL
82	44	255	64	257	S_GuardSwitch2	0	128	0	SAFEBOOL
83	44	259	64	261	DiscrepancyTime	0	128	0	TIME
84	44	263	61	265	S_StartReset	0	128	0	SAFEBOOL
85	44	267	61	269	S_AutoReset	0	128	0	SAFEBOOL
86	44	271	53	273	Reset	0	128	0	BOOL
87	78	247	86	249	Ready	1	0	128	BOOL
88	64	251	86	253	S_GuardMonitoring	1	0	128	SAFEBOOL
89	79	255	86	257	Error	1	0	128	BOOL
90	74	259	86	261	DiagCode	1	0	128	WORD
125	44	488	55	490	Activate	0	128	0	BOOL
126	44	492	64	494	S_GuardSwitch1	0	128	0	SAFEBOOL
127	44	496	64	498	S_GuardSwitch2	0	128	0	SAFEBOOL
128	44	500	64	502	DiscrepancyTime	0	128	0	TIME
129	44	504	61	506	S_StartReset	0	128	0	SAFEBOOL
130	44	508	61	510	S_AutoReset	0	128	0	SAFEBOOL
131	44	512	53	514	Reset	0	128	0	BOOL
132	78	488	86	490	Ready	1	0	128	BOOL
133	64	492	86	494	S_GuardMonitoring	1	0	128	SAFEBOOL
134	79	496	86	498	Error	1	0	128	BOOL
135	74	500	86	502	DiagCode	1	0	128	WORD
136	44	536	55	538	Activate	0	128	0	BOOL
137	44	540	64	542	S_GuardSwitch1	0	128	0	SAFEBOOL
138	44	544	64	546	S_GuardSwitch2	0	128	0	SAFEBOOL
139	44	548	64	550	DiscrepancyTime	0	128	0	TIME
140	44	552	61	554	S_StartReset	0	128	0	SAFEBOOL
141	44	556	61	558	S_AutoReset	0	128	0	SAFEBOOL
142	44	560	53	562	Reset	0	128	0	BOOL
143	78	536	86	538	Ready	1	0	128	BOOL
144	64	540	86	542	S_GuardMonitoring	1	0	128	SAFEBOOL
145	79	544	86	546	Error	1	0	128	BOOL
146	74	548	86	550	DiagCode	1	0	128	WORD
172	44	391	55	393	Activate	0	128	0	BOOL
173	44	395	64	397	S_GuardSwitch1	0	128	0	SAFEBOOL
174	44	399	64	401	S_GuardSwitch2	0	128	0	SAFEBOOL
175	44	403	64	405	DiscrepancyTime	0	128	0	TIME
176	44	407	61	409	S_StartReset	0	128	0	SAFEBOOL
177	44	411	61	413	S_AutoReset	0	128	0	SAFEBOOL
178	44	415	53	417	Reset	0	128	0	BOOL
179	78	391	86	393	Ready	1	0	128	BOOL
180	64	395	86	397	S_GuardMonitoring	1	0	128	SAFEBOOL
181	79	399	86	401	Error	1	0	128	BOOL
182	74	403	86	405	DiagCode	1	0	128	WORD
183	44	440	55	442	Activate	0	128	0	BOOL
184	44	444	64	446	S_GuardSwitch1	0	128	0	SAFEBOOL
185	44	448	64	450	S_GuardSwitch2	0	128	0	SAFEBOOL
186	44	452	64	454	DiscrepancyTime	0	128	0	TIME
187	44	456	61	458	S_StartReset	0	128	0	SAFEBOOL
188	44	460	61	462	S_AutoReset	0	128	0	SAFEBOOL
189	44	464	53	466	Reset	0	128	0	BOOL
190	78	440	86	442	Ready	1	0	128	BOOL
191	64	444	86	446	S_GuardMonitoring	1	0	128	SAFEBOOL
192	79	448	86	450	Error	1	0	128	BOOL
193	74	452	86	454	DiagCode	1	0	128	WORD
209	44	146	55	148	Activate	0	128	0	BOOL
210	44	150	64	152	S_GuardSwitch1	0	128	0	SAFEBOOL
211	44	154	64	156	S_GuardSwitch2	0	128	0	SAFEBOOL
212	44	158	64	160	DiscrepancyTime	0	128	0	TIME
213	44	162	61	164	S_StartReset	0	128	0	SAFEBOOL
214	44	166	61	168	S_AutoReset	0	128	0	SAFEBOOL
215	44	170	53	172	Reset	0	128	0	BOOL
216	78	146	86	148	Ready	1	0	128	BOOL
217	64	150	86	152	S_GuardMonitoring	1	0	128	SAFEBOOL
218	79	154	86	156	Error	1	0	128	BOOL
219	74	158	86	160	DiagCode	1	0	128	WORD
233	44	102	55	104	Activate	0	128	0	BOOL
234	44	106	64	108	S_GuardSwitch1	0	128	0	SAFEBOOL
235	44	110	64	112	S_GuardSwitch2	0	128	0	SAFEBOOL
236	44	114	64	116	DiscrepancyTime	0	128	0	TIME
237	44	118	61	120	S_StartReset	0	128	0	SAFEBOOL
238	44	122	61	124	S_AutoReset	0	128	0	SAFEBOOL
239	44	126	53	128	Reset	0	128	0	BOOL
240	78	102	86	104	Ready	1	0	128	BOOL
241	64	106	86	108	S_GuardMonitoring	1	0	128	SAFEBOOL
242	79	110	86	112	Error	1	0	128	BOOL
243	74	114	86	116	DiagCode	1	0	128	WORD
256	84	39	97	41	DiagCode	0	128	0	WORD
257	97	39	114	41	ResetRequired	1	0	129	BOOL
269	44	582	55	584	Activate	0	128	0	BOOL
270	44	586	64	588	S_GuardSwitch1	0	128	0	SAFEBOOL
271	44	590	64	592	S_GuardSwitch2	0	128	0	SAFEBOOL
272	44	594	64	596	DiscrepancyTime	0	128	0	TIME
273	44	598	61	600	S_StartReset	0	128	0	SAFEBOOL
274	44	602	61	604	S_AutoReset	0	128	0	SAFEBOOL
275	44	606	53	608	Reset	0	128	0	BOOL
276	78	582	86	584	Ready	1	0	128	BOOL
277	64	586	86	588	S_GuardMonitoring	1	0	128	SAFEBOOL
278	79	590	86	592	Error	1	0	128	BOOL
279	74	594	86	596	DiagCode	1	0	128	WORD
288	44	629	55	631	Activate	0	128	0	BOOL
289	44	633	64	635	S_GuardSwitch1	0	128	0	SAFEBOOL
290	44	637	64	639	S_GuardSwitch2	0	128	0	SAFEBOOL
291	44	641	64	643	DiscrepancyTime	0	128	0	TIME
292	44	645	61	647	S_StartReset	0	128	0	SAFEBOOL
293	44	649	61	651	S_AutoReset	0	128	0	SAFEBOOL
294	44	653	53	655	Reset	0	128	0	BOOL
295	78	629	86	631	Ready	1	0	128	BOOL
296	64	633	86	635	S_GuardMonitoring	1	0	128	SAFEBOOL
297	79	637	86	639	Error	1	0	128	BOOL
298	74	641	86	643	DiagCode	1	0	128	WORD
322	44	675	55	677	Activate	0	128	0	BOOL
323	44	679	64	681	S_GuardSwitch1	0	128	0	SAFEBOOL
324	44	683	64	685	S_GuardSwitch2	0	128	0	SAFEBOOL
325	44	687	64	689	DiscrepancyTime	0	128	0	TIME
326	44	691	61	693	S_StartReset	0	128	0	SAFEBOOL
327	44	695	61	697	S_AutoReset	0	128	0	SAFEBOOL
328	44	699	53	701	Reset	0	128	0	BOOL
329	78	675	86	677	Ready	1	0	128	BOOL
330	64	679	86	681	S_GuardMonitoring	1	0	128	SAFEBOOL
331	79	683	86	685	Error	1	0	128	BOOL
332	74	687	86	689	DiagCode	1	0	128	WORD
333	44	721	55	723	Activate	0	128	0	BOOL
334	44	725	64	727	S_GuardSwitch1	0	128	0	SAFEBOOL
335	44	729	64	731	S_GuardSwitch2	0	128	0	SAFEBOOL
336	44	733	64	735	DiscrepancyTime	0	128	0	TIME
337	44	737	61	739	S_StartReset	0	128	0	SAFEBOOL
338	44	741	61	743	S_AutoReset	0	128	0	SAFEBOOL
339	44	745	53	747	Reset	0	128	0	BOOL
340	78	721	86	723	Ready	1	0	128	BOOL
341	64	725	86	727	S_GuardMonitoring	1	0	128	SAFEBOOL
342	79	729	86	731	Error	1	0	128	BOOL
343	74	733	86	735	DiagCode	1	0	128	WORD
356	44	817	55	819	Activate	0	128	0	BOOL
357	44	821	64	823	S_GuardSwitch1	0	128	0	SAFEBOOL
358	44	825	64	827	S_GuardSwitch2	0	128	0	SAFEBOOL
359	44	829	64	831	DiscrepancyTime	0	128	0	TIME
360	44	833	61	835	S_StartReset	0	128	0	SAFEBOOL
361	44	837	61	839	S_AutoReset	0	128	0	SAFEBOOL
362	44	841	53	843	Reset	0	128	0	BOOL
363	78	817	86	819	Ready	1	0	128	BOOL
364	64	821	86	823	S_GuardMonitoring	1	0	128	SAFEBOOL
365	79	825	86	827	Error	1	0	128	BOOL
366	74	829	86	831	DiagCode	1	0	128	WORD
369	44	770	55	772	Activate	0	128	0	BOOL
370	44	774	64	776	S_GuardSwitch1	0	128	0	SAFEBOOL
371	44	778	64	780	S_GuardSwitch2	0	128	0	SAFEBOOL
372	44	782	64	784	DiscrepancyTime	0	128	0	TIME
373	44	786	61	788	S_StartReset	0	128	0	SAFEBOOL
374	44	790	61	792	S_AutoReset	0	128	0	SAFEBOOL
375	44	794	53	796	Reset	0	128	0	BOOL
376	78	770	86	772	Ready	1	0	128	BOOL
377	64	774	86	776	S_GuardMonitoring	1	0	128	SAFEBOOL
378	79	778	86	780	Error	1	0	128	BOOL
379	74	782	86	784	DiagCode	1	0	128	WORD
407	43	71	61	73	S_AirPressure1	0	129	0	SAFEBOOL
408	43	75	61	77	S_AirPressure2	0	129	0	SAFEBOOL
409	43	81	52	83	Reset	0	129	0	BOOL
410	69	71	89	73	S_AirPressureOut	1	0	129	SAFEBOOL
411	66	75	89	77	ResetRequiredEStop	1	0	129	BOOL
412	61	79	89	81	ResetRequiredEquivalent	1	0	129	BOOL

[KOT]
0

[VER]
0

