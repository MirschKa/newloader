﻿<?xml version="1.0" encoding="utf-8"?>
<?AutomationStudio Version=4.8.1.174?>
<SwConfiguration CpuAddress="SL1" xmlns="http://br-automation.co.at/AS/SwConfiguration">
  <TaskClass Name="Exception" />
  <TaskClass Name="Cyclic#1" />
  <TaskClass Name="Cyclic#2" />
  <TaskClass Name="Cyclic#3">
    <Task Name="SafetySol" Source="Safety.SafetySol.prg" Memory="UserROM" Language="IEC" Debugging="true" />
  </TaskClass>
  <TaskClass Name="Cyclic#4">
    <Task Name="loader_mai" Source="Loader.loader_main.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="SafeInterf" Source="Safety.SafeInterface.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="s_supply" Source="Loader.Stations.Supply.s_supply.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="m_acppower" Source="Loader.Stations.Supply.Modules.m_acppower.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="m_newMod" Source="Templates.NewModule.m_newMod.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="ComTCP_P" Source="Libraries.ComTCP.ComTCP_P.prg" Memory="UserROM" Language="ANSIC" Debugging="true" />
    <Task Name="i_induphy" Source="Libraries.ComTCP.i_induphy.prg" Memory="UserROM" Language="IEC" Debugging="true" />
  </TaskClass>
  <TaskClass Name="Cyclic#5" />
  <TaskClass Name="Cyclic#6">
    <Task Name="CSI_STD" Source="Hmi.Template.Interface.CSI.Tasks.Application.CSI_STD.CSI_STD.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="CSI_ML_1" Source="Hmi.Template.Interface.CSI.Tasks.Application.CSI_ML.CSI_ML_1.prg" Memory="UserROM" Language="IEC" Debugging="true" />
  </TaskClass>
  <TaskClass Name="Cyclic#7">
    <Task Name="CSI_ML_2" Source="Hmi.Template.Interface.CSI.Tasks.Application.CSI_ML.CSI_ML_2.prg" Memory="UserROM" Language="IEC" Debugging="true" />
  </TaskClass>
  <TaskClass Name="Cyclic#8">
    <Task Name="PLCinfo" Source="Hmi.Template.SystemFunctions.PLCinfo.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="TestDialog" Source="Hmi.TestDialog1.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="TestDialo1" Source="Hmi.TestDialog2.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="mach_id" Source="Hmi.Machine.mach_id.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="dialogh" Source="Hmi.Template.DialogHandling.dialogh.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="alarmh" Source="Hmi.Template.AlarmHandling.alarmh.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="io_diag" Source="Hmi.HardwareDiagnostics.IO_Information.io_diag.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="dialogIF" Source="Hmi.Template.DialogHandling.dialogIF.prg" Memory="UserROM" Language="IEC" Debugging="true" />
  </TaskClass>
  <Binaries>
    <BinaryObject Name="TCData" Source="" Memory="SystemROM" Language="Binary" />
    <BinaryObject Name="udbdef" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="mvLoader" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="FWRules" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="mCoWebSc" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="sysconf" Source="" Memory="SystemROM" Language="Binary" />
    <BinaryObject Name="Role" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="iomap" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="ashwd" Source="" Memory="SystemROM" Language="Binary" />
    <BinaryObject Name="asfw" Source="" Memory="SystemROM" Language="Binary" />
    <BinaryObject Name="arconfig" Source="" Memory="SystemROM" Language="Binary" />
    <BinaryObject Name="User" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="ashwac" Source="" Memory="UserROM" Language="Binary" />
  </Binaries>
  <Libraries>
    <LibraryObject Name="D_Driver" Source="Libraries.shared_DriverLibs.D_Driver.lby" Memory="UserROM" Language="IEC" Debugging="true" />
    <LibraryObject Name="D_DigIn" Source="Libraries.shared_DriverLibs.D_DigIn.lby" Memory="UserROM" Language="IEC" Debugging="true" />
    <LibraryObject Name="D_DigOut" Source="Libraries.shared_DriverLibs.D_DigOut.lby" Memory="UserROM" Language="IEC" Debugging="true" />
    <LibraryObject Name="D_Cylinder" Source="Libraries.shared_DriverLibs.D_Cylinder.lby" Memory="UserROM" Language="IEC" Debugging="true" />
    <LibraryObject Name="D_AnOut" Source="Libraries.shared_DriverLibs.D_AnOut.lby" Memory="UserROM" Language="IEC" Debugging="true" />
    <LibraryObject Name="D_AnIn" Source="Libraries.shared_DriverLibs.D_AnIn.lby" Memory="UserROM" Language="IEC" Debugging="true" />
    <LibraryObject Name="D_Sensor" Source="Libraries.shared_DriverLibs.D_Sensor.lby" Memory="UserROM" Language="IEC" Debugging="true" />
    <LibraryObject Name="D_Motor" Source="Libraries.shared_DriverLibs.D_Motor.lby" Memory="UserROM" Language="IEC" Debugging="true" />
    <LibraryObject Name="D_PwmMotor" Source="Libraries.shared_DriverLibs.D_PwmMotor.lby" Memory="UserROM" Language="IEC" Debugging="true" />
    <LibraryObject Name="D_Motion" Source="Libraries.shared_DriverLibs.D_Motion.lby" Memory="UserROM" Language="IEC" Debugging="true" />
    <LibraryObject Name="D_IoSensor" Source="Libraries.shared_DriverLibs.D_IoSensor.lby" Memory="UserROM" Language="IEC" Debugging="true" />
    <LibraryObject Name="_supply" Source="Loader.Stations.Supply._supply.lby" Memory="UserROM" Language="IEC" Debugging="true" />
    <LibraryObject Name="_newmod" Source="Templates.NewModule._newmod.lby" Memory="UserROM" Language="IEC" Debugging="true" />
    <LibraryObject Name="_acppower" Source="Loader.Stations.Supply.Modules._acppower.lby" Memory="UserROM" Language="IEC" Debugging="true" />
    <LibraryObject Name="GeaModeMan" Source="Libraries.shared_FrameworkLibs.GeaModeMan.lby" Memory="UserROM" Language="IEC" Debugging="true" />
    <LibraryObject Name="GeaGeneral" Source="Libraries.shared_FrameworkLibs.GeaGeneral.lby" Memory="UserROM" Language="IEC" Debugging="true" />
    <LibraryObject Name="GeaEvent" Source="Libraries.shared_FrameworkLibs.GeaEvent.lby" Memory="UserROM" Language="IEC" Debugging="true" />
    <LibraryObject Name="GeaString" Source="Libraries.shared_FrameworkLibs.GeaString.lby" Memory="UserROM" Language="IEC" Debugging="true" />
    <LibraryObject Name="GeaStation" Source="Libraries.shared_FrameworkLibs.GeaStation.lby" Memory="UserROM" Language="IEC" Debugging="true" />
    <LibraryObject Name="AsEPL" Source="Safety.Libraries.AsEPL.lby" Memory="UserROM" Language="binary" Debugging="true" />
    <LibraryObject Name="runtime" Source="Libraries.SystemLibs.runtime.lby" Memory="UserROM" Language="binary" Debugging="true" />
    <LibraryObject Name="operator" Source="Libraries.SystemLibs.operator.lby" Memory="UserROM" Language="binary" Debugging="true" />
    <LibraryObject Name="astime" Source="Libraries.SystemLibs.astime.lby" Memory="UserROM" Language="binary" Debugging="true" />
    <LibraryObject Name="AsIecCon" Source="Libraries.SystemLibs.AsIecCon.lby" Memory="UserROM" Language="binary" Debugging="true" />
    <LibraryObject Name="standard" Source="Libraries.SystemLibs.standard.lby" Memory="UserROM" Language="binary" Debugging="true" />
    <LibraryObject Name="sys_lib" Source="Libraries.SystemLibs.sys_lib.lby" Memory="UserROM" Language="binary" Debugging="true" />
    <LibraryObject Name="AsBrStr" Source="Libraries.SystemLibs.AsBrStr.lby" Memory="UserROM" Language="binary" Debugging="true" />
    <LibraryObject Name="brsystem" Source="Libraries.SystemLibs.brsystem.lby" Memory="UserROM" Language="binary" Debugging="true" />
    <LibraryObject Name="AsIODiag" Source="Libraries.SystemLibs.AsIODiag.lby" Memory="UserROM" Language="binary" Debugging="true" />
    <LibraryObject Name="FileIO" Source="Libraries.SystemLibs.FileIO.lby" Memory="UserROM" Language="binary" Debugging="true" />
    <LibraryObject Name="AsSafety" Source="Safety.Libraries.AsSafety.lby" Memory="UserROM" Language="binary" Debugging="true" />
    <LibraryObject Name="DataObj" Source="Safety.Libraries.DataObj.lby" Memory="UserROM" Language="binary" Debugging="true" />
    <LibraryObject Name="AsXml" Source="Safety.Libraries.AsXml.lby" Memory="UserROM" Language="binary" Debugging="true" />
    <LibraryObject Name="BrbLib" Source="Safety.Libraries.BrbLib.lby" Memory="UserROM" Language="ANSIC" Debugging="true" />
    <LibraryObject Name="visapi" Source="Libraries.SystemLibs.visapi.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="AsUSB" Source="Libraries.SystemLibs.AsUSB.lby" Memory="UserROM" Language="binary" Debugging="true" />
    <LibraryObject Name="AsBrMath" Source="Libraries.SystemLibs.AsBrMath.lby" Memory="UserROM" Language="binary" Debugging="true" />
    <LibraryObject Name="CFS_EXC" Source="Hmi.Library.CFS_EXC.lby" Memory="UserROM" Language="IEC" Debugging="true" />
    <LibraryObject Name="AsZip" Source="Libraries.SystemLibs.AsZip.lby" Memory="UserROM" Language="binary" Debugging="true" />
    <LibraryObject Name="CoTrace" Source="Libraries.SystemLibs.CoTrace.lby" Memory="UserROM" Language="binary" Debugging="true" />
    <LibraryObject Name="McAcpAx" Source="Libraries.McAcpAx.lby" Memory="UserROM" Language="binary" Debugging="true" />
    <LibraryObject Name="McBase" Source="Libraries.McBase.lby" Memory="UserROM" Language="binary" Debugging="true" />
    <LibraryObject Name="McAxis" Source="Libraries.McAxis.lby" Memory="UserROM" Language="binary" Debugging="true" />
    <LibraryObject Name="McPureVAx" Source="Libraries.McPureVAx.lby" Memory="UserROM" Language="binary" Debugging="true" />
    <LibraryObject Name="MpAlarmX" Source="Libraries.MpAlarmX.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="MpBase" Source="Libraries.SystemLibs.MpBase.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="MpAxis" Source="Libraries.MpAxis.lby" Memory="UserROM" Language="binary" Debugging="true" />
    <LibraryObject Name="StpCtrl" Source="Libraries.shared_FrameworkLibs.StpCtrl.lby" Memory="UserROM" Language="IEC" Debugging="true" />
    <LibraryObject Name="HWDiag" Source="Hmi.HardwareDiagnostics.IO_Information.Libraries.HWDiag.lby" Memory="UserROM" Language="ANSIC" Debugging="true" />
    <LibraryObject Name="AsIO" Source="Hmi.HardwareDiagnostics.IO_Information.Libraries.AsIO.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="AsIOMMan" Source="Hmi.HardwareDiagnostics.IO_Information.Libraries.AsIOMMan.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="AsIOLink" Source="Libraries.SystemLibs.AsIOLink.lby" Memory="UserROM" Language="binary" Debugging="true" />
    <LibraryObject Name="AsIOAcc" Source="Libraries.SystemLibs.AsIOAcc.lby" Memory="UserROM" Language="binary" Debugging="true" />
    <LibraryObject Name="CFS_DLG" Source="Hmi.Library.CFS_DLG.lby" Memory="UserROM" Language="IEC" Debugging="true" />
    <LibraryObject Name="LoopCont" Source="Hmi.Template.UserManagement.Logging.Libraries.LoopCont.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="L_CSI300" Source="Hmi.Template.Interface.CSI.Libraries.Standard.CSI_STD_V300.L_CSI300.lby" Memory="UserROM" Language="IEC" Debugging="true" />
    <LibraryObject Name="L_SM" Source="Hmi.Template.Interface.CSI.Libraries.Standard.CSI_TLS.StateModel.L_SM.lby" Memory="UserROM" Language="IEC" Debugging="true" />
    <LibraryObject Name="L_SEQ" Source="Hmi.Template.Interface.CSI.Libraries.Standard.CSI_TLS.Sequence.L_SEQ.lby" Memory="UserROM" Language="IEC" Debugging="true" />
    <LibraryObject Name="L_TCP" Source="Hmi.Template.Interface.CSI.Libraries.Standard.CSI_TLS.Communication.L_TCP.lby" Memory="UserROM" Language="IEC" Debugging="true" />
    <LibraryObject Name="F_UAClient" Source="Hmi.Template.Interface.CSI.Libraries.Standard.CSI_TLS.Communication.F_UAClient.lby" Memory="UserROM" Language="IEC" Debugging="true" />
    <LibraryObject Name="L_REC" Source="Hmi.Template.Interface.CSI.Libraries.Standard.CSI_TLS.Recipe.L_REC.lby" Memory="UserROM" Language="IEC" Debugging="true" Disabled="true" />
    <LibraryObject Name="L_HS" Source="Hmi.Template.Interface.CSI.Libraries.Standard.CSI_TLS.Handshake.L_HS.lby" Memory="UserROM" Language="IEC" Debugging="true" />
    <LibraryObject Name="L_CSI_EXT2" Source="Hmi.Template.Interface.CSI.Libraries.Standard.CSI_EXT.L_CSI_EXT2.lby" Memory="UserROM" Language="IEC" Debugging="true" />
    <LibraryObject Name="AsTCP" Source="Libraries.SystemLibs.AsTCP.lby" Memory="UserROM" Language="binary" Debugging="true" />
    <LibraryObject Name="AsOpcUac" Source="Libraries.SystemLibs.AsOpcUac.lby" Memory="UserROM" Language="binary" Debugging="true" />
    <LibraryObject Name="ComTCP_L" Source="Libraries.ComTCP.ComTCP_L.lby" Memory="None" Language="ANSIC" Debugging="true" />
    <LibraryObject Name="arssl" Source="" Memory="UserROM" Language="Binary" Debugging="true" />
  </Libraries>
</SwConfiguration>