/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: BrbLib
 * File: BrbGetRandomDint.c
 * Author: niedermeierr
 * Created: June 11, 2013
 ********************************************************************
 * Implementation of library BrbLib
 ********************************************************************/

#include <bur/plctypes.h>
#ifdef __cplusplus
	extern "C"
	{
#endif

	#include "BrbLib.h"

#ifdef __cplusplus
	};
#endif

/* Erzeugt eine Zufallszahl zwischen nMin und nMax */
signed long BrbGetRandomDint(signed long nMin, signed long nMax)
{
	DINT nRandom = 0;
	if(nMin == -2147483648UL || nMax == 2147483647)
	{
		nRandom = (DINT)(BrbGetRandomPercent()*nMax);
		BOOL bMinus = BrbGetRandomBool();
		if(bMinus == 1)
		{
			nRandom = nRandom * -1;
		}
	}
	else
	{
		nRandom = (DINT)(BrbGetRandomPercent()*(nMax-nMin+1))+nMin;
	}
	return nRandom;
}
