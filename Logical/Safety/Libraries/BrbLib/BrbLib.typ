(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: BrbLib
 * File: BrbLib.typ
 * Author: niedermeierr
 * Created: June 11, 2013
 ********************************************************************
 * Data types of library BrbLib
 ********************************************************************)
(*StepHandling*)

TYPE
	BrbStepHandlingCurrent_TYP : 	STRUCT 
		nStepNr : DINT; (*Aktuelle Schrittnummer*)
		sStepText : STRING[nBRB_STEP_TEXT_CHAR_MAX]; (*Aktueller Schritttext*)
		bTimeoutElapsed : BOOL; (*1=Timeout abgelaufen*)
		nTimeoutContinueStep : DINT; (*Schrittnummer nach Timeout*)
	END_STRUCT;
	BrbStepHandlingStep_TYP : 	STRUCT 
		nStepNr : DINT; (*Schrittnummer*)
		sStepText : STRING[nBRB_STEP_TEXT_CHAR_MAX]; (*Schritttext*)
		nCycleCount : UDINT; (*Zyklen*)
	END_STRUCT;
	BrbStepHandlingLog_TYP : 	STRUCT 
		bClear : BOOL; (*Kommando zum L�schen der Protokollierung*)
		bStop : BOOL; (*Kommando zum Stoppen der Protokollierung*)
		Steps : ARRAY[0..nBRB_STEPLOG_STEPS_MAX]OF BrbStepHandlingStep_TYP; (*Protokollierung*)
	END_STRUCT;
	BrbStepHandlingIntern_TYP : 	STRUCT 
		nStepNrOld : DINT; (*Alte Schrittnummer*)
		nCycleCount : UDINT; (*Aktueller Zyklusz�hler*)
		bLogOnNextCycle : BOOL; (*Flag bei Schrittwechsel*)
		fbTimeout : TON; (*Timeout-Fub*)
	END_STRUCT;
	BrbStepHandling_TYP : 	STRUCT 
		Current : BrbStepHandlingCurrent_TYP; (*Daten zum aktuellen Schritt*)
		Log : BrbStepHandlingLog_TYP; (*Protokollierung*)
		Intern : BrbStepHandlingIntern_TYP; (*Interne Daten*)
	END_STRUCT;
	BrbStopWatch_TYP : 	STRUCT 
		tStartTime : TIME; (*Start-Zeitstempel*)
		tStopTime : TIME; (*End-Zeitstempel*)
		nTimeDiff : UDINT; (*Berechnete Differenz*)
		sTimeDiff : STRING[24]; (*Differenz als Text*)
	END_STRUCT;
END_TYPE

(*TaskCommunication*)

TYPE
	BrbCallerStates_ENUM : 
		(
		eBRB_CALLER_STATE_NOT_READY := -1, (*-1=Nicht bereit*)
		eBRB_CALLER_STATE_OK := 0, (*0=Bereit*)
		eBRB_CALLER_STATE_BUSY := 1 (*1=Besetzt*)
		);
	BrbCaller_TYP : 	STRUCT 
		nCallerId : DINT; (*Task-ID*)
		bLock : BOOL; (*Semaphor-Flag*)
	END_STRUCT;
END_TYPE

(*VarHandling*)

TYPE
	BrbPvInfo_TYP : 	STRUCT 
		sName : STRING[nBRB_PVNAME_CHAR_MAX]; (*Variablen-Name*)
		nAdr : UDINT; (*Variablen-Adresse*)
		nDataType : UDINT; (*Variablen-Datentyp*)
		nLen : UDINT; (*Variablen-Gr��e*)
		nDimension : UINT; (*Max. Array-Index*)
		nItemIndex : UINT; (*Index des Array- oder Struktur-Items*)
	END_STRUCT;
END_TYPE

(*FileHandling*)

TYPE
	BrbUsbDeviceListEntry_TYP : 	STRUCT 
		sInterfaceName : STRING[nBRB_DEVICE_NAME_CHAR_MAX]; (*Schnittstellen-Name*)
		sDeviceName : STRING[nBRB_DEVICE_NAME_CHAR_MAX]; (*Schnittstellen-Name*)
		nNode : UDINT; (*Interne Node-Nummer*)
		nHandle : UDINT; (*Internes Handle*)
	END_STRUCT;
	BrbDirInfoFilter_ENUM : 
		(
		eBRB_DIR_INFO_ONLY_FILES, (*0=Nur Dateien*)
		eBRB_DIR_INFO_ONLY_DIRS, (*1=Nur Verzeichnisse*)
		eBRB_DIR_INFO_FILES_AND_DIRS (*2=Dateien und Verzeichnisse*)
		);
	BrbFileSorting_ENUM : 
		(
		eBRB_FILE_SORTING_NONE, (*0=Keine Sortierung*)
		eBRB_FILE_SORTING_ALPH_UP, (*1=Sortierung nach aufsteigendem Alphabet*)
		eBRB_FILE_SORTING_ALPH_DOWN, (*2=Sortierung nach aufsteigendem Alphabet*)
		eBRB_FILE_SORTING_OLDEST, (*3=Sortierung nach �ltesten Dateien*)
		eBRB_FILE_SORTING_YOUNGEST, (*4=Sortierung nach j�ngsten Dateien*)
		eBRB_FILE_SORTING_BIGGEST, (*5=Sortierung nach gr��ten Dateien*)
		eBRB_FILE_SORTING_SMALLEST (*6=Sortierung nach kleinsten Dateien*)
		);
	BrbReadDirListEntry_TYP : 	STRUCT 
		sName : STRING[nBRB_FILE_NAME_CHAR_MAX]; (*Verzeichnis- oder Datei-Name*)
		dtDate : DATE_AND_TIME; (*Zeitstempel*)
		nSize : UDINT; (*Gr��e (0=Verzeichnis)*)
	END_STRUCT;
END_TYPE

(*VisVc4*)

TYPE
	BrbVc4TouchStates_ENUM : 
		(
		eBRB_TOUCH_STATE_UNPUSHED := 0, (*0=Nicht gedr�ckt*)
		eBRB_TOUCH_STATE_UNPUSHED_EDGE := 1, (*1=Gerade losgelassen*)
		eBRB_TOUCH_STATE_PUSHED_EDGE := 2, (*2=Gerade gedr�ckt*)
		eBRB_TOUCH_STATE_PUSHED := 3, (*3=Gedr�ckt*)
		eBRB_TOUCH_STATE_DOUBLECLICK := 4 (*4=Doppelklick (nur f�r einen Zyklus)*)
		);
	BrbVc4GeneralTouch_TYP : 	STRUCT 
		nX : UDINT; (*Ausgang: X-Koordinate in [Pixel]*)
		nY : UDINT; (*Ausgang: Y-Koordinate in [Pixel]*)
		eState : BrbVc4TouchStates_ENUM; (*Ausgang: Momentaner Status des Touchs*)
		eStateSync : BrbVc4TouchStates_ENUM; (*Ausgang: Mit Redraw-Counter synchronisierter Status des Touchs*)
		fbDoubleClickDelay : TON; (*Interne Variable*)
		TouchAction : TouchAction; (*Interne Variable*)
		TouchActionOld : TouchAction; (*Interne Variable*)
	END_STRUCT;
	BrbVc4General_TYP : 	STRUCT 
		sVisName : STRING[nBRB_VIS_NAME_CHAR_MAX]; (*Eingang: Name des Vc-Objekts*)
		tDoubleclickDelay : TIME; (*Eingang: Zeit zwischen Loslassen und erneutem Dr�cken in [ms] zum Erkennen eines Doppelklicks*)
		nDoubleclickDrift : UDINT; (*Eingang: Max. Abstand zweier Klicks in [Pixel] zum Erkennen eines Doppelklicks*)
		nRedrawCycles : UINT; (*Eingang: Anzahl der Zyklen f�r das Zeichnen*)
		bDisableRedraw : BOOL; (*Eingang: 1=L�schen von eigenen Zeichnungen unterdr�cken*)
		nLanguageCurrent : UINT; (*Zur Anbindung an den Datenpunkt*)
		nLanguageChange : UINT; (*Zur Anbindung an den Datenpunkt*)
		nLifeSign : UDINT; (*Zur Anbindung an den Datenpunkt*)
		nVcHandle : UDINT; (*Ausgang: VcHandle der Visualisierung*)
		Touch : BrbVc4GeneralTouch_TYP; (*Aktuelle Touch-Daten*)
		nRedrawCounter : UINT; (*Ausgang: Z�hler f�rs Zeichnen (bei = 0 darf gezeichnet werden)*)
	END_STRUCT;
	BrbVc4ScreenSaver_TYP : 	STRUCT 
		bEnable : BOOL; (*Eingang: 1=Eingeschaltet*)
		hsScreen : BrbVc4Hotspot_TYP; (*Control: Optionaler Hotspot f�r R�ckschaltung*)
		nScreenSaverPage : UINT; (*Eingang: Bildschirmschoner-Seite*)
		fbScreenSaver : TON; (*Interne Variable*)
		TouchOld : BrbVc4GeneralTouch_TYP; (*Interne Variable*)
		nPageBeforeScreenSaver : DINT; (*Interne Variable*)
	END_STRUCT;
	BrbVc4PageHandling_TYP : 	STRUCT 
		nPageDefault : DINT; (*Eingang: Start-/Standard-Seite*)
		bChangePageDirect : BOOL; (*Eingang: Auf jeden Fall Seite wechseln*)
		nPageChange : DINT; (*Zur Anbindung an den Datenpunkt*)
		nPageCurrent : DINT; (*Zur Anbindung an den Datenpunkt*)
		nPageNext : DINT; (*Interne Variable*)
		PagesPreviousLifo : BrbMemListManagement_Typ; (*Interne Variable*)
		nPagesPrevious : ARRAY[0..nBRB_PAGE_LAST_INDEX_MAX]OF DINT; (*Interne Variable*)
		bPageInit : BOOL; (*Ausgang: Flanke bei Einsprung eine Seite*)
		bPageExit : BOOL; (*Ausgang: Flanke beim Verlassen einer Seite*)
		bPageChangeInProgress : BOOL; (*Ausgang: Seitenumschaltung im Gange*)
	END_STRUCT;
	BrbVc4Colors_ENUM : 
		(
		eBRB_COLOR_ENABLED := 59, (*59+256*0*)
		eBRB_COLOR_DISABLED := 63547 (*59+248*254*)
		);
	BrbVc4Animation_TYP : 	STRUCT 
		bEnable : BOOL; (*Eingang: 1=Enabled*)
		nIndexMin : UINT; (*Eingang: Kleinster Bitmap-Index der Animation*)
		nIndexMax : UINT; (*Eingang: Gr��ter Bitmap-Index der Animation*)
		nIndexStanding : UINT; (*Eingang: Bitmap-Index bei Stillstand*)
		nIndex : UINT; (*Zur Anbindung an den Datenpunkt*)
		nStatus : UINT; (*Zur Anbindung an den Datenpunkt*)
		fbTimer : TON; (*Interne Variable (Zeit PT muss eingestellt werden)*)
	END_STRUCT;
	BrbVc4Bargraph_TYP : 	STRUCT 
		nValue : DINT; (*Zur Anbindung an den Datenpunkt*)
		nColor : UINT; (*Zur Anbindung an den Datenpunkt*)
		nStatus : UINT; (*Zur Anbindung an den Datenpunkt*)
	END_STRUCT;
	BrbVc4Bitmap_TYP : 	STRUCT 
		nIndex : UINT; (*Zur Anbindung an den Datenpunkt*)
		nColor : UINT; (*Zur Anbindung an den Datenpunkt*)
		nFillColor1 : UINT; (*Zur Anbindung an den Datenpunkt*)
		nFillColor2 : UINT; (*Zur Anbindung an den Datenpunkt*)
		nStatus : UINT; (*Zur Anbindung an den Datenpunkt*)
	END_STRUCT;
	BrbVc4Button_TYP : 	STRUCT 
		nBmpIndex : UINT; (*Zur Anbindung an den Datenpunkt*)
		nTextIndex : UINT; (*Zur Anbindung an den Datenpunkt*)
		nColor : UINT; (*Zur Anbindung an den Datenpunkt*)
		bClicked : BOOL; (*Zur Anbindung an den Datenpunkt*)
		nStatus : UINT; (*Zur Anbindung an den Datenpunkt*)
	END_STRUCT;
	BrbVc4Checkbox_TYP : 	STRUCT 
		bClicked : BOOL; (*Zur Anbindung an den Datenpunkt*)
		nBmpIndex : UINT; (*Zur Anbindung an den Datenpunkt*)
		nTextColor : UINT; (*Zur Anbindung an den Datenpunkt*)
		nStatus : UINT; (*Zur Anbindung an den Datenpunkt*)
		bChecked : BOOL; (*Ausgang: 1=Angehakt*)
	END_STRUCT;
	BrbVc4DateTime_TYP : 	STRUCT 
		dtValue : DATE_AND_TIME; (*Zur Anbindung an den Datenpunkt*)
		nColor : UINT; (*Zur Anbindung an den Datenpunkt*)
		nStatus : UINT; (*Zur Anbindung an den Datenpunkt*)
	END_STRUCT;
	BrbVc4DropdownOptions_ENUM : 
		(
		eBRB_DD_OPTION_NORMAL := 0, (*0=Normal*)
		eBRB_DD_OPTION_DISABLED := 1, (*1=Ausgegraut*)
		eBRB_DD_OPTION_INVISIBLE := 2 (*2=Unsichtbar*)
		);
	BrbVc4Drawbox_TYP : 	STRUCT 
		nStatus : UINT; (*Zur Anbindung an den Datenpunkt*)
	END_STRUCT;
	BrbVc4Dropdown_TYP : 	STRUCT 
		nIndex : UINT; (*Zur Anbindung an den Datenpunkt*)
		nIndexMin : UINT; (*Zur Anbindung an den Datenpunkt*)
		nIndexMax : UINT; (*Zur Anbindung an den Datenpunkt*)
		nOptions : ARRAY[0..nBRB_DROPDOWN_OPTION_MAX]OF USINT; (*Zur Anbindung an den Datenpunkt*)
		bInputCompleted : BOOL; (*Zur Anbindung an den Datenpunkt*)
		nStatus : UINT; (*Zur Anbindung an den Datenpunkt*)
	END_STRUCT;
	BrbVc4EditCtrl_TYP : 	STRUCT 
		nStatus : UINT; (*Zur Anbindung an den Datenpunkt*)
		nBusy : UINT; (*Zur Anbindung an den Datenpunkt*)
		sUrl : STRING[nBRB_URL_CHAR_MAX]; (*Zur Anbindung an den Datenpunkt*)
		sCmdRequest : STRING[nBRB_EDIT_CTRL_CMD_CHAR_MAX]; (*Zur Anbindung an den Datenpunkt*)
		sCmdResponse : STRING[nBRB_EDIT_CTRL_CMD_CHAR_MAX]; (*Zur Anbindung an den Datenpunkt*)
		nCmdStatus : UINT; (*Zur Anbindung an den Datenpunkt*)
		nCompletion : UINT; (*Zur Anbindung an den Datenpunkt*)
		nCursorLine : UDINT; (*Zur Anbindung an den Datenpunkt*)
		nCursorColumn : UDINT; (*Zur Anbindung an den Datenpunkt*)
		nInsertMode : USINT; (*Zur Anbindung an den Datenpunkt*)
		nModified : USINT; (*Zur Anbindung an den Datenpunkt*)
		nSelectionMode : USINT; (*Zur Anbindung an den Datenpunkt*)
		nLineCount : UDINT; (*Interne Variable*)
		nColumsMax : UDINT; (*Interne Variable*)
		sLastCmdRequest : STRING[nBRB_EDIT_CTRL_CMD_CHAR_MAX]; (*Interne Variable*)
	END_STRUCT;
	BrbVc4Gauge_TYP : 	STRUCT 
		nValue : DINT; (*Zur Anbindung an den Datenpunkt*)
		nValueMin : DINT; (*Zur Anbindung an den Datenpunkt*)
		nValueMax : DINT; (*Zur Anbindung an den Datenpunkt*)
		nColor : UINT; (*Zur Anbindung an den Datenpunkt*)
		nStatus : UINT; (*Zur Anbindung an den Datenpunkt*)
	END_STRUCT;
	BrbVc4Hotspot_TYP : 	STRUCT 
		bClicked : BOOL; (*Zur Anbindung an den Datenpunkt*)
		nStatus : UINT; (*Zur Anbindung an den Datenpunkt*)
	END_STRUCT;
	BrbVc4Html_TYP : 	STRUCT 
		sCurrentUrl : STRING[nBRB_URL_CHAR_MAX]; (*Zur Anbindung an den Datenpunkt*)
		sChangeUrl : STRING[nBRB_URL_CHAR_MAX]; (*Zur Anbindung an den Datenpunkt*)
		sCurrentTitle : STRING[nBRB_URL_CHAR_MAX]; (*Zur Anbindung an den Datenpunkt*)
		bBusy : BOOL; (*Zur Anbindung an den Datenpunkt*)
		nHttpErrorCode : UDINT; (*Zur Anbindung an den Datenpunkt*)
		nStatus : UINT; (*Zur Anbindung an den Datenpunkt*)
		bBusyOld : BOOL; (*Interne Variable*)
	END_STRUCT;
	BrbVc4HwPosSwitchPositions_ENUM : 
		(
		eBRB_HPS_POS_0_EDGE, (*0=Gerade auf linke Stellung geschaltet*)
		eBRB_HPS_POS_0, (*1=Auf linke Stellung gestellt*)
		eBRB_HPS_POS_1_EDGE, (*2=Gerade auf mittlere Stellung geschaltet*)
		eBRB_HPS_POS_1, (*3=Auf mittlere Stellung gestellt*)
		eBRB_HPS_POS_2_EDGE, (*4=Gerade auf rechte Stellung geschaltet*)
		eBRB_HPS_POS_2, (*5=Auf rechte Stellung gestellt*)
		eBRB_HPS_POS_UNDEF (*6=Stellung aufgrund der Eing�nge nicht zu ermitteln*)
		);
	BrbVc4HwPosSwitch2_TYP : 	STRUCT 
		bPos0 : BOOL; (*Hw-Eingang: Schalter auf linker Stellung*)
		bPos2 : BOOL; (*Hw-Eingang: Schalter auf rechter Stellung*)
		nStatus : UINT; (*Zur Anbindung an den Datenpunkt*)
		nSwitchStatus0 : UINT; (*Zur Anbindung an den Datenpunkt*)
		nSwitchStatus1 : UINT; (*Zur Anbindung an den Datenpunkt*)
		nSwitchStatus2 : UINT; (*Zur Anbindung an den Datenpunkt*)
		ePosOld : BrbVc4HwPosSwitchPositions_ENUM; (*Interne Variable*)
		ePos : BrbVc4HwPosSwitchPositions_ENUM; (*Ausgang: Momentaner Status des Schalters*)
	END_STRUCT;
	BrbVc4HwSafteyButtonStates_ENUM : 
		(
		eBRB_HSB_STATE_UNPUSHED, (*0=Nicht gedr�ckt*)
		eBRB_HSB_STATE_UNPUSHED_EDGE, (*1=Gerade losgelassen*)
		eBRB_HSB_STATE_PUSHED_EDGE, (*2=Gerade gedr�ckt*)
		eBRB_HSB_STATE_PUSHED (*3=Gedr�ckt*)
		);
	BrbVc4HwSafetyButton_TYP : 	STRUCT 
		bNormallyOpened : BOOL; (*Hw-Eingang: Schliesser*)
		bNormallyClosed : BOOL; (*Hw-Eingang: �ffner*)
		nColor : UINT; (*Zur Anbindung an den Datenpunkt*)
		nStatus : UINT; (*Zur Anbindung an den Datenpunkt*)
		bPushedOld : BOOL; (*Interne Variable*)
		bPushed : BOOL; (*Ausgang: 0=Nicht gedr�ckt, 1=Gedr�ckt*)
		eState : BrbVc4HwSafteyButtonStates_ENUM; (*Ausgang: Momentaner Status des Tasters*)
	END_STRUCT;
	BrbVc4IncButtonStates_ENUM : 
		(
		eBRB_INC_BTN_STATE_UNPUSHED, (*0=Nicht gedr�ckt*)
		eBRB_INC_BTN_STATE_UNPUSHED_EDGE, (*1=Gerade losgelassen*)
		eBRB_INC_BTN_STATE_PUSHED_EDGE, (*2=Gerade gedr�ckt*)
		eBRB_INC_BTN_STATE_PUSHED_DELAY, (*3=Verz�gerunszeit l�uft*)
		eBRB_INC_BTN_STATE_PUSHED_REPEAT, (*4=Wiederholzeit l�uft*)
		eBRB_INC_BTN_STATE_PUSHED_INC (*5=Wiederholender Impuls*)
		);
	BrbVc4IncButton_TYP : 	STRUCT 
		bEnabled : BOOL; (*Eingang: 1=Enabled*)
		bSuppressDelay : BOOL; (*Eingang: 1=Keine Verz�gerungszeit*)
		bSuppressRepeat : BOOL; (*Eingang: 1=Keine Wiederholzeit*)
		nBmpIndex : UINT; (*Zur Anbindung an den Datenpunkt*)
		nTextIndex : UINT; (*Zur Anbindung an den Datenpunkt*)
		nColor : UINT; (*Zur Anbindung an den Datenpunkt*)
		bPushed : BOOL; (*Zur Anbindung an den Datenpunkt*)
		nStatus : UINT; (*Zur Anbindung an den Datenpunkt*)
		eIncState : BrbVc4IncButtonStates_ENUM; (*Ausgang: Momentaner Status des Buttons*)
		bPushedOld : BOOL; (*Interne Variable*)
		bEnabledOld : BOOL; (*Interne Variable*)
		fbDelay : TON; (*Interne Variable (Zeit PT muss eingestellt werden)*)
		fbRepeat : TON; (*Interne Variable (Zeit PT muss eingestellt werden)*)
	END_STRUCT;
	BrbVc4JogButtonStates_ENUM : 
		(
		eBRB_JOG_BTN_STATE_UNPUSHED, (*0=Nicht gedr�ckt*)
		eBRB_JOG_BTN_STATE_UNPUSHED_EDGE, (*1=Gerade losgelassen*)
		eBRB_JOG_BTN_STATE_PUSHED_EDGE, (*2=Gerade gedr�ckt*)
		eBRB_JOG_BTN_STATE_PUSHED (*3=Gedr�ckt*)
		);
	BrbVc4JogButton_TYP : 	STRUCT 
		bEnabled : BOOL; (*Eingang: 1=Enabled*)
		bSuppressTimeout : BOOL; (*Eingang: 1=Keine Verz�gerungszeit*)
		nBmpIndex : UINT; (*Zur Anbindung an den Datenpunkt*)
		nTextIndex : UINT; (*Zur Anbindung an den Datenpunkt*)
		nColor : UINT; (*Zur Anbindung an den Datenpunkt*)
		bPushed : BOOL; (*Zur Anbindung an den Datenpunkt*)
		nStatus : UINT; (*Zur Anbindung an den Datenpunkt*)
		eJogState : BrbVc4JogButtonStates_ENUM; (*Ausgang: Momentaner Status des Buttons*)
		bPushedOld : BOOL; (*Interne Variable*)
		bEnabledOld : BOOL; (*Interne Variable*)
		fbUnpush : TON; (*Interne Variable*)
	END_STRUCT;
	BrbVc4Layer_TYP : 	STRUCT 
		nStatus : UINT; (*Zur Anbindung an den Datenpunkt*)
	END_STRUCT;
	BrbVc4Listbox_TYP : 	STRUCT 
		nIndex : UINT; (*Zur Anbindung an den Datenpunkt*)
		nIndexMin : UINT; (*Zur Anbindung an den Datenpunkt*)
		nIndexMax : UINT; (*Zur Anbindung an den Datenpunkt*)
		nControlColor : UINT; (*Zur Anbindung an den Datenpunkt*)
		nDisabledColor : UINT; (*Zur Anbindung an den Datenpunkt*)
		nSelectedColor : UINT; (*Zur Anbindung an den Datenpunkt*)
		nSliderColor : UINT; (*Zur Anbindung an den Datenpunkt*)
		nOptions : ARRAY[0..nBRB_LISTBOX_OPTION_MAX]OF USINT; (*Zur Anbindung an den Datenpunkt*)
		bInputCompleted : BOOL; (*Zur Anbindung an den Datenpunkt*)
		nStatus : UINT; (*Zur Anbindung an den Datenpunkt*)
	END_STRUCT;
	BrbVc4Numeric_TYP : 	STRUCT 
		nValue : DINT; (*Zur Anbindung an den Datenpunkt*)
		nMin : DINT; (*Zur Anbindung an den Datenpunkt*)
		nMax : DINT; (*Zur Anbindung an den Datenpunkt*)
		rValue : REAL; (*Zur Anbindung an den Datenpunkt*)
		rMin : REAL; (*Zur Anbindung an den Datenpunkt*)
		rMax : REAL; (*Zur Anbindung an den Datenpunkt*)
		nColor : UINT; (*Zur Anbindung an den Datenpunkt*)
		bInputCompleted : BOOL; (*Zur Anbindung an den Datenpunkt*)
		nStatus : UINT; (*Zur Anbindung an den Datenpunkt*)
	END_STRUCT;
	BrbVc4Optionbox_TYP : 	STRUCT 
		bClicked : BOOL; (*Zur Anbindung an den Datenpunkt*)
		nBmpIndex : UINT; (*Zur Anbindung an den Datenpunkt*)
		nTextColor : UINT; (*Zur Anbindung an den Datenpunkt*)
		nStatus : UINT; (*Zur Anbindung an den Datenpunkt*)
		bChecked : BOOL; (*Ausgang: 1=Angehakt*)
	END_STRUCT;
	BrbVc4Password_TYP : 	STRUCT 
		sPasswords : ARRAY[0..nBRB_PASSWORD_INDEX_MAX]OF STRING[nBRB_PASSWORD_CHAR_MAX]; (*Passw�rter*)
		nLevel : UINT; (*Aktuelle Benutzer-Ebene*)
		nColor : UINT; (*Zur Anbindung an den Datenpunkt*)
		bInputCompleted : BOOL; (*Zur Anbindung an den Datenpunkt*)
		nStatus : UINT; (*Zur Anbindung an den Datenpunkt*)
	END_STRUCT;
	BrbVc4PieChart_TYP : 	STRUCT 
		nValue : ARRAY[0..nBRB_PIECHART_VALUE_INDEX_MAX]OF DINT; (*Zur Anbindung an den Datenpunkt*)
		nColor : UINT; (*Zur Anbindung an den Datenpunkt*)
		nStatus : UINT; (*Zur Anbindung an den Datenpunkt*)
	END_STRUCT;
	BrbVc4Scale_TYP : 	STRUCT 
		nValue : DINT; (*Zur Anbindung an den Datenpunkt*)
		nValueMin : DINT; (*Zur Anbindung an den Datenpunkt*)
		nValueMax : DINT; (*Zur Anbindung an den Datenpunkt*)
		bInputCompleted : BOOL; (*Zur Anbindung an den Datenpunkt*)
		nControlColor : UINT; (*Zur Anbindung an den Datenpunkt*)
		nRangeColor : UINT; (*Zur Anbindung an den Datenpunkt*)
		nStatus : UINT; (*Zur Anbindung an den Datenpunkt*)
	END_STRUCT;
	BrbVc4ScrollbarHor_TYP : 	STRUCT 
		bDisabled : BOOL; (*Eingang: 1=Disabled*)
		nTotalIndexMin : DINT; (*Eingang: Kleinster Index des Quelle*)
		nTotalIndexMax : DINT; (*Eingang: Gr��ter Index des Quelle*)
		nCountShow : UDINT; (*Eingang: Anzahl der angezeigen Zeilen*)
		nEntryCountTotal : UDINT; (*Kann zur Anzeige der totalen Eintr�ge verwendet werden*)
		nScrollTotal : UDINT; (*Interne Variable*)
		btnLeft : BrbVc4Button_TYP; (*Control*)
		btnPageLeft : BrbVc4IncButton_TYP; (*Control*)
		btnLineLeft : BrbVc4IncButton_TYP; (*Control*)
		btnLineRight : BrbVc4IncButton_TYP; (*Control*)
		btnPageRight : BrbVc4IncButton_TYP; (*Control*)
		btnRight : BrbVc4Button_TYP; (*Control*)
		bScrollDone : BOOL; (*Ausgang: 1=Es wurde gescrollt*)
	END_STRUCT;
	BrbVc4ScrollbarVer_TYP : 	STRUCT 
		bDisabled : BOOL; (*Eingang: 1=Disabled*)
		nTotalIndexMin : DINT; (*Eingang: Kleinster Index des Quelle*)
		nTotalIndexMax : DINT; (*Eingang: Gr��ter Index des Quelle*)
		nCountShow : UDINT; (*Eingang: Anzahl der angezeigen Zeilen*)
		nEntryCountTotal : UDINT; (*Kann zur Anzeige der totalen Eintr�ge verwendet werden*)
		nScrollTotal : UDINT; (* Interne Variable*)
		btnTop : BrbVc4Button_TYP; (*Control*)
		btnPageUp : BrbVc4IncButton_TYP; (*Control*)
		btnLineUp : BrbVc4IncButton_TYP; (*Control*)
		btnLineDown : BrbVc4IncButton_TYP; (*Control*)
		btnPageDown : BrbVc4IncButton_TYP; (*Control*)
		btnBottom : BrbVc4Button_TYP; (*Control*)
		bScrollDone : BOOL; (*Ausgang: 1=Es wurde gescrollt*)
	END_STRUCT;
	BrbVc4Scrollbar_TYP : 	STRUCT 
		Horizontal : BrbVc4ScrollbarHor_TYP; (*Horizontale Scrollbar*)
		Vertical : BrbVc4ScrollbarVer_TYP; (*Vertikale Scrollbar*)
	END_STRUCT;
	BrbVc4ScrollList_TYP : 	STRUCT 
		bGetList : BOOL; (*1=Liste wurde gescrollt und muss neu angezeigt werden*)
		nScrollOffset : DINT; (*Momentaner Offset des angezeigten Auschnitts der Quelle*)
		nSelectedIndex : DINT; (*Index der momentan selektierten Zeile*)
	END_STRUCT;
	BrbVc4Shape_TYP : 	STRUCT 
		nColor : UINT; (*Zur Anbindung an den Datenpunkt*)
		nStatus : UINT; (*Zur Anbindung an den Datenpunkt*)
	END_STRUCT;
	BrbVc4Slider_TYP : 	STRUCT 
		nValueShow : DINT; (*Zur Anbindung an den Datenpunkt*)
		nValueInput : DINT; (*Zur Anbindung an den Datenpunkt*)
		nValueMin : DINT; (*Zur Anbindung an den Datenpunkt*)
		nValueMax : DINT; (*Zur Anbindung an den Datenpunkt*)
		bInputCompleted : BOOL; (*Zur Anbindung an den Datenpunkt*)
		nColor : UINT; (*Zur Anbindung an den Datenpunkt*)
		nStatus : UINT; (*Zur Anbindung an den Datenpunkt*)
	END_STRUCT;
	BrbVc4String_TYP : 	STRUCT 
		sValue : STRING[nBRB_STRING_INPUT_CHAR_MAX]; (*Zur Anbindung an den Datenpunkt*)
		nColor : UINT; (*Zur Anbindung an den Datenpunkt*)
		bInputCompleted : BOOL; (*Zur Anbindung an den Datenpunkt*)
		nStatus : UINT; (*Zur Anbindung an den Datenpunkt*)
	END_STRUCT;
	BrbVc4TabPage_TYP : 	STRUCT 
		btnPage : BrbVc4Button_TYP; (*Control*)
		nStatus : UINT; (*Zur Anbindung an den Datenpunkt*)
	END_STRUCT;
	BrbVc4TabCtrl_TYP : 	STRUCT 
		nSelectedTabPageIndex : UINT; (*Ausgang: Selektierter Tab-Reiter*)
		TabPage : ARRAY[0..nBRB_TAB_PAGE_INDEX_MAX]OF BrbVc4TabPage_TYP; (*Tab-Reiter*)
	END_STRUCT;
	BrbVc4Text_TYP : 	STRUCT 
		nIndex : UINT; (*Zur Anbindung an den Datenpunkt*)
		nColor : UINT; (*Zur Anbindung an den Datenpunkt*)
		nStatus : UINT; (*Zur Anbindung an den Datenpunkt*)
	END_STRUCT;
	BrbVc4ToggleButtonStates_ENUM : 
		(
		eBRB_TOG_BTN_STATE_UNPUSHED, (*0=Nicht gedr�ckt*)
		eBRB_TOG_BTN_STATE_UNPUSHED_EDGE, (*1=Gerade losgelassen*)
		eBRB_TOG_BTN_STATE_PUSHED_EDGE, (*2=Gerade gedr�ckt*)
		eBRB_TOG_BTN_STATE_PUSHED (*3=Gedr�ckt*)
		);
	BrbVc4ToggleButton_TYP : 	STRUCT 
		nBmpIndex : UINT; (*Zur Anbindung an den Datenpunkt*)
		nTextIndex : UINT; (*Zur Anbindung an den Datenpunkt*)
		nColor : UINT; (*Zur Anbindung an den Datenpunkt*)
		bPushed : BOOL; (*Zur Anbindung an den Datenpunkt*)
		nStatus : UINT; (*Zur Anbindung an den Datenpunkt*)
		eToggleState : BrbVc4ToggleButtonStates_ENUM; (*Ausgang: Momentaner Status des Buttons*)
		bPushedOld : BOOL; (*Interne Variable*)
	END_STRUCT;
	BrbVc4Touchgrid_TYP : 	STRUCT 
		bClickEnabled : BOOL; (*Eingang: 1=Klicks werden erkannt*)
		bDrawGrid : BOOL; (*Eingang: 1=Gitter wird gezeichnet*)
		bDrawMarker : BOOL; (*Eingang: 1=Markierung wird gezeichnet*)
		nGridLeft : DINT; (*Eingang: Linke Koordinate*)
		nGridTop : DINT; (*Eingang: Obere Koordinate*)
		nCellWidth : UINT; (*Eingang: Breite einer Zelle*)
		nIndexMaxX : UINT; (*Eingang: Maximaler Index der Zellen in X-Richtung*)
		nCellHeight : UINT; (*Eingang: H�he einer Zelle*)
		nIndexMaxY : UINT; (*Eingang: Maximaler Index der Zellen in Y-Richtung*)
		nGridColor : UINT; (*Eingang: Farbe des Gitters*)
		eMarkerFigure : BrbVc4Figures_ENUM; (*Eingang: Figur der Markierung (z.B. Rechteck)*)
		MarkerLine : BrbVc4Line_TYP; (*Eingang: Markierung als Linie*)
		MarkerRectangle : BrbVc4Rectangle_TYP; (*Eingang: Markierung als Rechteck*)
		MarkerEllipse : BrbVc4Ellipse_TYP; (*Eingang: Markierung als Ellipse*)
		MarkerArc : BrbVc4Arc_TYP; (*Eingang: Markierung als Bogen*)
		MarkerText : BrbVc4DrawText_TYP; (*Eingang: Markierung als Text*)
		nSelectedIndexX : UINT; (*Ausgang: Selektierte Spalte*)
		nSelectedIndexY : UINT; (*Ausgang: Selektierte Zeile*)
		nSelectedIndex : UINT; (*Ein-/Ausgang: Selektierter Index*)
		nSelectedIndexMax : UDINT; (*Ausgang: Maximal Selektier-Index*)
		eState : BrbVc4TouchStates_ENUM; (*Ausgang: Momentaner Status des Tochs*)
	END_STRUCT;
END_TYPE

(*VisVc4Draw*)

TYPE
	BrbVc4Figures_ENUM : 
		(
		eBRB_FIGURE_LINE, (*0=Linie*)
		eBRB_FIGURE_RECTANGLE, (*1=Rechteck*)
		eBRB_FIGURE_ELLIPSE, (*2=Ellipse*)
		eBRB_FIGURE_ARC, (*3=Bogen*)
		eBRB_FIGURE_TEXT (*4=Text*)
		);
	BrbVc4Point_TYP : 	STRUCT 
		nX : DINT; (*X-Koordinate*)
		nY : DINT; (*Y-Koordinate*)
	END_STRUCT;
	BrbVc4Line_TYP : 	STRUCT 
		nLeft : DINT; (*Eingang: Linke Koordinate*)
		nTop : DINT; (*Eingang: Obere Koordinate*)
		nRight : DINT; (*Eingang: Rechte Koordinate*)
		nBottom : DINT; (*Eingang: Untere Koordinate*)
		nColor : UINT; (*Eingang: Farbe*)
		nDashWidth : UINT; (*Eingang: Breite f�r Strichelung (0=Solide)*)
	END_STRUCT;
	BrbVc4Rectangle_TYP : 	STRUCT 
		nLeft : DINT; (*Eingang: Linke Koordinate*)
		nTop : DINT; (*Eingang: Obere Koordinate*)
		nWidth : DINT; (*Eingang: Breite*)
		nHeight : DINT; (*Eingang: H�he*)
		nFillColor : UINT; (*Eingang: F�ll-Farbe*)
		nBorderColor : UINT; (*Eingang: Rand-Farbe*)
		nDashWidth : UINT; (*Eingang: Breite f�r Strichelung (0=Solide)*)
	END_STRUCT;
	BrbVc4Ellipse_TYP : 	STRUCT 
		nLeft : DINT; (*Eingang: Linke Koordinate*)
		nTop : DINT; (*Eingang: Obere Koordinate*)
		nWidth : DINT; (*Eingang: Breite*)
		nHeight : DINT; (*Eingang: H�he*)
		nFillColor : UINT; (*Eingang: F�ll-Farbe*)
		nBorderColor : UINT; (*Eingang: F�ll-Farbe*)
		nDashWidth : UINT; (*Eingang: Breite f�r Strichelung (0=Solide)*)
	END_STRUCT;
	BrbVc4Arc_TYP : 	STRUCT 
		nLeft : DINT; (*Eingang: Linke Koordinate*)
		nTop : DINT; (*Eingang: Obere Koordinate*)
		nWidth : DINT; (*Eingang: Breite*)
		nHeight : DINT; (*Eingang: H�he*)
		rStartAngle : REAL; (*Eingang: Start-Winkel (0..360�)*)
		rEndAngle : REAL; (*Eingang: End-Winkel (0..360�)*)
		nFillColor : UINT; (*Eingang: F�ll-Farbe (momentan nicht unterst�tzt)*)
		nBorderColor : UINT; (*Eingang: Rand-Farbe*)
		nDashWidth : UINT; (*Eingang: Breite f�r Strichelung (0=Solide)*)
	END_STRUCT;
	BrbVc4DrawText_TYP : 	STRUCT 
		nLeft : DINT; (*Eingang: Linke Koordinate*)
		nTop : DINT; (*Eingang: Obere Koordinate*)
		nFontIndex : UINT; (*Eingang: Index der Schrift*)
		nTextColor : UINT; (*Eingang: Text-Farbe*)
		nBackgroundColor : UINT; (*Eingang: Hintergrund-Farbe*)
		sText : STRING[nBRB_DRAW_TEXT_CHAR_MAX]; (*Eingang: Text*)
	END_STRUCT;
	BrbVc4Font_TYP : 	STRUCT 
		nIndex : UINT; (*Parameter: Index der Schrift*)
		nCharWidth : UINT; (*Parameter: Durchschnittliche Breite eines Zeichens in [Pixel]*)
		nCharHeight : UINT; (*Parameter: H�he eines Zeichens in [Pixel]*)
	END_STRUCT;
END_TYPE

(*VisVc4DrawExt*)

TYPE
	BrbVc4DrawPlotValue_TYP : 	STRUCT 
		rX : REAL; (*X-Wert des Plots*)
		rY : REAL; (*Y-Wert des Plots*)
	END_STRUCT;
	BrbVc4DrawPlotStatistics_TYP : 	STRUCT 
		rMinX : REAL; (*Ausgang: Kleinster Wert der X-Achse*)
		rMaxX : REAL; (*Ausgang: Gr��ter Wert der X-Achse*)
		rMinY : REAL; (*Ausgang: Kleinster Wert der Y-Achse*)
		rMaxY : REAL; (*Ausgang: Gr��ter Wert der Y-Achse*)
	END_STRUCT;
	BrbVc4DrawPlot_TYP : 	STRUCT 
		bShowDrawArea : BOOL; (*Eingang: 1=Zeichenbereich darstellen*)
		nDrawAreaLeft : UDINT; (*Eingang: Linke Koordinate des Zeichenbereichs in [Pixel]*)
		nDrawAreaTop : UDINT; (*Eingang: Obere Koordinate des Zeichenbereichs in [Pixel]*)
		nDrawAreaWidth : UDINT; (*Eingang: Breite des Zeichenbereichs in [Pixel]*)
		nDrawAreaHeight : UDINT; (*Eingang: H�he des Zeichenbereichs in [Pixel]*)
		nDrawAreaColor : UINT; (*Eingang: Farbe des Zeichenbereichs*)
		nDrawIndent : UDINT; (*Eingang: Einzug links in [Pixel]*)
		rValueMinY : REAL; (*Eingang: Kleinster Wert der Y-Achse*)
		rValueMaxY : REAL; (*Eingang: Gr��ter Wert der Y-Achse*)
		bShowScaleY : BOOL; (*Eingang: 1=Skalierung der Y-Achse darstellen*)
		nScaleCountY : UINT; (*Eingang: Anzahl der Skalierungs-Striche der Y-Achse*)
		rValueMinX : REAL; (*Eingang: Kleinster Wert der X-Achse*)
		rValueMaxX : REAL; (*Eingang: Gr��ter Wert der X-Achse*)
		bShowScaleX : BOOL; (*Eingang: 1=Skalierung der X-Achse darstellen*)
		nScaleCountX : UINT; (*Eingang: Anzahl der Skalierungs-Striche der X-Achse*)
		nScaleColor : UINT; (*Eingang: Farbe der Skalierung*)
		ScaleFont : BrbVc4Font_TYP; (*Eingang: Font der Skalierung*)
		bShowZeroLines : BOOL; (*Eingang: 1=Null-Linien darstellen*)
		nZeroLinesColor : UINT; (*Eingang: Farbe der Null-Linien*)
		pValues : REFERENCE TO BrbVc4DrawPlotValue_TYP; (*Eingang: Zeiger auf den ersten Eintrag im Werte-Array*)
		nValueIndexMax : UDINT; (*Eingang: Max. Index des Werte-Arrays*)
		nPlotColor : UINT; (*Eingang: Farbe des Plot*)
		bShowCursor : USINT; (*Eingang: 1=Cursor darstellen*)
		bSetCursorOnClick : BOOL; (*Eingang: 1=Cursor wird mit Klick gesetzt*)
		nCursorColor : UINT; (*Eingang: Farbe des Cursors*)
		rCursorX : REAL; (*Eingang: Aktueller X-Cursor-Wert*)
		rCursorY : REAL; (*Eingang: Aktueller X-CursorWert*)
		nCursorIndex : UDINT; (*Ausgang: Wert-Index des geklickten Cursors*)
		Statistics : BrbVc4DrawPlotStatistics_TYP; (*Statistische Werte*)
	END_STRUCT;
	BrbVc4DrawAxisCaptionOrder_ENUM : 
		(
		eBRB_DRAW_AXIS_NAME_POS_VEL, (*0=Name-Position-Geschwindigkeit*)
		eBRB_DRAW_AXIS_NAME_VEL_POS, (*1=Name-Geschwindigkeit-Position*)
		eBRB_DRAW_AXIS_POS_NAME_VEL, (*2=Position-Name-Geschwindigkeit*)
		eBRB_DRAW_AXIS_POS_VEL_NAME, (*3=Position-Geschwindigkeit-Name*)
		eBRB_DRAW_AXIS_VEL_NAME_POS, (*4=Geschwindigkeit-Name-Position*)
		eBRB_DRAW_AXIS_VEL_POS_NAME (*5=Geschwindigkeit-Position-Name*)
		);
	BrbVc4DrawAxisLinear_TYP : 	STRUCT 
		bVertical : BOOL; (*Eingang: 0=Horizontal, 1=Vertikal*)
		bShowDrawArea : BOOL; (*Eingang: 1=Zeichenbereich darstellen*)
		nDrawAreaLeft : UDINT; (*Eingang: Linke Koordinate des Zeichenbereichs in [Pixel]*)
		nDrawAreaTop : UDINT; (*Eingang: Obere Koordinate des Zeichenbereichs in [Pixel]*)
		nDrawAreaWidth : UDINT; (*Eingang: Breite des Zeichenbereichs in [Pixel]*)
		nDrawAreaHeight : UDINT; (*Eingang: H�he des Zeichenbereichs in [Pixel]*)
		nDrawAreaColor : UINT; (*Eingang: Farbe des Zeichenbereichs*)
		nDrawIndent : UDINT; (*Eingang: Einzug links + rechts bzw. oben + unten in [Pixel]*)
		nAxisLimitMin : DINT; (*Eingang: Kleinste Achsposition in [Achseinheiten]*)
		nAxisLimitMax : DINT; (*Eingang: Gr��te Achsposition in [Achseinheiten]*)
		bShowAxisScale : BOOL; (*Eingang: 1=Skalierung darstellen*)
		nAxisScaleCount : UINT; (*Eingang: Anzahl der Skalierungs-Striche*)
		nAxisScaleColor : UINT; (*Eingang: Farbe der Skalierung*)
		AxisScaleFont : BrbVc4Font_TYP; (*Eingang: Font der Skalierung*)
		bHighlightActPosition : BOOL; (*Eingang: 1=Skalierung hervorheben, wenn Achse auf dieser Position*)
		nAxisScaleHighlightColor : UINT; (*Eingang: Farbe der Hervorhebung*)
		bInverted : BOOL; (*Eingang: 1=Achs-Richtung umdrehen*)
		bClip : BOOL; (*Eingang: 1=Ausschnitt anzeigen*)
		nAxisClipRange : UDINT; (*Eingang: Gr��e des Ausschnitts in [Achseinheiten]*)
		bShowAxisPosLine : BOOL; (*Eingang: Achs-Positions-Strich darstellen*)
		bShowAxisBorder : BOOL; (*Eingang: Achs-Beschriftungs-Umrandung darstellen*)
		nAxisColor : UINT; (*Eingang: Farbe des Positions-Strichs und der Umrandung*)
		eAxisCaptionOrder : BrbVc4DrawAxisCaptionOrder_ENUM; (*Eingang: Reihenfolge der Achs-Beschriftung*)
		bShowAxisName : BOOL; (*Eingang: 1=Achsname darstellen*)
		sAxisName : STRING[nBRB_DRAW_TEXT_CHAR_MAX]; (*Eingang: Achsname*)
		nAxisNameColor : UINT; (*Eingang: Farbe des Achsnamens*)
		AxisNameFont : BrbVc4Font_TYP; (*Eingang: Font des Achsnamens*)
		bShowAxisActPosition : BOOL; (*Eingang: 1=Aktuelle Achs-Position darstellen*)
		rAxisActPosition : REAL; (*Eingang: Aktuelle Achs-Position*)
		nAxisActPositionColor : UINT; (*Eingang: Farbe der Achs-Position*)
		bShowAxisActVelocity : BOOL; (*Eingang: 1=Aktuelle Achs-Geschwindigkeit darstellen*)
		rAxisActVelocity : REAL; (*Eingang: Aktuelle Achs-Geschwindigkeit*)
		nAxisActVelocityColor : UINT; (*Eingang: Farbe der Achs-Geschwindigkeit*)
		AxisValueFont : BrbVc4Font_TYP; (*Eingang: Font der Achs-Position und Geschwindigkeit*)
		bShowAxisSetPosition : BOOL; (*Eingang: 1=Soll-Position darstellen*)
		rAxisSetPosition : REAL; (*Eingang: Soll-Achs-Position*)
		nAxisSetPositionColor : UINT; (*Eingang: Farbe der Soll-Achs-Position*)
	END_STRUCT;
	BrbVc4DrawAxisRadial_TYP : 	STRUCT 
		bShowDrawArea : BOOL; (*Eingang: 1=Zeichenbereich darstellen*)
		nDrawAreaLeft : UDINT; (*Eingang: Linke Koordinate des Zeichenbereichs in [Pixel]*)
		nDrawAreaTop : UDINT; (*Eingang: Obere Koordinate des Zeichenbereichs in [Pixel]*)
		nDrawAreaWidth : UDINT; (*Eingang: Breite des Zeichenbereichs in [Pixel]*)
		nDrawAreaHeight : UDINT; (*Eingang: H�he des Zeichenbereichs in [Pixel]*)
		nDrawAreaColor : UINT; (*Eingang: Farbe des Zeichenbereichs*)
		nRadius : UDINT; (*Eingang: Radius der Skalierung in [Pixel]*)
		nAxisLimitMin : DINT; (*Eingang: Kleinste Achsposition in [Achseinheiten]*)
		nAxisLimitMax : DINT; (*Eingang: Gr��te Achsposition in [Achseinheiten]*)
		bShowAxisScale : BOOL; (*Eingang: 1=Skalierung darstellen*)
		nAxisScaleCount : UINT; (*Eingang: Anzahl der Skalierungs-Striche*)
		nAxisScaleColor : UINT; (*Eingang: Farbe der Skalierung*)
		AxisScaleFont : BrbVc4Font_TYP; (*Eingang: Font der Skalierung*)
		bHighlightActPosition : BOOL; (*Eingang: 1=Skalierung hervorheben, wenn Achse auf dieser Position*)
		nAxisScaleHighlightColor : UINT; (*Eingang: Farbe der Hervorhebung*)
		bInverted : BOOL; (*Eingang: 1=Achs-Richtung umdrehen*)
		nOffset : UINT; (*Eingang: 1=Versatz der Achs-Position von 0 bis 360 in[�]*)
		bClip : BOOL; (*Eingang: 1=Ausschnitt anzeigen*)
		nAxisClipRange : UDINT; (*Eingang: Gr��e des Ausschnitts in [Achseinheiten]*)
		bShowAxisPosLine : BOOL; (*Eingang: Achs-Positions-Strich darstellen*)
		nAxisColor : UINT; (*Eingang: Farbe des Positions-Strichs und der Umrandung*)
		eAxisCaptionOrder : BrbVc4DrawAxisCaptionOrder_ENUM; (*Eingang: Reihenfolge der Achs-Beschriftung*)
		bShowAxisName : BOOL; (*Eingang: 1=Achsname darstellen*)
		sAxisName : STRING[nBRB_DRAW_TEXT_CHAR_MAX]; (*Eingang: Achsname*)
		nAxisNameColor : UINT; (*Eingang: Farbe des Achsnamens*)
		AxisNameFont : BrbVc4Font_TYP; (*Eingang: Font des Achsnamens*)
		bShowAxisActPosition : BOOL; (*Eingang: 1=Aktuelle Achs-Position darstellen*)
		rAxisActPosition : REAL; (*Eingang: Aktuelle Achs-Position*)
		nAxisActPositionColor : UINT; (*Eingang: Farbe der Achs-Position*)
		bShowAxisActVelocity : BOOL; (*Eingang: 1=Aktuelle Achs-Geschwindigkeit darstellen*)
		rAxisActVelocity : REAL; (*Eingang: Aktuelle Achs-Geschwindigkeit*)
		nAxisActVelocityColor : UINT; (*Eingang: Farbe der Achs-Geschwindigkeit*)
		AxisValueFont : BrbVc4Font_TYP; (*Eingang: Font der Achs-Position und Geschwindigkeit*)
		bShowAxisSetPosition : BOOL; (*Eingang: 1=Soll-Position darstellen*)
		rAxisSetPosition : REAL; (*Eingang: Soll-Achs-Position*)
		nAxisSetPositionColor : UINT; (*Eingang: Farbe der Soll-Achs-Position*)
	END_STRUCT;
END_TYPE

(*TimeAndDate*)

TYPE
	BrbTimeAndDateCompare_ENUM : 
		(
		eBRB_TAD_COMPARE_YOUNGER,
		eBRB_TAD_COMPARE_YOUNGEREQUAL,
		eBRB_TAD_COMPARE_EQUAL,
		eBRB_TAD_COMPARE_OLDEREQUAL,
		eBRB_TAD_COMPARE_OLDER
		);
END_TYPE

(*Memory*)

TYPE
	BrbMemListManagement_Typ : 	STRUCT 
		pList : UDINT; (*Adresse des Array-Anfangs*)
		nEntryLength : UDINT; (*Gr��e eines Eintrags*)
		nIndexMax : UINT; (*Maximaler Index des Arrays*)
		nEntryCount : UINT; (*Momentane Anzahl der g�ltigen Eintrage*)
	END_STRUCT;
END_TYPE

(*Additional*)

TYPE
	BrbError_ENUM : 
		(
		eBRB_ERR_OK := 0,
		eBRB_ERR_NULL_POINTER := 50000,
		eBRB_ERR_INVALID_PARAMETER := 50001,
		eBRB_ERR_WRONG_VERSION := 50700,
		eBRB_ERR_TOO_LESS_FIGURES := 50701,
		eBRB_ERR_TOO_LESS_TRIANGLES := 50702,
		eBRB_ERR_TOO_LESS_TEXTS := 50703,
		eBRB_ERR_BUSY := 65535
		);
END_TYPE
