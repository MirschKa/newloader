/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: BrbLib
 * File: BrbVc4HandleDropdown.c
 * Author: niedermeierr
 * Created: June 12, 2013
 ********************************************************************
 * Implementation of library BrbLib
 ********************************************************************/

#include <bur/plctypes.h>
#ifdef __cplusplus
	extern "C"
	{
#endif

#include "BrbLib.h"

#ifdef __cplusplus
	};
#endif

/* Logik f�r ein Dropdown */
plcbit BrbVc4HandleDropdown(struct BrbVc4Dropdown_TYP* pDropdown)
{
	BOOL	bInputCompleted = 0;
	if(BrbVc4IsControlEnabled(pDropdown->nStatus) == 0)
	{
		pDropdown->bInputCompleted = 0;
	}
	else
	{
		if(pDropdown->bInputCompleted == 1)
		{
			bInputCompleted = 1;
			pDropdown->bInputCompleted = 0;
		}
	}
	return bInputCompleted;
}
