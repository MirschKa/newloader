/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: BrbLib
 * File: BrbVc4HandleIncButton.c
 * Author: niedermeierr
 * Created: June 12, 2013
 ********************************************************************
 * Implementation of library BrbLib
 ********************************************************************/

#include <bur/plctypes.h>
#ifdef __cplusplus
	extern "C"
	{
#endif

#include "BrbLib.h"

#ifdef __cplusplus
	};
#endif

/* Logik für einen Inkrement-Button */
plcbit BrbVc4HandleIncButton(struct BrbVc4IncButton_TYP* pButton)
{
	if(pButton->fbDelay.PT == 0)
	{
		pButton->fbDelay.PT = 300;
	}
	TON(&pButton->fbDelay);
	if(pButton->fbRepeat.PT == 0)
	{
		pButton->fbRepeat.PT = 200;
	}
	TON(&pButton->fbRepeat);
	pButton->bEnabled = BrbVc4IsControlEnabled(pButton->nStatus);
	if(pButton->bEnabled == 0 && pButton->bEnabledOld == 1)
	{
		// Button wurde gerade disabled
		pButton->bPushed = 0;
		pButton->bPushedOld = 0;
		pButton->eIncState = eBRB_INC_BTN_STATE_UNPUSHED;
		pButton->fbDelay.IN = 0;
		pButton->fbRepeat.IN = 0;
		pButton->nBmpIndex = 0;
	}
	else if(pButton->bEnabled == 0)
	{
		// Button ist disabled
		pButton->bPushed = 0;
		pButton->bPushedOld = 0;
		pButton->eIncState = eBRB_INC_BTN_STATE_UNPUSHED;
		pButton->fbDelay.IN = 0;
		pButton->fbRepeat.IN = 0;
		pButton->nBmpIndex = 0;
	}
	else
	{
		// Button ist enabled
		pButton->nBmpIndex = 1;
		if(pButton->bPushed == 0 && pButton->bPushedOld == 0)
		{
			pButton->eIncState = eBRB_INC_BTN_STATE_UNPUSHED;
			pButton->fbDelay.IN = 0;
			pButton->fbRepeat.IN = 0;
		}
		else if(pButton->bPushed == 0 && pButton->bPushedOld == 1)
		{
			pButton->eIncState = eBRB_INC_BTN_STATE_UNPUSHED_EDGE;
			pButton->fbDelay.IN = 0;
			pButton->fbRepeat.IN = 0;
		}
		else if(pButton->bPushed == 1 && pButton->bPushedOld == 1)
		{
			if(pButton->fbDelay.Q == 1 || pButton->bSuppressDelay == 1)
			{
				// Verzögerungszeit abgelaufen
				pButton->fbRepeat.IN = 1;
				if(pButton->fbRepeat.Q == 1 || pButton->bSuppressRepeat == 1)
				{
					// Wiederholzeit abgelaufen
					pButton->fbRepeat.IN = 0;
					pButton->eIncState = eBRB_INC_BTN_STATE_PUSHED_INC;
				}
				else
				{
					pButton->eIncState = eBRB_INC_BTN_STATE_PUSHED_REPEAT;
				}
			}
			else
			{
				// Verzögerungszeit läuft
				pButton->eIncState = eBRB_INC_BTN_STATE_PUSHED_DELAY;
			}
		}
		else if(pButton->bPushed == 1 && pButton->bPushedOld == 0)
		{
			pButton->eIncState = eBRB_INC_BTN_STATE_PUSHED_EDGE;
			pButton->fbDelay.IN = 1;
		}
		pButton->bPushedOld = pButton->bPushed;
	}
	pButton->bEnabledOld = pButton->bEnabled;
	return pButton->bPushed;
}
