/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: BrbLib
 * File: BrbVc4HandleScrollbarHorizontal.c
 * Author: niedermeierr
 * Created: June 12, 2013
 ********************************************************************
 * Implementation of library BrbLib
 ********************************************************************/

#include <bur/plctypes.h>
#ifdef __cplusplus
	extern "C"
	{
#endif

#include "BrbLib.h"

#ifdef __cplusplus
	};
#endif

/* Logik f�r eine horizontale Scrollbar */
plcbit BrbVc4HandleScrollbarHorizontal(struct BrbVc4ScrollbarHor_TYP* pScrollbar, signed long* pScrollOffset)
{
	pScrollbar->nScrollTotal = pScrollbar->nTotalIndexMax - pScrollbar->nTotalIndexMin + 1;
	UDINT nScrollOffsetMax = 0;
	BOOL bScrollDone = 0;

	// Maximalen Scrolloffset berechnen
	if(pScrollbar->nScrollTotal <= pScrollbar->nCountShow)
	{
		nScrollOffsetMax = pScrollbar->nTotalIndexMin;
	}
	else
	{
		nScrollOffsetMax = pScrollbar->nTotalIndexMax - pScrollbar->nCountShow + 1;
	}

	if(pScrollbar->bDisabled == 0)
	{
		BOOL bIsOnTop;
		BOOL bIsOnBottom;
		BOOL bNoScroll = (pScrollbar->nScrollTotal <= pScrollbar->nCountShow);
		if(bNoScroll == 1)
		{
			bIsOnTop = 1;
			bIsOnBottom = 1;
		}
		else
		{
			bIsOnTop = (*pScrollOffset <= pScrollbar->nTotalIndexMin);
			bIsOnBottom = (*pScrollOffset >= nScrollOffsetMax);
		}
		BrbVc4SetControlEnability(&pScrollbar->btnLeft.nStatus, !bIsOnTop);
		BrbVc4SetControlEnability(&pScrollbar->btnPageLeft.nStatus, !bIsOnTop);
		BrbVc4SetControlEnability(&pScrollbar->btnLineLeft.nStatus, !bIsOnTop);
		BrbVc4SetControlEnability(&pScrollbar->btnLineRight.nStatus, !bIsOnBottom);
		BrbVc4SetControlEnability(&pScrollbar->btnPageRight.nStatus, !bIsOnBottom);
		BrbVc4SetControlEnability(&pScrollbar->btnRight.nStatus, !bIsOnBottom);
		if(BrbVc4HandleButton(&pScrollbar->btnLeft) == 1)
		{
			pScrollbar->btnLeft.bClicked = 0;
			*pScrollOffset = pScrollbar->nTotalIndexMin;
			bScrollDone = 1;
		}
		BrbVc4HandleIncButton(&pScrollbar->btnPageLeft);
		if(pScrollbar->btnPageLeft.eIncState == eBRB_INC_BTN_STATE_PUSHED_EDGE || pScrollbar->btnPageLeft.eIncState == eBRB_INC_BTN_STATE_PUSHED_INC)
		{
			*pScrollOffset -= pScrollbar->nCountShow;
			bScrollDone = 1;
		}
		BrbVc4HandleIncButton(&pScrollbar->btnLineLeft);
		if(pScrollbar->btnLineLeft.eIncState == eBRB_INC_BTN_STATE_PUSHED_EDGE || pScrollbar->btnLineLeft.eIncState == eBRB_INC_BTN_STATE_PUSHED_INC)
		{
			*pScrollOffset -= 1;
			bScrollDone = 1;
		}
		BrbVc4HandleIncButton(&pScrollbar->btnLineRight);
		if(pScrollbar->btnLineRight.eIncState == eBRB_INC_BTN_STATE_PUSHED_EDGE || pScrollbar->btnLineRight.eIncState == eBRB_INC_BTN_STATE_PUSHED_INC)
		{
			*pScrollOffset += 1;
			bScrollDone = 1;
		}
		BrbVc4HandleIncButton(&pScrollbar->btnPageRight);
		if(pScrollbar->btnPageRight.eIncState == eBRB_INC_BTN_STATE_PUSHED_EDGE || pScrollbar->btnPageRight.eIncState == eBRB_INC_BTN_STATE_PUSHED_INC)
		{
			*pScrollOffset += pScrollbar->nCountShow;
			bScrollDone = 1;
		}
		if(BrbVc4HandleButton(&pScrollbar->btnRight) == 1)
		{
			pScrollbar->btnRight.bClicked = 0;
			*pScrollOffset = nScrollOffsetMax;
			bScrollDone = 1;
		}
	}
	else
	{
		BrbVc4SetControlEnability(&pScrollbar->btnLeft.nStatus, 0);
		BrbVc4SetControlEnability(&pScrollbar->btnPageLeft.nStatus, 0);
		BrbVc4SetControlEnability(&pScrollbar->btnLineLeft.nStatus, 0);
		BrbVc4SetControlEnability(&pScrollbar->btnLineRight.nStatus, 0);
		BrbVc4SetControlEnability(&pScrollbar->btnPageRight.nStatus, 0);
		BrbVc4SetControlEnability(&pScrollbar->btnRight.nStatus, 0);
	}
	pScrollbar->btnLeft.nBmpIndex = BrbVc4IsControlEnabled(pScrollbar->btnLeft.nStatus);
	pScrollbar->btnPageLeft.nBmpIndex = BrbVc4IsControlEnabled(pScrollbar->btnPageLeft.nStatus);
	pScrollbar->btnLineLeft.nBmpIndex = BrbVc4IsControlEnabled(pScrollbar->btnLineLeft.nStatus);
	pScrollbar->btnLineRight.nBmpIndex = BrbVc4IsControlEnabled(pScrollbar->btnLineRight.nStatus);
	pScrollbar->btnPageRight.nBmpIndex = BrbVc4IsControlEnabled(pScrollbar->btnPageRight.nStatus);
	pScrollbar->btnRight.nBmpIndex = BrbVc4IsControlEnabled(pScrollbar->btnRight.nStatus);
	// Scrolloffset begrenzen
	if(*pScrollOffset < pScrollbar->nTotalIndexMin)
	{
		*pScrollOffset = pScrollbar->nTotalIndexMin;
		bScrollDone = 1;
	}
	if(*pScrollOffset > nScrollOffsetMax)
	{
		*pScrollOffset = nScrollOffsetMax;
		bScrollDone = 1;
	}
	// Durch die Defaultbesetzung am Schluss kann sie vor dem Aufruf auch ge�ndert werden
	pScrollbar->btnPageLeft.fbDelay.PT = 300;
	pScrollbar->btnPageLeft.fbRepeat.PT = 200;
	pScrollbar->btnLineLeft.fbDelay.PT = 300;
	pScrollbar->btnLineLeft.fbRepeat.PT = 200;
	pScrollbar->btnLineRight.fbDelay.PT = 300;
	pScrollbar->btnLineRight.fbRepeat.PT = 200;
	pScrollbar->btnPageRight.fbDelay.PT = 300;
	pScrollbar->btnPageRight.fbRepeat.PT = 200;
	pScrollbar->bScrollDone = bScrollDone;
	return bScrollDone;
}
