/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: BrbLib
 * File: BrbVc4HandleToggleButton.c
 * Author: niedermeierr
 * Created: June 12, 2013
 ********************************************************************
 * Implementation of library BrbLib
 ********************************************************************/

#include <bur/plctypes.h>
#ifdef __cplusplus
	extern "C"
	{
#endif

#include "BrbLib.h"

#ifdef __cplusplus
	};
#endif

/* Logik f�r einen Toggle-Button */
plcbit BrbVc4HandleToggleButton(struct BrbVc4ToggleButton_TYP* pButton)
{
	BOOL bPushedChanged = 0;
	if(BrbVc4IsControlEnabled(pButton->nStatus) == 0)
	{
		pButton->bPushed = 0;
		pButton->bPushedOld = 0;
		pButton->nBmpIndex = 0;
	}
	else
	{
		pButton->nBmpIndex = 1;
		if(pButton->bPushed == 0 && pButton->bPushedOld == 0)
		{
			pButton->eToggleState = eBRB_TOG_BTN_STATE_UNPUSHED;
		}
		else if(pButton->bPushed == 0 && pButton->bPushedOld == 1)
		{
			pButton->eToggleState = eBRB_TOG_BTN_STATE_UNPUSHED_EDGE;
			bPushedChanged = 1;
		}
		else if(pButton->bPushed == 1 && pButton->bPushedOld == 1)
		{
			pButton->eToggleState = eBRB_TOG_BTN_STATE_PUSHED;
		}
		else if(pButton->bPushed == 1 && pButton->bPushedOld == 0)
		{
			pButton->eToggleState = eBRB_TOG_BTN_STATE_PUSHED_EDGE;
			bPushedChanged = 1;
		}
	}
	pButton->bPushedOld = pButton->bPushed;
	return bPushedChanged;
}
