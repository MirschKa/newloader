/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: BrbLib
 * File: BrbVc4SetControlFocus.c
 * Author: niedermeierr
 * Created: June 11, 2013
 ********************************************************************
 * Implementation of library BrbLib
 ********************************************************************/

#include <bur/plctypes.h>
#ifdef __cplusplus
	extern "C"
	{
#endif

#include "BrbLib.h"

#ifdef __cplusplus
	};
#endif

/* Setzt oder l�scht den Focus auf ein Vis-Control */
plcbit BrbVc4SetControlFocus(unsigned short* pStatus, plcbit bFocus)
{
	if(bFocus == 0)
	{
		*pStatus = *pStatus | 4;
		return 0;
	}
	else
	{
		*pStatus = *pStatus & 0xFFFB;
		return 1;
	}
}
