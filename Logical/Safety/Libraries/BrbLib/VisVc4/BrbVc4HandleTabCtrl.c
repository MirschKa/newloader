/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: BrbLib
 * File: BrbVc4HandleTabCtrl.c
 * Author: niedermeierr
 * Created: June 12, 2013
 ********************************************************************
 * Implementation of library BrbLib
 ********************************************************************/

#include <bur/plctypes.h>
#ifdef __cplusplus
	extern "C"
	{
#endif

#include "BrbLib.h"

#ifdef __cplusplus
	};
#endif

/* Logik f�r eine TabCtrl */
plcbit BrbVc4HandleTabCtrl(struct BrbVc4TabCtrl_TYP* pTabCtrl)
{
	BOOL bResult = 0;
	UDINT nIndex = 0;
	for(nIndex=0; nIndex<=nBRB_TAB_PAGE_INDEX_MAX; nIndex++)
	{
		if(BrbVc4HandleButton(&pTabCtrl->TabPage[nIndex].btnPage) == 1)
		{
			pTabCtrl->TabPage[nIndex].btnPage.bClicked = 0;
			pTabCtrl->nSelectedTabPageIndex = nIndex;
			bResult = 1;
		}
		if(nIndex == pTabCtrl->nSelectedTabPageIndex)
		{
			BrbVc4SetControlVisiblity(&pTabCtrl->TabPage[nIndex].nStatus, 1);
		}
		else
		{
			BrbVc4SetControlVisiblity(&pTabCtrl->TabPage[nIndex].nStatus, 0);
		}
	}
	return bResult;
}
