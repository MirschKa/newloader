/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: BrbLib
 * File: BrbVc4HandleHwSafetyButton.c
 * Author: niedermeierr
 * Created: June 12, 2013
 ********************************************************************
 * Implementation of library BrbLib
 ********************************************************************/

#include <bur/plctypes.h>
#ifdef __cplusplus
	extern "C"
	{
#endif

#include "BrbLib.h"

#ifdef __cplusplus
	};
#endif

/* Logik f�r einen Hardware-Freigabe-Taster */
plcbit BrbVc4HandleHwSafetyButton(struct BrbVc4HwSafetyButton_TYP* pSafetyButton)
{
	if(pSafetyButton->bNormallyOpened == 0 && pSafetyButton->bNormallyClosed == 1)
	{
		pSafetyButton->bPushed = 1;
	}
	else
	{
		pSafetyButton->bPushed = 0;
	}
	if(pSafetyButton->bPushed == 0 && pSafetyButton->bPushedOld == 0)
	{
		pSafetyButton->eState = eBRB_HSB_STATE_UNPUSHED;
	}
	else if(pSafetyButton->bPushed == 0 && pSafetyButton->bPushedOld == 1)
	{
		pSafetyButton->eState = eBRB_HSB_STATE_UNPUSHED_EDGE;
	}
	else if(pSafetyButton->bPushed == 1 && pSafetyButton->bPushedOld == 1)
	{
		pSafetyButton->eState = eBRB_HSB_STATE_PUSHED;
	}
	else if(pSafetyButton->bPushed == 1 && pSafetyButton->bPushedOld == 0)
	{
		pSafetyButton->eState = eBRB_HSB_STATE_PUSHED_EDGE;
	}
	pSafetyButton->bPushedOld = pSafetyButton->bPushed;
	return pSafetyButton->bPushed;
}
