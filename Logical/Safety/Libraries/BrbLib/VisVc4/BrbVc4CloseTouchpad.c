/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: BrbLib
 * File: BrbVc4CloseTouchpad.c
 * Author: niedermeierr
 * Created: June 11, 2013
 ********************************************************************
 * Implementation of library BrbLib
 ********************************************************************/

#include <bur/plctypes.h>
#ifdef __cplusplus
	extern "C"
	{
#endif

#include "BrbLib.h"

#ifdef __cplusplus
	};
#endif

/* Schliesst das Touchpad an einem Eingabe-Control */
plcbit BrbVc4CloseTouchpad(unsigned short* pStatus)
{
	*pStatus = *pStatus & 0xFFF7;
	return 0;
}
