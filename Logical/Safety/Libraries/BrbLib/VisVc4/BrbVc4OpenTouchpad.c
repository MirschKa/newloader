/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: BrbLib
 * File: BrbVc4OpenTouchpad.c
 * Author: niedermeierr
 * Created: June 11, 2013
 ********************************************************************
 * Implementation of library BrbLib
 ********************************************************************/

#include <bur/plctypes.h>
#ifdef __cplusplus
	extern "C"
	{
#endif

#include "BrbLib.h"

#ifdef __cplusplus
	};
#endif

/* �ffnet das Touchpad an einem Eingabe-Control */
plcbit BrbVc4OpenTouchpad(unsigned short* pStatus)
{
	*pStatus = *pStatus | 8;
	return 0;	
}
