/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: BrbLib
 * File: BrbVc4ChangePageBack.c
 * Author: niedermeierr
 * Created: June 11, 2013
 ********************************************************************
 * Implementation of library BrbLib
 ********************************************************************/

#include <bur/plctypes.h>
#ifdef __cplusplus
	extern "C"
	{
#endif

#include "BrbLib.h"

#ifdef __cplusplus
	};
#endif

/* ChangePageBack */
plcbit BrbVc4ChangePageBack(struct BrbVc4PageHandling_TYP* pPageHandling)
{
	BOOL bResult = 0;
	// Vorherige Seite aus Lifo holen
	pPageHandling->PagesPreviousLifo.pList = (UDINT)&pPageHandling->nPagesPrevious;
	pPageHandling->PagesPreviousLifo.nEntryLength = sizeof(pPageHandling->nPagesPrevious[0]);
	pPageHandling->PagesPreviousLifo.nIndexMax = nBRB_PAGE_LAST_INDEX_MAX;
	DINT nPagePrevious = -1;
	BrbLifoOut(&pPageHandling->PagesPreviousLifo, (UDINT)&nPagePrevious);
	if(nPagePrevious > -1)
	{
		// Auf vorherige Seite umschalten
		pPageHandling->nPageChange = nPagePrevious;
		pPageHandling->nPageNext = nPagePrevious;
		pPageHandling->bPageChangeInProgress = 1;
		pPageHandling->bPageInit = 1;
		pPageHandling->bPageExit = 1;
		if(nPagePrevious == pPageHandling->nPageDefault)
		{
			// Startseite -> Lifo l�schen
			BrbMemListClear(&pPageHandling->PagesPreviousLifo);
		}
		bResult = 1;
	}
	else
	{
		// Auf Default-Seite umschalten
		pPageHandling->nPageChange = pPageHandling->nPageDefault;
		pPageHandling->nPageNext = pPageHandling->nPageDefault;
		pPageHandling->bPageChangeInProgress = 1;
		pPageHandling->bPageInit = 1;
		pPageHandling->bPageExit = 1;
		BrbMemListClear(&pPageHandling->PagesPreviousLifo);
		bResult = 0;
	}
	pPageHandling->bChangePageDirect = 0;
	return bResult;
}
