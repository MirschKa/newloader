/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: BrbLib
 * File: BrbVc4IsTouchpadOpen.c
 * Author: niedermeierr
 * Created: June 11, 2013
 ********************************************************************
 * Implementation of library BrbLib
 ********************************************************************/

#include <bur/plctypes.h>
#ifdef __cplusplus
	extern "C"
	{
#endif

#include "BrbLib.h"

#ifdef __cplusplus
	};
#endif

/* Gibt zur�ck, ob das Touchpad an einem Control ge�ffnet ist */
plcbit BrbVc4IsTouchpadOpen(unsigned short nStatus)
{
	return ((nStatus & 4096) > 0) | (nStatus & 8);
}
