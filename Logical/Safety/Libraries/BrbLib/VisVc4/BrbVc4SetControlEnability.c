/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: BrbLib
 * File: BrbVc4SetControlEnability.c
 * Author: niedermeierr
 * Created: June 11, 2013
 ********************************************************************
 * Implementation of library BrbLib
 ********************************************************************/

#include <bur/plctypes.h>
#ifdef __cplusplus
	extern "C"
	{
#endif

#include "BrbLib.h"

#ifdef __cplusplus
	};
#endif

/* Setzt den Status eines Vis-Controls auf Enabled/Disabled */
plcbit BrbVc4SetControlEnability(unsigned short* pStatus, plcbit bEnable)
{
	if(bEnable == 0)
	{
		*pStatus = *pStatus | 2;
		return 0;
	}
	else
	{
		*pStatus = *pStatus & 0xFFFD;
		return 1;
	}
}
