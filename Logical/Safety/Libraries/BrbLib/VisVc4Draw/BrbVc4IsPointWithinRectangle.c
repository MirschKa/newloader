/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: BrbLib
 * File: BrbVc4IsPointWithinRectangle.c
 * Author: niedermeierr
 * Created: November 06, 2014
 ********************************************************************
 * Implementation of library BrbLib
 ********************************************************************/

#include <bur/plctypes.h>
#ifdef __cplusplus
	extern "C"
	{
#endif

#include "BrbLib.h"

#ifdef __cplusplus
	};
#endif

/* Gibt zur�ck, ob ein Punkt innerhalb eines Rechtecks liegt */
plcbit BrbVc4IsPointWithinRectangle(signed long nPointX, signed long nPointY, struct BrbVc4Rectangle_TYP* Rectangle)
{
	BOOL bResult = 0;
	if(			nPointX >= Rectangle->nLeft 
			&&	nPointX <= Rectangle->nLeft + Rectangle->nWidth
			&&	nPointY >= Rectangle->nTop
			&&	nPointY <= Rectangle->nTop + Rectangle->nHeight
		)
	{
		bResult = 1;
	}
	return bResult;
}
