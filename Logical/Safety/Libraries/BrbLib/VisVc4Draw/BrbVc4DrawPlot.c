/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: BrbLib
 * File: BrbVc4DrawPlot.c
 * Author: niedermeierr
 * Created: June 25, 2014
 ********************************************************************
 * Implementation of library BrbLib
 ********************************************************************/

#include <bur/plctypes.h>
#ifdef __cplusplus
	extern "C"
	{
#endif

#include "BrbLib.h"
#include <AnsiCFunc.h>
#include <string.h>
#include <math.h>

#ifdef __cplusplus
	};
#endif

DINT ScaleValueY(REAL rValue, REAL rValueMin, REAL rValueMax, DINT nDrawMin, DINT nDrawMax);
DINT ScaleValueX(REAL rValue, REAL rValueMin, REAL rValueMax, DINT nDrawMin, DINT nDrawMax);
BOOL IsLineWithin(BrbVc4Line_TYP* pLine, DINT nLeft, DINT nTop, DINT nRight, DINT nBottom);
REAL GetDistance(DINT nX1, DINT nY1, DINT nX2, DINT nY2);
UDINT GetRealAsText(REAL rValue, STRING* pText, UDINT nTextSize);

/* Zeichnet einen XY-Graphen */
unsigned short BrbVc4DrawPlot(struct BrbVc4DrawPlot_TYP* pPlot, struct BrbVc4General_TYP* pGeneral)
{
	UINT nResult = 0;
	DINT nScaleFontCharHeightHalf = (DINT)(pPlot->ScaleFont.nCharHeight / 2.0);
	// Statistik vorbesetzen
	pPlot->Statistics.rMinX = 3.4e38;
	pPlot->Statistics.rMaxX = -3.4e38;
	pPlot->Statistics.rMinY = 3.4e38;
	pPlot->Statistics.rMaxY = -3.4e38;
	// Gesamter Zeichenbereich
	BrbVc4Rectangle_TYP DrawArea;
	DrawArea.nLeft = pPlot->nDrawAreaLeft;
	DrawArea.nTop = pPlot->nDrawAreaTop;
	DrawArea.nWidth = pPlot->nDrawAreaWidth;
	DrawArea.nHeight = pPlot->nDrawAreaHeight;
	DrawArea.nFillColor = pPlot->nDrawAreaColor;
	DrawArea.nBorderColor = 0;
	DrawArea.nDashWidth = 0;
	if(pPlot->bShowDrawArea == 1)
	{
		BrbVc4DrawRectangle(&DrawArea, pGeneral);
	}
	// Zeichenbereich des Plots
	BrbVc4Rectangle_TYP DrawAreaPlot;
	DrawAreaPlot.nLeft = DrawArea.nLeft + pPlot->nDrawIndent;
	DrawAreaPlot.nWidth = DrawArea.nWidth - (DINT)(1.5 * pPlot->nDrawIndent);
	DrawAreaPlot.nTop = DrawArea.nTop + nScaleFontCharHeightHalf;
	DrawAreaPlot.nHeight = DrawArea.nHeight - pPlot->ScaleFont.nCharHeight - nScaleFontCharHeightHalf;
	DrawAreaPlot.nFillColor = DrawArea.nFillColor;
	DrawAreaPlot.nBorderColor = 0;
	DrawAreaPlot.nDashWidth = 0;
	DINT nDrawAreaPlotRight = DrawAreaPlot.nLeft + DrawAreaPlot.nWidth;
	DINT nDrawAreaPlotBottom = DrawAreaPlot.nTop + DrawAreaPlot.nHeight;
	// Null-Linien
	if(pPlot->bShowZeroLines == 1)
	{
		// Y
		BrbVc4Line_TYP ZeroLine;
		ZeroLine.nLeft = DrawAreaPlot.nLeft + 1;
		ZeroLine.nRight = nDrawAreaPlotRight - 1;
		ZeroLine.nTop = ScaleValueY(0.0, pPlot->rValueMinY, pPlot->rValueMaxY, DrawAreaPlot.nTop, nDrawAreaPlotBottom);
		ZeroLine.nBottom = ZeroLine.nTop;
		ZeroLine.nColor = pPlot->nZeroLinesColor;
		ZeroLine.nDashWidth = 0;
		BrbVc4DrawLine(&ZeroLine, pGeneral);
		// X
		ZeroLine.nLeft = ScaleValueX(0.0, pPlot->rValueMinX, pPlot->rValueMaxX, DrawAreaPlot.nLeft, nDrawAreaPlotRight);
		ZeroLine.nRight = ZeroLine.nLeft;
		ZeroLine.nTop = DrawAreaPlot.nTop + 1;
		ZeroLine.nBottom = nDrawAreaPlotBottom - 1;
		ZeroLine.nColor = pPlot->nZeroLinesColor;
		ZeroLine.nDashWidth = 0;
		BrbVc4DrawLine(&ZeroLine, pGeneral);
	}
	// Skalierung
	BrbVc4DrawText_TYP ScaleCaption;
	// Skalierung Y
	if(pPlot->bShowScaleY == 1)
	{
		// Basis-Linie
		BrbVc4Line_TYP ScaleBaseY;
		ScaleBaseY.nLeft = ScaleValueX(pPlot->rValueMinX, pPlot->rValueMinX, pPlot->rValueMaxX, DrawAreaPlot.nLeft, nDrawAreaPlotRight);
		ScaleBaseY.nRight = ScaleBaseY.nLeft;
		ScaleBaseY.nTop = ScaleValueY(pPlot->rValueMaxY, pPlot->rValueMinY, pPlot->rValueMaxY, DrawAreaPlot.nTop, nDrawAreaPlotBottom);
		ScaleBaseY.nBottom = ScaleValueY(pPlot->rValueMinY, pPlot->rValueMinY, pPlot->rValueMaxY, DrawAreaPlot.nTop, nDrawAreaPlotBottom);
		ScaleBaseY.nColor = pPlot->nScaleColor;
		ScaleBaseY.nDashWidth = 0;
		BrbVc4DrawLine(&ScaleBaseY, pGeneral);
		// Skalierungsschritte
		ScaleCaption.nFontIndex = pPlot->ScaleFont.nIndex;
		ScaleCaption.nBackgroundColor = 255;
		BrbVc4Line_TYP ScaleLine;
		ScaleLine.nLeft = DrawAreaPlot.nLeft - 2;
		ScaleLine.nRight = ScaleLine.nLeft + 4;
		ScaleLine.nColor = pPlot->nScaleColor;
		ScaleLine.nDashWidth = 0;
		if(pPlot->nScaleCountY > 0)
		{
			UINT nScaleIndex = 0;
			REAL rScaleDistance = (pPlot->rValueMaxY - pPlot->rValueMinY) / pPlot->nScaleCountY;
			for(nScaleIndex=0; nScaleIndex<=pPlot->nScaleCountY; nScaleIndex++)
			{
				REAL rValue = pPlot->rValueMinY + (nScaleIndex * rScaleDistance);
				ScaleLine.nTop = ScaleValueY(rValue, pPlot->rValueMinY, pPlot->rValueMaxY, DrawAreaPlot.nTop, nDrawAreaPlotBottom);
				ScaleLine.nBottom = ScaleLine.nTop;
				BrbVc4DrawLine(&ScaleLine, pGeneral);
				UDINT nTextLen = GetRealAsText(rValue, ScaleCaption.sText, sizeof(ScaleCaption.sText));
				ScaleCaption.nLeft = ScaleLine.nLeft - nTextLen * pPlot->ScaleFont.nCharWidth - 4;
				ScaleCaption.nTop = ScaleLine.nTop - (DINT)(pPlot->ScaleFont.nCharHeight / 2.0);
				ScaleCaption.nTextColor = pPlot->nScaleColor;
				BrbVc4DrawText(&ScaleCaption, pGeneral);
			}
		}
	}
	// Skalierung X
	if(pPlot->bShowScaleX == 1)
	{
		// Basis-Linie
		BrbVc4Line_TYP ScaleBaseX;
		ScaleBaseX.nLeft = ScaleValueX(pPlot->rValueMinX, pPlot->rValueMinX, pPlot->rValueMaxX, DrawAreaPlot.nLeft, nDrawAreaPlotRight);
		ScaleBaseX.nRight = ScaleValueX(pPlot->rValueMaxX, pPlot->rValueMinX, pPlot->rValueMaxX, DrawAreaPlot.nLeft, nDrawAreaPlotRight);
		ScaleBaseX.nTop = ScaleValueY(pPlot->rValueMinY, pPlot->rValueMinY, pPlot->rValueMaxY, DrawAreaPlot.nTop, nDrawAreaPlotBottom);
		ScaleBaseX.nBottom = ScaleBaseX.nTop;
		ScaleBaseX.nColor = pPlot->nScaleColor;
		ScaleBaseX.nDashWidth = 0;
		BrbVc4DrawLine(&ScaleBaseX, pGeneral);
		// Skalierungsschritte
		ScaleCaption.nTop = ScaleBaseX.nTop;
		ScaleCaption.nFontIndex = pPlot->ScaleFont.nIndex;
		ScaleCaption.nBackgroundColor = 255;
		BrbVc4Line_TYP ScaleLine;
		ScaleLine.nTop = ScaleCaption.nTop - 2;
		ScaleLine.nBottom = ScaleLine.nTop + 4;
		ScaleLine.nColor = pPlot->nScaleColor;
		ScaleLine.nDashWidth = 0;
		if(pPlot->nScaleCountX > 0)
		{
			UINT nScaleIndex = 0;
			REAL rScaleDistance = (pPlot->rValueMaxX - pPlot->rValueMinX) / pPlot->nScaleCountX;
			for(nScaleIndex=0; nScaleIndex<=pPlot->nScaleCountX; nScaleIndex++)
			{
				REAL rValue = pPlot->rValueMinX + (nScaleIndex * rScaleDistance);
				ScaleLine.nLeft = ScaleValueX(rValue, pPlot->rValueMinX, pPlot->rValueMaxX, DrawAreaPlot.nLeft, nDrawAreaPlotRight);
				ScaleLine.nRight = ScaleLine.nLeft;
				BrbVc4DrawLine(&ScaleLine, pGeneral);
				UDINT nTextLen = GetRealAsText(rValue, ScaleCaption.sText, sizeof(ScaleCaption.sText));
				ScaleCaption.nLeft = ScaleLine.nLeft;
				// Zwischen-Beschriftungen zentrieren
				ScaleCaption.nLeft -= (DINT)((nTextLen * pPlot->ScaleFont.nCharWidth) / 2.0);
				ScaleCaption.nTextColor = pPlot->nScaleColor;
				BrbVc4DrawText(&ScaleCaption, pGeneral);
			}
		}
	}
	// Plot zeichnen
	if(pPlot->pValues != 0 && pPlot->nValueIndexMax > 0)
	{
		REAL rValueOldX = (pPlot->pValues + 0)->rX;
		if(rValueOldX < pPlot->Statistics.rMinX)
		{
			pPlot->Statistics.rMinX = rValueOldX;
		}
		if(rValueOldX > pPlot->Statistics.rMaxX)
		{
			pPlot->Statistics.rMaxX = rValueOldX;
		}
		REAL rValueOldY = (pPlot->pValues + 0)->rY;
		if(rValueOldY < pPlot->Statistics.rMinY)
		{
			pPlot->Statistics.rMinY = rValueOldY;
		}
		if(rValueOldY > pPlot->Statistics.rMaxY)
		{
			pPlot->Statistics.rMaxY = rValueOldY;
		}
		UDINT nValueIndex = 0;
		REAL rCursorDistanceOld = (REAL)DrawAreaPlot.nWidth;
		for(nValueIndex=1; nValueIndex<=pPlot->nValueIndexMax; nValueIndex++)
		{
			REAL rValueX = (pPlot->pValues + nValueIndex)->rX;
			REAL rValueY = (pPlot->pValues + nValueIndex)->rY;
			BrbVc4Line_TYP CurveLine;
			CurveLine.nLeft = ScaleValueX(rValueOldX, pPlot->rValueMinX, pPlot->rValueMaxX, DrawAreaPlot.nLeft, nDrawAreaPlotRight);
			CurveLine.nRight = ScaleValueX(rValueX, pPlot->rValueMinX, pPlot->rValueMaxX, DrawAreaPlot.nLeft, nDrawAreaPlotRight);
			CurveLine.nTop = ScaleValueY(rValueOldY, pPlot->rValueMinY, pPlot->rValueMaxY, DrawAreaPlot.nTop, nDrawAreaPlotBottom);
			CurveLine.nBottom = ScaleValueY(rValueY, pPlot->rValueMinY, pPlot->rValueMaxY, DrawAreaPlot.nTop, nDrawAreaPlotBottom);
			CurveLine.nColor = pPlot->nPlotColor;
			CurveLine.nDashWidth = 0;
			BOOL bDrawLine = IsLineWithin(&CurveLine, DrawAreaPlot.nLeft, DrawAreaPlot.nTop, nDrawAreaPlotRight, nDrawAreaPlotBottom);
			if(bDrawLine == 1)
			{
				BrbVc4DrawLine(&CurveLine, pGeneral);
			}
			rValueOldX = rValueX;
			rValueOldY = rValueY;
			// Statistik
			if(rValueX < pPlot->Statistics.rMinX)
			{
				pPlot->Statistics.rMinX = rValueX;
			}
			if(rValueX > pPlot->Statistics.rMaxX)
			{
				pPlot->Statistics.rMaxX = rValueX;
			}
			if(rValueY < pPlot->Statistics.rMinY)
			{
				pPlot->Statistics.rMinY = rValueY;
			}
			if(rValueY > pPlot->Statistics.rMaxY)
			{
				pPlot->Statistics.rMaxY = rValueY;
			}
			// Cursor setzen
			if(pPlot->bSetCursorOnClick == 1)
			{
				if(pGeneral->Touch.eState == eBRB_TOUCH_STATE_PUSHED)
				{
					DINT nCursorX = pGeneral->Touch.nX;
					DINT nCursorY = pGeneral->Touch.nY;
					if(nCursorX >= DrawAreaPlot.nLeft && nCursorX <= nDrawAreaPlotRight && nCursorY >= DrawAreaPlot.nTop && nCursorY <= nDrawAreaPlotBottom)
					{
						REAL rCursorDistance = GetDistance(nCursorX, nCursorY, CurveLine.nRight, CurveLine.nBottom);
						if(rCursorDistance < rCursorDistanceOld)
						{
							rCursorDistanceOld = rCursorDistance;
							pPlot->rCursorY = rValueY;
							pPlot->rCursorX = rValueX;
							pPlot->nCursorIndex = nValueIndex;
						}
					}
				}
			}
		}
	}
	// Cursor zeichnen
	if(pPlot->bShowCursor == 1)
	{
		BrbVc4Line_TYP CursorLine;
		CursorLine.nColor = pPlot->nCursorColor;
		CursorLine.nDashWidth = 0;
		// Horizontal
		CursorLine.nLeft = ScaleValueX(pPlot->rValueMinX, pPlot->rValueMinX, pPlot->rValueMaxX, DrawAreaPlot.nLeft, nDrawAreaPlotRight);
		CursorLine.nRight = ScaleValueX(pPlot->rValueMaxX, pPlot->rValueMinX, pPlot->rValueMaxX, DrawAreaPlot.nLeft, nDrawAreaPlotRight);
		CursorLine.nTop = ScaleValueY(pPlot->rCursorY, pPlot->rValueMinY, pPlot->rValueMaxY, DrawAreaPlot.nTop, nDrawAreaPlotBottom);
		CursorLine.nBottom = CursorLine.nTop;
		if(IsLineWithin(&CursorLine, DrawAreaPlot.nLeft, DrawAreaPlot.nTop, nDrawAreaPlotRight, nDrawAreaPlotBottom) == 1)
		{
			BrbVc4DrawLine(&CursorLine, pGeneral);
		}
		// Vertikal
		CursorLine.nLeft = ScaleValueX(pPlot->rCursorX, pPlot->rValueMinX, pPlot->rValueMaxX, DrawAreaPlot.nLeft, nDrawAreaPlotRight);
		CursorLine.nRight = CursorLine.nLeft;
		CursorLine.nTop = ScaleValueY(pPlot->rValueMinY, pPlot->rValueMinY, pPlot->rValueMaxY, DrawAreaPlot.nTop, nDrawAreaPlotBottom);
		CursorLine.nBottom = ScaleValueY(pPlot->rValueMaxY, pPlot->rValueMinY, pPlot->rValueMaxY, DrawAreaPlot.nTop, nDrawAreaPlotBottom);
		if(IsLineWithin(&CursorLine, DrawAreaPlot.nLeft, DrawAreaPlot.nTop, nDrawAreaPlotRight, nDrawAreaPlotBottom) == 1)
		{
			BrbVc4DrawLine(&CursorLine, pGeneral);
		}
		BrbVc4DrawText_TYP CursorCaption;
		memset(&CursorCaption.sText, 0, sizeof(CursorCaption.sText));
		STRING sFtoa[100];
		memset(&sFtoa, 0, sizeof(sFtoa));
		brsftoa(pPlot->rCursorX, (UDINT)&sFtoa);
		strcat(CursorCaption.sText, sFtoa);
		strcat(CursorCaption.sText, " ; ");
		brsftoa(pPlot->rCursorY, (UDINT)&sFtoa);
		strcat(CursorCaption.sText, sFtoa);
		DINT nTextLength = strlen(CursorCaption.sText) * pPlot->ScaleFont.nCharWidth;
		CursorCaption.nFontIndex = pPlot->ScaleFont.nIndex;
		CursorCaption.nBackgroundColor = 255;
		CursorCaption.nLeft = ScaleValueX(pPlot->rCursorX, pPlot->rValueMinX, pPlot->rValueMaxX, DrawAreaPlot.nLeft, nDrawAreaPlotRight) + 4;
		if(CursorCaption.nLeft >= nDrawAreaPlotRight - nTextLength)
		{
			CursorCaption.nLeft -= nTextLength;
		}
		CursorCaption.nTop = ScaleValueY(pPlot->rCursorY, pPlot->rValueMinY, pPlot->rValueMaxY, DrawAreaPlot.nTop, nDrawAreaPlotBottom);
		if(CursorCaption.nTop >= nDrawAreaPlotBottom - pPlot->ScaleFont.nCharHeight)
		{
			CursorCaption.nTop -= pPlot->ScaleFont.nCharHeight;
		}
		CursorCaption.nTextColor = pPlot->nCursorColor;
		BrbVc4DrawText(&CursorCaption, pGeneral);
	}	
	return nResult; 
}

DINT ScaleValueY(REAL rValue, REAL rValueMin, REAL rValueMax, DINT nDrawMin, DINT nDrawMax)
{
	DINT nResult = 0;
	REAL rValueRange = rValueMax - rValueMin;
	DINT nDrawRange = nDrawMax - nDrawMin;
	REAL rFactor = 1;
	if(rValueRange != 0)
	{
		rFactor = (REAL)nDrawRange / rValueRange;
	}
	nResult = (DINT)((rValue - rValueMin) * rFactor);
	nResult = nDrawMax - nResult;
	return nResult;
}

DINT ScaleValueX(REAL rValue, REAL rValueMin, REAL rValueMax, DINT nDrawMin, DINT nDrawMax)
{
	DINT nResult = 0;
	REAL rValueRange = rValueMax - rValueMin;
	DINT nDrawRange = nDrawMax - nDrawMin;
	REAL rFactor = 1;
	if(rValueRange != 0)
	{
		rFactor = (REAL)nDrawRange / rValueRange;
	}
	nResult = (DINT)((rValue - rValueMin) * rFactor);
	nResult += nDrawMin;
	return nResult;
}

BOOL IsLineWithin(BrbVc4Line_TYP* pLine, DINT nLeft, DINT nTop, DINT nRight, DINT nBottom)
{
	BOOL bDrawLine = 1;
	if(
		(pLine->nLeft <= nLeft && pLine->nRight <= nLeft)
		||	(pLine->nLeft >= nRight && pLine->nRight >= nRight)
		||	(pLine->nTop <= nTop && pLine->nBottom <= nTop)
		||	(pLine->nTop >= nBottom && pLine->nBottom >= nBottom)
		)
	{
		bDrawLine = 0;
	}
	if(pLine->nLeft < nLeft)
	{
		pLine->nLeft = nLeft;
	}
	else if(pLine->nLeft > nRight)
	{
		pLine->nLeft = nRight;
	}
	if(pLine->nRight < nLeft)
	{
		pLine->nRight = nLeft;
	}
	else if(pLine->nRight > nRight)
	{
		pLine->nRight = nRight;
	}
	if(pLine->nTop < nTop)
	{
		pLine->nTop = nTop;
	}
	else if(pLine->nTop > nBottom)
	{
		pLine->nTop = nBottom;
	}
	if(pLine->nBottom < nTop)
	{
		pLine->nBottom = nTop;
	}
	else if(pLine->nBottom > nBottom)
	{
		pLine->nBottom = nBottom;
	}
	return bDrawLine;
}

REAL GetDistance(DINT nX1, DINT nY1, DINT nX2, DINT nY2)
{
	return sqrt(pow((REAL)(nX1-nX2), 2) + pow((REAL)(nY1-nY2), 2) );
}
