/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: BrbLib
 * File: BrbVc4DrawLine.c
 * Author: niedermeierr
 * Created: July 02, 2013
 ********************************************************************
 * Implementation of library BrbLib
 ********************************************************************/

#include <bur/plctypes.h>
#ifdef __cplusplus
	extern "C"
	{
#endif

#include "BrbLib.h"
#include <AnsiCFunc.h>

#ifdef __cplusplus
	};
#endif

/* Zeichnet eine Linie */
unsigned short BrbVc4DrawLine(struct BrbVc4Line_TYP* pLine, struct BrbVc4General_TYP* pGeneral)
{
	UINT nResult = 0;
	if(pLine->nDashWidth > 1)
	{
		// Gestrichelt
		DINT nDiffX = pLine->nRight - pLine->nLeft;
		DINT nDiffY = pLine->nBottom - pLine->nTop;
		if(abs(nDiffX) >= abs(nDiffY))
		{
			// Linie breiter als hoch
			if(nDiffX != 0)
			{
				REAL rAngleFactor = (((REAL)nDiffY) / ((REAL)nDiffX));
				BOOL bSolid = 1;
				DINT nStep = 0;
				if(pLine->nLeft <= pLine->nRight)
				{
					// Linie geht von links nach rechts
					for(nStep=0; nStep<=nDiffX; nStep+=pLine->nDashWidth)
					{
						if(bSolid == 1)
						{
							BrbVc4Line_TYP SolidLine;
							SolidLine.nLeft =	pLine->nLeft + nStep;
							SolidLine.nTop = pLine->nTop + (rAngleFactor * nStep);
							SolidLine.nRight = SolidLine.nLeft + pLine->nDashWidth;
							SolidLine.nBottom = pLine->nTop + (rAngleFactor * (nStep + pLine->nDashWidth));
							SolidLine.nColor = pLine->nColor;
							if(SolidLine.nRight > pLine->nRight)
							{
								SolidLine.nRight = pLine->nRight;
							}
							if(SolidLine.nBottom > pLine->nBottom)
							{
								SolidLine.nBottom = pLine->nBottom;
							}
							nResult = VA_Line(1, pGeneral->nVcHandle, SolidLine.nLeft, SolidLine.nTop, SolidLine.nRight, SolidLine.nBottom, SolidLine.nColor);
						}
						bSolid = !bSolid;
					}
				}
				else
				{
					// Linie geht von rechts nach links
					for(nStep=0; nStep>=nDiffX; nStep-=pLine->nDashWidth)
					{
						if(bSolid == 1)
						{
							BrbVc4Line_TYP SolidLine;
							SolidLine.nLeft =	pLine->nLeft + nStep;
							SolidLine.nTop = pLine->nTop + (rAngleFactor * nStep);
							SolidLine.nRight = SolidLine.nLeft - pLine->nDashWidth;
							SolidLine.nBottom = pLine->nTop + (rAngleFactor * (nStep + pLine->nDashWidth));
							SolidLine.nColor = pLine->nColor;
							if(SolidLine.nRight < pLine->nRight)
							{
								SolidLine.nRight = pLine->nRight;
							}
							if(SolidLine.nBottom > pLine->nBottom)
							{
								SolidLine.nBottom = pLine->nBottom;
							}
							nResult = VA_Line(1, pGeneral->nVcHandle, SolidLine.nLeft, SolidLine.nTop, SolidLine.nRight, SolidLine.nBottom, SolidLine.nColor);
						}
						bSolid = !bSolid;
					}
				}
			}
		}
		else
		{
			// Linie h�her als breit
			if(nDiffY != 0)
			{
				REAL rAngleFactor = (((REAL)nDiffX) / ((REAL)nDiffY));
				BOOL bSolid = 1;
				DINT nStep = 0;
				if(pLine->nTop <= pLine->nBottom)
				{
					// Linie geht von oben nach unten
					for(nStep=0; nStep<=nDiffY; nStep+=pLine->nDashWidth)
					{
						if(bSolid == 1)
						{
							BrbVc4Line_TYP SolidLine;
							SolidLine.nLeft =	pLine->nLeft + (rAngleFactor * nStep);
							SolidLine.nTop = pLine->nTop + nStep;
							SolidLine.nRight = pLine->nLeft + (rAngleFactor * (nStep + pLine->nDashWidth));
							SolidLine.nBottom = SolidLine.nTop + pLine->nDashWidth;
							SolidLine.nColor = pLine->nColor;
							if(SolidLine.nRight > pLine->nRight)
							{
								SolidLine.nRight = pLine->nRight;
							}
							if(SolidLine.nBottom > pLine->nBottom)
							{
								SolidLine.nBottom = pLine->nBottom;
							}
							nResult = VA_Line(1, pGeneral->nVcHandle, SolidLine.nLeft, SolidLine.nTop, SolidLine.nRight, SolidLine.nBottom, SolidLine.nColor);
						}
						bSolid = !bSolid;
					}
				}
				else
				{
					// Linie geht von unten nach oben
					for(nStep=0; nStep>=nDiffY; nStep-=pLine->nDashWidth)
					{
						if(bSolid == 1)
						{
							BrbVc4Line_TYP SolidLine;
							SolidLine.nLeft =	pLine->nLeft + (rAngleFactor * nStep);
							SolidLine.nTop = pLine->nTop + nStep;
							SolidLine.nRight = pLine->nLeft + (rAngleFactor * (nStep + pLine->nDashWidth));
							SolidLine.nBottom = SolidLine.nTop - pLine->nDashWidth;
							SolidLine.nColor = pLine->nColor;
							if(SolidLine.nRight > pLine->nRight)
							{
								SolidLine.nRight = pLine->nRight;
							}
							if(SolidLine.nBottom < pLine->nBottom)
							{
								SolidLine.nBottom = pLine->nBottom;
							}
							nResult = VA_Line(1, pGeneral->nVcHandle, SolidLine.nLeft, SolidLine.nTop, SolidLine.nRight, SolidLine.nBottom, SolidLine.nColor);
						}
						bSolid = !bSolid;
					}
				}
			}
		}
		// Der letzte Punkt wird immer gezeichnet
		nResult = VA_Line(1, pGeneral->nVcHandle, pLine->nRight, pLine->nBottom, pLine->nRight, pLine->nBottom, pLine->nColor);
	}
	else
	{
		// Solide
		nResult = VA_Line(1, pGeneral->nVcHandle, pLine->nLeft, pLine->nTop, pLine->nRight, pLine->nBottom, pLine->nColor);
	}
	return nResult;
}
