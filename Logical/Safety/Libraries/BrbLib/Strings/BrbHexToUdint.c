/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: BrbLib
 * File: BrbHexToUdint.c
 * Author: niedermeierr
 * Created: June 11, 2013
 ********************************************************************
 * Implementation of library BrbLib
 ********************************************************************/

#include <bur/plctypes.h>
#ifdef __cplusplus
	extern "C"
	{
#endif

#include "BrbLib.h"
#include <math.h>
#include <string.h>

#ifdef __cplusplus
	};
#endif

/* Wandelt einen Hex-String in einen Udint */
unsigned long BrbHexToUdint(plcstring* pHex)
{
	UDINT nResult = 0;
	UINT nBitDigit = 0;
	UINT nHexLen = strlen((STRING*)pHex);
	INT nCharIndex = 0;
	for(nCharIndex = nHexLen-1; nCharIndex >= 0; nCharIndex--)
	{
		USINT* pChar = (USINT*)pHex+nCharIndex;
		UDINT nDigitValue = 0;
		UDINT nDigitValency = 0;
		if(*pChar >= 48 && *pChar <= 57)
		{
			nDigitValue = (*pChar-48);
		}
		else if(*pChar >= 65 && *pChar <= 70)
		{
			nDigitValue = (*pChar-55);
		}
		nDigitValency = pow(2, nBitDigit);
		nResult = nResult + (nDigitValue*nDigitValency);
		nBitDigit += 4;
	}
	return nResult;
}
