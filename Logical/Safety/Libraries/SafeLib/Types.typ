
TYPE
	SafeDoor_Type : 	STRUCT 
		Closed : SafeSwitch_Type;
		Locked : SafeSwitch_Type;
	END_STRUCT;
	SafeFromSL_Type : 	STRUCT 
		ModulesOK : BOOL;
		StandStill : SafeSwitch_Type;
		ResetRequired : BOOL;
		DoorsClosedAndLocked : BOOL;
		DoorsClosed : BOOL;
		EnableRotor : BOOL;
		EnableTraction : BOOL;
		EnableStacking : BOOL;
		EnableProductTray : BOOL;
		EnablePortioning : BOOL;
		EnableOutfeed : BOOL;
		EnableXYDrive : BOOL;
		AirPressureSupply : SafeSwitch_Type;
		StandStillSensorError : BOOL;
		InterlockResetRequired : BOOL;
		PLC_SafetyOK : BOOL;
		SafeLogicActive : BOOL;
		OptAddInfeedDoor : BOOL;
		OptEndPiceDoor : BOOL;
		OptOptiScan : BOOL;
	END_STRUCT;
	SafeIN_Type : 	STRUCT 
		UnlockDoors : BOOL;
		CleaningMode : BOOL;
		ResetSafeLogic : BOOL;
	END_STRUCT;
	SafeOUT_Type : 	STRUCT 
		SafeSwitchEStop : SafeSwitch_Type;
		AirPressureSupply : SafeSwitch_Type;
		SafeDoorInfeedRight : SafeDoor_Type;
		SafeDoorInfeedLeft : SafeDoor_Type;
		SafeDoorOutfeedRight : SafeDoor_Type;
		SafeDoorOutfeedLeft : SafeDoor_Type;
		SafeInfeedShutter : SafeDoor_Type;
		SafeExternalSafety : SafeDoor_Type;
		SafeAddInfeedLeft : SafeDoor_Type;
		SafeAddInfeedRight : SafeDoor_Type;
		SafeXRayShutter : SafeDoor_Type;
		SafeEndpiceDoorLeft : SafeDoor_Type;
		SafeEndpiceDoorRight : SafeDoor_Type;
		StandStill : SafeSwitch_Type;
		ResetRequired : BOOL;
		DoorsClosedAndLocked : BOOL;
		DoorsClosed : BOOL;
		EnableRotor : BOOL;
		EnableTraction : BOOL;
		EnableStacking : BOOL;
		EnableProductTray : BOOL;
		EnablePortioning : BOOL;
		EnableOutfeed : BOOL;
		EnableXYDrive : BOOL;
		StandStillSensorError : BOOL;
		InterlockResetRequired : BOOL;
		StatusSL : SafeStatusSL_Type;
	END_STRUCT;
	SafeSI_Type : 	STRUCT 
		SafeSwitchEStop : SafeSwitch_Type;
		AirPressureSupplyChannel_A : SafeSwitch_Type;
		AirPressureSupplyChannel_B : SafeSwitch_Type;
		SafeDoorInfeedRight : SafeDoor_Type;
		SafeDoorInfeedLeft : SafeDoor_Type;
		SafeDoorOutfeedRight : SafeDoor_Type;
		SafeDoorOutfeedLeft : SafeDoor_Type;
		SafeInfeedShutter : SafeDoor_Type;
		SafeExternalSafety : SafeDoor_Type;
		SafeAddInfeedLeft : SafeDoor_Type;
		SafeAddInfeedRight : SafeDoor_Type;
		SafeXRayShutter : SafeDoor_Type;
		SafeEndpiceDoorLeft : SafeDoor_Type;
		SafeEndpiceDoorRight : SafeDoor_Type;
		StandStillSensor : SafeSwitch_Type;
		StandStillSensorN : SafeSwitch_Type;
		AirPressureSupply : SafeSwitch_Type;
	END_STRUCT;
	SafeSwitch_Type : 	STRUCT 
		State : BOOL;
		TwoChannel_OK : BOOL;
		ChannelA : BOOL;
		ChannelB : BOOL;
	END_STRUCT;
	SafeToSL_Type : 	STRUCT 
		LockDoors : BOOL;
		UnlockDoors : BOOL;
		CleaningMode : BOOL;
		Reset : BOOL;
	END_STRUCT;
	Safe_Type : 	STRUCT 
		SI : SafeSI_Type;
		IN : SafeIN_Type;
		OUT : SafeOUT_Type;
		FromSL : SafeFromSL_Type;
		ToSL : SafeToSL_Type;
	END_STRUCT;
	SafeDoModulsStatusTyp : 	STRUCT 
		SafeOutStatus : ARRAY[0..5]OF BOOL;
	END_STRUCT;
	SafeStatusSL_Type : 	STRUCT 
		ModulesOK : BOOL;
		CrcOk : BOOL;
		Module : ARRAY[0..15]OF SafeDoModulsStatusTyp;
		SafeLogicAlife : BOOL;
		SafeLogicActive : BOOL;
		NewModule : BOOL;
	END_STRUCT;
END_TYPE
