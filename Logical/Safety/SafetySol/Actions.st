(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: SafetySol
 * File: Actions.st
 * Author: lochnert
 * Created: January 22, 2018
 ********************************************************************
 * Implementation of program SafetySol
 ********************************************************************) 

(* Sonstige Aktionen *)
ACTION ActionResetCmdEnability: 
	SetButtonEnability(ADR(Vis.CmdDownloadApp),FALSE);
	SetButtonEnability(ADR(Vis.CmdResetSL),FALSE);
	SetToggleButtonEnability(ADR(Vis.CmdChangeMachOptions),FALSE);
	BrbVc4SetControlEnability(ADR(Vis.NewCrcCode.nStatus),FALSE);
END_ACTION
	

ACTION ActionSetCmdEnability: 
	IF (brsstrlen(ADR(Vis.NewCrcCode.sValue)) = 8) THEN
		SetButtonEnability(ADR(Vis.CmdDownloadApp),TRUE);
	ELSE
		SetButtonEnability(ADR(Vis.CmdDownloadApp),FALSE);
	END_IF;
	SetButtonEnability(ADR(Vis.CmdResetSL),TRUE);
	SetToggleButtonEnability(ADR(Vis.CmdChangeMachOptions),TRUE);
	BrbVc4SetControlEnability(ADR(Vis.NewCrcCode.nStatus),TRUE);
END_ACTION

ACTION ActionResetStatusEnability:
	SetButtonEnability(ADR(Vis.SlStatus.CmdAck1Module),FALSE);
	SetButtonEnability(ADR(Vis.SlStatus.CmdAck2Module),FALSE);
	SetButtonEnability(ADR(Vis.SlStatus.CmdAck3Module),FALSE);
	SetButtonEnability(ADR(Vis.SlStatus.CmdAck4Module),FALSE);
	SetButtonEnability(ADR(Vis.SlStatus.CmdAcknModule),FALSE);
	SetButtonEnability(ADR(Vis.SlStatus.CmdFwAck),FALSE);
	SetButtonEnability(ADR(Vis.SlStatus.CmdScan),FALSE);
	SetButtonEnability(ADR(Vis.SlStatus.CmdSxExchange),FALSE);
END_ACTION

ACTION Action_MachOptSetStartState:
			
	Vis.MachOption.CmdSetOption.Enabled.bClicked := FALSE;	// Kommondo-Button (übertragung starten) zurücksetzen
	SetButtonEnability(ADR(Vis.MachOption.CmdSetOption),TRUE);
	SetButtonEnability(ADR(Vis.MachOption.CmdConfirmOption),FALSE);
	SetButtonEnability(ADR(Vis.MachOption.CmdCancel),FALSE);
		
	FOR iCnt := 0 TO MAX_MACHINE_OPTIONS DO
		Vis.MachOption.CbReadBack[iCnt].bChecked := FALSE;
		Vis.MachOption.CbConfirmation[iCnt].bChecked := FALSE;
		BrbVc4SetControlEnability(ADR(Vis.MachOption.CbConfirmation[iCnt].nStatus),FALSE);
		SetToggleButtonEnability(ADR(Vis.MachOption.CmdOption[iCnt]),TRUE);
		IF iCnt < ACT_MACH_OPTIONS THEN			
			Vis.MachOption.CmdOption[iCnt].Enabled.bPushed := TRUE;			// Als Startzustand sollen alle Maschinenoptionen ausgewählt sein, und müssen explizit abgewählt werden.
		ELSE
			Vis.MachOption.CmdOption[iCnt].Enabled.bPushed := FALSE;
		END_IF
	END_FOR
	
END_ACTION

ACTION ActionColorStatus:
	
	// R/E Status
	IF Vis.SlStatus.lblRE.nIndex = 102 THEN	// Safe Run
		Vis.SlStatus.lblRE.nColor := COLOR_GREEN;
	ELSE
		Vis.SlStatus.lblRE.nColor := COLOR_RED;
	END_IF
	
	//MXCHG Status
	CASE Vis.SlStatus.lblMXCHG.nIndex OF
		10: // Modules ok
			Vis.SlStatus.lblMXCHG.nColor := COLOR_GREEN;
		20:	// Modules missing
			Vis.SlStatus.lblMXCHG.nColor := COLOR_RED;
		ELSE	// scanning, new modules...
			Vis.SlStatus.lblMXCHG.nColor := COLOR_YELLOW;
	END_CASE
	
	//FW-Ack
	IF Vis.SlStatus.lblFwAck.nIndex = 10 THEN //Alles ok
		Vis.SlStatus.lblFwAck.nColor := COLOR_GREEN;
	ELSE
		Vis.SlStatus.lblFwAck.nColor := COLOR_YELLOW;
	END_IF
	
	//Fail
	CASE Vis.SlStatus.lblFail.nIndex OF
		0: // Pre-Operational
			Vis.SlStatus.lblFail.nColor := COLOR_YELLOW;
		1: // Operational
			Vis.SlStatus.lblFail.nColor := COLOR_GREEN;
		ELSE	// FAIL-Safe
			Vis.SlStatus.lblFwAck.nColor := COLOR_RED;
	END_CASE
	
	

END_ACTION

ACTION Action_HideModuleConfirm:
	BrbVc4SetControlVisiblity(ADR(Vis.SlStatus.CmdAck1Module.Enabled.nStatus),FALSE);
	BrbVc4SetControlVisiblity(ADR(Vis.SlStatus.CmdAck1Module.Disabled.nStatus),FALSE);
	
	BrbVc4SetControlVisiblity(ADR(Vis.SlStatus.CmdAck2Module.Enabled.nStatus),FALSE);
	BrbVc4SetControlVisiblity(ADR(Vis.SlStatus.CmdAck2Module.Disabled.nStatus),FALSE);
	
	BrbVc4SetControlVisiblity(ADR(Vis.SlStatus.CmdAck3Module.Enabled.nStatus),FALSE);
	BrbVc4SetControlVisiblity(ADR(Vis.SlStatus.CmdAck3Module.Disabled.nStatus),FALSE);
	
	BrbVc4SetControlVisiblity(ADR(Vis.SlStatus.CmdAck4Module.Enabled.nStatus),FALSE);
	BrbVc4SetControlVisiblity(ADR(Vis.SlStatus.CmdAck4Module.Disabled.nStatus),FALSE);
	
	BrbVc4SetControlVisiblity(ADR(Vis.SlStatus.CmdAcknModule.Enabled.nStatus),FALSE);
	BrbVc4SetControlVisiblity(ADR(Vis.SlStatus.CmdAcknModule.Disabled.nStatus),FALSE);
	
END_ACTION


ACTION Action_ShowModuleConfirm:
	BrbVc4SetControlVisiblity(ADR(Vis.SlStatus.CmdAck1Module.Enabled.nStatus),TRUE);
	BrbVc4SetControlVisiblity(ADR(Vis.SlStatus.CmdAck1Module.Disabled.nStatus),TRUE);
	
	BrbVc4SetControlVisiblity(ADR(Vis.SlStatus.CmdAck2Module.Enabled.nStatus),TRUE);
	BrbVc4SetControlVisiblity(ADR(Vis.SlStatus.CmdAck2Module.Disabled.nStatus),TRUE);
	
	BrbVc4SetControlVisiblity(ADR(Vis.SlStatus.CmdAck3Module.Enabled.nStatus),TRUE);
	BrbVc4SetControlVisiblity(ADR(Vis.SlStatus.CmdAck3Module.Disabled.nStatus),TRUE);
	
	BrbVc4SetControlVisiblity(ADR(Vis.SlStatus.CmdAck4Module.Enabled.nStatus),TRUE);
	BrbVc4SetControlVisiblity(ADR(Vis.SlStatus.CmdAck4Module.Disabled.nStatus),TRUE);
	
	BrbVc4SetControlVisiblity(ADR(Vis.SlStatus.CmdAcknModule.Enabled.nStatus),TRUE);
	BrbVc4SetControlVisiblity(ADR(Vis.SlStatus.CmdAcknModule.Disabled.nStatus),TRUE);
	
END_ACTION
