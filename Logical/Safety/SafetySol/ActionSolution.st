(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: SafetySol
 * File: ActionSolution.st
 * Author: lochnert
 * Created: January 22, 2018
 ********************************************************************
 * Implementation of program SafetySol
 ********************************************************************) 

(* SafetySolution *)
ACTION ActionSolution: 

	//****************************************************************************************************************************************
	//****************************************************************************************************************************************
	// Remote Control ************************************************************************************************************************
	//****************************************************************************************************************************************
	//****************************************************************************************************************************************
	
	
	GetSlStatus_TON.PT := T#4s;
	
	//FB-Aufruf zum Auslesen von CRC�s
	safeFileInfo_0(SafeLOGICID := 1, Execute := safeFileInfo_0.Execute, Type := safeDATA_TYPE_APPLICATION, pCommandData := ADR(FileInfoCmd_0)); 
		
	safeFileInfo_1(SafeLOGICID := 1, Execute := safeFileInfo_1.Execute, Type := safeDATA_TYPE_APPLICATION, pCommandData := ADR(FileInfoCmd_1)); 
						
	CASE RemoteStep OF 
		0:
			IF SLremote.Control.ExecuteCmd = TRUE THEN
				IF SLremote.Control.Cmds.Data <> 0 THEN 
					Save_Data := SLremote.Control.Cmds.Data; 
					RemoteExec := TRUE; 
				END_IF
				SLremote.Control.Cmds.Command := safeCMD_STATUS; 
				SLremote.Control.Cmds.Data := 0; 
			
				RemoteStep := 1;
			END_IF
						
		1: // Abarbeitung fertig (Status)
			IF (safeRemoteControl_0.Done = 1 OR safeRemoteControl_0.Error = 1) AND (safeRemoteControl_0.StatusID <> ERR_FUB_BUSY) THEN // Die Abfrage auf ERR_FUB_BUSY ist notwendig, da bis Version 4.26 Done, Busy und Error zu sp�t gesetzt werden
				SLremote.Control.ExecuteCmd := FALSE; 
				IF Save_Data <> 0 THEN 
					RemoteStep := 2;
				ELSE
					RemoteExec := FALSE; 
					RemoteStep := 0; 
				END_IF		
			END_IF
					
		2: 
			SLremote.Control.Cmds.Command 	:= safeCMD_ENTER;
			SLremote.Control.Cmds.Data := Save_Data; 
			Save_Data := 0;
			SLremote.Control.ExecuteCmd := TRUE; 
			RemoteStep := 3; 
			
		3: 
			IF (safeRemoteControl_0.StatusID <> safeERR_TIMEOUT) AND (safeRemoteControl_0.StatusID <> ERR_FUB_BUSY)  THEN 
				SLremote.Control.ExecuteCmd := FALSE;
				GetSlStatus_TON.IN := TRUE; 
				RemoteStep := 4;
			END_IF
			
		4: 
			SLremote.Control.ExecuteCmd := TRUE; 
			SLremote.Control.Cmds.Command := safeCMD_STATUS; 
			SLremote.Control.Cmds.Data := 0; 
			RemoteStep := 5; 

			
		5: 		
			IF (safeRemoteControl_0.Done = 1 OR safeRemoteControl_0.Error = 1) AND (safeRemoteControl_0.StatusID <> ERR_FUB_BUSY) THEN // Die Abfrage auf ERR_FUB_BUSY ist notwendig, da bis Version 4.26 Done, Busy und Error zu sp�t gesetzt werden
				SLremote.Control.ExecuteCmd := FALSE; 
				RemoteExec := FALSE; 
				RemoteStep := 0; 
			END_IF	
	END_CASE
	
	GetSlStatus_TON();
	
	safeRemoteControl_0(SafeLOGICID := SLremote.DlApp.AppData.ApplicationID, Execute := SLremote.Control.ExecuteCmd, pCommandData := ADR(SLremote.Control.Cmds));

	// Da SL im Busy-Fall die Aussg�nge nicht richtig zur�cksetzt, muss auch dass manuell getan werden....
	// ... und da ein Schreibzugriff auf die entsprechenden Statusvariablen nicht m�glich ist, muss der komplette Status zun�chst umkopiert werden
	lclSafeLogicStatus := safeRemoteControl_0.SafeLOGICStatus;
	IF (StatusBusy_TON.Q) OR ((safeRemoteControl_0.StatusID > ERR_OK) AND (safeRemoteControl_0.StatusID < ERR_FUB_BUSY)) THEN
		lclSafeLogicStatus.SafeOSState := 0;
	END_IF
	IF (lclSafeLogicStatus.NumberOfDifferentFirmware > 0) OR (lclSafeLogicStatus.NumberOfUDIDMismatches > 0) THEN
		NewModule := TRUE;
	ELSE
		NewModule := FALSE;
	END_IF
	(* variables for visualization *)
	ActionResetStatusEnability;	// Erstmal alle Enables r�cksetzen
	ActionResetCmdEnability;
	// Da SL aus unerfindlichen Gr�nden beim Haochluaf f�r CA 2s den Status SafeKeyChanged zur�ckliefert, diese Meldung um einen Status-Abtastzyklus verz�gern, um keine Irriationen hervorzuufen
	SafeKeyChanged_TON.IN := (lclSafeLogicStatus.SafeKEYChanged = 1);
	SafeKeyChanged_TON.PT := GetSlStatus_TON.PT;
	SafeKeyChanged_TON();
	

	
	// Nur wenn SL im State 0 ist, k�nnen Kommandos angenommen werden. Nur dann macht es �berhaupt Sinn,Buttons einzublenden
	IF (lclSafeLogicStatus.State = 0)  THEN
		IF (SafeKeyChanged_TON.Q = TRUE)  THEN
			SetButtonEnability(ADR(Vis.SlStatus.CmdSxExchange),TRUE);
		ELSIF (lclSafeLogicStatus.Scanning = 0) THEN
			IF (lclSafeLogicStatus.NumberOfUDIDMismatches = 1 ) THEN
				SetButtonEnability(ADR(Vis.SlStatus.CmdAck1Module),TRUE);
			ELSIF (lclSafeLogicStatus.NumberOfUDIDMismatches = 2 ) THEN
				SetButtonEnability(ADR(Vis.SlStatus.CmdAck2Module),TRUE);
			ELSIF (lclSafeLogicStatus.NumberOfUDIDMismatches = 3 ) THEN
				SetButtonEnability(ADR(Vis.SlStatus.CmdAck3Module),TRUE);
			ELSIF (lclSafeLogicStatus.NumberOfUDIDMismatches = 4 ) THEN
				SetButtonEnability(ADR(Vis.SlStatus.CmdAck4Module),TRUE);
			ELSIF (lclSafeLogicStatus.NumberOfUDIDMismatches > 4 ) THEN
				SetButtonEnability(ADR(Vis.SlStatus.CmdAcknModule),TRUE);
			ELSIF (lclSafeLogicStatus.NumberOfDifferentFirmware <> 0) THEN
				SetButtonEnability(ADR(Vis.SlStatus.CmdFwAck),TRUE);
			END_IF
		ELSE
			// Im Falle dass nur der Status abgefragt wird, soll der Scan-Button aus optischen gr�nden (flackern!!) freigebeben bleiben
			SetButtonEnability(ADR(Vis.SlStatus.CmdScan),TRUE);
		END_IF			
	END_IF

	IF (WaitForLazySafety_TOF.Q = 0) THEN
		ActionSetCmdEnability;
	END_IF
	
	IF Step.No > 0 AND Step.No < 4000 THEN 
		ActionResetStatusEnability; 
		ActionResetCmdEnability; 
		Vis.Processing.nStatus := FALSE;
	ELSE
		Vis.Processing.nStatus := TRUE; 
	END_IF
	
	IF RemoteControl_Step > 2 THEN 
		ActionSetCmdEnability; 
		Vis.Processing.nStatus := FALSE; 
	END_IF	

	
			
	// Anzeige Instant Message
	// Busy-Status nur Anzeigen, wenn nicht �blicher Status Update (dieser daurt nicht l�nger als 2s)
	StatusBusy_TON.IN := safeRemoteControl_0.Busy;
	StatusBusy_TON.PT := T#2s;
	StatusBusy_TON();
	
	// Farbe der Instant Message w�hlen
	CASE Vis.StatusText.nIndex OF
		ERR_OK:
			Vis.StatusText.nColor := COLOR_GEA_BLUE;
		1..1001:
			Vis.StatusText.nColor := COLOR_YELLOW;
		1002..1005:
			Vis.StatusText.nColor := COLOR_GREEN;
		ERR_FUB_BUSY:
			BrbVc4SetControlColor(ADR(Vis.StatusText.nColor),StatusBusy_TON.Q,COLOR_YELLOW,COLOR_GEA_BLUE);	// Gelb nur, wenn busy l�nger ansteht
			
		ELSE
			Vis.StatusText.nColor := COLOR_RED;
	END_CASE
	
	// Anzeige Warnung nicht betriebsbereit
	BrbVc4SetControlVisiblity(ADR(Vis.SlStatus.txtOperationalWarning.nStatus),NOT(Vis.SlStatus.lblRE.nIndex = 102)); //102 = Safe Run
		
	// Status-Texte nur sichtbar schalten, wenn SL in g�ltigem Zustand
	BrbVc4SetControlVisiblity(ADR(Vis.SlStatus.lblFail.nStatus),NOT(lclSafeLogicStatus.SafeOSState = 0));
	BrbVc4SetControlVisiblity(ADR(Vis.SlStatus.lblFwAck.nStatus),NOT(lclSafeLogicStatus.SafeOSState = 0));
	BrbVc4SetControlVisiblity(ADR(Vis.SlStatus.lblMXCHG.nStatus),NOT(lclSafeLogicStatus.SafeOSState = 0));
	
	(* status text - MXCHG *)
	IF (lclSafeLogicStatus.LedTestActive = 1) THEN
		Vis.SlStatus.lblMXCHG.nIndex := 40;
	ELSIF (lclSafeLogicStatus.Scanning = 1) THEN
		Vis.SlStatus.lblMXCHG.nIndex := 30;
	ELSIF (lclSafeLogicStatus.NumberOfUDIDMismatches = 1) THEN
		Vis.SlStatus.lblMXCHG.nIndex := 1;
	ELSIF (lclSafeLogicStatus.NumberOfUDIDMismatches = 2) THEN
		Vis.SlStatus.lblMXCHG.nIndex := 2;
	ELSIF (lclSafeLogicStatus.NumberOfUDIDMismatches = 3) THEN
		Vis.SlStatus.lblMXCHG.nIndex := 3;
	ELSIF (lclSafeLogicStatus.NumberOfUDIDMismatches = 4) THEN
		Vis.SlStatus.lblMXCHG.nIndex := 4;
	ELSIF (lclSafeLogicStatus.NumberOfUDIDMismatches > 4) THEN
		Vis.SlStatus.lblMXCHG.nIndex := 5;
	ELSIF (lclSafeLogicStatus.NumberOfMissingModules <> 0) THEN
		Vis.SlStatus.lblMXCHG.nIndex := 20;
	ELSIF (lclSafeLogicStatus.NumberOfUDIDMismatches = 0) THEN
		Vis.SlStatus.lblMXCHG.nIndex := 10;
	END_IF
	(* status text - FW-ACKN *)
	IF (lclSafeLogicStatus.SafeKEYChanged = 0 AND lclSafeLogicStatus.NumberOfDifferentFirmware = 0) THEN
		Vis.SlStatus.lblFwAck.nIndex := 10;
	ELSIF (lclSafeLogicStatus.SafeKEYChanged = 1) THEN
		Vis.SlStatus.lblFwAck.nIndex := 1;
	ELSIF (lclSafeLogicStatus.NumberOfDifferentFirmware <> 0) THEN
		Vis.SlStatus.lblFwAck.nIndex := 2;
	END_IF
	(* status text - FAIL *)
	IF (lclSafeLogicStatus.FailSafe <> 16#55) THEN
		Vis.SlStatus.lblFail.nIndex := 2;
	ELSIF (lclSafeLogicStatus.openSAFETYstate = 0) THEN
		Vis.SlStatus.lblFail.nIndex := 0;
	ELSIF (lclSafeLogicStatus.openSAFETYstate = 1) THEN
		Vis.SlStatus.lblFail.nIndex := 1;
	END_IF
	(* status text - R/E *)
	Vis.SlStatus.lblRE.nIndex := lclSafeLogicStatus.SafeOSState;
		
	
	safeLogicInfo_0( SafeLOGICID := SLremote.DlApp.AppData.ApplicationID, Execute := safeLogicInfo_0.Execute, Type := safeINFO_TYPE_PWD_INFO, pCommandData := ADR(InfoCmdPwdInfo));
	
	//****************************************************************************************************************************************
	//****************************************************************************************************************************************
	// Download Application ******************************************************************************************************************
	//****************************************************************************************************************************************
	//****************************************************************************************************************************************
	
	safeDownloadApplication_0(SafeLOGICID := SLremote.DlApp.AppData.ApplicationID, Execute := SLremote.DlApp.ExecuteDl, pCommandData := ADR(SLremote.DlApp.AppData));
	IF safeDownloadApplication_0.ApplicationCRC <> 0 THEN 
		safeCRC := safeDownloadApplication_0.ApplicationCRC; 
	END_IF
	
	//****************************************************************************************************************************************
	//****************************************************************************************************************************************
	// Download Machine Options ******************************************************************************************************************
	//****************************************************************************************************************************************
	//****************************************************************************************************************************************
	
	safeDownloadData_0(SafeLOGICID := SLremote.DlApp.AppData.ApplicationID, Execute := SLremote.DlData.ExecuteData, Type:= safeDATA_TYPE_MAOP, pCommandData := ADR(SLremote.DlData.Data));
	IF (SLremote.DlData.Data.pMachineOptionsOutput <> 0) THEN
		p_dataMachineOptionsBitOut ACCESS SLremote.DlData.Data.pMachineOptionsOutput;	(* feedback bit machine options*)
	END_IF

	(****************************************************************************************************************************************)
	(****************************************************************************************************************************************)
	(****************************************************************************************************************************************)
	
	(* read UDID form SafeLOGIC *)
(*	readUDIDlowSL(enable := 1, pDevice := ADR(Interface), node := Node, index := 16#2000, subindex := 6, pData := ADR(UDID_Low), datalen := SIZEOF(UDID_Low));

	IF readUDIDlowSL.status <> 0 AND readUDIDlowSL.status <> 65535 THEN
		UDID_Low := 0;
	END_IF

	readUDIDhighSL(enable := 1, pDevice := ADR(Interface), node := Node, index := 16#2000, subindex := 7, pData := ADR(UDID_High), datalen := SIZEOF(UDID_High));

	IF readUDIDhighSL.status <> 0 AND readUDIDhighSL.status <> 65535 THEN
		UDID_High := 0;
	END_IF
*)		

	(****************************************************************************************************************************************)
	(****************************************************************************************************************************************)
	(****************************************************************************************************************************************)
	(* write UDID for SafeLOGIC to command structure *)
	SLremote.DlApp.AppData.UDID_Low := UDID_Low;
	SLremote.DlApp.AppData.UDID_High := UDID_High;
	SLremote.DlData.Data.UDID_Low := UDID_Low;
	SLremote.DlData.Data.UDID_High := UDID_High;

	END_ACTION
