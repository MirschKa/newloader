(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: SafetySample
 * File: SafetySample.fun
 * Author: B&R
 ********************************************************************
 * Functions and function blocks of program SafetySample
 ********************************************************************)

FUNCTION toUpper : UINT
	VAR_INPUT
		pString : UDINT;
	END_VAR
	VAR
		i : USINT;
		letter : REFERENCE TO USINT;
	END_VAR
END_FUNCTION

FUNCTION itoahex : UINT (*converts a UDINT values into a HEX character string*) (*$GROUP=User*)
	VAR_INPUT
		value : UDINT; (*UDINT value to be converted into a HEX character string*)
		pString : UDINT; (*pointer to the destination character string*)
	END_VAR
	VAR
		i_counter : USINT; (*internal variable*)
		i_value : UDINT; (*internal variable*)
		i_cvalue : USINT; (*internal variable*)
	END_VAR
END_FUNCTION

FUNCTION SetToggleButtonEnability : BOOL (*Schaltet zwischen Enabled und Disabled Darstellung um*) (*$GROUP=User*)
	VAR_INPUT
		pButton : REFERENCE TO VisToggleButton_Type;
		Enable : BOOL;
	END_VAR
END_FUNCTION

FUNCTION SetButtonEnability : BOOL (*Schaltet zwischen Enabled und Disabled Darstellung um*) (*$GROUP=User*)
	VAR_INPUT
		pButton : REFERENCE TO VisButton_Type;
		Enable : BOOL;
	END_VAR
END_FUNCTION
