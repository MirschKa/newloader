(********************************************************************
 * COPYRIGHT -- GEA Group
 ********************************************************************
 * Program: m_slremote
 * File: toUpper.st
 * Author: sacher.ch
 * Created: July 14, 2015
 ********************************************************************
 * Implementation of funtion toUpper
 ********************************************************************) 

(* converts all lower case letters to high case letter in a 0-terminated string*)
FUNCTION toUpper
	IF pString <> 0 THEN 
		i := 0;
		REPEAT
			letter ACCESS pString + i;
			// letter is between 'a'=97 and 'z'=122
			IF (letter >= 97) AND (letter <= 122) THEN
				// convert to upper case letter 'a' - 'A'
				letter := letter - (97 - 65);
			END_IF
			// next letter
			i := i + 1;
		UNTIL letter = 0 // 0-terminated string
		END_REPEAT
	END_IF
	
	toUpper := 0;
END_FUNCTION