(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: SafetySample
 * File: SafetySample.typ
 * Author: B&R
 ********************************************************************
 * Local data types of program SafetySample
 ********************************************************************)

TYPE
	SLremoteV2_Type : 	STRUCT 
		Control : SLremoteControl_Type;
		DlApp : SLdlApp_Type;
		DlData : SLremoteDataV2_Type;
	END_STRUCT;
	SLremote_Type : 	STRUCT 
		Control : SLremoteControl_Type;
		DlApp : SLdlApp_Type;
		DlData : SLremoteData_Type;
	END_STRUCT;
	SLdlApp_Type : 	STRUCT 
		ExecuteDl : BOOL;
		AppData : DownloadCmdApplicationTypeV1;
	END_STRUCT;
	SLremoteControl_Type : 	STRUCT 
		ExecuteCmd : BOOL;
		Cmds : RemoteControlCmdTypeV1;
	END_STRUCT;
	SLremoteDataV2_Type : 	STRUCT 
		ExecuteData : BOOL;
		Data : DownloadCmdMaOpTypeV2;
	END_STRUCT;
	SLremoteData_Type : 	STRUCT 
		ExecuteData : BOOL;
		Data : DownloadCmdMaOpTypeV1;
	END_STRUCT;
	VisSafety_Type : 	STRUCT 
		CmdResetSL : VisButton_Type;
		CmdChangeMachOptions : VisToggleButton_Type;
		CmdDownloadApp : VisButton_Type;
		NewCrcCode : BrbVc4String_TYP;
		Processing : BrbVc4Slider_TYP;
		MachOption : VisSafetyMachOpt_Type;
		LayMachOpt : BrbVc4Layer_TYP;
		LaySlStatus : BrbVc4Layer_TYP;
		SlStatus : VisSafetySLStat_Type;
		StatusText : BrbVc4Text_TYP;
	END_STRUCT;
	VisSafetyMachOpt_Type : 	STRUCT 
		CmdOption : ARRAY[0..MAX_MACHINE_OPTIONS]OF VisToggleButton_Type;
		CbReadBack : ARRAY[0..MAX_MACHINE_OPTIONS]OF BrbVc4Checkbox_TYP;
		CbConfirmation : ARRAY[0..MAX_MACHINE_OPTIONS]OF BrbVc4Checkbox_TYP;
		CmdSetOption : VisButton_Type;
		CmdConfirmOption : VisButton_Type;
		CmdCancel : VisButton_Type;
	END_STRUCT;
	VisSafetySLStat_Type : 	STRUCT 
		CmdFwAck : VisButton_Type;
		CmdSxExchange : VisButton_Type;
		CmdScan : VisButton_Type;
		CmdAck1Module : VisButton_Type;
		CmdAck2Module : VisButton_Type;
		CmdAck3Module : VisButton_Type;
		CmdAck4Module : VisButton_Type;
		CmdAcknModule : VisButton_Type;
		lblMXCHG : BrbVc4Text_TYP;
		lblFwAck : BrbVc4Text_TYP;
		lblFail : BrbVc4Text_TYP;
		lblRE : BrbVc4Text_TYP;
		txtOperationalWarning : BrbVc4Text_TYP;
	END_STRUCT;
	StepSafety_Enum : 
		(
		stpsW_COMMAND,
		stps_DLAPP_START_ASCII := 1000,
		stps_DLAPP_DIALOG_ASCII_NO_HEX,
		stps_DLAPP_DIALOG_ASCII_W_ANSWER,
		stps_DLAPP_NO_PASSWORD,
		stps_DLAPP_SET_PASSWORD,
		stps_DLAPP_START_CRC_COMPARE,
		stps_DLAPP_DIALOG_STOP,
		stps_DLAPP_DIALOG_STOP_W_ANSWER,
		stps_DLAPP_DIALOG_START,
		stps_DLAPP_DIALOG_START_W_ANSWER,
		stps_DLAPP_FORMAT_SK,
		stps_DLAPP_W_FORMAT_SK,
		stps_DLAPP_PW_SK,
		stps_DLAPP_W_PW_SK,
		stps_DLAPP_DL_APP,
		stps_DLAPP_W_ACK_REQUESTED,
		stps_DLAPP_DIALOG_CRC_CHECK,
		stps_DLAPP_CRC_CONFIRM,
		stps_DLAPP_W_CONFIRMATION_DONE,
		stps_DLAPP_DIALOG_AFTER_DL,
		stps_DLAPP_W_DIALOG_AFTER_DL,
		stps_RESETSL_DIALOG_START := 2000,
		stps_RESETSL_DIALOG_START_W_ANSW,
		stps_RESETSL_START_RESET,
		stps_RESETSL_W_RESET_DONE,
		stps_MO_DIALOG_START := 3000,
		stps_MO_DIALOG_START_W_ANSWER,
		stps_MO_SET_MACH_OPTIONS,
		stps_MO_W_ACK_REQUESTED,
		stps_MO_DIALOG_CONFIRMATION,
		stps_MO_DIALOG_CONFIRMATION_OK,
		stps_MO_CONFIRM,
		stps_MO_W_CONFIRMATION_DONE,
		stps_MO_DIALOG_AFTER_CONFIRM,
		stps_MO_W_DIALOG_AFTER_CONFIRM,
		stps_RC_SET_COMMAND := 4000,
		stps_RC_W_COMMAND_SET
		);
	SafetyStep_Type : 	STRUCT 
		No : StepSafety_Enum;
		Text : STRING[80];
	END_STRUCT;
	VisToggleButton_Type : 	STRUCT 
		Enabled : BrbVc4ToggleButton_TYP;
		Disabled : BrbVc4ToggleButton_TYP;
	END_STRUCT;
	VisButton_Type : 	STRUCT 
		Enabled : BrbVc4Button_TYP;
		Disabled : BrbVc4Button_TYP;
	END_STRUCT;
END_TYPE
