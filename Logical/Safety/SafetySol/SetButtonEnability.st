(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: SafetySol
 * File: SetButtonEnability.st
 * Author: lochnert
 * Created: February 14, 2018
 ********************************************************************
 * Implementation of program SafetySol
 ********************************************************************) 

(* Schaltet zwischen Enabled und Disabled Darstellung um *)
FUNCTION SetButtonEnability
	IF (ADR(pButton) = 0) THEN		// fehlender Pointer
		SetButtonEnability := 1;
	ELSE
		IF Enable THEN
			BrbVc4SetControlVisiblity(ADR(pButton.Disabled.nStatus),FALSE);
			BrbVc4SetControlVisiblity(ADR(pButton.Enabled.nStatus),TRUE);
		ELSE
			BrbVc4SetControlVisiblity(ADR(pButton.Disabled.nStatus),TRUE);
			BrbVc4SetControlVisiblity(ADR(pButton.Enabled.nStatus),FALSE);
		END_IF
		
		SetButtonEnability := 0;
	END_IF
	
			
		
END_FUNCTION


(* Schaltet zwischen Enabled und Disabled Darstellung um *)
FUNCTION SetToggleButtonEnability
	IF ADR(pButton) = 0 THEN		// fehlender Pointer
		SetToggleButtonEnability := 1;
	ELSE
		IF Enable THEN
			BrbVc4SetControlVisiblity(ADR(pButton.Disabled.nStatus),FALSE);
			BrbVc4SetControlVisiblity(ADR(pButton.Enabled.nStatus),TRUE);
		ELSE
			BrbVc4SetControlVisiblity(ADR(pButton.Disabled.nStatus),TRUE);
			BrbVc4SetControlVisiblity(ADR(pButton.Enabled.nStatus),FALSE);
		END_IF
		
		SetToggleButtonEnability := 0;
	END_IF
	
			
		
END_FUNCTION
