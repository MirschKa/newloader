﻿<?xml version="1.0" encoding="utf-8"?>
<?AutomationStudio Version=4.5.3.86 SP?>
<Program SubType="IEC" xmlns="http://br-automation.co.at/AS/Program">
  <Files>
    <File Description="Implementation code">SafetySample.st</File>
    <File Description="Local data types" Private="true">SafetySample.typ</File>
    <File Description="Local variables" Private="true">SafetySample.var</File>
    <File Description="Useful functions" Private="true">SafetySample.fun</File>
    <File Description="converts a UDINT values into a HEX character string">itoahex.st</File>
    <File>toUpper.st</File>
    <File>ActionSolution.st</File>
    <File>Actions.st</File>
    <File Description="Schaltet zwischen Enabled und Disabled Darstellung um">SetButtonEnability.st</File>
  </Files>
</Program>