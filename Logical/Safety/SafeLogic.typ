(********************************************************************
 * COPYRIGHT -- Microsoft
 ********************************************************************
 * Package: SafeLogic
 * File: SafeLogic.typ
 * Author: Christian
 * Created: July 08, 2015
 ********************************************************************
 * Data types of package SafeLogic
 ********************************************************************)

TYPE
	M_SafeLogicRemote_typ : 	STRUCT 
		Cmd : M_SafeLogicRemoteCmd_typ;
		Para : M_SafeLogicRemotePara_typ;
		Stat : M_SafeLogicRemoteStat_typ;
		IO : M_SafeLogicRemoteIo_typ;
	END_STRUCT;
	M_SafeLogicRemoteCmd_typ : 	STRUCT 
		FormatSafeKey : BOOL;
		ChangePassword : BOOL;
		DownloadApplication : BOOL;
		CrcAcknowledge : BOOL;
		DownloadMachOptions : BOOL;
		MachOptionsAcknowledge : BOOL;
		SafeKeyAcknowledge : BOOL;
		ModuleAcknowledge : BOOL;
		FirmwareAcknowledge : BOOL;
		RebootSafeLogic : BOOL;
		GetTargetConfiguration : BOOL;
		ErrorAcknowledge : BOOL;
	END_STRUCT;
	M_SafeLogicRemotePara_typ : 	STRUCT 
		Config : M_SafeLogicRemoteParaConfig_typ;
		User : M_SafeLogicRemoteParaUser_typ;
	END_STRUCT;
	M_SafeLogicRemoteParaConfig_typ : 	STRUCT 
		SafeLogicID : UINT;
		ApplicationID : UINT;
		ApplicationDevice : STRING[16];
		ApplicationFile : STRING[32];
		InfoDevice : STRING[16];
		InfoFile : STRING[32];
		Version : UINT;
	END_STRUCT;
	M_SafeLogicRemoteParaUser_typ : 	STRUCT 
		SafeKeyPassword : STRING[16];
		NewSafeKeyPassword : STRING[16];
		MachOptions : ARRAY[0..7]OF BOOL;
		AckMachOptions : ARRAY[0..7]OF BOOL;
		Crc : STRING[10];
	END_STRUCT;
	M_SafeLogicRemoteStat_typ : 	STRUCT 
		Finished : BOOL;
		SeqString : STRING[32];
		StepString : STRING[32];
		SafeLogicState : USINT;
		Scanning : BOOL;
		SafeKeyChanged : USINT;
		NumberOfMissingModules : UINT;
		NumberOfUDIDMismatched : UINT;
		NumberOfDifferntFirmware : UINT;
		FirstMissingModule : USINT;
		FirstUDIDMismatch : USINT;
		FirstDifferentFirmware : USINT;
		ProjectCrc : STRING[8];
		ProjectDate : STRING[8];
		MachOptions : ARRAY[0..7]OF BOOL;
		Error : BOOL;
		ErrorNumber : UINT;
		ErrorInfo : UINT;
	END_STRUCT;
	M_SafeLogicRemoteIo_typ : 	STRUCT 
		UDID_low : UDINT;
		UDID_high : UINT;
	END_STRUCT;
	SafeLogicOsState_enum : 
		(
		slOsState_Invalid := 0,
		slOsState_Initialising := 15,
		slOsState_Loading := 51,
		slOsState_StopSafe := 85,
		slOsState_RunSafe := 102,
		slOsState_HaltDebug := 153,
		slOsState_StopDebug := 170,
		slOsState_RunDebug := 204,
		slOsState_NoExecution := 240
		);
	SafeLogicXml_typ : 	STRUCT 
		Crc : STRING[12];
		Timestamp : STRING[32];
	END_STRUCT;
END_TYPE
