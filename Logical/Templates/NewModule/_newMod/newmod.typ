(*----- Standard Type -----*)

TYPE
	NewMod_Typ : 	STRUCT 
		Interlock : BOOL;
		Configuration : NewModCfg_Typ;
		Parameter : NewModPara_Typ;
		Command : NewModCmd_Typ;
		Info : GenInfo_Typ;
		Status : NewModStat_Typ;
	END_STRUCT;
	NewModCfg_Typ : 	STRUCT 
		Available : BOOL;
		Simulation : BOOL;
	END_STRUCT;
	NewModCmd_Typ : 	STRUCT 
		Init : BOOL; (*Start module initialization*)
		Reset : BOOL; (*Reset module errors*)
		Axis : NewModAxisCmd_Typ;
		Cyl : NewModCylCmd_Typ;
	END_STRUCT;
	NewModPara_Typ : 	STRUCT 
		Velocity : REAL;
		Position : LREAL;
		Distance : LREAL;
		Acceleration : REAL;
		Deceleration : REAL;
		Ratio : REAL;
	END_STRUCT;
	NewModStat_Typ : 	STRUCT 
		Axis : NewModAxisStat_Typ;
		Cyl : NewModCylStat_Typ;
	END_STRUCT;
END_TYPE

(*----- Axis Type -----*)

TYPE
	NewModAxisCmd_Typ : 	STRUCT  (*axis commands*)
		SetHardware : BOOL; (* (delete not used commands)*)
		PowerOn : BOOL; (* (delete not used commands)*)
		PowerOff : BOOL; (* (delete not used commands)*)
		Home : BOOL; (* (delete not used commands)*)
		MovePositive : BOOL; (* (delete not used commands)*)
		MoveNegative : BOOL;
		MoveAbsolute : BOOL; (* (delete not used commands)*)
		MoveAdditive : BOOL; (* (delete not used commands)*)
		Stop : BOOL; (* (delete not used commands)*)
		JogPositive : BOOL; (* (delete not used commands)*)
		JogNegative : BOOL; (* (delete not used commands)*)
		Gear : BOOL; (* (delete not used commands)*)
	END_STRUCT;
	NewModAxisPara_Typ : 	STRUCT  (*axis parameters*)
		Velocity : REAL; (* (delete not used parameters)*)
		Acceleration : REAL; (* (delete not used parameters)*)
		Deceleration : REAL; (* (delete not used parameters)*)
		Position : LREAL; (* (delete not used parameters)*)
		Distance : LREAL; (* (delete not used parameters)*)
		Gear : NewModAxisGear_Typ; (* (delete not used parameters)*)
	END_STRUCT;
	NewModAxisGear_Typ : 	STRUCT  (*axis gear parameters*)
		MasterAxisName : STRING[32]; (*name of the master axis to couple up*)
		Ratio : REAL; (*ratio to master axis*)
	END_STRUCT;
	NewModAxisStat_Typ : 	STRUCT  (*axis status*)
		Position : LREAL; (*posistion of axis*)
		Velocity : REAL; (*velocity of axis*)
		PowerOn : BOOL; (*axis is switched on*)
		IsHomed : BOOL; (*axis is homed*)
		InStandstill : BOOL; (*axis standstill*)
		IsMoving : BOOL; (*axis is moving*)
		InGear : USINT; (*axis is in gear*)
	END_STRUCT;
END_TYPE

(*----- Cylinder Type -----*)

TYPE
	NewModCylCmd_Typ : 	STRUCT  (*cylinder commands*)
		Lift : BOOL; (*Variante 1 (delete not used commands)*)
		Lower : BOOL; (*Variante 1 (delete not used commands)*)
		Up : BOOL; (*Variante 2 (delete not used commands)*)
		Down : BOOL; (*Variante 2 (delete not used commands)*)
		Open : BOOL; (*Variante 3 (delete not used commands)*)
		Close : BOOL; (*Variante 3 (delete not used commands)*)
		In : BOOL; (*Variante 4 (delete not used commands)*)
		Out : BOOL; (*Variante 4 (delete not used commands)*)
	END_STRUCT;
	NewModCylStat_Typ : 	STRUCT  (*cylinder status*)
		IsLifted : BOOL; (*Variante 1 (delete not used status)*)
		IsLowered : BOOL; (*Variante 1 (delete not used status)*)
		IsUp : BOOL; (*Variante 2 (delete not used status)*)
		IsDown : BOOL; (*Variante 2 (delete not used status)*)
		IsOpened : BOOL; (*Variante 3 (delete not used status)*)
		IsClosed : BOOL; (*Variante 3 (delete not used status)*)
		IsIn : BOOL; (*Variante 4 (delete not used status)*)
		IsOut : BOOL; (*Variante 4 (delete not used status)*)
	END_STRUCT;
END_TYPE
