
FUNCTION_BLOCK newModule_FB
	VAR_INPUT
		Interlock : BOOL;
		pMe : REFERENCE TO StationMe_Typ;
		Instance : STRING[32];
		Config : NewModCfg_Typ;
		Command : NewModCmd_Typ;
		Parameter : NewModPara_Typ;
	END_VAR
	VAR_OUTPUT
		Valid : BOOL;
		Info : GenInfo_Typ;
		Status : NewModStat_Typ;
	END_VAR
	VAR
		rModule : REFERENCE TO NewMod_Typ;
		internal : ModuleInternal_Typ;
	END_VAR
END_FUNCTION_BLOCK
