// ===================================================================
//                        COPYRIGHT -- GEA GROUP 
// ===================================================================
// Program:    
// Descrition: 
//
// >>>===================== git information ==========================<<<

FUNCTION_BLOCK newModule_FB
	IF Valid = FALSE THEN
		IF ADR(pMe) <> 0 THEN
			internal.ModuleIndex := 255;
			FOR internal.i := 0 TO MAX_NUMBER_OF_MODULES DO
				IF pMe.Modules[internal.i].Name = Instance THEN
					internal.ModuleIndex := internal.i;
				END_IF
			END_FOR
		
			IF internal.ModuleIndex <= MAX_NUMBER_OF_MODULES THEN
				internal.CtrlSize := SIZEOF(rModule);
				IF pMe.Modules[internal.ModuleIndex].CtrlSize = internal.CtrlSize THEN
					Valid := TRUE;
				END_IF
			END_IF
		END_IF
	END_IF	
	
	IF Valid = TRUE THEN
		rModule.Interlock := Interlock;
		rModule ACCESS pMe.Modules[internal.ModuleIndex].pCtrl;
		rModule.Command := Command;
		rModule.Configuration := Config;
		rModule.Parameter := Parameter;
		Info := rModule.Info;
		Status := rModule.Status;		
	END_IF
END_FUNCTION_BLOCK