
TYPE
	LocalStepCtrl_Typ : 	STRUCT 
		Step : LocalStep_Enum;
		StepName : STRING[80];
		EnterNewStep : BOOL;
		PreviousStep : LocalStep_Enum;
	END_STRUCT;
	LocalStep_Enum : 
		(
		stepNO_STEP := 0,
		stepNO_INIT,
		stepINIT_AXIS,
		stepINIT_CYLINDER,
		stepINIT_DONE,
		stepWAIT_FOR_COMMAND,
		stepPOWER_ON,
		stepPOWER_ON_DONE,
		stepPOWER_OFF,
		stepPOWER_OFF_DONE,
		stepHOME,
		stepHOME_DONE,
		stepSTOP,
		stepSTOP_DONE,
		stepMOVE_VELOCITY,
		stepMOVE_VELOCITY_DONE,
		stepMOVE_ABSOLUTE,
		stepMOVE_ABSOLUTE_DONE,
		stepMOVE_ADDITIVE,
		stepMOVE_ADDITIVE_DONE,
		stepJOG_POSITIVE,
		stepJOG_NEGATIVE,
		stepGEAR,
		stepGEAR_DONE,
		stepOPEN_GRIPPER,
		stepOPEN_GRIPPER_DONE,
		stepCLOSE_GRIPPER,
		stepCLOSE_GRIPPER_DONE,
		stepERROR,
		stepERROR_RESET
		);
	LocalEvent_Enum : 
		( (*local event ids*)
		modSTATE_OK, (*no feedback fault*)
		modNOT_INITIALIZED,
		modNOT_AVAILABLE,
		modINTERLOCK_ACTIVE,
		modINIT_ERROR
		);
END_TYPE
