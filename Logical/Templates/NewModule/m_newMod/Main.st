// ===================================================================
//                        COPYRIGHT -- GEA GROUP 
// ===================================================================
// Program: 	m_NewModule   
// Descrition: this is module for station example
// 				https://geakempten.atlassian.net/wiki/spaces/GE/pages/2389835817/Diskussion+Modul
//
// >>>===================== git information ==========================<<<


PROGRAM _INIT
	
		// ----- nur f�r IBN ARSIM -----
//	D_ExampleAxis.Parameter.Acceleration := 80;
//	D_ExampleAxis.Parameter.Deceleration := 80;	
//	D_ExampleAxis.Parameter.Velocity     := 20;
//	D_ExampleAxis.Parameter.Ratio 		 := 1.0;

	
END_PROGRAM

PROGRAM _CYCLIC
	
	
	// ==========================================================
	//	register module in station
	// ==========================================================
	
	
	RegisterModule.CtrlName := 'M_NewModule';
	//	RegisterModule.pParent := ADR(gSlicer.Portioningsystem);
	RegisterModule();

	
	// ==========================================================
	//	module configuration
	// ==========================================================
	
	
	// ===== clear all module status and info =====
	
	IF (M_NewModule.Configuration.Available = FALSE) THEN
		
		D_ExampleAxis.Configuration.Available := FALSE;
		D_ExampleAxis();
		D_ExampleCylinder.Configuration.Available := FALSE;
		D_ExampleCylinder();
		
		// ----- module info -----
		M_NewModule.Info.InitOk 	:= FALSE;
		M_NewModule.Info.Busy 		:= FALSE;
		M_NewModule.Info.Done 		:= FALSE;
		M_NewModule.Info.Warning	:= FALSE;
		M_NewModule.Info.Error 	  	:= FALSE;
		M_NewModule.Info.EventId   := modNOT_AVAILABLE;
		M_NewModule.Info.EventText := 'NOT_AVAILABLE';
		sStepCtrl.Step 				:= stepNO_STEP;
		sStepCtrl.StepName 			:= 'stepNO_STEP';

		// ----- axis status -----	
		M_NewModule.Status.Axis.Position   	 := 0.0;
		M_NewModule.Status.Axis.Velocity   	 := 0.0;
		M_NewModule.Status.Axis.PowerOn 	  	 := FALSE;
		M_NewModule.Status.Axis.IsHomed 	  	 := FALSE;
		M_NewModule.Status.Axis.IsMoving   	 := FALSE;
		M_NewModule.Status.Axis.InStandstill := FALSE;
		M_NewModule.Status.Axis.InGear		 := FALSE;
		
		// ----- gripper status -----
		M_NewModule.Status.Cyl.IsOpened := FALSE;
		M_NewModule.Status.Cyl.IsClosed := FALSE;
		
		RETURN;
	END_IF
	
	
	// ===========================================================
	// 	module state machine
	// ===========================================================
	
	
	// ----- stop has priority over other movement commands -----
	
	
	// ==========================================================
	//	modul not initialized
	// ==========================================================
	
	
	IF (M_NewModule.Command.Init = TRUE) AND (M_NewModule.Info.Busy = FALSE) AND (M_NewModule.Info.InitOk = FALSE) THEN
		SetInfo(dsBUSY, ADR(M_NewModule.Info));
		sStepCtrl.Step := stepNO_INIT;
		
	ELSIF (M_NewModule.Info.InitOk = FALSE) THEN
		M_NewModule.Info.EventId   := modNOT_INITIALIZED;
		M_NewModule.Info.EventText := 'NOT_INITIALIZED';
	END_IF
	
	
	// ===========================================================
	// 	reset warning
	// ===========================================================
	
	
	IF EDGEPOS(M_NewModule.Command.Reset) THEN
		M_NewModule.Info.Warning 	:= FALSE;
		M_NewModule.Info.EventId  	:= STATE_OK;
		M_NewModule.Info.EventText	:= 'STATE_OK';
	END_IF

	D_ExampleAxis.Command.Reset 	  := M_NewModule.Command.Reset;
	D_ExampleCylinder.Command.Reset := M_NewModule.Command.Reset;
	
	
	// ===========================================================
	// 	sequencer
	// ===========================================================
	
	
	// ===== commands with priority =====

	IF (M_NewModule.Command.Axis.Stop = TRUE) AND (D_ExampleAxis.Status.IsMoving = TRUE) THEN
		SetInfo(dsBUSY, ADR(M_NewModule.Info));
		sStepCtrl.Step := stepWAIT_FOR_COMMAND;
	END_IF
		
	// ===== log enter step =====
	
	StepMonitor(pStepCtrl := ADR(sStepCtrl), Update := FALSE);	
		
	CASE sStepCtrl.Step OF		
		
		//	===== module initialization =====

		// ----- start init procedure -----
		stepNO_INIT:			
			//  the init command starts the init routine of all drivers 
			IF (M_NewModule.Command.Init = TRUE) THEN	
				// prepare driver configuration
				D_ExampleAxis.Configuration.Available 		:= TRUE;		
				D_ExampleCylinder.Configuration.Available := TRUE;				
				sStepCtrl.Step 									:= stepINIT_AXIS;
			END_IF
		
			// ----- initialization of driver ExampleAxis -----
		stepINIT_AXIS:
			sStepCtrl.StepName 								  := 'stepINIT_AXIS';
			D_ExampleAxis.Configuration.Simulation 	  := M_NewModule.Configuration.Simulation;
			D_ExampleAxis.Configuration.AxisName 		  := 'gAxExample'; 
			D_ExampleAxis.Configuration.PlcAddress 	  := 'SL2.IF1.ST56/DriveConfiguration/Channel2'; 
			D_ExampleAxis.Configuration.Homing.Mode 	  := axHOME_DIRECT;
			D_ExampleAxis.Configuration.Homing.SwLimits := axSW_LIMITS_INACTIVE;
			D_ExampleAxis.Command.PowerOn 			  	  := FALSE;
			D_ExampleAxis.Command.PowerOff			  	  := FALSE;
			D_ExampleAxis.Command.Home 			 	  	  := FALSE;
			D_ExampleAxis.Command.Stop				  	 	  := FALSE;	
			D_ExampleAxis.Command.Gear 			 	  	  := FALSE;
			D_ExampleAxis.Command.JogNegative 	  		  := FALSE;
			D_ExampleAxis.Command.JogPositive 	  		  := FALSE;
			D_ExampleAxis.Command.Init 					  := TRUE;
			
			IF (D_ExampleAxis.Info.Done = TRUE) AND (D_ExampleAxis.Info.InitOk = TRUE) THEN
				D_ExampleAxis.Command.Init := FALSE;
				sStepCtrl.Step 			   := stepINIT_CYLINDER;	// go to next driver or INIT_DONE
				
			ELSIF (D_ExampleAxis.Info.Error = TRUE) THEN
				;	// stay here for diagnostics
				SetInfoError(modINIT_ERROR, 'Driver ExampleAxis init error', ADR(M_NewModule.Info));
			END_IF
		
			// ----- initialization of driver ExampleCylinder -----
		stepINIT_CYLINDER:
			sStepCtrl.StepName 				 				 := 'stepINIT_CYLINDER';
			D_ExampleCylinder.Configuration.Simulation := M_NewModule.Configuration.Simulation;
			D_ExampleCylinder.Command.Init 				 := TRUE;
			
			IF (D_ExampleCylinder.Info.Done = TRUE) AND (D_ExampleCylinder.Info.InitOk = TRUE) THEN
				D_ExampleCylinder.Command.Init := FALSE;
				sStepCtrl.Step 					 := stepINIT_DONE;	// go to next driver or INIT_DONE
				
			ELSIF (D_ExampleCylinder.Info.Error = TRUE) THEN
				;	// stay here for diagnostics
				SetInfoError(modINIT_ERROR, 'Driver ExampleCylinder init error', ADR(M_NewModule.Info));
			END_IF
			
			// ----- init done -----	
		stepINIT_DONE:
			sStepCtrl.StepName 			:= 'stepINIT_DONE';
			M_NewModule.Info.EventId   := modSTATE_OK;
			M_NewModule.Info.EventText := 'STATE_OK';
			M_NewModule.Info.InitOk 	:= TRUE;
			
			SetInfo(dsDONE, ADR(M_NewModule.Info));
			
			// wait until init command is reset
			IF (M_NewModule.Command.Init = FALSE) THEN
				sStepCtrl.Step := stepWAIT_FOR_COMMAND;	
			END_IF
				
			//	===== module ready for operation =====
			
			// ----- wait for command -----
		stepWAIT_FOR_COMMAND:
			sStepCtrl.StepName := 'stepWAIT_FOR_COMMAND';	
			
			// ----- switch drive on -----
			IF (M_NewModule.Command.Axis.PowerOn = TRUE) THEN
				// prepare driver command to save one cycle
				D_ExampleAxis.Command.PowerOn := TRUE;
				sStepCtrl.Step 			  		:= stepPOWER_ON;
					
				// ----- switch drive off -----
			ELSIF (M_NewModule.Command.Axis.PowerOff = TRUE) THEN
				// prepare driver command to save one cycle
				D_ExampleAxis.Command.PowerOff := TRUE;
				sStepCtrl.Step 					 := stepPOWER_OFF; 
				
				// ----- stop axis -----
			ELSIF (M_NewModule.Command.Axis.Stop = TRUE) THEN
				// prepare driver command to save one cycle
				D_ExampleAxis.Command.Stop 			 := TRUE;
				D_ExampleAxis.Parameter.Deceleration := M_NewModule.Parameter.Deceleration;
				sStepCtrl.Step 			  				 := stepSTOP;
					
				// ----- home drive  -----
			ELSIF (M_NewModule.Command.Axis.Home = FALSE) AND (M_NewModule.Interlock = FALSE) THEN
				// prepare driver command to save one cycle
				D_ExampleAxis.Command.Home := TRUE;
				sStepCtrl.Step 				:= stepHOME;
				
			ELSIF (M_NewModule.Command.Axis.Home = TRUE) AND (M_NewModule.Interlock = TRUE) THEN
				SetInfoWarning(modINTERLOCK_ACTIVE, 'move home is interlocked', ADR(M_NewModule.Info));
						
				// ----- start infinite movement into positive direction -----
			ELSIF (M_NewModule.Command.Axis.MovePositive = TRUE) AND (M_NewModule.Interlock = FALSE) THEN
				// prepare driver command to save one cycle
				D_ExampleAxis.Command.MoveVelocity  := TRUE;
				D_ExampleAxis.Parameter.Acceleration := M_NewModule.Parameter.Acceleration;
				D_ExampleAxis.Parameter.Deceleration := M_NewModule.Parameter.Deceleration;
				D_ExampleAxis.Parameter.Velocity 	 := M_NewModule.Parameter.Velocity;
				D_ExampleAxis.Parameter.Direction    := mcDIR_POSITIVE;
				sStepCtrl.Step 		   			    := stepMOVE_VELOCITY;
				
			ELSIF (M_NewModule.Command.Axis.MovePositive = TRUE) AND (M_NewModule.Interlock = TRUE) THEN
				SetInfoWarning(modINTERLOCK_ACTIVE, 'move positive is interlocked', ADR(M_NewModule.Info));
					
				// ----- start infinite movement into negative direction -----
			ELSIF (M_NewModule.Command.Axis.MoveNegative = TRUE) AND (M_NewModule.Interlock = FALSE) THEN
				// prepare driver command to save one cycle
				D_ExampleAxis.Command.MoveVelocity 	 := TRUE;
				D_ExampleAxis.Parameter.Acceleration := M_NewModule.Parameter.Acceleration;
				D_ExampleAxis.Parameter.Deceleration := M_NewModule.Parameter.Deceleration;
				D_ExampleAxis.Parameter.Velocity 	 := M_NewModule.Parameter.Velocity;
				D_ExampleAxis.Parameter.Direction    := mcDIR_NEGATIVE;
				sStepCtrl.Step 					 	    := stepMOVE_VELOCITY;
					
			ELSIF (M_NewModule.Command.Axis.MoveNegative = TRUE) AND (M_NewModule.Interlock = TRUE) THEN
				SetInfoWarning(modINTERLOCK_ACTIVE, 'move negative is interlocked', ADR(M_NewModule.Info));
				
				// ----- start movement with defined distance -----
			ELSIF (M_NewModule.Command.Axis.MoveAdditive = TRUE) AND (M_NewModule.Interlock = FALSE) THEN
				// prepare driver command to save one cycle
				D_ExampleAxis.Command.MoveAdditive   := TRUE;
				D_ExampleAxis.Parameter.Acceleration := M_NewModule.Parameter.Acceleration;
				D_ExampleAxis.Parameter.Deceleration := M_NewModule.Parameter.Deceleration;
				D_ExampleAxis.Parameter.Velocity 	 := M_NewModule.Parameter.Velocity;
				sStepCtrl.Step 						    := stepMOVE_ADDITIVE;
				
			ELSIF (M_NewModule.Command.Axis.MoveAdditive = TRUE) AND (M_NewModule.Interlock = TRUE) THEN
				SetInfoWarning(modINTERLOCK_ACTIVE, 'move additive is interlocked', ADR(M_NewModule.Info));
				
				// ----- start movement with absolute distance -----
			ELSIF (M_NewModule.Command.Axis.MoveAbsolute = TRUE) AND (M_NewModule.Interlock = FALSE) THEN
				// prepare driver command to save one cycle
				D_ExampleAxis.Command.MoveAbsolute   := TRUE;
				D_ExampleAxis.Parameter.Acceleration := M_NewModule.Parameter.Acceleration;
				D_ExampleAxis.Parameter.Deceleration := M_NewModule.Parameter.Deceleration;
				D_ExampleAxis.Parameter.Velocity 	 := M_NewModule.Parameter.Velocity;
				D_ExampleAxis.Parameter.Position  	 := M_NewModule.Parameter.Position;
				sStepCtrl.Step 						    := stepMOVE_ABSOLUTE;
				
			ELSIF (M_NewModule.Command.Axis.MoveAbsolute = TRUE) AND (M_NewModule.Interlock = TRUE) THEN
				SetInfoWarning(modINTERLOCK_ACTIVE, 'move absolute is interlocked', ADR(M_NewModule.Info));
				
				// ----- jog into positive direction -----
			ELSIF (M_NewModule.Command.Axis.JogPositive = TRUE) AND (M_NewModule.Interlock = FALSE) THEN
				// prepare driver command to save one cycle
				D_ExampleAxis.Command.JogPositive    := TRUE;
				D_ExampleAxis.Parameter.Acceleration := M_NewModule.Parameter.Acceleration;
				D_ExampleAxis.Parameter.Deceleration := M_NewModule.Parameter.Deceleration;
				D_ExampleAxis.Parameter.Velocity 	 := M_NewModule.Parameter.Velocity;
				sStepCtrl.Step 						    := stepJOG_POSITIVE;
					
			ELSIF (M_NewModule.Command.Axis.JogPositive = TRUE) AND (M_NewModule.Interlock = TRUE) THEN
				SetInfoWarning(modINTERLOCK_ACTIVE, 'move jog positive is interlocked', ADR(M_NewModule.Info));
				
				// ----- jog into negative direction -----
			ELSIF (M_NewModule.Command.Axis.JogNegative = TRUE) AND (M_NewModule.Interlock = FALSE) THEN
				// prepare driver command to save one cycle	
				D_ExampleAxis.Command.JogNegative 	 := TRUE;
				D_ExampleAxis.Parameter.Acceleration := M_NewModule.Parameter.Acceleration;
				D_ExampleAxis.Parameter.Deceleration := M_NewModule.Parameter.Deceleration;
				D_ExampleAxis.Parameter.Velocity 	 := M_NewModule.Parameter.Velocity;
				sStepCtrl.Step 						 	 := stepJOG_NEGATIVE;
				
			ELSIF (M_NewModule.Command.Axis.JogNegative = TRUE) AND (M_NewModule.Interlock = TRUE) THEN
				SetInfoWarning(modINTERLOCK_ACTIVE, 'move jog negative is interlocked', ADR(M_NewModule.Info));
				
				// ----- axis in gear -----
			ELSIF (M_NewModule.Command.Axis.Gear = TRUE) AND (M_NewModule.Interlock = FALSE) THEN
				// prepare driver command to save one cycle	
				D_ExampleAxis.Command.Gear 		 	 		  := TRUE;
				D_ExampleAxis.Parameter.Gear.MasterAxisName := 'masterAxis';
				D_ExampleAxis.Parameter.Gear.Ratio   		  := M_NewModule.Parameter.Ratio;
				D_ExampleAxis.Parameter.Acceleration 		  := M_NewModule.Parameter.Acceleration;
				D_ExampleAxis.Parameter.Deceleration 		  := M_NewModule.Parameter.Deceleration;
				sStepCtrl.Step 			   			 		  := stepGEAR;
				
			ELSIF (M_NewModule.Command.Axis.Gear = TRUE) AND (M_NewModule.Interlock = TRUE) THEN
				SetInfoWarning(modINTERLOCK_ACTIVE, 'gear is interlocked', ADR(M_NewModule.Info));
				
				// ----- open gripper -----
			ELSIF (M_NewModule.Command.Cyl.Open = TRUE) AND (M_NewModule.Interlock = FALSE) THEN
				// prepare driver command to save one cycle			
				D_ExampleCylinder.Command.MoveWork := TRUE;
				sStepCtrl.Step 						  := stepOPEN_GRIPPER;
				
			ELSIF (M_NewModule.Command.Cyl.Open = TRUE) AND (M_NewModule.Interlock = TRUE) THEN
				SetInfoWarning(modINTERLOCK_ACTIVE, 'open gripper is interlocked', ADR(M_NewModule.Info));
				
				// ----- close gripper -----
			ELSIF (M_NewModule.Command.Cyl.Close = TRUE) AND (M_NewModule.Interlock = FALSE) THEN
				// prepare driver command to save one cycle		
				D_ExampleCylinder.Command.MoveHome := TRUE;
				sStepCtrl.Step 						  := stepCLOSE_GRIPPER;
				
			ELSIF (M_NewModule.Command.Cyl.Close = TRUE) AND (M_NewModule.Interlock = TRUE) THEN
				SetInfoWarning(modINTERLOCK_ACTIVE, 'close gripper is interlocked', ADR(M_NewModule.Info));
				
			END_IF			
			
			// ----- set module state information -----
			IF sStepCtrl.Step <> stepWAIT_FOR_COMMAND THEN
				SetInfo(dsBUSY, ADR(M_NewModule.Info));
			ELSE
				SetInfo(dsREADY, ADR(M_NewModule.Info));
			END_IF
		
			// ===== axis sequencer  =====
			
			// ----- switch drive on -----
		stepPOWER_ON:
			sStepCtrl.StepName := 'stepPOWER_ON';
			
			// wait until driver command is done or error
			IF (D_ExampleAxis.Info.Done = TRUE) AND (D_ExampleAxis.Status.PowerOn = TRUE) THEN
				SetInfo(dsDONE, ADR(M_NewModule.Info));
				sStepCtrl.Step := stepWAIT_FOR_COMMAND;		
				
			ELSIF (D_ExampleAxis.Info.Error = TRUE) THEN
				SetInfoError(D_ExampleAxis.Info.EventId, D_ExampleAxis.Info.EventText, ADR(M_NewModule.Info));
				sStepCtrl.Step := stepERROR;	
			END_IF
		
			// ----- switch drive off -----
		stepPOWER_OFF:
			sStepCtrl.StepName := 'stepPOWER_OFF';
			
			// wait until driver command is done or error
			IF (D_ExampleAxis.Info.Done = TRUE) AND (D_ExampleAxis.Status.PowerOn = FALSE) THEN
				SetInfo(dsDONE, ADR(M_NewModule.Info));
				sStepCtrl.Step := stepWAIT_FOR_COMMAND;	
				
			ELSIF (D_ExampleAxis.Info.Error = TRUE) THEN
				SetInfoError(D_ExampleAxis.Info.EventId, D_ExampleAxis.Info.EventText, ADR(M_NewModule.Info));
				sStepCtrl.Step := stepERROR;	
			END_IF 
		
			// ----- home drive -----
		stepHOME:
			sStepCtrl.StepName := 'stepHOME';	
			
			// wait until driver command is done or error
			IF (D_ExampleAxis.Info.Done = TRUE) AND (D_ExampleAxis.Status.IsHomed = TRUE) THEN
				SetInfo(dsDONE, ADR(M_NewModule.Info));
				sStepCtrl.Step := stepHOME_DONE;			
				
			ELSIF (D_ExampleAxis.Info.Error = TRUE) THEN
				SetInfoError(D_ExampleAxis.Info.EventId, D_ExampleAxis.Info.EventText, ADR(M_NewModule.Info));
				sStepCtrl.Step := stepERROR;	
			END_IF 
			
		stepHOME_DONE:
			sStepCtrl.StepName := 'stepSTOP_DONE';
			
			// wait until command is reset	
			IF (M_NewModule.Command.Axis.Home = FALSE) THEN
				D_ExampleAxis.Command.Home := FALSE;
				sStepCtrl.Step 				:= stepWAIT_FOR_COMMAND;
				
			ELSIF (D_ExampleAxis.Info.Error = TRUE) THEN
				SetInfoError(D_ExampleAxis.Info.EventId, D_ExampleAxis.Info.EventText, ADR(M_NewModule.Info));
				sStepCtrl.Step := stepERROR;	
			END_IF 	
			
			// ----- stop axis -----
		stepSTOP:
			sStepCtrl.StepName 						 := 'stepSTOP';

			// set driver commands and wait until it is done or error		
			IF (D_ExampleAxis.Info.Done = TRUE) AND (D_ExampleAxis.Status.IsStopped = TRUE) THEN
				SetInfo(dsDONE, ADR(M_NewModule.Info));
				sStepCtrl.Step := stepSTOP_DONE;	
				
			ELSIF (D_ExampleAxis.Info.Error = TRUE) THEN
				SetInfoError(D_ExampleAxis.Info.EventId, D_ExampleAxis.Info.EventText, ADR(M_NewModule.Info));
				sStepCtrl.Step := stepERROR;	
			END_IF 
		
		stepSTOP_DONE:
			sStepCtrl.StepName := 'stepSTOP_DONE';
			
			// wait until stop command is reset	
			IF (M_NewModule.Command.Axis.Stop = FALSE) THEN
				D_ExampleAxis.Command.Stop := FALSE;
				sStepCtrl.Step 				:= stepWAIT_FOR_COMMAND;
				
			ELSIF (D_ExampleAxis.Info.Error = TRUE) THEN
				SetInfoError(D_ExampleAxis.Info.EventId, D_ExampleAxis.Info.EventText, ADR(M_NewModule.Info));
				sStepCtrl.Step := stepERROR;	
			END_IF 
		
			// ----- move velocity -----
		stepMOVE_VELOCITY:
			sStepCtrl.StepName := 'stepMOVE_VELOCITY';		
			
			// set driver commands and wait until it is done or error
			IF (D_ExampleAxis.Info.Done = TRUE) THEN
				SetInfo(dsDONE, ADR(M_NewModule.Info));
				sStepCtrl.Step := stepMOVE_VELOCITY_DONE;	
				
			ELSIF (D_ExampleAxis.Info.Error = TRUE) THEN
				SetInfoError(D_ExampleAxis.Info.EventId, D_ExampleAxis.Info.EventText, ADR(M_NewModule.Info));
				sStepCtrl.Step := stepERROR;	
			END_IF 
		
		stepMOVE_VELOCITY_DONE:
			sStepCtrl.StepName := 'stepMOVE_VELOCITY_DONE';
			
			// wait until command is reset	
			IF (M_NewModule.Command.Axis.MovePositive = FALSE) AND (M_NewModule.Command.Axis.MoveNegative = FALSE) THEN
				D_ExampleAxis.Command.MoveVelocity := FALSE;
				sStepCtrl.Step 						  := stepWAIT_FOR_COMMAND;	
				
			ELSIF (D_ExampleAxis.Info.Error = TRUE) THEN
				SetInfoError(D_ExampleAxis.Info.EventId, D_ExampleAxis.Info.EventText, ADR(M_NewModule.Info));
				sStepCtrl.Step := stepERROR;	
			END_IF 
		
			// ----- move additive -----
		stepMOVE_ADDITIVE:
			sStepCtrl.StepName := 'stepMOVE_ADDITIVE';
		
			// set driver commands and wait until it is done or error
			IF (D_ExampleAxis.Info.Done = TRUE) THEN
				SetInfo(dsDONE, ADR(M_NewModule.Info));
				sStepCtrl.Step := stepMOVE_ADDITIVE_DONE;	
				
			ELSIF (D_ExampleAxis.Info.Error = TRUE) THEN
				SetInfoError(D_ExampleAxis.Info.EventId, D_ExampleAxis.Info.EventText, ADR(M_NewModule.Info));
				sStepCtrl.Step := stepERROR;	
			END_IF 
		
		stepMOVE_ADDITIVE_DONE:
			sStepCtrl.StepName := 'stepMOVE_ADDITIVE_DONE';
			
			// wait until command is reset	
			IF (M_NewModule.Command.Axis.MoveAdditive = FALSE) THEN
				D_ExampleAxis.Command.MoveAdditive := FALSE;
				sStepCtrl.Step 						  := stepWAIT_FOR_COMMAND;			
				
			ELSIF (D_ExampleAxis.Info.Error = TRUE) THEN
				SetInfoError(D_ExampleAxis.Info.EventId, D_ExampleAxis.Info.EventText, ADR(M_NewModule.Info));
				sStepCtrl.Step := stepERROR;	
			END_IF 
		
			// ----- move absolute -----
		stepMOVE_ABSOLUTE:
			sStepCtrl.StepName := 'stepMOVE_ABSOLUTE';
			
			// set driver commands and wait until it is done or error			
			IF (D_ExampleAxis.Info.Done = TRUE) THEN
				SetInfo(dsDONE, ADR(M_NewModule.Info));
				sStepCtrl.Step := stepMOVE_ABSOLUTE_DONE;		
				
			ELSIF (D_ExampleAxis.Info.Error = TRUE) THEN
				SetInfoError(D_ExampleAxis.Info.EventId, D_ExampleAxis.Info.EventText, ADR(M_NewModule.Info));
				sStepCtrl.Step := stepERROR;	
			END_IF 
		
		stepMOVE_ABSOLUTE_DONE:
			sStepCtrl.StepName := 'stepMOVE_ABSOLUTE_DONE';
			
			// wait until command is reset	
			IF (M_NewModule.Command.Axis.MoveAbsolute = FALSE) THEN
				D_ExampleAxis.Command.MoveAbsolute := FALSE;
				sStepCtrl.Step 					     := stepWAIT_FOR_COMMAND;		
				
			ELSIF (D_ExampleAxis.Info.Error = TRUE) THEN
				SetInfoError(D_ExampleAxis.Info.EventId, D_ExampleAxis.Info.EventText, ADR(M_NewModule.Info));
				sStepCtrl.Step := stepERROR;	
			END_IF 
			
			// ----- jog positive -----
		stepJOG_POSITIVE:
			sStepCtrl.StepName := 'stepJOG_POSITIVE';				

			// wait until jog command is reset
			IF (M_NewModule.Command.Axis.JogPositive = FALSE) THEN
				SetInfo(dsDONE, ADR(M_NewModule.Info));
				D_ExampleAxis.Command.JogPositive := FALSE;
				sStepCtrl.Step 						 := stepWAIT_FOR_COMMAND;
				
			ELSIF (D_ExampleAxis.Info.Error = TRUE) THEN
				SetInfoError(D_ExampleAxis.Info.EventId, D_ExampleAxis.Info.EventText, ADR(M_NewModule.Info));
				sStepCtrl.Step := stepERROR;	
			END_IF 
		
			// ----- jog negative -----
		stepJOG_NEGATIVE:
			sStepCtrl.StepName := 'stepJOG_NEGATIVE';

			// wait until jog command is reset	
			IF (M_NewModule.Command.Axis.JogNegative = FALSE) THEN
				SetInfo(dsDONE, ADR(M_NewModule.Info));
				D_ExampleAxis.Command.JogNegative := FALSE;
				sStepCtrl.Step 						 := stepWAIT_FOR_COMMAND;
				
			ELSIF (D_ExampleAxis.Info.Error = TRUE) THEN
				SetInfoError(D_ExampleAxis.Info.EventId, D_ExampleAxis.Info.EventText, ADR(M_NewModule.Info));
				sStepCtrl.Step := stepERROR;	
			END_IF
		
			// ----- gear -----
		stepGEAR:
			sStepCtrl.StepName 								  := 'stepGEAR';
	
			// set driver commands and wait until it is done or error
			IF (D_ExampleAxis.Info.Done = TRUE) THEN
				SetInfo(dsDONE, ADR(M_NewModule.Info));
				sStepCtrl.Step := stepGEAR_DONE;		
				
			ELSIF (D_ExampleAxis.Info.Error = TRUE) THEN
				SetInfoError(D_ExampleAxis.Info.EventId, D_ExampleAxis.Info.EventText, ADR(M_NewModule.Info));
				sStepCtrl.Step := stepERROR;	
			END_IF 
		
		stepGEAR_DONE:
			sStepCtrl.StepName := 'stepGEAR_DONE';
			
			// wait until command is reset			
			IF (M_NewModule.Command.Axis.Gear = FALSE) THEN
				D_ExampleAxis.Command.Gear := FALSE;
				sStepCtrl.Step 				:= stepWAIT_FOR_COMMAND;	
				
			ELSIF (D_ExampleAxis.Info.Error = TRUE) THEN
				SetInfoError(D_ExampleAxis.Info.EventId, D_ExampleAxis.Info.EventText, ADR(M_NewModule.Info));
				sStepCtrl.Step := stepERROR;	
			END_IF 
		
			// ===== gripper sequncer =====
			
			// ----- open gripper -----
		stepOPEN_GRIPPER:
			sStepCtrl.StepName 						:= 'stepOPEN_GRIPPER';
			D_ExampleCylinder.Parameter.Timeout := t#2s;
			
			// set driver commands and wait until it is done or error
			IF (D_ExampleCylinder.Info.Done = TRUE) THEN
				SetInfo(dsDONE, ADR(M_NewModule.Info));
				sStepCtrl.Step := stepOPEN_GRIPPER_DONE;		
				
			ELSIF (D_ExampleCylinder.Info.Error = TRUE) THEN
				SetInfoError(D_ExampleCylinder.Info.EventId, D_ExampleCylinder.Info.EventText, ADR(M_NewModule.Info));
				sStepCtrl.Step := stepERROR;	
			END_IF 
		
		stepOPEN_GRIPPER_DONE:
			sStepCtrl.StepName := 'stepOPEN_GRIPPER_DONE';
			
			// wait until command is reset			
			IF (M_NewModule.Command.Cyl.Open = FALSE) THEN
				D_ExampleCylinder.Command.MoveWork := FALSE;
				sStepCtrl.Step 						  := stepWAIT_FOR_COMMAND;	
				
			ELSIF (D_ExampleCylinder.Info.Error = TRUE) THEN
				SetInfoError(D_ExampleCylinder.Info.EventId, D_ExampleCylinder.Info.EventText, ADR(M_NewModule.Info));
				sStepCtrl.Step := stepERROR;	
			END_IF 
			
			// ----- close gripper -----
		stepCLOSE_GRIPPER:
			sStepCtrl.StepName 							 := 'stepCLOSE_GRIPPER';
			D_ExampleCylinder.Parameter.TimeHomePos := t#3s;
			
			// set driver commands and wait until it is done or error
			IF (D_ExampleCylinder.Info.Done = TRUE) THEN
				SetInfo(dsDONE, ADR(M_NewModule.Info));
				sStepCtrl.Step := stepCLOSE_GRIPPER_DONE;		
				
			ELSIF (D_ExampleCylinder.Info.Error = TRUE) THEN
				SetInfoError(D_ExampleCylinder.Info.EventId, D_ExampleCylinder.Info.EventText, ADR(M_NewModule.Info));
				sStepCtrl.Step := stepERROR;	
			END_IF 
		
		stepCLOSE_GRIPPER_DONE:
			sStepCtrl.StepName := 'stepCLOSE_GRIPPER_DONE';
			
			// wait until command is reset		
			IF (M_NewModule.Command.Cyl.Open = FALSE) THEN
				D_ExampleCylinder.Command.MoveHome := FALSE;
				sStepCtrl.Step 						  := stepWAIT_FOR_COMMAND;	
				
			ELSIF (D_ExampleCylinder.Info.Error = TRUE) THEN
				SetInfoError(D_ExampleCylinder.Info.EventId, D_ExampleCylinder.Info.EventText, ADR(M_NewModule.Info));
				sStepCtrl.Step := stepERROR;	
			END_IF 
			
			// ===== error handling =====
			
		stepERROR:
			sStepCtrl.StepName := 'stepERROR';
			
			// Wait for reset	
			IF (M_NewModule.Command.Reset = TRUE) THEN
				// set reset for all drivers;
				D_ExampleAxis.Command.Reset 	  := TRUE;
				D_ExampleCylinder.Command.Reset := TRUE;
				sStepCtrl.Step 				 	  := stepERROR_RESET;
			END_IF
				
		stepERROR_RESET:
			sStepCtrl.StepName 			:= 'stepERROR_RESET';
			M_NewModule.Info.EventId   := modSTATE_OK;
			M_NewModule.Info.EventText := 'STATE_OK';
			
			// wait until drivers have no error
			IF (M_NewModule.Command.Reset = FALSE) AND (M_NewModule.Info.InitOk = TRUE) THEN
				D_ExampleAxis.Command.Reset 	  := FALSE;
				D_ExampleCylinder.Command.Reset := FALSE;
				sStepCtrl.Step 					  := stepWAIT_FOR_COMMAND;
				
			ELSIF (M_NewModule.Command.Reset = FALSE) AND (M_NewModule.Info.InitOk = FALSE) THEN
				sStepCtrl.Step := stepNO_INIT;
			END_IF
		
	END_CASE
	
	// ===== log exit step =====
	
	StepMonitor(pStepCtrl := ADR(sStepCtrl), Update := TRUE);
	
	
	// ===========================================================
	// 	driver warning
	// ===========================================================
	
	
	IF (D_ExampleAxis.Info.Warning = TRUE) THEN
		SetInfoWarning(D_ExampleAxis.Info.EventId, D_ExampleAxis.Info.EventText, ADR(M_NewModule.Info));
	
	ELSIF (D_ExampleCylinder.Info.Warning = TRUE) THEN
		SetInfoWarning(D_ExampleCylinder.Info.EventId, D_ExampleCylinder.Info.EventText, ADR(M_NewModule.Info));
	END_IF
	
	
	// ===========================================================
	// 	function block calls 
	// ===========================================================
	
	
//	gAxExample; // dummy call for compiler
	D_ExampleAxis();
	D_ExampleCylinder();

	
	// ===========================================================
	// 	update of module status
	// ===========================================================
	
	
	// ===== axis status =====
	
	M_NewModule.Status.Axis.Position   	 := D_ExampleAxis.Status.Position;
	M_NewModule.Status.Axis.Velocity   	 := D_ExampleAxis.Status.Velocity;
	M_NewModule.Status.Axis.PowerOn 	  	 := D_ExampleAxis.Status.PowerOn;
	M_NewModule.Status.Axis.IsHomed 	  	 := D_ExampleAxis.Status.IsHomed;
	M_NewModule.Status.Axis.IsMoving   	 := D_ExampleAxis.Status.IsMoving;
	M_NewModule.Status.Axis.InStandstill := D_ExampleAxis.Status.InStandstill;
	
	// ===== gripper status =====
	
	M_NewModule.Status.Cyl.IsOpened := D_ExampleCylinder.Status.IsWork;
	M_NewModule.Status.Cyl.IsClosed := D_ExampleCylinder.Status.IsHome;
	
	 
END_PROGRAM

PROGRAM _EXIT

	// ===== call driver to close all communication channels to mapp configuration =====

	D_ExampleAxis.Configuration.Available := FALSE;
	D_ExampleAxis();
	
	 
END_PROGRAM

