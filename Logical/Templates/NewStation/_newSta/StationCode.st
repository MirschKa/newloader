// ===================================================================
//                        COPYRIGHT -- GEA GROUP 
// ===================================================================
// Program: _newSta\StationCode.st
// Descrition: 
//
// >>>===================== git information ==========================<<<

FUNCTION_BLOCK newSta_Status
	IF StationLink.pCtrl <> 0 THEN
		rStation ACCESS StationLink.pCtrl;
		Status := rStation.Status;
		Info := rStation.Info;
		DataValid := TRUE;
	ELSE
		brsmemset(ADR(Status), 0, SIZEOF(Status));
		DataValid := FALSE;
	END_IF   
END_FUNCTION_BLOCK


FUNCTION_BLOCK newSta_Master 
	IF StationLink.pCtrl <> 0 THEN
		rStation ACCESS StationLink.pCtrl;
		rStation.Command := Command;
		rStation.Recipe := Recipe;
		rStation.Config := Config;
		Status := rStation.Status;
		Info := rStation.Info;
		DataValid := TRUE;
	ELSE
		brsmemset(ADR(Status), 0, SIZEOF(Status));
		DataValid := FALSE;
	END_IF 
   
END_FUNCTION_BLOCK