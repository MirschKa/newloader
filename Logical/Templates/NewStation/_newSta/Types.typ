
TYPE
	newSta_Typ : 	STRUCT 
		Config : newStaCfg_Typ;
		Command : newStaCmd_Typ;
		Status : newStaStatus_Typ;
		Recipe : newStaRec_Typ;
		Info : StationInfo_Typ;
		Parameter : newStaPar_Typ;
	END_STRUCT;
	newStaCfg_Typ : 	STRUCT 
		New_Member : USINT;
	END_STRUCT;
	newStaRec_Typ : 	STRUCT 
		New_Member : USINT;
	END_STRUCT;
	newStaStatus_Typ : 	STRUCT 
		Running : BOOL;
		Stopped : BOOL;
		Paused : BOOL;
	END_STRUCT;
	newStaCmd_Typ : 	STRUCT 
		Start : BOOL;
		Stop : BOOL;
		Pause : BOOL;
	END_STRUCT;
	newStaPar_Typ : 	STRUCT 
		New_Member : USINT;
	END_STRUCT;
END_TYPE
