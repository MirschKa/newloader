
{REDUND_ERROR} FUNCTION_BLOCK newSta_Status (*TODO: Add your comment here*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_OUTPUT
		Info : StationInfo_Typ;
		Status : newStaStatus_Typ;
		DataValid : BOOL;
	END_VAR
	VAR_IN_OUT
		StationLink : StationLink_Typ;
	END_VAR
	VAR
		rStation : REFERENCE TO newSta_Typ;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK newSta_Master
	VAR_INPUT
		Config : newStaCfg_Typ;
		Command : newStaCmd_Typ;
		Recipe : newStaRec_Typ;
		Parameter : newStaPar_Typ;
	END_VAR
	VAR_OUTPUT
		Info : StationInfo_Typ;
		Status : newStaStatus_Typ;
		DataValid : BOOL;
	END_VAR
	VAR_IN_OUT
		StationLink : StationLink_Typ;
	END_VAR
	VAR
		rStation : REFERENCE TO newSta_Typ;
	END_VAR
END_FUNCTION_BLOCK
