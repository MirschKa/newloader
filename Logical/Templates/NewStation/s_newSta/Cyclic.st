// ===================================================================
//                        COPYRIGHT -- GEA GROUP 
// ===================================================================
// Program: s_newSta\Cyclic.st    
// Descrition: 
//
// >>>===================== git information ==========================<<<

PROGRAM _CYCLIC

	// synchronize module 
	#warning 'TODO: add modules'
//	 eg. M_Module(pMe := ADR(Me), Instance := 'M_Module');
	
 
	
	// ==========================================================
	//	Call shared functions for all stations
	// ==========================================================
	
	
	Action_StationShared;
   
	
	// ==========================================================
	// copy data from common function to ctrl struct
	// ==========================================================
	
	
	#warning TODO: station name ersetzen
	s_station.Info.ActiveState 	:= ModeStateClient.ActiveState;
	s_station.Info.MachineMode 	:= ModeStateClient.MachineMode;
	s_station.Info.OperatingMode 	:= ModeStateClient.OperationMode;
	s_station.Info.StateComplete 	:= ModeStateClient.StateComplete;
	
	
	// ==========================================================
	// synchronize module 
	// ==========================================================
	
	
	#warning TODO: add modules
	
	// ===== m_module1 =====
	
	m_module1.pMe			:= ADR(Me);
	m_module1.Instance	:= 'm_module1';
	m_module1();
	
	// ===== m_module2 =====
	
	m_module2.pMe			:= ADR(Me);
	m_module2.Instance	:= 'm_module2';
	m_module2();	
	
	// ===== m_module3 =====
	
	m_module3.pMe			:= ADR(Me);
	m_module3.Instance	:= 'm_module3';
	m_module3();	
	

	// ==========================================================
	//	   station configuration
	// ==========================================================
	
	
	// ===== modules =====
		
	#warning TODO : make modules available
	IF (s_station.Configuration.Available = TRUE) THEN
		m_module1.Configuration.Available := TRUE;
		m_module2.Configuration.Available := TRUE;
		m_module3.Configuration.Available := TRUE;
	END_IF
		
	// ===== clear all module status and info =====
	
	IF (s_station.Configuration.Available = FALSE) THEN

		m_module1.Configuration.Available := FALSE;
		m_module2.Configuration.Available := FALSE;
		m_module3.Configuration.Available := FALSE;
		
		// ----- module info -----
		s_station.Info.InitOk	:= FALSE;
		s_station.Info.Busy 		:= FALSE;
		s_station.Info.Done 		:= FALSE;
		s_station.Info.Warning	:= FALSE;
		s_station.Info.Error 	:= FALSE;
		s_station.Info.EventId  := staNOT_AVAILABLE;
		s_station.Info.EventText := 'NOT_AVAILABLE';
		sStepCtrlMain.StepName 	:= 'stepNO_STEP';
		RETURN;
	END_IF
	
	
	// ==========================================================
	//		Mode evaluation
	// ==========================================================
	
	
	// ===== commands with priority =====
	
	// ===== evaluation =====

	// ----- start automatic run -----
	IF (s_station.Info.MachineMode = msPRODUCTION) AND (s_station.Info.OperatingMode = msAUTO) THEN
		// log enter step
		StepMonitor(pStepCtrl := ADR(sStepCtrlMain), Update := FALSE);				
		
		// call automatic action
		ActionAutomatic;	
		
		// log exit step
		StepMonitor(pStepCtrl := ADR(sStepCtrlMain), Update := TRUE);
		
	ELSIF (s_station.Info.MachineMode = msPRODUCTION) AND (s_station.Info.OperatingMode = msMANUAL) THEN
		// call action MANUAL
		ActionManual;
	
	ELSIF (s_station.Info.MachineMode = msCLEANING) THEN
		// call action CLEANING
		ActionCleaning;
	END_IF

	
	// ===========================================================
	// 	Error handling
	// ===========================================================
	
	
	IF (m_module1.Info.Error = TRUE) THEN
		SetInfoError(m_module1.Info.EventId, m_module1.Info.EventText, ADR(s_station.Info));
		
	ELSIF (m_module2.Info.Error = TRUE) THEN
		SetInfoError(m_module2.Info.EventId, m_module2.Info.EventText, ADR(s_station.Info));
		
	ELSIF (m_module3.Info.Error = TRUE) THEN
		SetInfoError(m_module3.Info.EventId, m_module3.Info.EventText, ADR(s_station.Info));
	END_IF
	
		
	// ===========================================================
	// 	Warning handling
	// ===========================================================


	IF (m_module1.Info.Warning = TRUE) THEN
		SetInfoWarning(m_module1.Info.EventId, m_module1.Info.EventText, ADR(s_station.Info));
		
	ELSIF (m_module2.Info.Warning = TRUE) THEN
		SetInfoWarning(m_module2.Info.EventId, m_module2.Info.EventText, ADR(s_station.Info));
		
	ELSIF (m_module3.Info.Warning = TRUE) THEN
		SetInfoWarning(m_module3.Info.EventId, m_module3.Info.EventText, ADR(s_station.Info));
	END_IF
	
	
	// ===========================================================
	// 	update of station status
	// ===========================================================
	
//	s_portioningsystem(StationLink := gSlicer.Portioningsystem);
//	
//	s_cuttinghead(StationLink := gSlicer.Cuttinghead);
	
	
END_PROGRAM

