// ===================================================================
//                        COPYRIGHT -- GEA GROUP 
// ===================================================================
// Program: s_newSta\InitExit.st    
// Descrition: 
//
// >>>===================== git information ==========================<<<

PROGRAM _INIT
	// ==========================================
	// Register Station - enter specific data
	// ==========================================
	#error TODO: Enter station specific data
	RegisterStation.CtrlName := '';
	RegisterStation.pMachineLink := ADR( 0 );
END_PROGRAM

PROGRAM _EXIT
	#error TODO: Enter station specific data
	UnregisterStation(pMachineLink := ADR( 0 ));
END_PROGRAM