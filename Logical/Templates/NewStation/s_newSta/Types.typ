
TYPE
	LocalStepCtrl_Typ : 	STRUCT  (*local step control*)
		Step : FeedingStep_Enum;
		StepName : STRING[80];
		EnterNewStep : BOOL;
		PreviousStep : FeedingStep_Enum;
	END_STRUCT;
	newStaEvent_Enum : 
		( (*local event ids*)
		staSTATE_OK, (*no feedback fault*)
		staNOT_INITIALIZED, (*not initialized fault*)
		staNOT_AVAILABLE, (*not available fault*)
		staCONFIGURATION_FAULT, (*fault in configuration*)
		staSEQUENCER_ERROR, (*Error occured while sequencer was working*)
		staINIT_ERROR, (*fault while initialization*)
		staAUTOMATIC_ERROR, (*faule while automatic run*)
		staINTERLOCK_ACTIVE, (*interlock is active fault*)
		staPRODUCT_DETECTION, (*product detection event active*)
		staPRODUCT_FOUND_FIRST_RUN, (*Product was detected while first run*)
		staPRODUCT_DETECTED_KSF (*Product was detected before KSF was done*)
		);
	newStaStep_Enum : 
		(
		stepNO_STEP,
		stepIDLE,
		stepSTEP_1,
		stepSTEP_2
		);
END_TYPE
