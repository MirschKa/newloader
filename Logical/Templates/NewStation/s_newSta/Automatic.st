// ===================================================================
//                        COPYRIGHT -- GEA GROUP 
// ===================================================================
// Program: s_newSta\Automatic.st    
// Descrition: 
//
// >>>===================== git information ==========================<<<

ACTION ActionAutomatic: 
	CASE s_station.Info.ActiveState OF
		msSTOPPED:
			;
			// wahrscheinlich nichts machen
			
		msPREPARING, msREADY:
			// in this state should be the referende run
			// when reference run is done, send StateComplete to Master
			IF (m_module1.Info.InitOk = FALSE) THEN
				m_module1.Command.Init := TRUE;
			END_IF
			
			IF (m_module2.Info.InitOk = FALSE) THEN
				m_module2.Command.Init := TRUE;
			END_IF
						
			IF (ModeStateClient.TransientState = TRUE) AND (m_module2.Info.InitOk = TRUE) AND (m_module1.Info.InitOk = TRUE) THEN
				m_module1.Command.Init := FALSE;
				m_module2.Command.Init := FALSE;
				
				ModeStateClient.StateComplete := TRUE;
			END_IF
			
		
		msRUNNING, msHOLDING, msHELD, msRESTARTING:
			CASE sStepCtrl.Step OF
				stepIDLE:
					sStepCtrl.StepName := 'stepIDLE';
			
					IF (s_station.Info.ActiveState = msREADY) THEN
						sStepCtrl.Step := stepPRODUCT_DETECTION;
				
					ELSIF (s_station.Info.Error = TRUE) THEN
						;	// stay here for diagnostics
						//				SetInfoError(modINIT_ERROR, 'Driver Entrybelt init error', ADR(M_m_module1.Info));
					END_IF
		
				stepSTEP_1:
					sStepCtrl.StepName := 'stepSTEP_1';

					m_module2.Parameter.Acceleration	:= 100.0;
					m_module2.Parameter.Deceleration	:= 100.0;
					m_module2.Parameter.Velocity 		:= 10.0;
					m_module2.Command.Axis.Gear 		:= TRUE;
			
					IF (m_module2.Info.Done = TRUE) THEN
						sStepCtrl.Step := stepSTEP_2;
								
						m_module2.Command.Axis.Gear := FALSE;
						
					ELSIF (s_station.Info.Error = TRUE) THEN
						;	// stay here for diagnostics
						//				SetInfoError(modINIT_ERROR, 'Driver Entrybelt init error', ADR(M_m_module1.Info));
					END_IF
		
				stepSTEP_2:
					sStepCtrl.StepName := 'stepSTEP_2';

					m_module1.Parameter.Acceleration	:= 100.0;
					m_module1.Parameter.Deceleration	:= 100.0;
					m_module1.Parameter.Velocity 		:= 10.0;
					m_module1.Command.Axis.MovePositive := TRUE;
						
					IF (m_module1.Info.Done = TRUE) THEN
				
						m_module1.Command.Axis.MovePositive := FALSE;
						sStepCtrl.Step := stepPRODUCT_DETECTED;
			
					ELSIF (s_station.Info.Error = TRUE) THEN
						;	// stay here for diagnostics
						//				SetInfoError(modINIT_ERROR, 'Driver Entrybelt init error', ADR(M_m_module1.Info));
					END_IF
				
				stepSTEP_3:
					sStepCtrl.StepName := 'stepSTEP_3';
				
					TON_Timeout_Wait.IN := TRUE
				
					IF (TON_Timeout_Wait.Q = TRUE) THEN
						sStepCtrl.Step := stepSTEP_4;
					END_IF
			END_CASE
				
		msSTOPPING:
			// in this state the whole station is stopped immediatly
			// when stop is done, send StateComplete to Master
			m_module1.Command.Axis.Stop := TRUE;
			
			IF (m_module1.Info.Done = TRUE) THEN
				m_module1.Command.Axis.Stop := FALSE;
				
				ModeStateClient.StateComplete := TRUE;
			END_IF
						
				
		msABORTING:
			;
			// (evtl. ausserhalb Action)
					
		msABORTED:
			;
			// (evtl. ausserhalb Action)
					
		msRESETTING:
			;
		// Fehler ruecksetzen (evtl. ausserhalb Action)
			
	
	END_CASE
	
	// ===== timer =====
	
	// timer timeout wait 
	TON_Timeout_Wait.PT := t#5s;
	TON_Timeout_Wait();

END_ACTION
