
ACTION Action_StationShared :
	// ---------------------------------------------------------------------------------	
	// Input parameters of Register Station should be entered in _INIT Program
	// ---------------------------------------------------------------------------------
	RegisterStation(pMe := ADR(Me));	
	
	// ---------------------------------------------------------------------------------	
	// call ModeStateClient 
	// ---------------------------------------------------------------------------------
	ModeStateClient(pMe := ADR(Me));
	// reset state complete signal
	IF ModeStateClient.StateChanged THEN 
		ModeStateClient.StateComplete := FALSE;
	END_IF   
	

END_ACTION