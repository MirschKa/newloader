Option Explicit

'read user path
Dim MyPath

With WScript
  MyPath = Replace(.ScriptFullName&":", .ScriptName&":", "")
 End With
 
 '--------------------------------------------------------------------------------------
 Dim InsertStation
 Dim NewPath, ProgPath
 Dim FolderStat, FolderLib, FolderProg
  
 ' Useraction for insert new station name
InsertStation = InputBox("Enter new station:" ,"New Station")

If InsertStation <>"" Then 'no name insert --> abort

'Path for new station
NewPath = MyPath & InsertStation
ProgPath = Replace(MyPath, "Templates", "")

FolderStat = NewPath
FolderLib = NewPath & "\_" & LCase(InsertStation)
FolderProg = NewPath & "\" & "s_" & LCase(InsertStation)
	
' create new folder in user profile
CreateFolder FolderStat 
CreateFolder FolderLib 
CreateFolder FolderProg

'--------------------------------------------------------------------------------------
'Copy data from template "newSta"
Dim SourceFile
Dim DestinationFolder
Dim sFun, sVar, sLby, sSt, sTyp, sPrg, sPackage 'source
Dim dFun, dVar, dLby, dSt, dTyp, dPrg, dPackage 'destination
Dim fso

Set fso = CreateObject("Scripting.FileSystemObject")

sFun = MyPath & "NewStation\_newSta\*.fun"
sVar = MyPath & "NewStation\_newSta\*.var"
sLby = MyPath & "NewStation\_newSta\*.lby"
sSt = MyPath & "NewStation\_newSta\*.st"
sTyp = MyPath & "NewStation\_newSta\*.typ"
sPackage = MyPath & "NewStation\Package.pkg"

dFun = FolderLib & "\_" & LCase(InsertStation) & "*.fun"
dVar = FolderLib & "\_" & LCase(InsertStation) & "*.var"
dLby = FolderLib & "\_" & LCase(InsertStation) & "*.lby"
dSt = FolderLib & "\_" & LCase(InsertStation) & "*.st"
dTyp = FolderLib & "\_" & LCase(InsertStation) & "*.typ"
dPackage = NewPath & "\Package.pkg"

'copy package file
 fso.CopyFile sPackage, dPackage, True
 
If not fso.FileExists(dFun) Then
    fso.CopyFile sFun, FolderLib, True
End If
	
If not fso.FileExists(dVar) Then
    fso.CopyFile sVar, FolderLib, True
End If
	
If not fso.FileExists(dLby) Then
    fso.CopyFile sLby, FolderLib, True
End If
	
If not fso.FileExists(dSt) Then
    fso.CopyFile sSt, FolderLib, True
End If
	
If not fso.FileExists(dTyp) Then
    fso.CopyFile sTyp, FolderLib, True
End If
	
' rename file	
Dim file
Set file = fso.GetFile(FolderLib & "\" & "_newSta.fun")

file.Name = Replace(file.Name, "newSta", LCase(InsertStation))

'copy files form folder s_newSta
sPrg = MyPath & "NewStation\s_newSta\*.prg"
sVar = MyPath & "NewStation\s_newSta\*.var"
sSt = MyPath & "NewStation\s_newSta\*.st"

dVar = FolderProg & "\s_" & LCase(InsertStation) & "*.var"
dPrg = FolderProg & "\s_" & LCase(InsertStation) & "*.prg"
dSt = FolderProg & "\s_" & LCase(InsertStation) & "*.st"

If not fso.FileExists(dVar) Then
   fso.CopyFile sVar, FolderProg, True
End If

If not fso.FileExists(dSt) Then
    fso.CopyFile sSt, FolderProg, True
End If
	 
If not fso.FileExists(dprg) Then
    fso.CopyFile sPrg, FolderProg, True
End If
	 
'_newSta replace (.typ, .st, .fun, .pkg)
ReplaceString FolderLib & "\_" & LCase(InsertStation) & ".fun", "newSta", InsertStation 'elements and functionname
ReplaceString FolderLib & "\StationCode.st", "newSta", LCase(InsertStation) 'Header

ReplaceString FolderLib  & "\IEC.lby", "newSta", LCase(InsertStation)
ReplaceString FolderLib & "\Types.typ", "newSta", InsertStation
ReplaceString FolderLib & "\StationCode.st", "newSta", InsertStation

ReplaceString FolderProg & "\Cyclic.st", "//S_Portioning", "S_" & InsertStation
ReplaceString FolderProg & "\Cyclic.st", "newSta", LCase(InsertStation) 'Header
ReplaceString FolderProg & "\InitExit.st", "newSta", LCase(InsertStation) 'Header
ReplaceString FolderProg & "\Automatic.st", "newSta", LCase(InsertStation) 'Header
ReplaceString FolderProg & "\Manual.st", "newSta", LCase(InsertStation) 'Header
ReplaceString FolderProg & "\Cleaning.st", "newSta", LCase(InsertStation) 'Header

ReplaceString FolderProg & "\Cyclic.st", "#warning 'TODO: station name ersetzen '", "" 'delete line #warning

ReplaceString NewPath & "\Package.pkg", "new Station", InsertStation
ReplaceString NewPath & "\Package.pkg", "newSta", LCase(InsertStation)

' Shared station properties replace
ReplaceString FolderProg & "\IEC.prg", "<File Description=" & """Local variables""" & " Private=" & """true""" & ">StationShared.var</File>", "<File Description=" & """Local variables""" & " Private=" & """true""" &" Reference=" & """true""" & ">\Logical\Templates\NewStation\s_newSta\StationShared.var</File>"
ReplaceString FolderProg & "\IEC.prg", "<File Description=" & """Action for all stations""" & ">StationSharedCode.st</File>", "<File Description=" & """Action for all stations""" & " Reference=" & """true""" & ">\Logical\Templates\NewStation\s_newSta\StationSharedCode.st</File>"

InsertPackage MyPath & "\Package.pkg", InsertStation


'insert variable in new station
Dim Variable
Set fso = CreateObject("Scripting.FileSystemObject")
Set Variable = fso.OpenTextFile(FolderProg & "\Variables.var",2)
Variable.WriteLine "VAR"
Variable.WriteLine "S_" & InsertStation & " : " & InsertStation & "_Typ;"
Variable.WriteLine "END_VAR"
Variable.Close


'---------------------------------------------------------------------------------------------
' info for user
Dim Reminder
Reminder = "1. move folder in the right machine station" _
	   & vbCrLf & "2. insert program and library in cpu.sw" _
	   & vbCrLf & "3. insert new station to 'gMachine.typ'" _
	   & vbCrLf & "4. adjust the file 'InitExit.st'"
MsgBox Reminder, vbOKOnly, "Friendly Reminder"
'---------------------------------------------------------------------------------------------
End If
'End If


' ===== Help function to Insert Package with new Station =====
Sub InsertPackage (File, Insert)

Dim fso
Dim objFile
Dim Lines
Dim oNewFile
Dim oFSO1
Dim Comp

' open file --> read text --> close
Set fso = CreateObject("Scripting.FileSystemObject")
set objFile = fso.OpenTextFile(File,1)

Set oFSO1 = CreateObject("Scripting.FileSystemObject")
Set oNewFile = oFSO1.CreateTextFile(MyPath & "Package_neu.pkg", true)	

Do Until objFile.atEndOfStream 
	Lines = objFile.ReadLine
	oNewFile.WriteLine Lines
	Comp = StrComp(Lines, "  <Objects>", vbTextCompare)
	If Comp = 0 Then
		oNewFile.WriteLine "    <Object Type=" & """Package""" & ">" & Insert & "</Object>"
	End If
Loop

oNewFile.Close
objFile.Close

fso.CopyFile MyPath & "Package_neu.pkg", File, True
fso.DeleteFile MyPath & "Package_neu.pkg"

End Sub

' ===== Help function to Replace String =====
Sub ReplaceString (File, ToReplace, ReplaceBy)

Dim fso
Dim objFile
Dim Text
Dim NewText
Dim resultFile

Set fso = CreateObject("Scripting.FileSystemObject")
	
' open file --> read text --> close
set objFile = fso.OpenTextFile(File,1)
Text = objFile.ReadAll
objFile.Close

' change content
NewText = Replace(Text, ToReplace, ReplaceBy)
 
' create new file and insert changed content
set resultFile = fso.CreateTextFile(File, true)
resultFile.WriteLine NewText
resultFile.Close

End Sub

' ===== Help function to create folder with subfolders =====
Sub CreateFolder ( path )
	Dim fso, parentFolder
	Set fso = CreateObject("Scripting.FileSystemObject")
	
If fso.FolderExists( path ) = vbTrue Then
   fso.DeleteFolder ( path ), true
End If

If Not fso.FolderExists( path ) Then
		parentFolder = fso.GetParentFolderName( path )
		If Not fso.FolderExists( parentFolder ) Then 
			'recursive call
			CreateFolder parentFolder 
		Else
			fso.CreateFolder( path )
		End If
	End If
End Sub
	
























