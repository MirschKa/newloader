(************************************************************************************************************************)
(* Object name: CallDialog                                                                                              *)
(* Author:      Samuel Otto                                                                                             *)
(* Site:        B&R Bad Homburg                                                                                         *)
(* Created:     17-nov-2010                                                                                             *)
(* Restriction:                                                                                                         *)
(* Description: Function for hanging one dialog into dialogRingbuffer and waiting for response.                         *)
(*                                                                                                                      *)
(*----------------------------------------------------------------------------------------------------------------------*)
(* Additional information:                                                                                              *)
(*                                                                                                                      *)
(* No additional information available.                                                                                 *)
(*                                                                                                                      *)
(*----------------------------------------------------------------------------------------------------------------------*)
(* Version 3.00.0  18-jan-2012  Samuel Otto                     Development tool: B&R Automation Studio V3.0.90.19 SP01 *)
(*                                                                                                                      *)
(* Cleaned up this revision history and set version to V3.00.0                                                          *)
(*--------------------------------------------------------------------------------------------------------------------- *)
(************************************************************************************************************************)

FUNCTION CallDialog
	(* check for Nullpointer *)
	IF AdrDialogIdent = 0 OR AdrDialogInformation = 0 OR AdrDialogRingBuffer = 0 THEN
		CallDialog := TPL_ERR_NULLPOINTER;
		RETURN;
	END_IF

	
	
	
(*********************************************************************************************
 START FUNCTION
 *********************************************************************************************)
	pDialogIdent ACCESS(AdrDialogIdent);
	pDialogInformation ACCESS(AdrDialogInformation);
	pDialogRingBuffer ACCESS(AdrDialogRingBuffer);

	IF pDialogIdent = 0 THEN	// Hang the new dialog in the dialogringbuffer
		IF pDialogRingBuffer.LastToDrawDialog < (pDialogRingBuffer.RingBufferSize - 1) THEN// increment last element
			pDialogRingBuffer.LastToDrawDialog := pDialogRingBuffer.LastToDrawDialog + 1;
		ELSE
			pDialogRingBuffer.LastToDrawDialog := 1;
		END_IF
		pDialogIdent := pDialogRingBuffer.LastToDrawDialog;
		pDialogInformation.Return_PressedButton := 0;
		brsmemcpy(ADR(pDialogRingBuffer.DialogInformation[pDialogRingBuffer.LastToDrawDialog]), ADR(pDialogInformation), SIZEOF(pDialogRingBuffer.DialogInformation[0]));
	END_IF

	IF pDialogRingBuffer.DialogInformation[pDialogIdent].Return_PressedButton <> 0 THEN	// check if a button is pressed
		pDialogInformation.Return_PressedButton := pDialogRingBuffer.DialogInformation[pDialogIdent].Return_PressedButton;
		pDialogIdent := 0;
		CallDialog := 0;
		RETURN;
	ELSE // if no button ist pressed, the dialog is still shown and waits for an in put
		CallDialog := 500;
		RETURN;
	END_IF

	CallDialog := 0;
	RETURN;
END_FUNCTION
