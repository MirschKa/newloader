
TYPE
	dlgTxt_typ : 	STRUCT 
		txtHeader : ARRAY[0..99]OF UINT;
		txtBody : ARRAY[0..299]OF UINT;
		txtBtn1 : ARRAY[0..99]OF UINT;
		txtBtn2 : ARRAY[0..99]OF UINT;
		txtBtn3 : ARRAY[0..99]OF UINT;
		txtHeaderLen : UDINT;
		txtBodyLen : UDINT;
		txtBtn1Len : UDINT;
		txtBtn2Len : UDINT;
		txtBtn3Len : UDINT;
		txtBtn4Len : UDINT;
	END_STRUCT;
	digalogheader : 	STRUCT 
		Height : UINT;
		Color : UINT;
		BorderColor : UINT;
		Text : STRING[25];
		TextColor : UINT;
	END_STRUCT;
	dialogtextbox : 	STRUCT 
		Color : UINT;
		BorderColor : UINT;
		Text : STRING[150];
		TextColor : UINT;
	END_STRUCT;
	dialogbutton : 	STRUCT 
		NumberOfButton : USINT;
		Height : UINT;
		Width : UINT;
		ColorButtonOne : UINT;
		BorderColorButtonOne : UINT;
		ShadowColorButtonOne : UINT;
		TextButtonOne : STRING[8];
		TextColorOne : UINT;
		ColorButtonTwo : UINT;
		BorderColorButtonTwo : UINT;
		ShadowColorButtonTwo : UINT;
		TextButtonTwo : STRING[8];
		TextColorTwo : UINT;
		ColorButtonThree : UINT;
		BorderColorButtonThree : UINT;
		ShadowColorButtonThree : UINT;
		TextButtonThree : STRING[8];
		TextColorThree : UINT;
		ColorButtonFour : UINT;
		BorderColorButtonFour : UINT;
		ShadowColorButtonFour : UINT;
		TextButtonFour : STRING[8];
		TextColorFour : UINT;
	END_STRUCT;
	dialogcommand : 	STRUCT 
		DialogAlreadyDrawed : BOOL;
		PressedButton : USINT;
	END_STRUCT;
	dialogintern : 	STRUCT 
		AdrChangePicture : UDINT;
		AdrCurrentPicture : UDINT;
		LastPage : UINT;
		PageForDrawingDialog : UINT;
		Step : USINT;
		pBitmapData : UDINT;
		TextboxHeight : UINT;
		Move : USINT;
		StartMoveX : UINT;
		StartMoveY : UINT;
		ButtonOneXPosition : UINT;
		ButtonOneYPosition : UINT;
		ButtonTwoXPosition : UINT;
		ButtonTwoYPosition : UINT;
		ButtonThreeXPosition : UINT;
		ButtonThreeYPosition : UINT;
		ButtonFourXPosition : UINT;
		ButtonFourYPosition : UINT;
		OldXPosition : UINT;
		OldYPosition : UINT;
		WaitCounter : USINT;
	END_STRUCT;
	dialog : 	STRUCT 
		DisplayWidth : UINT;
		DisplayHeight : UINT;
		DisplayHeaderHeight : UINT;
		FontIndex : UINT;
		FontWidth : UINT;
		FontHeight : UINT;
		StartXCoordinates : UINT;
		StartYCoordinates : UINT;
		Width : UINT;
		Header : digalogheader;
		Textbox : dialogtextbox;
		Button : dialogbutton;
		Command : dialogcommand;
		Intern : dialogintern;
	END_STRUCT;
	dialoginfo : 	STRUCT 
		HeaderTextGroupNumber : UDINT;
		HeaderTextNumber : UINT;
		DialogTextGroupNumber : UDINT;
		DialogTextNumber : UINT;
		NumberOfButton : USINT;
		ButtonTextGroupNumber : UDINT;
		ButtonTextNumberOne : UINT;
		ButtonTextNumberTwo : UINT;
		ButtonTextNumberThree : UINT;
		Return_PressedButton : USINT;
		ProgressViewRequired : BOOL;
		AdrProgressValue : UDINT;
	END_STRUCT;
	dialogringbuffer : 	STRUCT 
		RingBufferSize : UINT;
		ActualDrawedDialog : UINT;
		LastToDrawDialog : UINT;
		DialogInformation : ARRAY[0..9]OF dialoginfo;
	END_STRUCT;
	dialogextern : 	STRUCT 
		DialogAvailable : BOOL;
		DialogIdent : UINT;
		HeaderTextGroupNumber : UDINT;
		HeaderTextNumber : UINT;
		DialogTextGroupNumber : UDINT;
		DialogTextNumber : UINT;
		NumberOfButton : USINT;
		ButtonTextGroupNumber : UDINT;
		ButtonTextNumberOne : UINT;
		ButtonTextNumberTwo : UINT;
		ButtonTextNumberThree : UINT;
		TPL_ReturnPressedButton : USINT;
		ProgressBarAvailable : BOOL;
		ProgressBarValue : USINT;
	END_STRUCT;
END_TYPE
