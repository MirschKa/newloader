(************************************************************************************************************************)
(* Object name: DrawDialog                                                                                              *)
(* Author:      Samuel Otto                                                                                             *)
(* Site:        B&R Bad Homburg                                                                                         *)
(* Created:     17-nov-2010                                                                                             *)
(* Restriction:                                                                                                         *)
(* Description: Function for getting Information from Text Groups for DialogHandling.                                   *)
(*                                                                                                                      *)
(*----------------------------------------------------------------------------------------------------------------------*)
(* Additional information:                                                                                              *)
(*                                                                                                                      *)
(* Note: There is no possibility to have errors in the dialoghandling. When other tasks call the dialoghandling, they   *)
(*       wait until the dialog is ready. So here we don't evalutate errors from VA_wcGetTextByTextGroup. We only wait   *)
(*       until status is no more busy (7000). Then we try to read the next text.                                        *)
(*                                                                                                                      *)
(*----------------------------------------------------------------------------------------------------------------------*)
(* Version 3.00.0  18-jan-2012  Samuel Otto                     Development tool: B&R Automation Studio V3.0.90.19 SP01 *)
(*                                                                                                                      *)
(* Cleaned up this revision history and set version to V3.00.0                                                          *)
(*----------------------------------------------------------------------------------------------------------------------*)
(* Version 3.01.0  19-sep-2012  FTG                             Development tool: B&R Automation Studio V3.0.90.21 SP03 *)
(* - new input to function "UseShared (BOOL)", if set that set bit 31 in text group numbers to use shared res.          *)
(*--------------------------------------------------------------------------------------------------------------------- *)
(************************************************************************************************************************)

FUNCTION DrawDialog
	(* check for Nullpointer *)
	IF AdrDialogInfo = 0 OR AdrTxt = 0 OR AdrStep = 0 THEN
		DrawDialog := TPL_ERR_NULLPOINTER;
		RETURN;
	END_IF
	
	(* VC_Handle ok?*)
	IF VC_Handle = 0 THEN
		DrawDialog := TPL_ERR_VC_HANDLE;
		RETURN;
	END_IF

(*********************************************************************************************
 START FUNCTION
 *********************************************************************************************)
	pDlgTxt ACCESS(AdrTxt);	(* AdrTxt has to be a pointer to a dlgTxt_typ -structure! *)
	pStep ACCESS(AdrStep);
	pDialogParameter ACCESS(AdrDialogInfo);	(* Important: AdrDialogInfo has to be a pointer to a struct of type dialoginfo *)

	DrawDialog := VA_Saccess(1,VC_Handle);
	IF DrawDialog <> 0 THEN					(* IF VA_Saccess fails, no VA_Srelease is necessary *)
		RETURN;
	END_IF
	(* Important: Here we have a valid VA_Saccess. Don't forget the VA_Srelease!!!*)

	SharedTxtGrpOffs := 0;
	IF UseShared THEN
		SharedTxtGrpOffs.31 := TRUE;
	END_IF
	CASE pStep OF
		0:	(* clear all strings *)
			brsmemset(ADR(pDlgTxt.txtHeader)	,0,SIZEOF(pDlgTxt.txtHeader));
			brsmemset(ADR(pDlgTxt.txtBody)		,0,SIZEOF(pDlgTxt.txtBody));
			brsmemset(ADR(pDlgTxt.txtBtn1)		,0,SIZEOF(pDlgTxt.txtBtn1));
			brsmemset(ADR(pDlgTxt.txtBtn2)		,0,SIZEOF(pDlgTxt.txtBtn2));
			brsmemset(ADR(pDlgTxt.txtBtn3)		,0,SIZEOF(pDlgTxt.txtBtn3));
			pStep := 1;
			
		1:	(* Read Header Text *)
			pDlgTxt.txtHeaderLen := SIZEOF(pDlgTxt.txtHeader);
			DrawDialog := VA_wcGetTextByTextGroup(1,VC_Handle, pDialogParameter.HeaderTextGroupNumber OR SharedTxtGrpOffs, pDialogParameter.HeaderTextNumber, ADR(pDlgTxt.txtHeader), ADR(pDlgTxt.txtHeaderLen));
			IF DrawDialog <> 7000 THEN
				pStep := 2;
			END_IF

		2:	(* Read Body Text *)
			pDlgTxt.txtBodyLen := SIZEOF(pDlgTxt.txtBody);
			DrawDialog := VA_wcGetTextByTextGroup(1,VC_Handle, pDialogParameter.DialogTextGroupNumber OR SharedTxtGrpOffs, pDialogParameter.DialogTextNumber, ADR(pDlgTxt.txtBody), ADR(pDlgTxt.txtBodyLen));
			IF DrawDialog <> 7000 THEN
				pStep := 3;
			END_IF
		
		3:	(* Check if buttons should be displayed on display *)
			IF pDialogParameter.NumberOfButton <> 0 THEN
				(* Read Button Text1 *)
				pDlgTxt.txtBtn1Len := SIZEOF(pDlgTxt.txtBtn1);
				DrawDialog := VA_wcGetTextByTextGroup(1,VC_Handle, pDialogParameter.ButtonTextGroupNumber OR SharedTxtGrpOffs, pDialogParameter.ButtonTextNumberOne, ADR(pDlgTxt.txtBtn1), ADR(pDlgTxt.txtBtn1Len));
				IF DrawDialog <> 7000 THEN
					pStep := 4;
				END_IF
			ELSE	(* No Buttons necessary *)
				pStep := 10;	(* ready *)
			END_IF
		4:	(* Read Button Text2 *)
			pDlgTxt.txtBtn2Len := SIZEOF(pDlgTxt.txtBtn2);
			DrawDialog := VA_wcGetTextByTextGroup(1,VC_Handle, pDialogParameter.ButtonTextGroupNumber OR SharedTxtGrpOffs, pDialogParameter.ButtonTextNumberTwo, ADR(pDlgTxt.txtBtn2), ADR(pDlgTxt.txtBtn2Len));
			IF DrawDialog <> 7000 THEN
				pStep := 5;
			END_IF

		5:	(* Read Button Text3 *)
			pDlgTxt.txtBtn3Len := SIZEOF(pDlgTxt.txtBtn3);
			DrawDialog := VA_wcGetTextByTextGroup(1,VC_Handle, pDialogParameter.ButtonTextGroupNumber OR SharedTxtGrpOffs, pDialogParameter.ButtonTextNumberThree, ADR(pDlgTxt.txtBtn3), ADR(pDlgTxt.txtBtn3Len));
			IF DrawDialog <> 7000 THEN
				pStep := 10;	(* ready *)
			END_IF

		10:	(* ready, all texts read successfully *)
				(* status changes outside the CASE *)
	END_CASE

	DrawDialog := DrawDialog OR VA_Srelease(1,VC_Handle);
	
	IF DrawDialog = 0 AND pStep <> 10 THEN	(* There are no errors from Visapi-functions, but DrawDialog is not ready. Show status busy *)
		DrawDialog := 16#FFFF;	(* ERR_FUB_BUSY *)
	ELSIF pStep = 10 THEN
		DrawDialog := 0;	(* If also the last VISAPI-function fails (e.g. status 7090) we must end the call of DrawDialog. *)
		pStep := 0;
	END_IF


END_FUNCTION
