
FUNCTION ExtDialogIF
	
	// Get acces to Ring buffer
	pDialogRingBuffer ACCESS(AdrDialogRingBuffer); 
	
	IF mode = 0 THEN

		// reset struct
		brsmemset(ADR(pDialogInformation),0,SIZEOF(pDialogInformation));
		// get all info
		pDialogInformation.HeaderTextGroupNumber 	:= pDialogRingBuffer.DialogInformation[pDialogRingBuffer.ActualDrawedDialog].HeaderTextGroupNumber; 
		pDialogInformation.HeaderTextNumber 		:= pDialogRingBuffer.DialogInformation[pDialogRingBuffer.ActualDrawedDialog].HeaderTextNumber;
		pDialogInformation.DialogTextGroupNumber 	:= pDialogRingBuffer.DialogInformation[pDialogRingBuffer.ActualDrawedDialog].DialogTextGroupNumber;
		pDialogInformation.DialogTextNumber 		:= pDialogRingBuffer.DialogInformation[pDialogRingBuffer.ActualDrawedDialog].DialogTextNumber;
		pDialogInformation.NumberOfButton 			:= pDialogRingBuffer.DialogInformation[pDialogRingBuffer.ActualDrawedDialog].NumberOfButton;
		pDialogInformation.ButtonTextGroupNumber 	:= pDialogRingBuffer.DialogInformation[pDialogRingBuffer.ActualDrawedDialog].ButtonTextGroupNumber;
		pDialogInformation.ButtonTextNumberOne 		:= pDialogRingBuffer.DialogInformation[pDialogRingBuffer.ActualDrawedDialog].ButtonTextNumberOne;
		pDialogInformation.ButtonTextNumberTwo 		:= pDialogRingBuffer.DialogInformation[pDialogRingBuffer.ActualDrawedDialog].ButtonTextNumberTwo;
		pDialogInformation.ButtonTextNumberThree 	:= pDialogRingBuffer.DialogInformation[pDialogRingBuffer.ActualDrawedDialog].ButtonTextNumberThree;
		pDialogInformation.DialogAvailable			:= TRUE;
		pDialogInformation.DialogIdent				:= pDialogRingBuffer.LastToDrawDialog;
		
		// Familiar code as used in the Dialogh.st to provide the percentage value directly to upper systems
		IF pDialogRingBuffer.DialogInformation[pDialogRingBuffer.ActualDrawedDialog].ProgressViewRequired = TRUE AND pDialogRingBuffer.DialogInformation[pDialogRingBuffer.ActualDrawedDialog].AdrProgressValue <> 0 THEN
			pDialogInformation.ProgressBarAvailable := TRUE;
			ProgressBarValue ACCESS pDialogRingBuffer.DialogInformation[pDialogRingBuffer.ActualDrawedDialog].AdrProgressValue;
		ELSE 
			pDialogInformation.ProgressBarAvailable := FALSE;
			ProgressBarValue ACCESS ADR(PROGRESS_VIEW_ZERO);
		END_IF
		pDialogInformation.ProgressBarValue := ProgressBarValue;
		
	ELSIF mode = 1 THEN
		// reset struct
		brsmemset(ADR(pDialogInformation),0,SIZEOF(pDialogInformation));
		// get ident info
		pDialogInformation.DialogIdent				:= pDialogRingBuffer.LastToDrawDialog;
		// get info from pressed button
		pDialogInformation.TPL_ReturnPressedButton 	:= 	pDialogRingBuffer.DialogInformation[pDialogRingBuffer.ActualDrawedDialog].Return_PressedButton;
	END_IF
	
	ExtDialogIF := 0;
	
END_FUNCTION
