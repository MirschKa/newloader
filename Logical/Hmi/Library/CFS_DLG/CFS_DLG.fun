
FUNCTION DrawDialog : UINT
	VAR_INPUT
		VC_Handle : UDINT;
		AdrDialogInfo : UDINT;
		AdrTxt : UDINT;
		AdrStep : UDINT;
		UseShared : BOOL;
	END_VAR
	VAR
		pDialogParameter : REFERENCE TO dialoginfo;
		pDlgTxt : REFERENCE TO dlgTxt_typ;
		pStep : REFERENCE TO UINT;
		SharedTxtGrpOffs : UDINT;
	END_VAR
END_FUNCTION

FUNCTION CallDialog : UINT
	VAR_INPUT
		AdrDialogIdent : UDINT;
		AdrDialogInformation : UDINT;
		AdrDialogRingBuffer : UDINT;
	END_VAR
	VAR
		pDialogIdent : REFERENCE TO UINT;
		pDialogInformation : REFERENCE TO dialoginfo;
		pDialogRingBuffer : REFERENCE TO dialogringbuffer;
	END_VAR
END_FUNCTION

FUNCTION ResetDialog : UINT
	VAR_INPUT
		AdrDialogIdent : UDINT;
		AdrDialogRingBuffer : UDINT;
	END_VAR
	VAR
		pDialogIdent : REFERENCE TO UINT;
		pDialogRingBuffer : REFERENCE TO dialogringbuffer;
	END_VAR
END_FUNCTION

FUNCTION ExtDialogIF : UINT (*Interface to capture TPL dialogs and provide them to outside*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		AdrDialogRingBuffer : UDINT;
		pDialogInformation : REFERENCE TO dialogextern;
		mode : USINT;
	END_VAR
	VAR
		pDialogRingBuffer : REFERENCE TO dialogringbuffer;
		ProgressBarValue : REFERENCE TO USINT;
	END_VAR
	VAR CONSTANT
		PROGRESS_VIEW_ZERO : USINT := 0;
	END_VAR
END_FUNCTION
