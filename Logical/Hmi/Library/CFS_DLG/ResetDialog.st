(************************************************************************************************************************)
(* Object name: ResetDialog                                                                                             *)
(* Author:      Samuel Otto                                                                                             *)
(* Site:        B&R Bad Homburg                                                                                         *)
(* Created:     17-nov-2010                                                                                             *)
(* Restriction:                                                                                                         *)
(* Description: Function for resetting one Dialog                                                                       *)
(*                                                                                                                      *)
(*----------------------------------------------------------------------------------------------------------------------*)
(* Additional information:                                                                                              *)
(*                                                                                                                      *)
(* No additional information available.                                                                                 *)
(*                                                                                                                      *)
(*----------------------------------------------------------------------------------------------------------------------*)
(* Version 3.00.0  18-jan-2012  Samuel Otto                     Development tool: B&R Automation Studio V3.0.90.19 SP01 *)
(*                                                                                                                      *)
(* Cleaned up this revision history and set version to V3.00.0                                                          *)
(*--------------------------------------------------------------------------------------------------------------------- *)
(************************************************************************************************************************)

FUNCTION ResetDialog
	(* check for Nullpointer *)
	IF AdrDialogIdent = 0 OR AdrDialogRingBuffer = 0 THEN
		ResetDialog := TPL_ERR_NULLPOINTER;
		RETURN;
	END_IF
	
(*********************************************************************************************
 START FUNCTION
 *********************************************************************************************)
	pDialogIdent ACCESS(AdrDialogIdent);
	pDialogRingBuffer ACCESS(AdrDialogRingBuffer);

	IF pDialogIdent = pDialogRingBuffer.ActualDrawedDialog THEN
		pDialogRingBuffer.DialogInformation[pDialogIdent].Return_PressedButton := 255;
		pDialogIdent := 0;	
		ResetDialog := 0;
		RETURN;
	ELSE
		ResetDialog := 600;
		RETURN;
	END_IF
END_FUNCTION
