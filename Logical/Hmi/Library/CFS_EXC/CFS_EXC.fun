FUNCTION_BLOCK SG4_ExceptionAlarmHandler
VAR
		i	:INT;	
		TON_AlarmHandler	:TON;	
		CycleTimeViolation	:BOOL;	
		CycleTimeViolationTC_1	:BOOL;	
		CycleTimeViolationTC_2	:BOOL;	
		CycleTimeViolationTC_3	:BOOL;	
		CycleTimeViolationTC_4	:BOOL;	
		CycleTimeViolationTC_5	:BOOL;	
		CycleTimeViolationTC_6	:BOOL;	
		CycleTimeViolationTC_7	:BOOL;	
		CycleTimeViolationTC_8	:BOOL;	
	END_VAR
	VAR_INPUT
		Initialization	:BOOL;	
	END_VAR
	VAR_IN_OUT
		Command	:SG4_ExceptionCommand;	
	END_VAR
END_FUNCTION_BLOCK
FUNCTION_BLOCK SG4_ExceptionAlarmTrigger
VAR_INPUT
		ExceptionNumber	:INT;	
	END_VAR
	VAR_IN_OUT
		Command	:SG4_ExceptionCommand;	
	END_VAR
END_FUNCTION_BLOCK
