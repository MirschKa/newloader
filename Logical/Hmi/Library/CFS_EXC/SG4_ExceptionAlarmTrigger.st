(************************************************************************************************************************)
(* Object name: SG4_ExceptionAlarmTrigger                                                                               *)
(* Author:      Mario Felius                                                                                            *)
(* Site:        CFS Bakel                                                                                               *)
(* Created:     26-feb-2007                                                                                             *)
(* Restriction: Must run in the exception task only.                                                                    *)
(* Description: Library function block for triggering SG4 exception alarms.                                             *)
(*                                                                                                                      *)
(*----------------------------------------------------------------------------------------------------------------------*)
(* Additional information:                                                                                              *)
(*                                                                                                                      *)
(* This routine triggers an alarm and increments the exception counter.                                                 *)
(* The following exceptions are supported:                                                                              *)
(*                                                                                                                      *)
(* Exception    Symbolic name                  Description                                                              *)
(*                                                                                                                      *)
(*    2         CFS_EXC_BUS_ERROR             Bus error                                                                 *)
(*    5         CFS_EXC_DIVISION_BY_ZERO      Division by zero                                                          *)
(*    145       CFS_EXC_MAX_CYCLETIME        Cycle time violation (any task)                                            *)
(*    151       CFS_EXC_MAX_CYCLETIME_TC_1   Cycle time violation for task class 1                                      *)
(*    152       CFS_EXC_MAX_CYCLETIME_TC_2   Cycle time violation for task class 2                                      *)
(*    153       CFS_EXC_MAX_CYCLETIME_TC_3   Cycle time violation for task class 3                                      *)
(*    154       CFS_EXC_MAX_CYCLETIME_TC_4   Cycle time violation for task class 4                                      *)
(*    155       CFS_EXC_MAX_CYCLETIME_TC_5   Cycle time violation for task class 5                                      *)
(*    156       CFS_EXC_MAX_CYCLETIME_TC_6   Cycle time violation for task class 6                                      *)
(*    157       CFS_EXC_MAX_CYCLETIME_TC_7   Cycle time violation for task class 7                                      *)
(*    158       CFS_EXC_MAX_CYCLETIME_TC_8   Cycle time violation for task class 8                                      *)
(*                                                                                                                      *)
(*----------------------------------------------------------------------------------------------------------------------*)
(* Version 3.00.0  18-jan-2012  Samuel Otto                     Development tool: B&R Automation Studio V3.0.90.19 SP01 *)
(*                                                                                                                      *)
(* Cleaned up this revision history and set version to V3.00.0                                                          *)
(*--------------------------------------------------------------------------------------------------------------------- *)
(************************************************************************************************************************)

FUNCTION_BLOCK SG4_ExceptionAlarmTrigger
(*----------------------------------------------------------------------------------------------------------------------*)
(* Exception Alarm Trigger.                                                                                             *)
(*----------------------------------------------------------------------------------------------------------------------*)
CASE ExceptionNumber OF

CFS_EXC_BUS_ERROR:
    Command.Status.BusErrorCounter :=
      Command.Status.BusErrorCounter + 1;
    Command.AlarmState[CFS_EXC_ALARM_BUS_ERROR]:= TRUE;

CFS_EXC_DIVISION_BY_ZERO:
    Command.Status.DivisionByZeroCounter :=
      Command.Status.DivisionByZeroCounter + 1;
    Command.AlarmState[CFS_EXC_ALARM_DIVISION_BY_ZERO]:= TRUE;

CFS_EXC_MAX_CYCLETIME:
    Command.Status.CycleTimeViolationCounter :=
      Command.Status.CycleTimeViolationCounter + 1;
    Command.AlarmState[CFS_EXC_ALARM_MAX_CYCLETIME]:= TRUE;

CFS_EXC_MAX_CYCLETIME_TC_1:
    Command.Status.CycleTimeViolationTC_1_Counter :=
      Command.Status.CycleTimeViolationTC_1_Counter + 1;
    Command.AlarmState[CFS_EXC_ALARM_MAX_CYCLETIME_TC_1]:= TRUE;

CFS_EXC_MAX_CYCLETIME_TC_2:
    Command.Status.CycleTimeViolationTC_2_Counter :=
      Command.Status.CycleTimeViolationTC_2_Counter + 1;
    Command.AlarmState[CFS_EXC_ALARM_MAX_CYCLETIME_TC_2]:= TRUE;

CFS_EXC_MAX_CYCLETIME_TC_3:
    Command.Status.CycleTimeViolationTC_3_Counter :=
      Command.Status.CycleTimeViolationTC_3_Counter + 1;
    Command.AlarmState[CFS_EXC_ALARM_MAX_CYCLETIME_TC_3]:= TRUE;

CFS_EXC_MAX_CYCLETIME_TC_4:
    Command.Status.CycleTimeViolationTC_4_Counter :=
      Command.Status.CycleTimeViolationTC_4_Counter + 1;
    Command.AlarmState[CFS_EXC_ALARM_MAX_CYCLETIME_TC_4]:= TRUE;

CFS_EXC_MAX_CYCLETIME_TC_5:
    Command.Status.CycleTimeViolationTC_5_Counter :=
      Command.Status.CycleTimeViolationTC_5_Counter + 1;
    Command.AlarmState[CFS_EXC_ALARM_MAX_CYCLETIME_TC_5]:= TRUE;

CFS_EXC_MAX_CYCLETIME_TC_6:
    Command.Status.CycleTimeViolationTC_6_Counter :=
      Command.Status.CycleTimeViolationTC_6_Counter + 1;
    Command.AlarmState[CFS_EXC_ALARM_MAX_CYCLETIME_TC_6]:= TRUE;

CFS_EXC_MAX_CYCLETIME_TC_7:
    Command.Status.CycleTimeViolationTC_7_Counter :=
      Command.Status.CycleTimeViolationTC_7_Counter + 1;
    Command.AlarmState[CFS_EXC_ALARM_MAX_CYCLETIME_TC_7]:= TRUE;

CFS_EXC_MAX_CYCLETIME_TC_8:
    Command.Status.CycleTimeViolationTC_8_Counter :=
      Command.Status.CycleTimeViolationTC_8_Counter + 1;
    Command.AlarmState[CFS_EXC_ALARM_MAX_CYCLETIME_TC_8]:= TRUE;

ELSE

    Command.AlarmState[CFS_EXC_ALARM_EXCEPTION_UNKNOWN]:= TRUE;

END_CASE;
END_FUNCTION_BLOCK
