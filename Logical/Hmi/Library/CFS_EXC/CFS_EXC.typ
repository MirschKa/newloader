TYPE
	SG4_ExceptionCommand_Status : STRUCT
		BusErrorCounter	:DINT;	
		DivisionByZeroCounter	:DINT;	
		CycleTimeViolationCounter	:DINT;	
		CycleTimeViolationTC_1_Counter	:DINT;	
		CycleTimeViolationTC_2_Counter	:DINT;	
		CycleTimeViolationTC_3_Counter	:DINT;	
		CycleTimeViolationTC_4_Counter	:DINT;	
		CycleTimeViolationTC_5_Counter	:DINT;	
		CycleTimeViolationTC_6_Counter	:DINT;	
		CycleTimeViolationTC_7_Counter	:DINT;	
		CycleTimeViolationTC_8_Counter	:DINT;	
	END_STRUCT;
			SG4_ExceptionCommand : STRUCT
		Status	:SG4_ExceptionCommand_Status;	
		AlarmState	:ARRAY[0..11] OF BOOL;	
		AlarmBitField	:ARRAY[0..11] OF BOOL;	
		AcknowledgeImage	:ARRAY[0..11] OF BOOL;	
	END_STRUCT;
END_TYPE
