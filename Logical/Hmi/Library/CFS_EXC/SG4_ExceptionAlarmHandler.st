(************************************************************************************************************************)
(* Object name: SG4_ExceptionAlarmHandler                                                                               *)
(* Author:      Mario Felius                                                                                            *)
(* Site:        CFS Bakel                                                                                               *)
(* Created:     26-feb-2007                                                                                             *)
(* Restriction: Must run in the 1000 ms task class or faster.                                                           *)
(* Description: Library function block for handling SG4 exception alarms.                                               *)
(*                                                                                                                      *)
(*----------------------------------------------------------------------------------------------------------------------*)
(* Additional information:                                                                                              *)
(*                                                                                                                      *)
(* No additional information available.                                                                                 *)
(*                                                                                                                      *)
(*----------------------------------------------------------------------------------------------------------------------*)
(* Version 3.00.0  18-jan-2012  Samuel Otto                     Development tool: B&R Automation Studio V3.0.90.19 SP01 *)
(*                                                                                                                      *)
(* Cleaned up this revision history and set version to V3.00.0                                                          *)
(*--------------------------------------------------------------------------------------------------------------------- *)
(************************************************************************************************************************)

FUNCTION_BLOCK SG4_ExceptionAlarmHandler
(*----------------------------------------------------------------------------------------------------------------------*)
(* Initialization.                                                                                                      *)
(*----------------------------------------------------------------------------------------------------------------------*)
IF Initialization THEN
  Command.Status.BusErrorCounter := 0;
  Command.Status.DivisionByZeroCounter := 0;
  Command.Status.CycleTimeViolationCounter := 0;
  Command.Status.CycleTimeViolationTC_1_Counter := 0;
  Command.Status.CycleTimeViolationTC_2_Counter := 0;
  Command.Status.CycleTimeViolationTC_3_Counter := 0;
  Command.Status.CycleTimeViolationTC_4_Counter := 0;
  Command.Status.CycleTimeViolationTC_5_Counter := 0;
  Command.Status.CycleTimeViolationTC_6_Counter := 0;
  Command.Status.CycleTimeViolationTC_7_Counter := 0;
  Command.Status.CycleTimeViolationTC_8_Counter := 0;
  FOR i := 0 TO SIZEOF(Command.AlarmState) - 1 DO
    Command.AlarmState[i] := FALSE;
    Command.AlarmBitField[i] := FALSE;
    Command.AcknowledgeImage[i] := FALSE;
  END_FOR;
END_IF;


(*----------------------------------------------------------------------------------------------------------------------*)
(* Handling the AlarmBitField and AcknowledgeImage bits.                                                                *)
(*----------------------------------------------------------------------------------------------------------------------*)
TON_AlarmHandler(IN := NOT TON_AlarmHandler.Q, PT := T#1s);

IF TON_AlarmHandler.Q THEN
  FOR i := 0 TO SIZEOF(Command.AlarmState) - 1 DO
    IF Command.AlarmState[i] THEN
      Command.AlarmBitField[i] := TRUE;
    ELSIF NOT Command.AcknowledgeImage[i] THEN
      Command.AlarmBitField[i] := FALSE;
    END_IF;
    Command.AlarmState[i] := FALSE;
  END_FOR;
END_IF;


(*----------------------------------------------------------------------------------------------------------------------*)
(* Writing warnings into the logbook.                                                                                   *)
(*----------------------------------------------------------------------------------------------------------------------*)
IF Command.AlarmBitField[CFS_EXC_ALARM_MAX_CYCLETIME] THEN
  IF NOT CycleTimeViolation THEN
    ERRxwarning(50003, 0, ADR('TC: maximum cycle time violation'));
    CycleTimeViolation := TRUE;
  END_IF;
ELSE
  CycleTimeViolation := FALSE;
END_IF;

IF Command.AlarmBitField[CFS_EXC_ALARM_MAX_CYCLETIME_TC_1] THEN
  IF NOT CycleTimeViolationTC_1 THEN
    ERRxwarning(50004, 0, ADR('TC#1: maximum cycle time violation'));
    CycleTimeViolationTC_1 := TRUE;
  END_IF;
ELSE
  CycleTimeViolationTC_1 := FALSE;
END_IF;

IF Command.AlarmBitField[CFS_EXC_ALARM_MAX_CYCLETIME_TC_2] THEN
  IF NOT CycleTimeViolationTC_2 THEN
    ERRxwarning(50005, 0, ADR('TC#2: maximum cycle time violation'));
    CycleTimeViolationTC_2 := TRUE;
  END_IF;
ELSE
  CycleTimeViolationTC_2 := FALSE;
END_IF;

IF Command.AlarmBitField[CFS_EXC_ALARM_MAX_CYCLETIME_TC_3] THEN
  IF NOT CycleTimeViolationTC_3 THEN
    ERRxwarning(50006, 0, ADR('TC#3: maximum cycle time violation'));
    CycleTimeViolationTC_3 := TRUE;
  END_IF;
ELSE
  CycleTimeViolationTC_3 := FALSE;
END_IF;

IF Command.AlarmBitField[CFS_EXC_ALARM_MAX_CYCLETIME_TC_4] THEN
  IF NOT CycleTimeViolationTC_4 THEN
    ERRxwarning(50007, 0, ADR('TC#4: maximum cycle time violation'));
    CycleTimeViolationTC_4 := TRUE;
  END_IF;
ELSE
  CycleTimeViolationTC_4 := FALSE;
END_IF;

IF Command.AlarmBitField[CFS_EXC_ALARM_MAX_CYCLETIME_TC_5] THEN
  IF NOT CycleTimeViolationTC_5 THEN
    ERRxwarning(50008, 0, ADR('TC#5: maximum cycle time violation'));
    CycleTimeViolationTC_5 := TRUE;
  END_IF;
ELSE
  CycleTimeViolationTC_5 := FALSE;
END_IF;

IF Command.AlarmBitField[CFS_EXC_ALARM_MAX_CYCLETIME_TC_6] THEN
  IF NOT CycleTimeViolationTC_6 THEN
    ERRxwarning(50009, 0, ADR('TC#6: maximum cycle time violation'));
    CycleTimeViolationTC_6 := TRUE;
  END_IF;
ELSE
  CycleTimeViolationTC_6 := FALSE;
END_IF;

IF Command.AlarmBitField[CFS_EXC_ALARM_MAX_CYCLETIME_TC_7] THEN
  IF NOT CycleTimeViolationTC_7 THEN
    ERRxwarning(50010, 0, ADR('TC#7: maximum cycle time violation'));
    CycleTimeViolationTC_7 := TRUE;
  END_IF;
ELSE
  CycleTimeViolationTC_7 := FALSE;

END_IF;

IF Command.AlarmBitField[CFS_EXC_ALARM_MAX_CYCLETIME_TC_8] THEN
  IF NOT CycleTimeViolationTC_8 THEN
    ERRxwarning(50011, 0, ADR('TC#8: maximum cycle time violation'));
    CycleTimeViolationTC_8 := TRUE;
  END_IF;
ELSE
  CycleTimeViolationTC_8 := FALSE;

END_IF;


END_FUNCTION_BLOCK
