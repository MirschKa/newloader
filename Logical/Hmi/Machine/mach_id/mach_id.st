(**************************************************************************************************************************)
(* COPYRIGHT --  GEA FOOD SOLUTIONS GMBH																				  *)
(**************************************************************************************************************************)
(* Program: mach_id																										  *)
(* File: 	mach_id.st																									  *)
(* Author: 	Cilingir.Er																									  *)
(* Created: November 15, 2013																							  *)
(**************************************************************************************************************************)
(* Version 	1.00.0  15-nov-2013  Erdal Cilingir                  Development tool: B&R Automation Studio V3.0.90.19 SP01  *)
(* 		- Set Machine Informations and stop executing the task                                                            *)
(*------------------------------------------------------------------------------------------------------------------------*)
(* Version 	1.00.1  21-jul-2014  Erdal Cilingir                  Development tool: B&R Automation Studio V3.0.90.28 SP10  *)
(* [#425] Task mach_id is missing template configuration variables														  *)
(* 		- Add comment for default configuration parameters		                                                          *)
(* [#424] Handling of HMI configuration (HMISysV3.csv)																	  *)
(* 		- Create new variable TPL_Config.In.Cfg.PreSetsLeading for configuration of handling presets                      *)
(* 		     False = HmiSysV3.CSV is leading and will overwrite the presets                      						  *)
(* 		     True  = Presets are leading and will overwrite HmiSysV3.CSV with pre-set values		                      *)
(*------------------------------------------------------------------------------------------------------------------------*)
(* Version 	1.00.2  04-sep-2015  Erdal Cilingir                  Development tool: B&R Automation Studio V3.0.90.30 SP12  *)
(* [#471] Do Filedevice creation programmatically   																	  *)
(*		  Create File Device over the function CreateFileDevice 														  *)
(*------------------------------------------------------------------------------------------------------------------------*)
(* Version 	1.00.3  27-mar-2018  Dominik Benner                  Development tool: B&R Automation Studio V4.3.4.121 SP    *)
(*      - Add parameter 'TPL_Password.In.Cfg.Login.LoginSystemWith5E9030' to default configuration   					  *)
(**************************************************************************************************************************)

PROGRAM _INIT
    //  ===========================    EXAMPLE FUNCTION CALL FOR CREATION FILE DEVICE   ====================================================================================================
    //CreateFileDevice(TPL_Config.Out.PanelType,TPL_SIM,'Exampe1','Exampe1\');
    //CreateFileDevice(TPL_Config.Out.PanelType,TPL_SIM,'Exampe2','Exampe2\');
END_PROGRAM

PROGRAM _CYCLIC
	//  ===========================    DEFAULT CONFIGURATION OF MACHINE   ====================================================================================================
	//		IF you want to have different DEFAULT configuration of your machine change this task. 
	//		Remove the comment in front of the PV you want to change and set it to your wished value.
	//  ======================================================================================================================================================================
	//	CONFIG TEMPLATE CONFIGURATION
	//		TPL_Config.In.Cfg.PreSetsLeading					:= TPL_DISABLED;				(*0 = HMI_Sys.csv is leading; 1 = Pre Sets in tasks are leading*)
	//  ======================================================================================================================================================================
	//	CONFIG ALARMH
	//		TPL_Alarmh.In.Cfg.TemplateType						:= TPL_TEMPLATE_TYPE_MACHINE;	(* Template Type*)
	//  ======================================================================================================================================================================
	//	CONFIG BACKUP
	//		TPL_Backup.In.Cfg.EnableMachineData					:= TPL_DISABLED;				(* machine data selection on visualization*)
	//		TPL_Backup.In.Cfg.HideConfigUserLevel				:= TPL_USER_LEVEL_7;			(* user level to save config data*)
	//		TPL_Backup.In.Cfg.HideMachineDataUserLevel			:= TPL_USER_LEVEL_7;			(* user level to save machine data*)
	//		TPL_Backup.In.Cfg.TimeIntervalSaveAlarmHistory		:= 0;							(* save alarm history automatic all x minutes*)
	//  ======================================================================================================================================================================
	//	CONFIG DIALOGH
	//		TPL_Dialogh.In.Cfg.UseSharedTxtGrp					:= TPL_DISABLED;				(* use text group from shared resources in dialog handling (set bit 31 in group number) *)
	//  ======================================================================================================================================================================
	//	CONFIG PASSWORDH
	//		TPL_Password.In.Cfg.Login.LoginSystem				:= TPL_DISABLED;				(* Set to "1" to activate CFS Login System by default; set to "0" to leave it deactivated by default *)
	//		TPL_Password.In.Cfg.Login.LoginSystemMaxMode		:= 2;							(* Maximum login system mode the customer can select on configuring the system (currently supported mode 0 & 1) *)
	//		TPL_Password.In.Cfg.Login.LoginSystemMode			:= 0;							(* Current active login system to be used (customer can change later using USBMasterKey; screen 5120) *)
	//      TPL_Password.In.Cfg.Login.LoginSystemReaderType     := 255;                			(* Set to "0" for B&R 5E9010.29, set to "1" for B&R 5E9030.29, set to "255" for auto search*)
    //  ======================================================================================================================================================================
	//	CONFIG RECIPEH
	//		TPL_Rec.In.Cfg.ExternalRecActive					:= TPL_DISABLED;				(* allow external recipe load in cmd*)
	//		TPL_Rec.In.Cfg.OfflineRecipeActive					:= TPL_DISABLED;				(* enables the usage of offline-recipes *)
	//  ======================================================================================================================================================================
	//	CONFIG SYSTEMH
	//		TPL_System.In.Cfg.ActualLanguage					:= 0;							(* actual language of visualization*)
	//		TPL_System.In.Cfg.AutoLogoutOn						:= TPL_ENABLED;					(* auto log on enabled*)
	//		TPL_System.In.Cfg.AutoLogoutTimeInMin				:= 5;							(* auto log on time*)
	//		TPL_System.In.Cfg.OperatingMode						:= 0;							(* operating mode 0=local;1=remote*)
	//		TPL_System.In.Cfg.SDMBtnVisible						:= TPL_ENABLED;					(* show or hide button to enter page with HTML-view of SDM *)

	
	TPL_SWID.In.Cfg.Machine.Name                := 'MachineName';			(* Machine name            (max 24 Characters)*)
	TPL_SWID.In.Cfg.Machine.Type                := 'Type';					(* Machine type            (max 24 Characters)*)
	TPL_SWID.In.Cfg.Machine.Number              := 'Number';				(* Machine number          (max 24 Characters)*)
	TPL_SWID.In.Cfg.Machine.MachineID           := 'MMMT';					(* Machine ID              (max  4 Characters)*)
	TPL_SWID.In.Cfg.Machine.MasterSoftware      := 'MasterSoftware';		(* Master software release (max 24 Characters)*)
	TPL_SWID.In.Cfg.Machine.SoftwareVersion     := 'Vx.xx';					(* Software version number (max  9 Characters)*)
	TPL_SWID.In.Cfg.Machine.SoftwareDate        := 'dd-mm-yyyy';			(* Software date           (max 11 Characters)*)
	TPL_SWID.In.Cfg.Machine.SoftwareAuthor      := 'YourNameHere';			(* Software engineer       (max 24 Characters)*)
	
	(*--------------------------------------------------------------------------------------------------------------------*)
	(* Stop executing this task.                                                                                          *)
	(*--------------------------------------------------------------------------------------------------------------------*)
	IF NOT (TPL_Config.In.Cfg.PreSetsLeading) OR TPL_Config.Out.FileReady THEN
		TPL_SWID.In.SetMachineInfo					:=	TRUE;
		ST_tmp_suspend(0);
	END_IF
	
END_PROGRAM
