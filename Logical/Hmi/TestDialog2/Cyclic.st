
PROGRAM _CYCLIC
	(* Insert code here *)
	CASE DialogStep2 OF
	
		0://Aufruf
			DialogInfo2.DialogTextNumber := DT_BASICREC_OVERWR_NOT_POSSIBLE;
			
			
			IF(Start2) THEN
				Start2:= FALSE;
				
				DialogStep2 := 1;
				
			END_IF
			
					
		1://Bestätigen/Rückmeldung
			DialogStatus2 := CallDialog(ADR(DialogIdent2), ADR(DialogInfo2), TPL_Dialogh.Out.AdrDialogRingBuffer);//Standardaufruf von Dialog
			//Optional ResetDialog(ADR(DialogIdent), TPL_Dialogh.Out.AdrDialogRingBuffer)
			
			IF(DialogInfo2.Return_PressedButton = 1(*Buttonnummer*)) THEN
				DialogInfo2.Return_PressedButton := 0;
				DialogStep2 := 2;
				strResult := 'Button 1';
				// Maschinenspezifischer Ablauf
			
			
			ELSIF(DialogInfo2.Return_PressedButton = 2(*Buttonnummer*)) THEN
				DialogInfo2.Return_PressedButton := 0;
				DialogStep2 := 2;
				strResult := 'Button 2';
				// Maschinenspezifischer Ablauf
			END_IF
			
		2://Reset
			//DialogInfo.Return_PressedButton := 0;
			DialogStep2 := 0;
			//DialogInfo2.Return_PressedButton := 0;
		
	END_CASE
		
END_PROGRAM
