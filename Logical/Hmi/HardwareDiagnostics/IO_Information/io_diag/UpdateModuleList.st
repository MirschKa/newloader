(************************************************************************************************************************)
(* Object name: UpdateModuleList.st                                                                                     *)
(* Author:      Erdal Cilimgir                                                                                          *)
(* Site:        GEA Germany GmbH (Wallau)                                                                               *)
(* Created:     19-jan-2015                                                                                             *)
(* Restriction: -					                                                                                    *)
(* Description: ACTION: Code is for updating Module List	                                                            *)
(*                                                                                                                      *)
(*----------------------------------------------------------------------------------------------------------------------*)
(* Version 2.00.0  01-apr-2016  Erdal Cilingir                  Development tool: B&R Automation Studio V4.1.7.61  SP51 *)
(*  Conversion to AS4.1                                                                                                 *)
(*--------------------------------------------------------------------------------------------------------------------- *)
(************************************************************************************************************************)

ACTION UpdateModuleList: 
	// Update module
	IO_Diag.Internal.Module.Counter:= 0;
	FOR IO_Diag.Internal.i:= UINT_TO_INT(IO_Diag.Internal.Module.Index.Start) TO UINT_TO_INT(IO_Diag.Internal.Module.Index.End) DO
		// Create tree view 
		IO_Diag.Internal.Module.TempString:='';
		IF (FIND(IO_Diag.Internal.Module.InfoWork[IO_Diag.Internal.i].Name,'BC') > 0 AND NOT(IO_Diag.In.Filter.Module.Cmd.Active))  THEN
			brsstrcat(ADR(IO_Diag.Internal.Module.TempString),ADR('  '));
			brsstrcat(ADR(IO_Diag.Internal.Module.TempString),ADR(IO_Diag.Internal.Module.InfoWork[IO_Diag.Internal.i].Name));
			brsmemcpy(ADR(IO_Diag.Hmi.Module.Name[IO_Diag.Internal.Module.Counter]),ADR(IO_Diag.Internal.Module.TempString),SIZEOF(IO_Diag.Hmi.Module.Name[IO_Diag.Internal.Module.Counter]));
		ELSIF (( (FIND(IO_Diag.Internal.Module.InfoWork[IO_Diag.Internal.i].Name,'DI') > 0 )
			OR (FIND(IO_Diag.Internal.Module.InfoWork[IO_Diag.Internal.i].Name,'DO') > 0 )
			OR (FIND(IO_Diag.Internal.Module.InfoWork[IO_Diag.Internal.i].Name,'DM') > 0 )
			OR (FIND(IO_Diag.Internal.Module.InfoWork[IO_Diag.Internal.i].Name,'DS') > 0 )
			OR (FIND(IO_Diag.Internal.Module.InfoWork[IO_Diag.Internal.i].Name,'AI') > 0 )
			OR (FIND(IO_Diag.Internal.Module.InfoWork[IO_Diag.Internal.i].Name,'AO') > 0 )
			OR (FIND(IO_Diag.Internal.Module.InfoWork[IO_Diag.Internal.i].Name,'AT') > 0 )
			OR (FIND(IO_Diag.Internal.Module.InfoWork[IO_Diag.Internal.i].Name,'PS') > 0 )) AND NOT(IO_Diag.In.Filter.Module.Cmd.Active))THEN
			brsstrcat(ADR(IO_Diag.Internal.Module.TempString),ADR('    '));
			brsstrcat(ADR(IO_Diag.Internal.Module.TempString),ADR(IO_Diag.Internal.Module.InfoWork[IO_Diag.Internal.i].Name));
			brsmemcpy(ADR(IO_Diag.Hmi.Module.Name[IO_Diag.Internal.Module.Counter]),ADR(IO_Diag.Internal.Module.TempString),SIZEOF(IO_Diag.Hmi.Module.Name[IO_Diag.Internal.Module.Counter])-1);
		ELSE
			brsmemcpy(ADR(IO_Diag.Hmi.Module.Name[IO_Diag.Internal.Module.Counter]),ADR(IO_Diag.Internal.Module.InfoWork[IO_Diag.Internal.i].Name),SIZEOF(IO_Diag.Hmi.Module.Name[IO_Diag.Internal.Module.Counter]));	
		END_IF
						
		// copy module infomrations to visu structure	
		brsmemcpy(ADR(IO_Diag.Hmi.Module.Path[IO_Diag.Internal.Module.Counter]),ADR(IO_Diag.Internal.Module.InfoWork[IO_Diag.Internal.i].Path),SIZEOF(IO_Diag.Hmi.Module.Path[IO_Diag.Internal.Module.Counter]));
		brsmemcpy(ADR(IO_Diag.Hmi.Module.ErrorName[IO_Diag.Internal.Module.Counter]),ADR(IO_Diag.Internal.Module.InfoWork[IO_Diag.Internal.i].ErrorName),SIZEOF(IO_Diag.Hmi.Module.ErrorName[IO_Diag.Internal.Module.Counter]));

		IO_Diag.Hmi.Module.State[IO_Diag.Internal.Module.Counter]:= IO_Diag.Internal.Module.InfoWork[IO_Diag.Internal.i].State;
		// Check for states and do adjustment for several states
		IF (IO_Diag.Hmi.Module.State[IO_Diag.Internal.Module.Counter] = 4) THEN
			brsmemset(ADR(IO_Diag.Internal.Module.TempString),0,SIZEOF(IO_Diag.Internal.Module.TempString));
			brsstrcat(ADR(IO_Diag.Internal.Module.TempString),ADR(' -> '));
			brsstrcat(ADR(IO_Diag.Internal.Module.TempString),ADR(IO_Diag.Hmi.Module.ErrorName[IO_Diag.Internal.Module.Counter]));
			brsmemcpy(ADR(IO_Diag.Hmi.Module.ErrorName[IO_Diag.Internal.Module.Counter]),ADR(IO_Diag.Internal.Module.TempString),SIZEOF(IO_Diag.Hmi.Module.ErrorName[IO_Diag.Internal.Module.Counter])-1);
		ELSIF (IO_Diag.Hmi.Module.State[IO_Diag.Internal.Module.Counter] = 1) THEN
			brsmemset(ADR(IO_Diag.Internal.Module.TempString),0,SIZEOF(IO_Diag.Internal.Module.TempString));
			brsstrcat(ADR(IO_Diag.Internal.Module.TempString),ADR(' -> '));
			brsstrcat(ADR(IO_Diag.Internal.Module.TempString),ADR(IO_Diag.Hmi.Module.ErrorName[IO_Diag.Internal.Module.Counter]));
			brsmemcpy(ADR(IO_Diag.Hmi.Module.ErrorName[IO_Diag.Internal.Module.Counter]),ADR(IO_Diag.Internal.Module.TempString),SIZEOF(IO_Diag.Hmi.Module.ErrorName[IO_Diag.Internal.Module.Counter])-1);
			IO_Diag.Hmi.Module.Name[IO_Diag.Internal.Module.Counter]:='    ';
		END_IF
		brsstrcat(ADR(IO_Diag.Hmi.Module.Name[IO_Diag.Internal.Module.Counter]),ADR(IO_Diag.Hmi.Module.ErrorName[IO_Diag.Internal.Module.Counter]));
		IO_Diag.Internal.Module.Counter:= IO_Diag.Internal.Module.Counter + 1;
	END_FOR
END_ACTION