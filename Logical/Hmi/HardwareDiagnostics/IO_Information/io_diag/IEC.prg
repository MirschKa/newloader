﻿<?xml version="1.0" encoding="utf-8"?>
<?AutomationStudio Version=4.8.1.174?>
<Program Version="1.04.0" SubType="IEC" xmlns="http://br-automation.co.at/AS/Program">
  <Files>
    <File>io_diag.st</File>
    <File Description="Local data types" Private="true">io_diag.typ</File>
    <File Private="true">io_diag.var</File>
    <File>UpdateIOList.st</File>
    <File>UpdateModuleList.st</File>
    <File>HMI_Module.st</File>
    <File Private="true">Ext_io_diag.typ</File>
    <File Private="true">Ext_io_diag.var</File>
    <File>NG_HMI_interface.st</File>
  </Files>
</Program>