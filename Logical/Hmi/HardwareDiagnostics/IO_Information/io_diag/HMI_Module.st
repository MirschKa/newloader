(************************************************************************************************************************)
(* Object name: HMI_Module.st                           		                                                        *)
(* Author:      Erdal Cilimgir                                                                                          *)
(* Site:        GEA Germany GmbH (Wallau)                                                                               *)
(* Created:     19-jan-2015                                                                                             *)
(* Restriction: -					                                                                                    *)
(* Description: ACTION: Code is for set Hmi parameters		                                                            *)
(*                                                                                                                      *)
(*----------------------------------------------------------------------------------------------------------------------*)
(************************************************************************************************************************)
ACTION HMI_Module: 
	// ***********************************************************************************************************************
	// **********************************************  Layer Module Visible											 *********
	// ***********************************************************************************************************************
	IF IO_Diag.Out.LayerStatus.Module.Info = TPL_VISIBLE THEN
		// Calculate Number of pages
		IO_Diag.Out.Info.Module.NumberPages		:= IO_Diag.Internal.Module.NumberOfTotalModules / IO_Diag.Config.ModuleLines;
		IF (IO_Diag.Internal.Module.NumberOfTotalModules MOD IO_Diag.Config.ModuleLines) > 0 THEN
			IO_Diag.Out.Info.Module.NumberPages	:= IO_Diag.Out.Info.Module.NumberPages	+ 1;
		END_IF
		// Check number of pages = 0
		IF IO_Diag.Out.Info.Module.NumberPages	= 0 THEN
			IO_Diag.Out.Info.Module.NumberPages	:= 1;
		END_IF
		// Calculate actual Page
		IO_Diag.Out.Info.Module.ActualPage		:= IO_Diag.Internal.Module.Index.End / IO_Diag.Config.ModuleLines;
		IF (IO_Diag.Internal.Module.Index.End MOD IO_Diag.Config.ModuleLines) > 0 THEN
			IO_Diag.Out.Info.Module.ActualPage	:= IO_Diag.Out.Info.Module.ActualPage	+ 1;
		END_IF	
		// Check actual Page = 0
		IF IO_Diag.Out.Info.Module.ActualPage	= 0 THEN
			IO_Diag.Out.Info.Module.ActualPage	:= 1;
		END_IF
		// Enable/Disable visibility of Page Down Button
		IF IO_Diag.Out.Info.Module.ActualPage  >= IO_Diag.Out.Info.Module.NumberPages THEN
			IO_Diag.Out.LayerStatus.Module.PageDownAvailable 	:= TPL_INVISIBLE;
		ELSE
			IO_Diag.Out.LayerStatus.Module.PageDownAvailable 	:= TPL_VISIBLE;
		END_IF	
		//  Enable/Disable visibility of Page Up Button
		IF IO_Diag.Out.Info.Module.ActualPage  > 1 THEN
			IO_Diag.Out.LayerStatus.Module.PageUpAvailable	:= TPL_VISIBLE;
		ELSE
			IO_Diag.Out.LayerStatus.Module.PageUpAvailable 	:= TPL_INVISIBLE;
		END_IF			
	END_IF		
	// ***********************************************************************************************************************
	// **********************************************  Layer Io Visible												 *********
	// ***********************************************************************************************************************
	IF IO_Diag.Out.LayerStatus.IO.Info = TPL_VISIBLE THEN
		// Enable/Disable top cursor Top
		IF  IO_Diag.Internal.IO.Index.First >= IO_Diag.Internal.IO.Index.Start THEN
			IO_Diag.Out.LayerStatus.IO.Cursor[0] 	:= TPL_VISIBLE;
		ELSE
			IO_Diag.Out.LayerStatus.IO.Cursor[0]  	:= TPL_INVISIBLE;
		END_IF
		// Enable/Disable top cursor Bottom
		IF  IO_Diag.Internal.IO.Index.End >= IO_Diag.Internal.IO.NumberOf + IO_Diag.Internal.IO.Index.First-1 
		OR IO_Diag.Internal.IO.NumberOf <= IO_Diag.Config.IOLines THEN
			IO_Diag.Out.LayerStatus.IO.Cursor[2] 	:= TPL_VISIBLE;
		ELSE
			IO_Diag.Out.LayerStatus.IO.Cursor[2]  	:= TPL_INVISIBLE;
		END_IF		
	END_IF
	// ***********************************************************************************************************************
	// **********************************************  Layer Filter is Visible										 *********
	// ***********************************************************************************************************************
	IF IO_Diag.Out.LayerStatus.Module.Filter.Show = TPL_VISIBLE THEN
		// Enable/Disable filter para for module settings
		IF IO_Diag.Internal.Module.FilterPara.Type.Enable THEN
			IO_Diag.Out.LayerStatus.Module.Filter.ModuleType 	:= TPL_VISIBLE;
		ELSE
			IO_Diag.Out.LayerStatus.Module.Filter.ModuleType 	:= TPL_INVISIBLE;
		END_IF	
		// Enable/Disable filter para for individual settings	
		IF IO_Diag.Internal.Module.FilterPara.Individual.Active THEN
			IO_Diag.Out.LayerStatus.Module.Filter.Individual 	:= TPL_VISIBLE;
		ELSE
			IO_Diag.Out.LayerStatus.Module.Filter.Individual 	:= TPL_INVISIBLE;
		END_IF		
		// Enable/Disable filter para for State settings
		IF IO_Diag.Internal.Module.FilterPara.State.Active THEN
			IO_Diag.Out.LayerStatus.Module.Filter.State 		:= TPL_VISIBLE;
		ELSE
			IO_Diag.Out.LayerStatus.Module.Filter.State 		:= TPL_INVISIBLE;
		END_IF		
	END_IF		
END_ACTION