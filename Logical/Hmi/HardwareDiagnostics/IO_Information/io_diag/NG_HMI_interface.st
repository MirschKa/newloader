
ACTION NG_HMI_interface: 
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++	
	// Action for NG HMI access 
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	IF NG_IO_Diag.In.Cmd.GetModuleDetails THEN
		NG_IO_Diag.Out.Stat.ModuleDetailsAvailable := FALSE;
		IF (IO_Diag.Internal.IO.pIOInformation.StartAdrBuffer <> 0) THEN
			// reset IO and line idx
			brsmemset(ADR(NG_IO_Diag.Out.ModuleIO),0,SIZEOF(NG_IO_Diag.Out.ModuleIO));
			NG_IO_internal.LineIdx:= 0;
			// loop for Module info
			FOR NG_IO_internal.FirstIdx:= 0 TO IO_Diag.Internal.IO.pIOInformation.TotalIONumber-1 DO
				NG_IO_internal.ReadIdx:= NG_IO_internal.FirstIdx;
				pNgHmiIOData ACCESS IO_Diag.Internal.IO.pIOInformation.StartAdrBuffer + (NG_IO_internal.ReadIdx * SIZEOF(pNgHmiIOData));
				IF (brsstrcmp(ADR(IO_Diag.Internal.Module.InfoFinished[NG_IO_Diag.In.Para.ModuleIndex].Path), ADR(pNgHmiIOData.Device)) = 0) THEN
					// found module with details, get module name, path, state
					brsmemcpy (ADR(NG_IO_Diag.Out.ModuleIO.ModuleName),ADR(IO_Diag.Internal.Module.InfoFinished[NG_IO_Diag.In.Para.ModuleIndex].Name),SIZEOF(NG_IO_Diag.Out.ModuleIO.ModuleName));
					brsmemcpy (ADR(NG_IO_Diag.Out.ModuleIO.ModulePath),ADR(IO_Diag.Internal.Module.InfoFinished[NG_IO_Diag.In.Para.ModuleIndex].Path),SIZEOF(NG_IO_Diag.Out.ModuleIO.ModulePath));
					brsmemcpy (ADR(NG_IO_Diag.Out.ModuleIO.ModuleStatus),ADR(IO_Diag.Internal.Module.InfoFinished[NG_IO_Diag.In.Para.ModuleIndex].State),SIZEOF(NG_IO_Diag.Out.ModuleIO.ModuleStatus));
					NG_IO_Diag.Out.Stat.ModuleDetailsAvailable := TRUE;
					EXIT;
				END_IF
			END_FOR	
		ELSE
			NG_IO_Diag.Out.Stat.ModuleDetailsAvailable := FALSE;
		END_IF
	ELSE
		NG_IO_Diag.Out.Stat.ModuleDetailsAvailable := FALSE;
		brsmemset(ADR(NG_IO_Diag.Out.ModuleIO),0,SIZEOF(NG_IO_Diag.Out.ModuleIO));
	END_IF				
		
	IF NG_IO_Diag.Out.Stat.ModuleDetailsAvailable AND NG_IO_Diag.In.Cmd.GetModuleDetails THEN
		// loop for IO info, FirstIdx defines start idx
		FOR NG_IO_internal.i:= UINT_TO_INT(NG_IO_internal.FirstIdx) TO UINT_TO_INT(IO_Diag.Internal.IO.pIOInformation.TotalIONumber)-1 DO
			NG_IO_internal.ReadIdx:= NG_IO_internal.i;
			pNgHmiIOData ACCESS IO_Diag.Internal.IO.pIOInformation.StartAdrBuffer + (NG_IO_internal.ReadIdx * SIZEOF(pNgHmiIOData));
			IF (brsstrcmp(ADR(IO_Diag.Internal.Module.InfoFinished[NG_IO_Diag.In.Para.ModuleIndex].Path), ADR(pNgHmiIOData.Device)) = 0) THEN
				IF NG_IO_internal.LineIdx <= 16 THEN
					// get description text and name
					brsmemcpy(ADR(NG_IO_Diag.Out.ModuleIO.DescriptionText[NG_IO_internal.LineIdx]),ADR(pNgHmiIOData.LogicalName),SIZEOF(NG_IO_Diag.Out.ModuleIO.DescriptionText[NG_IO_internal.LineIdx]));
					brsmemcpy(ADR(NG_IO_Diag.Out.ModuleIO.VariableName[NG_IO_internal.LineIdx]),ADR(pNgHmiIOData.PVName),SIZEOF(NG_IO_Diag.Out.ModuleIO.VariableName[NG_IO_internal.LineIdx]));
					// get current value of variable. 
					// value is determined within next loop, by running both loops, values are actual
					NG_IO_Diag.Out.ModuleIO.ActValue[NG_IO_internal.LineIdx]:= pNgHmiIoStatusBuff[NG_IO_internal.i].ActValue;
					IF brsstrcmp(ADR(pNgHmiIOData.Inverted),ADR(''))<>0 THEN
						NG_IO_Diag.Out.ModuleIO.Inverted[NG_IO_internal.LineIdx]:= TRUE;
					END_IF
					// increment index until next module -> exit
					NG_IO_internal.LineIdx:= NG_IO_internal.LineIdx +1;
				END_IF
			ELSE
				// next module reached, reset idx
				NG_IO_internal.LineIdx := 0;
				// determine current IO values with borders out of first loop
				NG_IO_internal.z := 0;
				FOR NG_IO_internal.k:= UINT_TO_INT(NG_IO_internal.FirstIdx) TO UINT_TO_INT(NG_IO_internal.ReadIdx -1) DO
					NgHmiUpdateIo[NG_IO_internal.z].Element			:=	NG_IO_internal.k;
					NgHmiUpdateIo[NG_IO_internal.z].Elements		:=	IO_Diag.Internal.IO.pIOInformation.TotalIONumber;
					NgHmiUpdateIo[NG_IO_internal.z].pIOList			:=	IO_Diag.Internal.IO.pIOInformation.StartAdrBuffer;
					NgHmiUpdateIo[NG_IO_internal.z].pStatusBuffer	:=	ADR(pNgHmiIoStatusBuff);
					NgHmiUpdateIo[NG_IO_internal.z]();
					NG_IO_internal.z := NG_IO_internal.z +1;
				END_FOR
				EXIT;
			END_IF
		END_FOR
	END_IF	
	
END_ACTION
