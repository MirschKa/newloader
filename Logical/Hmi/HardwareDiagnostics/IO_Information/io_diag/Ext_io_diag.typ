(*Additional structures for external access via NG HMI*)

TYPE
	HWD_NGHMI_Type : 	STRUCT 
		In : HWD_NGHMI_In_Type;
		Out : HWD_NGHMI_Out_Type;
	END_STRUCT;
	HWD_NGHMI_In_Type : 	STRUCT 
		Cmd : HWD_NGHMI_In_Cmd_Type;
		Para : HWD_NGHMI_In_Para_Type;
	END_STRUCT;
	HWD_NGHMI_In_Cmd_Type : 	STRUCT 
		GetModuleDetails : BOOL;
	END_STRUCT;
	HWD_NGHMI_In_Para_Type : 	STRUCT 
		ModuleIndex : UINT;
	END_STRUCT;
	HWD_NGHMI_Out_Type : 	STRUCT 
		Stat : HWD_NGHMI_Out_Stat_Type;
		ModuleIO : HWD_NGHMI_Out_ModuleIO_Type;
	END_STRUCT;
	HWD_NGHMI_Out_Stat_Type : 	STRUCT 
		ModuleDetailsAvailable : BOOL;
	END_STRUCT;
	HWD_NGHMI_Out_ModuleIO_Type : 	STRUCT 
		ModuleName : STRING[50]; (*Io information selected module name*)
		ModulePath : STRING[50]; (*Io information selected module path*)
		ModuleStatus : USINT; (*Io information selected module status*)
		VariableName : ARRAY[0..MAX_NUMBER_LINES]OF STRING[LENGTH_VARIABLE_STRING]; (*Io information selected module available variable names*)
		DescriptionText : ARRAY[0..MAX_NUMBER_LINES]OF STRING[LENGTH_VARIABLE_STRING]; (*Io information selected module available variable description*)
		ActValue : ARRAY[0..MAX_NUMBER_LINES]OF LREAL; (*Io information selected module available variable values*)
		Inverted : ARRAY[0..MAX_NUMBER_LINES]OF BOOL; (*Io information selected module available variable status variable*)
	END_STRUCT;
	HWD_NGHMIinternal_Type : 	STRUCT 
		ReadIdx : UINT;
		FirstIdx : UINT;
		LineIdx : UINT;
		i : INT;
		k : INT;
		z : USINT;
	END_STRUCT;
END_TYPE
