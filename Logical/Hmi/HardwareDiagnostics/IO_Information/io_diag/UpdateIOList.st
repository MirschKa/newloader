(************************************************************************************************************************)
(* Object name: UpdateIOList.st                                                                                         *)
(* Author:      Erdal Cilimgir                                                                                          *)
(* Site:        GEA Germany GmbH (Wallau)                                                                               *)
(* Created:     19-jan-2015                                                                                             *)
(* Restriction: -					                                                                                    *)
(* Description: ACTION: Code is for updating IOList			                                                            *)
(*                                                                                                                      *)
(*----------------------------------------------------------------------------------------------------------------------*)
(* Version 2.00.0  01-apr-2016  Erdal Cilingir                  Development tool: B&R Automation Studio V4.1.7.61  SP51 *)
(*  Conversion to AS4.1                                                                                                 *)
(*--------------------------------------------------------------------------------------------------------------------- *)
(************************************************************************************************************************)

ACTION UpdateIOList: 
	IO_Diag.Internal.IO.RefreshTimer(IN:=FALSE);	
	brsmemset(ADR(IO_Diag.Hmi.Io.Status),TPL_INVISIBLE,SIZEOF(IO_Diag.Hmi.Io.Status));
				
	(*Update the I/O list entries*)
	IO_Diag.Internal.IO.LineIndex:= 0;
	FOR IO_Diag.Internal.i:= UINT_TO_INT(IO_Diag.Internal.IO.Index.Start) TO UINT_TO_INT(IO_Diag.Internal.IO.Index.End) DO
		IO_Diag.Internal.IO.Index.Read:= IO_Diag.Internal.i;
		pIOData ACCESS IO_Diag.Internal.IO.pIOInformation.StartAdrBuffer + (IO_Diag.Internal.IO.Index.Read * SIZEOF(pIOData));
		IF brsstrcmp(ADR(pIOData.Inverted),ADR(''))<>0 THEN
			IO_Diag.Hmi.Io.Inverted[IO_Diag.Internal.IO.LineIndex]:= TRUE;
		END_IF
		brsmemcpy(ADR(IO_Diag.Hmi.Io.VariableName[IO_Diag.Internal.IO.LineIndex]),ADR(pIOData.PVName),SIZEOF(IO_Diag.Hmi.Io.VariableName[IO_Diag.Internal.IO.LineIndex]));
		brsmemcpy(ADR(IO_Diag.Hmi.Io.DescriptionText[IO_Diag.Internal.IO.LineIndex]),ADR(pIOData.LogicalName),SIZEOF(IO_Diag.Hmi.Io.DescriptionText[IO_Diag.Internal.IO.LineIndex]));
		IO_Diag.Hmi.Io.ActValue[IO_Diag.Internal.IO.LineIndex]:= IO_Diag.Internal.IO.pStatusBuffer[IO_Diag.Internal.i].ActValue;
		IO_Diag.Hmi.Io.Status[IO_Diag.Internal.IO.LineIndex]:= 0;
		IO_Diag.Internal.IO.IOIndexArray[IO_Diag.Internal.IO.LineIndex]:= IO_Diag.Internal.i;
		IO_Diag.Internal.IO.LineIndex:= IO_Diag.Internal.IO.LineIndex + 1;
	END_FOR
END_ACTION