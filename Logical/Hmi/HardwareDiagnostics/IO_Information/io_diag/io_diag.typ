
TYPE
	ModuleInformation_typ : 	STRUCT 
		Path : STRING[LENGTH_PATH_STRING]; (*module path*)
		Name : STRING[LENGTH_NAME_STRING]; (*module name*)
		State : USINT; (*module state*)
		Color : USINT; (*module color*)
		ErrorName : STRING[LENGTH_ERROR_STRING]; (*name of module with error*)
	END_STRUCT;
	HWD_Out_layerStatusFilt : 	STRUCT 
		Show : USINT;
		ModuleType : USINT;
		Individual : USINT;
		State : USINT;
	END_STRUCT;
	HWD_Out_layerStatus : 	STRUCT 
		Info : USINT;
		PageUpAvailable : USINT;
		PageDownAvailable : USINT;
		Cursor : ARRAY[0..2]OF USINT;
		Filter : HWD_Out_layerStatusFilt;
	END_STRUCT;
	HWD_Out_layer : 	STRUCT 
		IO : HWD_Out_layerStatus;
		Module : HWD_Out_layerStatus;
	END_STRUCT;
	HWD_Out_info_Module : 	STRUCT 
		ActualPage : UINT;
		NumberPages : UINT;
	END_STRUCT;
	HWD_Out_info_io : 	STRUCT 
		NoDigitalInputs : UINT;
		NoDigitalOutputs : UINT;
		NoAnalogInputs : UINT;
		NoAnalogOutputs : UINT;
	END_STRUCT;
	HWD_Out_info : 	STRUCT 
		Module : HWD_Out_info_Module;
		IO : HWD_Out_info_io;
		UpdateDuration : TIME;
		Status : UINT;
	END_STRUCT;
	HWD_Out : 	STRUCT 
		Info : HWD_Out_info;
		LayerStatus : HWD_Out_layer;
	END_STRUCT;
	HWD_module_in_cmd : 	STRUCT 
		Back : BOOL;
		LineUp : BOOL;
		LineDown : BOOL;
		ModuleUp : BOOL;
		ModuleDown : BOOL;
		PageUp : BOOL;
		PageDown : BOOL;
		EndList : BOOL;
		StartList : BOOL;
		SelectItem : BOOL;
		SelectListItem : USINT;
	END_STRUCT;
	HWD_module_in : 	STRUCT 
		Cmd : HWD_module_in_cmd;
	END_STRUCT;
	HWD_io_diag : 	STRUCT 
		Config : HWD_Config;
		In : HWD_In;
		Hmi : HWD_HMI; (*module information*)
		Out : HWD_Out;
		Internal : HWD_Internal;
	END_STRUCT;
	HWD_Internal_seq : 	STRUCT 
		Step : Enumeration_HWD_io_diag_seqenze;
	END_STRUCT;
	HWD_Internal_mod_index : 	STRUCT 
		Next : INT;
		Selected : UINT;
		SelectedLine : UINT;
		Start : UINT;
		End : UINT;
	END_STRUCT;
	HWD_Internal_mod : 	STRUCT 
		TempString : STRING[50];
		Counter : USINT;
		NumberOfFilteredModules : UINT;
		NumberOfTotalModules : UINT;
		NumberOfTotalModulesOld : UINT;
		DetailsAvailable : BOOL;
		RefreshTimer : TON;
		AlarmInformation : ModuleAlarmInformation_typ;
		InfoUpdate : ARRAY[0..MAX_NUMBER_ITEMS]OF ModuleInformation_typ;
		InfoFinished : ARRAY[0..MAX_NUMBER_ITEMS]OF ModuleInformation_typ;
		InfoWork : ARRAY[0..MAX_NUMBER_ITEMS]OF ModuleInformation_typ;
		InfoHide : ARRAY[0..MAX_NUMBER_ITEMS]OF BOOL;
		Index : HWD_Internal_mod_index;
		FilterPara : HWD_In_Filter_Type_para;
	END_STRUCT;
	HWD_Internal_io_index : 	STRUCT 
		Selected : UINT;
		SelectedLine : UINT;
		Start : UINT;
		End : UINT;
		First : UINT;
		Read : UINT;
	END_STRUCT;
	HWD_Internal_io : 	STRUCT 
		TempString : STRING[50];
		NumberOf : UINT;
		Counter : USINT;
		LineIndex : UINT;
		IOIndexArray : ARRAY[0..MAX_NUMBER_LINES]OF UINT;
		pIOInformation : IODPList_typ;
		pStatusBuffer : ARRAY[0..MAX_IO_DATAPOINTS]OF SingleIOStatus_typ;
		RefreshTimer : TON;
		Index : HWD_Internal_io_index;
	END_STRUCT;
	HWD_Internal : 	STRUCT 
		Result : DINT;
		i : INT;
		UpdateDuration : TON;
		Module : HWD_Internal_mod;
		IO : HWD_Internal_io;
		Sequenz : HWD_Internal_seq;
		FB_ModuleDiagnoseFinishedInfo : ModuleDiagnose;
		FB_ModuleDiagnoseRunInfo : ModuleDiagnose;
	END_STRUCT;
	HWD_In_Filter_Type_State : 	STRUCT 
		Active : BOOL;
		Compare : USINT;
	END_STRUCT;
	HWD_In_Filter_Type_para_type : 	STRUCT 
		Enable : BOOL;
		Mod_BC : BOOL;
		Mod_DI : BOOL;
		Mod_DO : BOOL;
		Mod_AI : BOOL;
		Mod_AO : BOOL;
		Mod_AT : BOOL;
		Mod_PS : BOOL;
	END_STRUCT;
	HWD_In_Filter_Type_para : 	STRUCT 
		Type : HWD_In_Filter_Type_para_type;
		State : HWD_In_Filter_Type_State;
		Individual : HWD_In_Filter_Type_indiv;
	END_STRUCT;
	HWD_In_Filter_Type_indiv : 	STRUCT 
		Active : BOOL;
		Text : STRING[70];
	END_STRUCT;
	HWD_In_Filter_Type_cmd : 	STRUCT 
		Active : BOOL;
		Set : BOOL;
		Cancel : BOOL;
		ShowFilter : BOOL;
	END_STRUCT;
	HWD_In_Filter_Type : 	STRUCT 
		Para : HWD_In_Filter_Type_para;
		Cmd : HWD_In_Filter_Type_cmd;
	END_STRUCT;
	HWD_In_Filter : 	STRUCT 
		Module : HWD_In_Filter_Type;
	END_STRUCT;
	HWD_In : 	STRUCT 
		Filter : HWD_In_Filter;
	END_STRUCT;
	HWD_HMI_Module : 	STRUCT 
		In : HWD_module_in;
		Name : ARRAY[0..MAX_NUMBER_LINES]OF STRING[LENGTH_NAME_STRING]; (* Informations module names*)
		Path : ARRAY[0..MAX_NUMBER_LINES]OF STRING[LENGTH_PATH_STRING]; (* Informations module pathes*)
		State : ARRAY[0..MAX_NUMBER_LINES]OF USINT; (* Informations module state*)
		ErrorName : ARRAY[0..MAX_NUMBER_LINES]OF STRING[LENGTH_ERROR_STRING]; (* Informations module Error module name*)
	END_STRUCT;
	HWD_HMI_Io : 	STRUCT 
		In : HWD_module_in;
		ModuleName : STRING[50]; (*Io information selected module name*)
		ModulePath : STRING[50]; (*Io information selected module path*)
		ModuleStatus : USINT; (*Io information selected module status*)
		VariableName : ARRAY[0..MAX_NUMBER_LINES]OF STRING[LENGTH_VARIABLE_STRING]; (*Io information selected module available variable names*)
		DescriptionText : ARRAY[0..MAX_NUMBER_LINES]OF STRING[LENGTH_VARIABLE_STRING]; (*Io information selected module available variable description*)
		ActValue : ARRAY[0..MAX_NUMBER_LINES]OF LREAL; (*Io information selected module available variable values*)
		Status : ARRAY[0..MAX_NUMBER_LINES]OF USINT; (*Io information selected module available variable status variable*)
		Inverted : ARRAY[0..MAX_NUMBER_LINES]OF BOOL; (*Io information selected module available variable status variable*)
	END_STRUCT;
	HWD_HMI : 	STRUCT 
		Module : HWD_HMI_Module; (*module information*)
		Io : HWD_HMI_Io; (*I/O information*)
	END_STRUCT;
	HWD_Config : 	STRUCT 
		UpdateModulePerCycle : USINT; (*Config: Number of modules to update per cycle*)
		ModuleLines : USINT; (*Config: Number of modules lines on HMI*)
		IOLines : USINT; (*Config: Number of io lines on HMI*)
		Option : Enumeration_hmi_IO_diag_option; (*Config: option for reading out hardware Informations*)
		ModuleRefreshTime : TIME; (*Config: update interval of modules on HMI*)
		IORefreshTime : TIME; (*Config: update interval of Io on HMI*)
	END_STRUCT;
	Enumeration_HWD_io_diag_seqenze : 
		(
		HWD_DIAG_STEP_INIT_MODULE := 0,
		HWD_DIAG_STEP_WAIT_CMD_MODULE := 1,
		HWD_DIAG_STEP_UPDATE_MODULE_LIST := 2,
		HWD_DIAG_STEP_INIT_IO := 3,
		HWD_DIAG_STEP_WAIT_CMD_IO := 4,
		HWD_DIAG_STEP_UPDATE_IO_LIST := 5
		);
END_TYPE
