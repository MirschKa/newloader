#include <bur/plctypes.h>
#include <AsIOMMan.h>
#include <DataObj.h>
#include <string.h>
#include <AsIO.h>
#include <AsIODiag.h>
#include <Sys_lib.h>
#include "internal_h.h"
#include <HWDiag.h>
#include <bur/plctypes.h>
#include <AsIODiag.h>
#include <string.h>
#include <Standard.h>

#define	FORCE_STATUS	0x04
#define	GET_DP_STATUS		3
#define	FUNCTION_BUSY	65535


/* TODO: Add your comment here */
void FB_IO_Update(struct FB_IO_Update* inst)
{	static	USINT		Step = 0;
	static	UINT		DPIndex = 0;
		
	USINT	ForceStatus = 0;
/*	USINT	StatusFlag = 0;  	Variable wird nur beschrieben und NICHT gelesen -> entfernt FTG 26.04.19 */
/*	UINT	Status = 0;  		Variable wird nur beschrieben und NICHT gelesen -> entfernt FTG 26.04.19 */
			
	
	*(UDINT*)&inst->SingleIOEntry = inst->pIOList;
	*(UDINT*)&inst->SingleIOStatus = inst->pStatusBuffer;
	
	if(inst->Elements <= 0)
	{	//return(0);
	}

	if(inst->Element > 0 && inst->Element<=inst->Elements){
		DPIndex = inst->Element;
		Step = GET_DP_STATUS;
	}
	
	switch(Step)
	{	/* Check actions for data point */
		/* Get data point status */
		case GET_DP_STATUS:	
			/* Call functio block to get data point status */
			inst->IODPStatus.enable = 1;
			inst->IODPStatus.pDatapoint = (UDINT)(inst->SingleIOEntry + DPIndex)->ID;
			
			AsIODPStatus(&(inst->IODPStatus));
/*			Status = inst->IODPStatus.status;	Variable wird nur beschrieben und NICHT gelesen -> entfernt FTG 26.04.19 */
			
			if(inst->IODPStatus.status == 0)
			{	/* Copy data to io status buffer */
				(inst->SingleIOStatus + DPIndex)->ActValue = (LREAL)inst->IODPStatus.value;
				(inst->SingleIOStatus + DPIndex)->ActForceValue = (LREAL)inst->IODPStatus.forceValue;
				(inst->SingleIOStatus + DPIndex)->DataType = inst->IODPStatus.datatype;
				
/*				StatusFlag = inst->IODPStatus.flags;	Variable wird nur beschrieben und NICHT gelesen -> entfernt FTG 26.04.19 */
				ForceStatus = inst->IODPStatus.flags & FORCE_STATUS;
				
				if(ForceStatus == 4)
				{	(inst->SingleIOStatus + DPIndex)->ForceActive = 1;	
				}
				else
				{	(inst->SingleIOStatus + DPIndex)->ForceActive = 0;
				}
				
/*				Status = 0;		Variable wird nur beschrieben und NICHT gelesen -> entfernt FTG 26.04.19 */
			}
			break;
	}
	//return(Status);
}

