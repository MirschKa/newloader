/************************************************************************************************************************/
/* Object name: iostatus.c																								*/
/* Author:      Andreas Hager, B&R																						*/
/* Site:        Eggelsberg                                                                                            	*/
/* Created:     24-Feb-2006                                                                                           	*/
/* Restriction: 													      		                                      	*/
/* Description: Function to handle configured IO data points	                                                      	*/
/*----------------------------------------------------------------------------------------------------------------------*/
/* Version 1.00			24-Feb-2006		 Andreas Hager			Official release								  		*/
/*----------------------------------------------------------------------------------------------------------------------*/
/* Version 1.01			12-Dec-2006		 Andreas Hager			More IOs can be handled and commment added 		  		*/
/*----------------------------------------------------------------------------------------------------------------------*/
/* Version 2.00			16-Jul-2008		 Andreas Hager			AS3 - Changes for GCC 4.1.1								*/
/*----------------------------------------------------------------------------------------------------------------------*/
/* Version 2.04			21-Jun-2010		 Andreas Hager			ActValue & ForceValue changed to data type LREAL		*/
/*----------------------------------------------------------------------------------------------------------------------*/
/* Version 2.05			02-Jul-2010		 Andreas Hager			Problem with force value and LREAL fixed				*/
/*----------------------------------------------------------------------------------------------------------------------*/
/* Development tool: B&R Automation Studio V3.0.80.25 																  	*/
/*----------------------------------------------------------------------------------------------------------------------*/
/* Comment:                                                                                                           	*/
/*----------------------------------------------------------------------------------------------------------------------*/
/*  Version 2.06		02-May-2019		 Dominik Benner			Comment of unused variables								*/                                                                                                         	
/************************************************************************************************************************/

#include <HWDiag.h>
#include <bur/plctypes.h>
#include <AsIOMMan.h>
#include <DataObj.h>
#include <string.h>
#include <AsIO.h>
#include <AsIODiag.h>
#include <Sys_lib.h>
#include "internal_h.h"

#define	FORCE_STATUS	0x04
#define	GET_DP_STATUS		3
#define	FUNCTION_BUSY	65535

static	AsIODPStatus_typ		IODPStatus;

unsigned short SelectedIOStatus(unsigned short Element, unsigned long pIOList, unsigned long pStatusBuffer, unsigned short Elements)
{	static	USINT		Step = 0;
	static	UINT		DPIndex = 0;
	

	
/*	USINT	StatusFlag = 0; 	Variable wird nur beschrieben und NICHT gelesen -> entfernt FTG 26.04.19 */
	USINT 	ForceStatus = 0;
	UINT	Status = 0;	
	
	SingleIOEntry_typ		*SingleIOEntry = 0;
	SingleIOStatus_typ		*SingleIOStatus = 0;


	*(UDINT*)&SingleIOEntry = pIOList;
	*(UDINT*)&SingleIOStatus = pStatusBuffer;
	
	if(Elements <= 0)
	{	return(0);
	}

	if(Element > 0 && Element<=Elements){
		DPIndex = Element;
		Step = GET_DP_STATUS;
	}
	
	switch(Step)
	{	/* Check actions for data point */
		/* Get data point status */
		case GET_DP_STATUS:	
			/* Call functio block to get data point status */
			IODPStatus.enable = 1;
			IODPStatus.pDatapoint = (UDINT)(SingleIOEntry + DPIndex)->ID;
			
			AsIODPStatus(&(IODPStatus));
			Status = IODPStatus.status;
			
			if(IODPStatus.status == 0)
			{	/* Copy data to io status buffer */
				(SingleIOStatus + DPIndex)->ActValue = (LREAL)IODPStatus.value;
				(SingleIOStatus + DPIndex)->ActForceValue = (LREAL)IODPStatus.forceValue;
				(SingleIOStatus + DPIndex)->DataType = IODPStatus.datatype;
				
/*				StatusFlag = IODPStatus.flags;		Variable wird nur beschrieben und NICHT gelesen -> entfernt FTG 26.04.19 */
				ForceStatus = IODPStatus.flags & FORCE_STATUS;
				
				if(ForceStatus == 4)
				{	(SingleIOStatus + DPIndex)->ForceActive = 1;	
				}
				else
				{	(SingleIOStatus + DPIndex)->ForceActive = 0;
				}
				
				Status = 0;
			}
			break;
	}
	return(Status);
}

