
FUNCTION_BLOCK ModuleDiagnose
	VAR_INPUT
		AdrModuleInformationBuffer : UDINT;
		Option : Enumeration_hmi_IO_diag_option;
		AdrAlarmInformation : UDINT;
	END_VAR
	VAR_OUTPUT
		status : UINT;
		ModuleNumber : UINT;
	END_VAR
	VAR
		ActualModuleInformation : SingleModuleInformationData_typ;
		PathBuffer : ARRAY[0..599] OF STRING[30];
		Step : USINT;
		LastStep : USINT;
	END_VAR
	VAR_OUTPUT
		NumberOfTotalModules : UINT;
	END_VAR
	VAR
		ModuleIndex : UINT;
		ModulesLastCheck : UINT;
		NumberNotCheckedModules : UINT;
		ModuleDifference : UINT;
		ResetModuleAlarms : UINT;
		WaitTimer : TON;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION SelectedIOStatus : UINT
	VAR_INPUT
		Element : UINT;
		pIOList : UDINT;
		pStatusBuffer : UDINT;
		Elements : UINT;
	END_VAR
END_FUNCTION

FUNCTION ForceIO : UINT
	VAR_INPUT
		Element : UINT;
		pIOList : UDINT;
		pStatusBuffer : UDINT;
		Elements : UINT;
		pEnableForce : UDINT;
		pForceValue : UDINT;
	END_VAR
END_FUNCTION

FUNCTION PageIOStatus : UINT
	VAR_INPUT
		StartElement : UINT;
		EndElement : UINT;
		pIOList : UDINT;
		pStatusBuffer : UDINT;
		Elements : UINT;
	END_VAR
END_FUNCTION

FUNCTION IOList : UINT
	VAR_INPUT
		pIOInformation : UDINT;
	END_VAR
END_FUNCTION

FUNCTION_BLOCK FB_IO_Update (*TODO: Add your comment here*)
	VAR_INPUT
		Element : UINT;
		pIOList : UDINT;
		pStatusBuffer : UDINT;
		Elements : UINT;
	END_VAR
	VAR
		SingleIOEntry : REFERENCE TO SingleIOEntry_typ;
		IODPStatus : AsIODPStatus;
		SingleIOStatus : REFERENCE TO SingleIOStatus_typ;
	END_VAR
END_FUNCTION_BLOCK
