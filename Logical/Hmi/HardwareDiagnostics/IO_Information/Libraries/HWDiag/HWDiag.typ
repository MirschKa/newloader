
TYPE
	Enumeration_hmi_IO_diag_option : 
		(
		HMI_DIAG_OPT_ALL := 0,
		HMI_DIAG_OPT_WITHOUT_IF_LS := 1,
		HMI_DIAG_OPT_WITHOUT_AC_LK := 2,
		HMI_DIAG_OPT_WITHOUT_IF_LS_AC_LK := 3
		);
	IODPList_typ : 	STRUCT 
		StartAdrBuffer : UDINT;
		NumberDigitalInput : UINT;
		NumberDigitalOutput : UINT;
		NumberAnalogInput : UINT;
		NumberAnalogOutput : UINT;
		TotalIONumber : UINT;
	END_STRUCT;
	SingleIOEntry_typ : 	STRUCT 
		ModuleName : STRING[20];
		Device : STRING[60];
		ID : STRING[60];
		LogicalName : STRING[60];
		Type : USINT;
		PVName : STRING[512];
		Inverted : STRING[20];
	END_STRUCT;
	SingleIOStatus_typ : 	STRUCT 
		ForceActive : BOOL;
		ActForceValue : LREAL;
		ActValue : LREAL;
		DataType : USINT;
	END_STRUCT;
	ModuleAlarmInformation_typ : 	STRUCT 
		ErrorModulePath : STRING[30];
		ErrorModuleName : STRING[20];
		AlarmBitField : ARRAY[0..1799]OF BOOL;
		ACKBitField : ARRAY[0..1799]OF BOOL;
	END_STRUCT;
	SingleModuleInformationData_typ : 	STRUCT 
		ModulePath : STRING[LENGTH_PATH_STRING];
		ModuleName : STRING[LENGTH_NAME_STRING];
		ModuleState : USINT;
		ModuleColor : USINT;
		ErrorModuleName : STRING[LENGTH_ERROR_STRING];
	END_STRUCT;
	ModuleInformationBuffer_typ : 	STRUCT 
		ModuleInformation : ARRAY[0..599]OF SingleModuleInformationData_typ;
	END_STRUCT;
END_TYPE
