(**************************************************************************************************************************)
(* COPYRIGHT --  GEA FOOD SOLUTIONS GMBH																				  *)
(**************************************************************************************************************************)
(* Program: SelHiddenModules																							  *)
(* File: 	SelHiddenModules.st																							  *)
(* Author: 	Dominik Benner																								  *)
(* Created: November 11, 2016																							  *)
(**************************************************************************************************************************)
(* Version 	1.00.0  11-nov-2016  Dominik Benner                  Development tool: B&R Automation Studio V4.1.10.69 SP    *)
(* 		- Created task and inserted example selections                                                           		  *)
(*------------------------------------------------------------------------------------------------------------------------*)
(* Version 	1.01.0  15-sep-2016  Dominik Benner                  Development tool: B&R Automation Studio V4.1.12.57 SP    *)
(* 		- Added examples for alias module names                                                           		  		  *)
(*------------------------------------------------------------------------------------------------------------------------*)
(**************************************************************************************************************************) 

PROGRAM _INIT

END_PROGRAM


PROGRAM _CYCLIC
	//  ===========================    SELECTION OPTIONS FOR SHOWING OR HIDDING NODES OR SINGLE MODUELS   ====================================================================
	//	Showing or hiding of modules works with 2 arrays:
	//	 - The first array named 'IO_DiagHideNode' is for showing/hiding superordinate modules/ nodes inkl. all subordinated modules.
	//	 - The second array named 'IO_DiagHideDummy' is for showing/hiding single modules. 
	// 	The 'IO_DiagHideNode'-array has a higher priority than the 'IO_DiagHideDummy'-array.
	//	
	//  NOTE: THE SHOWN SELECTIONS ARE ONLY EXAMPLES!
	//	If you want to use this funtionality you can use this task or realize it in any other task.
	//	======================================================================================================================================================================
	//	HIDE OR SHOW NODES (with all subordinated modules)
	//	IO_DiagHideNode[0].HwTreePath 	:= 'SL1.IF1.ST16'					(* Path of a Powerlink node (e.g. a X20BC0083) *)
	//	IO_DiagHideNode[0].HideMode		:= HIDE_NEVER;						(* 3 types are possible: 0:HIDE_NEVER; 1:HIDE_IF_MISSING; 2:HIDE_ALWAYS; *)				
	//  ======================================================================================================================================================================
	//	HIDE OR SHOW ONLY SINGLE MODULES
	//	IO_DiagHideDummy[0].HwTreePath 	:= 'SL1.IF1.ST16.IF1.ST2'			(* Path of a single module (e.g. a X20DI9371) *)
	//	IO_DiagHideDummy[0].HideMode	:= HIDE_NEVER;						(* 3 types are possible: 0:HIDE_NEVER; 1:HIDE_IF_MISSING; 2:HIDE_ALWAYS; *)
	//  ======================================================================================================================================================================
	
	
	//	===========================    OPTIONS FOR GIVING ALIAS NAMES TO NODES/ MODULES/ DEVICES    ==========================================================================
	//  This array is for giving alias names to common nodes, devices or single modules.
	//	For giving an alias name just put in the modules hardware path and the new name for this specific module etc.
	//
	//  NOTE: THE SHOWN SELECTIONS ARE ONLY EXAMPLES!
	//	If you want to use this funtionality you can use this task or realize it in any other task.	
	//	======================================================================================================================================================================
	//	REPLACE MODULE DESIGNATION BY NAME 
		IO_DiagAliasName[0].HwTreePath	:= '$$root';
		IO_DiagAliasName[0].Name		:= 'B&R IPC';	
		IO_DiagAliasName[1].HwTreePath	:= 'SL1';
		IO_DiagAliasName[1].Name		:= 'PWL Interface';	
	//  ======================================================================================================================================================================	


END_PROGRAM
