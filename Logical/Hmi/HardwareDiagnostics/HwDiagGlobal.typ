
TYPE
	IO_DiagHide_Type : 	STRUCT 
		HwTreePath : STRING[50]; (*Hardware tree path*)
		HideMode : USINT; (*Mode for hiding*)
	END_STRUCT;
	IO_DiagAlias_Type : 	STRUCT 
		HwTreePath : STRING[50]; (*Hardware tree path*)
		Name : STRING[50]; (*Name to replace a module designation*)
	END_STRUCT;
END_TYPE
