(*--------------------------------------------------------------------------------------------------------------------- *)
(* Version 3.01.0  09-mai-2012  Frank Thomas Greeb              Development tool: B&R Automation Studio V3.0.90.19 SP01 *)
(* member FileNumberExternal of TPL_RecipeInPara_Type changed from UINT to UDINT                                        *)
(************************************************************************************************************************)
(* *)
(*Network-Handling*)

TYPE
	TPL_Netweth_Type : 	STRUCT 
		In : TPL_NetwethIn_Type;
		Out : TPL_NetwethOut_Type;
	END_STRUCT;
	TPL_NetwethIn_Type : 	STRUCT 
		Cmd : TPL_NetwethInCmd_Type;
		Stat : TPL_NetwethInStat_Type;
		Cfg : TPL_NetwethInCfg_Type;
	END_STRUCT;
	TPL_NetwethInCmd_Type : 	STRUCT 
		LeavePageIpSettings : USINT;
		SaveSettingsHMI : BOOL;
		UpdateIpSettings : BOOL;
		ReadIpSettings : BOOL;
		UpdateNodeNumber : BOOL;
	END_STRUCT;
	TPL_NetwethInStat_Type : 	STRUCT 
		AddressChanged : BOOL;
	END_STRUCT;
	TPL_NetwethInCfg_Type : 	STRUCT 
		IpAddress : ARRAY[0..3]OF USINT;
		SubnetMask : ARRAY[0..3]OF USINT;
		DefGateway : ARRAY[0..3]OF USINT;
		IpAddressRemote : ARRAY[0..3]OF USINT;
		NodeNumber : USINT;
	END_STRUCT;
	TPL_NetwethOut_Type : 	STRUCT 
		Para : TPL_NetwethOutPara_Type;
		Stat : TPL_NetwethOutStat_Type;
		Cmd : TPL_NetwethOutCmd_Type;
	END_STRUCT;
	TPL_NetwethOutPara_Type : 	STRUCT 
		IpAddress : STRING[15];
		IpAddressValue : ARRAY[0..3]OF USINT;
		SubnetMask : STRING[15];
		SubnetMaskValue : ARRAY[0..3]OF USINT;
		DefGateway : STRING[15];
		DefGatewayValue : ARRAY[0..3]OF USINT;
		BroadcastAddress : STRING[15];
		BroadcastAddressValue : ARRAY[0..3]OF USINT;
		IpAddressRemoteStation : STRING[15];
		IpAddressRemoteValue : ARRAY[0..3]OF USINT;
		EthBaudrate : USINT;
	END_STRUCT;
	TPL_NetwethOutStat_Type : 	STRUCT 
		Busy : BOOL;
		EthAddressReady : BOOL;
	END_STRUCT;
	TPL_NetwethOutCmd_Type : 	STRUCT 
		SaveIpAddress : BOOL;
		SaveNodeNumber : BOOL;
	END_STRUCT;
END_TYPE

(*ScanUsb*)

TYPE
	TPL_ScanUsbTypeOut : 	STRUCT 
		UsbStickAvailable : BOOL;
		NumberOfUsbDevices : UDINT;
		MountedUsbInterface : STRING[150];
		MountedUsbNode : UDINT;
		VendorId : UINT;
		ProductId : UINT;
	END_STRUCT;
	TPL_ScanUsbType : 	STRUCT 
		Out : TPL_ScanUsbTypeOut;
	END_STRUCT;
END_TYPE

(* *)
(*Receipe-Handling*)

TYPE
	TPL_Recipe_Type : 	STRUCT  (*Template recipe type*)
		In : TPL_RecipeIn_Type; (*Input Interface*)
		Out : TPL_RecipeOut_Type; (*Output Interface*)
	END_STRUCT;
	TPL_RecipeIn_Type : 	STRUCT 
		Cmd : TPL_RecipeInCmd_Type;
		Para : TPL_RecipeInPara_Type;
		Cfg : TPL_RecipeInCfg_Type;
	END_STRUCT;
	TPL_RecipeInCmd_Type : 	STRUCT 
		LoadRecipeOffline : BOOL; (*Command  load recipe offline*)
		SaveRecipeOffline : BOOL; (*Command  save recipe offline*)
		DelRecipeOffline : BOOL; (*Command  delete recipe offline*)
		OpenNewRecipe : BOOL; (*Command open a new recipe*)
		LoadRecipeOnline : BOOL; (*Command  load recipe online*)
		SaveRecipeOnline : BOOL; (*Command  save recipe online*)
		DelRecipeOnline : BOOL; (*Command  delete recipe online*)
		LoadRecipeExternal : BOOL; (*Command  Load external recipe*)
		LoadRecipeExternalACK : BOOL; (*Command  Load external recipe acknowleged*)
		RefreshData : BOOL; (*Command  to update the datamodule*)
		Execute : BOOL; (*Command to execute an action*)
		EnSearchNumber : BOOL; (*Command to enable search for recipe number*)
		EnSearchName : BOOL; (*Command to enable search for recipe name*)
		EnSearchComment : BOOL; (*Command to enable search for recipe comment*)
		EnSearchType : BOOL; (*Command to enable search for recipe type*)
		ApplyFilter : BOOL; (*Command to apply filter settings*)
		SetFilter : BOOL; (*Command to set filter settings*)
	END_STRUCT;
	TPL_RecipeInPara_Type : 	STRUCT 
		FileNumber : UINT; (*Number of recipe  ? not used ?*)
		FileNumberExternal : UDINT; (*Number of external recipe*)
		UserLevel : USINT; (*Actual password level*)
		ActualPage : UINT; (*Actual page on panel*)
		LoadRecipeOnlineLock : BOOL; (*Command Lock Recipe online load*)
		RecipeSearch : TPL_RecipeSearch_Type;
	END_STRUCT;
	TPL_RecipeInCfg_Type : 	STRUCT 
		OfflineRecipeActive : BOOL; (*Offline recipe active enabled*)
		ExternalRecActive : BOOL; (*External loading of recipes enabled*)
		EnableRecipeSearch : BOOL; (*Enable to search in recipes*)
	END_STRUCT;
	TPL_RecipeOut_Type : 	STRUCT 
		Hmi : TPL_RecipeOutHmi_Type;
		Stat : TPL_RecipeOutStat_Type;
	END_STRUCT;
	TPL_RecipeOutHmi_Type : 	STRUCT 
		RecChangedField : BOOL; (*Shows that recipe parameter were changed (detected by checksum) *)
		FileNumberLoadRecipe : UINT;
		RecipeNumberStatus : UINT;
		RecipeNumberColor : UINT;
		RecHandlingError : BOOL;
	END_STRUCT;
	TPL_RecipeOutStat_Type : 	STRUCT 
		ActRecNumber : UDINT; (*active recipe number*)
		Error : BOOL; (*Error in recipe handling occurred*)
		ErrNumber : UINT; (*Error Number*)
		RecLoadedOffline : BOOL; (*Recipe Loaded Successfull*)
		RecLoaded : BOOL; (*Recipe Loaded Successfull*)
		RecChanged : BOOL; (*recipe parameter was changed*)
		StepNumber : UINT; (*Status of step counter *)
		Busy : BOOL; (*Recipe handling active*)
		AdrRecListBox : UDINT; (*Address of listbox to provide REC information for external MES applications*)
	END_STRUCT;
	TPL_RecipeSearch_Type : 	STRUCT 
		Number : USINT; (*searched recipe number*)
		Name : STRING[20]; (*searched recipe name*)
		Comment : STRING[30]; (*searched recipe comment*)
		Type : USINT; (*searched recipe type*)
	END_STRUCT;
END_TYPE

(* *)
(*Backup-Handling*)

TYPE
	TPL_Backup_Type : 	STRUCT 
		In : TPL_BackupIn_Type;
		Out : TPL_BackupOut_Type;
	END_STRUCT;
	TPL_BackupIn_Type : 	STRUCT 
		Cmd : TPL_BackupInCmd_Type;
		Para : TPL_BackupInPara_Type;
		Cfg : TPL_BackupInCfg_Type;
	END_STRUCT;
	TPL_BackupInCmd_Type : 	STRUCT 
		Backup : BOOL;
		Restore : BOOL;
		ExportLog : BOOL;
		ExportAlarmHis : BOOL;
		ExpLogAlarmHistVerbose : BOOL;
	END_STRUCT;
	TPL_BackupInPara_Type : 	STRUCT 
		Move : BOOL;
		EnableConfig : BOOL;
		EnableRecipe : BOOL;
		EnableData : BOOL;
		EnableMachineData : BOOL;
		EnablePBI : BOOL;
		SelectAll : BOOL;
	END_STRUCT;
	TPL_BackupInCfg_Type : 	STRUCT 
		EnableMachineData : BOOL;
		HideConfigUserLevel : UINT;
		HideMachineDataUserLevel : UINT;
		TimeIntervalSaveAlarmHistory : UINT;
	END_STRUCT;
	TPL_BackupOut_Type : 	STRUCT 
		Stat : TPL_BackupOutStat_Type;
		HMI : TPL_BackupOutHMI_Type;
	END_STRUCT;
	TPL_BackupOutStat_Type : 	STRUCT 
		Error : BOOL;
		ErrNumber : UINT;
		StepNumber : USINT;
		Busy : BOOL;
		NewDataLoaded : BOOL;
	END_STRUCT;
	TPL_BackupOutHMI_Type : 	STRUCT 
		StatusPBIbtn : USINT;
		StatusConfigbtn : USINT;
		StatusMachineDatabtn : USINT;
	END_STRUCT;
END_TYPE

(* *)
(*Config-Handling (Saving of parameters)*)

TYPE
	TPL_Config_Type : 	STRUCT 
		In : TPL_ConfigIn_Type;
		Out : TPL_ConfigOut_Type;
	END_STRUCT;
	TPL_ConfigInCfg_Type : 	STRUCT 
		PreSetsLeading : BOOL;
	END_STRUCT;
	TPL_ConfigIn_Type : 	STRUCT 
		Cmd : TPL_ConfigInCmd_Type;
		Cfg : TPL_ConfigInCfg_Type;
		Data : TPL_ConfigInData_Type;
	END_STRUCT;
	TPL_ConfigInCmd_Type : 	STRUCT 
		ReadFile : BOOL;
		WriteFile : BOOL;
	END_STRUCT;
	TPL_ConfigInData_Type : 	STRUCT  (*Here should be all config-parts of the correspondenting TPL_-structures. Also insert 2 lines in hmi_cfg.st*)
		SWID : TPL_SWID_InCfg_Type;
		System : TPL_SystemInCfg_Type;
		Recipe : TPL_RecipeInCfg_Type;
		Backup : TPL_BackupInCfg_Type;
		Password : TPL_PasswordInCfg_Type;
		Alarm : TPL_AlarmhInCfg_Type;
		Netweth : TPL_NetwethInCfg_Type;
		Dialogh : TPL_DialoghInCfg_Type;
		UserManagement : TPL_UserManagerInCfg_Type;
		MachineDataInferface : TPL_MDIInCfg_Type;
	END_STRUCT;
	TPL_ConfigOut_Type : 	STRUCT 
		Stat : TPL_ConfigOutStat_Type;
		StartUpFinished : BOOL;
		FileReady : BOOL;
		UpdateTcpIp : BOOL;
		ReadTcpIp : BOOL;
		PanelSize : USINT;
		PanelType : USINT;
	END_STRUCT;
	TPL_ConfigOutStat_Type : 	STRUCT 
		Busy : BOOL;
		Error : BOOL;
		ErrorNumber : UINT;
		StepNumber : UINT;
		PBIActivated : BOOL;
	END_STRUCT;
END_TYPE

(* *)
(*Dialog Interface for one Dialog*)

TYPE
	TPL_DialogIF_Type : 	STRUCT 
		In : TPL_DialogIF_In_Type;
		Out : TPL_DialogIF_Out_Type;
	END_STRUCT;
	TPL_DialogIF_In_Type : 	STRUCT 
		Cmd : TPL_DialogIF_InCmd_Type; (*Command for dialog handling*)
		Para : dialoginfo; (*Parameter for dialog handling, same as for the function call*)
	END_STRUCT;
	TPL_DialogIF_InCmd_Type : 	STRUCT 
		OpenDialog : BOOL; (*Dialog box is open as long as this command is true*)
	END_STRUCT;
	TPL_DialogIF_Out_Type : 	STRUCT 
		Info : TPL_DialogIF_OutInfo_Type; (*Information from dialog handling*)
	END_STRUCT;
	TPL_DialogIF_OutInfo_Type : 	STRUCT 
		Busy : BOOL; (*TPL_Dialog_CMD_Type*)
		Status : UINT; (*Status : 0 = OK; 600 = no dialog possible*)
		PressedButton : USINT; (*If a button is pressed the number of this button is available here, 0 as long no button pressed*)
	END_STRUCT;
END_TYPE

(* *)
(*Software-ID*)

TYPE
	TPL_SWID_Type : 	STRUCT 
		In : TPL_SWID_In_Type;
		Out : TPL_SWID_Out_Type;
	END_STRUCT;
	TPL_SWID_In_Type : 	STRUCT 
		SetMachineInfo : BOOL;
		Cfg : TPL_SWID_InCfg_Type;
	END_STRUCT;
	TPL_SWID_InCfg_Type : 	STRUCT 
		Machine : TPL_SWID_SoftwareID_Type;
		Template : TPL_SWID_SoftwareID_Type;
	END_STRUCT;
	TPL_SWID_SoftwareID_Type : 	STRUCT 
		Name : STRING[24];
		Type : STRING[24];
		Number : STRING[24];
		MachineID : STRING[4];
		MasterSoftware : STRING[24];
		SoftwareVersion : STRING[9];
		SoftwareDate : STRING[11];
		SoftwareAuthor : STRING[24];
	END_STRUCT;
	TPL_SWID_Out_Type : 	STRUCT 
		MachineNameTypeNumber : STRING[74];
		LoadSoftwareID_FromTask : USINT;
	END_STRUCT;
END_TYPE

(* *)
(*PageHandling*)

TYPE
	TPL_Pageh_Type : 	STRUCT 
		In : TPL_PagehIn_Type;
		Out : TPL_PagehOut_Type;
	END_STRUCT;
	TPL_PagehIn_Type : 	STRUCT 
		SwitchToPage : UINT; (*Command to pagehandling from any task or button of visu to change a page. Pageh checks if it is allowed*)
	END_STRUCT;
	TPL_PagehOut_Type : 	STRUCT 
		VC_Handle : UDINT; (*Screen handle from the panel object*)
		ActualPage : UINT; (*Connected to visu, shows the actual page*)
		SwitchToPage : UINT; (*Connected to visu, makes the change of pages*)
		PanelIsAlive : BOOL; (*Connected to visu, makes the change of pages*)
	END_STRUCT;
END_TYPE

(* *)
(*Lockhandling*)

TYPE
	TPL_Lockh_Type : 	STRUCT 
		In : TPL_LockhIn_Type;
	END_STRUCT;
	TPL_LockhIn_Type : 	STRUCT 
		Cmd : TPL_LockhInCmd_Type;
	END_STRUCT;
	TPL_LockhInCmd_Type : 	STRUCT 
		StatusForLockingButtons : UINT;
		StatusForLockingInputFields : UINT;
		StatusForLockingPasswordInput : UINT;
	END_STRUCT;
END_TYPE

(* *)
(*Passwordhandling*)

TYPE
	TPL_Password_Type : 	STRUCT 
		In : TPL_PasswordIn_Type;
		Out : TPL_PasswordOut_Type;
		InOut : TPL_PasswordInOut_Type;
	END_STRUCT;
	TPL_PasswordIn_Type : 	STRUCT 
		Cmd : TPL_PasswordInCmd_Type;
		Cfg : TPL_PasswordInCfg_Type;
	END_STRUCT;
	TPL_PasswordInCmd_Type : 	STRUCT 
		Logout : BOOL;
		OpenPasswordAlphaPad : BOOL;
	END_STRUCT;
	TPL_PasswordInCfg_Type : 	STRUCT 
		Login : TPL_PasswordInCfgLogin_Type;
		codedPwds : ARRAY[0..8]OF STRING[20];
		codeLength : UDINT;
	END_STRUCT;
	TPL_PasswordInCfgLogin_Type : 	STRUCT 
		LoginSystem : USINT;
		LoginSystemMaxMode : USINT;
		LoginSystemMode : USINT;
		LoginSystemReaderType : USINT;
	END_STRUCT;
	TPL_PasswordOut_Type : 	STRUCT 
		StatusPasswordInput : UINT;
		UserLevel : USINT;
	END_STRUCT;
	TPL_PasswordInOut_Type : 	STRUCT 
		Password : ARRAY[0..8]OF STRING[10];
		NewPassword : ARRAY[0..11]OF USINT;
		ChangPassword : ARRAY[0..8]OF STRING[10];
		SearchComplete : USINT;
	END_STRUCT;
END_TYPE

(* *)
(*Login System*)

TYPE
	TPL_IsoLoginSys_Type : 	STRUCT 
		Out : TPL_IsoLoginSysOut_Type;
	END_STRUCT;
	TPL_IsoLoginSysOut_Type : 	STRUCT 
		Stat : TPL_IsoLoginSysOutStat_Type;
	END_STRUCT;
	TPL_IsoLoginSysOutStat_Type : 	STRUCT 
		stIsoLoginSystemReady : BOOL;
		stIsoReaderFound : BOOL;
		stIsoReaderInitialized : BOOL;
		stIsoLoginSystemMasterKeyFound : BOOL;
		stKeyHasCfsID : BOOL;
		stKeyIsExpired : BOOL;
		stKeyIsValid : BOOL;
		stUsersPasswordMatching : BOOL;
		stLoginAccepted : BOOL;
	END_STRUCT;
END_TYPE

(* *)
(*Alarmhandling*)

TYPE
	TPL_Alarmh_Type : 	STRUCT 
		In : TPL_AlarmhIn_Type;
		Out : TPL_AlarmhOut_Type;
	END_STRUCT;
	TPL_AlarmhIn_Type : 	STRUCT 
		Cfg : TPL_AlarmhInCfg_Type;
		Cmd : TPL_AlarmhInCmd_Type;
	END_STRUCT;
	TPL_AlarmhInCfg_Type : 	STRUCT 
		TemplateType : USINT;
	END_STRUCT;
	TPL_AlarmhInCmd_Type : 	STRUCT 
		ExtQuitAllAlarms : BOOL;
	END_STRUCT;
	TPL_AlarmhOut_Type : 	STRUCT 
		Stat : TPL_AlarmhOutStat_Type;
		Info : TPL_AlarmhOutInfo_Type;
	END_STRUCT;
	TPL_AlarmhOutStat_Type : 	STRUCT 
		AlarmDialogIsShown : BOOL;
		ResetButtonPressed : BOOL;
		AllAcknowledged : BOOL;
	END_STRUCT;
	TPL_AlarmhOutInfo_Type : 	STRUCT 
		NotAckAlarmCount : UINT;
		ActAlarmCount : UINT;
		ActAlarm : ARRAY[0..500]OF UINT; (*STRING[500]*)
	END_STRUCT;
END_TYPE

(* *)
(*Dialoghandling*)

TYPE
	TPL_Dialogh_Type : 	STRUCT 
		In : TPL_DialoghIn_Type;
		Out : TPL_DialoghOut_Type;
	END_STRUCT;
	TPL_DialoghOut_Type : 	STRUCT 
		AdrDialogRingBuffer : UDINT;
		DialogHandlingWorking : BOOL;
		MessageTextActual : ARRAY[0..299]OF UINT; (*Actual shown messagetext ("" if no dialog is opened)*)
		MessageTextLast : ARRAY[0..299]OF UINT; (*Actual or last shown messagetext*)
	END_STRUCT;
	TPL_DialoghIn_Type : 	STRUCT 
		Cfg : TPL_DialoghInCfg_Type;
	END_STRUCT;
	TPL_DialoghInCfg_Type : 	STRUCT 
		UseSharedTxtGrp : BOOL;
	END_STRUCT;
END_TYPE

(* *)
(*System*)

TYPE
	TPL_System_Type : 	STRUCT  (*Output of Sysset*)
		In : TPL_SystemIn_Type;
		Out : TPL_SystemOut_Type;
	END_STRUCT;
	TPL_SystemIn_Type : 	STRUCT 
		Cfg : TPL_SystemInCfg_Type;
		Cmd : TPL_SystemInCmd_Type;
		Remote : TPL_SystemInRemote_Type;
	END_STRUCT;
	TPL_SystemInRemote_Type : 	STRUCT 
		Status : BOOL; (*0 = Error 1 = OK*)
	END_STRUCT;
	TPL_SystemInCmd_Type : 	STRUCT 
		GlobalStatusIndex : UINT;
	END_STRUCT;
	TPL_SystemInCfg_Type : 	STRUCT 
		ActualLanguage : UINT;
		VNC_Password : ARRAY[0..1]OF STRING[20];
		OperatingMode : UINT;
		AutoLogoutOn : UINT;
		AutoLogoutTimeInMin : USINT;
		SDMBtnVisible : BOOL;
	END_STRUCT;
	TPL_SystemOut_Type : 	STRUCT 
		Stat : TPL_SystemOutStat_Type;
		Info : TPL_SystemOutInfo_Type;
	END_STRUCT;
	TPL_SystemOutRemote_Type : 	STRUCT 
		Mode : UINT; (*0 = Local 1 = Remote*)
		Status : BOOL; (*0 = Error 1 = OK*)
	END_STRUCT;
	TPL_SystemOutStat_Type : 	STRUCT 
		GlobalStatusIndex : UINT;
		ActualLanguageInitialized : BOOL;
		Remote : TPL_SystemOutRemote_Type;
	END_STRUCT;
	TPL_SystemOutInfo_Type : 	STRUCT  (*Output of date and time (CSI,Costfox�)*)
		DateTimeActual : DTStructure;
		DateOut : STRING[8];
		TimeOut : STRING[8];
		LogBook : ARRAY[0..38]OF STRING[80];
		BatteryStatusCPU : USINT;
		PowerOnCycles : UDINT;
		OperatingHours : UDINT;
	END_STRUCT;
END_TYPE

(*PLC info*)

TYPE
	TPL_PLCinfo_Type : 	STRUCT 
		Out : TPL_PLCinfoOut_Type;
	END_STRUCT;
	TPL_PLCinfoOut_Type : 	STRUCT 
		SerialNumber_CPU : STRING[32];
		ModuleID_CPU : STRING[32];
		PowerOnCycles_CPU : STRING[32];
		OperatingHours_CPU : STRING[32];
		Temperature_CPU : STRING[32];
		Temperature_RAM : STRING[32];
		MtcxVersion : STRING[32];
		FreeSRAM : STRING[32];
		FreeUserROM : STRING[32];
		FreeSystemROM : STRING[32];
		FreeDRAM : STRING[32];
		Version_AR : STRING[32];
	END_STRUCT;
END_TYPE

(**)
(*UserManager*)

TYPE
	TPL_UserManager_Type : 	STRUCT 
		In : TPL_UserManagerIn_Type;
		Out : TPL_UserManagerOut_Type;
		InOut : TPL_UserManagerInOut_Type;
	END_STRUCT;
	TPL_UserManagerIn_Type : 	STRUCT 
		Cfg : TPL_UserManagerInCfg_Type;
		Info : TPL_UserManagerInInfo_Type;
	END_STRUCT;
	TPL_UserManagerInInfo_Type : 	STRUCT 
		HMI_Ctrl : TPL_UserManagerInInfoHMI_Type;
		DataHandler : TPL_UserManagerInInfoDH_Type;
		RFID : TPL_UserManagerInInfoRFID_Type;
	END_STRUCT;
	TPL_UserManagerInCfg_Type : 	STRUCT 
		NbrOfRFIDReader : USINT;
		LoggingAvailable : BOOL;
		DoubleAuthentificationAvailable : BOOL;
		PWCheck_LowerCharacter : USINT;
		PWCheck_UpperCharacter : USINT;
		PWCheck_Number : USINT;
		PWCheck_SpecialCharacter : USINT;
		PWCheck_NumberCharacter : INT;
	END_STRUCT;
	TPL_UserManagerOut_Type : 	STRUCT 
		Data : TPL_UserManagerOutData_Type;
		Cmd : TPL_UserManagerOutCmd_Type;
		Status : UINT;
	END_STRUCT;
	TPL_UserManagerOutData_Type : 	STRUCT 
		UserData : TPL_UserManager_UserDataTyp;
	END_STRUCT;
	TPL_UserManagerOutCmd_Type : 	STRUCT 
		AddUser : BOOL;
		SayHi : BOOL;
		DoBackUp : BOOL;
		AbortProcess : BOOL;
		ShowInfoScreen : BOOL;
		FindUser : BOOL;
	END_STRUCT;
	TPL_UserManagerInInfoRFID_Type : 	STRUCT 
		UID : STRING[16];
		ServiceTag : USINT;
		Status : UINT;
		TransponderTyp : STRING[20];
		TagName : STRING[20];
	END_STRUCT;
	TPL_UserManagerInInfoDH_Type : 	STRUCT 
		Status : UINT;
	END_STRUCT;
	TPL_UserManagerInInfoHMI_Type : 	STRUCT 
		Status : UINT;
	END_STRUCT;
	TPL_UserManagerInOut_Type : 	STRUCT 
		UserDataTmp : ARRAY[0..MAX_USER]OF TPL_UserManager_UserDataTyp;
	END_STRUCT;
	TPL_UserManager_UsrDataCryp_Type : 	STRUCT 
		UserName : STRING[20];
		UID : STRING[24];
		UserLvl : STRING[4];
		LogoutTime : USINT;
		Language : UINT;
		Password : STRING[28];
		ExpirationDate : STRING[16];
		LoginCounter : STRING[4];
	END_STRUCT;
	TPL_UserManager_UserDataTyp : 	STRUCT 
		UserName : STRING[20];
		UID : STRING[16];
		UserLvl : STRING[2];
		LogoutTime : USINT;
		Language : UINT;
		Password : STRING[20];
		ExpirationDate : STRING[10];
		LoginCounter : STRING[2];
	END_STRUCT;
END_TYPE

(**)
(*ESignature*)

TYPE
	TPL_ESignature_Type : 	STRUCT 
		In : TPL_ESignatureIn_Type;
		Out : TPL_ESignatureOut_Type;
	END_STRUCT;
	TPL_ESignatureIn_Type : 	STRUCT 
		Cmd : TPL_ESignatureInCmd_Type;
		Data : TPL_ESignatureInData_Type;
	END_STRUCT;
	TPL_ESignatureOut_Type : 	STRUCT 
		Status : UINT;
		Tracer : STRING[80];
	END_STRUCT;
	TPL_ESignatureInCmd_Type : 	STRUCT 
		Compare : BOOL;
	END_STRUCT;
	TPL_ESignatureInData_Type : 	STRUCT 
		FirstValue : STRING[20];
		SecondValue : STRING[20];
	END_STRUCT;
END_TYPE

(**)
(*Dialog interface to external clients*)

TYPE
	TPL_ExtDialogIF_Type : 	STRUCT 
		In : TPL_ExtDialogIF_In_Type;
		Out : TPL_ExtDialogIF_Out_Type;
	END_STRUCT;
	TPL_ExtDialogIF_In_Type : 	STRUCT 
		Cmd : TPL_ExtDialogIF_InCmd_Type;
	END_STRUCT;
	TPL_ExtDialogIF_InCmd_Type : 	STRUCT 
		PressButton : USINT;
	END_STRUCT;
	TPL_ExtDialogIF_Out_Type : 	STRUCT 
		Status : dialogextern;
	END_STRUCT;
END_TYPE

(**)
(*MachineDataInterface*)

TYPE
	TPL_MDIInCfg_Type : 	STRUCT 
		Available : BOOL;
		OPCUA_Client : BOOL;
		CycleTime : TIME;
	END_STRUCT;
END_TYPE
