(********************************************************************
 * COPYRIGHT --  
 ********************************************************************
 * Program: pageh
 * File: Pages.st
 * Author: Cilingir.Er
 * Created: October 18, 2013
 ********************************************************************
 * Implementation of program pageh
 ********************************************************************) 

(* Action for allowed pages evaluation *)
ACTION Pages: 
				FOR xcnt:= 0 TO (PageDataLen - 1) DO
					IF SearchStep = 0 THEN
						brsstrcpy(ADR(Mark),ADR('#P#'));
					ELSE
						brsstrcpy(ADR(Mark),ADR('#L#'));
					END_IF
					SearchMark ACCESS(AdrPageData + xcnt);
					Result:= brsstrcmp(ADR(SearchMark), ADR(Mark));
					IF Result = 0  AND SearchStep = 0 THEN	(* we foung #P#, the number of the page *)
						pUINT ACCESS(AdrPageData + SIZEOF(Mark) + xcnt);
						AvailablePage:= pUINT;	// we search now, if we can access this page
						SearchStep:= 1;
					ELSIF Result = 0 AND SearchStep <> 0 THEN (* we found #L#, now the allowed userlevels are listet *)
						brsstrcpy(ADR(Mark),ADR('#P#'));	// search, until we find the next page #P#
						FOR ycnt:= xcnt TO (PageDataLen - 1) DO	// lets look from this position to maximum the end...
							SearchMark ACCESS(AdrPageData + SIZEOF(Mark) + ycnt);
							EndOfData:= brsstrcmp(ADR(SearchMark), ADR(EndMark));	// have we found the end of file?
							Result:= brsstrcmp(ADR(SearchMark), ADR(Mark));	// or have we found the next #P#?
							IF Result <> 0 THEN	// if not, lets look, if we can access the page
								pUINT ACCESS(AdrPageData + SIZEOF(Mark) + ycnt);
								IF pUINT = TPL_Password.Out.UserLevel THEN	// if we can access the page with the actual userlevel, lets save it
									AllowedPages[NumberOfAllowedPages]:= AvailablePage;
									NumberOfAllowedPages:= NumberOfAllowedPages + 1;
									SearchStep:= 0;
									xcnt:= ycnt;
									EXIT;
								END_IF
							ELSIF Result = 0 THEN
								xcnt:= ycnt;
								SearchStep:= 0;
								EXIT;
							END_IF
							IF EndOfData = 0 THEN
								EXIT;
							END_IF
						END_FOR
					END_IF
				END_FOR
END_ACTION