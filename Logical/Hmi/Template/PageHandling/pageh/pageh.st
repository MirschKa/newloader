(************************************************************************************************************************)
(* Object name: pageh                                                                                                   *)
(* Author:      Roman Haslinger                                                                                         *)
(* Site:        B&R Eggelsberg                                                                                          *)
(* Created:     12-aug-2003                                                                                             *)
(* Restriction: consider task order.                                                                                    *)
(* Description: Page handling.                                                                                          *)
(*                                                                                                                      *)
(*----------------------------------------------------------------------------------------------------------------------*)
(* Additional information:                                                                                              *)
(*                                                                                                                      *)
(* No additional information available.                                                                                 *)
(*                                                                                                                      *)
(*----------------------------------------------------------------------------------------------------------------------*)
(* Version 3.00.0  18-jan-2012  Samuel Otto                     Development tool: B&R Automation Studio V3.0.90.19 SP01 *)
(*                                                                                                                      *)
(* Cleaned up this revision history and set version to V3.00.0                                                          *)
(*--------------------------------------------------------------------------------------------------------------------- *)
(* Version 3.00.1  08-may-2012  FTG                             Development tool: B&R Automation Studio V3.0.90.19 SP01 *)
(* use the "CalibrationStateDatapoint" from visu to detect if touch calibration is necessary                            *)
(*--------------------------------------------------------------------------------------------------------------------- *)
(* Version 3.00.2  10-sep-2012  FTG                             Development tool: B&R Automation Studio V3.0.90.21 SP03 *)
(* - "CalibrationStateDatapoint" has to be used only in combination with "PanelLifeSign" and in cyclic part because     *)
(*   in Terminal mode the datapoint is only valid in panel is life, sometimes (WinTerminal) even later -> TON needed    *)
(*--------------------------------------------------------------------------------------------------------------------- *)
(* Version 3.00.3  11-sep-2012  FTG                             Development tool: B&R Automation Studio V3.0.90.21 SP03 *)
(* - PV ConnectionStatus := 1 in INIT: status PV for connetion error page. Terminal mode                                *)
(* - do NOT call VA_Setup until a visu is connected! again: Terminal mode!                                              *)
(*--------------------------------------------------------------------------------------------------------------------- *)
(* Version 3.00.4  12-oct-2012  FTG                             Development tool: B&R Automation Studio V3.0.90.21 SP03 *)
(* - call VA_Setup outside stepchain (cyclic) to get a new handle if things happen to VCWT...                           *)
(*--------------------------------------------------------------------------------------------------------------------- *)
(* Version 3.00.5  18-oct-2013  EC                             Development tool: B&R Automation Studio V3.0.90.21 SP03 	*)
(* [#40] on a template update the data module "pages" of the machine is overwriten.                           			*)
(* 		Change case for Pagehandling																					*)
(* 			- 2 Datamodules pages and MachinePag created																*)
(* 			- Read out pages data module first and then read out MachinePag datamodule									*)
(************************************************************************************************************************)
(* Version 3.04.1  01-jul-2015  SO                              Development tool: B&R Automation Studio V3.0.90.30 SP12 *)
(* [#421] Dialogtext not OK if wrong level to enter value and login system active										*)
(* 		- Changed dialog in case of wrong user level with login system													*)
(* [#473] Template doesn't start up if HmiSysV3.csv is corrupted														*)
(*		  - Make Panel is Alive available in Out Structure (TPL_Pageh.Out.PanelIsAlive)           						*)
(************************************************************************************************************************)

PROGRAM _INIT
DialogInfo.HeaderTextGroupNumber:= TPL_DIALOG_HEADERTEXT;
DialogInfo.DialogTextGroupNumber:= TPL_DIALOG_TEXT;
DialogInfo.ButtonTextGroupNumber:= TPL_DIALOG_BUTTONTEXT;

ActualUserLevel:= 1;
TPL_Pageh.In.SwitchToPage:= 1;
LastPage:= 1;
BackFromAlarming:= 1;
PageEasyPasswordFlag:= 0;

brsstrcpy(ADR(EndMark), ADR('#E#'));

(*----------------------------------------------------------------------------------------------------------------------*)
(* Touch calibration on startup:                                                                                        *)
(* must be handled in cyclic because of terminal mode                                                                   *)
(*----------------------------------------------------------------------------------------------------------------------*)

KeyLevel;	// This variable is only connected to the visu. So we need to call it here one time.
ConnectionStatus := 1; (* if the connection is OK the visu reads 1=hide, if no connection visu sets to 0=show *)
ReadModuleNr := 0;
END_PROGRAM

PROGRAM _CYCLIC
		
(* ********************************************** *)
(* 					PAGE_HANDLING				  *)
(* ********************************************** *)
(* get handle to visu outside of stepchain (Terminal mode!) *)
IF PanelIsLife THEN 	(* VA_Setup must not be called until a visu is connectet (Terminal mode!) *)
	IF TPL_Pageh.Out.VC_Handle = 0 THEN
		TPL_Pageh.Out.VC_Handle:= VA_Setup(1, PANEL_OBJECT_NAME);
	END_IF
ELSE
	TPL_Pageh.Out.VC_Handle:= 0;	// don't use handle if panel is not life
END_IF

(* if dialog task not work, pagehandling is active *)
IF TPL_Dialogh.Out.DialogHandlingWorking = 0 THEN

	CASE Step OF
		TPL_VC_HANDLE:				(* get handle from display *)
			IF PanelIsLife THEN 	(* VA_Setup must not be called until a visu is connectet (Terminal mode!) *)
				IF TPL_Pageh.Out.VC_Handle = 0 THEN
					;	//TPL_Pageh.Out.VC_Handle:= VA_Setup(1, PANEL_OBJECT_NAME);
				ELSE
					// Reset the variables for Pagehandling
					ActualUserLevel := 99;
					ReadModuleNr := 0;
					NumberOfAvailablePages:=0;
					NumberOfAllowedPages:=0;
					NumberOfAvailableJumpPages:=0;
					brsmemset(ADR(AvailablePages),0,SIZEOF(AvailablePages));
					brsmemset(ADR(AvailableJumpPages),0,SIZEOF(AvailableJumpPages));
					brsmemset(ADR(AllowedPages),0,SIZEOF(AllowedPages));
					// Step data module selection
					Step:= DATAMODULE_SELECTION;
				END_IF
			END_IF;

		DATAMODULE_SELECTION:
			// Which module has to be read
			CASE ReadModuleNr OF
				0: Step:= OPEN_DATAMODULE_PAGES;
				1: Step:= OPEN_DATAMODULE_MACHINE;
				ELSE
				Step:= READ_ALLOWEDUSERPAGES;
				
			END_CASE



		OPEN_DATAMODULE_PAGES:	(* Open datamodule for Template pages *)
			DOInfoPages.enable:= 1;
			DOInfoPages.pName:= ADR(PAGES_DATA_OBJECT_NAME);
			DOInfoPages();
			Status.DOInfo:= DOInfoPages.status;
			AdrPageData:= DOInfoPages.pDatObjMem;
			PageDataLen:= DOInfoPages.len;
			ReadModuleNr:=ReadModuleNr+1;
			Step:= READ_AVAILABLEPAGES;

		OPEN_DATAMODULE_MACHINE:	(* Open datamodule for Machine pages *)
			DOInfoMachine.enable:= 1;
			DOInfoMachine.pName:= ADR(MACHINE_DATA_OBJECT_NAME);
			DOInfoMachine();
			Status.DOInfo:= DOInfoMachine.status;
			AdrPageData:= DOInfoMachine.pDatObjMem;
			PageDataLen:= DOInfoMachine.len;
			Step:= READ_AVAILABLEPAGES;
			ReadModuleNr:=ReadModuleNr+1;

		READ_AVAILABLEPAGES:	(* Read all available pages from the datamodule and fill array *)
			brsstrcpy(ADR(Mark),ADR('#P#'));
			FOR xcnt:= 0 TO (PageDataLen - 1) DO
				SearchMark ACCESS(AdrPageData + xcnt);
				Result:= brsstrcmp(ADR(SearchMark), ADR(Mark));
				IF Result = 0 THEN	(* Mark is found *)
					pUINT ACCESS(AdrPageData + SIZEOF(Mark) + xcnt);	// pointer to the position directly after #P#, there is the pagenumber
					AvailablePages[NumberOfAvailablePages]:= pUINT;
					NumberOfAvailablePages:= NumberOfAvailablePages + 1;
				END_IF
			END_FOR
			Step:= READ_DIRECTJUMPAGES;

		READ_DIRECTJUMPAGES:	(* Read all direct jump pages from the datamodule and fill array *)
			brsstrcpy(ADR(Mark),ADR('#D#'));
			FOR xcnt:= 0 TO (PageDataLen - 1) DO
				SearchMark ACCESS(AdrPageData + xcnt);
				Result:= brsstrcmp(ADR(SearchMark), ADR(Mark));
				IF Result = 0 THEN	(* Mark is found *)
					pUINT ACCESS(AdrPageData + SIZEOF(Mark) + xcnt);
					AvailableJumpPages[NumberOfAvailableJumpPages]:= pUINT;
					NumberOfAvailableJumpPages:= NumberOfAvailableJumpPages + 1;
				END_IF
			END_FOR
			Step:=DATAMODULE_SELECTION;

		READ_ALLOWEDUSERPAGES:	(* Read allowed pages for the actual user level and fill array *)
			(* CHECK_DIRECTJUMP:*)
			IF ActualUserLevel <> TPL_Password.Out.UserLevel THEN
				// Reset the allowed Pages array information
				NumberOfAllowedPages:= 0;
				SearchStep:= 0;
				brsmemset(ADR(AllowedPages),0,SIZEOF(AllowedPages));
				// Change Adress to Datamodule information of template pages
				AdrPageData:= DOInfoPages.pDatObjMem;
				PageDataLen:= DOInfoPages.len;
				// Run Action for evaluating the Allowed pages informationa
				Pages;
				// Change Adress to Datamodule information of machine pages
				AdrPageData:= DOInfoMachine.pDatObjMem;
				PageDataLen:= DOInfoMachine.len;
				// Run Action for evaluating the Allowed pages informationa
				Pages;
				ActualUserLevel:= TPL_Password.Out.UserLevel;
				FirstCallOver:= 1;
			END_IF

			(* if page is changed or password, control for access *)
			IF (TPL_Pageh.Out.ActualPage <> TPL_Pageh.In.SwitchToPage OR SecondTurn <> 0) AND TPL_Pageh.Out.ActualPage <> 0 THEN
				FOR ycnt:= 0 TO NumberOfAvailablePages - 1 DO (* Check if page exist *)
					IF DirectJumpCompletion = 1 THEN (* check if a direct jump is activ *)
						IF AvailablePages[ycnt] = TPL_Pageh.In.SwitchToPage THEN
							IF AvailableJumpPages[ycnt] <> 0 THEN	// page exists and is accessible
								PageExists:= 1;
								DirectJumpCompletion:= 0;
								EXIT;
							ELSE	// page exists, but is not accessible
								PageExists:= 0;
								ScreenNotAccessable:= 1;
								DirectJumpCompletion:= 0;
								EXIT;
							END_IF
						ELSIF ycnt = NumberOfAvailablePages - 1 THEN	// page does NOT exist
							PageExists:= 0;
							DirectJumpCompletion:= 0;
						END_IF
					ELSIF AvailablePages[ycnt] = TPL_Pageh.In.SwitchToPage THEN	// no direct jump, but page exists
						PageExists:= 1;
						EXIT;
					ELSIF ycnt = NumberOfAvailablePages - 1 THEN	// page does NOT exist
						PageExists:= 0;
					END_IF
				END_FOR

				IF PageExists = 1 THEN
					FOR xcnt:= 0 TO NumberOfAllowedPages - 1 DO
						IF SecondTurn <> 0 THEN
						(* Change of page after password is filled in *)
							IF AllowedPages[xcnt] = TPL_Pageh.In.SwitchToPage THEN  (* Replaced FTG *)
								SecondTurn:= 0;
								TPL_Pageh.Out.SwitchToPage:= TPL_Pageh.In.SwitchToPage;  (* Replaced FTG *)
								EXIT;
							(*  *)
							ELSIF xcnt = NumberOfAllowedPages - 1 AND TPL_Password.InOut.SearchComplete = 1 THEN
								TPL_Password.InOut.SearchComplete:= 0;
								LastPage:= TPL_Pageh.Out.ActualPage;
								KindOfDialog:= WRONG_PASSWORD;
								Step:= CALL_DIALOG;
								EXIT;
							ELSIF xcnt = NumberOfAllowedPages - 1 AND TPL_Password.InOut.SearchComplete = 2 THEN (* Wrong password for this page*)
								TPL_Password.InOut.SearchComplete:= 0;
								TPL_Pageh.In.SwitchToPage:= TPL_Pageh.Out.ActualPage;  (* Replaced FTG *)
								SecondTurn:= 0;
								EXIT;
							END_IF
						(* Page is changed without password change *)
						ELSE
							IF ByPassReturnButton = 1 THEN	(* If we go one level higher, no password should be necessary *)
								IF TPL_Pageh.Out.ActualPage <> TPL_PAGE_ALARM_ACTUAL AND TPL_Pageh.Out.ActualPage <> TPL_PAGE_ALARM_HISTORY THEN
									BackFromAlarming:= TPL_Pageh.Out.ActualPage;
								END_IF
								LastPage:= TPL_Pageh.In.SwitchToPage; 
								TPL_Pageh.Out.SwitchToPage:= TPL_Pageh.In.SwitchToPage;  (* Replaced FTG *)
								ByPassReturnButton := 0;
								EXIT;
							ELSIF AllowedPages[xcnt] = TPL_Pageh.In.SwitchToPage AND ByPassReturnButton <> 1 THEN	(* Page is found with sufficient password level *)
								IF TPL_Pageh.Out.ActualPage <> TPL_PAGE_ALARM_ACTUAL AND TPL_Pageh.Out.ActualPage <> TPL_PAGE_ALARM_HISTORY THEN
									BackFromAlarming:= TPL_Pageh.Out.ActualPage;
								END_IF
								LastPage:= TPL_Pageh.In.SwitchToPage;
								TPL_Pageh.Out.SwitchToPage:= TPL_Pageh.In.SwitchToPage;
								EXIT;
							ELSIF xcnt = NumberOfAllowedPages -1 THEN	// it's not allowed to change to this page
								(* avoid the password/login popup dialog activation as long as a USB MasterKey is detected *)
								IF (TPL_Password.In.Cfg.Login.LoginSystem = TPL_ENABLED) AND (TPL_IsoLoginSys_Global.Out.Stat.stIsoLoginSystemMasterKeyFound = TRUE) THEN
								(* Password level not sufficient but CFS Login System USBmasterkey detected -> no page change allowed *)
									TPL_Pageh.In.SwitchToPage:= TPL_Pageh.Out.ActualPage; (* force page-change request to keep old page *)  (* Replaced FTG *)
									Step:= CALL_DIALOG; (* V2.03.2 added *)
									EXIT;
								ELSE
								(* Password level not sufficient *)
									LastPage:= TPL_Pageh.Out.ActualPage;
									KindOfDialog:= ACCESS_NOT_ALLOWED;
									Step:= CALL_DIALOG;
									EXIT;
								END_IF;
							END_IF
						END_IF
					END_FOR

				(* Page is not existing *)
				ELSE
					IF ScreenNotAccessable = 1 THEN
						LastPage:= TPL_Pageh.Out.ActualPage;
						KindOfDialog:= PAGE_NOT_ACCESSIBLE;
						ScreenNotAccessable:= 0;
						Step:= CALL_DIALOG;
					ELSE
						LastPage:= TPL_Pageh.Out.ActualPage;
						KindOfDialog:= SCREEN_NOT_AVAILABLE;
						Step:= CALL_DIALOG;
					END_IF
				END_IF
			END_IF

			(* jump back from the alarm pages *)
			IF ButBackFromAlarming = 1 THEN
				ButBackFromAlarming:= 0;
				TPL_Pageh.In.SwitchToPage:= BackFromAlarming;  (* Replaced FTG *)
			END_IF

		CALL_DIALOG:
			(* define text for the different messages *)
			IF	KindOfDialog = WRONG_PASSWORD THEN
				DialogInfo.HeaderTextNumber:= DHT_PAGE_HANDLING;
				DialogInfo.DialogTextNumber:= DT_WRONG_PASSWORD_FOR_PAGE;
				DialogInfo.NumberOfButton:= 1;
				DialogInfo.ButtonTextNumberOne:= DBT_OK;
			ELSIF KindOfDialog = SCREEN_NOT_AVAILABLE THEN
				DialogInfo.HeaderTextNumber:= DHT_PAGE_HANDLING;
				DialogInfo.DialogTextNumber:= DT_SCREEN_NOT_AVAILABLE;
				DialogInfo.NumberOfButton:= 1;
				DialogInfo.ButtonTextNumberOne:= DBT_OK;
			ELSIF KindOfDialog = ACCESS_NOT_ALLOWED THEN
	(* ~~~~~CFS ISO Login System V2.03.1/V2.03.2, modified, begin *)
				IF TPL_IsoLoginSys_Global.Out.Stat.stIsoLoginSystemMasterKeyFound THEN
					(* Master key found => just fire an info dialog, no manual login action trigger possible *)
					DialogInfo.HeaderTextNumber:= DHT_PAGE_HANDLING;
					DialogInfo.DialogTextNumber:= DT_WRONG_USER_LEVEL;
					DialogInfo.NumberOfButton:= 1;
					DialogInfo.ButtonTextNumberOne:= DBT_OK;
				(* Check for CFS Login system deactive OR CFS Login system active but Login mode = MODE Standard: => Normal handling showing LOGIN button *)
				ELSIF (TPL_Password.In.Cfg.Login.LoginSystem = TPL_DISABLED)
				   OR ((TPL_Password.In.Cfg.Login.LoginSystem = TPL_ENABLED) AND (TPL_Password.In.Cfg.Login.LoginSystemMode = CFS_LOGIN_SYSTEM_MODE_STANDARD)) THEN
					(* STANDARD Handling: Show Dialog offering LOGIN button or Cancel action *)
					DialogInfo.HeaderTextNumber:= DHT_PASSWORD;
					DialogInfo.DialogTextNumber:= DT_ENTER_PASSWORD;
					DialogInfo.NumberOfButton:= 2;
					DialogInfo.ButtonTextNumberOne:= DBT_LOGIN;
					DialogInfo.ButtonTextNumberTwo:= DBT_CANCEL;
				ELSE
					(* CFS ISO LOGIN SYSTEM Handling: Show Dialog offering just OK button; insufficient level for this page; no LOGIN action possible *)
					DialogInfo.HeaderTextNumber:= DHT_PAGE_HANDLING;
					DialogInfo.DialogTextNumber:= DT_WRONG_USER_LEVEL;
					DialogInfo.NumberOfButton:= 1;
					DialogInfo.ButtonTextNumberOne:= DBT_OK;
				END_IF;
	(* ~~~~~CFS ISO Login System V2.03.1/V2.03.2, modified, end *)
			ELSIF KindOfDialog = PAGE_NOT_ACCESSIBLE THEN							(* Text output for limitation *)
				DialogInfo.HeaderTextNumber:= DHT_PAGE_HANDLING;
				DialogInfo.DialogTextNumber:= DT_SCREEN_NOT_ASSESSABLE;
				DialogInfo.NumberOfButton:= 1;
				DialogInfo.ButtonTextNumberOne:= DBT_OK;
			END_IF

			(* call dialog function *)
			Status.Dialog:= CallDialog(ADR(DialogIdent),ADR(DialogInfo),TPL_Dialogh.Out.AdrDialogRingBuffer);

			IF DialogInfo.Return_PressedButton = 1 AND KindOfDialog = WRONG_PASSWORD THEN
				TPL_Pageh.In.SwitchToPage:= LastPage;  (* Replaced FTG *)
				SecondTurn:= 0;
				Step:= READ_ALLOWEDUSERPAGES;
			END_IF

			IF DialogInfo.Return_PressedButton = 1 AND KindOfDialog = SCREEN_NOT_AVAILABLE THEN
				TPL_Pageh.In.SwitchToPage:= LastPage;  (* Replaced FTG *)
				Step:= READ_ALLOWEDUSERPAGES;
			END_IF

			IF DialogInfo.Return_PressedButton = 1 AND KindOfDialog = ACCESS_NOT_ALLOWED THEN
	(* ~~~~~CFS ISO Login System, V2.03.1/V2.03.2 modified, begin *)
				IF TPL_IsoLoginSys_Global.Out.Stat.stIsoLoginSystemMasterKeyFound THEN
					(* MasterKey found => Back to caller page; back into step "READ_ALLOWEDUSERPAGES"; no wait for input action *)
					TPL_Pageh.In.SwitchToPage:= LastPage;  (* Replaced FTG *)
					Step:= READ_ALLOWEDUSERPAGES;
				(* Check for CFS Login system deactive OR CFS Login system active but Login mode = MODE Standard: => Normal handling showing LOGIN button *)
				ELSIF (TPL_Password.In.Cfg.Login.LoginSystem = TPL_DISABLED)
				   OR ((TPL_Password.In.Cfg.Login.LoginSystem = TPL_ENABLED) AND (TPL_Password.In.Cfg.Login.LoginSystemMode = CFS_LOGIN_SYSTEM_MODE_STANDARD)) THEN
					(* STANDARD Handling: Goto "Wait entered password and open alpha touch pad *)
					TPL_Password.In.Cmd.OpenPasswordAlphaPad:= 1;
					TPL_Password.InOut.SearchComplete:= 0;
					Step:= WAIT_ENTERED_PASSWORD;
				ELSE
					(* CFS ISO LOGIN SYSTEM Handling: Back to caller page; back into step "READ_ALLOWEDUSERPAGES" *)
					TPL_Pageh.In.SwitchToPage:= LastPage;  (* Replaced FTG *)
					Step:= READ_ALLOWEDUSERPAGES;
				END_IF;
	(* ~~~~~CFS ISO Login System, V2.03.1/V2.03.2 modified, end *)
			END_IF

			IF DialogInfo.Return_PressedButton = 2 AND KindOfDialog = ACCESS_NOT_ALLOWED THEN
	(* ~~~~~CFS ISO Login System, V2.03.1/V2.03.2 modified, begin *)
				IF TPL_IsoLoginSys_Global.Out.Stat.stIsoLoginSystemMasterKeyFound THEN
					(* MasterKey found => STANDARD Handling: Return to "READ_ALLOWEDUSERPAGES" step *)
					TPL_Pageh.In.SwitchToPage:= LastPage;  (* Replaced FTG *)
					Step:= READ_ALLOWEDUSERPAGES;
				(* Check for CFS Login system deactive OR CFS Login system active but Login mode = MODE Standard: => Normal handling showing LOGIN button *)
				ELSIF (TPL_Password.In.Cfg.Login.LoginSystem = TPL_DISABLED)
				   OR ((TPL_Password.In.Cfg.Login.LoginSystem = TPL_ENABLED) AND (TPL_Password.In.Cfg.Login.LoginSystemMode = CFS_LOGIN_SYSTEM_MODE_STANDARD)) THEN
					(* STANDARD Handling: Return to "READ_ALLOWEDUSERPAGES" step *)
					TPL_Pageh.In.SwitchToPage:= LastPage;  (* Replaced FTG *)
					Step:= READ_ALLOWEDUSERPAGES;
				END_IF;
	(* ~~~~~CFS ISO Login System, V2.03.1/V2.03.2 modified, end *)
			END_IF

			IF DialogInfo.Return_PressedButton = 1 AND KindOfDialog = PAGE_NOT_ACCESSIBLE THEN
			   TPL_Pageh.In.SwitchToPage:= LastPage;  (* Replaced FTG *)
   			   Step:= READ_ALLOWEDUSERPAGES;
			END_IF


		WAIT_ENTERED_PASSWORD:

			IF TPL_Password.InOut.SearchComplete <> 255 AND TPL_Password.InOut.SearchComplete <> 0 THEN
				SecondTurn:= 1;
				Step:= READ_ALLOWEDUSERPAGES;
			ELSIF TPL_Password.Out.StatusPasswordInput = 16384 AND TPL_Password.In.Cmd.OpenPasswordAlphaPad = 0 AND TPL_Password.InOut.SearchComplete = 0 THEN
				TPL_Pageh.Out.ActualPage:= LastPage;
				TPL_Pageh.In.SwitchToPage:= LastPage;  (* Replaced FTG *)
				Step:=READ_ALLOWEDUSERPAGES;
			END_IF
	END_CASE
END_IF

(* ********************************************** *)
(* 			generate life signal if visu runs	  *)
(* ********************************************** *)
PanelLifeSign;															(* has to be connected to life sign datapoint in visu *)
TOF_PanelLife(IN := (PanelLifeSign > PanelLifeSignOld), PT := t#1000ms);	(* if lifesign is not incremented for 1 sek-> visu is dead *)
TON_PanelLife(IN:=TOF_PanelLife.Q, PT:= t#500ms);							(* if lifesign is incremented for 1/2 sek-> visu is life *)
PanelLifeSignOld := PanelLifeSign;
PanelIsLife := TON_PanelLife.Q;
TPL_Pageh.Out.PanelIsAlive:=PanelIsLife;

(* ********************************************** *)
(* 				TOUCH CALIBRATION				  *)
(* ********************************************** *)
IF EDGEPOS(PanelIsLife) AND (TPL_Config.Out.PanelType <> TPL_X20_VCWT) THEN
	DoTouchCalibration := TouchCalState <> 1;	
END_IF
TON_StartUpBitmap(IN:= PanelIsLife, PT:=T#6s);
TON_StartTouchCalibration(IN:= StartTouchCalibrationTimer, PT:=T#2s);	// StartTouchCalibrationTimer will be activated by momentary datapoint on Startup-Layer

IF(TON_StartTouchCalibration.Q OR DoTouchCalibration) AND NOT StartTouchCalibration THEN	// If pressed 2s on Startup-Layer or no calibration found on startup
	StartTouchCalibration := TRUE;
END_IF

IF TPL_Config.Out.StartUpFinished THEN
	IF StartTouchCalibration THEN	// we need to calibrate touch on startup
		MainPageStartUpLayer				:= TPL_INVISIBLE;
		MainPageTouchCalibrationLayer		:= TPL_VISIBLE;
		IF TouchCalibrationStarted THEN	// wait, until the touchcalibration has started
			MainPageTouchCalibrationLayer	:= TPL_INVISIBLE;
			StartTouchCalibration			:= FALSE;
			StartTouchCalibrationTimer		:= FALSE;
			DoTouchCalibration				:= FALSE;
		END_IF
	ELSIF TON_StartUpBitmap.Q THEN
		MainPageStartUpLayer				:= TPL_INVISIBLE;
		MainPageTouchCalibrationLayer		:= TPL_INVISIBLE;
	END_IF
END_IF
END_PROGRAM
