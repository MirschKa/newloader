(************************************************************************************************************************)
(* Object name: dlg_IF                                                                                                  *)
(* Author:      Samuel Otto                                                                                             *)
(* Site:        B&R Bad Homburg                                                                                         *)
(* Created:     08-feb-2011                                                                                             *)
(* Restriction: consider task order.                                                                                    *)
(* Description: Opens one dialog with Interface.                                                                        *)
(*                                                                                                                      *)
(*----------------------------------------------------------------------------------------------------------------------*)
(* Additional information:                                                                                              *)
(*                                                                                                                      *)
(* No additional information available.                                                                                 *)
(*                                                                                                                      *)
(*----------------------------------------------------------------------------------------------------------------------*)
(* Version 3.00.0  18-jan-2012  Samuel Otto                     Development tool: B&R Automation Studio V3.0.90.19 SP01 *)
(*                                                                                                                      *)
(* Cleaned up this revision history and set version to V3.00.0                                                          *)
(*--------------------------------------------------------------------------------------------------------------------- *)
(* Version 3.00.1  25-oct-2013                     Development tool: B&R Automation Studio V3.0.90.19 SP01 				*)
(* TPL_DialogIF.Out.Info.Busy ist also true while Step <> STEP_WAIT 													*)
(*--------------------------------------------------------------------------------------------------------------------- *)
(************************************************************************************************************************)

PROGRAM _INIT
(************************************************************************************************************************)
(* BEGIN Sample																										 	*)
(************************************************************************************************************************)
	TPL_DialogIF.In.Para.HeaderTextGroupNumber	:= TPL_DIALOG_HEADERTEXT;
	TPL_DialogIF.In.Para.DialogTextGroupNumber	:= TPL_DIALOG_TEXT;
	TPL_DialogIF.In.Para.ButtonTextGroupNumber	:= TPL_DIALOG_BUTTONTEXT;
	TPL_DialogIF.In.Para.HeaderTextNumber		:= DHT_PASSWORD;
	TPL_DialogIF.In.Para.DialogTextNumber		:= DT_ENTER_PASSWORD;
	TPL_DialogIF.In.Para.NumberOfButton			:= 2;
	TPL_DialogIF.In.Para.ButtonTextNumberOne	:= DBT_NO;
	TPL_DialogIF.In.Para.ButtonTextNumberTwo	:= DBT_YES;
	TPL_DialogIF.In.Para.ProgressViewRequired	:= FALSE;
	TPL_DialogIF.In.Para.AdrProgressValue		:= ADR(ProgressValue);
	ProgressValue := 30;
(************************************************************************************************************************)
(* END Sample																										 	*)
(************************************************************************************************************************)
	Step := STEP_WAIT;
END_PROGRAM

PROGRAM _CYCLIC
	CASE Step OF
		STEP_WAIT:
			IF TPL_DialogIF.In.Cmd.OpenDialog THEN
				Step := STEP_DIALOG_CALL;
			END_IF
			TPL_DialogIF.Out.Info.Status := 0;
			TPL_DialogIF.Out.Info.PressedButton := 0;

		STEP_DIALOG_CALL:	(* call dialog function *)
			CallDialog(ADR(DialogIdent), ADR(TPL_DialogIF.In.Para), TPL_Dialogh.Out.AdrDialogRingBuffer);
			TPL_DialogIF.Out.Info.Status := 0; (* If no button ist pressed, the function CallDialog returns value 500. But in this case, the specification
													says that TPL_DialogIF.Out.Info.Status has to be 0. *)
			IF TPL_DialogIF.In.Para.Return_PressedButton <> 0 THEN
				TPL_DialogIF.Out.Info.PressedButton := TPL_DialogIF.In.Para.Return_PressedButton;
				Step	:= STEP_DIALOG_END;
			ELSIF NOT(TPL_DialogIF.In.Cmd.OpenDialog) THEN
				Step	:= STEP_DIALOG_RESET;
			END_IF

		STEP_DIALOG_RESET:	(* reset dialog *)
			(* If now still an older dialog is shown, the IF-Dialog is still waiting in the background. In this case, the
				function ResetDialog can't close the waiting IF-Dialog. If this happens, status 600 (from DialogReset) is
				shown and busy is true, until the dialog is colsed. *)
			TPL_DialogIF.Out.Info.Status := ResetDialog(ADR(DialogIdent), TPL_Dialogh.Out.AdrDialogRingBuffer);
			IF TPL_DialogIF.Out.Info.Status = 0 THEN
				Step	:= STEP_DIALOG_END;
			ELSIF TPL_DialogIF.Out.Info.Status = 600 THEN
				(* Do nothing, wait until the dialog is closed. Therefore all older dialogs have to be closed, ResetDialog will close the IF-Dialog *)
			END_IF
		
		STEP_DIALOG_END:	(* Dialog is closed, wait until TPL_DialogIF.In.Cmd.OpenDialog is FALSE *)
			IF NOT(TPL_DialogIF.In.Cmd.OpenDialog) THEN
				Step	:= STEP_WAIT;
				TPL_DialogIF.Out.Info.PressedButton := 0;
			END_IF
	END_CASE
	TPL_DialogIF.Out.Info.Busy := TPL_DialogIF.In.Cmd.OpenDialog OR TPL_DialogIF.Out.Info.Status = 600 OR Step <> STEP_WAIT ;
END_PROGRAM