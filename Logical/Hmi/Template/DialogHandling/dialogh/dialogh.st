(************************************************************************************************************************)
(* Object name: dialogh                                                                                                 *)
(* Author:      Roman Haslinger                                                                                         *)
(* Site:        B&R Eggelsberg                                                                                          *)
(* Created:     12-aug-2003                                                                                             *)
(* Restriction: consider task order.                                                                                    *)
(* Description: Dialog handling.                                                                                        *)
(*                                                                                                                      *)
(*----------------------------------------------------------------------------------------------------------------------*)
(* Additional information:                                                                                              *)
(*                                                                                                                      *)
(* No additional information available.                                                                                 *)
(*                                                                                                                      *)
(*----------------------------------------------------------------------------------------------------------------------*)
(* Version 3.00.0  18-jan-2012  Samuel Otto                     Development tool: B&R Automation Studio V3.0.90.19 SP01 *)
(*                                                                                                                      *)
(* Cleaned up this revision history and set version to V3.00.0                                                          *)
(*--------------------------------------------------------------------------------------------------------------------- *)
(* Version 3.00.1  04-apr-2012  Frank Thomas Greeb              Development tool: B&R Automation Studio V3.0.90.19 SP01 *)
(* in DialogDrawStep = 1 chek if NumberOfButtons is OK before setting something visible                                 *)
(*--------------------------------------------------------------------------------------------------------------------- *)
(* Version 3.01.0  19-sep-2012  Frank Thomas Greeb              Development tool: B&R Automation Studio V3.0.90.21 SP03 *)
(* - new config PV : TPL_Dialogh.In.Cfg.UseSharedTxtGrp (BOOL)                                                          *)
(*   if set than bit 31 in text group numbers are set =>  drawdialog uses text group from shared resources              *)
(************************************************************************************************************************)
(* Version 3.04.1  16-apr-2015  SO                              Development tool: B&R Automation Studio V3.0.90.28 SP10 *)
(* [#465] Make message in dialog box available																			*)
(* 		- Output two texts:																								*)
(*			TPL_Dialogh.Out.MessageTextActual:	Shows exacty the text of the dialogbox. If the dialogbox is closed, no	*)
(*											  	text will be shown														*)
(*			TPL_Dialogh.Out.MessageTextLast:	Always shows the last (or actual) shown dialog, also if the dialog is	*)
(*												already closed															*)
(************************************************************************************************************************)

PROGRAM _INIT
	TPL_DialogRingBuffer.RingBufferSize := SIZEOF(TPL_DialogRingBuffer.DialogInformation) / SIZEOF(TPL_DialogRingBuffer.DialogInformation[0]);
	TPL_Dialogh.Out.AdrDialogRingBuffer := ADR(TPL_DialogRingBuffer);

	DrawDialog_DialogStep := 0;
	DialogVC.Status :=					TPL_INVISIBLE;
	DialogVC.StatusButton[0] :=			TPL_INVISIBLE;
	DialogVC.StatusButton[1] :=			TPL_INVISIBLE;
	DialogVC.StatusButton[2] :=			TPL_INVISIBLE;
	DialogVC.StatusProgressView :=		TPL_INVISIBLE;
END_PROGRAM

PROGRAM _CYCLIC
(************************************)
(* Set busy status					*)
(************************************)
	IF (TPL_DialogRingBuffer.ActualDrawedDialog <> TPL_DialogRingBuffer.LastToDrawDialog OR DialogDrawStep <> 0) AND TPL_Pageh.Out.ActualPage <> TPL_PAGE_CLNG2 THEN
		TPL_Dialogh.Out.DialogHandlingWorking:= 1;
	ELSE
		TPL_Dialogh.Out.DialogHandlingWorking:= 0;
	END_IF

	(************************************)
	(* Initialize a new dialog			*)
	(************************************)
	IF DialogDrawStep = 0 AND TPL_DialogRingBuffer.ActualDrawedDialog <> TPL_DialogRingBuffer.LastToDrawDialog THEN
		// Increment ActualDrawedDialog. If DrawDialog won't be ready, ActualDrawedDialog will be decremented later
		IF (TPL_DialogRingBuffer.ActualDrawedDialog < (TPL_DialogRingBuffer.RingBufferSize - 1)) THEN
			TPL_DialogRingBuffer.ActualDrawedDialog:= TPL_DialogRingBuffer.ActualDrawedDialog + 1;
		ELSE
			TPL_DialogRingBuffer.ActualDrawedDialog:= 1;
		END_IF

		// Read all texts with Visapi from the visu
		//Status := DrawDialog(TPL_Pageh.Out.VC_Handle,ADR(TPL_DialogRingBuffer.DialogInformation[TPL_DialogRingBuffer.ActualDrawedDialog]), ADR(DlgTxt), ADR(DrawDialog_DialogStep), TPL_Dialogh.In.Cfg.UseSharedTxtGrp);
		//IF (Status = 0) THEN
			(* copy texts from library into variables connected to the visu-elements *)
			brsmemcpy(ADR(DialogVC.IndexHeader),	ADR(DlgTxt.txtHeader),	SIZEOF(DialogVC.IndexHeader));
			brsmemcpy(ADR(DialogVC.IndexText),		ADR(DlgTxt.txtBody),	SIZEOF(DialogVC.IndexText));
			brsmemcpy(ADR(DialogVC.IndexButton1),	ADR(DlgTxt.txtBtn1),	SIZEOF(DialogVC.IndexButton1));
			brsmemcpy(ADR(DialogVC.IndexButton2),	ADR(DlgTxt.txtBtn2),	SIZEOF(DialogVC.IndexButton2));
			brsmemcpy(ADR(DialogVC.IndexButton3),	ADR(DlgTxt.txtBtn3),	SIZEOF(DialogVC.IndexButton3));
			DialogVC.NumberOfButtons:= TPL_DialogRingBuffer.DialogInformation[TPL_DialogRingBuffer.ActualDrawedDialog].NumberOfButton;
			DialogDrawStep:= 1;
			DialogVC.PressedButton:= 0;	
			brsmemcpy(ADR(TPL_Dialogh.Out.MessageTextActual),ADR(DlgTxt.txtBody),SIZEOF(TPL_Dialogh.Out.MessageTextActual)); // Output Messagetext e.g. for Linecontrol
			brsmemcpy(ADR(TPL_Dialogh.Out.MessageTextLast),ADR(DlgTxt.txtBody),SIZEOF(TPL_Dialogh.Out.MessageTextLast)); // Output Messagetext e.g. for Linecontrol
		//ELSE
			// Decrement ActualDrawedDialog, because DrawDialog was not ready. So call DrawDialog again in the next cycle
		//	TPL_DialogRingBuffer.ActualDrawedDialog := TPL_DialogRingBuffer.ActualDrawedDialog - 1;		
		//END_IF	
		
		(************************************)
		(* Initialize the visu Dialog-box	*)
		(************************************)
	ELSIF DialogDrawStep = 1 THEN
		(*          [..Button1..]        *)
		(*  [..Button1..]  [..Buton2..]  *) // all 6 Buttons are in the same y-position!
		(* [Button1] [Button2] [Button3] *)
		DialogVC.Status:= TPL_VISIBLE;	// Show all "basic"-elements of the Dialogbox
		// If the PrograssValue is required and the adress is valid, show it. Otherwise connect the zero-vaule and don't show the progressbar
		IF  TPL_DialogRingBuffer.DialogInformation[TPL_DialogRingBuffer.ActualDrawedDialog].ProgressViewRequired = TRUE AND TPL_DialogRingBuffer.DialogInformation[TPL_DialogRingBuffer.ActualDrawedDialog].AdrProgressValue <> 0 THEN
			DialogVCProgressValue ACCESS TPL_DialogRingBuffer.DialogInformation[TPL_DialogRingBuffer.ActualDrawedDialog].AdrProgressValue;
			DialogVC.StatusProgressView:= TPL_VISIBLE;
		ELSE
			DialogVCProgressValue ACCESS ADR(PROGRESS_VIEW_ZERO);
			DialogVC.StatusProgressView:= TPL_INVISIBLE;
		END_IF
		DialogVC.StatusButton[0]:= TPL_INVISIBLE;
		DialogVC.StatusButton[1]:= TPL_INVISIBLE;
		DialogVC.StatusButton[2]:= TPL_INVISIBLE;
		IF (DialogVC.NumberOfButtons > 0) AND (DialogVC.NumberOfButtons <=3) THEN	(* check NumberOfButtons *)
			DialogVC.StatusButton[DialogVC.NumberOfButtons-1] := TPL_VISIBLE;
		END_IF
		
		// External NG-HMI interface, to get current state of displayed dialog
		ExtDialogIF(TPL_Dialogh.Out.AdrDialogRingBuffer,ADR(TPL_ExtDialogIF.Out.Status),0); 
		
		DialogDrawStep := 2;

		(************************************)
		(* Wait for pressed Buttons			*)
		(************************************)
	ELSIF DialogDrawStep = 2 THEN
		
		// External NG-HMI interface, to get current state of displayed dialog
		ExtDialogIF(TPL_Dialogh.Out.AdrDialogRingBuffer,ADR(TPL_ExtDialogIF.Out.Status),0);
		
		IF DialogVC.PressedButton <> 0 OR TPL_ExtDialogIF.In.Cmd.PressButton <> 0 THEN
			IF DialogVC.PressedButton <> 0 THEN
				TPL_DialogRingBuffer.DialogInformation[TPL_DialogRingBuffer.ActualDrawedDialog].Return_PressedButton:= DialogVC.PressedButton;
			ELSIF TPL_ExtDialogIF.In.Cmd.PressButton <> 0 THEN
				TPL_DialogRingBuffer.DialogInformation[TPL_DialogRingBuffer.ActualDrawedDialog].Return_PressedButton:= TPL_ExtDialogIF.In.Cmd.PressButton;
			END_IF
			DialogVC.Status:=				TPL_INVISIBLE;
			DialogVC.StatusButton[0]:=		TPL_INVISIBLE;
			DialogVC.StatusButton[1]:=		TPL_INVISIBLE;
			DialogVC.StatusButton[2]:=		TPL_INVISIBLE;
			DialogVC.StatusProgressView:=	TPL_INVISIBLE;
			DialogVC.PressedButton:= 0;
			// externes Dialoginterface
			TPL_ExtDialogIF.In.Cmd.PressButton := 0;
			
			DialogDrawStep:= 0;
			
			// External NG-HMI interface, to get current state of displayed dialog
			ExtDialogIF(TPL_Dialogh.Out.AdrDialogRingBuffer,ADR(TPL_ExtDialogIF.Out.Status),1);

			brsmemset(ADR(TPL_Dialogh.Out.MessageTextActual),0 ,SIZEOF(TPL_Dialogh.Out.MessageTextActual)); // Delete Messagetext
			
			(* for function reset dialog *)
		ELSIF TPL_DialogRingBuffer.DialogInformation[TPL_DialogRingBuffer.ActualDrawedDialog].Return_PressedButton = 255 THEN
			ExtDialogIF(TPL_Dialogh.Out.AdrDialogRingBuffer,ADR(TPL_ExtDialogIF.Out.Status),1);
			DialogVC.PressedButton:= 255;
		END_IF
	END_IF
END_PROGRAM

