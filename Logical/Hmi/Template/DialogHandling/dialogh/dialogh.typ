
TYPE
	dialogVC_typ : 	STRUCT 
		IndexHeader : ARRAY[0..99] OF UINT;
		IndexText : ARRAY[0..299] OF UINT;
		NumberOfButtons : UINT;
		IndexButton1 : ARRAY[0..99] OF UINT;
		IndexButton2 : ARRAY[0..99] OF UINT;
		IndexButton3 : ARRAY[0..99] OF UINT;
		Status : UINT;
		StatusButton : ARRAY[0..2] OF UINT; (*0=1 Button, 1=2 Buttons, 2=3 Buttons*)
		PressedButton : USINT;
		StatusProgressView : UINT;
	END_STRUCT;
END_TYPE
