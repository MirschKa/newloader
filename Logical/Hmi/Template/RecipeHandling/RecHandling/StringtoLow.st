(************************************************************************************************************************)
(* Object name: StringtoLow                                                                                             *)
(* Author:      Javier Calvillo                                                                                         *)
(* Site:        GEA Food Solutions Kempten                                                                              *)
(* Created:     09-jul-2019                                                                                             *)
(* Restriction:                                                                                                         *)
(* Description: Function for change a string into lower case characters.                                                         *)
(*                                                                                                                      *)
(*----------------------------------------------------------------------------------------------------------------------*)
(* Additional information:                                                                                              *)
(*                                                                                                                      *)
(************************************************************************************************************************)

FUNCTION StringtoLow

	//Reinitialize result string
	StringLow:=StringValueEmpty;
	
	FOR usCounter:=1 TO INT_TO_UINT(LEN(StringInput))  DO
		usStringValue := MID(StringInput,1,usCounter);
		
		// Access each character and return the ASCII value
		pValue ACCESS ADR(usStringValue);
		
		// Replace upper case with lower case character
		IF (pValue >64 AND pValue <91) THEN
		
			// Calculate the corresponding lower character
			pValueLower:=pValue+32;
			
			StringValueLower  ACCESS ADR(pValueLower);
			StringLow:=REPLACE(StringLow,StringValueLower,1,usCounter);
		ELSE
			//Maintain lower case character
			StringValueUpper  ACCESS ADR(pValue);
			StringLow:=REPLACE(StringLow,StringValueUpper,1,usCounter);
		END_IF;			
	END_FOR	
	
END_FUNCTION

