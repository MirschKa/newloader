
FUNCTION StringtoLow : UINT
	VAR_INPUT
		StringInput : STRING[20];
	END_VAR
	VAR_IN_OUT
		StringLow : STRING[20];
	END_VAR
	VAR
		usCounter : UINT;
		usStringValue : STRING[20];
		StringValueLower : REFERENCE TO STRING[20];
		StringValueUpper : REFERENCE TO STRING[20];
		StringValueEmpty : STRING[20];
		pValueLower : UINT;
		pValue : REFERENCE TO UINT;
	END_VAR
END_FUNCTION
