(************************************************************************************************************************)
(* File name:   recipeh_Action.st                                                                                       *)
(* Author:      N�her Tilmann                                                                                           *)
(* Site:        B&R Bad Homburg                                                                                         *)
(* Created:     04-feb-2011                                                                                             *)
(* Restriction: consider task order.                                                                                    *)
(* Description: number 99 in FileRecipeType is the recognition for a deleted recipe which means an empty line           *)
(*              in the listbox                                                                                          *)
(*                                                                                                                      *)
(*----------------------------------------------------------------------------------------------------------------------*)
(* Additional information:                                                                                              *)
(*                                                                                                                      *)
(* No additional information available.                                                                                 *)
(*                                                                                                                      *)
(*----------------------------------------------------------------------------------------------------------------------*)
(* Version 3.00.0  18-jan-2012  Samuel Otto                     Development tool: B&R Automation Studio V3.0.90.19 SP01 *)
(*                                                                                                                      *)
(* Cleaned up this revision historsy and set version in all template-tasks to V3.00.0                                   *)
(*--------------------------------------------------------------------------------------------------------------------- *)
(* Version 3.00.1  04-apr-2012  Frank Thomas Greeb              Development tool: B&R Automation Studio V3.0.90.19 SP01 *)
(* ACTION SortRecipeLbox: interleave IF-statement to check idx before using idx-1                                       *)
(*--------------------------------------------------------------------------------------------------------------------- *)
(* Version 3.00.2  11-apr-2019  Dominik Benner             		Development tool: B&R Automation Studio V4.5.2.102 		*)
(* BugFix [#486]                                      																	*)
(************************************************************************************************************************)

(* Sort recipe listbox to file numbers*)
ACTION SortRecipeLbox: 
	FOR idx:= 0 TO RecipeInternal.MaxRecipes - 1 DO
			IF (RecipeListBox.FileName[idx].FileNumberAscii = '') THEN
				IF (idx>0) THEN		(* check if idx > 0 before using idx-1 *)
					IF (RecipeListBox.FileName[idx-1].FileNumberAscii = '9999999999') THEN
						RecipeListBox.FileName[idx-1].FileNumberAscii:= '';
					END_IF
				END_IF
   				EXIT ;
			END_IF
		
			FOR idx2:= (idx + 1) TO RecipeInternal.MaxRecipes - 1 DO
			
				IF (RecipeListBox.FileName[idx2].FileNumberAscii = '') THEN
   					EXIT ;
				ELSIF brsatoi(ADR(RecipeListBox.FileName[idx2].FileNumberAscii)) < brsatoi(ADR(RecipeListBox.FileName[idx].FileNumberAscii))  THEN
						
					brsmemcpy(ADR(RecipePlaceHolder),ADR(RecipeListBox.FileName[idx]), SIZEOF(RecipeListBox.FileName[0]));
					brsmemcpy(ADR(RecipeListBox.FileName[idx]),ADR(RecipeListBox.FileName[idx2]), SIZEOF(RecipeListBox.FileName[0]));
					brsmemcpy(ADR(RecipeListBox.FileName[idx2]),ADR(RecipePlaceHolder), SIZEOF(RecipeListBox.FileName[0]));
					
	   				sortieren:= sortieren + 1;
				END_IF			
				
			END_FOR
		END_FOR		
END_ACTION


ACTION FindFileType: 
	FOR idx:= 0 TO RecipeInternal.MaxRecipes - 1 DO
		IF RecipeListBox.FileName[idx].FileNumberAscii = PtrRecipeToOverwrite THEN
 			IF (RecipeListBox.FileName[idx].FileRecipeType = '1') THEN
				RecipeType1 := TRUE;
				RecipeType2 := FALSE; (* BugFix [#486] *)
			ELSIF (RecipeListBox.FileName[idx].FileRecipeType = '2') THEN
				RecipeType1 := FALSE; (* BugFix [#486] *)
				RecipeType2 := TRUE;
			ELSE
				RecipeType1 := FALSE;
				RecipeType2 := FALSE;
			END_IF
		END_IF	
	END_FOR
END_ACTION			
						
					

