
TYPE
	SC : 
		( (*Step counter recipe handling enumeration type*)
		_0_WAIT := 0,
		_1_RESET_COMMANDS := 1,
		_2_STEP_CONTROL_RECIPE_NAME := 2,
		_3_STEP_FILE_SEARCH := 3,
		_4_STEP_NO_NAME_ENTERED := 4,
		_6_STEP_FILE_WRITE := 6,
		_7_STEP_FILEHANDLING_ERROR := 7,
		_8_STEP_FILE_SELECT := 8,
		_9_STEP_FILE_OPEN := 9,
		_10_STEP_FALSE_STRUCTURE := 10,
		_11_STEP_RESET_DIALOG := 11,
		_12_STEP_DIALOG_FILE_LOAD := 12,
		_13_STEP_FILE_LISTBOX := 13,
		_14_STEP_ASK_OVERWRITE := 14,
		_15_STEP_DIALOG_FILE_SAVE := 15,
		_16_STEP_FILE_CREATE_NEW := 16,
		_18_STEP_ASK_SURE_DELETE := 18,
		_19_STEP_CHANGED_INPUT := 19,
		_20_STEP_DG_ONLINE_DELETE := 20,
		_21_STEP_DG_ONLINE_OVERWR := 21,
		_22_STEP_BASICREC_DELETE := 22,
		_23_STEP_BASICREC_OWERWR := 23,
		_25_STEP_FILE_DELETE := 25,
		_26_STEP_OPEN_NUMPAD := 26,
		_27_STEP_COPY_DATA := 27,
		_28_STEP_SORT := 28,
		_29_STEP_DIALOG_FILE_DELETED := 29,
		_30_STEP_EXT_RECIPE_LOAD := 30,
		_34_STEP_DIRECTORY_INFO := 34,
		_35_STEP_RENEW_FILE_LIST := 35,
		_36_STEP_DIALOG_SELECTION := 36,
		_40_STEP_DT_RESTORE_IN_PROGRESS := 40,
		_41_STEP_DT_LISTBOX_EXEEDED := 41,
		_200_STEP_UPDATE_DATAMODULE := 200,
		_201_STEP_SORT_DATAMODULE := 201,
		_220_STEP_DIALOG_REC_MAX_NUMBER := 220,
		_225_STEP_DIALOG_LISTBOX_EXCEED := 225,
		_226_STEP_DIALOG_DT_MEMORY := 226,
		_227_STEP_DIALOG_REC_NOT_SAVED := 227,
		_228_STEP_DIALOG_REC_LOAD_LOCKED := 228,
		_230_STEP_DIALOG_FILE_ERROR := 230 (*error reading recipe file*)
		);
	CSV_ListFileOption_typ : 	STRUCT 
		SearchVarName : STRING[80];
		SearchVarComment : STRING[80];
		FileLenToRead : UINT;
		MaxNrLen : USINT;
		MaxNameLen : USINT;
	END_STRUCT;
	recipe_internal_type : 	STRUCT 
		CSVFileHeader : csvfileheader;
		CSVStructureName : STRING[32];
		FilehandlingStatus : UINT;
		OnlineStructureName : STRING[32];
		OfflineStructureName : STRING[32];
		NextStep : USINT;
		DataAlreadyChanged : UINT;
		CommandRecipe : USINT;
		TypeRecipe : USINT;
		LastCommandRecipe : USINT;
		OffsetTypeRecipe : USINT;
		ComeFromPage : UINT;
		AdrCSVRecipe : UDINT;
		AdrOnlineRecipe : UDINT;
		AdrOfflineRecipe : UDINT;
		AdrDefaultRecipe : UDINT;
		LenCSVRecipe : UDINT;
		LenOnlineRecipe : UDINT;
		LenOfflineRecipe : UDINT;
		LenDefaultRecipe : UDINT;
		ActualRecipeNumASCII : STRING[20];
		ActualRecipeNumber : UDINT;
		ActualRecipeType : UDINT;
		AdrCSVRecipeStructureName : UDINT;
		InputChanged : UINT;
		FileDevice : STRING[10];
		NumberExistingFiles : UINT;
		SelRecipeNumber : STRING[10];
		SelRecipeType : STRING[2];
		RecipeNumberStatusInverted : UINT; (*Shows and hides a Outputfield, when Input is not possible*)
		RecipeNumberStatus : UINT;
		RecipeNumberColor : UINT;
		MaxRecipes : UINT;
		MaxRecipeTypes : UINT;
		RestorDialogActiv : BOOL;
		BtnGoToOfflieRecipe : UINT; (*Set button offline recipe to invisible*)
	END_STRUCT;
	BackupRecipeData : 	STRUCT 
		Number : UDINT;
	END_STRUCT;
	Page_Type : 	STRUCT 
		ActualPageRead : UINT;
		ActualPageWrite : UINT;
		PanelSize : USINT;
		UserLevel : USINT;
		SwitchToPage : UINT;
		StartUpFinished : BOOL;
	END_STRUCT;
	csvfileheader : 	STRUCT 
		MachineNumber : STRING[30];
		Comment : STRING[30];
		Version : STRING[30];
	END_STRUCT;
	Rec_Filter_vis : 	STRUCT 
		Filterbar : USINT; (*visibility of filter bar*)
		RegularLayer : USINT; (*visibility of regular result list without filter options*)
		FilterLayer : USINT; (*visibility of filter settings layer*)
		ResultList : USINT; (*visibility of resultlist if filter is staid*)
		SearchFieldNumber : USINT; (*visibility of searchfield number*)
		SearchFieldName : USINT; (*visibility of searchfield  name*)
		SearchFieldComment : USINT; (*visibility of searchfield comment*)
		SearchFieldType : USINT; (*visibility of searchfield type*)
	END_STRUCT;
END_TYPE
