(************************************************************************************************************************)
(* Object name: alarmh                                                                                                  *)
(* Author:      Samuel Otto                                                                                             *)
(* Site:        B&R Bad Homburg                                                                                         *)
(* Created:     21-mar-2011                                                                                             *)
(* Restriction: consider task order.                                                                                    *)
(* Description: Alarm handling.                                                                                         *)
(*                                                                                                                      *)
(*----------------------------------------------------------------------------------------------------------------------*)
(* Additional information:                                                                                              *)
(*                                                                                                                      *)
(* No additional information available.                                                                                 *)
(*                                                                                                                      *)
(*----------------------------------------------------------------------------------------------------------------------*)
(* Version 3.00.0  18-jan-2012  Samuel Otto                     Development tool: B&R Automation Studio V3.0.90.19 SP01 *)
(*                                                                                                                      *)
(* Cleaned up this revision history and set version to V3.00.0                                                          *)
(*--------------------------------------------------------------------------------------------------------------------- *)
(*         3.00.1  17-sep-2012  FTG                      Development tool: B&R Automation Studio V3.0.90.21 SP03    	*)
(* default init of config para in INIT changed to allow machine task to predefine                                       *)
(*--------------------------------------------------------------------------------------------------------------------- *)
(*         3.00.2  16-oct-2012  FTG                      Development tool: B&R Automation Studio V3.0.90.21 SP03    	*)
(* TPL_Alarmh.In.ExtQuitAllAlarms  ->  TPL_Alarmh.In.Cmd.ExtQuitAllAlarms  (Cmd-Struct)                                 *)
(*--------------------------------------------------------------------------------------------------------------------- *)
(*         3.00.3  21-nov-2012  FTG                      Development tool: B&R Automation Studio V3.0.90.21 SP03    	*)
(* TPL_Alarmh.Out.Info structure defined an filled (counts and text as unicode)                                         *)
(*--------------------------------------------------------------------------------------------------------------------- *)
(*         3.00.4  21-nov-2013  EC	                     Development tool: B&R Automation Studio V3.0.90.21 SP03    	*)
(* [#73] Reset button on alarm page																						*)
(* 	 - Expand TPL_Alarmh.Out.Stat structure with ResetButtonPressed					                                    *)
(* [#54] Add flag alarm acknowledge																						*)
(*   - Expand TPL_Alarmh.Out.Stat structure with AllAknowledged						                                    *)
(*     Create Signal that to show info, that all Alarms are acknowledge						                            *)
(* [#410] Alarm pop-up dialog																							*)
(*   - With newer VC Runtime function VA_GetAlarmCount is available to get number of alarm counts                       *)
(*     Old alarm count is changed with the new funtion of VA_GetAlarmCount						                        *)
(************************************************************************************************************************)
(* Version 3.04.1  16-apr-2015  SO                              Development tool: B&R Automation Studio V3.0.90.28 SP10 *)
(* [#464] DATETIMEFORMAT in actual alarm message																		*)
(* 		- Constant DATETIMEFORMAT : USINT := 6 changed from 0 ... DD.MM.YY_HH:MM:SS to 6 ... NO_TIME_NO_DATE			*)
(*		  Resason: smaller text for diaplay in Line Control Slicingline													*)
(************************************************************************************************************************)

PROGRAM _INIT
AlarmVC.GlobalStatus				:= TPL_INVISIBLE;
AlarmVC.AckOneAndAllBtnStatus		:= TPL_INVISIBLE;
AlarmVC.AckOneBtnStatus				:= TPL_INVISIBLE;
CASE TPL_Alarmh.In.Cfg.TemplateType OF
	TPL_TEMPLATE_TYPE_MACHINE: 		; // OK
	TPL_TEMPLATE_TYPE_LINECONTROL:	; // OK
ELSE
	TPL_Alarmh.In.Cfg.TemplateType		:= TPL_TEMPLATE_TYPE_MACHINE; // if not correct -> TEMPLATE_TYPE_MACHINE
END_CASE
TPL_Alarmh.In.Cmd.ExtQuitAllAlarms		:= FALSE;
(******* Images for testing Alarmhandling *******)
(************************************************)
TestAlarmImage;
TestAcknowledgeImage;
(************************************************)
(************************************************)
END_PROGRAM

PROGRAM _CYCLIC
// wait until Vc_handle is ready from pagehandling and Config-file is ready
IF TPL_Pageh.Out.VC_Handle = 0 OR TPL_Config.Out.StartUpFinished = FALSE THEN
	RETURN;
END_IF

(****************************************)
(* Count Active and Acknowledged Alarms	*)
(* Get active alarm text				*)
(****************************************)
IF VA_Saccess(1,TPL_Pageh.Out.VC_Handle) = 0 THEN
	// Get the text of the first (act) alarm
	Alarmlen := SIZEOF(Alarmstring) / 2;
	Status := VA_wcGetActAlarmList(1, TPL_Pageh.Out.VC_Handle, ADR(Alarmstring), ADR(Alarmlen), 1 ,SEPERATOR ,DATETIMEFORMAT);
	IF Status = 0 THEN		// There is an alarm and it was read sucessfully
		TPL_Alarmh.Out.Info.ActAlarm := Alarmstring;
	ELSIF Status = 240 THEN	// There are no (more) alarms
		brsmemset(ADR(TPL_Alarmh.Out.Info.ActAlarm),0,SIZEOF (TPL_Alarmh.Out.Info.ActAlarm));	// clear info
	END_IF
	// Count active alarms
	Status := VA_GetAlarmCount(1, TPL_Pageh.Out.VC_Handle, vaALS_ACTIVE, ADR(ActiveAlarmCount));
	Status := VA_GetAlarmCount(1, TPL_Pageh.Out.VC_Handle, vaALS_CURRENT, ADR(CurrentAlarmCount)) AND Status; // Check if all 3 calls of VA_GetAlarmCount have no errors
	Status := VA_GetAlarmCount(1, TPL_Pageh.Out.VC_Handle, vaALS_ACKNOWLEDGED, ADR(AckAlarmCount)) AND Status; // Check if all 3 calls of VA_GetAlarmCount have no errors
	IF Status = 0 THEN
		TPL_Alarmh.Out.Info.NotAckAlarmCount := DINT_TO_UINT(MIN(CurrentAlarmCount - AckAlarmCount, 65535));		// set info var for external use
		TPL_Alarmh.Out.Info.ActAlarmCount    := DINT_TO_UINT(MIN(ActiveAlarmCount,65535));
		END_IF
	VA_Srelease (1,TPL_Pageh.Out.VC_Handle);
END_IF

(************************************)
(* Set visibilities	and status		*)
(************************************)
// Important: The alarm can be visible, if a dialog is shown. An alarm is more prior!
IF TPL_Alarmh.In.Cfg.TemplateType = TPL_TEMPLATE_TYPE_MACHINE AND TPL_Alarmh.Out.Info.NotAckAlarmCount = 1 THEN
	AlarmVC.GlobalStatus			:= TPL_VISIBLE;
	AlarmVC.AckOneBtnStatus			:= TPL_VISIBLE;
	AlarmVC.AckOneAndAllBtnStatus	:= TPL_INVISIBLE;
ELSIF TPL_Alarmh.In.Cfg.TemplateType = TPL_TEMPLATE_TYPE_MACHINE AND TPL_Alarmh.Out.Info.NotAckAlarmCount > 1 THEN
	AlarmVC.GlobalStatus			:= TPL_VISIBLE;
	AlarmVC.AckOneBtnStatus			:= TPL_INVISIBLE;
	AlarmVC.AckOneAndAllBtnStatus	:= TPL_VISIBLE;
ELSE
	AlarmVC.GlobalStatus			:= TPL_INVISIBLE;
	AlarmVC.AckOneBtnStatus			:= TPL_INVISIBLE;
	AlarmVC.AckOneAndAllBtnStatus	:= TPL_INVISIBLE;
END_IF
TPL_Alarmh.Out.Stat.AlarmDialogIsShown := NOT(UINT_TO_BOOL(AlarmVC.GlobalStatus));

(************************************)
(* Quit alarms by external signal	*)
(************************************)
IF TPL_Alarmh.In.Cmd.ExtQuitAllAlarms THEN
	IF VA_Saccess(1, TPL_Pageh.Out.VC_Handle)= 0 THEN
		Status := VA_QuitAlarms(1, TPL_Pageh.Out.VC_Handle, 65535);
		IF Status = 0 THEN
			TPL_Alarmh.In.Cmd.ExtQuitAllAlarms := FALSE;
		END_IF
		VA_Srelease(1, TPL_Pageh.Out.VC_Handle);
	END_IF
END_IF

(************************************)
(* Pulse for flashing alarmbutton	*)
(************************************)
// Generate a pulse train with a cycle time of 2 s and a duty cycle of 50 %.
TON_1(IN :=		Clock_2_sec, PT := T#1s);
TON_2(IN := NOT Clock_2_sec, PT := T#1s);
IF TON_1.Q THEN Clock_2_sec := FALSE; END_IF;
IF TON_2.Q THEN Clock_2_sec := TRUE; END_IF;

(****************************************)
(* Bitmapstatus Alarmbutton (Headline)	*)
(****************************************)
TPL_Alarmh.Out.Stat.AllAcknowledged := FALSE;
IF TPL_Alarmh.Out.Info.NotAckAlarmCount = 0 THEN		// no unacknowledged alarms
	TPL_Alarmh.Out.Stat.AllAcknowledged := TRUE;
	IF ActiveAlarmCount = 0 THEN			// no active alarms -> no alarms
		AlarmVC.ColorAlarmButton := WHITE;
	ELSE									// alarms are active, but all are acknowledged
		AlarmVC.ColorAlarmButton := RED;
	END_IF
ELSIF Clock_2_sec THEN		// unacknowledged alarms exist --> blinking
	AlarmVC.ColorAlarmButton := RED;
ELSE
	AlarmVC.ColorAlarmButton := WHITE;
END_IF

END_PROGRAM
