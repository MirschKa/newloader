
TYPE
	AlarmVC_Type : 	STRUCT 
		GlobalStatus : UINT; (*Status datapoint for the Alarm-Popup (Background, Headline...)*)
		AckOneBtnStatus : UINT; (*Status datapoint for Ack-One-Alarm-Button; If this is shown, the one-and-all-Button must not be shown!*)
		AckOneAndAllBtnStatus : UINT; (*Status datapoint for One and Ack-All-Alarms-Button; If this is shown, the one-Button must not be shown!*)
		ColorAlarmButton : UINT; (*Back-color of Alarmbutton in Headline (0: White, 1: Red). It's the index of a Bitmapgroup*)
	END_STRUCT;
END_TYPE
