//(************************************************************************************************************************)
//(* Object name: ESignature       	                                                                                      *)
//(* Author:      Alexander J�ger                                                                                         *)
//(* Site:        GEA Food Solutions Germany GmbH                                                                         *)
//(* Created:     05-aug-2019                                                                                            *)
//(* Restriction: No restrictions.                                                                                        *)
//(* Description:                																						  *)
//(*                                                                                                                      *)
//(*----------------------------------------------------------------------------------------------------------------------*)
//(* Additional information:                                                                                              *)
//(*                                                                                                                      *)
//(* No additional information available.                                                                                 *)
//(*                                                                                                                      *)
//(*----------------------------------------------------------------------------------------------------------------------*)
//(* Version 1.00.0  05-aug-2019  Alexander J�ger                 Development tool: B&R Automation Studio V4.5.2.102	  *)
//(*                                                                                                                      *)
//(* First Release                                                                                                        *)
//(*--------------------------------------------------------------------------------------------------------------------- *)
//(************************************************************************************************************************)
PROGRAM _INIT
	(* Insert code here *)
StepESignature(Start:= TRUE, TiVa:= NO_TO, Next := INITIAL);

TPL_ESignature.Out.Status := BUSY;
	 
END_PROGRAM

PROGRAM _CYCLIC
	(* Insert code here *)

(* Dialog Info for e-signature *)
DialogInfoESignature.HeaderTextGroupNumber	:= TPL_DIALOG_HEADERTEXT;
DialogInfoESignature.HeaderTextNumber		:= DHT_E_SIGNATURE;
DialogInfoESignature.DialogTextGroupNumber	:= TPL_DIALOG_TEXT;
DialogInfoESignature.ButtonTextGroupNumber	:= TPL_DIALOG_BUTTONTEXT;

CASE StepESignature.State OF

	//***************************************************************************************************************
	//	E_SIGNATURE_INITIAL
	//*************************************************************************************************************** 
	INITIAL:
		TPL_ESignature.Out.Tracer := 'INITIAL';
		IF TPL_Config.Out.StartUpFinished THEN
			StepESignature(Start:= TRUE,TiVa:= NO_TO,Next := WAIT_COMMAND);
		END_IF
	 
	//***************************************************************************************************************
	//	E_SIGNATURE_WAIT_COMMAND
	//***************************************************************************************************************
	WAIT_COMMAND:
		TPL_ESignature.Out.Tracer := 'WAIT_COMMAND';
		TPL_ESignature.Out.Status := 0;

		IF TPL_ESignature.In.Cmd.Compare THEN
			TPL_ESignature.Out.Status := BUSY;
			StepESignature(Start:= TRUE,TiVa:= NO_TO,Next := COMPARE_INITIAL);
		END_IF

	//***************************************************************************************************************
	//	E_SIGNATURE_COMPARE_INITIAL
	//***************************************************************************************************************
	COMPARE_INITIAL:
		TPL_ESignature.Out.Tracer := 'COMPARE_INITIAL';
	
		IF NOT TPL_ESignature.In.Cmd.Compare THEN
			TPL_ESignature.Out.Status := ESIG_VALUE_REQUEST;
			StepESignature(Start:= TRUE,TiVa:= NO_TO,Next := WAIT_VALUE_PRESENT);
		END_IF

	//***************************************************************************************************************
	//	E_SIGNATURE_WAIT_VALUE_PRESENT
	//***************************************************************************************************************
	WAIT_VALUE_PRESENT:
		TPL_ESignature.Out.Tracer := 'WAIT_VALUE_PRESENT';

		IF TPL_ESignature.In.Data.FirstValue <> '' THEN
			TPL_ESignature.Out.Status := BUSY;
			Keypad_Status.3 := TRUE;
			IF Keypad_Status.14 THEN
				StepESignature(Start:= TRUE,TiVa:= NO_TO,Next := WAIT_KEYPAD_CLOSED);
			END_IF
		ELSE
			TPL_ESignature.Out.Status := ESIG_NOTHING_TO_COMPARE;
			DialogInfoESignature.NumberOfButton			:= 1;
			DialogInfoESignature.ButtonTextNumberOne	:= DBT_ACK;
			DialogInfoESignature.DialogTextNumber 		:= DT_ESIG_NO_VALUE_TO_COMP;
			DialogStatusESignature := CallDialog(ADR(DialogIdentESignature), ADR(DialogInfoESignature), TPL_Dialogh.Out.AdrDialogRingBuffer);
	
			IF DialogInfoESignature.Return_PressedButton = 1 THEN
				StepESignature(Start:= TRUE,TiVa:= NO_TO,Next := WAIT_COMMAND);
			END_IF
		END_IF			

	//***************************************************************************************************************
	//	E_SIGNATURE_WAIT_KEYPAD_CLOSED
	//***************************************************************************************************************
	WAIT_KEYPAD_CLOSED:
		TPL_ESignature.Out.Tracer := 'WAIT_KEYPAD_CLOSED';

		IF EDGENEG(Keypad_Status.12) THEN
			StepESignature(Start:= TRUE,TiVa:= NO_TO,Next := COMPARE_VALUES);
		END_IF

	//***************************************************************************************************************
	//	E_SIGNATURE_COMPARE_VALUES
	//***************************************************************************************************************
	COMPARE_VALUES:
		TPL_ESignature.Out.Tracer := 'COMPARE_VALUES';

		IF TPL_ESignature.In.Data.SecondValue <> '' THEN
			IF TPL_ESignature.In.Data.FirstValue = TPL_ESignature.In.Data.SecondValue THEN
				TPL_ESignature.Out.Status := ESIG_MATCH;
				DialogInfoESignature.NumberOfButton			:= 1;
				DialogInfoESignature.ButtonTextNumberOne	:= DBT_ACK;
				DialogInfoESignature.DialogTextNumber 		:= DT_ESIG_VALUES_MATCH;
				DialogStatusESignature := CallDialog(ADR(DialogIdentESignature), ADR(DialogInfoESignature), TPL_Dialogh.Out.AdrDialogRingBuffer);
		
				IF DialogInfoESignature.Return_PressedButton = 1 THEN
					TPL_ESignature.In.Data.SecondValue := '';
					StepESignature(Start:= TRUE,TiVa:= NO_TO,Next := WAIT_COMMAND);
				END_IF
			ELSE
				TPL_ESignature.Out.Status := ESIG_NO_MATCH;
				DialogInfoESignature.NumberOfButton			:= 1;
				DialogInfoESignature.ButtonTextNumberOne	:= DBT_ACK;
				DialogInfoESignature.DialogTextNumber 		:= DT_ESIG_VALUES_NOT_MATCH;
				DialogStatusESignature := CallDialog(ADR(DialogIdentESignature), ADR(DialogInfoESignature), TPL_Dialogh.Out.AdrDialogRingBuffer);
		
				IF DialogInfoESignature.Return_PressedButton = 1 THEN
					TPL_ESignature.In.Data.SecondValue := '';
					StepESignature(Start:= TRUE,TiVa:= NO_TO,Next := WAIT_COMMAND);
				END_IF
			END_IF
		ELSE
			TPL_ESignature.Out.Status := ESIG_NO_INPUT;
			DialogInfoESignature.NumberOfButton			:= 1;
			DialogInfoESignature.ButtonTextNumberOne	:= DBT_ACK;
			DialogInfoESignature.DialogTextNumber 		:= DT_ESIG_NO_INPUT;
			DialogStatusESignature := CallDialog(ADR(DialogIdentESignature), ADR(DialogInfoESignature), TPL_Dialogh.Out.AdrDialogRingBuffer);
	
			IF DialogInfoESignature.Return_PressedButton = 1 THEN
				StepESignature(Start:= TRUE,TiVa:= NO_TO,Next := WAIT_COMMAND);
			END_IF
		END_IF

END_CASE

END_PROGRAM

PROGRAM _EXIT
	(* Insert code here *)
	 
END_PROGRAM

