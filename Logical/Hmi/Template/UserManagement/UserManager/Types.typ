
TYPE
	User_Typ : 	STRUCT 
		AccessDenied : BOOL;
		LoginCounter : UDINT;
		Status : UINT;
		Time : UserTime_Typ;
		Tracer : STRING[80];
		UserLevelTmp : USINT;
		UID_OLD : STRING[16];
		SetAdmin : BOOL;
		UserAllreadyExists : BOOL;
		BackUpNeeded : BOOL;
		tmpString : STRING[2];
	END_STRUCT;
	UserTime_Typ : 	STRUCT 
		NumOfWrongPassword : USINT;
		SystemDateTime : DATE_AND_TIME;
		UserDateTime : DATE_AND_TIME;
		TimeDiff : InternalTimeDiff_Type;
	END_STRUCT;
	InternalTimeDiff_Type : 	STRUCT 
		DateTime : UDINT;
		Days : UDINT;
		Minutes : UDINT;
	END_STRUCT;
END_TYPE
