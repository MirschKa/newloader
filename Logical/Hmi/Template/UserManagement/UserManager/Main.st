//(************************************************************************************************************************)
//(* Object name: UserManager                                                                                             *)
//(* Author:      Florian Becker                                                                                          *)
//(* Site:        GEA Food Solutions Germany GmbH                                                                         *)
//(* Created:     03-june-2019                                                                                            *)
//(* Restriction: No restrictions.                                                                                        *)
//(* Description:                 																						  																					*)
//(*                                                                                                                     	*)
//(*----------------------------------------------------------------------------------------------------------------------*)
//(* Additional information:                                                                                              *)
//(*                                                                                                                      *)
//(* No additional information available.                                                                                 *)
//(*                                                                                                                      *)
//(*----------------------------------------------------------------------------------------------------------------------*)
//(* Version 1.00.0  03-june-2019  Florian Becker                  Development tool: B&R Automation Studio V4.5.2.102     *)
//(*                                                                                                                      *)
//(* First Release                                                                                                        *)
//(*--------------------------------------------------------------------------------------------------------------------- *)
//(************************************************************************************************************************)
PROGRAM _INIT
	(* Insert code here *)
	StepUM(Start:= TRUE, TiVa:= NO_TO, Next :=  INITIAL);

END_PROGRAM
PROGRAM _CYCLIC
	// Get current system DateTime continously
	DTGetTime_0(enable:= TRUE);
	Internal.Time.SystemDateTime := DTGetTime_0.DT1;
	
	IF TPL_Password.In.Cmd.Logout <> 0 THEN
		brsmemset(ADR(TPL_UserManager.Out.Data.UserData),0,SIZEOF(TPL_UserManager.Out.Data.UserData));
		brsmemset(ADR(TPL_UserManager.In.Info.RFID.UID),0,SIZEOF(TPL_UserManager.In.Info.RFID.UID));
		brsmemset(ADR(Internal.UID_OLD),0,SIZEOF(Internal.UID_OLD));
		brsmemset(ADR(TPL_UserManager.In.Info.RFID.TransponderTyp),0,SIZEOF(TPL_UserManager.In.Info.RFID.TransponderTyp));
		brsmemset(ADR(TPL_UserManager.In.Info.RFID.TagName),0,SIZEOF(TPL_UserManager.In.Info.RFID.TagName));
		TPL_UserManager.In.Info.RFID.ServiceTag := 0;
	END_IF
	
	IF TPL_Password.In.Cfg.Login.LoginSystemMode = CFS_LOGIN_SYSTEM_MODE_USERCHECK THEN
		CASE StepUM.State OF
			//***************************************************************************************************************
			//	USERMANAGER_INITIAL
			//***************************************************************************************************************
			INITIAL:
				Internal.Tracer := 'INITIAL';
				brsmemset(ADR(TPL_UserManager.In.Info.RFID.UID),0,SIZEOF(TPL_UserManager.In.Info.RFID.UID));
				brsmemset(ADR(Internal.UID_OLD),0,SIZEOF(Internal.UID_OLD));
				TPL_Password.In.Cmd.Logout := TRUE;
				StepUM(Start:= TRUE, TiVa:= NO_TO, Next :=  IDLE);
			
			//***************************************************************************************************************
			//	USERMANAGER_IDLE
			//***************************************************************************************************************		
			IDLE:
				Internal.Tracer := 'IDLE';
				TPL_UserManager.Out.Cmd.AbortProcess := FALSE;
				IF TPL_UserManager.In.Info.RFID.UID <> Internal.UID_OLD AND TPL_UserManager.In.Info.RFID.UID <> '00000000' THEN
					IF TPL_UserManager.In.Info.RFID.ServiceTag = 1 THEN
						Internal.UID_OLD := TPL_UserManager.In.Info.RFID.UID;
						StepUM(Start:= TRUE, TiVa:= NO_TO, Next :=  CHECK_DATA);
					ELSIF  TPL_UserManager.In.Info.RFID.ServiceTag = 0 THEN
						Internal.UID_OLD := TPL_UserManager.In.Info.RFID.UID;
						StepUM(Start:= TRUE, TiVa:= NO_TO, Next :=  CHECK_USER);
					END_IF
				END_IF
				IF TPL_UserManager.In.Info.HMI_Ctrl.Status = ADD_WAIT_FOR_UID THEN
					StepUM(Start:= TRUE, TiVa:= NO_TO, Next :=  WAIT_CREATE_USER_FINISHED);
				END_IF	
				IF TPL_UserManager.In.Info.HMI_Ctrl.Status = EDIT_USER THEN
					StepUM(Start:= TRUE, TiVa:= NO_TO, Next :=  SEND_UID_DURING_EDIT);
				END_IF	
		
			//***************************************************************************************************************
			//	USERMANAGER_CHECK_USER
			//***************************************************************************************************************	
			CHECK_USER:
				Internal.Tracer := 'CHECK_USER';
				tmp := 0;	
				FOR xcnt :=0 TO MAX_USER DO
					IF TPL_UserManager.InOut.UserDataTmp[xcnt].UID = TPL_UserManager.In.Info.RFID.UID THEN
						Internal.UserAllreadyExists := TRUE;
						tmp := xcnt;
						EXIT;
					END_IF	
				END_FOR
				IF Internal.UserAllreadyExists THEN
					TPL_UserManager.Out.Data.UserData := TPL_UserManager.InOut.UserDataTmp[tmp];
					Internal.UserAllreadyExists:= FALSE;
					StepUM(Start:= TRUE, TiVa:= NO_TO, Next :=  CHECK_DATA);
				ELSE
					StepUM(Start:= TRUE, TiVa:= NO_TO, Next :=  CREATE_USER);
					TPL_UserManager.Out.Data.UserData.UID := TPL_UserManager.In.Info.RFID.UID;
				END_IF	
			
			//***************************************************************************************************************
			//	USERMANAGER_CREATE_USER
			//***************************************************************************************************************	
			CREATE_USER:
				Internal.Tracer := 'CREATE_USER';
				TPL_UserManager.Out.Cmd.AddUser := TRUE;		
				StepUM(Start:= TRUE, TiVa:= NO_TO, Next :=  WAIT_CREATE_USER_FINISHED);

			//***************************************************************************************************************
			//	USERMANAGER_WAIT_CREATE_USER_FINISHED
			//***************************************************************************************************************							
			WAIT_CREATE_USER_FINISHED:
				Internal.Tracer := 'WAIT_CREATE_USER_FINISHED';				
				CASE TPL_UserManager.In.Info.HMI_Ctrl.Status OF
					ADD_READY:
						TPL_UserManager.In.Info.RFID.UID := '';
						Internal.UID_OLD := '';
						IF NOT Internal.BackUpNeeded THEN
							Internal.BackUpNeeded := FALSE;
							TPL_Password.In.Cmd.Logout := TRUE; 
							StepUM(Start:= TRUE, TiVa:= NO_TO, Next :=  INITIAL);	
						ELSE																	
							TPL_Password.In.Cmd.Logout := TRUE;
							StepUM(Start:= TRUE, TiVa:= NO_TO, Next :=  CREATE_USER_BACKUP);
						END_IF
					
					ADD_WAIT_FOR_UID:
						IF TPL_UserManager.In.Info.RFID.UID <> Internal.UID_OLD  AND TPL_UserManager.In.Info.RFID.UID <> ''  THEN
							IF TPL_UserManager.In.Info.RFID.ServiceTag = 1 THEN
								Internal.UserAllreadyExists := TRUE;
							END_IF
							tmp := 0;	
							FOR xcnt :=0 TO MAX_USER DO
								IF TPL_UserManager.InOut.UserDataTmp[xcnt].UID = TPL_UserManager.In.Info.RFID.UID THEN
									Internal.UserAllreadyExists := TRUE;
									tmp := xcnt;
									EXIT;
								END_IF	
							END_FOR
							IF NOT(Internal.UserAllreadyExists) THEN
								Internal.BackUpNeeded := TRUE;
								Internal.UID_OLD := TPL_UserManager.In.Info.RFID.UID;
								TPL_UserManager.Out.Data.UserData.UID := TPL_UserManager.In.Info.RFID.UID; 
								StepUM(Start:= TRUE, TiVa:= NO_TO, Next :=  WAIT_CREATE_USER_FINISHED);
							ELSE
								TPL_UserManager.Out.Cmd.AbortProcess := TRUE;
								Internal.UserAllreadyExists := FALSE;
							END_IF
						END_IF	
	
					ADD_ADMIN_TAG_REQUEST:
						StepUM(Start:= TRUE, TiVa:= NO_TO, Next :=  WAIT_FOR_ADMIN);
				
					ADD_TAG_EXISTS:	
						TPL_UserManager.Out.Cmd.AbortProcess := FALSE;
						TPL_UserManager.In.Info.RFID.UID := '';
						Internal.UID_OLD := '';
						StepUM(Start:= TRUE, TiVa:= NO_TO, Next :=  WAIT_CREATE_USER_FINISHED);
				
					ADD_DO_BACKUP:		
						Internal.BackUpNeeded := TRUE;
						StepUM(Start:= TRUE, TiVa:= NO_TO, Next :=  WAIT_CREATE_USER_FINISHED);
								
					ADD_ACTIVE:	
						TPL_UserManager.Out.Cmd.AddUser := FALSE;
						StepUM(Start:= TRUE, TiVa:= NO_TO, Next :=  WAIT_CREATE_USER_FINISHED);
					
					ADD_ABORT:	
						StepUM(Start:= TRUE, TiVa:= NO_TO, Next :=  INITIAL);
						
				END_CASE
			
			//***************************************************************************************************************
			//	USERMANAGER_WAIT_FOR_ADMIN
			//***************************************************************************************************************		
			WAIT_FOR_ADMIN:
				Internal.Tracer := 'WAIT_FOR_ADMIN';
				IF (TPL_UserManager.In.Info.RFID.UID <> Internal.UID_OLD AND TPL_UserManager.In.Info.RFID.UID <> '') OR TPL_UserManager.In.Info.RFID.ServiceTag = 1 THEN
					Internal.UID_OLD := TPL_UserManager.In.Info.RFID.UID;
					StepUM(Start:= TRUE, TiVa:= NO_TO, Next :=  CHECK_ADMIN);
				END_IF
				IF TPL_UserManager.In.Info.HMI_Ctrl.Status = BUSY THEN
					StepUM(Start:= TRUE, TiVa:= NO_TO, Next :=  CREATE_USER);
				END_IF
				IF TPL_UserManager.In.Info.HMI_Ctrl.Status = TAG_ISNT_ADMIN THEN
					TPL_UserManager.Out.Cmd.AbortProcess := FALSE;
					StepUM(Start:= TRUE, TiVa:= NO_TO, Next :=  WAIT_CREATE_USER_FINISHED);
				END_IF
		
			//***************************************************************************************************************
			//	USERMANAGER_CHECK_ADMIN
			//***************************************************************************************************************		
			CHECK_ADMIN:	
				tmp := 0;
				FOR xcnt :=0 TO MAX_USER DO
					IF TPL_UserManager.InOut.UserDataTmp[xcnt].UID = TPL_UserManager.In.Info.RFID.UID  THEN
						tmp := xcnt;
						Internal.UserLevelTmp := UDINT_TO_USINT(brsatoi(ADR(TPL_UserManager.InOut.UserDataTmp[tmp].UserLvl)));
						IF Internal.UserLevelTmp >= 5 THEN
							Internal.SetAdmin:= TRUE;
							EXIT;
						END_IF
					END_IF	
				END_FOR
				
				IF Internal.SetAdmin OR TPL_UserManager.In.Info.RFID.ServiceTag = 1 THEN
					Internal.SetAdmin :=FALSE;
					TPL_UserManager.Out.Data.UserData.UID := TPL_UserManager.In.Info.RFID.UID;
					TPL_UserManager.Out.Data.UserData.UserLvl := TPL_UserManager.InOut.UserDataTmp[tmp].UserLvl;
					StepUM(Start:= TRUE, TiVa:= NO_TO, Next :=  WAIT_CREATE_USER_FINISHED);
				ELSE 
					TPL_UserManager.Out.Cmd.AbortProcess := TRUE;
					StepUM(Start:= TRUE, TiVa:= NO_TO, Next :=  WAIT_CREATE_USER_FINISHED); // ABORT CMD setzen	
				END_IF	
				
			//***************************************************************************************************************
			//	USERMANAGER_CREATE_USER_BACKUP
			//***************************************************************************************************************		
			CREATE_USER_BACKUP:
				Internal.Tracer := 'CREATE_USER_BACKUP';	
				TPL_UserManager.Out.Cmd.DoBackUp := TRUE;
				IF Internal.AccessDenied THEN
					Internal.AccessDenied := FALSE;
					TPL_UserManager.Out.Cmd.AbortProcess := TRUE;
				ELSE
					TPL_UserManager.Out.Cmd.ShowInfoScreen:= TRUE;
				END_IF
				StepUM(Start:= TRUE, TiVa:= NO_TO, Next :=  WAIT_BACKUP_FINISHED);
		
			//***************************************************************************************************************
			//	USERMANAGER_WAIT_BACKUP_FINISHED
			//***************************************************************************************************************	
			WAIT_BACKUP_FINISHED:	
				Internal.Tracer := 'WAIT_BACKUP_FINISHED';
				IF TPL_UserManager.In.Info.DataHandler.Status = BUSY THEN
					TPL_UserManager.Out.Cmd.DoBackUp :=FALSE;
				ELSIF TPL_UserManager.In.Info.DataHandler.Status = 0 THEN
					TPL_UserManager.Out.Cmd.ShowInfoScreen:= FALSE;
					TPL_UserManager.Out.Cmd.AbortProcess := FALSE ;
					StepUM(Start:= TRUE, TiVa:= NO_TO, Next :=  INITIAL);
				END_IF
				
			//***************************************************************************************************************
			//	USERMANAGER_CHECK_DATA
			//***************************************************************************************************************	
			CHECK_DATA:
				Internal.Tracer := 'CHECK_DATA';
				brsmemcpy(ADR(day),ADR(TPL_UserManager.Out.Data.UserData.ExpirationDate),2);  
				TimeDT.day := STRING_TO_USINT(day);
				brsmemcpy(ADR(month),ADR(TPL_UserManager.Out.Data.UserData.ExpirationDate)+3,2);  
				TimeDT.month := STRING_TO_USINT(month);
				brsmemcpy(ADR(year),ADR(TPL_UserManager.Out.Data.UserData.ExpirationDate)+6,4); 
				TimeDT.year	:= STRING_TO_UINT(year);
				// convert DTStructure into DT format
				Internal.Time.UserDateTime := DTStructure_TO_DT(ADR(TimeDT));
				// compare both DT, output in seconds
				Internal.Time.TimeDiff.DateTime := DiffDT(Internal.Time.UserDateTime,Internal.Time.SystemDateTime);  
				Internal.LoginCounter := brsatoi(ADR(TPL_UserManager.InOut.UserDataTmp[tmp].LoginCounter));
				IF (Internal.Time.TimeDiff.DateTime = 16#FFFFFFFF OR Internal.LoginCounter >= MAX_PW_FAIL_INPUTS) AND TPL_UserManager.In.Info.RFID.ServiceTag = 0  THEN 
					TPL_UserManager.Out.Cmd.AbortProcess := TRUE;
					brsmemset(ADR(TPL_UserManager.In.Info.RFID.UID),0,SIZEOF(TPL_UserManager.In.Info.RFID.UID));
					StepUM(Start:= TRUE, TiVa:= NO_TO, Next :=  ERROR);
				ELSE // Date valid
					Internal.Time.TimeDiff.Minutes	:= Internal.Time.TimeDiff.DateTime /60; 		// minutes until expiration
					Internal.Time.TimeDiff.Days		:= Internal.Time.TimeDiff.DateTime /86400;	// days until expiration
					TPL_UserManager.Out.Cmd.SayHi := TRUE;
					IF TPL_Config.In.Data.UserManagement.DoubleAuthentificationAvailable THEN
						IF TPL_UserManager.In.Info.RFID.ServiceTag = 1 THEN
							TPL_UserManager.In.Info.RFID.UID := 'GEA SERVICE';
							TPL_UserManager.Out.Data.UserData.Password := '492349236';
						END_IF
						StepUM(Start:= TRUE, TiVa:= NO_TO, Next :=  CHECK_PW_AUTHORIZATION);	
					ELSE
						StepUM(Start:= TRUE, TiVa:= NO_TO, Next :=  WELCOME_USER);
					END_IF					
				END_IF
				
			//***************************************************************************************************************
			//	USERMANAGER_CHECK_PW_AUTHORIZATION
			//***************************************************************************************************************		
			CHECK_PW_AUTHORIZATION:	
				TPL_UserManager.Out.Cmd.SayHi := FALSE;
				Internal.Tracer := 'CHECK_PW_AUTHORIZATION';
				Internal.AccessDenied := FALSE;
				IF TPL_UserManager.In.Info.HMI_Ctrl.Status = PWINPUT_VALID THEN
					StepUM(Start:= TRUE, TiVa:= NO_TO, Next :=  WELCOME_USER);
				ELSIF TPL_UserManager.In.Info.HMI_Ctrl.Status = PWINPUT_NOT_VALID THEN
					TPL_UserManager.Out.Cmd.AbortProcess := TRUE;
					Internal.LoginCounter := Internal.LoginCounter + 1;
					IF Internal.LoginCounter >= MAX_PW_FAIL_INPUTS THEN
						Internal.LoginCounter := 3;
						Internal.AccessDenied := TRUE;						
					END_IF
					brsitoa(Internal.LoginCounter,ADR(Internal.tmpString)); 
					TPL_UserManager.InOut.UserDataTmp[tmp].LoginCounter := Internal.tmpString;
					brsmemset(ADR(TPL_UserManager.In.Info.RFID.UID),0,SIZEOF(TPL_UserManager.In.Info.RFID.UID));
					brsmemset(ADR(Internal.UID_OLD),0,SIZEOF(Internal.UID_OLD));
					StepUM(Start:= TRUE, TiVa:= NO_TO, Next :=  CREATE_USER_BACKUP);
				ELSIF TPL_UserManager.In.Info.HMI_Ctrl.Status = PWINPUT_ABORTED THEN
					StepUM(Start:= TRUE, TiVa:= NO_TO, Next :=  INITIAL);
				END_IF
				
			//***************************************************************************************************************
			//	USERMANAGER_SAY_HI
			//***************************************************************************************************************				
			WELCOME_USER:
				Internal.Tracer := 'WELCOME_USER';		
				IF TPL_UserManager.In.Info.HMI_Ctrl.Status = BUSY THEN
					TPL_UserManager.Out.Cmd.SayHi := FALSE;
					TPL_System.In.Cfg.ActualLanguage := TPL_UserManager.Out.Data.UserData.Language;
					TPL_System.In.Cfg.AutoLogoutTimeInMin := TPL_UserManager.Out.Data.UserData.LogoutTime;
					StepUM(Start:= TRUE, TiVa:= NO_TO, Next :=  SET_USER_PROPERTIES);
				END_IF	
						
			//***************************************************************************************************************
			//	USERMANAGER_SET_USER_PROPERTIESS
			//***************************************************************************************************************	
			SET_USER_PROPERTIES:
				Internal.Tracer := 'SET_USER_PROPERTIES';
				CASE TPL_UserManager.In.Info.RFID.ServiceTag OF		
					CUSTOMER_LOGIN:
						Internal.UserLevelTmp := UDINT_TO_USINT(brsatoi(ADR(TPL_UserManager.Out.Data.UserData.UserLvl)));
						IF Internal.UserLevelTmp > MAX_CUSTOMER_LEVEL THEN
							IF TPL_UserManager.In.Info.RFID.ServiceTag = 1 THEN
								TPL_Password.Out.UserLevel := Internal.UserLevelTmp;
								TPL_UserManager.Out.Data.UserData.UserName:= 'GEA Service';
								TPL_UserManager.Out.Data.UserData.LoginCounter := '00';
								TPL_UserManager.Out.Data.UserData.Password := '492349236';
							ELSE
								Internal.UserLevelTmp := MAX_CUSTOMER_LEVEL;
								TPL_Password.Out.UserLevel := Internal.UserLevelTmp;
								TPL_System.In.Cfg.ActualLanguage := TPL_UserManager.Out.Data.UserData.Language;
								TPL_System.In.Cfg.AutoLogoutTimeInMin := TPL_UserManager.Out.Data.UserData.LogoutTime;
							END_IF
						ELSE
							TPL_Password.Out.UserLevel := Internal.UserLevelTmp;	
							TPL_System.In.Cfg.ActualLanguage := TPL_UserManager.Out.Data.UserData.Language;
							TPL_System.In.Cfg.AutoLogoutTimeInMin := TPL_UserManager.Out.Data.UserData.LogoutTime;	
						END_IF
					SERVICE_LOGIN:
						TPL_UserManager.Out.Data.UserData.UserName:= 'GEA Service';
						TPL_UserManager.Out.Data.UserData.LoginCounter := '00';
						TPL_UserManager.Out.Data.UserData.LogoutTime := 10;
						TPL_UserManager.Out.Data.UserData.Password := '492349236';
						TPL_Password.Out.UserLevel := 7;
				END_CASE
			
				IF TPL_UserManager.In.Info.HMI_Ctrl.Status = 0 THEN
					StepUM(Start:= TRUE, TiVa:= NO_TO, Next :=  IDLE);
					brsmemset(ADR(TPL_UserManager.In.Info.RFID.UID),0,SIZEOF(TPL_UserManager.In.Info.RFID.UID));
					brsmemset(ADR(Internal.UID_OLD),0,SIZEOF(Internal.UID_OLD));
				END_IF
				
			//***************************************************************************************************************
			//	USERMANAGER_SEND_UID_DURING_EDIT
			//***************************************************************************************************************					
			SEND_UID_DURING_EDIT:
				Internal.Tracer := 'SEND_UID_DURING_EDIT';
				IF (TPL_UserManager.In.Info.RFID.UID <> Internal.UID_OLD AND TPL_UserManager.In.Info.RFID.UID <> '') THEN
					TPL_UserManager.Out.Data.UserData.UID := Internal.UID_OLD := TPL_UserManager.In.Info.RFID.UID;
					TPL_UserManager.Out.Cmd.FindUser := TRUE;
				END_IF
				IF TPL_UserManager.In.Info.HMI_Ctrl.Status = EDIT_FIND_USER THEN
					TPL_UserManager.Out.Cmd.FindUser := FALSE;
					StepUM(Start:= TRUE, TiVa:= NO_TO, Next :=  SEND_UID_DURING_EDIT);
				ELSIF TPL_UserManager.In.Info.HMI_Ctrl.Status = EDIT_ABORT THEN
					StepUM(Start:= TRUE, TiVa:= NO_TO, Next := INITIAL);	
				ELSIF TPL_UserManager.In.Info.HMI_Ctrl.Status = EDIT_DO_BACKUP THEN
					StepUM(Start:= TRUE, TiVa:= NO_TO, Next :=  CREATE_USER_BACKUP);
				END_IF
				
			//***************************************************************************************************************
			//	USERMANAGER_ERROR
			//***************************************************************************************************************	
			ERROR:	
				Internal.Tracer := 'ERROR';
				TPL_Password.In.Cmd.Logout := TRUE;
				TPL_UserManager.Out.Cmd.AbortProcess := FALSE;
				brsmemset(ADR(TPL_UserManager.In.Info.RFID.UID),0,SIZEOF(TPL_UserManager.In.Info.RFID.UID));
				StepUM(Start:= TRUE, TiVa:= NO_TO, Next :=  IDLE);
			
			ELSE	
				;
		END_CASE
		
	ELSE
		TPL_UserManager.Out.Cmd.AbortProcess    := FALSE;
		TPL_UserManager.Out.Cmd.AddUser         := FALSE;
		TPL_UserManager.Out.Cmd.DoBackUp        := FALSE;
		TPL_UserManager.Out.Cmd.ShowInfoScreen  := FALSE;
		TPL_UserManager.Out.Cmd.SayHi           := FALSE;
		TPL_UserManager.Out.Cmd.FindUser        := FALSE;
	END_IF
	
	
END_PROGRAM

