ACTION Action_ConfigFileReadWrite: 
	// Acces the right File information
	
	(*UserDataFile ACCESS AdrFileVariable;*)
	CASE UserDataFile.Internal.Step OF
		//***************************************************************************************************************
		//		Check For Commands
		//***************************************************************************************************************
		FILE_INIT:
			IF UserDataFile.Cmd.ReadFile THEN
				UserDataFile.Cmd.ReadFile			:= FALSE;
				UserDataFile.Out.DataReady			:= FALSE;
				UserDataFile.Internal.Step			:= FILE_LOAD;
			ELSIF UserDataFile.Cmd.WriteFile THEN
				UserDataFile.Cmd.WriteFile			:= FALSE;
				UserDataFile.Out.DataReady			:= FALSE;
				UserDataFile.Internal.Step			:= FILE_SEARCH_STRUCT;
			END_IF
		//***************************************************************************************************************
		//	FILE_LOAD
		//***************************************************************************************************************
		FILE_LOAD:
			UserDataFile.FunctionBlock.FileRead.pFileName			:= ADR(UserDataFile.Config.File);
			UserDataFile.FunctionBlock.FileRead.pDevice				:= ADR(UserDataFile.Config.FileDevice);
			UserDataFile.FunctionBlock.FileRead.No_DatatypControl	:= FALSE;
			UserDataFile.FunctionBlock.FileRead.Separator 			:= 59;
			UserDataFile.FunctionBlock.FileRead();
      
			IF UserDataFile.FunctionBlock.FileRead.status = 0 THEN
				UserDataFile.Out.DataReady				:= TRUE;
				UserDataFile.Internal.Step				:= FILE_INIT;
				DataHandler.Info.FileReady 				:= TRUE;
			ELSIF UserDataFile.FunctionBlock.FileRead.status = 20708 THEN
				UserDataFile.Internal.Step				:= FILE_INIT;
				UserDataFile.Cmd.WriteFile 				:= TRUE;
			ELSIF UserDataFile.FunctionBlock.FileRead.status <> 65535 THEN	
				UserDataFile.Internal.Step				:= FILE_ERROR;
			END_IF 
		//***************************************************************************************************************
		//	FILE_SEARCH_STRUCT
		//***************************************************************************************************************
		FILE_SEARCH_STRUCT: // Write File
			UserDataFile.FunctionBlock.FileSearchStruct.pStructName	  	:= ADR(UserDataFile.Config.AdrStructNames);
			UserDataFile.FunctionBlock.FileSearchStruct.NumOfStructs	:= UserDataFile.FunctionBlock.FileInit.NumOfStructs;
			UserDataFile.FunctionBlock.FileSearchStruct.pStructMem		:= UserDataFile.FunctionBlock.FileInit.pStructMem;
			UserDataFile.FunctionBlock.FileSearchStruct.MemLen			:= UserDataFile.FunctionBlock.FileInit.MemCounter;
			UserDataFile.FunctionBlock.FileSearchStruct.FullNameLenght	:= UserDataFile.FunctionBlock.FileInit.FullNameLenght;
			UserDataFile.FunctionBlock.FileSearchStruct.FileHeader		:= TRUE;
			UserDataFile.FunctionBlock.FileSearchStruct.pComment		:= ADR('');
			UserDataFile.FunctionBlock.FileSearchStruct.pVersion		:= ADR('User Management');
			UserDataFile.FunctionBlock.FileSearchStruct.Separator     	:= 59;
			UserDataFile.FunctionBlock.FileSearchStruct();
      
			IF UserDataFile.FunctionBlock.FileSearchStruct.status = 0 THEN
				UserDataFile.Internal.Step			:= FILE_WRITE;
			ELSIF UserDataFile.FunctionBlock.FileSearchStruct.status <> 65535 THEN
				UserDataFile.Internal.Step			:= FILE_ERROR;
			END_IF
		//***************************************************************************************************************
		//	FILE_WRITE
		//***************************************************************************************************************
		FILE_WRITE:
			UserDataFile.FunctionBlock.FileWrite.pFileName		:= ADR(UserDataFile.Config.File);
			UserDataFile.FunctionBlock.FileWrite.pDevice		:= ADR(UserDataFile.Config.FileDevice);
			UserDataFile.FunctionBlock.FileWrite.pStructMem		:= UserDataFile.FunctionBlock.FileInit.pStructMem;
			UserDataFile.FunctionBlock.FileWrite.MemLen		  	:= UserDataFile.FunctionBlock.FileSearchStruct.FileLen;
			UserDataFile.FunctionBlock.FileWrite.Mode			:= 1;
			UserDataFile.FunctionBlock.FileWrite();
      
			IF UserDataFile.FunctionBlock.FileWrite.status = 0 THEN
				DataHandler.Info.WriteDataReady		:= TRUE;
				UserDataFile.Cmd.ReadFile			:= TRUE;
				UserDataFile.Internal.Step			:= FILE_LOAD;
			ELSIF UserDataFile.FunctionBlock.FileWrite.status <> 65535 THEN
				UserDataFile.Internal.Step			:= FILE_ERROR;
			END_IF	
		//***************************************************************************************************************
		//	FILE_ERROR
		//***************************************************************************************************************
		FILE_ERROR: ;
	END_CASE
	
	
END_ACTION
