//(************************************************************************************************************************)
//(* Object name: DataHandler                                                                                             *)
//(* Author:      Florian Becker                                                                                          *)
//(* Site:        GEA Food Solutions Germany GmbH                                                                         *)
//(* Created:     03-june-2019                                                                                            *)
//(* Restriction: No restrictions.                                                                                        *)
//(* Description:                																						  *)
//(*                                                                                                                      *)
//(*----------------------------------------------------------------------------------------------------------------------*)
//(* Additional information:                                                                                              *)
//(*                                                                                                                      *)
//(* No additional information available.                                                                                 *)
//(*                                                                                                                      *)
//(*----------------------------------------------------------------------------------------------------------------------*)
//(* Version 1.00.0  03-june-2019  Florian Becker                  Development tool: B&R Automation Studio V4.5.2.102     *)
//(*                                                                                                                      *)
//(* First Release                                                                                                        *)
//(*--------------------------------------------------------------------------------------------------------------------- *)
//(************************************************************************************************************************)
PROGRAM _INIT
	//********************************************************************************************************************
	//		UserManagement DATA  CSV FILE
	//********************************************************************************************************************
	UserDataFile.Config.File					  	:= 'USER_DATA.CSV';
	UserDataFile.Config.FileDevice			      	:= 'DATA';	
	UserDataFile.Config.StructNames[0]			  	:= 'DataHandler:UserDataCipher';
	UserDataFile.Config.AdrStructNames[0] 		  	:= ADR(UserDataFile.Config.StructNames[0]);	
	UserDataFile.Cmd.ReadFile				  		:= TRUE;
	UserDataFile.FunctionBlock.FileInit(pStructName := ADR(UserDataFile.Config.AdrStructNames),NumOfStructs := 1,FullNameLenght := 0);
	
	StepDH(Start:= TRUE, TiVa:= NO_TO, Next :=  INITIAL);
	DataHandler.Info.FileReady 						:= FALSE;
																													
END_PROGRAM

PROGRAM _CYCLIC

	CASE StepDH.State OF
		//***************************************************************************************************************
		//	DATAHANDLER_INITIAL
		//*************************************************************************************************************** 
		INITIAL:
			DataHandler.Info.Tracer := 'INITIAL';
			IF TPL_Config.Out.StartUpFinished THEN
				Action_ConfigFileReadWrite;
			END_IF
			IF DataHandler.Info.FileReady THEN
				StepDH(Start:= TRUE,TiVa:= NO_TO,Next := DECIPHER);
			END_IF
			
		//***************************************************************************************************************
		//	DATAHANDLER_IDLE
		//***************************************************************************************************************
		IDLE:// Status Idle an Manager
			DataHandler.Info.Tracer := 'IDLE';
			TPL_UserManager.In.Info.DataHandler.Status := 0;
			DataHandler.Info.WriteDataReady := FALSE;
			Action_ConfigFileReadWrite;
			IF TPL_UserManager.Out.Cmd.DoBackUp THEN // Manager Befehl
				StepDH(Start:= TRUE,TiVa:= NO_TO,Next := SORT_AND_CIPHER);
				TPL_UserManager.In.Info.DataHandler.Status := BUSY;
			END_IF
			
		//***************************************************************************************************************
		//	DATAHANDLER_SORT_AND_CIPHER
		//***************************************************************************************************************
		SORT_AND_CIPHER:
			DataHandler.Info.Tracer := 'SORT_AND_CIPHER';
			TPL_UserManager.Out.Cmd.DoBackUp := FALSE;	// Status busy an Manager
			brsmemset(ADR(UserData),0,SIZEOF(UserData ));
			// ++++ Sortieren	
			xtmp := 0;
			FOR xcnt := 0 TO MAX_USER DO	
				IF  brsstrlen(ADR(TPL_UserManager.InOut.UserDataTmp[xcnt].UID)) > EMPTY_STRING THEN
					UserData[xtmp] := TPL_UserManager.InOut.UserDataTmp[xcnt];
					xtmp := xtmp+1;
				END_IF
			END_FOR

			brsmemcpy(ADR(TPL_UserManager.InOut.UserDataTmp),ADR(UserData),SIZEOF(TPL_UserManager.InOut.UserDataTmp));
			//brsmemset(ADR(TPL_UserManager.InOut.UserDataTmpUserDataTmp),0,SIZEOF(TPL_UserManager.InOut.UserDataTmpUserDataTmp));

			// ++++ Verschlüsselt ablegen		
			FOR xcnt := 0 TO MAX_USER DO					
				EncodeB64(ADR(UserData[xcnt].UID),SIZEOF(UserData[xcnt].UID),ADR(UserDataCipher[xcnt].UID),SIZEOF(UserDataCipher[xcnt].UID),ADR(CheckLength));
				EncodeB64(ADR(UserData[xcnt].UserLvl),SIZEOF(UserData[xcnt].UserLvl),ADR(UserDataCipher[xcnt].UserLvl),SIZEOF(UserDataCipher[xcnt].UserLvl),ADR(CheckLength));
				EncodeB64(ADR(UserData[xcnt].Password),SIZEOF(UserData[xcnt].Password),ADR(UserDataCipher[xcnt].Password),SIZEOF(UserDataCipher[xcnt].Password),ADR(CheckLength));
				EncodeB64(ADR(UserData[xcnt].ExpirationDate),SIZEOF(UserData[xcnt].ExpirationDate),ADR(UserDataCipher[xcnt].ExpirationDate),SIZEOF(UserDataCipher[xcnt].ExpirationDate),ADR(CheckLength));
				EncodeB64(ADR(UserData[xcnt].LoginCounter),SIZEOF(UserData[xcnt].LoginCounter),ADR(UserDataCipher[xcnt].LoginCounter),SIZEOF(UserDataCipher[xcnt].LoginCounter),ADR(CheckLength));
				UserDataCipher[xcnt].Language := UserData[xcnt].Language;
				UserDataCipher[xcnt].LogoutTime:=UserData[xcnt].LogoutTime;
				UserDataCipher[xcnt].UserName := UserData[xcnt].UserName;
			END_FOR	
			UserDataFile.Cmd.WriteFile := TRUE;	
			StepDH(Start:= TRUE,TiVa:= NO_TO,Next := WAIT_WRITE_FILE);
			
		//***************************************************************************************************************
		//	DATAHANDLER_WAIT_WRITE_FILE
		//***************************************************************************************************************				
		WAIT_WRITE_FILE:
			DataHandler.Info.Tracer := 'WAIT_WRITE_FILE';
			Action_ConfigFileReadWrite;
			IF DataHandler.Info.WriteDataReady THEN
				StepDH(Start:= TRUE,TiVa:= NO_TO,Next := IDLE);
			END_IF
	
		//***************************************************************************************************************
		//	DATAHANDLER_DECIPHER
		//***************************************************************************************************************
		DECIPHER:
			DataHandler.Info.Tracer := 'DECIPHER';
			CheckLength := 0;
			FOR xcnt := 0 TO MAX_USER DO
				UserData[xcnt].Language 	:=	UserDataCipher[xcnt].Language; 
				UserData[xcnt].LogoutTime 	:=  UserDataCipher[xcnt].LogoutTime;
				UserData[xcnt].UserName		:=  UserDataCipher[xcnt].UserName;
				CheckLength := brsstrlen(ADR(UserDataCipher[0].UID));
				DecodeB64(ADR(UserDataCipher[xcnt].UID),CheckLength,ADR(UserData[xcnt].UID),SIZEOF(UserData[xcnt].UID));
				CheckLength := brsstrlen(ADR(UserDataCipher[0].UserLvl));
				DecodeB64(ADR(UserDataCipher[xcnt].UserLvl),CheckLength,ADR(UserData[xcnt].UserLvl),SIZEOF(UserData[xcnt].UserLvl));
				CheckLength := brsstrlen(ADR(UserDataCipher[0].Password));
				DecodeB64(ADR(UserDataCipher[xcnt].Password),CheckLength,ADR(UserData[xcnt].Password),SIZEOF(UserData[xcnt].Password));
				CheckLength := brsstrlen(ADR(UserDataCipher[0].ExpirationDate));
				DecodeB64(ADR(UserDataCipher[xcnt].ExpirationDate),CheckLength,ADR(UserData[xcnt].ExpirationDate),SIZEOF(UserData[xcnt].ExpirationDate));
				CheckLength := brsstrlen(ADR(UserDataCipher[0].LoginCounter));
				DecodeB64(ADR(UserDataCipher[xcnt].LoginCounter),CheckLength,ADR(UserData[xcnt].LoginCounter),SIZEOF(UserData[xcnt].LoginCounter));			
			END_FOR
			
			StepDH(Start:= TRUE,TiVa:= NO_TO,Next := COPY_DATA);
			
		//***************************************************************************************************************
		//	DATAHANDLER_COPY_DATA
		//***************************************************************************************************************							
		COPY_DATA:
			DataHandler.Info.Tracer := 'COPY_DATA';
			brsmemcpy(ADR(TPL_UserManager.InOut.UserDataTmp),ADR(UserData),SIZEOF(TPL_UserManager.InOut.UserDataTmp));
			StepDH(Start:= TRUE,TiVa:= NO_TO,Next := IDLE);
				
		//***************************************************************************************************************
		//	DATAHANDLER_ERROR
		//***************************************************************************************************************
		ERROR:
			DataHandler.Info.Tracer := 'ERROR'; 

		ELSE
			;

	END_CASE

	
END_PROGRAM
