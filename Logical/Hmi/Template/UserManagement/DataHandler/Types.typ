(*FileIO*)

TYPE
	FileReadWriteTyp : 	STRUCT 
		Config : FileReadWriteTypConfig;
		FunctionBlock : FileReadWriteTypFB;
		Cmd : FileReadWriteTypCmd;
		Internal : FileReadWriteTypInternal;
		Out : FileReadWriteTypOut;
	END_STRUCT;
	FileReadWriteTypConfig : 	STRUCT 
		FileDevice : STRING[80];
		File : STRING[80];
		StructNames : ARRAY[0..19]OF STRING[80];
		AdrStructNames : ARRAY[0..19]OF UDINT;
	END_STRUCT;
	FileReadWriteTypFB : 	STRUCT 
		FileWrite : CSV_WriteFile;
		FileSearchStruct : CSV_Search;
		FileRead : CSV_ReadFile;
		FileInit : CSV_Init;
	END_STRUCT;
	FileReadWriteTypCmd : 	STRUCT 
		WriteFile : BOOL;
		ReadFile : BOOL;
	END_STRUCT;
	FileReadWriteTypOut : 	STRUCT 
		DataReady : BOOL;
		Status : UINT;
	END_STRUCT;
	FileReadWriteTypInternal : 	STRUCT 
		TempString : STRING[80];
		Step : Enumeration_FileWriteLoad;
		i : INT;
	END_STRUCT;
	Enumeration_FileWriteLoad : 
		(
		FILE_INIT := 0,
		FILE_LOAD := 1,
		FILE_WRITE := 2,
		FILE_SEARCH_STRUCT := 3,
		FILE_ERROR := 500
		);
END_TYPE

(*Allgemein*)

TYPE
	DataHType : 	STRUCT 
		Info : DataH_Info_Type;
	END_STRUCT;
	DataH_Info_Type : 	STRUCT 
		DataReady : BOOL;
		FileReady : BOOL;
		SortReady : BOOL;
		CipherReady : BOOL;
		DecipherReady : BOOL;
		Tracer : STRING[80];
		WriteDataReady : BOOL;
	END_STRUCT;
END_TYPE
