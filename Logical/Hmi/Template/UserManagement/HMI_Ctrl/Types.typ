(*Benutzerdaten*)

TYPE
	ExpirationDate_Type : 	STRUCT 
		Day : STRING[2];
		Month : STRING[2];
		Year : STRING[4];
		Complete : STRING[10];
	END_STRUCT;
	ExpirationDateTemp_Type : 	STRUCT 
		Day : UINT;
		Month : UINT;
		Year : UINT;
	END_STRUCT;
	StringNumeric_Type : 	STRUCT 
		String : STRING[2];
		Numeric : UINT;
	END_STRUCT;
END_TYPE

(**)
(*Allgemein*)

TYPE
	HMI_Ctrl_Type : 	STRUCT 
		Info : HMI_Ctrl_Info_Type;
		Cmd : HMI_Ctrl_Cmd_Type;
	END_STRUCT;
	HMI_Ctrl_Info_Type : 	STRUCT 
		Tracer : STRING[80];
	END_STRUCT;
	HMI_Ctrl_Cmd_Type : 	STRUCT 
		AddUser : BOOL;
		SayHi : BOOL;
	END_STRUCT;
	HMI_Ctrl_Internal_Type : 	STRUCT 
		Cmd : HMI_Ctrl_Internal_Commands_Type;
		Info : HMI_Ctrl_Info_Type;
	END_STRUCT;
	HMI_Ctrl_Internal_Commands_Type : 	STRUCT 
		EraseUserData : BOOL;
		Delete : BOOL;
		Confirm : BOOL;
		Cancel : BOOL;
		Edit : BOOL;
		Add : BOOL;
		Forward : BOOL;
		Backward : BOOL;
		FindUser : BOOL;
	END_STRUCT;
	HMI_Ctrl_UserDataHMI_Type : 	STRUCT 
		UserName : STRING[20];
		UID : STRING[16];
		UserLvl : UINT;
		LogoutTime : USINT;
		Language : UINT;
		Password : STRING[20];
		ExpirationDate : ExpirationDateTemp_Type;
		LoginCounter : UINT;
	END_STRUCT;
END_TYPE

(**)
(*HMI Visible/Invisible*)

TYPE
	Texts_Type : 	STRUCT 
		Name : Visible_Invisible_Type;
		Password : Visible_Invisible_Type;
		Language : Visible_Invisible_Type;
		UserLevel : Visible_Invisible_Type;
		ExpirationDate : Visible_Invisible_Type;
		LogoutTime : Visible_Invisible_Type;
		LoginCounter : Visible_Invisible_Type;
		UID : Visible_Invisible_Type;
		LanguageSystem : Visible_Invisible_Type;
		LogoutTimeSystem : Visible_Invisible_Type;
	END_STRUCT;
	Buttons_Type : 	STRUCT 
		Password : Visible_Invisible_Type;
		BackwardUserArray : Visible_Invisible_Type;
		ForwardUserArray : Visible_Invisible_Type;
		Confirm : Visible_Invisible_Type;
		Cancel : Visible_Invisible_Type;
		EraseAllUserData : Visible_Invisible_Type;
		DeleteUser : Visible_Invisible_Type;
		EditUser : Visible_Invisible_Type;
		AddUser : Visible_Invisible_Type;
		UserManagement : Visible_Invisible_Type;
	END_STRUCT;
	Visible_Invisible_Type : 	STRUCT 
		Invisible : USINT;
		Visible : USINT;
	END_STRUCT;
END_TYPE

(**)
(*Logger*)

TYPE
	UIDLogger_Type : 	STRUCT 
		In : UIDLogger_In_Type;
		Out : UIDLogger_Out_Type;
	END_STRUCT;
	UIDLogger_Info_Type : 	STRUCT 
		Tracer : STRING[80];
		Status : UINT;
	END_STRUCT;
	UIDLogger_In_Type : 	STRUCT 
		Data : UIDLogger_Data_Type;
	END_STRUCT;
	UIDLogger_Out_Type : 	STRUCT 
		Info : UIDLogger_Info_Type;
	END_STRUCT;
	UIDLogger_Data_Type : 	STRUCT 
		UserName : STRING[20];
		UID : STRING[16];
		UserLvl : STRING[2];
	END_STRUCT;
END_TYPE
