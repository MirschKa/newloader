
(********************************************************************
 * COPYRIGHT --  
 ********************************************************************
 * Library: F_FTG
 * File: StepTO.st
 * Author: walus1094
 * Created: long ago, header new February 09, 2012
 ********************************************************************
 * Implementation of library F_FTG
 ********************************************************************) 
  // Ver. Date        Who     comment
  
FUNCTION_BLOCK StepTO
(* Implementation of FbkStepTO. -------------------*)

IF Start THEN												(* if new step in chain is requested .. *)
	IF Timer.IN THEN									(* to start timeout timer first clear timer input *)
		Timer (IN:=FALSE);							(* run timer *)
	END_IF
	Timer.IN := TRUE;									(* start timout timer .. *)
	Timer.PT := MAXTIME;							(* with max. possible time *)
	Timer();													(* run timer *)
	TOut:=FALSE;											(* clear timeout *)
	Start := FALSE;										(* clear start flag (not realy neccessary..) *)
	IF (Next<>State) THEN							(* if the state changes log it *)
		brsmemmove(pDest:=ADR(StateLog[1]), pSrc:=ADR(StateLog[0]), length:=SIZEOF(StateLog)-1);
		StateLog[0]:=Next;
	END_IF
	State := Next;										(* switch case to requested state *)
ELSE
	Timer();													(* run timer *)
	IF Timer.ET >= DINT_TO_TIME (TiVa) THEN				(* check for timeout *)
		TOut := TRUE;										(* timeout has ocured *)
	END_IF
	Duration[State]:=Timer.ET + t#5ms;(* put elapsed time to duration array, add 5ms for rounding *)
END_IF
END_FUNCTION_BLOCK
