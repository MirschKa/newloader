//(************************************************************************************************************************)
//(* Object name: PWCheck                                                                                                 *)
//(* Author:      Florian Becker                                                                                          *)
//(* Site:        GEA Food Solutions Germany GmbH                                                                         *)
//(* Created:     03-june-2019                                                                                            *)
//(* Restriction: No restrictions.                                                                                        *)
//(* Description: The function block checks whether a password corresponds to a configured password policy                *)
//(*                                                                                                                      *)
//(*----------------------------------------------------------------------------------------------------------------------*)
//(* Additional information:                                                                                              *)
//(*                                                                                                                      *)
//(* No additional information available.                                                                                 *)
//(*                                                                                                                      *)
//(*----------------------------------------------------------------------------------------------------------------------*)
//(* Version 1.00.0  03-june-2019  Florian Becker                  Development tool: B&R Automation Studio V4.5.2.102     *)
//(*                                                                                                                      *)
//(* First Release                                                                                                        *)
//(*--------------------------------------------------------------------------------------------------------------------- *)
//(************************************************************************************************************************)
FUNCTION_BLOCK PWCheck
	IF Enable  THEN
		Enable := FALSE;
		Password_Valid := FALSE;
		upper_case := lower_case := number := special :=  i:= 0;
		size := 0;
		i := 0;
		WHILE i<= MAX_STRING DO		
			IF pw[i] = 0 THEN
				EXIT;
			END_IF
			IF (pw[i] >= UC_LOWER_BORDER AND pw[i] <= UC_HIGHER_BORDER) THEN
				upper_case := upper_case +1;
			ELSIF (pw[i] >= LC_LOWER_BORDER AND pw[i] <= LC_HIGHER_BORDER) THEN
				lower_case := lower_case +1;
			ELSIF (pw[i] >= N_LOWER_BORDER AND pw[i] <= N_HIGHER_BORDER) THEN
				number := number +1;
			ELSE
				special := special +1;		
			END_IF
			i := i +1;
			size:= size+1;	
		END_WHILE
		// Konfigparameter
		IF upper_case >= Config_UpperCharacter AND lower_case >= Config_LowerCharacter AND number >= Config_Number AND special >= Config_SpecialCharacter AND size >= Config_NumberCharacter  THEN
			Password_Valid := TRUE;
		END_IF
	END_IF
END_FUNCTION_BLOCK
