
{REDUND_ERROR} FUNCTION_BLOCK PWCheck (*TODO: Hier einen Kommentar eingeben*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		Enable : BOOL;
		pw : REFERENCE TO ARRAY[0..MAX_STRING] OF USINT;
		Config_LowerCharacter : USINT;
		Config_UpperCharacter : USINT;
		Config_Number : USINT;
		Config_SpecialCharacter : USINT;
		Config_NumberCharacter : INT; (*min. Anzahl Zeichen*)
	END_VAR
	VAR_OUTPUT
		Password_Valid : BOOL;
	END_VAR
	VAR CONSTANT
		UC_LOWER_BORDER : USINT := 65;
		UC_HIGHER_BORDER : USINT := 90;
		LC_LOWER_BORDER : USINT := 97;
		LC_HIGHER_BORDER : USINT := 122;
		N_LOWER_BORDER : USINT := 48;
		N_HIGHER_BORDER : USINT := 57;
	END_VAR
	VAR
		upper_case : USINT;
		lower_case : USINT;
		number : USINT;
		special : USINT;
		size : INT;
	END_VAR
	VAR CONSTANT
		MAX_STRING : USINT := 19;
	END_VAR
	VAR
		i : USINT;
	END_VAR
END_FUNCTION_BLOCK

{REDUND_ERROR} FUNCTION_BLOCK StepTO (*TODO: Hier einen Kommentar eingeben*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		Start : BOOL;
		TiVa : DINT;
		Next : USINT;
	END_VAR
	VAR_OUTPUT
		TOut : BOOL;
		State : USINT;
	END_VAR
	VAR
		Timer : TON;
		Duration : ARRAY[0..255] OF TIME;
		StateLog : ARRAY[0..63] OF USINT;
	END_VAR
	VAR CONSTANT
		MAXTIME : TIME := T#24d;
	END_VAR
END_FUNCTION_BLOCK
