/******* Standard header B&R-D-Bad Homburg *******/
/* Version 1.3 2004-02-04 puc                    */

#ifndef _BR_D_HG_H_
#ifdef __cplusplus
extern "C" {
#endif
#define _BR_D_HG_H_

#include <inttypes.h>
#include <limits.h>
#include <stdio.h>
/*#include <stdlib.h>*/
#include <ctype.h>
#include <string.h>
#include <math.h>
#include <AsUSB.h>
#include <FileIO.h>
#include <bur/plctypes.h>
/*#include <brDhgLib.h>*/


#ifndef NULL
#define NULL 0L
#endif

#ifndef NOT
#define NOT(arg) ((~arg) & 1)
#endif

#ifndef FALSE
#define FALSE 0
#endif

#ifndef TRUE
#define TRUE 1
#endif

#ifndef ARRAY_SIZE
#define ARRAY_SIZE(var) (sizeof(var) / sizeof(var[0]))
#endif

#ifndef ARRAY_MAXINDEX
#define ARRAY_MAXINDEX(var) (ARRAY_SIZE(var) - 1)
#endif

#ifndef safeStrcpy
#define safeStrcpy(pDest, pSrc) \
        dhgStrncpy(pDest, pSrc, sizeof(pDest))
#endif

#ifndef safeStrcat
#define safeStrcat(pDest, pCat, pSrc) \
        dhgStrncat(pDest, pCat, pSrc, sizeof(pDest))
#endif


/******** Limits ********/
#ifndef _USHRT_MIN
#define _USHRT_MIN 0      /* (All other limits are already defined in <limits.h>!) */
#endif


#ifdef __cplusplus
}
#endif

#endif /* _BR_D_HG_H_ */


