
FUNCTION_BLOCK dhgPulse (*Generate cyclic pulsing signal*)
	VAR_INPUT
		period : TIME; (*TIME means msecs!*)
		ratio : UINT; (*1-99%, default 50%, changes of ratio need restart of PLC to take effect!*)
	END_VAR
	VAR_OUTPUT
		out : BOOL; (*Output pulse*)
	END_VAR
	VAR
		_RTInfo_inst : RTInfo;
		_threshold : TIME;
		_cnt : TIME;
	END_VAR
END_FUNCTION_BLOCK

{REDUND_ERROR} FUNCTION hextoi : UDINT (*TODO: Hier einen Kommentar eingeben*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		pString : REFERENCE TO SINT;
	END_VAR
END_FUNCTION
