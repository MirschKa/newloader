#include <stdlib.h>

#include "br_d_hg.h"
#include "brDhgLib.h"



/******** Systemkonstanten, noetig fuer Init-UP ********/
#define   SYS_INIT_PHASE_1          0
#define   SYS_INIT_PHASE_PRE_START      SYS_INIT_PHASE_1

#define   SYS_INIT_PHASE_DOWNLOAD     0x00008000


#define inst (*ptr_instance)


/****************************************************************************/
void dhgPulse(dhgPulse_typ *ptr_instance) {
  if (inst._RTInfo_inst.enable == FALSE) {
    inst._RTInfo_inst.enable= TRUE;
    RTInfo(&inst._RTInfo_inst);
    inst._RTInfo_inst.cycle_time /= 1000; /* musec -> TIME (= msec) */
    inst._threshold= (inst.ratio >= 1) && (inst.ratio <= 99) ? (inst.ratio * inst.period) / 100 : inst.period / 2;
  }
  inst._cnt += inst._RTInfo_inst.cycle_time;
  if (inst._cnt >= inst.period)
    inst._cnt -= inst.period;
  inst.out= inst._cnt < inst._threshold;
}

///*********************************************************************************************/
unsigned long hextoi(signed char *pString) {

  #define chr (*(unsigned char *)pString)

  UDINT ret_val= 0;

  while (1) {
    if (chr == 0)
      break;
    ret_val <<= 4;
    if (!isxdigit(chr)) {
      ret_val= 0;
      break;
    }
    if (chr <= '9')
      ret_val |= (chr - '0') & 0x0f;
    else
      ret_val |= ((chr & ~0x20) - 'a' + 0x0a) & 0x0f;
    pString ++;
  }
  return ret_val;

  #undef chr

} /* hextoi() */


