
TYPE
	dhgCurveByPts_TabEntry_type : 	STRUCT 
		x : DINT;
		y : DINT;
	END_STRUCT;
	dhgMd5_Context : 	STRUCT 
		total : ARRAY[0..1]OF UDINT;
		state : ARRAY[0..3]OF UDINT;
		buffer : ARRAY[0..63]OF USINT;
		ipad : ARRAY[0..63]OF USINT;
		opad : ARRAY[0..63]OF USINT;
	END_STRUCT;
END_TYPE
