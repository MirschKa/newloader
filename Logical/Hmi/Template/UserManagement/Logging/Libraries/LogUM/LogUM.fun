(********************************************************************
 * COPYRIGHT --  
 ********************************************************************
 * Library: GEA_Logger
 * File: GEA_Logger.fun
 * Author: walus1351
 * Created: February 02, 2012
 ********************************************************************
 * Functions and function blocks of library CFS_Logger
 ********************************************************************)

FUNCTION_BLOCK loggingUM
	VAR_INPUT
		enable : BOOL;
		Name : STRING[200];
	END_VAR
	VAR_OUTPUT
		NumOfEntries : INT;
		OutName : ARRAY[0..1024] OF STRING[200];
	END_VAR
	VAR
		MaxIndex : UINT;
		I : INT;
		year : STRING[4];
		month : STRING[2];
		day : STRING[2];
		hour : STRING[2];
		minute : STRING[2];
		second : STRING[2];
		millisec : STRING[3];
		TempText : STRING[200];
		TextOutput : STRING[200];
		TimeStatus : UDINT;
		InternalActualTime : RTCtime_typ;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK LoggingOrganisationUM
	VAR_INPUT
		enable : BOOL;
		AdrActionLogging : DINT;
		MaxSizeOfFile : UDINT;
		MaxSaveIntervall : UDINT;
		MaxNumOfEntries : INT;
		MaxNumOfFiles : USINT;
		FileExtension : STRING[4];
		SavePath : STRING[40];
		FileName : STRING[20];
		InitialLine : STRING[310];
		InitialSeparatorLines : USINT;
	END_VAR
	VAR
		strPath : STRING[40];
		strFileNumbering : STRING[2];
		strFile : STRING[60];
		ActionLogging : REFERENCE TO loggingUM;
		Init : BOOL;
		dwIdent : UDINT;
		FWrite : FileWrite;
		FClose : FileClose;
		FOpen : FileOpen;
		FRename : FileRename;
		FDelete : FileDelete;
		FCreate : FileCreate;
		DCreate : DirCreate;
		wError : UINT;
		wStatus : UINT;
		LastStep : USINT;
		RenameCounter : USINT;
		Step : USINT;
		FileNames : ARRAY[0..20] OF STRING[65];
		strRenameTo2 : STRING[32];
		strRenameTo1 : STRING[32];
		strErrorEnty : STRING[32];
		strError : STRING[32];
		StepInformationTemp : STRING[40];
		StepInformation : STRING[200];
		strDevice : STRING[32];
		i : INT;
		Buffer : ARRAY[0..1024] OF STRING[200];
		strWrite : STRING[51000];
		cmdRename : BOOL;
		tracer_error : log_tracerUM;
		tracer_task : log_tracerUM;
		RTInfoPD : RTInfo;
		t_OneSecond : UDINT;
		t_time : UDINT;
		t_CycleTime : UDINT;
		CycleTime : UDINT;
		DurationLastSave : UDINT;
		helpvarmilliCycleTime : UDINT;
		NewFileCreated : BOOL;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK log_tracerUM
	VAR_INPUT
		enable : BOOL;
		LastEntryText : STRING[120];
		EntryText : STRING[120];
	END_VAR
	VAR_OUTPUT
		NumOfEntries : INT;
		OutName : ARRAY[0..126] OF STRING[120];
	END_VAR
	VAR
		MaxIndex : USINT;
		I : INT;
		year : STRING[4];
		month : STRING[2];
		day : STRING[2];
		hour : STRING[2];
		minute : STRING[2];
		second : STRING[2];
		millisec : STRING[3];
		TempText : STRING[120];
		TextOutput : STRING[120];
		TimeStatus : UDINT;
		InternalActualTime : RTCtime_typ;
	END_VAR
END_FUNCTION_BLOCK
