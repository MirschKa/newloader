FUNCTION_BLOCK loggingUM
(* Implementation of logging

	Objectname:		logging
	Author:			Erdal Cilingir
	Side:			SW&C
	Created:		26.01.2012
	Description:	Create entry if Input changes with Timestamplog this inputs in a array
	
	26.01.2012, EC, V1.00, start programming
	
*)

IF enable AND (MaxIndex <> 0) THEN
	NumOfEntries:=0;
	IF Name <> OutName[0] THEN
		TextOutput:='';
		IF Name<>'' THEN
			NumOfEntries:=1;
		END_IF
		FOR I := UINT_TO_INT(MaxIndex)  TO 1 BY -1 DO
			OutName[I] := OutName[I-1];
			IF OutName[I]<>'' THEN
				NumOfEntries:=NumOfEntries+1;	
			END_IF
		END_FOR

			TimeStatus := RTC_gettime(ADR(InternalActualTime));
		IF (TimeStatus = 0) THEN
			brsitoa((UINT_TO_DINT(InternalActualTime.year)), ADR(year));
			brsitoa((USINT_TO_DINT(InternalActualTime.month)), ADR(month));
			brsitoa((USINT_TO_DINT(InternalActualTime.day)), ADR(day));
			brsitoa((USINT_TO_DINT(InternalActualTime.hour)), ADR(hour));
			brsitoa((USINT_TO_DINT(InternalActualTime.minute)), ADR(minute));
			brsitoa((USINT_TO_DINT(InternalActualTime.second)), ADR(second));
			brsitoa((UINT_TO_DINT(InternalActualTime.millisec)), ADR(millisec));
			// Add 0 to Date and Time if less then 10 to get Format YYYY_MM_DD_HH_MM
			IF brsatoi(ADR(month)) <10 THEN
				month:=INSERT(month, '0', 0);
			END_IF
			IF brsatoi(ADR(day)) <10 THEN
				day:=INSERT(day, '0', 0);
			END_IF		
			IF brsatoi(ADR(hour)) <10 THEN
				hour:=INSERT(hour, '0', 0);
			END_IF			
			IF brsatoi(ADR(minute)) <10 THEN
				minute:=INSERT(minute, '0', 0);
			END_IF	
			IF brsatoi(ADR(second)) <10 THEN
				second:=INSERT(second, '0', 0);
			END_IF
			
			IF brsatoi(ADR(millisec)) < 10 THEN
				millisec:=INSERT(millisec, '00', 0);
			ELSIF brsatoi(ADR(millisec)) < 100 THEN
				millisec:=INSERT(millisec, '0', 0);
			END_IF
		END_IF
		brsstrcat(ADR(TextOutput),ADR(year));
		brsstrcat(ADR(TextOutput),ADR(Seperator));
		brsstrcat(ADR(TextOutput),ADR(month));
		brsstrcat(ADR(TextOutput),ADR(Seperator));
		brsstrcat(ADR(TextOutput),ADR(day));
		brsstrcat(ADR(TextOutput),ADR('T'));
		brsstrcat(ADR(TextOutput),ADR(hour));
		brsstrcat(ADR(TextOutput),ADR(':'));
		brsstrcat(ADR(TextOutput),ADR(minute));
		brsstrcat(ADR(TextOutput),ADR(':'));
		brsstrcat(ADR(TextOutput),ADR(second));
		brsstrcat(ADR(TextOutput),ADR(':'));
		brsstrcat(ADR(TextOutput),ADR(millisec));
		brsstrcat(ADR(TextOutput),ADR(Semikolon));
		IF brsstrlen(ADR(Name))>(SIZEOF(Name)-1) THEN
			brsmemcpy(ADR(TempText),ADR(Name),(SIZEOF(Name)-1));
			brsstrcat(ADR(TextOutput),ADR(TempText));
			OutName[0] := TextOutput;
		ELSE
			brsstrcat(ADR(TextOutput),ADR(Name));
			OutName[0] := TextOutput;
		END_IF	
	END_IF
ELSE
	MaxIndex := (SIZEOF(OutName) / SIZEOF(OutName[0])) - 1;
END_IF

END_FUNCTION_BLOCK
