FUNCTION_BLOCK LoggingOrganisationUM
(* Implementation of LoggingOrganisation *)


	


	IF enable THEN
		IF Init=FALSE THEN
			Step          := ST_ERROR_2;
			LastStep		:=	Step;
			strDevice       := 'CF';
			strPath			:= 'DATA\OEE\LOG';
			brsstrcat(ADR(strFile),ADR(strPath));
			brsstrcat(ADR(strFile),ADR('\logger-00.csv'));
			brsstrcat(ADR(strRenameTo1),ADR(strPath));
			brsstrcat(ADR(strRenameTo1),ADR('\logger-01.csv'));
			brsstrcat(ADR(strRenameTo2),ADR(strPath));
			brsstrcat(ADR(strRenameTo2),ADR('\logger-02.csv'));
			tracer_task.enable	:=TRUE;
			tracer_error.enable	:=TRUE;
			RTInfoPD(enable:=TRUE);
			//		IF MaxSizeOfFile > 1000000 OR MaxSizeOfFile <= 0 THEN
			//			MaxSizeOfFile		:=64000;	//64k
			//		END_IF
			//		IF MaxNumOfEntries > 250 OR MaxNumOfEntries < 20 THEN
			//			MaxNumOfEntries		:=100;	
			//		END_IF
			//		IF MaxSaveIntervall > 7200 OR MaxSaveIntervall < 900 THEN		
			//			MaxSaveIntervall	:=900;		//15min=900s
			//		END_IF
			//		IF MaxNumOfFiles > 21 OR MaxNumOfFiles<0 THEN
			//			MaxNumOfFiles:=20;
			//		END_IF
			brsmemset(ADR(FileNames),0,SIZEOF(FileNames));
			FOR i:=0 TO USINT_TO_INT(MaxNumOfFiles) DO
				brsstrcat(ADR(FileNames[i]),ADR(SavePath));
				brsstrcat(ADR(FileNames[i]),ADR('\'));
				brsstrcat(ADR(FileNames[i]),ADR(FileName));
				brsstrcat(ADR(FileNames[i]),ADR('_'));
				IF brsitoa(i,ADR(strFileNumbering))=1 THEN
					brsstrcat(ADR(FileNames[i]),ADR('0'));
				END_IF
				brsstrcat(ADR(FileNames[i]),ADR(strFileNumbering));
				brsstrcat(ADR(FileNames[i]),ADR('.'));
				brsstrcat(ADR(FileNames[i]),ADR(FileExtension));
			END_FOR
			Init:=TRUE;			
		END_IF
	
		IF Init=TRUE THEN
			CASE Step OF
	
				ST_ERROR:
					(**** Error step ****)
					strErrorEnty:='';
					brsstrcat(ADR(strErrorEnty),ADR('ERROR AFTER STEP: '));
					brsitoa(LastStep,ADR(strError));
					brsstrcat(ADR(strErrorEnty),ADR(strError));
					tracer_error( EntryText:= strErrorEnty);
					Step:=ST_ERROR_2;		
						
				ST_ERROR_2:	
					strErrorEnty:='';
					brsstrcat(ADR(strErrorEnty),ADR('STATUS: '));
					brsitoa(wStatus,ADR(strError));
					brsstrcat(ADR(strErrorEnty),ADR(strError));
					brsstrcat(ADR(strErrorEnty),ADR(', ERROR: '));
					brsitoa(wError,ADR(strError));
					brsstrcat(ADR(strErrorEnty),ADR(strError));
					tracer_error( EntryText:= strErrorEnty);
					wStatus:=0;
					wError:=0;
					Step:=ST_INIT_;
					
				ST_INIT_:
					// Wait until the logger is initialiated.	
					IF AdrActionLogging<>0 THEN
						ActionLogging ACCESS(AdrActionLogging);
						Step :=ST_INIT_DIRECTION;
						tracer_task( EntryText:= 'Access to logger');
					END_IF
									
				ST_INIT_DIRECTION:
					//Initialize read directory structure 
					DCreate.enable    := 1;
					DCreate.pDevice   := ADR(strDevice);
					DCreate.pName     := ADR(SavePath);
					DCreate();
					wStatus:=DCreate.status;	
					StepInformation:='Create Direction ';
					brsstrcat(ADR(StepInformation),ADR(SavePath));
					IF (wStatus=ERR_OK ) THEN
						brsstrcat(ADR(StepInformation),ADR(' -> OK'));
						tracer_task( EntryText:= StepInformation);
						Step:=ST_INIT_FILE;
						StepInformation:='';
					ELSIF (wStatus=fiERR_DIR_ALREADY_EXIST) THEN
						brsstrcat(ADR(StepInformation),ADR(' -> Exists'));
						tracer_task( EntryText:= StepInformation);   
						Step:=ST_INIT_FILE;
						StepInformation:='';
					ELSIF (wStatus<>ERR_FUB_BUSY) THEN
						brsstrcat(ADR(StepInformation),ADR(' -> ERROR'));
						tracer_task( EntryText:= StepInformation);   
						Step:=ST_ERROR;
						StepInformation:='';
					END_IF
				
				ST_INIT_FILE:
					//Initialize File
					FCreate.enable  := 1;
					FCreate.pDevice := ADR(strDevice);
					FCreate.pFile   := ADR(FileNames[0]);
					FCreate();
					dwIdent:= FCreate.ident;
					wStatus := FCreate.status;
					StepInformation:='Init File  ';
					brsstrcat(ADR(StepInformation),ADR(FileName));
					brsstrcat(ADR(StepInformation),ADR('_00'));
					brsstrcat(ADR(StepInformation),ADR('.'));
					brsstrcat(ADR(StepInformation),ADR(FileExtension));
					IF (wStatus = ERR_OK) THEN
						brsstrcat(ADR(StepInformation),ADR(' -> OK'));
						//-------------------
						NewFileCreated := TRUE;
						//-------------------
						tracer_task( EntryText:= StepInformation);
						Step          := ST_INIT_FILE_CLOSE;
					ELSIF (wStatus = fiERR_EXIST) THEN	
						brsstrcat(ADR(StepInformation),ADR(' -> File Exists'));
						tracer_task( EntryText:= StepInformation);
						Step          := ST_WAIT_COMMAND;
					ELSIF (wStatus <> ERR_FUB_BUSY) THEN
						Step          := ST_ERROR;
						brsstrcat(ADR(StepInformation),ADR(' -> ERROR'));
						tracer_task( EntryText:= StepInformation);
						brsstrcat(ADR(tracer_task.EntryText),ADR(wStatus));
						IF (wStatus = 20799) THEN
							wError := FileIoGetSysError();
						END_IF
					END_IF

				ST_INIT_FILE_CLOSE:	
					// Close file
					FClose.enable   := 1;
					FClose.ident    := dwIdent;
					FClose();
					wStatus := FClose.status;
					IF (wStatus = 0) THEN
						tracer_task( EntryText:= 'Close File -> OK');
						Step := ST_WAIT_COMMAND;
					ELSE
						IF (wStatus <> 65535) THEN
							tracer_task( EntryText:= 'Close File -> Error');
							Step          := ST_ERROR;
							IF (wStatus = 20799) THEN
								wError := FileIoGetSysError();
							END_IF
						END_IF
					END_IF	
					
				ST_WAIT_COMMAND:
					CycleTime := RTInfoPD.cycle_time;
					t_OneSecond := 1000; (* 1 second *)
					t_CycleTime := (helpvarmilliCycleTime + CycleTime) / 1000;
					helpvarmilliCycleTime := (helpvarmilliCycleTime + CycleTime) MOD 1000;
					t_time := (t_time + t_CycleTime);
					IF (t_time >= t_OneSecond) THEN
						t_time := 0;
						DurationLastSave:=DurationLastSave+1;
					END_IF
					IF (DurationLastSave >= MaxSaveIntervall) OR NewFileCreated THEN
						Step:=ST_COPY_LOG;
						DurationLastSave:=0;
					END_IF
					IF ActionLogging.NumOfEntries>=MaxNumOfEntries THEN
						Step          := ST_COPY_LOG;
						DurationLastSave		:= 0;
					END_IF
				
				ST_COPY_LOG:
					// Copy the logger into the Buffer an delete the entires
					tracer_task( EntryText:= 'Copy into Buffer');
					brsmemcpy(ADR(Buffer),ADR(ActionLogging.OutName),SIZEOF(Buffer));
					brsmemset(ADR(ActionLogging.OutName),0 ,SIZEOF(ActionLogging.OutName));
					ActionLogging( Name:='');
					IF ActionLogging.NumOfEntries<>0 THEN
						tracer_task( EntryText:= 'Buffer not read right, Goto init');
						Step          := ST_INIT_;
					ELSE
						tracer_task( EntryText:= 'Buffer read OK no entries leaved');
						Step          := ST_CREATE_STRING;					
					END_IF

			
				ST_CREATE_STRING:
					// Copy the buffer into one String
					tracer_task(EntryText:= 'Create Write String');
					strWrite	:=	'';
					
					IF NewFileCreated AND InitialLine > '' THEN
						NewFileCreated := FALSE;
						brsstrcat(ADR(strWrite),ADR(InitialLine));
						brsstrcat(ADR(strWrite),ADR('$0D$0A'));
						IF InitialSeparatorLines > 0 THEN
							FOR i :=0 TO USINT_TO_INT(InitialSeparatorLines) -1 DO
								brsstrcat(ADR(strWrite),ADR('$0D$0A'));
							END_FOR
						END_IF
					END_IF
					
					
					FOR i:=254 TO 0 BY -1 DO 
						IF Buffer[i]<>'' THEN
							brsstrcat(ADR(strWrite),ADR(Buffer[i]));
							brsstrcat(ADR(strWrite),ADR('$0D$0A')); 
						END_IF
					END_FOR	
					brsmemset(ADR(Buffer),0 ,SIZEOF(Buffer));
					// Data for writing available?
					IF strWrite = '' THEN
						tracer_task( EntryText:= 'Create Write -> No Data to Write');
						Step          := ST_INIT_DIRECTION;
					ELSE 
						tracer_task( EntryText:= 'Create Write -> OK');
						Step          := ST_FILE_OPEN;
					END_IF
																	
				ST_FILE_OPEN:
					// Try to open the existing file 
					FOpen.enable    := 1;
					FOpen.pDevice   := ADR(strDevice);
					FOpen.pFile     := ADR(FileNames[0]);
					FOpen.mode      := FILE_RW;
					FOpen();
					(* Get FUB output information *)
					dwIdent := FOpen.ident;
					wStatus := FOpen.status;
					IF (wStatus = 20708) THEN
						Step := ST_FILE_CREATE;
						tracer_task( EntryText:= 'Try Open -> File does not exist');
					ELSE
						IF (wStatus = 0) THEN
							Step := ST_FILE_WRITE;
							tracer_task( EntryText:= 'Try Open -> OK');
							IF FOpen.filelen > MaxSizeOfFile THEN
								cmdRename:=TRUE;
							END_IF
						ELSE
							IF (wStatus <> 65535) THEN
								tracer_task( EntryText:= 'Try Open -> Error');
								Step          := ST_ERROR;
								IF (wStatus = 20799) THEN
									wError := FileIoGetSysError();
								END_IF
							END_IF
						END_IF
					END_IF
					
				ST_FILE_CREATE:
					// Create file
					FCreate.enable  := 1;
					FCreate.pDevice := ADR(strDevice);
					FCreate.pFile   := ADR(FileNames[0]);
					FCreate();
					(* Get output information of FUB *)
					dwIdent := FCreate.ident;
					wStatus := FCreate.status;
					IF (wStatus = 0) THEN
						Step := ST_FILE_WRITE;
						
						NewFileCreated := TRUE;
						
						tracer_task( EntryText:= 'Create File -> OK');
					ELSE
						IF (wStatus <> 65535) THEN
							tracer_task( EntryText:= 'Create File -> Error');
							Step          := ST_ERROR;
							IF (wStatus = 20799) THEN
								wError := FileIoGetSysError();
							END_IF
						END_IF
					END_IF
					
				ST_FILE_WRITE:
					// Write data to file
					FWrite.enable   := 1;
					FWrite.ident    := dwIdent;
					FWrite.offset   := FOpen.filelen;
					FWrite.pSrc     := ADR(strWrite);
					FWrite.len      := brsstrlen(ADR(strWrite));
					FWrite();
					wStatus := FWrite.status;
					IF (wStatus = 0) THEN
						tracer_task( EntryText:= 'Write Data -> OK');
						Step := ST_FILE_CLOSE;
					ELSE
						IF (wStatus <> 65535) THEN
							tracer_task( EntryText:= 'Write Data -> Error');
							Step          := ST_ERROR;
							IF (wStatus = 20799) THEN
								wError := FileIoGetSysError();
							END_IF
						END_IF
					END_IF
					        	
				ST_FILE_CLOSE: 
					// Close file
					FClose.enable   := 1;
					FClose.ident    := dwIdent;
					FClose();
					wStatus := FClose.status;
					IF (wStatus = 0) THEN
						tracer_task( EntryText:= 'Close File -> OK');
						Step := ST_RENAME;
					ELSE
						IF (wStatus <> 65535) THEN
							tracer_task( EntryText:= 'Close File -> Error');
							Step          := ST_ERROR;
							IF (wStatus = 20799) THEN
								wError := FileIoGetSysError();
							END_IF
						END_IF
					END_IF

				ST_RENAME: 
					IF cmdRename THEN
						cmdRename:=FALSE;		
						Step:=ST_DELETE_LOG_2;
					ELSE
						Step:=ST_WAIT_COMMAND;
					END_IF
			
				ST_DELETE_LOG_2:	
					FDelete.enable	:=	1;
					FDelete.pDevice	:= 	ADR(strDevice);
					FDelete.pName	:=	ADR(FileNames[MaxNumOfFiles]);
					FDelete();
					wStatus := FDelete.status;
					StepInformation:='Delete File ';
					brsitoa(MaxNumOfFiles,ADR(StepInformationTemp));
					brsstrcat(ADR(StepInformation),ADR(StepInformationTemp));
					IF (wStatus = 20708) THEN
						RenameCounter:=MaxNumOfFiles;
						brsstrcat(ADR(StepInformation),ADR('-> File does not exist'));
						tracer_task( EntryText:= StepInformation);
						StepInformation:='';
						IF RenameCounter =0 THEN
							Step := ST_INIT_DIRECTION;
						ELSE
							Step := ST_RENAME_1_TO_2;
						END_IF
					ELSE
						IF (wStatus = 0) THEN
							brsstrcat(ADR(StepInformation),ADR(' -> OK'));
							tracer_task( EntryText:= StepInformation);
							StepInformation:='';
							RenameCounter:=MaxNumOfFiles;
							IF RenameCounter =0 THEN
								Step := ST_INIT_DIRECTION;
							ELSE
								Step := ST_RENAME_1_TO_2;
							END_IF
						ELSE
							IF (wStatus <> 65535) THEN
								brsstrcat(ADR(StepInformation),ADR(' -> ERROR'));
								tracer_task( EntryText:= StepInformation);
								StepInformation:='';
								Step          := ST_ERROR;
								IF (wStatus = 20799) THEN
									wError := FileIoGetSysError();
								END_IF
							END_IF
						END_IF
					END_IF
			
				ST_RENAME_1_TO_2:					
					FRename.enable   := 1;
					FRename.pDevice := ADR(strDevice);
					FRename.pName   := ADR(FileNames[RenameCounter-1]);
					FRename.pNewName:= ADR(FileNames[RenameCounter]);
					FRename();
					wStatus := FRename.status;
					StepInformation:='Rename ';
					brsitoa((RenameCounter-1),ADR(StepInformationTemp));
					brsstrcat(ADR(StepInformation),ADR(StepInformationTemp));
					brsstrcat(ADR(StepInformation),ADR(' to '));
					brsitoa(RenameCounter,ADR(StepInformationTemp));
					brsstrcat(ADR(StepInformation),ADR(StepInformationTemp));				
					IF (wStatus = 20708) THEN
						brsstrcat(ADR(StepInformation),ADR(' -> File does not exist'));
						tracer_task( EntryText:= StepInformation);
						StepInformation:='';
						RenameCounter:=RenameCounter-1;
					ELSE
						IF (wStatus = 0) THEN
							brsstrcat(ADR(StepInformation),ADR(' -> OK'));
							tracer_task( EntryText:= StepInformation);
							StepInformation:='';
							RenameCounter:=RenameCounter-1;
						ELSE
							IF (wStatus <> 65535) THEN
								brsstrcat(ADR(StepInformation),ADR(' -> ERROR'));
								tracer_task( EntryText:= StepInformation);
								StepInformation:='';							
								Step          := ST_ERROR;
								IF (wStatus = 20799) THEN
									wError := FileIoGetSysError();
								END_IF
							END_IF
						END_IF
					END_IF
					IF RenameCounter=0 THEN
						Step          := ST_INIT_DIRECTION;
					END_IF
				
				ST_RENAME_LOG_TO_1:					
					FRename.enable   := 1;
					FRename.pDevice := ADR(strDevice);
					FRename.pName   := ADR(strFile);
					FRename.pNewName:= ADR(strRenameTo1);
					FRename();
					wStatus := FRename.status;
					IF (wStatus = 0) THEN
						tracer_task( EntryText:= 'Rename File.log to File.1 -> OK');
						Step := ST_INIT_DIRECTION;
					ELSE
						IF (wStatus <> 65535) THEN
							tracer_task( EntryText:= 'Rename File.log to File.1 -> Error');
							Step          := ST_ERROR;
							IF (wStatus = 20799) THEN
								wError := FileIoGetSysError();
							END_IF
						END_IF
					END_IF

			END_CASE				
			IF LastStep<>Step THEN
				LastStep:=Step;
			END_IF				
		END_IF			
	ELSE
	
	END_IF

END_FUNCTION_BLOCK
