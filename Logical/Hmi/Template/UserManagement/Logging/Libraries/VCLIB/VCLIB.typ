
TYPE
	VCDPInfo : STRUCT
		iAddFractionDigits		: DINT;
		uiElement				: UDINT;
		uiRequiredUnit			: UDINT;
		uiStringSize			: UDINT;
		uiFlags					: UDINT;
	END_STRUCT;

	VCPVInfo : STRUCT
		uiUpdateMsecs			: UINT;
		uiUserID				: UINT;
	END_STRUCT;

END_TYPE
