//**************************************************************************************
// * File: 		logger
// * Author: 	Erdal Cilingir
// * Created: 	01.02.2011
// *************************************************************************************
// Version  	DATE        	Name 	Comment *)
// V3.00.0		01.02.2011		EC		Logger for logging the actions in CostFox and save 
//										to the Compact Flash
//**************************************************************************************
// Version  	DATE        	Name 	Comment *)
// V3.00.1		04.09.2017		DB		Added activation/ deactivation variables for logger
//**************************************************************************************

PROGRAM _INIT

// Logging beim Hochfahren in dieser Task aktivieren/ deaktivieren
RFID_LoggerActive := TRUE;

strRFIDHeaderTxtComplete := 'Date & Time;UID;User Level;Event;';
	
END_PROGRAM
 
 
PROGRAM _CYCLIC

IF NOT(InitRFID) AND ADR(RFIDActions) > 0 THEN		
	RFID_Action_Logging.enable := TRUE;
	RFID_Action_Logging.AdrActionLogging := ADR(RFIDActions);
	RFID_Action_Logging.InitialLine := strRFIDHeaderTxtComplete; // Headline
	RFID_Action_Logging.InitialSeparatorLines := 0; // Anzahl Leerzeilen zwischen Headline und Loggertext
	RFID_Action_Logging.MaxNumOfEntries:= 1000; // Max. Eintr�ge je Datei. Danach wird eine neue Datei erzeugt
	RFID_Action_Logging.MaxSaveIntervall:= 15; // Speicherintervall in Sekunden
	RFID_Action_Logging.MaxSizeOfFile:=1000000; // Max. Dateigr��e in Byte
	RFID_Action_Logging.FileExtension:='csv'; // Dateisuffix
	RFID_Action_Logging.MaxNumOfFiles:=10; // Max. Anzahl an Dateien
	RFID_Action_Logging.FileName:='UID_LOG'; // Dateiname
	RFID_Action_Logging.SavePath:='DATA'; // Speicherpfad auf CF/CFast
	RFIDActions.enable :=TRUE;
	InitRFID :=TRUE;
ELSIF InitRFID AND RFID_LoggerActive THEN
	RFID_Action_Logging;
END_IF	
	
END_PROGRAM
