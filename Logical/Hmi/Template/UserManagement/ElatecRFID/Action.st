ACTION USE_DATA: 
	brsmemcpy(ADR(strTagOk[xcnt]),ADR(Elatec[xcnt].receive_data[3]),1); // lese Stelle 3		// Tag read ok
	brsmemcpy(ADR(strTagTyp[xcnt]),ADR(Elatec[xcnt].receive_data[4]),2); // lese Stelle 4/5	    // Tag Typ
	brsmemcpy(ADR(strTagUIDLength[xcnt]),ADR(Elatec[xcnt].receive_data[6]),4); // lese Stelle 6/7/8/9 // Tag UID
	
	TagType[xcnt] := hextoi(ADR(strTagTyp[xcnt]));
	
	IF TagType[xcnt] >= 64 AND TagType[xcnt] <= 90 THEN
		TPL_UserManager.In.Info.RFID.TransponderTyp:= 'Low Frequency';
	ELSIF
		TagType[xcnt] >= 128 AND TagType[xcnt] <= 138 THEN
		TPL_UserManager.In.Info.RFID.TransponderTyp:= 'High Frequency';
//	ELSE
//		TransponderTyp:= 'N/A';
	END_IF
	
	IF strTagOk[xcnt] = '1' THEN
		CASE TagType[xcnt] OF
		//== LF ==
			64: TPL_UserManager.In.Info.RFID.TagName := 'EM4x02/CASI-RUSCO';
			65: TPL_UserManager.In.Info.RFID.TagName := 'HITAG 1/HITAG S';   
			66: TPL_UserManager.In.Info.RFID.TagName := 'HITAG 2';           
			67: TPL_UserManager.In.Info.RFID.TagName := 'EM4x50';          
			68: TPL_UserManager.In.Info.RFID.TagName := 'T55x7';           
			69: TPL_UserManager.In.Info.RFID.TagName := 'ISO FDX-B';         
			70: TPL_UserManager.In.Info.RFID.TagName := 'N/A';                 
			71: TPL_UserManager.In.Info.RFID.TagName := 'N/A';                 
			72: TPL_UserManager.In.Info.RFID.TagName := 'N/A';                 
			73: TPL_UserManager.In.Info.RFID.TagName :=	'HID Prox';
			74: TPL_UserManager.In.Info.RFID.TagName :=	'ISO HDX/TIRIS';
			75: TPL_UserManager.In.Info.RFID.TagName :=	'Cotag';
			76: TPL_UserManager.In.Info.RFID.TagName :=	'ioProx';
			77: TPL_UserManager.In.Info.RFID.TagName :=	'Indala';
			78: TPL_UserManager.In.Info.RFID.TagName :=	'NexWatch';
			79: TPL_UserManager.In.Info.RFID.TagName :=	'AWID';
			80: TPL_UserManager.In.Info.RFID.TagName :=	'G-Prox';
			81: TPL_UserManager.In.Info.RFID.TagName :=	'Pyramid';
			82: TPL_UserManager.In.Info.RFID.TagName :=	'Keri';
			83: TPL_UserManager.In.Info.RFID.TagName :=	'Deister';
			84: TPL_UserManager.In.Info.RFID.TagName :=	'Cardax';
			85: TPL_UserManager.In.Info.RFID.TagName :=	'Nedap';
			86: TPL_UserManager.In.Info.RFID.TagName :=	'PAC';
			87: TPL_UserManager.In.Info.RFID.TagName :=	'IDTECK';
			88: TPL_UserManager.In.Info.RFID.TagName :=	'UltraProx';
			89: TPL_UserManager.In.Info.RFID.TagName :=	'ICT';
			90: TPL_UserManager.In.Info.RFID.TagName :=	'Isonas';
		//== HF ==
			128: TPL_UserManager.In.Info.RFID.TagName := 'ISO14443A/MIFARE';
			129: TPL_UserManager.In.Info.RFID.TagName := 'ISO14443B';
			130: TPL_UserManager.In.Info.RFID.TagName := 'ISO15693';
				IF NOT(GEATagFound[xcnt]) THEN
					GEATagFound[xcnt] := TRUE;
				ELSE
					GEATagFound[xcnt]:= FALSE;
				END_IF
			131: TPL_UserManager.In.Info.RFID.TagName := 'LEGIC';
			132: TPL_UserManager.In.Info.RFID.TagName := 'HID iCLASS';
			133: TPL_UserManager.In.Info.RFID.TagName := 'FeliCa';
			134: TPL_UserManager.In.Info.RFID.TagName := 'SRX';
			135: TPL_UserManager.In.Info.RFID.TagName := 'NFC Peer-to-Peer';
			136: TPL_UserManager.In.Info.RFID.TagName := 'Bluetooth Low Energy';
			137: TPL_UserManager.In.Info.RFID.TagName := 'Topaz';
			138: TPL_UserManager.In.Info.RFID.TagName := 'CTS256/CTS512'; 			
			ELSE
				 TPL_UserManager.In.Info.RFID.TagName := 'N/A';
		END_CASE			
		IF strTagUIDLength[xcnt] = '2004' THEN 												 						(* ident  8 Byte UID*)
			brsmemset(ADR(strTagUID[xcnt]),0, SIZEOF(strTagUID[xcnt]));
			brsmemcpy(ADR(strTagUID[xcnt]),ADR(Elatec[xcnt].receive_data[10]),8);
			UID_NEW[xcnt] := strTagUID[xcnt];
		ELSIF strTagUIDLength[xcnt] = '4008' THEN												 						(* ident  16 Byte UID*)
			brsmemset(ADR(strTagUID[xcnt]),0, SIZEOF(strTagUID[xcnt]));
			brsmemcpy(ADR(strTagUID[xcnt]),ADR(Elatec[xcnt].receive_data[10]),16);
			UID_NEW[xcnt] := strTagUID[xcnt];
		END_IF
	ELSE
		strTagUID[xcnt] :='No Tag';
		brsmemset(ADR(UID_NEW[xcnt]),0, SIZEOF(UID_NEW[xcnt]));
		brsmemset(ADR(TPL_UserManager.In.Info.RFID.UID),0,SIZEOF(TPL_UserManager.In.Info.RFID.UID));
	END_IF	
END_ACTION
