//(************************************************************************************************************************)
//(* Object name: ElatecRFID                                                                                              *)
//(* Author:      Florian Becker                                                                                          *)
//(* Site:        GEA Food Solutions Germany GmbH                                                                         *)
//(* Created:     18-june-2019                                                                                            *)
//(* Restriction: No restrictions.                                                                                        *)
//(* Description: The Task controlls the Elatec RFID Reader                												  *)
//(*                                                                                                                      *)
//(*----------------------------------------------------------------------------------------------------------------------*)
//(* Additional information:                                                                                              *)
//(*                                                                                                                        *)
//(* No additional information available.                                                                                 *)
//(*                                                                                                                      *)
//(*----------------------------------------------------------------------------------------------------------------------*)
//(* Version 1.00.0  18-june-2019  Florian Becker                  Development tool: B&R Automation Studio V4.5.2.102     *)
//(*                                                                                                                      *)
//(* First Release                                                                                                        *)
//(*--------------------------------------------------------------------------------------------------------------------- *)
//(************************************************************************************************************************)


PROGRAM _INIT
	IF TPL_Config.In.Data.UserManagement.NbrOfRFIDReader > 1 OR TPL_Config.In.Data.UserManagement.NbrOfRFIDReader = 0 THEN
		TPL_Config.In.Data.UserManagement.NbrOfRFIDReader := 1;
	END_IF
	FOR xcnt:= 0  TO 1  DO
	 	Elatec[xcnt].step := FRM_WAIT;
		Elatec[xcnt].cmd.open_ht := TRUE;
		Elatec[xcnt].Hmi.Visibility := TPL_INVISIBLE;
	 	Elatec[xcnt].send_data := '050020';
	END_FOR
	 
END_PROGRAM

PROGRAM _CYCLIC
	IF TPL_Password.In.Cfg.Login.LoginSystemMode = CFS_LOGIN_SYSTEM_MODE_USERCHECK AND TPL_Config.In.Data.UserManagement.NbrOfRFIDReader > 0 THEN
		FOR  xcnt:= 0  TO TPL_Config.In.Data.UserManagement.NbrOfRFIDReader-1 DO
			CASE Elatec[xcnt].step OF
				//***************************************************************************************************************
														(*call the FRM_xopen() function*)
				//	ELATEC FRM_WAIT
				//*************************************************************************************************************** 	
				FRM_WAIT:	(*--- wait for command*)
					Elatec[xcnt].Info.Tracer := 'FRM_WAIT' ;
					Elatec[xcnt].Hmi.Visibility := TPL_INVISIBLE;
					Elatec[xcnt].Hmi.Status :=  LED_GREY;
					IF Elatec[xcnt].cmd.open_ht = TRUE THEN																(*cmd for sender activation*)
						Elatec[xcnt].step := FRM_OPEN;
					END_IF			
					(*close will be requested in step FRM_WRITE*)
					
					//***************************************************************************************************************
					//	Elatec[xcnt] FRM_OPEN
					//*************************************************************************************************************** 		
				FRM_OPEN:	(*--- open serial interface*)
					Elatec[xcnt].Info.Tracer := 'FRM_OPEN' ;
					(*Parameters for FRM_xopen()*)
					Elatec[xcnt].Communication.FRM_xopen_0.enable := TRUE;
					Elatec[0].Communication.FRM_xopen_0.device  := ADR('SL1.IF5');										(*Devicename --> see your serial interface properties*)
					Elatec[1].Communication.FRM_xopen_0.device  := ADR('SL1.IF1.ST1.IF1.ST2.IF1');
					Elatec[xcnt].Communication.FRM_xopen_0.mode := ADR('/PHY=RS232 /BD=9600 /DB=8 /PA=N /SB=1'); 			(*Modestring --> specifies the seria operation mode*)	
					Elatec[xcnt].Communication.FRM_xopen_0();																(*call the FRM_xopen() function*)
			
					(*call the FRM_gbuf() function*)
					IF Elatec[xcnt].Communication.FRM_xopen_0.status = 0 THEN
						Elatec[xcnt].step := FRM_GBUF;		
						Elatec[xcnt].Hmi.Visibility := TPL_VISIBLE;
						Elatec[xcnt].Hmi.Status := LED_YELLOW;
						(*Interface opend successfully --> next step*)
					ELSIF Elatec[xcnt].Communication.FRM_xopen_0.status = BUSY THEN
						Elatec[xcnt].step := FRM_OPEN;																	(*operation not finished yet --> call again*)
					ELSE
						Elatec[xcnt].step := FRM_ERROR;	
						Elatec[xcnt].Hmi.Status := LED_RED;																
						(*function returned errorcode --> check help*)
					END_IF	
							
					//***************************************************************************************************************
					//	ELATEC FRM_GBUF
					//*************************************************************************************************************** 			
				FRM_GBUF:	(*--- aquire sendbuffer for FRM_WRITE*)
					Elatec[xcnt].Info.Tracer := 'FRM_GBUF' ;
					Elatec[xcnt].Communication.FRM_gbuf_0.enable := TRUE;
					Elatec[xcnt].Communication.FRM_gbuf_0.ident := Elatec[xcnt].Communication.FRM_xopen_0.ident;			
					Elatec[xcnt].Communication.FRM_gbuf_0();																(*call the FRM_gbuf() function*)
					Elatec[xcnt].Hmi.Status := LED_GREEN;
					IF Elatec[xcnt].Communication.FRM_gbuf_0.status = 0 THEN
						brsmemset(Elatec[xcnt].Communication.FRM_gbuf_0.buffer,0,Elatec[xcnt].Communication.FRM_gbuf_0.buflng);	(*clear sendbuffer*)
						Elatec[xcnt].step := FRM_PREPARE_SENDDATA;														(*system returned a valid buffer --> next step*)
					ELSIF Elatec[xcnt].Communication.FRM_gbuf_0.status = BUSY THEN
						Elatec[xcnt].step := FRM_GBUF;																	(*operation not finished yet --> call again*)
					ELSE
						Elatec[xcnt].step := FRM_ERROR;	
						Elatec[xcnt].Hmi.Status := LED_YELLOW;															
						(*function returned errorcode --> check help*)
					END_IF			
			
					//***************************************************************************************************************
					//	ELATEC FRM_PREPARE_SENDDATA
					//*************************************************************************************************************** 		
				FRM_PREPARE_SENDDATA: (*--- prepare senddata, copy data to sendbuffer*)
					Elatec[xcnt].Info.Tracer := 'FRM_PREPARE_SENDDATA' ;
					IF Elatec[xcnt].cmd.m_firstinit = TRUE THEN
						Elatec[xcnt].send_data := '050020';
						brsstrcat(ADR(Elatec[xcnt].send_data), ADR('$r'));												(*Send INIT text with carridge return *)
						Elatec[xcnt].cmd.m_firstinit := FALSE;
						GEATagCmd[xcnt] := CHECK_USERTAG;																					(*disable "first" flag*)
					ELSIF GEATagFound[xcnt] THEN
						GEATagCmd[xcnt] := SEARCH_GEA_KEY_ID;
						Elatec[xcnt].send_data := '0D050300FF';																
						brsstrcat(ADR(Elatec[xcnt].send_data), ADR('$r'));
					ELSIF GEAUsrLvlValid[xcnt] THEN
						GEATagCmd[xcnt] := SEARCH_USERLEVEL;
						Elatec[xcnt].send_data := '0D050400FF';																
						brsstrcat(ADR(Elatec[xcnt].send_data), ADR('$r'));
					ELSIF NOT(GEATagFound[xcnt]) AND NOT(	GEAUsrLvlValid[xcnt]) OR JobDone[xcnt] THEN
						Elatec[xcnt].send_data := '050020';
						brsstrcat(ADR(Elatec[xcnt].send_data), ADR('$r'));
					END_IF	
					
					brsstrcpy(Elatec[xcnt].Communication.FRM_gbuf_0.buffer,ADR(Elatec[xcnt].send_data));						(*copy Data to Sendbuffer*)
					Elatec[xcnt].step := FRM_WRITE;																		(*call the FRM_write() function*)(*--> next step*)
											
					//***************************************************************************************************************
					//	ELATEC FRM_WRITE
					//*************************************************************************************************************** 		
				FRM_WRITE:	(*--- write data to interface*)
					Elatec[xcnt].Info.Tracer := 'FRM_WRITE' ;
					(*Parameters for FRM_write()*)
					Elatec[xcnt].Communication.FRM_write_0.enable := TRUE;
					Elatec[xcnt].Communication.FRM_write_0.ident := Elatec[xcnt].Communication.FRM_xopen_0.ident;				(*ident from FRM_xopen()*)
					Elatec[xcnt].Communication.FRM_write_0.buffer := Elatec[xcnt].Communication.FRM_gbuf_0.buffer;				(*sendbuffer*)
					Elatec[xcnt].Communication.FRM_write_0.buflng := UDINT_TO_UINT(brsstrlen(ADR(Elatec[xcnt].send_data)));		(*net length of senddata*)					
					Elatec[xcnt].Communication.FRM_write_0();																(*call the FRM_write() function*)
			
					IF Elatec[xcnt].Communication.FRM_write_0.status = 0 THEN
						IF Elatec[xcnt].cmd.close = TRUE THEN																(*requst to close the serial port*)
							Elatec[xcnt].cmd.open_ht := FALSE;															(*disable cmd open_send*)
							Elatec[xcnt].step := FRM_CLOSE;
						ELSE
							Elatec[xcnt].step := FRM_READ;																(*writing successful --> goto listen mode "FRM_READ"*)
						END_IF
					ELSIF Elatec[xcnt].Communication.FRM_write_0.status = BUSY THEN
						Elatec[xcnt].step := FRM_WRITE;																	(*operation not finished yet --> call again*)
					ELSE
															(*call the FRM_robuf() function*)
						Elatec[xcnt].step := FRM_ROBUF;																	(*function returned errorcode --> check help*)
					END_IF	
							
					//***************************************************************************************************************
					//	ELATEC FRM_ROBUF
					//*************************************************************************************************************** 			
				FRM_ROBUF:	(*--- release sendbuffer in case of no successful write operation*)
					Elatec[xcnt].Info.Tracer := 'FRM_ROBUF' ;
					Elatec[xcnt].Communication.FRM_robuf_0.enable := TRUE;
					Elatec[xcnt].Communication.FRM_robuf_0.buffer := Elatec[xcnt].Communication.FRM_gbuf_0.buffer;				(*sendbuffer*)
					Elatec[xcnt].Communication.FRM_robuf_0.buflng := Elatec[xcnt].Communication.FRM_gbuf_0.buflng;				(*buffer length*)
					Elatec[xcnt].Communication.FRM_robuf_0.ident := Elatec[xcnt].Communication.FRM_xopen_0.ident;				(*ident open*)
					Elatec[xcnt].Communication.FRM_robuf_0();																(*call the FRM_robuf() function*)
		
					IF Elatec[xcnt].Communication.FRM_robuf_0.status = 0 THEN
						Elatec[xcnt].step := FRM_GBUF;																	(*released buffer successful --> get next sendbuffer*)
					ELSIF Elatec[xcnt].Communication.FRM_robuf_0.status = BUSY THEN
						Elatec[xcnt].step := FRM_ROBUF;																	(*operation not finished yet --> call again*)
					ELSE
						Elatec[xcnt].step := FRM_ERROR;																	(*function returned errorcode --> check help*)
					END_IF			
					
					//***************************************************************************************************************
					//	ELATEC FRM_READ
					//*************************************************************************************************************** 		
				FRM_READ:	(*--- read data from serial interface*)
					Elatec[xcnt].Info.Tracer := 'FRM_READ' ;
					Elatec[xcnt].Communication.FRM_read_0.enable := TRUE;
					Elatec[xcnt].Communication.FRM_read_0.ident := Elatec[xcnt].Communication.FRM_xopen_0.ident;			
					Elatec[xcnt].Communication.FRM_read_0();																(*call the FRM_read() function*)
			
					IF Elatec[xcnt].Communication.FRM_read_0.status = 0 THEN
						Elatec[xcnt].step := FRM_COPY_RD_DATA;															(*system returned a valid data --> next step*)
					ELSIF Elatec[xcnt].Communication.FRM_read_0.status = frmERR_NOINPUT THEN
						Elatec[xcnt].step := FRM_READ;																	(*no data available --> call again*)
					ELSIF Elatec[xcnt].Communication.FRM_read_0.status = frmERR_INPUTERROR THEN
						Elatec[xcnt].step := FRM_RBUF;																	(*received Frame with defective characters, skip data*)
					ELSE
						Elatec[xcnt].step := FRM_ERROR;																	(*function returned errorcode --> check help*)
					END_IF						

					//***************************************************************************************************************
					//	ELATEC FRM_COPY_RD_DATA
					//*************************************************************************************************************** 			
				FRM_COPY_RD_DATA: (*--- copy data out of the receive buffer*)
					Elatec[xcnt].Info.Tracer := 'FRM_COPY_RD_DATA' ;
					brsmemset(ADR(Elatec[xcnt].receive_data),0,SIZEOF(Elatec[xcnt].receive_data));								(*clear read_data buffer*)																						(*copy and evaluate read data*)
					brsmemcpy(ADR(Elatec[xcnt].receive_data),Elatec[xcnt].Communication.FRM_read_0.buffer, Elatec[xcnt].Communication.FRM_read_0.buflng);			
					(* ACTION to analyze read data *)
				
					CASE GEATagCmd[xcnt] OF
						CHECK_USERTAG:
							USE_DATA;
							IF GEATagFound[xcnt] AND strTagOk[xcnt] = '1' THEN
								GEATagCmd[xcnt]:= SEARCH_GEA_KEY_ID;
							ELSIF strTagOk[xcnt] = '1' THEN
								GEATagCmd[xcnt] := 3;
								GEATAGValid[xcnt] := FALSE;
							END_IF
					
						SEARCH_GEA_KEY_ID:	
							brsmemset(ADR(GEA_Data[xcnt]),0,SIZEOF(GEA_Data[xcnt]));	
							brsmemcpy(ADR(GEA_Data[xcnt]),ADR(Elatec[xcnt].receive_data[6]),8); // lese Stelle 6/7/8/9 // Tag UID
							IF GEA_Data[xcnt] = '15211099' THEN 
								GEATagFound[xcnt] := FALSE;
								GEAUsrLvlValid[xcnt] := TRUE;
								GEATagCmd[xcnt] := SEARCH_USERLEVEL;
							ELSE
								GEATagCmd[xcnt] := CHECK_USERTAG;
								GEATagFound[xcnt] := FALSE;	
							END_IF			
			
						SEARCH_USERLEVEL:
							brsmemset(ADR(GEA_Data[xcnt]),0,SIZEOF(GEA_Data[xcnt]));
							brsmemcpy(ADR(GEA_Data[xcnt]),ADR(Elatec[xcnt].receive_data[6]),2); // lese Stelle 6/7/8/9 // Tag UID
							IF GEA_Data[xcnt] = '07' OR GEA_Data[xcnt] = '7' THEN 
								GEATAGValid[xcnt] := TRUE;
								GEATagFound[xcnt] := FALSE;
								GEATAGValid[xcnt]:= GEAUsrLvlValid[xcnt] := FALSE;
								IF UID_NEW[xcnt] <> UID_OLD[xcnt] THEN
									UID_OLD[xcnt] := UID_NEW[xcnt];
									TPL_UserManager.In.Info.RFID.UID := UID_OLD[xcnt];	
									TPL_UserManager.In.Info.RFID.ServiceTag := 1;
									TPL_UserManager.In.Info.RFID.UID := 'GEA SERVICE';
									brsmemset(ADR(UID_OLD[xcnt]),0,SIZEOF(UID_OLD[xcnt]));
								END_IF
								JobDone[xcnt] := TRUE;			
								GEATagCmd[xcnt] := CHECK_USERTAG;
							ELSE
								GEATAGValid[xcnt]:= GEAUsrLvlValid[xcnt] := FALSE;
								IF UID_NEW[xcnt] <> UID_OLD[xcnt] THEN
									UID_OLD[xcnt] := UID_NEW[xcnt];
									TPL_UserManager.In.Info.RFID.UID := UID_OLD[xcnt];
									brsmemset(ADR(UID_OLD[xcnt]),0,SIZEOF(UID_OLD[xcnt]));
								END_IF	
								TPL_UserManager.In.Info.RFID.ServiceTag := 0;
								GEATagCmd[xcnt] := CHECK_USERTAG;
							END_IF
					
						SEND_DATA: 	
							IF UID_NEW[xcnt] <> UID_OLD[xcnt] THEN
								UID_OLD[xcnt] := UID_NEW[xcnt];
								TPL_UserManager.In.Info.RFID.UID := UID_OLD[xcnt];
								brsmemset(ADR(UID_OLD[xcnt]),0,SIZEOF(UID_OLD[xcnt]));
							END_IF		
							TPL_UserManager.In.Info.RFID.ServiceTag := 0;

							GEATagCmd[xcnt] := CHECK_USERTAG;	
					END_CASE
																																		(*call the FRM_rbuf() function*)
					Elatec[xcnt].step := FRM_RBUF;																		(*release read buffer*)
					
					//***************************************************************************************************************
					//	ELATEC FRM_RBUF
					//***************************************************************************************************************			
				FRM_RBUF:	(*--- release readbuffer*)
					Elatec[xcnt].Info.Tracer := 'FRM_RBUF' ;
					(*Parameters for FRM_rbuf()*)
					Elatec[xcnt].Communication.FRM_rbuf_0.enable := TRUE;
					Elatec[xcnt].Communication.FRM_rbuf_0.ident  := Elatec[xcnt].Communication.FRM_xopen_0.ident;				(*ident from FRM_xopen()*)
					Elatec[xcnt].Communication.FRM_rbuf_0.buffer := Elatec[xcnt].Communication.FRM_read_0.buffer;				(*read buffer*)
					Elatec[xcnt].Communication.FRM_rbuf_0.buflng := Elatec[xcnt].Communication.FRM_read_0.buflng;				(*length of sendbuffer*)  			
					Elatec[xcnt].Communication.FRM_rbuf_0();																(*call the FRM_rbuf() function*)
				
													(*call the FRM_close() function*)
					IF Elatec[xcnt].Communication.FRM_rbuf_0.status = 0 THEN
						Elatec[xcnt].step := FRM_GBUF;																	(*send accnowledge -> aquire sendbuffer first*)
					ELSIF Elatec[xcnt].Communication.FRM_rbuf_0.status = BUSY THEN
						Elatec[xcnt].step := FRM_RBUF;																	(*operation not finished yet --> call again*)
					ELSIF Elatec[xcnt].Communication.FRM_rbuf_0.status = frmERR_INVALIDBUFFER THEN				
						Elatec[xcnt].step := FRM_READ;																	(*buffer is invalid --> read again*)
					ELSE
						Elatec[xcnt].step := FRM_ERROR;																	(*function returned errorcode --> check help*)
					END_IF			
					//***************************************************************************************************************
					//	Elatec[xcnt] FRM_CLOSE
					//*************************************************************************************************************** 		
				FRM_CLOSE:	(*--- close the interface*)
					Elatec[xcnt].Info.Tracer := 'FRM_CLOSE' ;
					Elatec[xcnt].Communication.FRM_close_0.enable := TRUE;
					Elatec[xcnt].Communication.FRM_close_0.ident := Elatec[xcnt].Communication.FRM_xopen_0.ident;				(*ident from FRM_xopen()*)			
					Elatec[xcnt].Communication.FRM_close_0();																(*call the FRM_close() function*)
			
																					
					IF Elatec[xcnt].Communication.FRM_close_0.status = 0 THEN
						Elatec[xcnt].cmd.close := FALSE;																	(*disable close cmd*)
						Elatec[xcnt].cmd.m_firstinit := TRUE;																(*mark as "first" for next open*)
						Elatec[xcnt].step := FRM_WAIT;																	(*closed interface successfully --> wait step*)
					ELSIF Elatec[xcnt].Communication.FRM_close_0.status = BUSY THEN
						Elatec[xcnt].step := FRM_CLOSE;																	(*operation not finished yet --> call again*)
					ELSE
						Elatec[xcnt].step := FRM_ERROR;																	(*function returned errorcode --> check help*)
					END_IF
					//***************************************************************************************************************
					//	ELATEC FRM_ERROR
					//*************************************************************************************************************** 			
				FRM_ERROR:	(*--- error handling*)
					Elatec[xcnt].Info.Tracer := 'FRM_ERROR' ;
				; 																								(*not implementet yet, check help for error codes*)
			END_CASE	
		END_FOR
		
		IF TPL_Password.In.Cmd.Logout THEN
			brsmemset(ADR(UID_OLD[xcnt]),0,SIZEOF(UID_OLD[xcnt]));
			brsmemset(ADR(UID_NEW[xcnt]),0,SIZEOF(UID_NEW[xcnt]));
		END_IF
		
	END_IF

END_PROGRAM
	 

