(*Schrittkette*)

TYPE
	enTERM_STEPS : 
		(
		FRM_WAIT := 0, (*Defines the wait step*)
		FRM_OPEN, (*Defines Step for FRM_xopen() *)
		FRM_GBUF, (*Defines Step for FRM_rbuf()*)
		FRM_PREPARE_SENDDATA, (*Defines Step for preparing send data*)
		FRM_WRITE, (*Defines Step for FRM_write()*)
		FRM_ROBUF, (*Defines Step for FRM_robuf()*)
		FRM_READ, (*Defines Step for FRM_read()*)
		FRM_COPY_RD_DATA, (*Defines Step for storing read data*)
		FRM_RBUF, (*Defines Step for FRM_rbuf()*)
		FRM_CLOSE, (*Defines Step for FRM_close()*)
		FRM_ERROR := 255 (*Defines Step for Errorhandling*)
		);
END_TYPE

(*Elatec *)

TYPE
	ElaCmdTyp : 	STRUCT 
		open_ht : BOOL; (*Open serial port, accnowledges data from opposite station*)
		close : BOOL; (*Close serial port*)
		m_firstinit : BOOL; (*flag for marking the first sendframe*)
	END_STRUCT;
	ElaTyp : 	STRUCT 
		step : enTERM_STEPS; (*Step of the Statemachine*)
		cmd : ElaCmdTyp; (*Commandinterface*)
		Communication : ElaComTyp;
		receive_data : ARRAY[0..39]OF USINT; (*Receive data from Terminal*)
		send_data : STRING[10]; (*Send data to Terminal*)
		Info : ElaInfoTyp;
		Hmi : ElaHmiTyp;
	END_STRUCT;
	ElaComTyp : 	STRUCT 
		FRM_xopen_0 : FRM_xopen; (*Functionblock FRM_xopen()*)
		FRM_gbuf_0 : FRM_gbuf; (*Functionblock FRM_gbuf()*)
		FRM_robuf_0 : FRM_robuf; (*Functionblock FRM_robuf()*)
		FRM_write_0 : FRM_write; (*Functionblock FRM_write)*)
		FRM_read_0 : FRM_read; (*Functionblock FRM_read()*)
		FRM_rbuf_0 : FRM_rbuf; (*Functionblock FRM_rbuf()*)
		FRM_close_0 : FRM_close; (*Functionblock FRM_close()*)
		xopenConfig : XOPENCONFIG; (*Configuration Type for FRM_xopen()*)
	END_STRUCT;
	ElaInfoTyp : 	STRUCT 
		Tracer : STRING[80];
		Status : UINT;
	END_STRUCT;
	ElaHmiTyp : 	STRUCT 
		Status : USINT;
		Visibility : USINT;
	END_STRUCT;
END_TYPE
