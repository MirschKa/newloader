
TYPE
	PasswordStatus_type : 	STRUCT 
		DORead : UINT;
		DOInfo : UINT;
		DOWrite : UINT;
		DOCreate : UINT;
		Dialog : UINT;
	END_STRUCT;
END_TYPE
