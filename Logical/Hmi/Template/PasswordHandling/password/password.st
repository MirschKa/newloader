(************************************************************************************************************************)
(* Object name: password                                                                                                *)
(* Author:      Roman Haslinger                                                                                         *)
(* Site:        B&R Eggelsberg                                                                                          *)
(* Created:     12-aug-2003                                                                                             *)
(* Restriction: consider task order                                                                                     *)
(* Description: Password handling                                                                                       *)
(*                                                                                                                      *)
(*----------------------------------------------------------------------------------------------------------------------*)
(* Additional information:                                                                                              *)
(*                                                                                                                      *)
(* No additional information available.                                                                                 *)
(*                                                                                                                      *)
(*----------------------------------------------------------------------------------------------------------------------*)
(* Version 3.00.0  18-jan-2012  Samuel Otto                     Development tool: B&R Automation Studio V3.0.90.19 SP01 *)
(*                                                                                                                      *)
(* Cleaned up this revision history and set version to V3.00.0                                                          *)
(*--------------------------------------------------------------------------------------------------------------------- *)
(*         3.00.1  17-sep-2012  FTG                      Development tool: B&R Automation Studio V3.0.90.21 SP03    	*)
(* default init of config para in INIT changed to allow machine task to predefine                                       *)
(*--------------------------------------------------------------------------------------------------------------------- *)
(*         3.00.2  17-oct-2013  EC                      Development tool: B&R Automation Studio V3.0.90.21 SP03    		*)
(* [#79] Logout via button in login dialog (password / rfid login)														*)
(*       - Create new TPL_Lockh.In.Cmd.StatusForLockingPasswordInput and fill in if input password is pressed			*)
(*       - No Logout, if user password input is wrong																	*)
(*--------------------------------------------------------------------------------------------------------------------- *)
(*         3.00.3  31-jan-2018  DoB                      Development tool: B&R Automation Studio V4.1.14.40 SP   		*)
(* [#505] Login AlphaNumPad is locked by page change																	*)
(*       - Prevent the AlphaNumPad's status variable from getting into an invalid state 								*)
(*		 - If variable reaches an invalid state, the cmd and the reminder variable will be reset						*)
(************************************************************************************************************************)
(* Version 3.00.4  01-march-2018  FB                            Development tool: B&R Automation Studio V4.1.15.54 SP   *)
(* [#504] Extend GEA Login System to Support B&R 5E9030.29 RFID Reader           										*)
(*        # set TPL_Password.In.Cfg.Login.LoginSystemWith5E9030 as default Reader in HMISys                             *)
(************************************************************************************************************************)

PROGRAM _INIT
(* default passwords, if data object 'pword' does not exist *)
brsstrcpy(ADR(TPL_Password.InOut.Password[0]),ADR('147'));		(* Password level 1 *)
brsstrcpy(ADR(TPL_Password.InOut.Password[1]),ADR('369'));		(* Password level 2 *)
brsstrcpy(ADR(TPL_Password.InOut.Password[2]),ADR('1604'));		(* Password level 3 *)
brsstrcpy(ADR(TPL_Password.InOut.Password[3]),ADR('1310'));		(* Password level 4 *)
brsstrcpy(ADR(TPL_Password.InOut.Password[4]),ADR('75319'));	(* Password level 5 *)
brsstrcpy(ADR(TPL_Password.InOut.Password[5]),ADR('46564'));	(* Password level 6 *)
brsstrcpy(ADR(TPL_Password.InOut.Password[6]),ADR('492349236'));(* Password level 7 *)
brsstrcpy(ADR(TPL_Password.InOut.Password[7]),ADR('32808'));	(* Password level 8 *)
brsstrcpy(ADR(TPL_Password.InOut.Password[8]),ADR('59531'));	(* Password level 9 *)

//TPL_Password.In.Cfg.Login.LoginSystem := TPL_DISABLED;	(* Set to "1" to activate CFS Login System by default; set to "0" to leave it deactivated by default *)
IF TPL_Password.In.Cfg.Login.LoginSystemMaxMode > 2 THEN
	TPL_Password.In.Cfg.Login.LoginSystemMaxMode := 2;	(* Maximum login system mode the customer can select on configuring the system (currently supported mode 0 & 1) *)
END_IF
(* Current active login system to be used (customer can change later using USBMasterKey; screen 5120) *)
TPL_Password.In.Cfg.Login.LoginSystemMode := LIMIT(0, TPL_Password.In.Cfg.Login.LoginSystemMode, TPL_Password.In.Cfg.Login.LoginSystemMaxMode);

(*----------------------------------------------------------------------------------------------------------------------*)
(*  Comment to use B&R 5E9010.29 ISO Transponder reader/writer device, obsolet since 2017                               *)
(*  Uncomment to use B&R 5E9030.29 ISO Transponder reader/writer device as default Reader in HMISys                     *)                                                            
(*----------------------------------------------------------------------------------------------------------------------*)
//TPL_Password.In.Cfg.Login.LoginSystemWith5E9030 := TRUE; 
(* DoB, 27.03.2018: parameter 'TPL_Password.In.Cfg.Login.LoginSystemWith5E9030' is also availabel in task 'mach_id.st' as presetting for TPL *)

DialogInfo.HeaderTextGroupNumber:= TPL_DIALOG_HEADERTEXT;
DialogInfo.DialogTextGroupNumber:= TPL_DIALOG_TEXT;
DialogInfo.ButtonTextGroupNumber:= TPL_DIALOG_BUTTONTEXT;

PasswordStep:= 0;
END_PROGRAM

PROGRAM _CYCLIC
CASE PasswordStep OF

	WAIT:  (* WAIT *)

		(* Check if CFS Login System is enabled and together with that, the MasterUSBkey is found. If YES, the INVISIBLE*)
		(* Bit inside the status datapoint of the password input control will be set and the control will be made       *)
		(* invisible to avoid login actions by the user.                                                                *)
		(* This shall ensure, that no user access level can be made active while the MasterUSBkey is plugged and active!*)
		(* and so no UBS actions can be made active such as backup/restore which would run onto the same USB port(stick)*)

		IF (TPL_Password.In.Cfg.Login.LoginSystem = TPL_ENABLED) AND (TPL_IsoLoginSys_Global.Out.Stat.stIsoLoginSystemMasterKeyFound = TRUE) THEN
			IF (BitEvalWord AND 1) = 0 THEN (* check if invisible bit inside the status datapoint is 0 (= VISIBLE *)
				TPL_Password.Out.StatusPasswordInput:= TPL_Password.Out.StatusPasswordInput OR 1; (* set invisible bit (Bit 0 => decimal 1 *)
			END_IF;
		ELSE
			BitEvalWord:= TPL_Password.Out.StatusPasswordInput;
			IF (BitEvalWord AND 1) = 1 THEN (* check if invisible bit inside the status datapoint was set (= INVISIBLE *)
				TPL_Password.Out.StatusPasswordInput:= TPL_Password.Out.StatusPasswordInput XOR 1; (* reset invisible bit (Bit 0 => decimal 1 *)
			END_IF;
		END_IF;


			(* Check for login system disabled OR mode standard activated while login system activated but no IsoLoginSystemMasterKeyFound is plugged !!! *)
		IF (TPL_Password.In.Cfg.Login.LoginSystem = TPL_DISABLED) OR ((TPL_Password.In.Cfg.Login.LoginSystemMode = CFS_LOGIN_SYSTEM_MODE_STANDARD) AND NOT (TPL_IsoLoginSys_Global.Out.Stat.stIsoLoginSystemMasterKeyFound = TRUE)) THEN
			(* CFS Login System disabled OR Login mode "Standard" acitvated -> unlock user level input possibility via password input field *)
			(* Locking activated by setting Bit 1 of the controls status (2 dec = 00000010 BIN*)
			TPL_Password.Out.StatusPasswordInput:= BIT_CLR(TPL_Password.Out.StatusPasswordInput, 1);

			(* CFS Login System not enabled -> use "old standard" user level login system *)
			IF TPL_Password.In.Cmd.OpenPasswordAlphaPad = 1 THEN
				IF TPL_Password.Out.StatusPasswordInput <> 20480 AND AlphaPadOpened = 0 THEN
					TPL_Password.Out.StatusPasswordInput:= 8;
					AlphaPadOpened:= 1;
				ELSIF TPL_Password.Out.StatusPasswordInput = 20480 THEN
					TPL_Password.In.Cmd.OpenPasswordAlphaPad := 0;
					AlphaPadOpened:= 0;
				// User has interacted with visu(e.g. triggered a page change) before AlphaNumPad was successfully opened
				ELSIF (TPL_Password.Out.StatusPasswordInput = 0 OR TPL_Password.Out.StatusPasswordInput = 16384) AND TPL_Password.In.Cmd.OpenPasswordAlphaPad = 1 AND AlphaPadOpened = 1 THEN 
				 	// Kill the undefined state by resetting the Cmd- AND reminder-variable
					TPL_Password.In.Cmd.OpenPasswordAlphaPad := 0;
					AlphaPadOpened:= 0;
				END_IF;
			END_IF;
		ELSE
			(* CFS Login System enabled -> lock "old standard" user level input possibility via password input field *)
			(* Locking activated by setting Bit 1 of the controls status (2 dec = 00000010 BIN*)
			TPL_Password.Out.StatusPasswordInput:= BIT_SET(TPL_Password.Out.StatusPasswordInput, 1);
		END_IF;

		(* ------------  CFS Login System, differentiated logout handling -----------------------------*)
		(* If the CFS Login System is enabled and the selected mode is > Login Mode Standard (= 0 =>   *)
		(* usual old style password->user level handling without key card reader) then standard        *)
		(* logout handling must not be processed. Instead process CFS Login System specific logout.    *)
		(*---------------------------------------------------------------------------------------------*)

		IF (TPL_Password.In.Cfg.Login.LoginSystem = TPL_DISABLED) OR ((TPL_Password.In.Cfg.Login.LoginSystem = TPL_ENABLED) AND (TPL_Password.In.Cfg.Login.LoginSystemMode = CFS_LOGIN_SYSTEM_MODE_STANDARD)) THEN

			(* for logout *)
			IF TPL_Password.In.Cmd.Logout <> 0 THEN
				TPL_Password.In.Cmd.Logout:= 0;
				TPL_Password.Out.UserLevel:= 0; (* clear user level *)
				TPL_Password.InOut.NewPassword[0]:= 0;
			END_IF;
		ELSE
			(* CFS Login System logout processing *)
			IF TPL_Password.In.Cmd.Logout <> 0 THEN
				TPL_Password.In.Cmd.Logout:= 0;
				TPL_Password.Out.UserLevel:= 0; (* clear user level *)
				brsmemset(ADR(TPL_Password.InOut.NewPassword), 0, SIZEOF(TPL_Password.InOut.NewPassword)); (* 20100713 modified HH; clear the password input fields datapoint *)
			END_IF;

		END_IF;


		(* ------------  CFS Login System, interlock edit/password handling ---------------------------*)
		(* If the CFS Login System is enabled and the selected mode is > Login Mode Standard (= 0 =>   *)
		(* usual old style password->user level handling without key card reader, then the edit        *)
		(* must not be processed. This to avoid unexpected changes to the password list due to mis-    *)
		(* interpreted login data that is partially not avaliable with the key card reader modes 1..3  *)
		(*---------------------------------------------------------------------------------------------*)

		IF (TPL_Password.In.Cfg.Login.LoginSystem = TPL_DISABLED) OR ((TPL_Password.In.Cfg.Login.LoginSystem = TPL_ENABLED) AND (TPL_Password.In.Cfg.Login.LoginSystemMode = CFS_LOGIN_SYSTEM_MODE_STANDARD)) THEN

			(* only when password expert is included *)
			IF ChangePasswordSystem <> ButExpertPasswordOn THEN
				ChangePasswordSystem:= ButExpertPasswordOn;
				OldPassword[0]:= 0;
				TPL_Password.InOut.NewPassword[0]:= 0;
			END_IF;

			(* for login *)
			ResultPassword:= brsstrcmp(ADR(TPL_Password.InOut.NewPassword), ADR(OldPassword));
			IF ResultPassword <> 0 OR PasswordEntered = 1 THEN
				TPL_Password.InOut.SearchComplete:= 255; (* password searching starts *)
				PasswordStep:= SEARCH_PASSWORD;  (* SEARCH_PASSWORD *)
			END_IF;


			(* status variables for editing password *)
			(* if level is 0, all ...StatusChangePasswordInput should be 1 *)
			FOR xcnt:= 0 TO 8 DO
				IF 		xcnt < (TPL_Password.Out.UserLevel - 1) AND xcnt < 6
					AND TPL_Password.Out.UserLevel <> 0
					AND TPL_Password.Out.UserLevel > 4 THEN
					StatusChangePasswordInput[xcnt]:= 0;
				ELSE
					StatusChangePasswordInput[xcnt]:= 1;	(* value = 1 all invisible *)
				END_IF;
			END_FOR;

			IF 		(TPL_Password.Out.UserLevel >= 5)
				AND (NOT(ButExpertPasswordOn)) THEN

				FOR xcnt:= 0 TO 8 DO
					ResultPassword:= brsstrcmp(ADR(TPL_Password.InOut.Password[xcnt]), ADR(TPL_Password.InOut.ChangPassword[xcnt]));
					IF ResultPassword <> 0 THEN
						FOR ycnt:= 0 TO 8 DO
							ResultPassword:= brsstrcmp(ADR(TPL_Password.InOut.Password[ycnt]), ADR(TPL_Password.InOut.ChangPassword[xcnt]));
							IF ResultPassword = 0 THEN
								brsstrcpy(ADR(TPL_Password.InOut.ChangPassword[xcnt]), ADR(TPL_Password.InOut.Password[xcnt]));
								EXIT;
							ELSIF ycnt = 8 THEN
								brsstrcpy(ADR(TPL_Password.InOut.Password[xcnt]), ADR(TPL_Password.InOut.ChangPassword[xcnt]));
								EXIT;
							END_IF;
						END_FOR;
					END_IF;
				END_FOR;
			END_IF;
		END_IF;


	SEARCH_PASSWORD:  (* SEARCH_PASSWORD *)

		(* a configuration file was present on the cpu, so it is going to be read *)
		IF (TPL_Config.Out.StartUpFinished = 1) THEN

			FOR xcnt:= 0 TO 8 DO
				brsstrcpy(ADR(TPL_Password.InOut.ChangPassword[xcnt]), ADR(TPL_Password.InOut.Password[xcnt]));
			END_FOR;

			FOR xcnt:= 0 TO 8 DO

				brsstrcpy(ADR(pPassword), ADR(TPL_Password.InOut.Password[xcnt]));
				ResultPassword:= brsstrcmp(ADR(TPL_Password.InOut.NewPassword), ADR(pPassword));

				IF ResultPassword = 0 THEN
					brsstrcpy(ADR(OldPassword), ADR(TPL_Password.InOut.NewPassword));
					TPL_Password.Out.UserLevel:= UINT_TO_USINT(xcnt + 1);
					TPL_Password.InOut.SearchComplete:= 1; (* Password exists *)
					PasswordEntered:= 0;
					PasswordStep:= WAIT;  (* WAIT *)
					EXIT;
				ELSIF xcnt = 8 THEN
					IF TPL_Password.InOut.NewPassword[0] <> 0 THEN	(* Wrong password *)
						OldPassword[0]:= 0;
						TPL_Password.InOut.NewPassword[0]:= 0;
						TPL_Password.InOut.SearchComplete:= 2; (* Password not exists *)
						PasswordEntered:= 0;
						PasswordStep:= CALL_DIALOG;  (* WAIT *)
					ELSE (* Logout *)
						OldPassword[0]:= 0;
						TPL_Password.InOut.NewPassword[0]:= 0;
						PasswordEntered:= 0;
						PasswordStep:= WAIT;
					END_IF;
				END_IF;
			END_FOR;
		ELSE

			FOR xcnt:= 0 TO 8 DO
				brsstrcpy(ADR(TPL_Password.InOut.ChangPassword[xcnt]), ADR(TPL_Password.InOut.Password[xcnt]));
			END_FOR;

			FOR xcnt:= 0 TO 8 DO
				ResultPassword:= brsstrcmp(ADR(TPL_Password.InOut.NewPassword), ADR(TPL_Password.InOut.Password[xcnt]));
				IF ResultPassword = 0 THEN
					brsstrcpy(ADR(OldPassword), ADR(TPL_Password.InOut.NewPassword));
					TPL_Password.Out.UserLevel:= UINT_TO_USINT(xcnt + 1);
					TPL_Password.InOut.SearchComplete:= 1; (* Password exists *)
					PasswordEntered:= 0;
					PasswordStep:= WAIT;  (* WAIT *)
					EXIT;
				ELSIF xcnt = 8 THEN
					IF TPL_Password.InOut.NewPassword[0] <> 0 THEN	(* Wrong password *)
						OldPassword[0]:= 0;
						TPL_Password.InOut.NewPassword[0]:= 0;
						TPL_Password.Out.UserLevel:= 0;
						TPL_Password.InOut.SearchComplete:= 2; (* Password not exists *)
						PasswordEntered:= 0;
						PasswordStep:= CALL_DIALOG;  (* WAIT *)
					ELSE (* Logout *)
						OldPassword[0]:= 0;
						TPL_Password.InOut.NewPassword[0]:= 0;
						PasswordEntered:= 0;
						PasswordStep:= WAIT;
					END_IF;
				END_IF;
			END_FOR;
		END_IF;

	CALL_DIALOG: (* Wrong password *)

		DialogInfo.HeaderTextNumber:= DHT_PASSWORD;
		DialogInfo.DialogTextNumber:= DT_WRONG_PASSWORD;
		DialogInfo.NumberOfButton:= 1;
		DialogInfo.ButtonTextNumberOne:= DBT_OK;
		(* call dialog function *)
		Status.Dialog:= CallDialog(ADR(DialogIdent), ADR(DialogInfo), TPL_Dialogh.Out.AdrDialogRingBuffer);

		IF DialogInfo.Return_PressedButton = 1 THEN
			PasswordStep:= WAIT;
		END_IF

END_CASE;

	IF TPL_Password.Out.StatusPasswordInput.0 = FALSE THEN // visible		
		IF TPL_Password.Out.StatusPasswordInput.1 = FALSE THEN // not locked, it's an inputfield
			PasswordInputFieldStatusDPInv	:= 1; // Outputfield invisible
			PasswordInputFieldStatusDP		:= BIT_CLR(PasswordInputFieldStatusDP,0);	// Inputfield visible
			IF PasswordInputFieldStatusDP.13 = TRUE THEN
				PasswordInputFieldStatusDP.13 := FALSE;
				TPL_Lockh.In.Cmd.StatusForLockingPasswordInput := 8192;
			END_IF
		ELSE // locked, it's an outputfield
			PasswordInputFieldStatusDP		:= 1;
			PasswordInputFieldStatusDPInv	:= 0;
		END_IF
	ELSE // invisible (Masterkey is active)
		PasswordInputFieldStatusDP			:= 1;
		PasswordInputFieldStatusDPInv		:= 1;
	END_IF

END_PROGRAM

