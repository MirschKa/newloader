
TYPE
	T_CFS_IsoLoginSys_Commands : 	STRUCT 
		cmdSearchForReader : BOOL;
		cmdResetLoginCycleStepper : BOOL;
	END_STRUCT;
	T_CFS_IsoLoginSys_Processing : 	STRUCT 
		TpUsersPasswordOnKey : STRING[80];
		TpUsersPasswordInput : STRING[80];
	END_STRUCT;
	T_CFS_IsoLoginSys_Handling : 	STRUCT 
		TpInterfaceName : STRING[80];
		TpInterfaceName_2Reader : ARRAY[0..1]OF STRING[127];
		TpProcessingStep : UINT;
		TpCardProcessingStatus : USINT;
		TpChipValidationResult : USINT;
		TpReaderInitErrorNo : UINT;
		TpReadmFirstBlock : USINT;
		TpRssiIndication : USINT;
		TpChipUidFoundInRange : STRING[20];
		TpErrorNo : UDINT;
		TpErrorStep : UINT;
		TpUserPswdInputRetryCount : USINT;
		TpInfoFubEnabled : BOOL;
		TpReadmFubEnabled : BOOL;
		TpCountResetLoginCycleExecs : UDINT;
	END_STRUCT;
	T_CFS_IsoLoginSys_Hmi : 	STRUCT 
		stIsoLoginSysBtnGotoCfgPage : UINT;
		stIsoLoginSysVisCtrls : UINT;
		stIsoLoginSysShowSignalStrength : UINT;
	END_STRUCT;
	T_Iso15693_ChipData : 	STRUCT 
		ReadBuffer : ARRAY[0..1023]OF USINT;
		ReadBufferCopy : ARRAY[0..1023]OF USINT;
		UID : STRING[20];
		KeyUserLevel : USINT;
		KeyExpirationDate : DATE_AND_TIME;
		KeyUserPassword : STRING[80];
		KeyLoginSystemMode : USINT;
	END_STRUCT;
	T_Iso15693_ChipProperties : 	STRUCT 
		BlockSize : USINT;
		NumBlocks : USINT;
		MemSize : UINT;
		MultipleBlocks : USINT;
	END_STRUCT;
	T_Iso15693_LibraryData : 	STRUCT 
		Ident : UDINT;
	END_STRUCT;
	T_Iso15693_RuntimeInfo : 	STRUCT 
		TaskCycleTime : UDINT;
	END_STRUCT;
	T_CFS_IsoLoginSys_Settings : 	STRUCT 
		cfgUseAutoCycleRestartOnError : BOOL;
	END_STRUCT;
	T_CFS_IsoLoginSys_Internal : 	STRUCT 
		Commands : T_CFS_IsoLoginSys_Commands;
		Processing : T_CFS_IsoLoginSys_Processing;
		Handling : T_CFS_IsoLoginSys_Handling;
		Hmi : T_CFS_IsoLoginSys_Hmi;
		ChipData : T_Iso15693_ChipData;
		ChipProperties : T_Iso15693_ChipProperties;
		LibraryData : T_Iso15693_LibraryData;
		RuntimeInfo : T_Iso15693_RuntimeInfo;
		Settings : T_CFS_IsoLoginSys_Settings;
	END_STRUCT;
END_TYPE
