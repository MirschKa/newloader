(************************************************************************************************************************)
(* Object name: lockh                                                                                                   *)
(* Author:      Roman Haslinger                                                                                         *)
(* Site:        B&R Eggelsberg                                                                                          *)
(* Created:     24-oct-2003                                                                                             *)
(* Restriction: consider task order.                                                                                    *)
(* Description: Controls locked inputfields                                                                             *)
(*                                                                                                                      *)
(*----------------------------------------------------------------------------------------------------------------------*)
(* Additional information:                                                                                              *)
(*                                                                                                                      *)
(* No additional information available.                                                                                 *)
(*                                                                                                                      *)
(*----------------------------------------------------------------------------------------------------------------------*)
(* Version 3.00.0  18-jan-2012  Samuel Otto                     Development tool: B&R Automation Studio V3.0.90.19 SP01 *)
(*                                                                                                                      *)
(* Cleaned up this revision history and set version to V3.00.0                                                          *)
(*--------------------------------------------------------------------------------------------------------------------- *)
(*         3.00.1  17-oct-2013  EC                      Development tool: B&R Automation Studio V3.0.90.21 SP03    		*)
(* [#79] Logout via button in login dialog (password / rfid login)														*)
(*       - Change Case:	User level input in global layer, show AlphaPad if user level is =0								*)
(*       				User level input in global layer, show Dialogbox (Login/Logout/Cancel) if user level is >0		*)
(*       				Any Control (Status Variable)show Dialogbox	(Login/Cancel)										*)
(************************************************************************************************************************)
(* Version 3.04.1  01-jul-2015  SO                              Development tool: B&R Automation Studio V3.0.90.30 SP12 *)
(* [#421] Dialogtext not OK if wrong level to enter value and login system active										*)
(* 		- Changed dialog in case of wrong user level with login system in step CALL_DIALOG_LOGINSYS						*)
(************************************************************************************************************************)


PROGRAM _INIT
(* dialog parameter for touch calibration *)
DialogInfo.HeaderTextGroupNumber:= TPL_DIALOG_HEADERTEXT;
DialogInfo.HeaderTextNumber:= DHT_PASSWORD;
DialogInfo.DialogTextGroupNumber:= TPL_DIALOG_TEXT;
END_PROGRAM

PROGRAM _CYCLIC
CASE LockingStep OF

	CONTROL_LOCKING:

		IF TPL_Pageh.Out.ActualPage >= 7000 AND TPL_Pageh.Out.ActualPage < 8000 THEN
			LockingDirectJump:= 1;
		ELSE
			LockingDirectJump:= 0;
		END_IF
		
		// If we use passwords, make the button for entering passwords on page 5100 visible, else invisible
		IF TPL_Password.In.Cfg.Login.LoginSystemMode = CFS_LOGIN_SYSTEM_MODE_STANDARD THEN
			StatusOpenPasswordInput := TPL_VISIBLE;
		ELSE
			StatusOpenPasswordInput := TPL_INVISIBLE;
		END_IF

		IF LockingDirectJump = 1 THEN
			StatusScreenNrIputField			:= TPL_INVISIBLE;
			StatusScreenNrIputFieldInverted	:= TPL_VISIBLE;
		ELSE
			StatusScreenNrIputField			:= TPL_VISIBLE;
			StatusScreenNrIputFieldInverted	:= TPL_INVISIBLE;
		END_IF
		IF TPL_Config.In.Data.Password.Login.LoginSystemMode = 2 AND TPL_Password.Out.UserLevel <= 5 THEN
			TPL_Lockh.In.Cmd.StatusForLockingLogoutButton.0 := 1;
		ELSE
			TPL_Lockh.In.Cmd.StatusForLockingLogoutButton.0 := 0;
		END_IF	
		//IF TPL_Lockh.In.Cmd.StatusForLockingInputFields = 8192 OR TPL_Lockh.In.Cmd.StatusForLockingButtons = 8192 OR TPL_Lockh.In.Cmd.StatusForLockingPasswordInput = 8192 THEN
		IF BIT_TST(TPL_Lockh.In.Cmd.StatusForLockingInputFields,13) OR BIT_TST(TPL_Lockh.In.Cmd.StatusForLockingButtons,13) OR BIT_TST(TPL_Lockh.In.Cmd.StatusForLockingPasswordInput,13) OR BIT_TST(TPL_Lockh.In.Cmd.StatusForLockingLogoutButton,13) THEN
				
				
	(* ~~~~~V2.03.2/V2.03.3 CFS ISO Login System, modified, begin *)

			(* MasterKey found => Only dialog with OK button (no manual login action trigger possible) *)
			IF TPL_IsoLoginSys_Global.Out.Stat.stIsoLoginSystemMasterKeyFound THEN
				LockingStep:= CALL_DIALOG_LOGINSYS;

			(* Login system enabled and key based mode activated *)
			ELSIF (TPL_Password.In.Cfg.Login.LoginSystem = TPL_ENABLED) AND (TPL_Password.In.Cfg.Login.LoginSystemMode <> CFS_LOGIN_SYSTEM_MODE_STANDARD) THEN
				LockingStep:= CALL_DIALOG_LOGINSYS;

			(* Login system disabled *)(* Login system enabled but Mode Standard (touch pad input) activated *)
			ELSIF (TPL_Password.In.Cfg.Login.LoginSystem = TPL_DISABLED) OR ((TPL_Password.In.Cfg.Login.LoginSystem = TPL_ENABLED) AND (TPL_Password.In.Cfg.Login.LoginSystemMode = CFS_LOGIN_SYSTEM_MODE_STANDARD)) THEN
				
				IF TPL_Lockh.In.Cmd.StatusForLockingPasswordInput = 8192 THEN
					IF TPL_Password.Out.UserLevel = 0 THEN
						LockingStep:= SHOW_LOGIN;
					ELSE
						LockingStep:= CALL_DIALOG_LOG_LOGOUT_CANCEL;
					END_IF
				ELSE
					LockingStep:= CALL_DIALOG_LOG_CANCEL;
				END_IF
			END_IF;
	(* ~~~~~V2.03.2/V2.03.3 CFS ISO Login System, modified, end *)
		END_IF

	CALL_DIALOG_LOG_CANCEL:
	(* ~~~~~ V2.03.2, CFS ISO Login System, modified, begin *)
		DialogInfo.DialogTextNumber			:= DT_ENTER_PASSWORD;
		DialogInfo.NumberOfButton			:= 2;
		DialogInfo.ButtonTextGroupNumber	:= TPL_DIALOG_BUTTONTEXT;
		DialogInfo.ButtonTextNumberOne		:= DBT_LOGIN;
		DialogInfo.ButtonTextNumberTwo		:= DBT_CANCEL;
	(* ~~~~~ V2.03.2, CFS ISO Login System, modified, end *)

		CallDialog(ADR(DialogIdent), ADR(DialogInfo), TPL_Dialogh.Out.AdrDialogRingBuffer);
		IF DialogInfo.Return_PressedButton = 1 THEN
			LockingStep						:= SHOW_LOGIN;
		ELSIF DialogInfo.Return_PressedButton = 2 THEN
			LockingStep						:= RESET;
		END_IF


	CALL_DIALOG_LOG_LOGOUT_CANCEL:
		DialogInfo.DialogTextNumber			:= DT_ENTER_PASSWORD;
		DialogInfo.NumberOfButton			:= 3;
		DialogInfo.ButtonTextGroupNumber	:= TPL_DIALOG_BUTTONTEXT;
		DialogInfo.ButtonTextNumberOne		:= DBT_LOGIN;
		DialogInfo.ButtonTextNumberTwo		:= DBT_LOGOUT;
		DialogInfo.ButtonTextNumberThree	:= DBT_CANCEL;

		CallDialog(ADR(DialogIdent), ADR(DialogInfo), TPL_Dialogh.Out.AdrDialogRingBuffer);
		IF DialogInfo.Return_PressedButton = 1 THEN
			LockingStep						:= SHOW_LOGIN;
		ELSIF DialogInfo.Return_PressedButton = 2 THEN
			TPL_Password.In.Cmd.Logout		:=TRUE;
			LockingStep						:= RESET;
		ELSIF DialogInfo.Return_PressedButton = 3 THEN
			LockingStep						:= RESET;
		END_IF

	SHOW_LOGIN:
			TPL_Password.In.Cmd.OpenPasswordAlphaPad		:= 1;
			LockingStep										:= RESET;

	RESET:	
			// Edit DoB, 22.09.2019: Its only necessary to reset bit 13 (-> stausdatapoint) and not the whole variable
			//TPL_Lockh.In.Cmd.StatusForLockingPasswordInput:= 0;
			//TPL_Lockh.In.Cmd.StatusForLockingInputFields	:= 0;
			//TPL_Lockh.In.Cmd.StatusForLockingButtons		:= 0;
			TPL_Lockh.In.Cmd.StatusForLockingPasswordInput	:= BIT_CLR(TPL_Lockh.In.Cmd.StatusForLockingPasswordInput,13);
			TPL_Lockh.In.Cmd.StatusForLockingInputFields	:= BIT_CLR(TPL_Lockh.In.Cmd.StatusForLockingInputFields,13);
			TPL_Lockh.In.Cmd.StatusForLockingButtons		:= BIT_CLR(TPL_Lockh.In.Cmd.StatusForLockingButtons,13);
			TPL_Lockh.In.Cmd.StatusForLockingButtons		:= BIT_CLR(TPL_Lockh.In.Cmd.StatusForLockingLogoutButton,13);
			LockingStep										:= CONTROL_LOCKING;

	(* ~~~~~ V2.03.2, CFS ISO Login System, modified, begin *)
	CALL_DIALOG_LOGINSYS:
		DialogInfo.DialogTextNumber			:= DT_WRONG_USER_LEVEL;
		DialogInfo.NumberOfButton			:= 1;
		DialogInfo.ButtonTextGroupNumber	:= TPL_DIALOG_BUTTONTEXT;
		DialogInfo.ButtonTextNumberOne		:= DBT_OK;

		CallDialog(ADR(DialogIdent), ADR(DialogInfo), TPL_Dialogh.Out.AdrDialogRingBuffer);
			IF DialogInfo.Return_PressedButton = 1 THEN
			// Edit DoB, 22.09.2019: Its only necessary to reset bit 13 (-> stausdatapoint) and not the whole variable
			//TPL_Lockh.In.Cmd.StatusForLockingInputFields	:= 0;
			//TPL_Lockh.In.Cmd.StatusForLockingButtons		:= 0;
			TPL_Lockh.In.Cmd.StatusForLockingInputFields	:= BIT_CLR(TPL_Lockh.In.Cmd.StatusForLockingInputFields,13);
			TPL_Lockh.In.Cmd.StatusForLockingButtons		:= BIT_CLR(TPL_Lockh.In.Cmd.StatusForLockingButtons,13);
			TPL_Lockh.In.Cmd.StatusForLockingButtons		:= BIT_CLR(TPL_Lockh.In.Cmd.StatusForLockingLogoutButton,13);
			LockingStep										:= CONTROL_LOCKING;
		END_IF
	(* ~~~~~ V2.03.2, CFS ISO Login System, modified, end *)

END_CASE
END_PROGRAM

