
FUNCTION EncodeB64 : UINT (*Encodes amount of data with base64. NeededOutput shows the number of used bytes of Out-data*)
	VAR_INPUT
		adrIn : UDINT;
		sizeIn : UDINT;
		adrOut : UDINT;
		sizeOut : UDINT;
		neededOutput : REFERENCE TO UDINT;
	END_VAR
	VAR
		actposIN : UDINT;
		actposOUT : UDINT;
		blockLen : USINT;
	END_VAR
END_FUNCTION

FUNCTION DecodeB64 : UINT (*Decodes amount of data with base64. NeededOutput shows the number uf used bytes of Out-data*)
	VAR_INPUT
		adrIn : UDINT;
		sizeIn : UDINT;
		adrOut : UDINT;
		sizeOut : UDINT;
	END_VAR
	VAR
		neededOutput : UDINT;
		actposIN : UDINT;
		actposOUT : UDINT;
		blockLen : USINT;
	END_VAR
END_FUNCTION

FUNCTION EncodeBlockB64 : UINT (*Encodes a block of 1-3 bytes (len) with base64 to exactly 4 bytes*)
	VAR_INPUT
		in : REFERENCE TO ARRAY[0..2] OF USINT;
		out : REFERENCE TO ARRAY[0..3] OF USINT;
		len : REFERENCE TO USINT;
	END_VAR
	VAR
		key : ARRAY[0..63] OF USINT;
	END_VAR
END_FUNCTION

FUNCTION DecodeBlockB64 : UINT (*Decodes a block of exact 4 bytes with base64 to a block of 1-3 (len) bytes*)
	VAR_INPUT
		in : REFERENCE TO ARRAY[0..3] OF USINT;
		out : REFERENCE TO ARRAY[0..2] OF USINT;
		len : REFERENCE TO USINT;
	END_VAR
	VAR
		key : ARRAY[0..63] OF USINT;
		inbuffer : ARRAY[0..3] OF USINT;
		i : USINT;
		j : USINT;
	END_VAR
END_FUNCTION
