(************************************************************************************************************************)
(* Object name: DecodeBlockB64                                                                                          *)
(* Author:      Samuel Otto                                                                                             *)
(* Site:        B&R Bad Homburg                                                                                         *)
(* Created:     16-may-2011                                                                                             *)
(* Restriction: consider task order.                                                                                    *)
(* Description: Encoding and Decoding with base64                                                                       *)
(*                                                                                                                      *)
(*----------------------------------------------------------------------------------------------------------------------*)
(* Additional information:                                                                                              *)
(*                                                                                                                      *)
(* No additional information available.                                                                                 *)
(*                                                                                                                      *)
(*----------------------------------------------------------------------------------------------------------------------*)
(* Version 1.00   16-may-2011  Samuel Otto                      Development tool: B&R Automation Studio V3.0.81.26 SP04 *)
(*                                                                                                                      *)
(* First release.                                                                                                       *)
(*--------------------------------------------------------------------------------------------------------------------- *)
(************************************************************************************************************************)

FUNCTION DecodeBlockB64
	brsmemcpy(ADR(key), ADR('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'), SIZEOF(key));
	len := 0;
	FOR i := 0 TO 3 DO	// Translate from characters to originally 6-bit blocks
		FOR j := 0 TO (SIZEOF(key) / SIZEOF(key[0]) - 1) DO
			IF in[i] = key[j] THEN
				inbuffer[i] := j;
				EXIT;
			ELSIF in[i] = 61 THEN
				inbuffer[i] := 0;
				len := BIT_SET(len, i);	// Set the bit, if "=" found. Validation see below.
				EXIT;
			END_IF
		END_FOR
		IF j = SIZEOF(key) / SIZEOF(key[0]) THEN	// No character from key and no "=", so it's a wrong character!
			DecodeBlockB64 := BASE64_ERR_CHAR;
			RETURN;
		END_IF
	END_FOR
	(* Check if we have a valid code and set len *)
	IF len = 0 THEN // no "=" found, so we have 3 bytes with data
		len := 3;
	ELSIF len.2 = TRUE THEN	// byte 2 has a "=". If the code is valid, also byte 3 must have a "=", all others not.
		IF len = 12 THEN
			len := 1;
		ELSE
			len := 0;
			DecodeBlockB64 := BASE64_ERR_CHAR_ORDER;
			RETURN;
		END_IF
	ELSIF len = 8 THEN	// only byte 3 has a "="
		len := 2;
	ELSE	// wrong code. Maybe byte 1 or 2 contains "=". This is not valid!
		len := 0;
		DecodeBlockB64 := BASE64_ERR_CHAR_ORDER;
		RETURN;
	END_IF
	// connect the 4 6-bit blocks to 3 byte-blocks
	out[0] := SHL(inbuffer[0],2) OR SHR(inbuffer[1],4);
	IF len = 2 OR len = 3 THEN
		out[1] := SHL(inbuffer[1],4) OR SHR(inbuffer[2],2);
	END_IF
	IF len = 3 THEN
		out[2] := SHL(inbuffer[2],6) OR inbuffer[3];
	END_IF
	DecodeBlockB64 := 0;
END_FUNCTION