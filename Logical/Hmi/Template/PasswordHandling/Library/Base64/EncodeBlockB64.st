(************************************************************************************************************************)
(* Object name: EncodeBlockB64                                                                                          *)
(* Author:      Samuel Otto                                                                                             *)
(* Site:        B&R Bad Homburg                                                                                         *)
(* Created:     16-may-2011                                                                                             *)
(* Restriction: consider task order.                                                                                    *)
(* Description: Encoding and Decoding with base64                                                                       *)
(*                                                                                                                      *)
(*----------------------------------------------------------------------------------------------------------------------*)
(* Additional information:                                                                                              *)
(*                                                                                                                      *)
(* No additional information available.                                                                                 *)
(*                                                                                                                      *)
(*----------------------------------------------------------------------------------------------------------------------*)
(* Version 1.00   16-may-2011  Samuel Otto                      Development tool: B&R Automation Studio V3.0.81.26 SP04 *)
(*                                                                                                                      *)
(* First release.                                                                                                       *)
(*----------------------------------------------------------------------------------------------------------------------*)
(************************************************************************************************************************)

FUNCTION EncodeBlockB64
	IF len <= 0 OR len > 3 THEN	// Len is the number of bytes to encode (1-3)
		EncodeBlockB64 := BASE64_ERR_LEN;
		RETURN;
	END_IF
	brsmemcpy(ADR(key), ADR('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'), SIZEOF(key));
	out[0] := key[ SHR(in[0],2) ];	// always
	IF len = 1 THEN
		out[1] := key[ SHL((in[0] AND 16#03),4)];
		out[2] := 61;	// 61 = '='
		out[3] := 61;	// 61 = '='
	ELSIF len = 2 THEN
		out[1] := key[ SHL((in[0] AND 16#03),4) OR SHR(in[1],4) ];
		out[2] := key[ SHL((in[1] AND 16#0F),2)];
		out[3] := 61;	// 61 = '='
	ELSIF len = 3 THEN
		out[1] := key[ SHL((in[0] AND 16#03),4) OR SHR(in[1],4) ];
		out[2] := key[ SHL((in[1] AND 16#0F),2) OR SHR(in[2],6) ];
		out[3] := key[ in[2] AND 16#3F ];
	END_IF
	EncodeBlockB64 := 0;
END_FUNCTION
