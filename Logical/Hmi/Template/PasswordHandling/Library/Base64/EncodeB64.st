(************************************************************************************************************************)
(* Object name: EncodeB64                                                                                               *)
(* Author:      Samuel Otto                                                                                             *)
(* Site:        B&R Bad Homburg                                                                                         *)
(* Created:     16-may-2011                                                                                             *)
(* Restriction: consider task order.                                                                                    *)
(* Description: Encoding and Decoding with base64                                                                       *)
(*                                                                                                                      *)
(*----------------------------------------------------------------------------------------------------------------------*)
(* Additional information:                                                                                              *)
(*                                                                                                                      *)
(* No additional information available.                                                                                 *)
(*                                                                                                                      *)
(*----------------------------------------------------------------------------------------------------------------------*)
(* Version 1.00   16-may-2011  Samuel Otto                      Development tool: B&R Automation Studio V3.0.81.26 SP04 *)
(*                                                                                                                      *)
(* First release.                                                                                                       *)
(*----------------------------------------------------------------------------------------------------------------------*)
(************************************************************************************************************************)

FUNCTION EncodeB64
	neededOutput := (sizeIn/3)*4 + BOOL_TO_UDINT(UDINT_TO_BOOL(sizeIn MOD 3))*4;
	IF neededOutput > sizeOut THEN
		EncodeB64 := BASE64_ERR_OUTSIZE_TO_SMALL;	// Outbuffer too small
		RETURN;
	END_IF
	brsmemset(adrOut, 0, sizeOut);	// clear outbuffer
	actposIN := adrIn;
	actposOUT := adrOut;
	WHILE actposIN < adrIn + sizeIn DO
		blockLen := UDINT_TO_USINT(MIN(3, adrIn + sizeIn - actposIN));
		EncodeB64 := EncodeBlockB64(actposIN, actposOUT, ADR(blockLen));
		IF EncodeB64 <> 0 THEN
			RETURN;
		END_IF
		actposIN := actposIN + 3;
		actposOUT := actposOUT + 4;
	END_WHILE
END_FUNCTION
