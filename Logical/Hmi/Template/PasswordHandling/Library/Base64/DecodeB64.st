(************************************************************************************************************************)
(* Object name: DecodeB64                                                                                               *)
(* Author:      Samuel Otto                                                                                             *)
(* Site:        B&R Bad Homburg                                                                                         *)
(* Created:     16-may-2011                                                                                             *)
(* Restriction: consider task order.                                                                                    *)
(* Description: Encoding and Decoding with base64                                                                       *)
(*                                                                                                                      *)
(*----------------------------------------------------------------------------------------------------------------------*)
(* Additional information:                                                                                              *)
(*                                                                                                                      *)
(* No additional information available.                                                                                 *)
(*                                                                                                                      *)
(*----------------------------------------------------------------------------------------------------------------------*)
(* Version 1.00   16-may-2011  Samuel Otto                      Development tool: B&R Automation Studio V3.0.81.26 SP04 *)
(*                                                                                                                      *)
(* First release.                                                                                                       *)
(*----------------------------------------------------------------------------------------------------------------------*)
(************************************************************************************************************************)

FUNCTION DecodeB64
	IF (sizeIn MOD 4) <> 0 THEN
		DecodeB64 := BASE64_ERR_WRONG_INSIZE;	// Wrong insize
		RETURN;
	END_IF
	neededOutput := sizeIn/4 * 3;	// Outputsize if all bytes are used.
	IF brsmemcmp(adrIn + sizeIn - 2, ADR('='), 1) = 0 THEN	// Check if "==" is at the end
		neededOutput := neededOutput - 2;
	ELSIF brsmemcmp(adrIn + sizeIn - 1, ADR('='), 1) = 0 THEN	// Check if "=" is at the end
		neededOutput := neededOutput -1;
	END_IF
	IF neededOutput > sizeOut THEN
		DecodeB64 := BASE64_ERR_OUTSIZE_TO_SMALL;	// Outbuffer too small
		RETURN;
	END_IF
	brsmemset(adrOut, 0, sizeOut);	// clear outbuffer
	actposIN := adrIn;
	actposOUT := adrOut;
	WHILE actposIN < adrIn + sizeIn DO
		DecodeB64 := DecodeBlockB64(actposIN, actposOUT, ADR(blockLen));
		IF DecodeB64 <> 0 THEN
			RETURN;
		END_IF
		actposIN := actposIN + 4;
		actposOUT := actposOUT + 3;
	END_WHILE
END_FUNCTION
