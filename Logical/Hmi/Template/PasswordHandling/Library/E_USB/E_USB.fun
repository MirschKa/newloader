
FUNCTION_BLOCK USBFindDevice
	VAR_INPUT
		EN : BOOL;
		DeviceVendorID : UINT;
		DeviceProductID : UINT;
		DeviceBcd : UINT;
	END_VAR
	VAR_OUTPUT
		ErrorStep : UINT;
		Error : BOOL;
		Status : UINT;
		DeviceFound : BOOL;
		DeviceInterfaceName : STRING[127];
		Done : BOOL;
		CountDevicesFound : UINT;
	END_VAR
	VAR
		R_TRIG_0 : R_TRIG;
		Step : UINT;
		UsbNodeListGet_0 : UsbNodeListGet;
		NodeListBuffer : ARRAY[0..24] OF UDINT;
		StatusNodeListGet : UINT;
		NodesFoundConnected : UDINT;
		NodeIndexInList : UDINT;
		UsbNodeGet_0 : UsbNodeGet;
		UsbNodeBuffer : usbNode_typ;
		StatusNodeGet : UINT;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK USBFindDevice1
	VAR_INPUT
		EN : BOOL;
		DeviceVendorID : UINT;
		DeviceProductID : UINT;
		DeviceBcd : UINT;
	END_VAR
	VAR_OUTPUT
		ErrorStep : UINT;
		Error : BOOL;
		Status : UINT;
		DeviceFound : BOOL;
		DeviceInterfaceName : ARRAY[0..MAX_DEV_IDX] OF STRING[127];
		Done : BOOL;
		CountDevicesFound : UINT;
	END_VAR
	VAR
		R_TRIG_0 : R_TRIG;
		Step : UINT;
		UsbNodeListGet_0 : UsbNodeListGet;
		NodeListBuffer : ARRAY[0..24] OF UDINT;
		StatusNodeListGet : UINT;
		NodesFoundConnected : UDINT;
		NodeIndexInList : UDINT;
		UsbNodeGet_0 : UsbNodeGet;
		UsbNodeBuffer : ARRAY[0..MAX_DEV_IDX] OF usbNode_typ;
		StatusNodeGet : UINT;
	END_VAR
END_FUNCTION_BLOCK
