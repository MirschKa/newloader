(************************************************************************************************************************)
(* Object name: USBFindDevice                                                                                           *)
(* Author:      Hartmut Hoffmann                                                                                        *)
(* Site:        CFS Wallau                                                                                              *)
(* Created:     16-dec-2009                                                                                             *)
(* Restriction: No known restrictions.                                                                                  *)
(* Description: Library function block to search for a specific USB Device to the USB bus and, if found, to return the  *)
(*              interface name it is connected to                                                                       *)
(*                                                                                                                      *)
(*----------------------------------------------------------------------------------------------------------------------*)
(* Additional information:                                                                                              *)
(*                                                                                                                      *)
(* The USB device to search for is specified via its unique identification data which is "Product ID", "Vendor ID"      *)
(* and "BCD device". With these input parameters, the USB Bus is scanned using the B&R AsUsb FUB's "UsbNodeGet" and     *)
(* UsbNodeListGet". If the specified device is found connected in the node list, then its interface name is retrieved   *)
(* from the node details and returned to the FUB's DeviceInterfaceName output.                                          *)
(* fIf the device specified is not found, the FUB's output DeviceFound will return FALSE.                               *)
(*                                                                                                                      *)
(* Restrictions          : Will work with AS 2.7.x in general, but it has to be taken into consideration that some      *)
(* -------------           CFS related B&R USB Hardware can be detected only from a certain minimum Automation Runtime  *)
(*                         on (e.g. RFID Reader 5E9010.29 needs at least AR R2.95 to be transparent to, detecable by    *)
(*                         the system.                                                                                  *)
(* Important Usage                                                                                                      *)
(* information           : Due to the R_TRIG used to create a defiend entry point on activation of the FUB, it has to   *)
(* ---------------         made sure that the FUB is called cyclic without any condition upfront. The control of the    *)
(*                         find action should be done in controlling the EN input.                                      *)
(*                                                                                                                      *)
(* FUB-Inputs                                                                                                           *)
(* ----------                                                                                                           *)
(* EN                    : Turn execution of FBK on and off - needed for usage in ladder but more importnat to control  *)
(*                         the activation of the find process.                                                          *)
(* DeviceProductId       : USB device specifc identification data for its Product ID                                    *)
(* DeviceVendorId        : USB device specifc identification data for its Vendor ID                                     *)
(* DeviceBcd             : USB device specifc identification data for its BCD device code                               *)
(*                         Note: If DeviceBcd ist set to Zero (0), then the DeviceBcd parameter will not be used on     *)
(*                               finding (identifying) the USB device; only VendorId and ProductId will be used.        *)
(*                               The DeviceBcd is according to the USB Specification something like a Revision code of  *)
(*                               the device execution. If this code is more or less not of interest, it so can be left  *)
(*                               zero to reject it from the validation/find procedure.                                  *)
(*                                                                                                                      *)
(* FUB-Outputs                                                                                                          *)
(* -----------                                                                                                          *)
(* DeviceFound           : Resulting found/not found status. TRUE=Device found connected, Interface name output valid   *)
(* CountDevicesFound     : Number of devices found connected matching the search specs (if more than one device was     *)
(*                         connected having the same execution/specification, this figure will be > 1.                  *)
(* DeviceInterfaceName   : Resulting USB Interface name, it is found connected to. Valid only if "DeviceFound" = TRUE   *)
(*                         If more than one matching device was found, the interface string will represent the last     *)
(*                         matching device found in the list! A warning could be fired that more than one device was    *)
(*                         found on base of the content "CountDevicesFound".                                            *)
(* Error                 : Indicating that an error occured. If = TRUE -> evaluate status content for details           *)
(* Status                : Status reflects the respective error number of a FUB or an FUB internal assigned error       *)
(*                                                                                                                      *)
(* Status values         :    0 = o.k., no problem detected                                                             *)
(*                         5001 = One or more input parameters, specifying the device to find, missing                  *)
(*                         5002 = No USB device found connected to the PLCs USB BUS (more a status than an error)       *)
(*                         5003 = Specified USB Device could not be found in the list of connected devices (more a      *)
(*                                status than an error)                                                                 *)
(*                         other= Status/Result info of the internally called/used FUBs (AsUsb FUBs for instance)       *)
(*                                                                                                                      *)
(*----------------------------------------------------------------------------------------------------------------------*)
(* Version 3.00.0  18-jan-2012  Samuel Otto                     Development tool: B&R Automation Studio V3.0.90.19 SP01 *)
(*                                                                                                                      *)
(* Cleaned up this revision history and set version to V3.00.0                                                          *)
(*--------------------------------------------------------------------------------------------------------------------- *)
(************************************************************************************************************************)

FUNCTION_BLOCK USBFindDevice

	
	(* ===========================================================================
	   Set intial entry step of the case statement using an R_TRIG on the EN input
	   ===========================================================================*)
	
	R_TRIG_0(CLK:= EN);
	IF R_TRIG_0.Q = 1 THEN
		DeviceInterfaceName:= ''; (* Pre-init; Interface name not known yet *)
		DeviceFound:= FALSE; (* Pre-init; not found yet *)
		Error:= FALSE; (* Pre-init; no error yet *)
		ErrorStep:= 0; (* Pre-init; no error step info yet *)
		Done:= FALSE; (* We are going to process the job -> not done *)
		Step:= STEP_GET_NODE_LIST; (* Set first step to process *)
	END_IF;
	
	
	IF EN THEN (* FUB enabled ? *)
	
		(* ===========================================================================
		FBK Pre-Checks 
		===========================================================================*)
		IF (DeviceProductID = 0) OR (DeviceVendorID = 0) THEN (* According to the USB specification, Vendor ID and Product ID should be <> 0 *)
			Status:= ERR_FIND_USB_DEV_PARAMS_MISSED;
			DeviceInterfaceName:= '';
			DeviceFound:= FALSE; (* Params missing,; not able to find a device *)
			Error:= TRUE;
			Done:= TRUE; (* Job done *)
			RETURN; (* Exit FUB *)
		END_IF;
	
		(* ===========================================================================
		   FBK Processing
		   ===========================================================================*)
		CASE Step OF
	
			(* ---------------------------------------------------------------------------
			   Kind of wait or trap step if something forces the case to have a defined state
		   	   ---------------------------------------------------------------------------*)
			STEP_IDLE:
				Step:= Step; (* Dummy statement *)
	
				(* ---------------------------------------------------------------------------
				USB Bus Enumeration; search for USB devices connected to the PLC's USB Bus
				---------------------------------------------------------------------------*)
			STEP_GET_NODE_LIST:
				Status:= ERR_FUB_BUSY;
				UsbNodeListGet_0(enable:= TRUE, pBuffer:= ADR(NodeListBuffer), bufferSize:= SIZEOF(NodeListBuffer), filterInterfaceClass:= 0, filterInterfaceSubClass:= 0);
				StatusNodeListGet:= UsbNodeListGet_0.status; (* get function call status *)
				NodesFoundConnected:= UsbNodeListGet_0.listNodes; (* get no. of nodes entered in the result list *)
	
				IF (StatusNodeListGet = 0) THEN (* finished without errors ? *)
					IF NodesFoundConnected > 0 THEN (* at least one USB device found connected ? *)
						Step:= STEP_GET_NODE_DETAILS;
						NodeIndexInList:= 0; (* set index to begin of the result list *)
						CountDevicesFound:= 0; (* set count matching devices found to zero *)
					ELSE (* no device found, exit *)
						Status:= ERR_FIND_USB_DEV_NO_DEVICE_FOUND;
						DeviceInterfaceName:= '';
						DeviceFound:= FALSE; (* No device found *)
						CountDevicesFound:= 0; (* set count matching devices found to zero *)
						Done:= TRUE;
					END_IF;
				ELSIF (StatusNodeListGet <> 0) AND (StatusNodeListGet <> ERR_FUB_BUSY) THEN (* result <> 0 but not busy anymore ? => some Error *)
					DeviceInterfaceName:= '';
					DeviceFound:= FALSE; (* No device found *)
					CountDevicesFound:= 0; (* set count matching devices found to zero *)
					Error:= TRUE;
					ErrorStep:= STEP_GET_NODE_LIST; (* Error step info *)
					Status:= StatusNodeListGet;  (* Return error number of the prior FUB call *)
					Done:= TRUE;
				END_IF;
	
	
			(* ---------------------------------------------------------------------------
			   Search for specified USB device in the results list
		   	   ---------------------------------------------------------------------------*)
			STEP_GET_NODE_DETAILS:
				Status:= ERR_FUB_BUSY;
				UsbNodeGet_0(enable:= TRUE, nodeId:= NodeListBuffer[NodeIndexInList] , pBuffer:= ADR(UsbNodeBuffer), bufferSize:= SIZEOF(UsbNodeBuffer) );
				StatusNodeGet:= UsbNodeGet_0.status;
	
				IF (StatusNodeGet = 0) THEN (* finished without errors ? *)
					IF DeviceBcd <> 0 THEN (* DeviceBcd <> 0 => take into consideration on doing the check *)
						IF (DeviceProductID = UsbNodeBuffer.productId) AND (DeviceVendorID = UsbNodeBuffer.vendorId) AND (DeviceBcd = UsbNodeBuffer.bcdDevice) THEN
							DeviceInterfaceName:= UsbNodeBuffer.ifName; (* Device found -> get interface name it is connected to *)
							DeviceFound:= TRUE; (* Signal "Device found" to the caller *)
							CountDevicesFound:= CountDevicesFound + 1; (* increment number of matching devices found *)
							Status:= 0; (* Well done *)
						END_IF;
					ELSE (* DeviceBcd = 0 => use VendorId and ProductId only on doing the check *)
						IF (DeviceProductID = UsbNodeBuffer.productId) AND (DeviceVendorID = UsbNodeBuffer.vendorId) THEN
							DeviceInterfaceName:= UsbNodeBuffer.ifName; (* Device found -> get interface name it is connected to *)
							DeviceFound:= TRUE; (* Signal "Device found" to the caller *)
							CountDevicesFound:= CountDevicesFound + 1; (* increment number of matching devices found *)
							Status:= 0; (* Well done *)
						END_IF;
					END_IF;
	
					(* Set index to next entry if still not at the end of the result list *)
					IF (NodeIndexInList < NodesFoundConnected -1) THEN
						NodeIndexInList:= NodeIndexInList + 1;
					ELSE (* We are at the end. Check if a device was found - If not, set to Status "ERR_FIND_USB_DEV_SPCFD_NOT_FOUND" *)
						IF (DeviceFound = FALSE) THEN
							Status:= ERR_FIND_USB_DEV_SPCFD_NOT_FOUND;
							DeviceInterfaceName:= '';
						END_IF;
	
						Step:= STEP_IDLE; (* Set to defined idle/trap step *)
						Done:= TRUE; (* Job done *)
					END_IF;
	
				ELSIF (StatusNodeGet <> 0) AND (StatusNodeGet <> ERR_FUB_BUSY) THEN
					DeviceInterfaceName:= '';
					DeviceFound:= FALSE;
					Error:= TRUE;
					ErrorStep:= STEP_GET_NODE_DETAILS; (* Error step info *)
					Status:= StatusNodeGet; (* Return error number of the prior FUB call *)
					Step:= STEP_IDLE; (* Set to defined idle/trap step *)
					Done:= TRUE; (* Job done *)
				END_IF;
	
	
		END_CASE; (* CASE Step OF *)
	
	END_IF; (* IF EN THEN *)
	
END_FUNCTION_BLOCK
FUNCTION_BLOCK USBFindDevice1

	
	(* ===========================================================================
	   Set intial entry step of the case statement using an R_TRIG on the EN input
	   ===========================================================================*)
	
	R_TRIG_0(CLK:= EN);
	IF R_TRIG_0.Q = 1 THEN
		brsmemset(ADR(DeviceInterfaceName),0,SIZEOF(DeviceInterfaceName));(* Pre-init; Interface name not known yet *)
		DeviceFound:= FALSE; (* Pre-init; not found yet *)
		Error:= FALSE; (* Pre-init; no error yet *)
		ErrorStep:= 0; (* Pre-init; no error step info yet *)
		Done:= FALSE; (* We are going to process the job -> not done *)
		Step:= STEP_GET_NODE_LIST; (* Set first step to process *)
	END_IF;
	
	
	IF EN THEN (* FUB enabled ? *)
	
		(* ===========================================================================
		FBK Pre-Checks 
		===========================================================================*)
		IF (DeviceProductID = 0) OR (DeviceVendorID = 0) THEN (* According to the USB specification, Vendor ID and Product ID should be <> 0 *)
			Status:= ERR_FIND_USB_DEV_PARAMS_MISSED;
			brsmemset(ADR(DeviceInterfaceName),0,SIZEOF(DeviceInterfaceName));
			DeviceFound:= FALSE; (* Params missing,; not able to find a device *)
			Error:= TRUE;
			Done:= TRUE; (* Job done *)
			RETURN; (* Exit FUB *)
		END_IF;
	
		(* ===========================================================================
		   FBK Processing
		   ===========================================================================*)
		CASE Step OF
	
			(* ---------------------------------------------------------------------------
			   Kind of wait or trap step if something forces the case to have a defined state
		   	   ---------------------------------------------------------------------------*)
			STEP_IDLE:
				Step:= Step; (* Dummy statement *)
	
				(* ---------------------------------------------------------------------------
				USB Bus Enumeration; search for USB devices connected to the PLC's USB Bus
				---------------------------------------------------------------------------*)
			STEP_GET_NODE_LIST:
				Status:= ERR_FUB_BUSY;
				UsbNodeListGet_0(enable:= TRUE, pBuffer:= ADR(NodeListBuffer), bufferSize:= SIZEOF(NodeListBuffer), filterInterfaceClass:= 0, filterInterfaceSubClass:= 0);
				StatusNodeListGet:= UsbNodeListGet_0.status; (* get function call status *)
				NodesFoundConnected:= UsbNodeListGet_0.listNodes; (* get no. of nodes entered in the result list *)
	
				IF (StatusNodeListGet = 0) THEN (* finished without errors ? *)
					IF NodesFoundConnected > 0 THEN (* at least one USB device found connected ? *)
						Step:= STEP_GET_NODE_DETAILS;
						NodeIndexInList:= 0; (* set index to begin of the result list *)
						CountDevicesFound:= 0; (* set count matching devices found to zero *)
					ELSE (* no device found, exit *)
						Status:= ERR_FIND_USB_DEV_NO_DEVICE_FOUND;
						brsmemset(ADR(DeviceInterfaceName),0,SIZEOF(DeviceInterfaceName));
						DeviceFound:= FALSE; (* No device found *)
						CountDevicesFound:= 0; (* set count matching devices found to zero *)
						Done:= TRUE;
					END_IF;
				ELSIF (StatusNodeListGet <> 0) AND (StatusNodeListGet <> ERR_FUB_BUSY) THEN (* result <> 0 but not busy anymore ? => some Error *)
					brsmemset(ADR(DeviceInterfaceName),0,SIZEOF(DeviceInterfaceName));
					DeviceFound:= FALSE; (* No device found *)
					CountDevicesFound:= 0; (* set count matching devices found to zero *)
					Error:= TRUE;
					ErrorStep:= STEP_GET_NODE_LIST; (* Error step info *)
					Status:= StatusNodeListGet;  (* Return error number of the prior FUB call *)
					Done:= TRUE;
				END_IF;
	
	
			(* ---------------------------------------------------------------------------
			   Search for specified USB device in the results list
		   	   ---------------------------------------------------------------------------*)
			STEP_GET_NODE_DETAILS:
				Status:= ERR_FUB_BUSY;
				UsbNodeGet_0(enable:= TRUE, nodeId:= NodeListBuffer[NodeIndexInList] , pBuffer:= ADR(UsbNodeBuffer[NodeIndexInList]), bufferSize:= SIZEOF(UsbNodeBuffer[NodeIndexInList]) );
				StatusNodeGet:= UsbNodeGet_0.status;
	
				IF (StatusNodeGet = 0) THEN (* finished without errors ? *)
					IF DeviceBcd <> 0 THEN (* DeviceBcd <> 0 => take into consideration on doing the check *)
						IF (DeviceProductID = UsbNodeBuffer[NodeIndexInList].productId) AND (DeviceVendorID = UsbNodeBuffer[NodeIndexInList].vendorId) AND (DeviceBcd = UsbNodeBuffer[NodeIndexInList].bcdDevice) THEN
							DeviceInterfaceName[NodeIndexInList]:= UsbNodeBuffer[NodeIndexInList].ifName; (* Device found -> get interface name it is connected to *)
							DeviceFound:= TRUE; (* Signal "Device found" to the caller *)
							CountDevicesFound:= CountDevicesFound + 1; (* increment number of matching devices found *)
							Status:= 0; (* Well done *)
						END_IF;
					ELSE (* DeviceBcd = 0 => use VendorId and ProductId only on doing the check *)
						IF (DeviceProductID = UsbNodeBuffer[NodeIndexInList].productId) AND (DeviceVendorID = UsbNodeBuffer[NodeIndexInList].vendorId) THEN
							DeviceInterfaceName[NodeIndexInList]:= UsbNodeBuffer[NodeIndexInList].ifName; (* Device found -> get interface name it is connected to *)
							DeviceFound:= TRUE; (* Signal "Device found" to the caller *)
							CountDevicesFound:= CountDevicesFound + 1; (* increment number of matching devices found *)
							Status:= 0; (* Well done *)
						END_IF;
					END_IF;
	
					(* Set index to next entry if still not at the end of the result list *)
					IF (NodeIndexInList < NodesFoundConnected -1) THEN
						IF NodeIndexInList < MAX_DEV_IDX THEN
							NodeIndexInList:= NodeIndexInList + 1;
						ELSE
							NodeIndexInList:= MAX_DEV_IDX;
						END_IF

					ELSE (* We are at the end. Check if a device was found - If not, set to Status "ERR_FIND_USB_DEV_SPCFD_NOT_FOUND" *)
						IF (DeviceFound = FALSE) THEN
							Status:= ERR_FIND_USB_DEV_SPCFD_NOT_FOUND;
							brsmemset(ADR(DeviceInterfaceName),0,SIZEOF(DeviceInterfaceName));
						END_IF;
	
						Step:= STEP_IDLE; (* Set to defined idle/trap step *)
						Done:= TRUE; (* Job done *)
					END_IF;
	
				ELSIF (StatusNodeGet <> 0) AND (StatusNodeGet <> ERR_FUB_BUSY) THEN
					brsmemset(ADR(DeviceInterfaceName),0,SIZEOF(DeviceInterfaceName));
					DeviceFound:= FALSE;
					Error:= TRUE;
					ErrorStep:= STEP_GET_NODE_DETAILS; (* Error step info *)
					Status:= StatusNodeGet; (* Return error number of the prior FUB call *)
					Step:= STEP_IDLE; (* Set to defined idle/trap step *)
					Done:= TRUE; (* Job done *)
				END_IF;
	
	
		END_CASE; (* CASE Step OF *)
	
	END_IF; (* IF EN THEN *)
	
END_FUNCTION_BLOCK