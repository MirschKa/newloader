/******************************************************************************/
/**  \file 		tpwritem.c
	 \author 	krammerm, Copyright Bernecker + Rainer
	 \date 		January 15, 2009
	 \brief 	Implementation of function block TPWriteM().
*/
/******************************************************************************/

#include <bur/plctypes.h>
#include <string.h>

#include "tp15693.h"
#include "tp15693_internal.h"

#define RETURN_ON_ERROR(err, errnum)											\
{																				\
	if (err)																	\
	{																			\
		inst->ErrorID = errnum;													\
		inst->Error = 1;														\
		inst->Busy = 0;															\
		pInternal->Locked = FUB_FREE;											\
		pInternal->Step = STEP_IDLE;											\
		return;																	\
	}																			\
}
																				
#define RETURN_REGULAR(condition)												\
{																				\
	if (condition)																\
	{																			\
		inst->Done = 1;															\
		inst->Busy = 0;															\
		pInternal->Locked = FUB_FREE;											\
		pInternal->Step = STEP_IDLE;											\
		return;																	\
	}																			\
}

#define RESET_INTERNAL(condition)												\
{																				\
	if (condition)																\
	{																			\
		pInternal->Status = TP_STATUS_NO_CARDINRANGE;							\
		pInternal->RSSI = 0;													\
		pInternal->BlockSize = 0;												\
		pInternal->NumBlocks = 0;												\
		for (i = 0; i < UID_SIZE; i++) pInternal->UID[i] = 0;					\
	}																			\
}
	

/** \struct TPWriteM_typ
	\ingroup grp_TPWriteM
    Input and output parameters of function block TPWriteM().
*/

/**	\var TPWriteM_typ::Ident
	\copybrief TPInfo_typ::Ident
	\copydetails TPInfo_typ::Ident
*/

/**	\var TPWriteM_typ::Enable
	\copybrief TPInfo_typ::Enable
	\copydetails TPInfo_typ::Enable
*/

/**	\var TPWriteM_typ::Error
	\copybrief TPInfo_typ::Error
	\copydetails TPInfo_typ::Error
*/

/**	\var TPWriteM_typ::ErrorID
	\copybrief TPInfo_typ::ErrorID
	\copydetails TPInfo_typ::ErrorID
*/

/**	\var TPWriteM_typ::Done
	\copybrief TPInfo_typ::Done
	\copydetails TPInfo_typ::Done
*/

/**	\var TPWriteM_typ::Busy
	\copybrief TPInfo_typ::Busy
	\copydetails TPInfo_typ::Busy
*/

/**	\var TPWriteM_typ::pSource
	<B>[IN]</B> Memory from which the data to be written is copied.
*/

/**	\var TPWriteM_typ::FirstBlock
	<B>[IN]</B> First block to be written, the first block on chipcard is block number 0.
*/

/**	\var TPWriteM_typ::NumBlocks
	<B>[IN]</B> Number of blocks to be written in total.
*/

/**	\var TPWriteM_typ::Multiple
	<B>[IN]</B> Number of blocks to be written during one communication slot with chipcard [0 to 120 / BlockSize].
	The higher you set this value, the faster the function block will finish writing the memory.<BR>
	Note that the maximum number of blocks during one write multiple blocks command varies for different chipcard manufacturers.
	The transponder reader itself supports a maximum number of blocks during one write request of 120 / BlockSize, for example
	with BlockSize = 4 this would be 30.
*/


/** Write memory to transponder using write multiple command - NOT supported by most chipcards. This function block works much faster
	than TPWrite(), but access can only be on block-level and not on byte-level.
	\ingroup grp_TPWriteM
*/
void TPWriteM(struct TPWriteM* inst)
{
	InternalPara_type* pInternal;
	TRF7960_Frame_type TRFFrame;
	ISO15693_Frame_type ISOFrame;
	UINT i;

	pInternal = (InternalPara_type*)inst->Ident;
	if (pInternal == NULL )													{ inst->Error = 1; inst->ErrorID = tpERR_IDENT_NULL; 		return; }	
	if (pInternal->ChkSum != sizeof(InternalPara_type))						{ inst->Error = 1; inst->ErrorID = tpERR_IDENT_INVALID; 	return; }

	if (inst->Enable == 0)
	{
		inst->Busy = 0;
		inst->Done = 0;
		inst->Error = 0;
		inst->ErrorID = 0;
		pInternal->Locked = FUB_FREE;
		return;
	}
	else if (inst->Done == 1 || inst->Error == 1)	/* function block has finished, must be reset once with Enable = 0 ! */
	{
		return;	
	}
	
	if (pInternal->Locked != FUB_TPWRITEM && pInternal->Locked != FUB_FREE)	{ inst->Error = 1; inst->ErrorID = tpERR_SEMAPHORE_LOCKED; 	return; }
	if (pInternal->Status == TP_STATUS_NO_CARDINRANGE)						{ inst->Error = 1; inst->ErrorID = tpERR_NO_CARD_IN_RANGE;	return; }
	if (pInternal->NumBlocks == 0 || pInternal->BlockSize == 0)				{ inst->Error = 1; inst->ErrorID = tpERR_NO_BLOCKINFO;		return; }
	if (inst->FirstBlock + inst->NumBlocks > pInternal->NumBlocks)			{ inst->Error = 1; inst->ErrorID = tpERR_BLOCKS_EXCEEDED;	return; }
	if (inst->NumBlocks == 0)												{ inst->Done = 1; 											return; }
	
	if (pInternal->Step == STEP_IDLE)
	{
		pInternal->Locked = FUB_TPWRITEM;
		inst->Busy = 1;
		pInternal->Step = STEP_TPWRITEM_WRITE_REQ;
		pInternal->BlockCount = inst->FirstBlock;
		pInternal->BlockLast = inst->FirstBlock + inst->NumBlocks - 1;
		pInternal->SourceOffset = 0;
	}

	/* --- timeout monitoring --- */
	if (pInternal->Step != STEP_IDLE && pInternal->StepOld == pInternal->Step)
	{
		pInternal->cntTime += pInternal->CycleTime;
		RETURN_ON_ERROR((pInternal->cntTime > pInternal->Timeout), tpERR_TIMEOUT);
	}
	else
	{
		pInternal->cntTime = 0;
		pInternal->StepOld = pInternal->Step;
	}
	
	switch (pInternal->Step)
	{
		case STEP_TPWRITEM_WRITE_RESP:
			/* --- read incoming frame --- */
			FRM_read(&pInternal->FRM_read_0);
			if (pInternal->FRM_read_0.status == frmERR_NOINPUT)
				break;	/* try again next cycle */
			RETURN_ON_ERROR((pInternal->FRM_read_0.status != 0), pInternal->FRM_read_0.status);
			RETURN_ON_ERROR((pInternal->FRM_read_0.buflng > sizeof(pInternal->FrameIn)), tpERR_BUFFERLEN);
			memcpy(pInternal->FrameIn, (void*)pInternal->FRM_read_0.buffer, pInternal->FRM_read_0.buflng);
			memset(pInternal->FrameIn + pInternal->FRM_read_0.buflng, 0, sizeof(pInternal->FrameIn) - pInternal->FRM_read_0.buflng);
			
			/* --- release input buffer --- */
			pInternal->FRM_rbuf_0.buffer = pInternal->FRM_read_0.buffer;
			pInternal->FRM_rbuf_0.buflng = pInternal->FRM_read_0.buflng;
			FRM_rbuf(&pInternal->FRM_rbuf_0);
			RETURN_ON_ERROR((pInternal->FRM_rbuf_0.status != 0), pInternal->FRM_rbuf_0.status);

			/* --- evaluate response --- */
			RESET_INTERNAL(RegExpMatch((STRING*)pInternal->FrameIn, "[]\r\n") == 1);			/* no card in range */
			RESET_INTERNAL(RegExpMatch((STRING*)pInternal->FrameIn, ".*&{00&}.*\r\n") == 1);	/* '{00}'	data transmission conflict, bad card position */
			RESET_INTERNAL(RegExpMatch((STRING*)pInternal->FrameIn, ".*z.*\r\n") == 1);			/* 'z'		data transmission conflict, bad card position */
			RETURN_ON_ERROR(pInternal->Status == TP_STATUS_NO_CARDINRANGE, tpERR_CARD_REMOVED);

			RETURN_ON_ERROR((RegExpMatch((STRING*)pInternal->FrameIn, "[01##]\r\n") == 1), tpERR_ISOERR_WRITEM_RESP + Hex2Byte(&pInternal->FrameIn[3]));
			RETURN_ON_ERROR((RegExpMatch((STRING*)pInternal->FrameIn, "[00]\r\n") == 0), tpERR_INVALID_WRITEM_RESP);

			pInternal->SourceOffset += pInternal->BlockSize*pInternal->BlockMultiple;
			pInternal->BlockCount += pInternal->BlockMultiple;
			
			RETURN_REGULAR(pInternal->BlockCount > pInternal->BlockLast);
			pInternal->Step = STEP_TPWRITEM_WRITE_REQ;
		/* break; */	/* no break so that next read request is immediately sent */
		
		case STEP_TPWRITEM_WRITE_REQ:
			/* --- compose the frame to be sent --- */
			ISOFrame.Flags = ISO_FLAG_DATA_RATE_HIGH | ISO_FLAG_ADDRESS | ISO_FLAG_OPTION;	/* most cards require option flag! */
			ISOFrame.Cmd = ISO_CMD_WRITE_MULTIPLE_BLOCKS;
			for (i = 0; i < UID_SIZE; i++)
				ISOFrame.Parameters[i] = pInternal->UID[i];		/* UID for addressed mode. */
			ISOFrame.Parameters[i] = pInternal->BlockCount;
			if (inst->NumBlocks - (pInternal->BlockCount - inst->FirstBlock) < inst->Multiple)
				pInternal->BlockMultiple = inst->NumBlocks - (pInternal->BlockCount - inst->FirstBlock);
			else
				pInternal->BlockMultiple = inst->Multiple;
			/* ISO15693: The number of blocks is one less than the num of blocks that the chipcard shall return in its response: */
			ISOFrame.Parameters[i+1] = pInternal->BlockMultiple - 1;
			memcpy(&ISOFrame.Parameters[i+2], &((USINT*)inst->pSource)[pInternal->SourceOffset], pInternal->BlockSize*pInternal->BlockMultiple);

			GetTRFFromISO(&TRFFrame, &ISOFrame, UID_SIZE + 2 + pInternal->BlockSize*pInternal->BlockMultiple);
			GetTRFRaw(pInternal->FrameOut, &TRFFrame);

			/* --- send the frame --- */
			pInternal->FRM_write_0.buflng = TRFFrame.PacketLength*2;
			FRM_write(&pInternal->FRM_write_0);
			RETURN_ON_ERROR((pInternal->FRM_write_0.status != 0), pInternal->FRM_write_0.status);

			pInternal->Step = STEP_TPWRITEM_WRITE_RESP;
		break;
	
		default:
		break;
	}
}
