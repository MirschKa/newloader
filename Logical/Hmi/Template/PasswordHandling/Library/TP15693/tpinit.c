/******************************************************************************/
/**  \file 		tpinit.c
	 \author 	krammerm, Copyright Bernecker + Rainer
	 \date 		November 18, 2008
	 \brief 	Implementation of function block TPInit().
*/
/******************************************************************************/
/* Version 3.04.1  16-apr-2015  SO                              Development tool: B&R Automation Studio V3.0.90.28 SP10 *)
(* [#80] RFID reader sporadic stops working																				*)
(* 		- If the reader will be newly initialized, now first the port will be closed and no new memory will be allocated*)
(************************************************************************************************************************/
#include <bur/plctypes.h>
#include <sys_lib.h>
#include <string.h>

#include "tp15693.h"
#include "tp15693_internal.h"

/** \struct TPInit_typ
	\ingroup grp_TPInit
    Input and output parameters of function block TPInit().
*/

/**	\var TPInit_typ::Ident
	<B>[OUT]</B> Ident of the transponder used.
	The Ident for a transponder is retrieved from this function block and needs to be passed on to all other function blocks of this library.
*/

/**	\var TPInit_typ::Error
	\copybrief TPInfo_typ::Error
	\copydetails TPInfo_typ::Error
*/

/**	\var TPInit_typ::ErrorID
	\copybrief TPInfo_typ::ErrorID
	\copydetails TPInfo_typ::ErrorID
*/

/** \var TPInit_typ::pDevice
	<B>[IN]</B> Pointer to the device string of the serial interface of the transponder reader, such as "IF3".
*/

/** \var TPInit_typ::CycleTime
	<B>[IN]</B> Cycle time of the task resource where the function blocks of this library are called in [us].
*/

/** \var TPInit_typ::Timeout
	<B>[IN]</B> Timeout when waiting for a response from transponder reader [us].
*/	

/** Initialization procedure.
    Allocate memory, initialize the serial interface for communication with the transponder reader.
	\attention This function  block should only be called in the INIT SP of a PLC task. 
	\ingroup grp_TPInit
*/
void TPInit(struct TPInit* inst)
{
	XOPENCONFIG xopen_config;
	InternalPara_type* pInternal;
	
	inst->Error = 0;
	inst->ErrorID = 0;

	/* --- allocate memory for internal parameters, if not yet done --- */
	if (inst->Ident == 0){
		inst->ErrorID = TMP_alloc(sizeof(InternalPara_type), (void**)&inst->Ident);
		if (inst->ErrorID != 0)	{ inst->Error = 1; return; }
		memset((void**)inst->Ident,0, sizeof(InternalPara_type));
	}
	pInternal = (InternalPara_type*)inst->Ident;
	pInternal->ChkSum = sizeof(InternalPara_type);

	/* --- close frame if open --- */
	if (pInternal->DVIdent != 0){
		pInternal->FRM_close_0.enable = 1;
		pInternal->FRM_close_0.ident = pInternal->DVIdent;
		FRM_close(&pInternal->FRM_close_0);
		inst->ErrorID = pInternal->FRM_close_0.status;
		if (inst->ErrorID != 0)	{ inst->Error = 1; return; }
	}	
	/* --- open serial interface for DVFrame communication --- */
    xopen_config.idle = 65000;
    xopen_config.delimc = 1;
    xopen_config.delim[0] = '\n';
    xopen_config.delim[1] = 0;
    xopen_config.tx_cnt = 3;
    xopen_config.rx_cnt = 3;
    xopen_config.tx_len = 256;
    xopen_config.rx_len = 256;
    xopen_config.argc = 0;
    xopen_config.argv = 0;
	pInternal->FRM_xopen_0.config = (UDINT)&xopen_config;
	pInternal->FRM_xopen_0.enable = 1;
	pInternal->FRM_xopen_0.device = inst->pDevice;
	pInternal->FRM_xopen_0.mode = (UDINT)"/BD=115200 /PA=N /DB=8 /SB=1 /PHY=RS232";
	FRM_xopen(&pInternal->FRM_xopen_0);
	inst->ErrorID = pInternal->FRM_xopen_0.status;
	if (inst->ErrorID != 0)	{ inst->Error = 1; return; }
	
	/* --- initialization succesful, preset DVFrame function blocks --- */
	pInternal->DVIdent = pInternal->FRM_xopen_0.ident;
	pInternal->FRM_write_0.enable = 1;
	pInternal->FRM_write_0.ident = pInternal->DVIdent;
	pInternal->FRM_write_0.buffer = (UDINT)pInternal->FrameOut;
	pInternal->FRM_read_0.enable = 1;
	pInternal->FRM_read_0.ident = pInternal->DVIdent;
	pInternal->FRM_rbuf_0.enable = 1;
	pInternal->FRM_rbuf_0.ident = pInternal->DVIdent;

	/* --- other initialiuations --- */
	pInternal->FirstRun = 1;
	pInternal->CycleTime = inst->CycleTime;
	pInternal->Timeout = inst->Timeout;
}

