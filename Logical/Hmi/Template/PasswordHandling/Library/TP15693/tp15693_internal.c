/******************************************************************************/
/**  \file 		tp15693_internal.c
	 \author 	krammerm, Copyright Bernecker + Rainer
	 \date 		November 18, 2008
	 \brief 	Implementation of internal helper functions for the TP15693 Library.
*/
/******************************************************************************/

#include <bur/plctypes.h>
#include <string.h>
#include "tp15693_internal.h"

UDINT myatoi(STRING* pStr)
{
	UDINT retval = 0;
	
	while (*pStr >= '0' && *pStr <= '9')
	{
		retval = retval*10 + (*pStr - '0');
		pStr++;
	}
	
	return retval;
}

void strreverse(STRING* pBeg, STRING* pEnd)
{
	SINT aux;
	
	while(pEnd > pBeg)
		aux=*pEnd, *pEnd--=*pBeg, *pBeg++=aux;
}

void myitoa(INT value, STRING* pStr)
{
	static SINT num[] = "0123456789";
	STRING* pWStr=pStr;
	INT sign;

	if ((sign=value) < 0) value = -value;

	do *pWStr++ = num[value%10]; while(value/=10);
	if(sign<0) *pWStr++='-';
	*pWStr='\0';

	strreverse(pStr,pWStr-1);
}

BOOL RegExpMatch(STRING* pStr, STRING* pRegExp)
{
	BOOL 	match 				= 1;	/* match is 1 as lon as pStr matches pRegExp and set to 0 when a mismatch is found. */
	BOOL 	endloop				= 0;	/* loop in wildcard search should be ended */
	USINT	offs				= 0;	/* needed in wildcard search */
	UINT	s = 0;						/* index for pStr parsing */
	UINT 	r = 0;						/* index for pRegExp parsing */
	UINT	i;							/* index for various purposes */
	UDINT	rep_min;					/* Minimum evaluated from repetition clause. */
	UDINT	rep_max;					/* Maximum evaluated from repetition clause. */
	SINT	repchar;					/* Char to be repeated according in a repetition clause. */ 
	
	/* match is 1 as long as pStr matches pRegExp, 
	   It is set to 0 when the first mismatch is detected and serves as end condition for the main while() loop */

	/* || pRegExp[r] == CH_WILDCARD is necessary when wildcard clause is at the end.
	   Example: pRegExp="ac*" and pStr="ac", s points to '\0' and r points to '*' */
	while (pRegExp[r] != '\0' && match == 1 && (pStr[s] != '\0' || pRegExp[r] == CH_WILDCARD))
	{
		switch (pRegExp[r])
		{
			case CH_ANY:
				if (pRegExp[r+1] == CH_WILDCARD)	match = 1;
				else								match = MATCH_ANY(pStr[s]); 
			break;
			
			case CH_HEX:
				/* If the next character is a wildcard '*', then do not check the matching, because wildcard stands also for 0 times. */
				/* example: pStr="xz" pRegExp="x#*z" -> if wildcard as next char would not ne considered, then z != # would return a mismatch. */
				if (pRegExp[r+1] == CH_WILDCARD)	match = 1;
				else								match = MATCH_HEX(pStr[s]);
			break;
			
			case CH_DIG:
				if (pRegExp[r+1] == CH_WILDCARD)	match = 1;
				else								match = MATCH_DIG(pStr[s]);
			break;
			
			case CH_REP_BEG:
				if (r == 0)	{ match = 0; break; }	/* repetition begin is invalid as first char of regexp */

				/* evaluate repetition clause in regular expression */
				i = 0;
				repchar = pRegExp[r-1];
				if (repchar == CH_WILDCARD)	{ match = 0; break; } /* wildcard is invalid character in repetition clause */
				
				r++;	/* r now points to first char in repetition clause x {xx-yy} */
				/* evaluate the first number (minimum) in the repetition clause xx {xx-yy} */
				while (pRegExp[r+i] != CH_REP_TO && pRegExp[r+i] != CH_REP_END && match == 1)
				{
					if (!MATCH_DIG(pRegExp[r+i]))	match = 0;
					else							i++;
				}
				if (match == 0)	break;	/* invalid character found in repetition clause */
				rep_max = rep_min = myatoi(&pRegExp[r]);	/* min and max are equal of there is only one number in repetition clause */
				/* evaluate the second number (maximum) in the repetition clause yy {xx-yy} if there is any */
				if (pRegExp[r+i] == CH_REP_TO)
				{
					r = r + i + 1;	/* r now points to first char of second number in repetition clause y {xx-yy} */
					i = 0;
					while (pRegExp[r+i] != CH_REP_END && match == 1)
					{
						if (!MATCH_DIG(pRegExp[r+i]))	match = 0;
						else							i++;
					}
					if (match == 0) break;	/* invalid character in repetition clause */
					rep_max = myatoi(&pRegExp[r]);
				}
				r = r + i;	/* r now points to first char after repetition clause z {xx-yy}z */
				
				/* invalid values of min and max in repetition clause */
				if (rep_min < 2 || rep_min > rep_max || rep_max > 32767) { match = 0;	break;  }
				
				/* check the corresponding number of chars in string if they are equal to the repchar */
				for (i = 1; i < rep_max; i++)
				{
					if (!MATCH(pStr[s], repchar))	
					{ 
						if (i < rep_min)	match = 0; 
						break;
					}
					s++;
				}
				if (match == 1)	
					s--;	/* undo the last increase of s, because s is increased at the end of the main while statement */			
				
			break;
			
			case CH_WILDCARD:
				/* r points now to the wildcard '*' */
				if (r == 0)	{ match = 0; break; }	/* CH_WILDCARD s invalid as first char of regexp */
				
				s--;	/* s points now to the previous char, example: pStr="abbbc" and pRegExp="ab*c" , s points now to the first 'b' in pStr */
				endloop = 0;
				if (pRegExp[r+1] == CH_SPECIAL)		offs = 2;	/* if char after wildcard is CH_SPECIAL, then the overnext char must be checked */
				else								offs = 1;
				/* example: pRegExp="ab*&.", pStr="abbbb." 
				   pRegExp[r-1] is 		'b'		the character that is repeated 0-n times
				   pRegExp[r+offs] is 	'.'		the next char after the wildchard repetition */	
				while (endloop == 0)
				{
					/* a match of char after the wildcard '*' in regexp is detected -> loop ends with match  */
					if (MATCH(pStr[s], pRegExp[r+offs])) 	{ match = 1; endloop = 1; }
					/* a different char from the one that is repeated is detected -> loop ends without match */
					else if (!MATCH(pStr[s], pRegExp[r-1]))	{ match = 0; endloop = 1; }
 					s++;
				}
				r = r + offs;	/* r now points to the next char after wildcard repetition, example: pRegExp="ab*c"  r points to 'c' */
				if (match == 1)
					s--;	/* undo the last increase of s, because s is increased at the end of the main while statement */				
			break;
						
			case CH_SPECIAL:
				r++;							/* handle next character as normal char ('break' is commented! default step is executed immediately) */
				if (pRegExp[r] == '\0')	break;	/* but break (and consequently return match=0) if special char is the last char in string */
			/* break; */
			default:
				/* if the next character is a wildcard '*', then do not check the matching, because wildcard stands also for 0 times */
				/* example: pStr="ac" pRegExp="ab*c" -> if wildcard as next char would not ne considered, then c != b would return a mismatch */
				if (pRegExp[r+1] == CH_WILDCARD)	match = 1;
				else								match = (pStr[s] == pRegExp[r]);
			break;	
		}

		if (pStr[s] != '\0')	s++;	/* go to next character in string */
		if (pRegExp[r] != '\0')	r++;	/* go to next character in regexp */
	}
	
	/* string is finished, but one character is still present with wildcard in regexp */
	/* example: pRegExp="ab*" and pStr="a" */
	if (pStr[s] == '\0' && pRegExp[r] != '\0' && pRegExp[r+1] == CH_WILDCARD && pRegExp[r+2] == '\0')
	{
		r += 2;	/* go to end of regexp and skip wildcard check */
	}
	
	return (match == 1 && pStr[s] == '\0' && pRegExp[r] == '\0');
}


unsigned char	hexValues[16] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

USINT* AddHex(USINT* pDest, USINT val)
{
	*pDest = hexValues[(val & 0xF0) >> 4];
	pDest++;
	*pDest = hexValues[val & 0x0F];
	
	return pDest + 1;
}

USINT Hex2Byte(USINT* pHex)
{
	USINT retval;
	
	if (pHex[0] >= '0' && pHex[0] <= '9')		retval = pHex[0] - '0';
	else if (pHex[0] >= 'a' && pHex[0] <= 'f')	retval = pHex[0] - 'a' + 10;
	else if (pHex[0] >= 'A' && pHex[0] <= 'F')	retval = pHex[0] - 'A' + 10;
	else										retval = 0;
	
	retval = retval << 4;
	
	if (pHex[1] >= '0' && pHex[1] <= '9')		retval |= pHex[1] - '0';
	else if (pHex[1] >= 'a' && pHex[1] <= 'f')	retval |= pHex[1] - 'a' + 10;
	else if (pHex[1] >= 'A' && pHex[1] <= 'F')	retval |= pHex[1] - 'A' + 10;
	else										retval |= 0;
	
	return retval;	
}

void InitTRFFrame(TRF7960_Frame_type* pFrame)
{
	pFrame->constSOF = 					TRF7960_FRAME_CONST_SOF;
	pFrame->constZero = 				TRF7960_FRAME_CONST_ZERO;
	pFrame->constBeginDataPayload1 = 	TRF7960_FRAME_CONST_BEGIN_DATA_PAYLOAD1;
	pFrame->constBeginDataPayload2 = 	TRF7960_FRAME_CONST_BEGIN_DATA_PAYLOAD2;
	pFrame->constEOF1 = 				TRF7960_FRAME_CONST_EOF1;
	pFrame->constEOF2 = 				TRF7960_FRAME_CONST_EOF2;
}

void GetTRFFromISO(TRF7960_Frame_type* pTRF, ISO15693_Frame_type* pISO, USINT LengthPara)
{
	USINT i;

	InitTRFFrame(pTRF);
	
	if (pISO->Cmd == ISO_CMD_INVENTORY)		pTRF->Cmd = TRF7960_CMD_INVENTORY_ISO;
	else									pTRF->Cmd = TRF7960_CMD_REQUEST_ISO;
	
	pTRF->Parameters[0] = pISO->Flags;
	pTRF->Parameters[1] = pISO->Cmd;
	
	for (i = 0; i < LengthPara; i++)
		pTRF->Parameters[2+i] = pISO->Parameters[i];
	
	pTRF->PacketLength = LengthPara + ISO_FRAME_CONST/2 + TRF7960_FRAME_CONST/2;
}

void GetTRFRaw(USINT* pDest, TRF7960_Frame_type* pSrcFrame)
{
	USINT i;
	USINT* pTmp;
	
	/* fill up frame with values from structure */
	pTmp = AddHex(pDest, pSrcFrame->constSOF);
	pTmp = AddHex(pTmp, pSrcFrame->PacketLength);
	pTmp = AddHex(pTmp, pSrcFrame->constZero);
	pTmp = AddHex(pTmp, pSrcFrame->constBeginDataPayload1);
	pTmp = AddHex(pTmp, pSrcFrame->constBeginDataPayload2);
	pTmp = AddHex(pTmp, pSrcFrame->Cmd);

	for (i = 0; i < pSrcFrame->PacketLength - TRF7960_FRAME_CONST/2; i++)
		pTmp = AddHex(pTmp, pSrcFrame->Parameters[i]);
	
	pTmp = AddHex(pTmp, pSrcFrame->constEOF1);
	pTmp = AddHex(pTmp, pSrcFrame->constEOF2);
	
	/* set rest of frame to 0 */
	memset(pTmp, 0, TRF7960_FRAME_LEN/2 - pSrcFrame->PacketLength);	
}



