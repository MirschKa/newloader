/******************************************************************************/
/**  \file 		tpdiag.c
	 \author 	krammerm, Copyright Bernecker + Rainer
	 \date 		January 26, 2008
	 \brief 	Implementation of function block TPDiag().
*/
/******************************************************************************/

#include <bur/plctypes.h>
#include <string.h>

#include "tp15693.h"
#include "tp15693_internal.h"

/** \struct TPDiag_typ
	\ingroup grp_TPDiag
    Input and output parameters of function block TPDiag().
*/

/**	\var TPDiag_typ::Enable
	<B>[IN]</B> Diagnosis information is provided if Enable==TRUE.
*/

/**	\var TPDiag_typ::Ident
	\copybrief TPInfo_typ::Ident
	\copydetails TPInfo_typ::Ident
*/

/**	\var TPDiag_typ::Error
	\copybrief TPInfo_typ::Error
	\copydetails TPInfo_typ::Error
*/

/**	\var TPDiag_typ::ErrorID
	\copybrief TPInfo_typ::ErrorID
	\copydetails TPInfo_typ::ErrorID
*/
	
/**	\var TPDiag_typ::DiagInfo
	<B>[OUT]</B> Diagnosis information that can help to find internal errors/bugs, especially in the serial communication to
	the transponder reader.
*/

/** \struct DiagInfo_typ
	\ingroup grp_TPDiag
    Diagnosis information that can help to find internal errors/bugs.
*/

/** Get diagnosis information.
    Use this function block to retrieve internal diagnosis information that can help the developper to find internal errors
	or bugs.
	\ingroup grp_TPDiag
*/
void TPDiag(struct TPDiag* inst)
{
	InternalPara_type* pInternal;

	pInternal = (InternalPara_type*)inst->Ident;
	if (pInternal == NULL )									{ inst->Error = 1; inst->ErrorID = tpERR_IDENT_NULL; 		return; }	
	if (pInternal->ChkSum != sizeof(InternalPara_type))		{ inst->Error = 1; inst->ErrorID = tpERR_IDENT_INVALID; 	return; }

	if (inst->Enable == 1)
	{
		inst->DiagInfo.ChkSum = 			pInternal->ChkSum;
		inst->DiagInfo.Status = 			pInternal->Status;
		inst->DiagInfo.RSSI = 				pInternal-> RSSI;
		inst->DiagInfo.CycleTime = 			pInternal->CycleTime;
		inst->DiagInfo.Timeout = 			pInternal->Timeout;
		inst->DiagInfo.cntTime = 			pInternal->cntTime;
		inst->DiagInfo.NumBlocks = 			pInternal->NumBlocks;
		inst->DiagInfo.BlockSize = 			pInternal->BlockSize;
		inst->DiagInfo.DVIdent = 			pInternal->DVIdent;
		inst->DiagInfo.Step = 				pInternal->Step;
		inst->DiagInfo.StepOld = 			pInternal->StepOld;
		inst->DiagInfo.Locked = 			pInternal->Locked;
		inst->DiagInfo.FirstRun = 			pInternal->FirstRun;
		inst->DiagInfo.BlockCount = 		pInternal->BlockCount;
		inst->DiagInfo.BlockLast = 			pInternal->BlockLast;
		inst->DiagInfo.BlockIncomplBeg = 	pInternal->BlockIncomplBeg;
		inst->DiagInfo.BlockIncomplEnd = 	pInternal->BlockIncomplEnd;
		inst->DiagInfo.DestOffset = 		pInternal->DestOffset;
		inst->DiagInfo.SourceOffset = 		pInternal->SourceOffset;
		inst->DiagInfo.BlockMultiple = 		pInternal->BlockMultiple;
		inst->DiagInfo.DebugInfo = 			pInternal->DebugInfo;
		memcpy(inst->DiagInfo.UID, 			pInternal->UID, 		sizeof(inst->DiagInfo.UID));
		memcpy(inst->DiagInfo.FirstBlock, 	pInternal->FirstBlock, 	sizeof(inst->DiagInfo.FirstBlock));
		memcpy(inst->DiagInfo.LastBlock, 	pInternal->LastBlock, 	sizeof(inst->DiagInfo.LastBlock));
		memcpy(inst->DiagInfo.FrameOut, 	pInternal->FrameOut, 	sizeof(inst->DiagInfo.FrameOut));
		memcpy(inst->DiagInfo.FrameIn, 		pInternal->FrameIn, 	sizeof(inst->DiagInfo.FrameIn));
		memcpy(&inst->DiagInfo.FRM_rbuf_0, 	&pInternal->FRM_rbuf_0, sizeof(inst->DiagInfo.FRM_rbuf_0));
		memcpy(&inst->DiagInfo.FRM_xopen_0, &pInternal->FRM_xopen_0, sizeof(inst->DiagInfo.FRM_xopen_0));
		memcpy(&inst->DiagInfo.FRM_write_0, &pInternal->FRM_write_0, sizeof(inst->DiagInfo.FRM_write_0));
		memcpy(&inst->DiagInfo.FRM_read_0, 	&pInternal->FRM_read_0, sizeof(inst->DiagInfo.FRM_read_0));
	}
	else
	{
		inst->DiagInfo.ChkSum = 			0;
		inst->DiagInfo.Status = 			0;
		inst->DiagInfo.RSSI = 				0;
		inst->DiagInfo.CycleTime = 			0;
		inst->DiagInfo.Timeout = 			0;
		inst->DiagInfo.cntTime = 			0;
		inst->DiagInfo.NumBlocks = 			0;
		inst->DiagInfo.BlockSize = 			0;
		inst->DiagInfo.DVIdent = 			0;
		inst->DiagInfo.Step = 				0;
		inst->DiagInfo.StepOld = 			0;
		inst->DiagInfo.Locked = 			0;
		inst->DiagInfo.FirstRun = 			0;
		inst->DiagInfo.BlockCount = 		0;
		inst->DiagInfo.BlockLast = 			0;
		inst->DiagInfo.BlockIncomplBeg = 	0;
		inst->DiagInfo.BlockIncomplEnd = 	0;
		inst->DiagInfo.DestOffset = 		0;
		inst->DiagInfo.SourceOffset = 		0;
		inst->DiagInfo.BlockMultiple = 		0;
		inst->DiagInfo.DebugInfo = 			0;
		memset(inst->DiagInfo.UID, 			0, 	sizeof(inst->DiagInfo.UID));
		memset(inst->DiagInfo.FirstBlock, 	0, 	sizeof(inst->DiagInfo.FirstBlock));
		memset(inst->DiagInfo.LastBlock, 	0, 	sizeof(inst->DiagInfo.LastBlock));
		memset(inst->DiagInfo.FrameOut, 	0, 	sizeof(inst->DiagInfo.FrameOut));
		memset(inst->DiagInfo.FrameIn, 		0, 	sizeof(inst->DiagInfo.FrameIn));
		memset(&inst->DiagInfo.FRM_rbuf_0, 	0, 	sizeof(inst->DiagInfo.FRM_rbuf_0));
		memset(&inst->DiagInfo.FRM_xopen_0, 0, 	sizeof(inst->DiagInfo.FRM_xopen_0));
		memset(&inst->DiagInfo.FRM_write_0, 0, 	sizeof(inst->DiagInfo.FRM_write_0));
		memset(&inst->DiagInfo.FRM_read_0, 	0, 	sizeof(inst->DiagInfo.FRM_read_0));
	}
}

