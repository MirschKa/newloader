/******************************************************************************/
/**  \file 		tpread.c
	 \author 	krammerm, Copyright Bernecker + Rainer
	 \date 		November 18, 2008
	 \brief 	Implementation of function block TPRead().
*/
/******************************************************************************/

#include <bur/plctypes.h>
#include <string.h>

#include "tp15693.h"
#include "tp15693_internal.h"

#define RETURN_ON_ERROR(err, errnum)											\
{																				\
	if (err)																	\
	{																			\
		inst->ErrorID = errnum;													\
		inst->Error = 1;														\
		inst->Busy = 0;															\
		pInternal->Locked = FUB_FREE;											\
		pInternal->Step = STEP_IDLE;											\
		return;																	\
	}																			\
}
																				
#define RETURN_REGULAR(condition)												\
{																				\
	if (condition)																\
	{																			\
		inst->Done = 1;															\
		inst->Busy = 0;															\
		pInternal->Locked = FUB_FREE;											\
		pInternal->Step = STEP_IDLE;											\
		return;																	\
	}																			\
}

#define RESET_INTERNAL(condition)												\
{																				\
	if (condition)																\
	{																			\
		pInternal->Status = TP_STATUS_NO_CARDINRANGE;							\
		pInternal->RSSI = 0;													\
		pInternal->BlockSize = 0;												\
		pInternal->NumBlocks = 0;												\
		for (i = 0; i < UID_SIZE; i++) pInternal->UID[i] = 0;					\
	}																			\
}

/** \struct TPRead_typ
	\ingroup grp_TPRead
    Input and output parameters of function block TPRead().
*/

/**	\var TPRead_typ::Ident
	\copybrief TPInfo_typ::Ident
	\copydetails TPInfo_typ::Ident
*/

/**	\var TPRead_typ::Enable
	\copybrief TPInfo_typ::Enable
	\copydetails TPInfo_typ::Enable
*/

/**	\var TPRead_typ::Error
	\copybrief TPInfo_typ::Error
	\copydetails TPInfo_typ::Error
*/

/**	\var TPRead_typ::ErrorID
	\copybrief TPInfo_typ::ErrorID
	\copydetails TPInfo_typ::ErrorID
*/

/**	\var TPRead_typ::Done
	\copybrief TPInfo_typ::Done
	\copydetails TPInfo_typ::Done
*/

/**	\var TPRead_typ::Busy
	\copybrief TPInfo_typ::Busy
	\copydetails TPInfo_typ::Busy
*/

/**	\var TPRead_typ::pDestination
	<B>[IN]</B> Memory to which the data to be read is copied.
*/

/**	\var TPRead_typ::Offset
	<B>[IN]</B> Offset in the memory of the chipcard [bytes].
*/

/**	\var TPRead_typ::Len
	<B>[IN]</B> Length of the data to be read [bytes].
*/

/** Read memory of transponder.
	The physical memory on a chipcard is organized in blocks, however using this function block memory can be accessed byte-by-byte.
	\ingroup grp_TPRead
*/
void TPRead(struct TPRead* inst)
{
	InternalPara_type* pInternal;
	TRF7960_Frame_type TRFFrame;
	ISO15693_Frame_type ISOFrame;
	UINT i;
	STRING	strRegExp[100];
	STRING	strTmp[100];
	USINT	offs, max;

	pInternal = (InternalPara_type*)inst->Ident;
	if (pInternal == NULL )													{ inst->Error = 1; inst->ErrorID = tpERR_IDENT_NULL; 		return; }	
	if (pInternal->ChkSum != sizeof(InternalPara_type))						{ inst->Error = 1; inst->ErrorID = tpERR_IDENT_INVALID; 	return; }

	if (inst->Enable == 0)
	{
		inst->Busy = 0;
		inst->Done = 0;
		inst->Error = 0;
		inst->ErrorID = 0;
		pInternal->Locked = FUB_FREE;
		return;
	}
	else if (inst->Done == 1 || inst->Error == 1)	/* function block has finished, must be reset once with Enable = 0 ! */
	{
		return;	
	}
	
	if (pInternal->Locked != FUB_TPREAD && pInternal->Locked != FUB_FREE)	{ inst->Error = 1; inst->ErrorID = tpERR_SEMAPHORE_LOCKED; 	return; }
	if (pInternal->Status == TP_STATUS_NO_CARDINRANGE)						{ inst->Error = 1; inst->ErrorID = tpERR_NO_CARD_IN_RANGE;	return; }
	if (pInternal->NumBlocks == 0 || pInternal->BlockSize == 0)				{ inst->Error = 1; inst->ErrorID = tpERR_NO_BLOCKINFO;		return; }
	if (inst->Offset+inst->Len > pInternal->NumBlocks*pInternal->BlockSize)	{ inst->Error = 1; inst->ErrorID = tpERR_MEM_EXCEEDED;		return; }
	if (inst->Len == 0)														{ inst->Done = 1; 											return; }
	
	if (pInternal->Step == STEP_IDLE)
	{
		pInternal->Locked = FUB_TPREAD;
		inst->Busy = 1;
		pInternal->Step = STEP_TPREAD_READ_REQ;
		pInternal->BlockCount = inst->Offset / pInternal->BlockSize;
		pInternal->DestOffset = 0;
		if (inst->Offset % pInternal->BlockSize != 0)	/* 1 incomplete block at the beginning */
		{
			pInternal->BlockIncomplBeg = 1;
			/* 1 incomplete block at beginning and a complete block at the end */
			if ((inst->Len + (inst->Offset % pInternal->BlockSize)) % pInternal->BlockSize == 0)
			{
				pInternal->BlockLast = pInternal->BlockCount + inst->Len / pInternal->BlockSize + 0;		
				pInternal->BlockIncomplEnd = 0;
			}
			/* 1 incomplete block at the beginning and no other block */
			else if (inst->Len + (inst->Offset % pInternal->BlockSize) < pInternal->BlockSize)					
			{
				pInternal->BlockLast = pInternal->BlockCount;												
				pInternal->BlockIncomplEnd = 1;
			}
			/* 1 incomplete block at beginning and 1 incomplete block at end */
			else 
			{
				pInternal->BlockLast = pInternal->BlockCount + (inst->Offset % pInternal->BlockSize + inst->Len) / pInternal->BlockSize;		
				pInternal->BlockIncomplEnd = 1;
			}
				
		}
		else	/* no incomplete block at the beginning */
		{
			pInternal->BlockIncomplBeg = 0;
			if (inst->Len % pInternal->BlockSize == 0)
			{
				pInternal->BlockLast = pInternal->BlockCount + inst->Len / pInternal->BlockSize - 1;		/* complete blocks at beginning and at end */
				pInternal->BlockIncomplEnd = 0;
			}
			else
			{
				pInternal->BlockLast = pInternal->BlockCount + inst->Len / pInternal->BlockSize + 0;		/* incomplete block at end */
				pInternal->BlockIncomplEnd = 1;
			}
		}
	}

	/* --- timeout monitoring --- */
	if (pInternal->Step != STEP_IDLE && pInternal->StepOld == pInternal->Step)
	{
		pInternal->cntTime += pInternal->CycleTime;
		RETURN_ON_ERROR((pInternal->cntTime > pInternal->Timeout), tpERR_TIMEOUT);
	}
	else
	{
		pInternal->cntTime = 0;
		pInternal->StepOld = pInternal->Step;
	}
	
	switch (pInternal->Step)
	{
		case STEP_TPREAD_READ_RESP:
			/* --- read incoming frame --- */
			FRM_read(&pInternal->FRM_read_0);
			if (pInternal->FRM_read_0.status == frmERR_NOINPUT)
				break;	/* try again next cycle */
			RETURN_ON_ERROR((pInternal->FRM_read_0.status != 0), pInternal->FRM_read_0.status);
			RETURN_ON_ERROR((pInternal->FRM_read_0.buflng > sizeof(pInternal->FrameIn)), tpERR_BUFFERLEN);
			memcpy(pInternal->FrameIn, (void*)pInternal->FRM_read_0.buffer, pInternal->FRM_read_0.buflng);
			memset(pInternal->FrameIn + pInternal->FRM_read_0.buflng, 0, sizeof(pInternal->FrameIn) - pInternal->FRM_read_0.buflng);
			
			/* --- release input buffer --- */
			pInternal->FRM_rbuf_0.buffer = pInternal->FRM_read_0.buffer;
			pInternal->FRM_rbuf_0.buflng = pInternal->FRM_read_0.buflng;
			FRM_rbuf(&pInternal->FRM_rbuf_0);
			RETURN_ON_ERROR((pInternal->FRM_rbuf_0.status != 0), pInternal->FRM_rbuf_0.status);

			/* --- evaluate response --- */
			RESET_INTERNAL(RegExpMatch((STRING*)pInternal->FrameIn, "[]\r\n") == 1);			/* no card in range */
			RESET_INTERNAL(RegExpMatch((STRING*)pInternal->FrameIn, ".*&{00&}.*\r\n") == 1);	/* '{00}'	data transmission conflict, bad card position */
			RESET_INTERNAL(RegExpMatch((STRING*)pInternal->FrameIn, ".*z.*\r\n") == 1);			/* 'z'		data transmission conflict, bad card position */
			RETURN_ON_ERROR(pInternal->Status == TP_STATUS_NO_CARDINRANGE, tpERR_CARD_REMOVED);

			RETURN_ON_ERROR((RegExpMatch((STRING*)pInternal->FrameIn, "[01##]\r\n") == 1), tpERR_ISOERR_READ_RESP + Hex2Byte(&pInternal->FrameIn[3]));
			strcpy(strRegExp, "[#{");
			myitoa(2+pInternal->BlockSize*2, strTmp);
			strcat(strRegExp, strTmp);
			strcat(strRegExp, "}]\r\n");
			RETURN_ON_ERROR((RegExpMatch((STRING*)pInternal->FrameIn, strRegExp) == 0), tpERR_INVALID_READ_RESP);

			/* 1 incomplete block at the beginning and no other block */
			if (pInternal->BlockIncomplBeg == 1 && pInternal->BlockLast == pInternal->BlockCount)
			{
				offs = inst->Offset % pInternal->BlockSize;
				max = inst->Offset % pInternal->BlockSize + inst->Len;
			}
			/* 1 incomplete block at the beginning and some following blocks */
			else if (pInternal->BlockIncomplBeg == 1)
			{
				offs = inst->Offset % pInternal->BlockSize;
				max = pInternal->BlockSize; 
			}
			/* incomplete block at the end */
			else if (pInternal->BlockCount == pInternal->BlockLast && pInternal->BlockIncomplEnd)
			{
				offs = 0;		
				max = (inst->Len + (inst->Offset % pInternal->BlockSize)) % pInternal->BlockSize;
			}
			/* full block */
			else	
			{
				offs = 0;
				max = pInternal->BlockSize;
			}
			pInternal->BlockIncomplBeg = 0;
			
			for (i = offs; i < max; i++, pInternal->DestOffset++)
				((USINT*)inst->pDestination)[pInternal->DestOffset] = Hex2Byte(&pInternal->FrameIn[3+2*i]);

			pInternal->BlockCount++;
			RETURN_REGULAR(pInternal->BlockCount > pInternal->BlockLast);
			pInternal->Step = STEP_TPREAD_READ_REQ;
		/* break; */	/* no break so that next read request is immediately sent */
		
		case STEP_TPREAD_READ_REQ:
			/* --- compose the frame to be sent --- */
			ISOFrame.Flags = ISO_FLAG_DATA_RATE_HIGH | ISO_FLAG_ADDRESS;
			ISOFrame.Cmd = ISO_CMD_READ_SINGLE_BLOCK;
			for (i = 0; i < UID_SIZE; i++)
				ISOFrame.Parameters[i] = pInternal->UID[i];		/* UID for addressed mode. */
			ISOFrame.Parameters[i] = pInternal->BlockCount;
			GetTRFFromISO(&TRFFrame, &ISOFrame, UID_SIZE+1);
			GetTRFRaw(pInternal->FrameOut, &TRFFrame);

			/* --- send the frame --- */
			pInternal->FRM_write_0.buflng = TRFFrame.PacketLength*2;
			FRM_write(&pInternal->FRM_write_0);
			RETURN_ON_ERROR((pInternal->FRM_write_0.status != 0), pInternal->FRM_write_0.status);

			pInternal->Step = STEP_TPREAD_READ_RESP;
		break;
	
		default:
		break;
	}
}
