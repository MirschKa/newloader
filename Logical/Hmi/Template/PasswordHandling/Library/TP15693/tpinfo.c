/******************************************************************************/
/**  \file 		tpinfo.c
	 \author 	krammerm, Copyright Bernecker + Rainer
	 \date 		November 18, 2008
	 \brief 	Implementation of function block TPInfo().
*/
/******************************************************************************/

#include <bur/plctypes.h>
#include <string.h>

#include "tp15693.h"
#include "tp15693_internal.h"

#define RETURN_ON_ERROR(err, errnum)											\
{																				\
	if (err)																	\
	{																			\
		inst->ErrorID = errnum;													\
		inst->Error = 1;														\
		inst->Busy = 0;															\
		inst->Status = pInternal->Status = TP_STATUS_NO_CARDINRANGE;			\
		inst->BlockSize = pInternal->BlockSize = 0;								\
		inst->MemSize = 0;														\
		inst->NumBlocks = pInternal->NumBlocks = 0;								\
		inst->RSSI = pInternal->RSSI = 0;										\
		for (i = 0; i < UID_SIZE; i++) inst->UID[i] = pInternal->UID[i] = 0;	\
		pInternal->Locked = FUB_FREE;											\
		pInternal->Step = STEP_IDLE;											\
		pInternal->FirstRun = 1;												\
		return;																	\
	}																			\
}
																				
#define RETURN_REGULAR(condition)												\
{																				\
	if (condition)																\
	{																			\
		inst->Done = 1;															\
		inst->Busy = 0;															\
		inst->BlockSize = pInternal->BlockSize;									\
		inst->NumBlocks = pInternal->NumBlocks;									\
		inst->MemSize = inst->BlockSize * inst->NumBlocks;						\
		inst->Status = pInternal->Status;										\
		for (i = 0; i < UID_SIZE; i++)	inst->UID[i] = pInternal->UID[i];		\
		inst->RSSI = pInternal->RSSI;											\
		pInternal->Locked = FUB_FREE;											\
		pInternal->Step = STEP_IDLE;											\
		return;																	\
	}																			\
}

#define RESET_INTERNAL(condition)												\
{																				\
	if (condition)																\
	{																			\
		pInternal->Status = TP_STATUS_NO_CARDINRANGE;							\
		pInternal->RSSI = 0;													\
		pInternal->BlockSize = 0;												\
		pInternal->NumBlocks = 0;												\
		for (i = 0; i < UID_SIZE; i++) pInternal->UID[i] = 0;					\
	}																			\
}																				\
		

/** \struct TPInfo_typ
	\ingroup grp_TPInfo
    Input and output parameters of function block TPInfo().
*/

/**	\var TPInfo_typ::Enable
	<B>[IN]</B> Function block is executed only if Enable==TRUE, see \ref page_timing.
*/

/**	\var TPInfo_typ::Ident
	<B>[IN]</B> Ident retrieved from TPInit().
   	The Ident for a transponder is retrieved from function block TPInit() and needs to be passed on to all other function blocks of this library.
*/
	
/**	\var TPInfo_typ::Done
	<B>[OUT]</B> Function block has finished successfully, see \ref page_timing.
*/

/**	\var TPInfo_typ::Busy
	<B>[OUT]</B> Function block is not finished, see \ref page_timing.
*/

/**	\var TPInfo_typ::Error
	<B>[OUT]</B> Error occurred within function block, see \ref page_timing.
*/

/**	\var TPInfo_typ::ErrorID
	<B>[OUT]</B> Error number, see \ref page_timing.
*/

/**	\var TPInfo_typ::Status
	<B>[OUT]</B> Status of the transponder, for possible values see \ref grp_Constants.
*/

/**	\var TPInfo_typ::RSSI
	<B>[OUT]</B> Received signal strength indicator, [64= lowest, 128=highest].
*/

/**	\var TPInfo_typ::MemSize
	<B>[OUT]</B> Available memory size on the chipcard, it is calculated by TPInfo_typ::BlockSize * TPInfo_typ::NumBlocks.
*/

/**	\var TPInfo_typ::BlockSize
	<B>[OUT]</B> Size of one memory block on the chipcard [bytes].
*/

/**	\var TPInfo_typ::NumBlocks
	<B>[OUT]</B> Number of memory blocks on the chipcard.
*/

/**	\var TPInfo_typ::UID
	<B>[OUT]</B> UID of the transponder.
	The Unique ID is a 64bit value unique for every chipcard. \c 0x0000000000000000 is returned if no chipcard is in range.
*/

/** Poll for a chipcard.
    Use this function block to poll for a chipcard. If a chipcard is in range, all basic information (UID, memory size and memory format) is read
	out from the card. Memory on the card is organized in blocks, see also \ref page_glossary "Glossary".
	\ingroup grp_TPInfo */
void TPInfo(struct TPInfo* inst)
{
	InternalPara_type* pInternal;
	TRF7960_Frame_type TRFFrame;
	ISO15693_Frame_type ISOFrame;
	UINT i;
	USINT newUIDbyte;
	BOOL UIDchanged;
	USINT infoflags;
	USINT offs;
	BOOL recvempty;

	pInternal = (InternalPara_type*)inst->Ident;
	if (pInternal == NULL )									{ inst->Error = 1; inst->ErrorID = tpERR_IDENT_NULL; 		return; }	
	if (pInternal->ChkSum != sizeof(InternalPara_type))		{ inst->Error = 1; inst->ErrorID = tpERR_IDENT_INVALID; 	return; }

	if (inst->Enable == 0)
	{
		inst->Busy = 0;
		inst->Done = 0;
		inst->Error = 0;
		inst->ErrorID = 0;
		inst->BlockSize = 0;
		inst->MemSize = 0;
		inst->NumBlocks = 0;
		inst->Status = TP_STATUS_NO_CARDINRANGE;
		inst->RSSI = 0;
		for (i = 0; i < UID_SIZE; i++) inst->UID[i] = 0;
		pInternal->Locked = FUB_FREE;
		return;
	}
	else if (inst->Done == 1 || inst->Error == 1)	/* function block has finished, must be reset once with Enable = 0 ! */
	{
		return;	
	}
	
	if (pInternal->Locked != FUB_TPINFO && pInternal->Locked != FUB_FREE)	{ inst->Error = 1; inst->ErrorID = tpERR_SEMAPHORE_LOCKED; 	return; }
	
	if (pInternal->Step == STEP_IDLE)
	{
		pInternal->Locked = FUB_TPINFO;
		inst->Busy = 1;
		/* if it is the first call of the function block, the transponder chip must be initialized */
		if (pInternal->FirstRun)	
		{
			pInternal->Status = TP_STATUS_NO_CARDINRANGE;
			pInternal->RSSI = 0;
			pInternal->BlockSize = 0;
			pInternal->NumBlocks = 0;
			for (i = 0; i < UID_SIZE; i++) pInternal->UID[i] = 0;
			pInternal->Step = STEP_TPINFO_SET_PROTOCOL_REQ;
		}
		else
		{
			pInternal->Step = STEP_TPINFO_GET_UID_REQ;
		}
	}

	/* --- timeout monitoring --- */
	if (pInternal->Step != STEP_IDLE && pInternal->StepOld == pInternal->Step)
	{
		pInternal->cntTime += pInternal->CycleTime;
		RETURN_ON_ERROR((pInternal->cntTime > pInternal->Timeout), tpERR_TIMEOUT);
	}
	else
	{
		pInternal->cntTime = 0;
		pInternal->StepOld = pInternal->Step;
	}
	
	/* --- main step sequence --- */
	switch (pInternal->Step)
	{
		case STEP_TPINFO_SET_PROTOCOL_REQ:
			/* --- empty receive buffers --- */
			recvempty = 0;
			while (recvempty == 0)
			{
				FRM_read(&pInternal->FRM_read_0);
				if (pInternal->FRM_read_0.status != 0) recvempty = 1;
				else
				{
					pInternal->FRM_rbuf_0.buffer = pInternal->FRM_read_0.buffer;
					pInternal->FRM_rbuf_0.buflng = pInternal->FRM_read_0.buflng;
					FRM_rbuf(&pInternal->FRM_rbuf_0);
					if (pInternal->FRM_rbuf_0.status != 0) recvempty = 1;
				}
			}		

			/* --- compose the frame to be sent, according to TRF7960 documentation --- */
			InitTRFFrame(&TRFFrame);
			TRFFrame.Cmd = 				TRF7960_CMD_WRITE_SINGLE_REGISTER;
			TRFFrame.Parameters[0] = 	TRF7960_REGISTER_CHIPSTATUS;
			TRFFrame.Parameters[1] = 	TRF7960_FLAG_CHIPSTAT_VRS_5 | TRF7960_FLAG_CHIPSTAT_RF_ON;
			TRFFrame.Parameters[2] = 	TRF7960_REGISTER_ISO;
			TRFFrame.Parameters[3] = 	TRF7960_FLAG_ISO_DATA_RATE_HIGH;
			TRFFrame.PacketLength = 	4 + TRF7960_FRAME_CONST/2;
			GetTRFRaw(pInternal->FrameOut, &TRFFrame);

			/* --- send the frame --- */
			pInternal->FRM_write_0.buflng = TRFFrame.PacketLength*2;	/* every byte is transmitted as 2 characters representing the hex value */
			FRM_write(&pInternal->FRM_write_0);
			RETURN_ON_ERROR((pInternal->FRM_write_0.status != 0), pInternal->FRM_write_0.status);
			
			pInternal->FirstRun = 0;
			pInternal->Step = STEP_TPINFO_SET_PROTOCOL_RESP;
		break;

		case STEP_TPINFO_SET_PROTOCOL_RESP:
			/* --- read incoming frame --- */
			FRM_read(&pInternal->FRM_read_0);
			if (pInternal->FRM_read_0.status == frmERR_NOINPUT)
				break;	/* try again next cycle */
			RETURN_ON_ERROR((pInternal->FRM_read_0.status != 0), pInternal->FRM_read_0.status);
			RETURN_ON_ERROR((pInternal->FRM_read_0.buflng > sizeof(pInternal->FrameIn)), tpERR_BUFFERLEN);
			memcpy(pInternal->FrameIn, (void*)pInternal->FRM_read_0.buffer, pInternal->FRM_read_0.buflng);
			memset(pInternal->FrameIn + pInternal->FRM_read_0.buflng, 0, sizeof(pInternal->FrameIn) - pInternal->FRM_read_0.buflng);
			
			/* --- release input buffer --- */
			pInternal->FRM_rbuf_0.buffer = pInternal->FRM_read_0.buffer;
			pInternal->FRM_rbuf_0.buflng = pInternal->FRM_read_0.buflng;
			FRM_rbuf(&pInternal->FRM_rbuf_0);
			RETURN_ON_ERROR((pInternal->FRM_rbuf_0.status != 0), pInternal->FRM_rbuf_0.status);
			
			pInternal->Step = STEP_TPINFO_GET_UID_REQ;
		break;

		case STEP_TPINFO_GET_UID_REQ:
			/* --- compose the frame to be sent --- */
			ISOFrame.Flags = ISO_FLAG_INVENTORY | ISO_FLAG_DATA_RATE_HIGH | ISO_FLAG_INV_NB_SLOTS_1;
			ISOFrame.Cmd = ISO_CMD_INVENTORY;
			ISOFrame.Parameters[0] = 0;		/* mask length */
			GetTRFFromISO(&TRFFrame, &ISOFrame, 1);
			GetTRFRaw(pInternal->FrameOut, &TRFFrame);

			/* --- send the frame --- */
			pInternal->FRM_write_0.buflng = TRFFrame.PacketLength*2;
			FRM_write(&pInternal->FRM_write_0);
			RETURN_ON_ERROR((pInternal->FRM_write_0.status != 0), pInternal->FRM_write_0.status);

			pInternal->Step = STEP_TPINFO_GET_UID_RESP;
		break;
		
		case STEP_TPINFO_GET_UID_RESP:
			/* --- read incoming frame --- */
			FRM_read(&pInternal->FRM_read_0);
			if (pInternal->FRM_read_0.status == frmERR_NOINPUT)
				break;	/* try again next cycle */
			RETURN_ON_ERROR((pInternal->FRM_read_0.status != 0), pInternal->FRM_read_0.status);
			RETURN_ON_ERROR((pInternal->FRM_read_0.buflng > sizeof(pInternal->FrameIn)), tpERR_BUFFERLEN);
			memcpy(pInternal->FrameIn, (void*)pInternal->FRM_read_0.buffer, pInternal->FRM_read_0.buflng);
			memset(pInternal->FrameIn + pInternal->FRM_read_0.buflng, 0, sizeof(pInternal->FrameIn) - pInternal->FRM_read_0.buflng);
			
			/* --- release input buffer --- */
			pInternal->FRM_rbuf_0.buffer = pInternal->FRM_read_0.buffer;
			pInternal->FRM_rbuf_0.buflng = pInternal->FRM_read_0.buflng;
			FRM_rbuf(&pInternal->FRM_rbuf_0);
			RETURN_ON_ERROR((pInternal->FRM_rbuf_0.status != 0), pInternal->FRM_rbuf_0.status);

			/* --- evaluate response --- */
			pInternal->Status = TP_STATUS_CARDINRANGE;
			RESET_INTERNAL(RegExpMatch((STRING*)pInternal->FrameIn, "[,##]\r\n") == 1);			/* no card in range */
			RESET_INTERNAL(RegExpMatch((STRING*)pInternal->FrameIn, ".*&{00&}.*\r\n") == 1);	/* '{00}'	data transmission conflict, bad card position */
			RESET_INTERNAL(RegExpMatch((STRING*)pInternal->FrameIn, ".*z.*\r\n") == 1);			/* 'z'		data transmission conflict, bad card position */
			RETURN_REGULAR(pInternal->Status == TP_STATUS_NO_CARDINRANGE);

			RETURN_ON_ERROR((RegExpMatch((STRING*)pInternal->FrameIn, "[#{16},##]\r\n") == 0), tpERR_INVALID_UID_RESP);
			
			UIDchanged = 0;
			for (i = 0; i < UID_SIZE; i++)
			{
				newUIDbyte = Hex2Byte(&pInternal->FrameIn[1 + 2*i]);
				UIDchanged |= (newUIDbyte != pInternal->UID[i]);
				pInternal->UID[i] = newUIDbyte;
			}
			pInternal->RSSI = Hex2Byte(&pInternal->FrameIn[18]);

			RETURN_REGULAR(UIDchanged == 0);

			pInternal->Step = STEP_TPINFO_GET_SYSTEMINFO;
		break;

		case STEP_TPINFO_GET_SYSTEMINFO:
			/* --- compose the frame to be sent --- */
			ISOFrame.Flags = ISO_FLAG_DATA_RATE_HIGH | ISO_FLAG_ADDRESS;
			ISOFrame.Cmd = ISO_CMD_GET_SYSTEM_INFORMATION;
			for (i = 0; i < UID_SIZE; i++)
				ISOFrame.Parameters[i] = pInternal->UID[i];		/* UID for addressed mode. */
			GetTRFFromISO(&TRFFrame, &ISOFrame, UID_SIZE);
			GetTRFRaw(pInternal->FrameOut, &TRFFrame);

			/* --- send the frame --- */
			pInternal->FRM_write_0.buflng = TRFFrame.PacketLength*2;
			FRM_write(&pInternal->FRM_write_0);
			RETURN_ON_ERROR((pInternal->FRM_write_0.status != 0), pInternal->FRM_write_0.status);

			pInternal->Step = STEP_TPINFO_GET_SYSTEMINFO_RESP;
		break;

		case STEP_TPINFO_GET_SYSTEMINFO_RESP:
			/* --- read incoming frame --- */
			FRM_read(&pInternal->FRM_read_0);
			if (pInternal->FRM_read_0.status == frmERR_NOINPUT)
				break;	/* try again next cycle */
			RETURN_ON_ERROR((pInternal->FRM_read_0.status != 0), pInternal->FRM_read_0.status);
			RETURN_ON_ERROR((pInternal->FRM_read_0.buflng > sizeof(pInternal->FrameIn)), tpERR_BUFFERLEN);
			memcpy(pInternal->FrameIn, (void*)pInternal->FRM_read_0.buffer, pInternal->FRM_read_0.buflng);
			memset(pInternal->FrameIn + pInternal->FRM_read_0.buflng, 0, sizeof(pInternal->FrameIn) - pInternal->FRM_read_0.buflng);
			
			/* --- release input buffer --- */
			pInternal->FRM_rbuf_0.buffer = pInternal->FRM_read_0.buffer;
			pInternal->FRM_rbuf_0.buflng = pInternal->FRM_read_0.buflng;
			FRM_rbuf(&pInternal->FRM_rbuf_0);
			RETURN_ON_ERROR((pInternal->FRM_rbuf_0.status != 0), pInternal->FRM_rbuf_0.status);

			/* --- evaluate response --- */
			RESET_INTERNAL(RegExpMatch((STRING*)pInternal->FrameIn, "[]\r\n") == 1)			/* no card in range */
			RESET_INTERNAL(RegExpMatch((STRING*)pInternal->FrameIn, ".*&{00&}.*\r\n") == 1)	/* '{00}'	data transmission conflict, bad card position */
			RESET_INTERNAL(RegExpMatch((STRING*)pInternal->FrameIn, ".*z.*\r\n") == 1)		/* 'z'		data transmission conflict, bad card position */
			RETURN_REGULAR(pInternal->Status == TP_STATUS_NO_CARDINRANGE);

			RETURN_ON_ERROR((RegExpMatch((STRING*)pInternal->FrameIn, "[01##]\r\n") == 1), tpERR_ISOERR_SYSINFO_RESP + Hex2Byte(&pInternal->FrameIn[3]));
			RETURN_ON_ERROR((RegExpMatch((STRING*)pInternal->FrameIn, "[#{20-30}]\r\n") == 0), tpERR_INVALID_SYSINFO_RESP);

			infoflags = Hex2Byte(&pInternal->FrameIn[3]);
			offs = 0;
			if (infoflags&ISO_FLAG_SYSINFO_DSFID)	offs += 2;
			if (infoflags&ISO_FLAG_SYSINFO_AFI)		offs += 2;
			if (infoflags&ISO_FLAG_SYSINFO_MEMSIZE)
			{
				pInternal->NumBlocks = Hex2Byte(&pInternal->FrameIn[21+offs]) + 1;		
				pInternal->BlockSize = (Hex2Byte(&pInternal->FrameIn[23+offs])&0x1F) + 1;	/* ISO15693 spec: upper 3 bits are RFU */
			}
			else
			{
				pInternal->NumBlocks = 0;
				pInternal->BlockSize = 0;
			}

			RETURN_REGULAR(1);
		break;
		
		default:
		break;	
	}	
}

