/******************************************************************************/
/**  \file 		tpraw.c
	 \author 	krammerm, Copyright Bernecker + Rainer
	 \date 		November 18, 2008
	 \brief 	Implementation of function block TPRaw().
*/
/******************************************************************************/

#include <bur/plctypes.h>
#include <string.h>

#include "tp15693.h"
#include "tp15693_internal.h"

#define RETURN_ON_ERROR(err, errnum)											\
{																				\
	if (err)																	\
	{																			\
		inst->ErrorID = errnum;													\
		inst->Error = 1;														\
		inst->Busy = 0;															\
		pInternal->Locked = FUB_FREE;											\
		pInternal->Step = STEP_IDLE;											\
		return;																	\
	}																			\
}
																				
#define RETURN_REGULAR(condition)												\
{																				\
	if (condition)																\
	{																			\
		inst->Done = 1;															\
		inst->Busy = 0;															\
		pInternal->Locked = FUB_FREE;											\
		pInternal->Step = STEP_IDLE;											\
		return;																	\
	}																			\
}

/** \struct TPRaw_typ
	\ingroup grp_TPRaw
    Input and output parameters of function block TPRaw().
*/

/**	\var TPRaw_typ::Ident
	\copybrief TPInfo_typ::Ident
	\copydetails TPInfo_typ::Ident
*/

/**	\var TPRaw_typ::Enable
	\copybrief TPInfo_typ::Enable
	\copydetails TPInfo_typ::Enable
*/

/**	\var TPRaw_typ::Error
	\copybrief TPInfo_typ::Error
	\copydetails TPInfo_typ::Error
*/

/**	\var TPRaw_typ::ErrorID
	\copybrief TPInfo_typ::ErrorID
	\copydetails TPInfo_typ::ErrorID
*/

/**	\var TPRaw_typ::Done
	\copybrief TPInfo_typ::Done
	\copydetails TPInfo_typ::Done
*/

/**	\var TPRaw_typ::Busy
	\copybrief TPInfo_typ::Busy
	\copydetails TPInfo_typ::Busy
*/

/**	\var TPRaw_typ::pFrameIn
	<B>[IN]</B> Memory to which the incoming ISO Frame is copied (address of a string).
*/

/**	\var TPRaw_typ::pFrameOut
	<B>[IN]</B> ISO Frame to be sent to the transponder, according to request format specified in ISO15693 (address of a string).
*/

/**	\var TPRaw_typ::LenOut
	<B>[IN]</B> Length of the outgoing ISO frame [bytes].
*/

/**	\var TPRaw_typ::LenIn
	<B>[OUT]</B> Length of the incoming ISO frame [bytes].
*/

/** Submit a raw ISO command to the transponder.
	\ingroup grp_TPRaw
	
	The <B>request</B> to be sent to the transponder must be composed according to Part 3 of ISO15693, but must not contain the SOF,
	CRC and EOF parts. The sub-carrier_flag  must be set to 0, the data_rate_flag must be set to 1.<BR>
	<BR>
	\image html iso15693_requestformat.jpg "Fig. 03: ISO15693 General request format."
	Example - write data to the first block in the chipcard:<BR>
	<BR>
	<CENTER><tt>42 21 00 12341234</tt></CENTER><BR>
	42: Data_rate_flag and Option_flag are set<BR>
	21: ISO15693 command to write a single block<BR>
	00: block number 00<BR>
	12345678: the 4 bytes of the block should be set to 0x12, 0x34, 0x56, 0x78<BR>
	<BR>
	
	The <B>response</B> of the transponder reader has the following format: <tt>[ISOresponse]$R$L</tt><BR>
	The CRLF signals the end of the response received. The <tt>ISOresponse</tt> that is received does not contain SOF, CRC and EOF
	parts.<BR>
	<BR>
	\image html iso15693_responseformat.jpg "Fig. 03: ISO15693 General response format."
	Example - command 21 (write single block) was executed successfully:<BR>
	<BR>
	<CENTER><tt>[00]$R$L</tt></CENTER><BR>
	<BR>
	Example - command 21 (write single block) wanted to write a block that does not exist on the chipcard:<BR>
	<BR>
	<CENTER><tt>[0110]$R$L</tt></CENTER><BR>
	<BR>

*/
void TPRaw(struct TPRaw* inst)
{
	InternalPara_type* pInternal;
	TRF7960_Frame_type TRFFrame;
	UINT i;
	USINT* pFrameOut;
	USINT* pFrameIn;

	pInternal = (InternalPara_type*)inst->Ident;
	if (pInternal == NULL )													{ inst->Error = 1; inst->ErrorID = tpERR_IDENT_NULL; 		return; }	
	if (pInternal->ChkSum != sizeof(InternalPara_type))						{ inst->Error = 1; inst->ErrorID = tpERR_IDENT_INVALID; 	return; }

	pFrameOut = (USINT*)inst->pFrameOut;
	pFrameIn = (USINT*)inst->pFrameIn;

	if (inst->Enable == 0)
	{
		inst->Busy = 0;
		inst->Done = 0;
		inst->Error = 0;
		inst->ErrorID = 0;
		pInternal->Locked = FUB_FREE;
		return;
	}
	else if (inst->Done == 1 || inst->Error == 1)	/* function block has finished, must be reset once with Enable = 0 ! */
	{
		return;	
	}
	
	if (pInternal->Locked != FUB_TPRAW && pInternal->Locked != FUB_FREE)	{ inst->Error = 1; inst->ErrorID = tpERR_SEMAPHORE_LOCKED; 	return; }
	if (pInternal->Status == TP_STATUS_NO_CARDINRANGE)						{ inst->Error = 1; inst->ErrorID = tpERR_NO_CARD_IN_RANGE;	return; }
	if (inst->LenOut > ISO_FRAME_PARA_SIZE || inst->LenOut % 2 != 0)		{ inst->Error = 1; inst->ErrorID = tpERR_ILLEGAL_LENGTH;	return; }
	if (inst->LenOut == 0)													{ inst->Done = 1; 											return; }
	
	if (pInternal->Step == STEP_IDLE)
	{
		pInternal->Locked = FUB_TPRAW;
		inst->Busy = 1;
		pInternal->Step = STEP_TPRAW_REQ;
	}

	/* --- timeout monitoring --- */
	if (pInternal->Step != STEP_IDLE && pInternal->StepOld == pInternal->Step)
	{
		pInternal->cntTime += pInternal->CycleTime;
		RETURN_ON_ERROR((pInternal->cntTime > pInternal->Timeout), tpERR_TIMEOUT);
	}
	else
	{
		pInternal->cntTime = 0;
		pInternal->StepOld = pInternal->Step;
	}
	
	switch (pInternal->Step)
	{
		case STEP_TPRAW_REQ:
			/* --- compose the frame to be sent --- */
			InitTRFFrame(&TRFFrame);
			TRFFrame.Cmd = TRF7960_CMD_REQUEST_ISO;
			for (i = 0; i < inst->LenOut / 2; i++)
				TRFFrame.Parameters[i] = Hex2Byte(&pFrameOut[i*2]);
			TRFFrame.PacketLength = inst->LenOut/2 + TRF7960_FRAME_CONST/2;
			GetTRFRaw(pInternal->FrameOut, &TRFFrame);

			/* --- send the frame --- */
			pInternal->FRM_write_0.buflng = TRFFrame.PacketLength*2;
			FRM_write(&pInternal->FRM_write_0);
			RETURN_ON_ERROR((pInternal->FRM_write_0.status != 0), pInternal->FRM_write_0.status);

			pInternal->Step = STEP_TPRAW_RESP;
		break;

		case STEP_TPRAW_RESP:
			/* --- read incoming frame --- */
			FRM_read(&pInternal->FRM_read_0);
			if (pInternal->FRM_read_0.status == frmERR_NOINPUT)
				break;	/* try again next cycle */
			RETURN_ON_ERROR((pInternal->FRM_read_0.status != 0), pInternal->FRM_read_0.status);
			RETURN_ON_ERROR((pInternal->FRM_read_0.buflng > sizeof(pInternal->FrameIn)), tpERR_BUFFERLEN);
			memcpy(pInternal->FrameIn, (void*)pInternal->FRM_read_0.buffer, pInternal->FRM_read_0.buflng);
			memset(pInternal->FrameIn + pInternal->FRM_read_0.buflng, 0, sizeof(pInternal->FrameIn) - pInternal->FRM_read_0.buflng);
			memcpy(pFrameIn, (void*)pInternal->FRM_read_0.buffer, pInternal->FRM_read_0.buflng);
			inst->LenIn = pInternal->FRM_read_0.buflng;
						
			/* --- release input buffer --- */
			pInternal->FRM_rbuf_0.buffer = pInternal->FRM_read_0.buffer;
			pInternal->FRM_rbuf_0.buflng = pInternal->FRM_read_0.buflng;
			FRM_rbuf(&pInternal->FRM_rbuf_0);
			RETURN_ON_ERROR((pInternal->FRM_rbuf_0.status != 0), pInternal->FRM_rbuf_0.status);

			RETURN_REGULAR(1);
		break;
	
		default:
		break;
	}
}
