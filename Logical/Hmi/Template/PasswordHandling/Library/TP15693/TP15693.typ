(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: TP15693
 * File: TP15693.typ
 * Author: krammerm
 * Created: January 26, 2009
 ********************************************************************
 * Data types of library TP15693
 ********************************************************************)

TYPE
	DiagInfo_typ : 	STRUCT 
		ChkSum : UINT;
		FrameOut : ARRAY[0..255] OF USINT;
		FrameIn : ARRAY[0..255] OF USINT;
		Step : UINT;
		StepOld : UINT;
		Status : USINT;
		UID : ARRAY[0..7] OF USINT;
		RSSI : USINT;
		BlockSize : USINT;
		NumBlocks : USINT;
		cntTime : UDINT;
		Timeout : UDINT;
		CycleTime : UDINT;
		DVIdent : UDINT;
		Locked : USINT;
		FirstRun : BOOL;
		BlockCount : UINT;
		BlockLast : UINT;
		BlockIncomplBeg : BOOL;
		BlockIncomplEnd : BOOL;
		DestOffset : UINT;
		SourceOffset : UINT;
		BlockMultiple : UINT;
		LastBlock : ARRAY[0..31] OF USINT;
		FirstBlock : ARRAY[0..31] OF USINT;
		DebugInfo : UDINT;
		FRM_rbuf_0 : FRM_rbuf;
		FRM_read_0 : FRM_read;
		FRM_write_0 : FRM_write;
		FRM_xopen_0 : FRM_xopen;
	END_STRUCT;
END_TYPE
