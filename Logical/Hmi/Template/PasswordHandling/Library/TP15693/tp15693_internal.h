/******************************************************************************/
/**  \file tp15693_internal.h
	 \author 	krammerm, Copyright Bernecker + Rainer
	 \date 		November 18, 2008
	 \brief Internal declarations and defines for TP15693 Library.
	 \details Contains also additional doxygen documentation referring to the AutomationStudio automatically generated library header file,
	 	which cannot be edited manually. Contains also additional doxygen documentation for grouping in "Modules" and additional pages.
*/
/******************************************************************************/
#ifndef _TP15693_INTERNAL_
#define _TP15693_INTERNAL_

#include <bur/plctypes.h>
#include <DVFrame.h>

/* \cond DEV */
/* --- locking/sharing of internal memory between all function blocks of the library --- */
#define	FUB_FREE									0		/**< Internal memory is free. */
#define	FUB_TPINIT									1		/**< Internal memory locked by Function block TPInit(). */
#define FUB_TPINFO									2		/**< Internal memory locked by Function block TPInfo(). */
#define FUB_TPREAD									3		/**< Internal memory locked by Function block TPRead(). */
#define FUB_TPWRITE									4		/**< Internal memory locked by Function block TPWrite(). */
#define FUB_TPRAW									5		/**< Internal memory locked by Function block TPRaw(). */
#define FUB_TPREADM									6		/**< Internal memory locked by Function block TPReadM(). */
#define FUB_TPWRITEM								7		/**< Internal memory locked by Function block TPWriteM(). */

/* --- constants for representing the steps in the state sequences of all function blocks. --- */
#define	STEP_IDLE 							0		/**< Idle Step of function block TPInfo(). Function block is disabled. */

#define	STEP_TPINFO_SET_PROTOCOL_REQ		1		/**< step of TPInfo(): send request for chip initialization. */
#define	STEP_TPINFO_SET_PROTOCOL_RESP		2		/**< step of TPInfo(): get response of chip initialization. */
#define	STEP_TPINFO_GET_UID_REQ				3		/**< step of TPInfo(): send request to get UID (inventory command). */
#define	STEP_TPINFO_GET_UID_RESP			4		/**< step of TPInfo(): get the UID (reponse of inventory command). */
#define	STEP_TPINFO_GET_SYSTEMINFO			5		/**< step of TPInfo(): send request to get chipcard information. */
#define	STEP_TPINFO_GET_SYSTEMINFO_RESP		6		/**< step of TPInfo(): get the chipcard information. */

#define STEP_TPREAD_READ_REQ				100		/**< step of TPRead(): send request to read a block from chipcard. */
#define STEP_TPREAD_READ_RESP				101		/**< step of TPRead(): get the responst (data of one block from chipcard). */

#define STEP_TPRAW_REQ						200		/**< step of TPRaw(): send ISO request to chipcard. */
#define STEP_TPRAW_RESP						201		/**< step of TPRaw(): get ISO response from chipcard. */

#define STEP_TPWRITE_READFIRST_REQ			300		/**< step of TPWrite(): if first block is incomplete: send request to read the first block from chipcard. */
#define STEP_TPWRITE_READFIRST_RESP			301		/**< step of TPWrite(): if first block is incomplete: get the response (data of first block). */
#define STEP_TPWRITE_READLAST_REQ			302		/**< step of TPWrite(): if last block is incomplete: send request to read the last block from chipcard. */
#define STEP_TPWRITE_READLAST_RESP			303		/**< step of TPWrite(): if last block is incomplete: get the response (data of last block). */
#define STEP_TPWRITE_WRITE_REQ				304		/**< step of TPWrite(): send request to a block to chipcard. */
#define STEP_TPWRITE_WRITE_RESP				305		/**< step of TPWrite(): get the response (after writing a block to chipcard). */

#define STEP_TPREADM_READ_REQ				400		/**< step of TPReadM(): send request to read multiple blocks from chipcard. */
#define STEP_TPREADM_READ_RESP				401		/**< step of TPReadM(): get the response (data of multiple blocks from chipcard). */

#define STEP_TPWRITEM_WRITE_REQ				500		/**< step of TPWriteM(): send request to write multiple blocks to chipcard. */
#define STEP_TPWRITEM_WRITE_RESP			501		/**< step of TPWriteM(): get the response (data of multiple blocks from chipcard). */


/* --- constants and macros for regular expression matching function --- */
#define CH_SPECIAL	'&'			/**< The following metacharacter is presented as a data character standing for itself. */
#define CH_ANY	 	'.'			/**< Any Character except string end '\\0'. */	
#define CH_HEX		'#'			/**< Any hexadecimal character (0-9, a-f, A-F). */
#define CH_DIG		'%'			/**< Any decimal character (0-9). */
#define CH_WILDCARD '*'			/**< The preceding character must be in the string 0 or more times. */
#define CH_REP_BEG 	'{'			/**< Repetition modifier, for example a{2-4} means aa, aaa or aaaa. */
#define CH_REP_TO 	'-'			/**< Repetition modifier, for example a{2-4} means aa, aaa or aaaa. */
#define CH_REP_END 	'}'			/**< Repetition modifier, for example a{2-4} means aa, aaa or aaaa. */

#define MATCH_HEX(ch)	(((ch) >= '0' && (ch) <= '9') || ((ch) >= 'a' && (ch) <= 'f') || ((ch) >= 'A' && (ch) <= 'F'))
#define MATCH_ANY(ch) 	((ch) != '\0')
#define MATCH_DIG(ch)	((ch) >= '0' && (ch) <= '9')

#define MATCH(ch, rch)	(	((rch) == CH_ANY && MATCH_ANY(ch)) 	||											\
							((rch) == CH_HEX && MATCH_HEX(ch)) 	||											\
							((rch) == CH_DIG && MATCH_DIG(ch))		||										\
							((rch) != CH_ANY && (rch) != CH_HEX && (rch) != CH_DIG && (ch == rch))			\
						)

/* --- constants for the control of the TI chip, see the following documentation from Texas Instruments: 
		"Multi-Standard Fully Integrated 13.56-MHz Radio Frequency Identification (RFID)
		Analog Front End and Data Framing Reader System."													--- */
#define TRF7960_REGISTER_CHIPSTATUS					0x00
#define TRF7960_REGISTER_ISO						0x01

#define TRF7960_CMD_TRF7960_ENABLE					0x03
#define TRF7960_CMD_DIRECT_MODE						0x0F
#define TRF7960_CMD_WRITE_SINGLE_REGISTER			0x10
#define TRF7960_CMD_WRITE_CONTINUOUS				0x11
#define TRF7960_CMD_READ_SINGLE_REGISTER			0x12
#define TRF7960_CMD_READ_CONTINUOUS					0x13
#define TRF7960_CMD_INVENTORY_ISO					0x14
#define TRF7960_CMD_DIRECT_COMMAND					0x15
#define TRF7960_CMD_WRITE_RAW						0x16
#define TRF7960_CMD_REQUEST_ISO						0x18
#define TRF7960_CMD_SID_POLL						0x34
#define TRF7960_CMD_BEGIN_ROUND						0x54
#define TRF7960_CMD_CLOSE_SLOT_SEQUENCE				0x55
#define TRF7960_CMD_REQA							0xA0
#define TRF7960_CMD_SELECT							0xA2
#define TRF7960_CMD_REQB							0xB0
#define TRF7960_CMD_AGC_SELECTION					0xF0
#define TRF7960_CMD_AMPM_INPUT_SELECTION			0xF1    /** changed from original 0XF1 to 0x00, 01.04.2011, HH **/
#define TRF7960_CMD_GET_VERSION						0xFE

#define	TRF7960_FRAME_CONST_SOF						0x01
#define TRF7960_FRAME_CONST_ZERO					0x00
#define TRF7960_FRAME_CONST_BEGIN_DATA_PAYLOAD1		0x03
#define TRF7960_FRAME_CONST_BEGIN_DATA_PAYLOAD2		0x04
#define TRF7960_FRAME_CONST_EOF1					0x00
#define TRF7960_FRAME_CONST_EOF2					0x00

#define	TRF7960_FLAG_CHIPSTAT_VRS_5					0x01	/**< 0 = 3V operation				1 = 5V operation					*/	
#define TRF7960_FLAG_CHIPSTAT_REC_ON				0x02
#define TRF7960_FLAG_CHIPSTAT_AGC_ON				0x04	/**< 0 = AGC off					1 = AGC on							*/
#define TRF7960_FLAG_CHIPSTAT_PM_ON					0x08	/**< 0 = AM input signal			1 = PM input signal					*/
#define TRF7960_FLAG_CHIPSTAT_RF_PWR				0x10	/**< 0 = full output power			1 = half output power				*/
#define TRF7960_FLAG_CHIPSTAT_RF_ON					0x20	/**< 0 = RF output not active		1 = RF output active				*/
#define TRF7960_FLAG_CHIPSTAT_DIRECT				0x40
#define TRF7960_FLAG_CHIPSTAT_STDBY					0x80

#define TRF7960_FLAG_ISO_DATA_CODING_MODE_256		0x01	/**< 0 = 1 out of 4					1 = 1 out of 256 					*/
#define TRF7960_FLAG_ISO_DATA_RATE_HIGH				0x02	/**< 0 = low bit rate 6,62 kbps		1 = high bit rate 26,48 kbps 		*/
#define TRF7960_FLAG_ISO_SUBCARRIER_DOUBLE			0x04	/**< 0 = one subcarrier				1 = double subcarrier				*/
#define TRF7960_FLAG_ISO_BIT3_NOTUSED				0x08
#define TRF7960_FLAG_ISO_BIT4_NOTUSED				0x10
#define TRF7960_FLAG_RFID_MODE						0x20
#define TRF7960_FLAG_ISO_DIRECT_MODE_TYPE			0x40
#define TRF7960_FLAG_ISO_RX_CRC_N					0x80	/**< 0: RX CRC						1: NO RX CRC						*/

/* --- constants for the ISO15693 framing, see the international standard
		ISO/IEC 15693-3 Part 3 "Anticollision and transmission protocol", First edition 2001-04-01	--- */
#define	ISO_CMD_INVENTORY							0x01
#define ISO_CMD_STAY_QUIET							0x02
#define ISO_CMD_READ_SINGLE_BLOCK					0x20
#define ISO_CMD_WRITE_SINGLE_BLOCK					0x21
#define ISO_CMD_LOCK_BLOCK							0x22
#define ISO_CMD_READ_MULTIPLE_BLOCKS				0x23
#define ISO_CMD_WRITE_MULTIPLE_BLOCKS				0x24
#define ISO_CMD_SELECT								0x25
#define ISO_CMD_RESET_TO_READY						0x26
#define ISO_CMD_WRITE_AFI							0x27
#define ISO_CMD_LOCK_AFI							0x28
#define ISO_CMD_WRITE_DSFID							0x29
#define ISO_CMD_LOCK_DSFID							0x2A
#define ISO_CMD_GET_SYSTEM_INFORMATION				0x2B
#define ISO_CMD_GET_MULTIPLE_BLOCKS_SECURITY_STATUS	0x2C

#define ISO_FLAG_SUBCARRIER_DOUBLE					0x01
#define ISO_FLAG_DATA_RATE_HIGH						0x02
#define ISO_FLAG_INVENTORY							0x04
#define ISO_FLAG_PROTOCOL_EXTENSION					0x08

#define ISO_FLAG_SELECT								0x10
#define ISO_FLAG_ADDRESS							0x20
#define ISO_FLAG_OPTION								0x40
#define ISO_FLAG_RFU								0x80

#define ISO_FLAG_INV_AFI_PRESENT					0x10
#define ISO_FLAG_INV_NB_SLOTS_1						0x20
#define ISO_FLAG_INV_OPTION							0x40
#define ISO_FLAG_INV_RFU							0x80

#define ISO_FLAG_SYSINFO_DSFID						0x01
#define ISO_FLAG_SYSINFO_AFI						0x02
#define ISO_FLAG_SYSINFO_MEMSIZE					0x04
#define ISO_FLAG_SYSINFO_ICREFERENCE				0x08
#define ISO_FLAG_SYSINFO_RFU1						0x10
#define ISO_FLAG_SYSINFO_RFU2						0x20
#define ISO_FLAG_SYSINFO_RFU3						0x40
#define ISO_FLAG_SYSINFO_RFU4						0x80


#define TRF7960_FRAME_CONST	(TRF7960_FRAME_LEN - TRF7960_FRAME_PARA_SIZE)	/**< Constant length of a TRF7960 frame without parameters.*/
#define ISO_FRAME_CONST		(TRF7960_FRAME_PARA_SIZE - ISO_FRAME_PARA_SIZE)	/**< Constant length of a ISO15693 frame without parameters. */

#define TRF7960_FRAME_LEN		256	/**< Maximum frame length of a serial frame to TRF7960. */

#define TRF7960_FRAME_PARA_SIZE	240 /**< Maximum possible parameter size of a frame to the TRF7960. Variable length of parameter field. */
#define ISO_FRAME_PARA_SIZE		236 /**< Maximum possible parameter size of a ISO15693 frame within a TRF7960 frame. Variable length of parameter field. */

#define UID_SIZE				8 /**< size of UID in bytes */

#define MAX_BLOCK_SIZE			32 /**< maximum size of one block in bytes, according to ISO15693 */

/** Structure representing the frame that is sent to the TRF7960.	
*/
typedef struct {
	USINT			constSOF;
	USINT			PacketLength;
	USINT			constZero;
	USINT			constBeginDataPayload1;
	USINT			constBeginDataPayload2;
	USINT			Cmd;									/**< Firmware Command */
	USINT			Parameters[TRF7960_FRAME_PARA_SIZE/2];	/**< Parameters, meaning depending on TRF7960_Frame_type::Cmd. */
															/* Only /2 half of parameter size is needed because frame to transponder reader is ASCII coded */
	USINT			constEOF1;
	USINT			constEOF2;
}	TRF7960_Frame_type;

/** Structure representing an ISO15693 frame that is embedded in a TRF7960 frame.	
*/
typedef struct {
	USINT			Flags;
	USINT			Cmd;									/**< ISO Command */
	USINT			Parameters[ISO_FRAME_PARA_SIZE/2];		/**< Parameters, meaning depending on ISO15693_Frame_type::Cmd. */
															/* Only /2 half of parameter size is needed because frame to transponder reader is ASCII coded */
}	ISO15693_Frame_type;	
	
/** Internal memory of parameters used by all functions of TP15693 Library.
	An instance of this structure is allocated and initialized in TPInit() and passed on as Ident to all other function blocks of the library.
*/
typedef struct {
	UINT			ChkSum;			/**< Size of the structure. Set by TPInit() and checked by all function blocks to determine validity of the Ident. */
	USINT 			Status;			/**< Status of the transponder. See \ref grp_Constants for possible values. */
	USINT			UID[UID_SIZE];	/**< UID of the transponder. */
	USINT			RSSI;			/**< Signal strength indicator. */
	UDINT			CycleTime;		/**< CycleTime of the task the function blocks are called [us]. */
	UDINT			Timeout;		/**< Timeout for receiving messages from transponder [us]. */
	UDINT			cntTime;		/**< Counter for Timeout monitoring [us]. */
	USINT			NumBlocks;		/**< Number of memory blocks on the chipcard. */
	USINT			BlockSize;		/**< Size of one memory block on the chipcard [bytes]. */
	UDINT			DVIdent;		/**< Ident for all DVFrame function blocks. */
	UINT			Step;			/**< Step of state sequence of a function block. */
	UINT			StepOld;		/**< StepOld of state sequence, to detect rising edge on step change. */
	USINT			Locked;			/**< Semaphore to lock internal memory. No parallel calls of function blocks allowed. */
	BOOL			FirstRun;		/**< Indicates the first run after booting for function block TPInfo(). */
	UINT			BlockCount;		/**< Block counter for reading/writing memory bigger than 1 block. */
	UINT			BlockLast;		/**< Number of blocks for reading/writing memory bigger than 1 block. */
	BOOL			BlockIncomplBeg;/**< At beginning of the requested memory an incomplete block must be read/written. */
	BOOL			BlockIncomplEnd;/**< At end of the requested memory an incomplete block must be read/written. */
	UINT			DestOffset;		/**< Offset of destination memory, that is filled up with the several Block reads. */
	UINT			SourceOffset;	/**< Offset of source memory from which is copied to chipcard in several Block writes. */
	UINT			BlockMultiple;	/**< Number of blocks that are actually read/written during one read/write multiple command. */
	USINT			FirstBlock[MAX_BLOCK_SIZE];		/**< If the first block to write is incomplete, the untouched data must be read first. */
	USINT			LastBlock[MAX_BLOCK_SIZE];		/**< If the last block to write is incomplete, the untouched data must be read first. */
	UDINT			DebugInfo;		/**< For Debug purposes */
	USINT			FrameOut[TRF7960_FRAME_LEN];	/**< Raw data of outgoing frame via serial connection to transponder. For debug purposes only. */
	USINT			FrameIn[TRF7960_FRAME_LEN];		/**< Raw data of first incoming frame via serial connection from transponder. For debug purposes only. */	
	FRM_xopen_typ	FRM_xopen_0;	/**< Instance of FRM_xopen function block. */
	FRM_close_typ	FRM_close_0;	/**< Instance of FRM_close function block. */
	FRM_write_typ	FRM_write_0;	/**< Instance of FRM_write function block. */
	FRM_read_typ	FRM_read_0;		/**< Instance of FRM_read function block. */
	FRM_rbuf_typ	FRM_rbuf_0;		/**< Instance of FRM_rbuf function block. */
} InternalPara_type;

/** Reverse the byte order.
	A total number of \c length bytes of \c pSrc will by reversely written to \c pDest.
*/
void ReverseBytes(USINT* pSrc, USINT* pDest, UINT length);

/** Generate TRF7960 struct from ISO15693 struct.
*/
void GetTRFFromISO(TRF7960_Frame_type* pTRF, ISO15693_Frame_type* pISO, USINT LengthPara);

/** Get raw TRF7960 Frame from struct.
*/
void GetTRFRaw(USINT* pDest, TRF7960_Frame_type* pSrcFrame);

/** Initialize TRF7960 Frame with the constant values.
*/
void InitTRFFrame(TRF7960_Frame_type* pFrame);

/** Add one byte in hex notation to the given address, return the address after adding for recursove calls. 
*/
USINT* AddHex(USINT* pDest, USINT val);

/** Get one byte from hex notation (2 chars, for example "7F").
*/
USINT Hex2Byte(USINT* pHex);

/** Reverse a string.
*/
void strreverse(STRING* pBeg, STRING* pEnd);

/** ASCII to integer conversion.
*/
UDINT myatoi(STRING* pStr);

/** integer to ASCII conversion.
*/
void myitoa(INT value, STRING* str);

/** Simple regexp evaluator to validate answers from transponder.
*/
BOOL RegExpMatch(STRING* pStr, STRING* pRegExp);


/** \endcond */


/* ---------------------------------------------------------------------------------------------------------*/
/* Doxygen documentation: Main page  */
/* ---------------------------------------------------------------------------------------------------------*/
/** \mainpage General Information

	The TP15693 library can be used to program the ISO15693-compatible transponder reader MATERIALNUMBER, which must be connected via
	a serial interface.<BR>
	The international standard ISO15693 Part 1 defines the physical characteristics of the contactless integrated circuit cards, Part 2
	defines the air interface (modulation, data coding, ...) and Part 3 defines the transmission protocol between the transponder
	reader and the chipcard.<BR>
	<BR>
	Call the function block TPInit() in the init subprogram of your task to perform all necessary initialization procedures (memory
	allocation, initialization of the serial interface). You receive an ident which later must be passed on to all other function blocks.
	Call the function block TPInfo() to determine whether a chipcard is in range and to get basic information about  the card, such as the
	card's unique identifier UID and memory structure.<BR>
	Use the function blocks TPRead() and TPWrite() if you want to access the memory byte-by-byte.
	Use the function blocks TPReadM() and TPWriteM() ifor a much faster access block-by-block.
	Call the function block TPRaw() if you need to submit a raw command (to use functionality that is not supported by this library, such as
	locking of blocks).	The serial communication protocol for raw commands is described in the ISO15693 Part 3. The most important lists and
	tables of this international standard are summarized on page \ref page_ISO "ISO15693 Reference".<BR>
	After calling the function block TPInfo() and receiving a positive answer (chipcard in range) it is assumed that the chipcard is
	neither changed nor removed. If one of the other function blocks detects a missing or different chipcard, the error
	\ref tpERR_CARD_REMOVED	is returned. TPInfo() must be called again to determine the new chipcard data.<BR>
	<BR>
	It is important to call all function blocks in the same task class. Only one function block can be enabled at a time.<BR>
	<BR>
	The error codes are listed in chapter \ref grp_ErrorNumbers, apart from these library-specific error codes the function blocks might also
	return error codes of the DVFrame library. Refer to the AutomationStudio library documentation of the DVFrame library. 
	
	\subpage page_version "Version History"<BR>
	\subpage page_system "System Requirements"<BR>
	\subpage page_glossary "Glossary"<BR>
	\subpage page_timing "Timing Diagrams"<BR>
	\subpage page_ISO "ISO15693 Reference"<BR>
	\subpage page_restrictions "Restrictions"<BR>
*/

/* ---------------------------------------------------------------------------------------------------------*/
/* Doxygen documentation: Page Version history  */
/* ---------------------------------------------------------------------------------------------------------*/
/** \page page_version Version History
	<table border="1" width="100%">
		<tr bgcolor="#f0f0f0" align="left">
			<th  width="20%">Version</th>
			<th  width="30%">Date</th>
			<th  width="50%">Comment</th>
		</tr>
	 	<tr>
			<td>V1.00.0</td>
			<td>11 Dec 2008</td>
			<td>First Version.</td>
		</tr>
	 	<tr>
			<td>V1.01.0</td>
			<td>15 Jan 2009</td>
			<td>Added function blocks TPReadM() and TPWriteM(). Speed of TPRead() and TPWrite() calls is doubled, because
				directly after receiving a response the next read/write request is submitted still in the same function call.</td>
		</tr>
	 	<tr>
			<td>V1.02.0</td>
			<td>26 Jan 2009</td>
			<td>Added function block TPDiag(). Documentation extended.</td>
		</tr>
	</table>

*/

/* ---------------------------------------------------------------------------------------------------------*/
/* Doxygen documentation: Page System Requirements  */
/* ---------------------------------------------------------------------------------------------------------*/
/** \page page_system System Requirements
	<table border="1" width="100%" bgcolor="#f0f0f0">
		<tr>
			<td  width="50%"><b>Supported Platforms</b></td>
			<td  width="50%">SG4</td>
		</tr>
	 	<tr>
			<td><b>Operating System Requirments</b></td>
			<td>V2.60 or higher</td>
		</tr>
	 	<tr>
			<td><b>Additional Libraries</b></td>
			<td>DVFrame</td>
		</tr>
	</table>

	\attention The function blocks of this library must all be called in the same taskclass.
*/

/* ---------------------------------------------------------------------------------------------------------*/
/* Doxygen documentation: Restrictions  */
/* ---------------------------------------------------------------------------------------------------------*/
/** \page page_restrictions Restrictions
	
	<ul>
	<li>Parallel communication to multiple chipcards (ISO15693 anticollistion sequence) is not supported.</li>
	<li>Locking of blocks is not supported.</li>
	<li>VICC states are not supported ("select" command).</li>
	<li>Reading and writing AFI (application family idenfifier) is not supported.</li>
	<li>Reading and writing DSFID (data storage format idenfifier) is not supported.</li>
	<li>USB connection is not supported.</li>
	</ul>
*/

/* ---------------------------------------------------------------------------------------------------------*/
/* Doxygen documentation: Page Glossary  */
/* ---------------------------------------------------------------------------------------------------------*/
/** \page page_glossary Glossary
	<table border="1" width="100%">
		<tr>
			<td  width="20%" bgcolor="#f0f0f0"><b>UID</b></td>
			<td>The Unique Identifier is a 64bit value that identifies every chipcard uniquely. It is set permanently by the
				chipcard manufacturer.</td>
		</tr>
		<tr>
			<td  width="20%" bgcolor="#f0f0f0"><b>Block</b></td>
			<td>The commands in the standard IS15693 assume that the physical memory of a chipcard is organized
				in blocks, each block can be of up to 256 bits. Up to 256 blocks can be addressed. This leads to a maximum memory
				capacity of up to 8 kBytes.</td>
		</tr>
		<tr>
			<td  width="20%" bgcolor="#f0f0f0"><b>Chipcard</b></td>
			<td>Refers to the RFID tag or VICC (vicinity integrated circuit card - according to ISO15693).</td>
		</tr>
		<tr>
			<td  width="20%" bgcolor="#f0f0f0"><b>Transponder reader</b></td>
			<td>Refers to the reader hardware or VCD (vicinity coupling device - according to ISO15693).</td>
		</tr>
	</table>
*/

/* ---------------------------------------------------------------------------------------------------------*/
/* Doxygen documentation: Page Timing Diagrams  */
/* ---------------------------------------------------------------------------------------------------------*/
/** \page page_timing Timing Diagrams
	
	The timing of the common parameters is the same to all function blocks of this library.
	A function block is not active when <tt>Enable=FALSE</tt>. The function block is activated by setting <tt>Enable=TRUE</tt>. The output 
	<tt>Busy=TRUE</tt> remains until the function block has finished. If the function block finished without error, <tt>Done=TRUE</tt>.
	If the function block finished with error, <tt>Error=TRUE</tt> and the error number can be found in <tt>ErrorID</tt>.
	Before calling the function block again, the function block must be reset for at least one cycle with <tt>Enable=FALSE</tt>.
	
	\attention The function blocks of this library must not be enabled parallely. Only one function block can be enabled at a time.
	There is anyway no point in calling the function blocks parallely.
	
	\image html timing_success.jpg "Fig. 01: Function block finished without error."
	\image html timing_error.jpg "Fig. 02: Error occurred."
*/

/* ---------------------------------------------------------------------------------------------------------*/
/* Doxygen documentation: Page ISO15693 Reference  */
/* ---------------------------------------------------------------------------------------------------------*/
/** \page page_ISO ISO15693 Reference
	<B>Response error code definition:</B>
	<table border="1" width="100%">
		<tr bgcolor="#f0f0f0" align="left"><th  width="20%">Error code</th><th  width="80%">Meaning</th></tr>
	 	<tr><td>0x01</td><td>The command is not supported, i.e. the request code is not recognised.</td></tr>
	 	<tr><td>0x02</td><td>The command is not recognised, for example: a format error occurred.</td></tr>
	 	<tr><td>0x03</td><td>The command option is not supported.</td></tr>
	 	<tr><td>0x0F</td><td>Error with no information given or a specific error code is not supported.</td></tr>
	 	<tr><td>0x10</td><td>The specified block is not available (does not exist).</td></tr>
	 	<tr><td>0x11</td><td>The specified block is already locked and thus cannot be locked again.</td></tr>
	 	<tr><td>0x12</td><td>The specified block is locked and its content cannot be changed.</td></tr>
	 	<tr><td>0x13</td><td>The specified block was not successfully programmed.</td></tr>
	 	<tr><td>0x14</td><td>The specified block was not successfully locked.</td></tr>
	</table>
	
	<BR>
	<B>Request flags 1 to 4 definition:</B><BR>
	<BR>
	\image html table_flags1_4.jpg
	
	<BR>
	<B>Request flags 5 to 8 definition when inventory flag is NOT set:</B><BR>
	<BR>
	\image html table_flags5_8.jpg
	
	<BR>
	<B>Request flags 5 to 8 definition when inventory flag is set:</B><BR>
	<BR>
	\image html table_flags5_8_inv.jpg

	<BR>
	<B>Response flags 1 to 8 definition:</B><BR>
	<BR>
	\image html table_respflags1_8.jpg

	<BR>
	<B>Command codes:</B><BR>
	<BR>
	\image html iso15693_command_codes.jpg

*/

/* ---------------------------------------------------------------------------------------------------------*/
/* Doxygen documentation: group definitions for the "Modules" page  */
/* ---------------------------------------------------------------------------------------------------------*/
/** \defgroup grp_TPInit TPInit() */
/** \defgroup grp_TPInfo TPInfo() */
/** \defgroup grp_TPRead TPRead() */
/** \defgroup grp_TPWrite TPWrite() */
/** \defgroup grp_TPReadM TPReadM() */
/** \defgroup grp_TPWriteM TPWriteM() */
/** \defgroup grp_TPRaw TPRaw() */
/** \defgroup grp_TPDiag TPDiag() */
/** \defgroup grp_ErrorNumbers Error Numbers */
/** \defgroup grp_Constants Constants */

/* ---------------------------------------------------------------------------------------------------------*/
/* Doxygen documentation: library error numbers contained in automatically generated .h file */
/* ---------------------------------------------------------------------------------------------------------*/
/**	\var tpERR_IDENT_NULL
	\ingroup grp_ErrorNumbers
	The Ident is NULL.
*/

/**	\var tpERR_IDENT_INVALID
	\ingroup grp_ErrorNumbers
	The Ident is invalid. A Checksum is built and stored at a certain place in the internal memory. At every function block call the
	size of the internal memory is crosschecked with the checksum entry. If it does not match, this error is returned. The Ident was
	not passed on correctly in the application, or the memory was corrupted by an external cause.
*/

/**	\var tpERR_SEMAPHORE_LOCKED
	\ingroup grp_ErrorNumbers
	Two different function blocks were enabled parallely, which is not allowed. Only one function block can be enabled at a time.
*/

/**	\var tpERR_TIMEOUT
	\ingroup grp_ErrorNumbers
	Timeout occurred when waiting for a response from transponder reader. Increase the timeout setting of function block TPInit().
	Check the serial connection between the CPU and the transponder reader. Might also be caused by malfunction of transponder reader
	hardware.
*/

/**	\var tpERR_BUFFERLEN
	\ingroup grp_ErrorNumbers
	Buffer length of internal buffers of DVFrame Library is smaller than internal frame buffers. This is a fatal software problem,
	contact developper of this library.
*/

/**	\var tpERR_INVALID_UID_RESP
	\ingroup grp_ErrorNumbers
	The response of the transponder reader after the "Get UID"-request is not a valid format. Call the function block again.
	Minor software problem, contact developper of this library.
*/

/**	\var tpERR_NO_CARD_IN_RANGE
	\ingroup grp_ErrorNumbers
	No chipcard is in range of the transponder reader. You need to call the function block TPInfo() to determine whether a chipcard
	is in range.
*/

/**	\var tpERR_NO_BLOCKINFO
	\ingroup grp_ErrorNumbers
	The information about the memory organisation (blocks) of the chipcard is not valid. Call the function block TPInfo() again to
	get the information about block number and block size of the chipcard. The chipcard maybe does not support block information.
	In this case, the chipcard cannot be used.
*/

/**	\var tpERR_INVALID_READ_RESP
	\ingroup grp_ErrorNumbers
	The response of the transponder reader after the "Read"-request is not a valid format. Call the function block again.
	Minor software problem, contact developper of this library.
*/

/**	\var tpERR_INVALID_SYSINFO_RESP
	\ingroup grp_ErrorNumbers
	The response of the transponder reader after the "Get Systeminfo"-request is not a valid format. Call the function block again.
	Minor software problem, contact developper of this library.
*/

/**	\var tpERR_CARD_REMOVED
	\ingroup grp_ErrorNumbers
	The chipcard was removed or changed since last call of TPInfo() or during the function block call. Call the function block TPInfo() again
	to determine the chipcard status and information, then perform again the read or write operation.
*/

/**	\var tpERR_MEM_EXCEEDED
	\ingroup grp_ErrorNumbers
	The specified offset and length exceed the memory capacity of the chipcard.
*/

/**	\var tpERR_ILLEGAL_LENGTH
	\ingroup grp_ErrorNumbers
	The specified length of the outgoing ISO frame exceeds the maximum frame size.
*/

/**	\var tpERR_INVALID_WRITE_RESP
	\ingroup grp_ErrorNumbers
	The response of the transponder reader after the "Write"-request is not a valid format. Call the function block again.
	Minor software problem, contact developper of this library.
*/

/**	\var tpERR_BLOCKS_EXCEEDED
	\ingroup grp_ErrorNumbers
	The specified first block and number of blocks or the multiple selection exceed the memory capacity of the chipcard.
*/

/**	\var tpERR_INVALID_READM_RESP
	\ingroup grp_ErrorNumbers
	The response of the transponder reader after the "Read multiple"-request is not a valid format. Call the function block again.
	Minor software problem, contact developper of this library.
*/

/**	\var tpERR_INVALID_WRITEM_RESP
	\ingroup grp_ErrorNumbers
	The response of the transponder reader after the "Write multiple"-request is not a valid format. Call the function block again.
	Minor software problem, contact developper of this library.
*/

/**	\var tpERR_ISOERR_SYSINFO_RESP
	\ingroup grp_ErrorNumbers
	The response of the chipcard after the "Get Systeminfo"-request indicated an error according to ISO15693 specification. The ISO error codes
	can be found on page \ref page_ISO "ISO15693 Reference". The ISO error code is added to this error number <tt>tpERR_ISOERR_SYSINFO_RESP + ISOErrorCode</tt>.
	Call the function block again. If the error continues to exist, the chipcard is not working with this library or does not support
	all the commands needed.
*/

/**	\var tpERR_ISOERR_READ_RESP
	\ingroup grp_ErrorNumbers
	The response of the chipcard after the "Read"-request indicated an error according to ISO15693 specification. The ISO error codes
	can be found on page \ref page_ISO "ISO15693 Reference". The ISO error code is added to this error number <tt>tpERR_ISOERR_READ_RESP + ISOErrorCode</tt>.
	Call the function block again. If the error continues to exist, the chipcard is not working with this library or does not support
	all the commands needed.
*/

/**	\var tpERR_ISOERR_WRITE_RESP
	\ingroup grp_ErrorNumbers
	The response of the chipcard after the "Write"-request indicated an error according to ISO15693 specification. The ISO error codes
	can be found on page \ref page_ISO "ISO15693 Reference". The ISO error code is added to this error number <tt>tpERR_ISOERR_WRITE_RESP + ISOErrorCode</tt>.
	Call the function block again. If the error continues to exist, the chipcard is not working with this library or does not support
	all the commands needed.
*/

/**	\var tpERR_ISOERR_READM_RESP
	\ingroup grp_ErrorNumbers
	The response of the chipcard after the "Read multiple"-request indicated an error according to ISO15693 specification. The ISO error codes
	can be found on page \ref page_ISO "ISO15693 Reference". The ISO error code is added to this error number <tt>tpERR_ISOERR_READM_RESP + ISOErrorCode</tt>.
	Call the function block again. If the error continues to exist, the chipcard is not working with this library or does not support
	all the commands needed.
*/

/**	\var tpERR_ISOERR_WRITEM_RESP
	\ingroup grp_ErrorNumbers
	The response of the chipcard after the "Write multiple"-request indicated an error according to ISO15693 specification. The ISO error codes
	can be found on page \ref page_ISO "ISO15693 Reference". The ISO error code is added to this error number <tt>tpERR_ISOERR_WRITEM_RESP + ISOErrorCode</tt>.
	Call the function block again. If the error continues to exist, the chipcard is not working with this library or does not support
	all the commands needed.
*/

/* ---------------------------------------------------------------------------------------------------------*/
/* Doxygen documentation: constants contained in automatically generated .h file */
/* ---------------------------------------------------------------------------------------------------------*/
/**	\var TP_STATUS_NO_CARDINRANGE 
	\ingroup grp_Constants
	No chipcard is in range of the transponder reader.
	Return value for TPInfo_typ::Status.
*/

/**	\var TP_STATUS_CARDINRANGE 
	\ingroup grp_Constants
	A chipcard is in range of the transponder reader.
	Return value for TPInfo_typ::Status.
*/

/* ---------------------------------------------------------------------------------------------------------*/
/* Doxygen documentation: example task how to use this library */
/* ---------------------------------------------------------------------------------------------------------*/
/** \example tpexample1.c
  	The usage of the function blocks TPInfo(), TPWrite() and TPRead() is demonstrated in this example task. It also shows how to poll for
	a chipcard. 
	
	\example tpexample2.c
  	The usage of the function blocks TPInfo(), TPWriteM(), TPReadM(), TPRaw() and TPDiag() is demonstrated in this example task.
  	
 */


#endif


