/******************************************************************************/
/**  \file 		twrite.c
	 \author 	krammerm, Copyright Bernecker + Rainer
	 \date 		November 18, 2008
	 \brief 	Implementation of function block TPWrite().
*/
/******************************************************************************/

#include <bur/plctypes.h>
#include <string.h>

#include "tp15693.h"
#include "tp15693_internal.h"

#define RETURN_ON_ERROR(err, errnum)											\
{																				\
	if (err)																	\
	{																			\
		inst->ErrorID = errnum;													\
		inst->Error = 1;														\
		inst->Busy = 0;															\
		pInternal->Locked = FUB_FREE;											\
		pInternal->Step = STEP_IDLE;											\
		return;																	\
	}																			\
}
																				
#define RETURN_REGULAR(condition)												\
{																				\
	if (condition)																\
	{																			\
		inst->Done = 1;															\
		inst->Busy = 0;															\
		pInternal->Locked = FUB_FREE;											\
		pInternal->Step = STEP_IDLE;											\
		return;																	\
	}																			\
}

#define RESET_INTERNAL(condition)												\
{																				\
	if (condition)																\
	{																			\
		pInternal->Status = TP_STATUS_NO_CARDINRANGE;							\
		pInternal->RSSI = 0;													\
		pInternal->BlockSize = 0;												\
		pInternal->NumBlocks = 0;												\
		for (i = 0; i < UID_SIZE; i++) pInternal->UID[i] = 0;					\
	}																			\
}

/** \struct TPWrite_typ
	\ingroup grp_TPWrite
     Input and output parameters of function block TPWrite().
*/

/**	\var TPWrite_typ::Ident
	\copybrief TPInfo_typ::Ident
	\copydetails TPInfo_typ::Ident
*/

/**	\var TPWrite_typ::Enable
	\copybrief TPInfo_typ::Enable
	\copydetails TPInfo_typ::Enable
*/

/**	\var TPWrite_typ::Error
	\copybrief TPInfo_typ::Error
	\copydetails TPInfo_typ::Error
*/

/**	\var TPWrite_typ::ErrorID
	\copybrief TPInfo_typ::ErrorID
	\copydetails TPInfo_typ::ErrorID
*/

/**	\var TPWrite_typ::Done
	\copybrief TPInfo_typ::Done
	\copydetails TPInfo_typ::Done
*/

/**	\var TPWrite_typ::Busy
	\copybrief TPInfo_typ::Busy
	\copydetails TPInfo_typ::Busy
*/

/**	\var TPWrite_typ::pSource
	<B>[IN]</B> Memory from which the data to be written is copied.
*/

/**	\var TPWrite_typ::Offset
	<B>[IN]</B> Offset in the memory of the chipcard [bytes].
*/

/**	\var TPWrite_typ::Len
	<B>[IN]</B> Length of the data to be written [bytes].
*/

/** Write memory to transponder.
	The physical memory on a chipcard is organized in blocks, however using this function block memory can be accessed byte-by-byte.
	\ingroup grp_TPWrite
*/
void TPWrite(struct TPWrite* inst)
{
	InternalPara_type* pInternal;
	TRF7960_Frame_type TRFFrame;
	ISO15693_Frame_type ISOFrame;
	UINT i;
	STRING	strRegExp[100];
	STRING	strTmp[100];
	USINT	offs, len;

	pInternal = (InternalPara_type*)inst->Ident;
	if (pInternal == NULL )													{ inst->Error = 1; inst->ErrorID = tpERR_IDENT_NULL; 		return; }	
	if (pInternal->ChkSum != sizeof(InternalPara_type))						{ inst->Error = 1; inst->ErrorID = tpERR_IDENT_INVALID; 	return; }

	if (inst->Enable == 0)
	{
		inst->Busy = 0;
		inst->Done = 0;
		inst->Error = 0;
		inst->ErrorID = 0;
		pInternal->Locked = FUB_FREE;
		return;
	}
	else if (inst->Done == 1 || inst->Error == 1)	/* function block has finished, must be reset once with Enable = 0 ! */
	{
		return;	
	}
	
	if (pInternal->Locked != FUB_TPWRITE && pInternal->Locked != FUB_FREE)	{ inst->Error = 1; inst->ErrorID = tpERR_SEMAPHORE_LOCKED; 	return; }
	if (pInternal->Status == TP_STATUS_NO_CARDINRANGE)						{ inst->Error = 1; inst->ErrorID = tpERR_NO_CARD_IN_RANGE;	return; }
	if (pInternal->NumBlocks == 0 || pInternal->BlockSize == 0)				{ inst->Error = 1; inst->ErrorID = tpERR_NO_BLOCKINFO;		return; }
	if (inst->Offset+inst->Len > pInternal->NumBlocks*pInternal->BlockSize)	{ inst->Error = 1; inst->ErrorID = tpERR_MEM_EXCEEDED;		return; }
	if (inst->Len == 0)														{ inst->Done = 1; 											return; }

	if (pInternal->Step == STEP_IDLE)
	{
		pInternal->Locked = FUB_TPWRITE;
		inst->Busy = 1;
		pInternal->BlockCount = inst->Offset / pInternal->BlockSize;
		pInternal->SourceOffset = 0;
		if (inst->Offset % pInternal->BlockSize != 0)	/* 1 incomplete block at the beginning */
		{
			pInternal->BlockIncomplBeg = 1;
			/* 1 incomplete block at beginning and a complete block at the end */
			if ((inst->Len + (inst->Offset % pInternal->BlockSize)) % pInternal->BlockSize == 0)
			{
				pInternal->BlockLast = pInternal->BlockCount + inst->Len / pInternal->BlockSize + 0;		
				pInternal->BlockIncomplEnd = 0;
			}
			/* 1 incomplete block at the beginning and no other block */
			else if (inst->Len + (inst->Offset % pInternal->BlockSize) < pInternal->BlockSize)					
			{
				pInternal->BlockLast = pInternal->BlockCount;												
				pInternal->BlockIncomplEnd = 1;
			}
			/* 1 incomplete block at beginning and 1 incomplete block at end */
			else 
			{
				pInternal->BlockLast = pInternal->BlockCount + (inst->Offset % pInternal->BlockSize + inst->Len) / pInternal->BlockSize;		
				pInternal->BlockIncomplEnd = 1;
			}
				
		}
		else	/* no incomplete block at the beginning */
		{
			pInternal->BlockIncomplBeg = 0;
			if (inst->Len % pInternal->BlockSize == 0)
			{
				pInternal->BlockLast = pInternal->BlockCount + inst->Len / pInternal->BlockSize - 1;		/* complete blocks at beginning and at end */
				pInternal->BlockIncomplEnd = 0;
			}
			else
			{
				pInternal->BlockLast = pInternal->BlockCount + inst->Len / pInternal->BlockSize + 0;		/* incomplete block at end */
				pInternal->BlockIncomplEnd = 1;
			}
		}
		
		if (pInternal->BlockIncomplBeg == 1)		pInternal->Step = STEP_TPWRITE_READFIRST_REQ;
		else if (pInternal->BlockIncomplEnd == 1)	pInternal->Step = STEP_TPWRITE_READLAST_REQ;
		else										pInternal->Step = STEP_TPWRITE_WRITE_REQ;

	}

	/* --- timeout monitoring --- */
	if (pInternal->Step != STEP_IDLE && pInternal->StepOld == pInternal->Step)
	{
		pInternal->cntTime += pInternal->CycleTime;
		RETURN_ON_ERROR((pInternal->cntTime > pInternal->Timeout), tpERR_TIMEOUT);
	}
	else
	{
		pInternal->cntTime = 0;
		pInternal->StepOld = pInternal->Step;
	}
	
	switch (pInternal->Step)
	{
		case STEP_TPWRITE_READFIRST_REQ:
			/* --- compose the frame to be sent --- */
			ISOFrame.Flags = ISO_FLAG_DATA_RATE_HIGH | ISO_FLAG_ADDRESS;
			ISOFrame.Cmd = ISO_CMD_READ_SINGLE_BLOCK;
			for (i = 0; i < UID_SIZE; i++)
				ISOFrame.Parameters[i] = pInternal->UID[i];		/* UID for addressed mode. */
			ISOFrame.Parameters[i] = pInternal->BlockCount;
			GetTRFFromISO(&TRFFrame, &ISOFrame, UID_SIZE+1);
			GetTRFRaw(pInternal->FrameOut, &TRFFrame);

			/* --- send the frame --- */
			pInternal->FRM_write_0.buflng = TRFFrame.PacketLength*2;
			FRM_write(&pInternal->FRM_write_0);
			RETURN_ON_ERROR((pInternal->FRM_write_0.status != 0), pInternal->FRM_write_0.status);

			pInternal->Step = STEP_TPWRITE_READFIRST_RESP;
		break;

		case STEP_TPWRITE_READFIRST_RESP:
			/* --- read incoming frame --- */
			FRM_read(&pInternal->FRM_read_0);
			if (pInternal->FRM_read_0.status == frmERR_NOINPUT)
				break;	/* try again next cycle */
			RETURN_ON_ERROR((pInternal->FRM_read_0.status != 0), pInternal->FRM_read_0.status);
			RETURN_ON_ERROR((pInternal->FRM_read_0.buflng > sizeof(pInternal->FrameIn)), tpERR_BUFFERLEN);
			memcpy(pInternal->FrameIn, (void*)pInternal->FRM_read_0.buffer, pInternal->FRM_read_0.buflng);
			memset(pInternal->FrameIn + pInternal->FRM_read_0.buflng, 0, sizeof(pInternal->FrameIn) - pInternal->FRM_read_0.buflng);
			
			/* --- release input buffer --- */
			pInternal->FRM_rbuf_0.buffer = pInternal->FRM_read_0.buffer;
			pInternal->FRM_rbuf_0.buflng = pInternal->FRM_read_0.buflng;
			FRM_rbuf(&pInternal->FRM_rbuf_0);
			RETURN_ON_ERROR((pInternal->FRM_rbuf_0.status != 0), pInternal->FRM_rbuf_0.status);

			/* --- evaluate response --- */
			RESET_INTERNAL(RegExpMatch((STRING*)pInternal->FrameIn, "[]\r\n") == 1);			/* no card in range */
			RESET_INTERNAL(RegExpMatch((STRING*)pInternal->FrameIn, ".*&{00&}.*\r\n") == 1);	/* '{00}'	data transmission conflict, bad card position */
			RESET_INTERNAL(RegExpMatch((STRING*)pInternal->FrameIn, ".*z.*\r\n") == 1);			/* 'z'		data transmission conflict, bad card position */
			RETURN_ON_ERROR(pInternal->Status == TP_STATUS_NO_CARDINRANGE, tpERR_CARD_REMOVED);

			RETURN_ON_ERROR((RegExpMatch((STRING*)pInternal->FrameIn, "[01##]\r\n") == 1), tpERR_ISOERR_READ_RESP + Hex2Byte(&pInternal->FrameIn[3]));
			strcpy(strRegExp, "[#{");
			myitoa(2+pInternal->BlockSize*2, strTmp);
			strcat(strRegExp, strTmp);
			strcat(strRegExp, "}]\r\n");
			RETURN_ON_ERROR((RegExpMatch((STRING*)pInternal->FrameIn, strRegExp) == 0), tpERR_INVALID_READ_RESP);

			/* --- copy block data to internal memory --- */
			for (i = 0; i < pInternal->BlockSize; i++)
				pInternal->FirstBlock[i] = Hex2Byte(&pInternal->FrameIn[3+2*i]);

			/* --- replace parts of block data with the bytes to write within the first block --- */
			offs = inst->Offset % pInternal->BlockSize;
			/* if: 1 incomplete block at the beginning and no other block */
			/* else: 1 incomplete block at the beginning and some following blocks */
			if (pInternal->BlockLast == pInternal->BlockCount)		len = inst->Len;
			else													len = pInternal->BlockSize - offs;
			for (i = 0; i < len; i++)
				pInternal->FirstBlock[offs + i] = ((USINT*)inst->pSource)[i];
			
			pInternal->SourceOffset += pInternal->BlockSize - inst->Offset % pInternal->BlockSize;	/* adjust for the write steps */
			
			/* --- set next step --- */
			if (pInternal->BlockIncomplEnd == 1 && pInternal->BlockLast != pInternal->BlockCount)
				pInternal->Step = STEP_TPWRITE_READLAST_REQ;
			else
				pInternal->Step = STEP_TPWRITE_WRITE_REQ;
		break;

		case STEP_TPWRITE_READLAST_REQ:
			/* --- compose the frame to be sent --- */
			ISOFrame.Flags = ISO_FLAG_DATA_RATE_HIGH | ISO_FLAG_ADDRESS;
			ISOFrame.Cmd = ISO_CMD_READ_SINGLE_BLOCK;
			for (i = 0; i < UID_SIZE; i++)
				ISOFrame.Parameters[i] = pInternal->UID[i];		/* UID for addressed mode. */
			ISOFrame.Parameters[i] = pInternal->BlockLast;
			GetTRFFromISO(&TRFFrame, &ISOFrame, UID_SIZE+1);
			GetTRFRaw(pInternal->FrameOut, &TRFFrame);

			/* --- send the frame --- */
			pInternal->FRM_write_0.buflng = TRFFrame.PacketLength*2;
			FRM_write(&pInternal->FRM_write_0);
			RETURN_ON_ERROR((pInternal->FRM_write_0.status != 0), pInternal->FRM_write_0.status);

			pInternal->Step = STEP_TPWRITE_READLAST_RESP;
		break;

		case STEP_TPWRITE_READLAST_RESP:
			/* --- read incoming frame --- */
			FRM_read(&pInternal->FRM_read_0);
			if (pInternal->FRM_read_0.status == frmERR_NOINPUT)
				break;	/* try again next cycle */
			RETURN_ON_ERROR((pInternal->FRM_read_0.status != 0), pInternal->FRM_read_0.status);
			RETURN_ON_ERROR((pInternal->FRM_read_0.buflng > sizeof(pInternal->FrameIn)), tpERR_BUFFERLEN);
			memcpy(pInternal->FrameIn, (void*)pInternal->FRM_read_0.buffer, pInternal->FRM_read_0.buflng);
			memset(pInternal->FrameIn + pInternal->FRM_read_0.buflng, 0, sizeof(pInternal->FrameIn) - pInternal->FRM_read_0.buflng);
			
			/* --- release input buffer --- */
			pInternal->FRM_rbuf_0.buffer = pInternal->FRM_read_0.buffer;
			pInternal->FRM_rbuf_0.buflng = pInternal->FRM_read_0.buflng;
			FRM_rbuf(&pInternal->FRM_rbuf_0);
			RETURN_ON_ERROR((pInternal->FRM_rbuf_0.status != 0), pInternal->FRM_rbuf_0.status);

			/* --- evaluate response --- */
			RESET_INTERNAL(RegExpMatch((STRING*)pInternal->FrameIn, "[]\r\n") == 1);			/* no card in range */
			RESET_INTERNAL(RegExpMatch((STRING*)pInternal->FrameIn, ".*&{00&}.*\r\n") == 1);	/* '{00}'	data transmission conflict, bad card position */
			RESET_INTERNAL(RegExpMatch((STRING*)pInternal->FrameIn, ".*z.*\r\n") == 1);			/* 'z'		data transmission conflict, bad card position */
			RETURN_ON_ERROR(pInternal->Status == TP_STATUS_NO_CARDINRANGE, tpERR_CARD_REMOVED);

			RETURN_ON_ERROR((RegExpMatch((STRING*)pInternal->FrameIn, "[01##]\r\n") == 1), tpERR_ISOERR_READ_RESP + Hex2Byte(&pInternal->FrameIn[3]));
			strcpy(strRegExp, "[#{");
			myitoa(2+pInternal->BlockSize*2, strTmp);
			strcat(strRegExp, strTmp);
			strcat(strRegExp, "}]\r\n");
			RETURN_ON_ERROR((RegExpMatch((STRING*)pInternal->FrameIn, strRegExp) == 0), tpERR_INVALID_READ_RESP);

			/* --- copy block data to internal memory --- */
			for (i = 0; i < pInternal->BlockSize; i++)
				pInternal->LastBlock[i] = Hex2Byte(&pInternal->FrameIn[3+2*i]);

			/* --- replace parts of block data with the bytes to write within the last block --- */
			offs = inst->Len - (inst->Offset + inst->Len) % pInternal->BlockSize;
			len = (inst->Offset + inst->Len) % pInternal->BlockSize;
			for (i = 0; i < len; i++)
				pInternal->LastBlock[i] = ((USINT*)inst->pSource)[offs + i];
				
			/* --- set next step --- */
			pInternal->Step = STEP_TPWRITE_WRITE_REQ;
		break;

		case STEP_TPWRITE_WRITE_RESP:
			/* --- read incoming frame --- */
			FRM_read(&pInternal->FRM_read_0);
			if (pInternal->FRM_read_0.status == frmERR_NOINPUT)
				break;	/* try again next cycle */
			RETURN_ON_ERROR((pInternal->FRM_read_0.status != 0), pInternal->FRM_read_0.status);
			RETURN_ON_ERROR((pInternal->FRM_read_0.buflng > sizeof(pInternal->FrameIn)), tpERR_BUFFERLEN);
			memcpy(pInternal->FrameIn, (void*)pInternal->FRM_read_0.buffer, pInternal->FRM_read_0.buflng);
			memset(pInternal->FrameIn + pInternal->FRM_read_0.buflng, 0, sizeof(pInternal->FrameIn) - pInternal->FRM_read_0.buflng);
			
			/* --- release input buffer --- */
			pInternal->FRM_rbuf_0.buffer = pInternal->FRM_read_0.buffer;
			pInternal->FRM_rbuf_0.buflng = pInternal->FRM_read_0.buflng;
			FRM_rbuf(&pInternal->FRM_rbuf_0);
			RETURN_ON_ERROR((pInternal->FRM_rbuf_0.status != 0), pInternal->FRM_rbuf_0.status);

			/* --- evaluate response --- */
			RESET_INTERNAL(RegExpMatch((STRING*)pInternal->FrameIn, "[]\r\n") == 1);			/* no card in range */
			RESET_INTERNAL(RegExpMatch((STRING*)pInternal->FrameIn, ".*&{00&}.*\r\n") == 1);	/* '{00}'	data transmission conflict, bad card position */
			RESET_INTERNAL(RegExpMatch((STRING*)pInternal->FrameIn, ".*z.*\r\n") == 1);			/* 'z'		data transmission conflict, bad card position */
			RETURN_ON_ERROR(pInternal->Status == TP_STATUS_NO_CARDINRANGE, tpERR_CARD_REMOVED);

			RETURN_ON_ERROR((RegExpMatch((STRING*)pInternal->FrameIn, "[01##]\r\n") == 1), tpERR_ISOERR_WRITE_RESP + Hex2Byte(&pInternal->FrameIn[3]));
			RETURN_ON_ERROR((RegExpMatch((STRING*)pInternal->FrameIn, "[00]\r\n") == 0), tpERR_INVALID_WRITE_RESP);

			if (pInternal->BlockIncomplBeg == 1) 	pInternal->BlockIncomplBeg = 0;
			else									pInternal->SourceOffset += pInternal->BlockSize;

			pInternal->BlockCount++;
			RETURN_REGULAR(pInternal->BlockCount > pInternal->BlockLast);
			pInternal->Step = STEP_TPWRITE_WRITE_REQ;
		/* break; */	/* no break so that next read request is immediately sent */

		case STEP_TPWRITE_WRITE_REQ:
			/* --- compose the frame to be sent --- */
			ISOFrame.Flags = ISO_FLAG_DATA_RATE_HIGH | ISO_FLAG_ADDRESS | ISO_FLAG_OPTION;	/* most cards require option flag! */
			ISOFrame.Cmd = ISO_CMD_WRITE_SINGLE_BLOCK;
			for (i = 0; i < UID_SIZE; i++)
				ISOFrame.Parameters[i] = pInternal->UID[i];		/* UID for addressed mode. */
			ISOFrame.Parameters[i] = pInternal->BlockCount;
			i++;
			
			if (pInternal->BlockIncomplBeg == 1)
				memcpy(&ISOFrame.Parameters[i], pInternal->FirstBlock, pInternal->BlockSize);
			else if (pInternal->BlockCount == pInternal->BlockLast && pInternal->BlockIncomplEnd == 1)
				memcpy(&ISOFrame.Parameters[i], pInternal->LastBlock, pInternal->BlockSize);
			else
				memcpy(&ISOFrame.Parameters[i], &((USINT*)inst->pSource)[pInternal->SourceOffset], pInternal->BlockSize);
			
			GetTRFFromISO(&TRFFrame, &ISOFrame, UID_SIZE + 1 + pInternal->BlockSize);
			GetTRFRaw(pInternal->FrameOut, &TRFFrame);

			/* --- send the frame --- */
			pInternal->FRM_write_0.buflng = TRFFrame.PacketLength*2;
			FRM_write(&pInternal->FRM_write_0);
			RETURN_ON_ERROR((pInternal->FRM_write_0.status != 0), pInternal->FRM_write_0.status);

			pInternal->Step = STEP_TPWRITE_WRITE_RESP;
		break;
		
	
		default:
		break;
	}
}
