(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: TP15693
 * File: TP15693.fun
 * Author: krammerm
 * Created: November 13, 2008
 ********************************************************************
 * Functions and function blocks of library TP15693
 ********************************************************************)

FUNCTION_BLOCK TPInfo
	VAR_INPUT
		Enable : BOOL;
		Ident : UDINT;
	END_VAR
	VAR_OUTPUT
		Done : BOOL;
		Busy : BOOL;
		Error : BOOL;
		ErrorID : UDINT;
		Status : USINT;
		UID : ARRAY[0..7] OF USINT;
		RSSI : USINT;
		BlockSize : USINT;
		NumBlocks : USINT;
		MemSize : UINT;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK TPDiag
	VAR_INPUT
		Enable : BOOL;
		Ident : UDINT;
	END_VAR
	VAR_OUTPUT
		Error : BOOL;
		ErrorID : UDINT;
		DiagInfo : DiagInfo_typ;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK TPInit
	VAR_INPUT
		pDevice : UDINT;
		CycleTime : UDINT;
		Timeout : UDINT;
	END_VAR
	VAR_OUTPUT
		Error : BOOL;
		ErrorID : UDINT;
		Ident : UDINT;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK TPRead
	VAR_INPUT
		Enable : BOOL;
		Ident : UDINT;
		pDestination : UDINT;
		Offset : UINT;
		Len : UINT;
	END_VAR
	VAR_OUTPUT
		Done : BOOL;
		Busy : BOOL;
		Error : BOOL;
		ErrorID : UDINT;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK TPReadM
	VAR_INPUT
		Enable : BOOL;
		Ident : UDINT;
		pDestination : UDINT;
		FirstBlock : USINT;
		NumBlocks : USINT;
		Multiple : USINT;
	END_VAR
	VAR_OUTPUT
		Done : BOOL;
		Busy : BOOL;
		Error : BOOL;
		ErrorID : UDINT;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK TPWriteM
	VAR_INPUT
		Enable : BOOL;
		Ident : UDINT;
		pSource : UDINT;
		FirstBlock : USINT;
		NumBlocks : USINT;
		Multiple : USINT;
	END_VAR
	VAR_OUTPUT
		Done : BOOL;
		Busy : BOOL;
		Error : BOOL;
		ErrorID : UDINT;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK TPRaw
	VAR_INPUT
		Enable : BOOL;
		Ident : UDINT;
		pFrameIn : UDINT;
		pFrameOut : UDINT;
		LenOut : UINT;
	END_VAR
	VAR_OUTPUT
		LenIn : UINT;
		Done : BOOL;
		Busy : BOOL;
		Error : BOOL;
		ErrorID : UDINT;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK TPWrite
	VAR_INPUT
		Enable : BOOL;
		Ident : UDINT;
		pSource : UDINT;
		Offset : UINT;
		Len : UINT;
	END_VAR
	VAR_OUTPUT
		Done : BOOL;
		Busy : BOOL;
		Error : BOOL;
		ErrorID : UDINT;
	END_VAR
END_FUNCTION_BLOCK
