(************************************************************************************************************************)
(* Object name:                                                                                                         *)
(* Author:                                                                                                              *)
(* Site:                                                                                                                *)
(* Created:                                                                                                             *)
(* Restriction:                                                                                                         *)
(* Description:                                                                                                         *)
(*                                                                                                                      *)
(*----------------------------------------------------------------------------------------------------------------------*)
(* Additional information:                                                                                              *)
(*                                                                                                                      *)
(* No additional information available.                                                                                 *)
(*                                                                                                                      *)
(*----------------------------------------------------------------------------------------------------------------------*)
(* Version 3.00.0  18-jan-2012  Samuel Otto                     Development tool: B&R Automation Studio V3.0.90.19 SP01 *)
(*                                                                                                                      *)
(* Cleaned up this revision history and set version to V3.00.0                                                          *)
(*--------------------------------------------------------------------------------------------------------------------- *)
(************************************************************************************************************************)

FUNCTION_BLOCK Encode
IF (Enable = 1) THEN

	Status := 65535;

	(* Encode inserted Data (SHL) *)
 	FOR cntPw := 0 TO 8 DO
       	brsstrcpy(ADR(tmpstr),ADR(Data[cntPw]));
       	FOR cntLen := 0 TO 9 DO
			pChar ACCESS (ADR(tmpstr) + cntLen);
			IF (pChar <> 0) THEN
				brsitoa((pChar*8), ADR(Str));
				IF (cntLen = 0) THEN
					brsstrcpy(ADR(tmpstr1),ADR(Str));
				ELSE
					brsstrcat(ADR(tmpstr1),ADR(Str));
				END_IF
			ELSE
				EXIT;
			END_IF
       	END_FOR
		brsstrcpy(ADR(PwRol[cntPw]),ADR(tmpstr1));
 	END_FOR
	Status := 0;
	Enable := 0;
END_IF

END_FUNCTION_BLOCK
