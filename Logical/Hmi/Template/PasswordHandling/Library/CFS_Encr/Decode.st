(************************************************************************************************************************)
(* Object name:                                                                                                         *)
(* Author:                                                                                                              *)
(* Site:                                                                                                                *)
(* Created:                                                                                                             *)
(* Restriction:                                                                                                         *)
(* Description:                                                                                                         *)
(*                                                                                                                      *)
(*----------------------------------------------------------------------------------------------------------------------*)
(* Additional information:                                                                                              *)
(*                                                                                                                      *)
(* No additional information available.                                                                                 *)
(*                                                                                                                      *)
(*----------------------------------------------------------------------------------------------------------------------*)
(* Version 3.00.0  18-jan-2012  Samuel Otto                     Development tool: B&R Automation Studio V3.0.90.19 SP01 *)
(*                                                                                                                      *)
(* Cleaned up this revision history and set version to V3.00.0                                                          *)
(*--------------------------------------------------------------------------------------------------------------------- *)
(************************************************************************************************************************)

FUNCTION_BLOCK Decode
IF (Enable = 1) THEN

	Status := 65535;

	(* Encode inserted Data (SHR) *)
	FOR cntPw := 0 TO 8 DO
		brsstrcpy(ADR(tmpstr),ADR(PwRol[cntPw]));
		brsstrcpy(ADR(Str1),ADR(''));
		Offset := 3;
		FOR cntLen := 0 TO 9 DO
			brsmemcpy(ADR(Str),(ADR(tmpstr) + (cntLen * Offset)),Offset);

			res := brsstrcmp(ADR(Str),ADR(''));
			IF (res <> 0) THEN

				tmpVal  := brsatoi(ADR(Str));
				tmpVal1 := DINT_TO_USINT(tmpVal / 8);
				pChar ACCESS (ADR(Str1));
				pChar := tmpVal1;
				IF (cntLen = 0) THEN
					brsstrcpy(ADR(tmpStr2),ADR(Str1));
				ELSE
					brsstrcat(ADR(tmpStr2),ADR(Str1));
				END_IF
			ELSE
				EXIT;
			END_IF
		END_FOR
    	brsstrcpy(ADR(Data[cntPw]),ADR(tmpStr2));
    END_FOR

    Status := 0;
	Enable := 0;

END_IF
END_FUNCTION_BLOCK
