(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: TP9030ISO
 * File: TP9030ISO.typ
 * Author: A. Festerling
 * Created: October 19, 2017
 ********************************************************************
 * Data types of library TP9030ISO
 ********************************************************************)
(*//////////////////////////////////////////// TP9030Init ////////////////////////////////////////////////////////////////////////////////*)

TYPE
	TP9030Init_Type : 	STRUCT 
		FUB : TP9030InitFUB_Type;
		Para : TP9030InitPara_Type;
	END_STRUCT;
	TP9030InitFUB_Type : 	STRUCT 
		FRM_xopen_0 : FRM_xopen;
		FRM_close_0 : FRM_close;
	END_STRUCT;
	TP9030InitPara_Type : 	STRUCT 
		XOpenConfig : XOPENCONFIG;
		FirstCycle : BOOL; (*First execution of the FUB => Flag to reset FRM_xopen*)
	END_STRUCT;
END_TYPE

(*//////////////////////////////////////////// TP9030Info ////////////////////////////////////////////////////////////////////////////////*)

TYPE
	TP9030Info_Type : 	STRUCT 
		FUB : TP9030InfoFUB_Type;
		Para : TP9030InfoPara_Type;
		Status : TP9030InfoStatus_Type;
		StepInfo : StepInfo_enum;
	END_STRUCT;
	TP9030InfoFUB_Type : 	STRUCT 
		FRM_read_0 : FRM_read;
		FRM_write_0 : FRM_write;
		FRM_rbuf_0 : FRM_rbuf;
		FRM_robuf_0 : FRM_robuf;
		GetTPErrors_0 : GetTPErrors;
	END_STRUCT;
	TP9030InfoPara_Type : 	STRUCT 
		FrameIn : ARRAY[0..1000]OF USINT;
		FrameOut : ARRAY[0..255]OF USINT;
		CntInfo : UDINT;
	END_STRUCT;
	TP9030InfoStatus_Type : 	STRUCT 
		StartOfUID : UDINT;
		PiccRemove : STRING[255];
		PiccRemoveCatched : BOOL;
		LoopCounter : USINT;
		TPErrorActiveOld : BOOL;
	END_STRUCT;
	StepInfo_enum : 
		(
		INIT,
		WAIT_TAG_DETECTED,
		GET_UID,
		REQUEST_SYSTEM_INFO,
		GET_SYSTEMINFO,
		DECODE_SYSTEMINFO,
		WAIT_TAG_REMOVE,
		ERROR_READ,
		ERROR_WRITE,
		ERROR_TP
		);
END_TYPE

(*//////////////////////////////////////////// TP9030ReadBlocks ////////////////////////////////////////////////////////////////////////////////*)

TYPE
	TP9030ReadBlocks_Type : 	STRUCT 
		FUB : TP9030ReadBlocksFUB_Type;
		Para : TP9030ReadBlocksPara_Type;
		Status : TP9030ReadBlocksStatus_Type;
		StepReadBlocks : StepReadBlocks_enum;
	END_STRUCT;
	TP9030ReadBlocksFUB_Type : 	STRUCT 
		FRM_read_0 : FRM_read;
		FRM_write_0 : FRM_write;
		FRM_rbuf_0 : FRM_rbuf;
		FRM_robuf_0 : FRM_robuf;
		GetTPErrors_0 : GetTPErrors;
	END_STRUCT;
	TP9030ReadBlocksPara_Type : 	STRUCT 
		FrameIn : ARRAY[0..1000]OF USINT;
		FrameOut : ARRAY[0..255]OF USINT;
		StrStartBlock : STRING[10];
		StrLastBlock : STRING[10];
		BlockData : ARRAY[0..500]OF USINT;
	END_STRUCT;
	TP9030ReadBlocksStatus_Type : 	STRUCT 
		MessageNotFinished : BOOL;
		Offset : UDINT;
		CntCopy : UDINT;
		TPErrorActiveOld : BOOL;
	END_STRUCT;
	StepReadBlocks_enum : 
		(
		READ_BLOCKS_INIT,
		REQUEST_BLOCKS,
		GET_BLOCKS,
		DECODE_BLOCKS,
		ALL_BLOCKS_DONE,
		ERROR_RB_READ,
		ERROR_RB_WRITE,
		TP_ERROR
		);
END_TYPE

(*//////////////////////////////////////////// GetTPErrors ////////////////////////////////////////////////////////////////////////////////*)

TYPE
	TP9030GetTPErrors_Type : 	STRUCT 
		CntError : UDINT;
		FrameIn : STRING[1000];
	END_STRUCT;
END_TYPE
