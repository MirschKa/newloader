(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: TP9030ISO
 * File: TP9030ISO.fun
 * Author: A. Festerling
 * Created: October 19, 2017
 ********************************************************************
 * Functions and function blocks of library TP9030ISO
 ********************************************************************)

FUNCTION_BLOCK TP9030Init (*Initialize and Deinitialize plugged Transponder*) (*$GROUP=User*)
	VAR_INPUT
		Enable : BOOL; (*Only executed if TRUE*)
		pDevice : UDINT; (*Pointer to string of the Device name (ADR)*)
	END_VAR
	VAR_OUTPUT
		Error : BOOL; (*Error active*)
		ErrorID : UDINT; (*Error number*)
		Ident : UDINT; (*Identifier of opened frame => Has to be used for all other FUBs of this library*)
	END_VAR
	VAR
		InternalTP9030Init : TP9030Init_Type;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK TP9030Info (*Detection if chip is placed or removed as all as reading of the tag information*) (*$GROUP=User*)
	VAR_INPUT
		Enable : BOOL; (*Starts execution of the functionblock. Also used as error reset*)
		Ident : UDINT; (*Frame Ident => Output from TP9030Init*)
	END_VAR
	VAR_OUTPUT
		Done : BOOL; (*Execution finished successfully*)
		Busy : BOOL; (*FUB is busy*)
		Error : BOOL; (*Error active*)
		ErrorID : UINT; (*Error number of internal functionblocks or this library*)
		ErrorMessage : STRING[50]; (*Error message if Transponder has send an error*)
		Status : USINT; (*Status if card is in range or not*)
		UID : ARRAY[0..7] OF USINT; (*UID of the present tag*)
		RSSI : USINT; (*Not used yet*)
		BlockSize : USINT; (*Size in Byte of one block*)
		NumBlocks : USINT; (*Number of blocks*)
		MemSize : USINT; (*Memory size of the whole card => BlockSize * NumBlocks*)
		TagSelect : BOOL; (*Card / Tag selected by the transponder*)
		TagRemove : BOOL; (*Card / Tag removed from transponder*)
	END_VAR
	VAR
		InternalTP9030Info : TP9030Info_Type;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK TP9030ReadBlocks (*Read out the passed number of blocks*) (*$GROUP=User*)
	VAR_INPUT
		Enable : BOOL; (*Starts execution of the functionblock. Also used as error reset!*)
		Ident : UDINT; (*Frame Ident => Output from TP9030Init*)
		pDestination : {REDUND_UNREPLICABLE} UDINT; (*Pointer to data buffer to store tag data in*)
		FirstBlock : USINT; (*First block of the present tag which should be read*)
		NumBlocks : USINT; (*Number of blocks which should be read beginning from the FirstBlock. Attention: Number has to be <= then NumBlocks of TP9030Info*)
		BlockSize : USINT; (*Size in Byte of one block => Output BlockSize of TP9030Info*)
		Multiple : USINT; (*Not used yet*)
	END_VAR
	VAR_OUTPUT
		Done : BOOL; (*All blocks are red successfully*)
		Busy : BOOL; (*FUB is busy*)
		Error : BOOL; (*Error active! Reset FUB by calling it with Enable := FALSE*)
		ErrorID : UDINT; (*Error ID of internal FUBs or library*)
		ErrorMessage : STRING[50]; (*Error message send by transponder*)
	END_VAR
	VAR
		InternalTP9030ReadBlocks : TP9030ReadBlocks_Type;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK GetTPErrors (*Check if Transponder is sending an error message => If so, extract error number and message*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		pFrameIn : UDINT; (*Frame send by transponder*)
	END_VAR
	VAR_OUTPUT
		TPErrorActive : BOOL; (*Frame contains an error message*)
		ErrorMessage : STRING[1000]; (*Error message send by the transponder*)
	END_VAR
	VAR
		InternalTP9030GetTPErrors : TP9030GetTPErrors_Type;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION HexToByte : USINT (*Converts Hex values to byte*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		pHex : UDINT;
	END_VAR
	VAR
		IntHexValue0 : REFERENCE TO USINT;
		IntHexValue1 : REFERENCE TO USINT;
		retval : USINT; (*Internal Return Value*)
	END_VAR
END_FUNCTION
