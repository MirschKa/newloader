(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: TP9030ISO
 * File: TP9030Info.st
 * Author: A. Festerling
 * Created: October 19, 2017
 ********************************************************************
 * Implementation of library TP9030ISO
 ********************************************************************) 

(* Detection if chip is placed or removed as all as reading of the tag information *)
FUNCTION_BLOCK TP9030Info
	
	IF Enable THEN
		
		Error																:= (InternalTP9030Info.StepInfo = ERROR_READ) OR (InternalTP9030Info.StepInfo = ERROR_WRITE) OR (InternalTP9030Info.StepInfo = ERROR_TP);
		Done																:= (InternalTP9030Info.StepInfo = WAIT_TAG_REMOVE);
		Busy																:= (InternalTP9030Info.StepInfo >= WAIT_TAG_DETECTED) AND (InternalTP9030Info.StepInfo < WAIT_TAG_REMOVE);
	
		(* Check for errors globally *)
		IF (InternalTP9030Info.FUB.FRM_read_0.status <> 0) AND (InternalTP9030Info.FUB.FRM_read_0.status <> ERR_FUB_ENABLE_FALSE)
			AND (InternalTP9030Info.FUB.FRM_read_0.status <> ERR_FUB_BUSY) AND (InternalTP9030Info.FUB.FRM_read_0.status <> frmERR_NOINPUT) THEN
			
			InternalTP9030Info.StepInfo										:= ERROR_READ;
		END_IF;
		
		(* Check for errors globally *)
		IF (InternalTP9030Info.FUB.FRM_rbuf_0.status <> 0) AND (InternalTP9030Info.FUB.FRM_rbuf_0.status <> ERR_FUB_ENABLE_FALSE)
			AND (InternalTP9030Info.FUB.FRM_rbuf_0.status <> ERR_FUB_BUSY) THEN
			
			InternalTP9030Info.StepInfo										:= ERROR_READ;
		END_IF;
		
		(* Check for errors globally *)
		IF (InternalTP9030Info.FUB.FRM_write_0.status <> 0) AND (InternalTP9030Info.FUB.FRM_write_0.status <> ERR_FUB_ENABLE_FALSE)
			AND (InternalTP9030Info.FUB.FRM_write_0.status <> ERR_FUB_BUSY) THEN
			
			InternalTP9030Info.StepInfo										:= ERROR_WRITE;
		END_IF;
		
		
		CASE InternalTP9030Info.StepInfo OF
			
			INIT:
	
				IF (Ident <> 0) THEN
					
					(* Always try to release buffers incase FUB execution was aborted *)
					InternalTP9030Info.FUB.FRM_rbuf_0.enable					:= TRUE;
					InternalTP9030Info.FUB.FRM_rbuf_0.buffer					:= InternalTP9030Info.FUB.FRM_read_0.buffer;
					InternalTP9030Info.FUB.FRM_rbuf_0.buflng					:= InternalTP9030Info.FUB.FRM_read_0.buflng;
					InternalTP9030Info.FUB.FRM_rbuf_0.ident						:= Ident;
					InternalTP9030Info.FUB.FRM_rbuf_0();
			
					IF (InternalTP9030Info.FUB.FRM_rbuf_0.status <> ERR_FUB_BUSY) AND (InternalTP9030Info.FUB.FRM_rbuf_0.status <> ERR_FUB_ENABLE_FALSE) THEN
						InternalTP9030Info.FUB.FRM_rbuf_0.enable				:= FALSE;
						InternalTP9030Info.FUB.FRM_rbuf_0();
						InternalTP9030Info.FUB.FRM_read_0.enable				:= FALSE;
						InternalTP9030Info.FUB.FRM_read_0();
					END_IF;
				
					(* Reset all FUBs *)
					InternalTP9030Info.FUB.FRM_write_0.enable					:= FALSE;
					InternalTP9030Info.FUB.FRM_write_0();
					InternalTP9030Info.FUB.FRM_robuf_0.enable					:= FALSE;
					InternalTP9030Info.FUB.FRM_robuf_0();
			
					(* Clear Buffer *)
					brsmemset(	ADR(InternalTP9030Info.Para.FrameIn), 0, SIZEOF(InternalTP9030Info.Para.FrameIn)	);
					brsmemset(	ADR(InternalTP9030Info.Para.FrameOut), 0, SIZEOF(InternalTP9030Info.Para.FrameOut)	);
					brsmemset(	ADR(InternalTP9030Info.Status), 0, SIZEOF(InternalTP9030Info.Status)	);
			
					(* Set status *)
					brsmemset(ADR(UID) , 0, SIZEOF(UID));
					brsmemset(	ADR(ErrorMessage), 0, SIZEOF(ErrorMessage)	);
					BlockSize													:= 0;
					NumBlocks													:= 0;
					Done														:= FALSE;
					Busy														:= FALSE;
					Error														:= FALSE;
					ErrorID														:= 0;
					TagRemove													:= FALSE;
					TagSelect													:= FALSE;
					MemSize														:= 0;
					RSSI														:= 0;
					Status														:= TP_STATUS_NO_CARDINRANGE;
				
					(* Change state *)
					InternalTP9030Info.StepInfo									:= WAIT_TAG_DETECTED;
				
				ELSE
					Error														:= TRUE;
					ErrorID														:= FRAME_IDENT_MISSING;
				END_IF;	
			(********************************************************************************************************************************************)

			
			WAIT_TAG_DETECTED:
			
				InternalTP9030Info.FUB.FRM_read_0.enable					:= TRUE;
				InternalTP9030Info.FUB.FRM_read_0.ident						:= Ident;
				InternalTP9030Info.FUB.FRM_read_0();
			
				IF (InternalTP9030Info.FUB.FRM_read_0.status = frmERR_NOINPUT) THEN
					(* Do nothing until Tag is detected *)
				
				(* Tag was detected *)
				ELSIF (InternalTP9030Info.FUB.FRM_read_0.status = 0) THEN
					brsmemcpy( ADR(InternalTP9030Info.Para.FrameIn), InternalTP9030Info.FUB.FRM_read_0.buffer, InternalTP9030Info.FUB.FRM_read_0.buflng	); 
					brsmemset( InternalTP9030Info.FUB.FRM_read_0.buffer, 0, InternalTP9030Info.FUB.FRM_read_0.buflng	);
					
					TagSelect												:= TRUE;
					TagRemove												:= FALSE;
					
					
					(* Catch unwanted "PICC_REMOVE" *)
					brsmemset(	ADR(InternalTP9030Info.Status.PiccRemove), 0, SIZEOF(InternalTP9030Info.Status.PiccRemove)	);
					brsstrcpy(ADR(InternalTP9030Info.Status.PiccRemove), ADR(InternalTP9030Info.Para.FrameIn));
					
					FOR InternalTP9030Info.Para.CntInfo := 0 TO (SIZEOF(InternalTP9030Info.Status.PiccRemove) - brsstrlen(ADR('PiccRemove')) - 1) DO
						IF (brsmemcmp(ADR(InternalTP9030Info.Status.PiccRemove)+ InternalTP9030Info.Para.CntInfo, ADR('PiccRemove'),10 ) = 0 ) THEN
							TagSelect										:= FALSE;
							TagRemove										:= FALSE;
							InternalTP9030Info.Status.PiccRemoveCatched		:= TRUE;
							EXIT;
						END_IF;
					END_FOR;
					
					InternalTP9030Info.FUB.FRM_rbuf_0.enable					:= TRUE;
					InternalTP9030Info.FUB.FRM_rbuf_0.buffer					:= InternalTP9030Info.FUB.FRM_read_0.buffer;
					InternalTP9030Info.FUB.FRM_rbuf_0.buflng					:= InternalTP9030Info.FUB.FRM_read_0.buflng;
					InternalTP9030Info.FUB.FRM_rbuf_0.ident						:= Ident;
					InternalTP9030Info.FUB.FRM_rbuf_0();
				
					IF (InternalTP9030Info.FUB.FRM_rbuf_0.status = 0) THEN
						InternalTP9030Info.FUB.FRM_rbuf_0.enable				:= FALSE;
						InternalTP9030Info.FUB.FRM_rbuf_0();
						InternalTP9030Info.FUB.FRM_read_0.enable				:= FALSE;
						InternalTP9030Info.FUB.FRM_read_0();
						
						IF InternalTP9030Info.Status.PiccRemoveCatched THEN
							InternalTP9030Info.Status.PiccRemoveCatched			:= FALSE;
						ELSE
							ErrorID												:= 0;
							Status												:= TP_STATUS_CARDINRANGE;
					
							InternalTP9030Info.StepInfo							:= GET_UID;
						END_IF;
					END_IF;
				END_IF;
			(********************************************************************************************************************************************)
			
			
			GET_UID:
			
			(* Get Tag-UID from Tag Detection Frame => Search in String / Array for char ":". After that UID is following  *)
				FOR InternalTP9030Info.Para.CntInfo := 0 TO 255 DO
					IF  (InternalTP9030Info.Para.FrameIn[InternalTP9030Info.Para.CntInfo] = COLON) THEN 			//58 => ':'
						InternalTP9030Info.Status.StartOfUID				:= InternalTP9030Info.Para.CntInfo + 2;		// +2 because empty char ('') is following directly after colon
						EXIT;
					END_IF;
				END_FOR;
			
				InternalTP9030Info.Status.LoopCounter						:= 0;
				FOR InternalTP9030Info.Para.CntInfo := InternalTP9030Info.Status.StartOfUID TO (InternalTP9030Info.Status.StartOfUID + 15) BY 2 DO
					UID[InternalTP9030Info.Status.LoopCounter]				:= HexToByte(ADR(InternalTP9030Info.Para.FrameIn[InternalTP9030Info.Para.CntInfo]));
					InternalTP9030Info.Status.LoopCounter					:= InternalTP9030Info.Status.LoopCounter + 1;
				END_FOR;
			
				InternalTP9030Info.StepInfo									:= REQUEST_SYSTEM_INFO;
				
				(* Clear Buffer *)
				brsmemset(	ADR(InternalTP9030Info.Para.FrameIn), 0, SIZEOF(InternalTP9030Info.Para.FrameIn)	);
				brsmemset(	ADR(InternalTP9030Info.Para.FrameOut), 0, SIZEOF(InternalTP9030Info.Para.FrameOut)	);
			(********************************************************************************************************************************************)
			
			
			REQUEST_SYSTEM_INFO:
			
				(* Build request string *)
				brsstrcpy(	ADR(InternalTP9030Info.Para.FrameOut), ADR('sys_info,22$n')	);
			
			
				InternalTP9030Info.FUB.FRM_write_0.enable					:= TRUE;
				InternalTP9030Info.FUB.FRM_write_0.ident 					:= Ident;					
				InternalTP9030Info.FUB.FRM_write_0.buffer 					:= ADR(InternalTP9030Info.Para.FrameOut);					
				InternalTP9030Info.FUB.FRM_write_0.buflng 					:= UDINT_TO_UINT(brsstrlen(ADR(InternalTP9030Info.Para.FrameOut)));
				InternalTP9030Info.FUB.FRM_write_0();	
				
			
				IF InternalTP9030Info.FUB.FRM_write_0.status = 0 THEN
					InternalTP9030Info.FUB.FRM_write_0.enable 				:= FALSE;
					InternalTP9030Info.FUB.FRM_write_0();
		
					InternalTP9030Info.StepInfo								:= GET_SYSTEMINFO;
				END_IF;
			(********************************************************************************************************************************************)
			
			
			GET_SYSTEMINFO:
			
				InternalTP9030Info.FUB.FRM_read_0.enable					:= TRUE;
				InternalTP9030Info.FUB.FRM_read_0.ident						:= Ident;
				InternalTP9030Info.FUB.FRM_read_0();
			
				IF (InternalTP9030Info.FUB.FRM_read_0.status = frmERR_NOINPUT) THEN
					(* Do nothing until Tag is detected *)
				
					(* Tag was detected *)
				ELSIF (InternalTP9030Info.FUB.FRM_read_0.status = 0) THEN
					brsmemcpy( ADR(InternalTP9030Info.Para.FrameIn), InternalTP9030Info.FUB.FRM_read_0.buffer, InternalTP9030Info.FUB.FRM_read_0.buflng	); 
					brsmemset( InternalTP9030Info.FUB.FRM_read_0.buffer, 0, InternalTP9030Info.FUB.FRM_read_0.buflng	);
					
					
					InternalTP9030Info.FUB.FRM_rbuf_0.enable					:= TRUE;
					InternalTP9030Info.FUB.FRM_rbuf_0.buffer					:= InternalTP9030Info.FUB.FRM_read_0.buffer;
					InternalTP9030Info.FUB.FRM_rbuf_0.buflng					:= InternalTP9030Info.FUB.FRM_read_0.buflng;
					InternalTP9030Info.FUB.FRM_rbuf_0.ident						:= Ident;
					InternalTP9030Info.FUB.FRM_rbuf_0();
				
					IF (InternalTP9030Info.FUB.FRM_rbuf_0.status = 0) THEN
						InternalTP9030Info.FUB.FRM_rbuf_0.enable				:= FALSE;
						InternalTP9030Info.FUB.FRM_rbuf_0();
						InternalTP9030Info.FUB.FRM_read_0.enable				:= FALSE;
						InternalTP9030Info.FUB.FRM_read_0();
						
						InternalTP9030Info.StepInfo								:= DECODE_SYSTEMINFO;
					END_IF;
				END_IF;
			(********************************************************************************************************************************************)
			
			
			DECODE_SYSTEMINFO:
			
				NumBlocks													:= HexToByte(ADR(InternalTP9030Info.Para.FrameIn[START_CHAR_NBR_BLOCKS])) + 1;
				BlockSize													:= HexToByte(ADR(InternalTP9030Info.Para.FrameIn[START_CHAR_BLOCK_SIZE])) + 1;
				MemSize														:= NumBlocks*BlockSize;
				
				(* Clear Buffer *)
				brsmemset(	ADR(InternalTP9030Info.Para.FrameIn), 0, SIZEOF(InternalTP9030Info.Para.FrameIn)	);
				brsmemset(	ADR(InternalTP9030Info.Para.FrameOut), 0, SIZEOF(InternalTP9030Info.Para.FrameOut)	);
			
				InternalTP9030Info.StepInfo									:= WAIT_TAG_REMOVE;
			
			(********************************************************************************************************************************************)
			
			
			WAIT_TAG_REMOVE:
			
				InternalTP9030Info.FUB.FRM_read_0.enable					:= TRUE;
				InternalTP9030Info.FUB.FRM_read_0.ident						:= Ident;
				InternalTP9030Info.FUB.FRM_read_0();
			
				IF (InternalTP9030Info.FUB.FRM_read_0.status = frmERR_NOINPUT) THEN
					(* Do nothing until Tag is detected *)
				
					(* Tag was detected *)
				ELSIF (InternalTP9030Info.FUB.FRM_read_0.status = 0) THEN
					brsmemcpy( ADR(InternalTP9030Info.Para.FrameIn), InternalTP9030Info.FUB.FRM_read_0.buffer, InternalTP9030Info.FUB.FRM_read_0.buflng	); 
					brsmemset( InternalTP9030Info.FUB.FRM_read_0.buffer, 0, InternalTP9030Info.FUB.FRM_read_0.buflng	);
				
					brsmemset(ADR(InternalTP9030Info.Status.PiccRemove), 0, SIZEOF(InternalTP9030Info.Status.PiccRemove));
					brsstrcpy(ADR(InternalTP9030Info.Status.PiccRemove), ADR(InternalTP9030Info.Para.FrameIn));
					
					(* Clear Buffer *)
					brsmemset(	ADR(InternalTP9030Info.Para.FrameIn), 0, SIZEOF(InternalTP9030Info.Para.FrameIn)	);
					brsmemset(	ADR(InternalTP9030Info.Para.FrameOut), 0, SIZEOF(InternalTP9030Info.Para.FrameOut)	);
					
					TagSelect												:= FALSE;
					TagRemove												:= TRUE;
					
					Status													:= TP_STATUS_NO_CARDINRANGE;
					
					InternalTP9030Info.FUB.FRM_rbuf_0.enable					:= TRUE;
					InternalTP9030Info.FUB.FRM_rbuf_0.buffer					:= InternalTP9030Info.FUB.FRM_read_0.buffer;
					InternalTP9030Info.FUB.FRM_rbuf_0.buflng					:= InternalTP9030Info.FUB.FRM_read_0.buflng;
					InternalTP9030Info.FUB.FRM_rbuf_0.ident						:= Ident;
					InternalTP9030Info.FUB.FRM_rbuf_0();
				
					IF (InternalTP9030Info.FUB.FRM_rbuf_0.status = 0) THEN
						InternalTP9030Info.FUB.FRM_rbuf_0.enable				:= FALSE;
						InternalTP9030Info.FUB.FRM_rbuf_0();
						InternalTP9030Info.FUB.FRM_read_0.enable				:= FALSE;
						InternalTP9030Info.FUB.FRM_read_0();
						
						InternalTP9030Info.StepInfo								:= WAIT_TAG_DETECTED;
					END_IF;
				END_IF;
			
			(********************************************************************************************************************************************)

				
			ERROR_READ:
			
				ErrorID														:= InternalTP9030Info.FUB.FRM_read_0.status;
				(* Always try ro release all buffers *)
			
				(* Reading Frame *)
				InternalTP9030Info.FUB.FRM_rbuf_0.enable					:= TRUE;
				InternalTP9030Info.FUB.FRM_rbuf_0.buffer					:= InternalTP9030Info.FUB.FRM_read_0.buffer;
				InternalTP9030Info.FUB.FRM_rbuf_0.buflng					:= InternalTP9030Info.FUB.FRM_read_0.buflng;
				InternalTP9030Info.FUB.FRM_rbuf_0.ident						:= Ident;
				InternalTP9030Info.FUB.FRM_rbuf_0();
				
				IF (InternalTP9030Info.FUB.FRM_rbuf_0.status <> ERR_FUB_BUSY) AND (InternalTP9030Info.FUB.FRM_rbuf_0.status <> ERR_FUB_ENABLE_FALSE) THEN
					InternalTP9030Info.FUB.FRM_rbuf_0.enable				:= FALSE;
					InternalTP9030Info.FUB.FRM_rbuf_0();
					InternalTP9030Info.FUB.FRM_read_0.enable				:= FALSE;
					InternalTP9030Info.FUB.FRM_read_0();
					InternalTP9030Info.StepInfo								:= INIT;
				END_IF;
			
		
			ERROR_WRITE:
				
				ErrorID														:= InternalTP9030Info.FUB.FRM_write_0.status;
				
				(* Writing Frame *)
				InternalTP9030Info.FUB.FRM_robuf_0.enable					:= TRUE;
				InternalTP9030Info.FUB.FRM_robuf_0.buffer					:= ADR(InternalTP9030Info.Para.FrameOut);;
				InternalTP9030Info.FUB.FRM_robuf_0.buflng					:= SIZEOF(InternalTP9030Info.Para.FrameOut);;
				InternalTP9030Info.FUB.FRM_robuf_0.ident					:= Ident;
				InternalTP9030Info.FUB.FRM_robuf_0();
				
				IF (InternalTP9030Info.FUB.FRM_robuf_0.status <> ERR_FUB_BUSY) AND (InternalTP9030Info.FUB.FRM_robuf_0.status <> ERR_FUB_ENABLE_FALSE) THEN
					InternalTP9030Info.FUB.FRM_robuf_0.enable				:= FALSE;
					InternalTP9030Info.FUB.FRM_robuf_0();
					InternalTP9030Info.FUB.FRM_write_0.enable				:= FALSE;
					InternalTP9030Info.FUB.FRM_write_0();
					InternalTP9030Info.StepInfo								:= INIT;
				END_IF;
			(******************************************************************************************************************************************************************)
				
			
			ERROR_TP:
			(* Do nothing. Only block further execution of the FUB *)
			(* Error reset of the FUB done by calling it with Enable := FALSE; *)
			
		END_CASE;
		(******************************************************************************************************************************************************************)
		
		(**********************************************************************)
		(*					Check for transponder error message				  *)
		(**********************************************************************)
		InternalTP9030Info.FUB.GetTPErrors_0.pFrameIn						:= ADR(InternalTP9030Info.Para.FrameIn);
		InternalTP9030Info.FUB.GetTPErrors_0();
		IF InternalTP9030Info.FUB.GetTPErrors_0.TPErrorActive > InternalTP9030Info.Status.TPErrorActiveOld THEN
			brsmemcpy(ADR(ErrorMessage), ADR(InternalTP9030Info.FUB.GetTPErrors_0.ErrorMessage), brsstrlen(ADR(InternalTP9030Info.FUB.GetTPErrors_0.ErrorMessage))-2); // -2 => Cut off $r$n from string end
			InternalTP9030Info.StepInfo										:= ERROR_TP;
		END_IF;
		InternalTP9030Info.Status.TPErrorActiveOld							:= InternalTP9030Info.FUB.GetTPErrors_0.TPErrorActive;
		
		
		
		(******************************************************************************************************************************************************************)
		(******************************************************************************************************************************************************************)	
	ELSE
		brsmemset(ADR(UID) , 0, SIZEOF(UID));
		BlockSize															:= 0;
		NumBlocks															:= 0;
		InternalTP9030Info.StepInfo											:= INIT;
		Done																:= FALSE;
		Busy																:= FALSE;
		Error																:= FALSE;
		ErrorID																:= FALSE;
		TagRemove															:= FALSE;
		TagSelect															:= FALSE;
		MemSize																:= 0;
		RSSI																:= 0;
		Status																:= TP_STATUS_NO_CARDINRANGE;
		
	END_IF;
END_FUNCTION_BLOCK
