(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: TP9030ISO
 * File: TP9030Init.st
 * Author: A. Festerling
 * Created: October 19, 2017
 ********************************************************************
 * Implementation of library TP9030ISO
 ********************************************************************) 

(* Initialize and Deinitialize plugged Transponder *)
FUNCTION_BLOCK TP9030Init
	
	(* Only execute and so open Frame if FUB is enabled *)
	IF Enable THEN
		
		(* Reset FRM_xopen once at the beginning *)
		IF InternalTP9030Init.Para.FirstCycle THEN
			InternalTP9030Init.FUB.FRM_xopen_0.enable			:= FALSE;
			InternalTP9030Init.FUB.FRM_xopen_0();
			Ident												:= 0;
			
			IF (pDevice <> 0) THEN
				InternalTP9030Init.Para.FirstCycle				:= FALSE;
				Error											:= 0;
				ErrorID											:= 0;
			ELSE
				Error											:= TRUE;
				ErrorID											:= NO_DEVICE_PASSED;
			END_IF;
		ELSE
			
			(* Init parameter structure *)
			InternalTP9030Init.Para.XOpenConfig.idle			:= 65000;
			InternalTP9030Init.Para.XOpenConfig.delimc			:= 1;
			InternalTP9030Init.Para.XOpenConfig.delim[0]		:= 10;		//$n
			InternalTP9030Init.Para.XOpenConfig.delim[1]		:= 0;
			InternalTP9030Init.Para.XOpenConfig.tx_cnt			:= 1;
			InternalTP9030Init.Para.XOpenConfig.rx_cnt			:= 8;
			InternalTP9030Init.Para.XOpenConfig.tx_len			:= 256;		//256 Byte
			InternalTP9030Init.Para.XOpenConfig.rx_len			:= 256;		//256 Byte
			InternalTP9030Init.Para.XOpenConfig.argc			:= 0;
			InternalTP9030Init.Para.XOpenConfig.argv			:= 0;
			
			(* Open Frame like configured *)
			InternalTP9030Init.FUB.FRM_xopen_0.enable			:= TRUE;
			InternalTP9030Init.FUB.FRM_xopen_0.device			:= pDevice;
			InternalTP9030Init.FUB.FRM_xopen_0.mode				:= ADR('/BD=115200 /PA=N /DB=8 /SB=1 /PHY=RS232');
			InternalTP9030Init.FUB.FRM_xopen_0.config			:= ADR(InternalTP9030Init.Para.XOpenConfig);
			InternalTP9030Init.FUB.FRM_xopen_0();
			
			IF (InternalTP9030Init.FUB.FRM_xopen_0.status = 0) THEN
				Ident											:= InternalTP9030Init.FUB.FRM_xopen_0.ident;
			END_IF;
		
			Error												:= (InternalTP9030Init.FUB.FRM_xopen_0.status <> 0);
			ErrorID												:= InternalTP9030Init.FUB.FRM_xopen_0.status;
		END_IF;
		
	
	(* If FUB is not enabled anymore and a Frame was opened, close frame and reset outputs *)
	ELSE
		IF (Ident <> 0) THEN
			InternalTP9030Init.FUB.FRM_close_0.enable			:= TRUE;
			InternalTP9030Init.FUB.FRM_close_0.ident			:= Ident;
			InternalTP9030Init.FUB.FRM_close_0();
			
			IF (	(InternalTP9030Init.FUB.FRM_close_0.status <> ERR_FUB_BUSY) AND (InternalTP9030Init.FUB.FRM_close_0.status <> ERR_FUB_ENABLE_FALSE) 	) THEN
				
				InternalTP9030Init.FUB.FRM_close_0.enable		:= FALSE;
				InternalTP9030Init.FUB.FRM_close_0();
				Ident											:= 0;
			END_IF;	
		END_IF;
		Error													:= FALSE;
		ErrorID													:= 0;
		InternalTP9030Init.Para.FirstCycle						:= TRUE;
	END_IF;
END_FUNCTION_BLOCK
