(*********************************************************************************
 * Copyright: Bernecker + Rainer
 * Author:    A. Festerling
 * Created:   November 13, 2017/11:02 AM 
 *********************************************************************************)

(* Check if Transponder is sending an error message => If so, extract error number and message *)
FUNCTION_BLOCK GetTPErrors
	
	(* Always clear outputs before every cycle *)
	brsmemset(ADR(InternalTP9030GetTPErrors.FrameIn), 0, SIZEOF(InternalTP9030GetTPErrors.FrameIn));
	brsmemset(ADR(ErrorMessage), 0, SIZEOF(ErrorMessage));
	
	(* Copy passed string into internal variable *)
	brsmemcpy(ADR(InternalTP9030GetTPErrors.FrameIn), pFrameIn, brsstrlen(pFrameIn));
	
	(* Search for error message withing passed Frame input *)
	IF (brsstrlen(ADR(InternalTP9030GetTPErrors.FrameIn)) >= brsstrlen(ADR('Error'))) THEN
		FOR InternalTP9030GetTPErrors.CntError := 0 TO (brsstrlen(ADR(InternalTP9030GetTPErrors.FrameIn)) - brsstrlen(ADR('Error'))) DO
			IF (brsmemcmp(ADR(InternalTP9030GetTPErrors.FrameIn) + InternalTP9030GetTPErrors.CntError, ADR('Error'), brsstrlen(ADR('Error')) ) = 0 ) THEN
				TPErrorActive									:= TRUE;
				brsmemcpy(ADR(ErrorMessage), ADR(InternalTP9030GetTPErrors.FrameIn) + InternalTP9030GetTPErrors.CntError, brsstrlen(ADR(InternalTP9030GetTPErrors.FrameIn)) - InternalTP9030GetTPErrors.CntError);
				EXIT;
			ELSE
				TPErrorActive									:= FALSE;
			END_IF;
		END_FOR;
	ELSE
		TPErrorActive											:= FALSE;
	END_IF;
	
END_FUNCTION_BLOCK
