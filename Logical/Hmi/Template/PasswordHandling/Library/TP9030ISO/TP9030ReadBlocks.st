(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: TP9030ISO
 * File: TP9030ReadBlocks.st
 * Author: A. Festerling
 * Created: October 20, 2017
 ********************************************************************
 * Implementation of library TP9030ISO
 ********************************************************************) 

(* Read out the passed number of blocks *)
FUNCTION_BLOCK TP9030ReadBlocks
	
	IF Enable THEN
		
		Error																	:= (InternalTP9030ReadBlocks.StepReadBlocks = ERROR_RB_READ)
																					OR (InternalTP9030ReadBlocks.StepReadBlocks = ERROR_RB_WRITE)
																					OR (InternalTP9030ReadBlocks.StepReadBlocks = TP_ERROR);
		(********************************************************************************************************************************************)
	
		(* Check for errors globally *)
		IF (InternalTP9030ReadBlocks.FUB.FRM_read_0.status <> 0) AND (InternalTP9030ReadBlocks.FUB.FRM_read_0.status <> ERR_FUB_ENABLE_FALSE)
			AND (InternalTP9030ReadBlocks.FUB.FRM_read_0.status <> ERR_FUB_BUSY) AND (InternalTP9030ReadBlocks.FUB.FRM_read_0.status <> frmERR_NOINPUT) THEN
			
			InternalTP9030ReadBlocks.StepReadBlocks								:= ERROR_RB_READ;
		END_IF;
		
		(* Check for errors globally *)
		IF (InternalTP9030ReadBlocks.FUB.FRM_rbuf_0.status <> 0) AND (InternalTP9030ReadBlocks.FUB.FRM_rbuf_0.status <> ERR_FUB_ENABLE_FALSE)
			AND (InternalTP9030ReadBlocks.FUB.FRM_rbuf_0.status <> ERR_FUB_BUSY) THEN
			
			InternalTP9030ReadBlocks.StepReadBlocks								:= ERROR_RB_READ;
		END_IF;
		
		(* Check for errors globally *)
		IF (InternalTP9030ReadBlocks.FUB.FRM_write_0.status <> 0) AND (InternalTP9030ReadBlocks.FUB.FRM_write_0.status <> ERR_FUB_ENABLE_FALSE)
			AND (InternalTP9030ReadBlocks.FUB.FRM_write_0.status <> ERR_FUB_BUSY) THEN
			
			InternalTP9030ReadBlocks.StepReadBlocks								:= ERROR_RB_WRITE;
		END_IF;
		(********************************************************************************************************************************************)
		
		
		(**************************************)
		(*** Main State machine 			***)
		(**************************************)
		CASE InternalTP9030ReadBlocks.StepReadBlocks OF

			(* Initialize main components of the FUB *)
			READ_BLOCKS_INIT:
				
				IF (Ident > 0) AND (pDestination > 0) AND (BlockSize > 0) AND (NumBlocks > 0) THEN	
					
					(* Always try to release buffers incase FUB execution was aborted *)
					InternalTP9030ReadBlocks.FUB.FRM_rbuf_0.enable				:= TRUE;
					InternalTP9030ReadBlocks.FUB.FRM_rbuf_0.buffer				:= InternalTP9030ReadBlocks.FUB.FRM_read_0.buffer;
					InternalTP9030ReadBlocks.FUB.FRM_rbuf_0.buflng				:= InternalTP9030ReadBlocks.FUB.FRM_read_0.buflng;
					InternalTP9030ReadBlocks.FUB.FRM_rbuf_0.ident				:= Ident;
					InternalTP9030ReadBlocks.FUB.FRM_rbuf_0();
			
					IF (InternalTP9030ReadBlocks.FUB.FRM_rbuf_0.status <> ERR_FUB_BUSY) AND (InternalTP9030ReadBlocks.FUB.FRM_rbuf_0.status <> ERR_FUB_ENABLE_FALSE) THEN
						InternalTP9030ReadBlocks.FUB.FRM_rbuf_0.enable			:= FALSE;
						InternalTP9030ReadBlocks.FUB.FRM_rbuf_0();
						InternalTP9030ReadBlocks.FUB.FRM_read_0.enable			:= FALSE;
						InternalTP9030ReadBlocks.FUB.FRM_read_0();
					END_IF;
			
			
					(* Reset all FUBs *)
					InternalTP9030ReadBlocks.FUB.FRM_write_0.enable				:= FALSE;
					InternalTP9030ReadBlocks.FUB.FRM_write_0();
					InternalTP9030ReadBlocks.FUB.FRM_robuf_0.enable				:= FALSE;
					InternalTP9030ReadBlocks.FUB.FRM_robuf_0();
			
			
					(* Clear Buffer *)
					brsmemset(	ADR(InternalTP9030ReadBlocks.Para), 0, SIZEOF(InternalTP9030ReadBlocks.Para)	);
					brsmemset(	ADR(InternalTP9030ReadBlocks.Status), 0, SIZEOF(InternalTP9030ReadBlocks.Status)	);
					brsmemset(	ADR(ErrorMessage), 0, SIZEOF(ErrorMessage)	);
					Done														:= FALSE;
					Error														:= FALSE;
					Busy														:= FALSE;
					ErrorID														:= 0;
					Multiple;
			
					(* Change state *)
					InternalTP9030ReadBlocks.StepReadBlocks						:= REQUEST_BLOCKS;
				
				ELSE
					Error														:= TRUE;
					IF Ident = 0 THEN
						ErrorID													:= FRAME_IDENT_MISSING;
					ELSIF pDestination = 0 THEN
						ErrorID													:= NO_DESTINATION_DATA_BUFFER;
					ELSIF BlockSize = 0 THEN
						ErrorID													:= INVALID_BLOCK_SIZE;
					ELSIF NumBlocks = 0 THEN
						ErrorID													:= INVALID_NUMBER_OF_BLOCKS;
					END_IF;
				END_IF;	
			(********************************************************************************************************************************************)
	
			
			(* Send Frame to request Blocks from present TAG *)
			REQUEST_BLOCKS:
				Busy																:= TRUE;
				IF (Ident > 0) AND (pDestination > 0) AND (BlockSize > 0) AND (NumBlocks > 0) THEN	
					(* Build request string ==>> e.g. 'read_blocks,22,0,22$r$n																			*)
					(* read_blocks,			==>> ISO15693 Command "what to do" 																			*)
					(* 22					==>> Flag Definition in Hex!! => Example Flag 22 in Bin => Bit8 - 00100010 - Bit 1, For Description see manual	*) 
					InternalTP9030ReadBlocks.Para.StrStartBlock						:= USINT_TO_STRING(FirstBlock);
					InternalTP9030ReadBlocks.Para.StrLastBlock						:= USINT_TO_STRING(FirstBlock + NumBlocks - 1);
					brsstrcpy(	ADR(InternalTP9030ReadBlocks.Para.FrameOut), ADR('read_blocks,22,')	);
					brsstrcat(	ADR(InternalTP9030ReadBlocks.Para.FrameOut), ADR(InternalTP9030ReadBlocks.Para.StrStartBlock)	);
					brsstrcat(	ADR(InternalTP9030ReadBlocks.Para.FrameOut), ADR(',')	);
					brsstrcat(	ADR(InternalTP9030ReadBlocks.Para.FrameOut), ADR(InternalTP9030ReadBlocks.Para.StrLastBlock)	);
					brsstrcat(	ADR(InternalTP9030ReadBlocks.Para.FrameOut), ADR('$n')	);	
			
			
					InternalTP9030ReadBlocks.FUB.FRM_write_0.enable					:= TRUE;
					InternalTP9030ReadBlocks.FUB.FRM_write_0.ident 					:= Ident;					
					InternalTP9030ReadBlocks.FUB.FRM_write_0.buffer 				:= ADR(InternalTP9030ReadBlocks.Para.FrameOut);					
					InternalTP9030ReadBlocks.FUB.FRM_write_0.buflng 				:= UDINT_TO_UINT(brsstrlen(ADR(InternalTP9030ReadBlocks.Para.FrameOut)));
					InternalTP9030ReadBlocks.FUB.FRM_write_0();	
				
			
					IF (InternalTP9030ReadBlocks.FUB.FRM_write_0.status = 0) THEN
						InternalTP9030ReadBlocks.FUB.FRM_write_0.enable 			:= FALSE;
						InternalTP9030ReadBlocks.FUB.FRM_write_0();
					
						(* Clear Frame Out Buffer *)
						brsmemset(	ADR(InternalTP9030ReadBlocks.Para.FrameOut), 0, SIZEOF(InternalTP9030ReadBlocks.Para.FrameOut)	);
					
						InternalTP9030ReadBlocks.StepReadBlocks						:= GET_BLOCKS;
						
					END_IF;
				ELSE
					Error														:= TRUE;
					IF Ident = 0 THEN
						ErrorID													:= FRAME_IDENT_MISSING;
					ELSIF pDestination = 0 THEN
						ErrorID													:= NO_DESTINATION_DATA_BUFFER;
					ELSIF BlockSize = 0 THEN
						ErrorID													:= INVALID_BLOCK_SIZE;
					ELSIF NumBlocks = 0 THEN
						ErrorID													:= INVALID_NUMBER_OF_BLOCKS;
					END_IF;
				END_IF;		
			(********************************************************************************************************************************************)
			
			
			(* Read incoming frame *)
			GET_BLOCKS:
			
				InternalTP9030ReadBlocks.FUB.FRM_read_0.enable					:= TRUE;
				InternalTP9030ReadBlocks.FUB.FRM_read_0.ident					:= Ident;
				InternalTP9030ReadBlocks.FUB.FRM_read_0();
			
				IF (InternalTP9030ReadBlocks.FUB.FRM_read_0.status = frmERR_NOINPUT) THEN

				
				(* Incoming Frame detected *)
				ELSIF (InternalTP9030ReadBlocks.FUB.FRM_read_0.status = 0) THEN
					brsmemcpy( ADR(InternalTP9030ReadBlocks.Para.FrameIn) + InternalTP9030ReadBlocks.Status.Offset, InternalTP9030ReadBlocks.FUB.FRM_read_0.buffer, InternalTP9030ReadBlocks.FUB.FRM_read_0.buflng	); 
					brsmemset( InternalTP9030ReadBlocks.FUB.FRM_read_0.buffer, 0, InternalTP9030ReadBlocks.FUB.FRM_read_0.buflng	);
					
					InternalTP9030ReadBlocks.Status.Offset						:= InternalTP9030ReadBlocks.Status.Offset + InternalTP9030ReadBlocks.FUB.FRM_read_0.buflng;
					
					(* Check if message is finished *)
					IF InternalTP9030ReadBlocks.Status.Offset < (	(BlockSize * 2 * (NumBlocks)) + ((NumBlocks + 1)*2)	) THEN
						InternalTP9030ReadBlocks.Status.MessageNotFinished		:= TRUE;
					ELSE
						InternalTP9030ReadBlocks.Status.MessageNotFinished		:= FALSE;
					END_IF;	
					
					InternalTP9030ReadBlocks.FUB.FRM_rbuf_0.enable					:= TRUE;
					InternalTP9030ReadBlocks.FUB.FRM_rbuf_0.buffer					:= InternalTP9030ReadBlocks.FUB.FRM_read_0.buffer;
					InternalTP9030ReadBlocks.FUB.FRM_rbuf_0.buflng					:= InternalTP9030ReadBlocks.FUB.FRM_read_0.buflng;
					InternalTP9030ReadBlocks.FUB.FRM_rbuf_0.ident					:= Ident;
					InternalTP9030ReadBlocks.FUB.FRM_rbuf_0();
					
					IF (InternalTP9030ReadBlocks.FUB.FRM_rbuf_0.status = 0) THEN
						InternalTP9030ReadBlocks.FUB.FRM_rbuf_0.enable				:= FALSE;
						InternalTP9030ReadBlocks.FUB.FRM_rbuf_0();
						InternalTP9030ReadBlocks.FUB.FRM_read_0.enable				:= FALSE;
						InternalTP9030ReadBlocks.FUB.FRM_read_0();
					
						(* Check if message is already finished or not *)
						IF InternalTP9030ReadBlocks.Status.MessageNotFinished THEN
							InternalTP9030ReadBlocks.StepReadBlocks					:= GET_BLOCKS;
						ELSE
							InternalTP9030ReadBlocks.StepReadBlocks					:= ALL_BLOCKS_DONE;
							Done													:= TRUE;
							Busy													:= FALSE;
						
							FOR InternalTP9030ReadBlocks.Status.CntCopy := 0 TO (BlockSize * 2 * NumBlocks + ((NumBlocks + 1)*2))/2 DO
								InternalTP9030ReadBlocks.Para.BlockData[InternalTP9030ReadBlocks.Status.CntCopy]	:= HexToByte(ADR(InternalTP9030ReadBlocks.Para.FrameIn[2+2*InternalTP9030ReadBlocks.Status.CntCopy]));
								brsmemcpy(pDestination, ADR(InternalTP9030ReadBlocks.Para.BlockData), SIZEOF(InternalTP9030ReadBlocks.Para.BlockData));
							END_FOR;
						END_IF;
					END_IF;
				END_IF;
			(********************************************************************************************************************************************)
			
			ALL_BLOCKS_DONE:
				
			(********************************************************************************************************************************************)
			
			
			ERROR_RB_READ:
				Done																:= FALSE;
				Busy																:= FALSE;
				ErrorID																:= InternalTP9030ReadBlocks.FUB.FRM_read_0.status;
			
				(* Always try ro release all buffers *)
				(* Reading Frame *)
				InternalTP9030ReadBlocks.FUB.FRM_rbuf_0.enable						:= TRUE;
				InternalTP9030ReadBlocks.FUB.FRM_rbuf_0.buffer						:= InternalTP9030ReadBlocks.FUB.FRM_read_0.buffer;
				InternalTP9030ReadBlocks.FUB.FRM_rbuf_0.buflng						:= InternalTP9030ReadBlocks.FUB.FRM_read_0.buflng;
				InternalTP9030ReadBlocks.FUB.FRM_rbuf_0.ident						:= Ident;
				InternalTP9030ReadBlocks.FUB.FRM_rbuf_0();
				
				IF (InternalTP9030ReadBlocks.FUB.FRM_rbuf_0.status <> ERR_FUB_BUSY) AND (InternalTP9030ReadBlocks.FUB.FRM_rbuf_0.status <> ERR_FUB_ENABLE_FALSE) THEN
					InternalTP9030ReadBlocks.FUB.FRM_rbuf_0.enable					:= FALSE;
					InternalTP9030ReadBlocks.FUB.FRM_rbuf_0();
					InternalTP9030ReadBlocks.FUB.FRM_read_0.enable					:= FALSE;
					InternalTP9030ReadBlocks.FUB.FRM_read_0();
					
					InternalTP9030ReadBlocks.StepReadBlocks							:= READ_BLOCKS_INIT;
				END_IF;
			
		
			ERROR_RB_WRITE:
				Done																:= FALSE;
				Busy																:= FALSE;
				ErrorID																:= InternalTP9030ReadBlocks.FUB.FRM_write_0.status;
				
				(* Always try ro release all buffers *)
				(* Writing Frame *)
				InternalTP9030ReadBlocks.FUB.FRM_robuf_0.enable						:= TRUE;
				InternalTP9030ReadBlocks.FUB.FRM_robuf_0.buffer						:= ADR(InternalTP9030ReadBlocks.Para.FrameOut);;
				InternalTP9030ReadBlocks.FUB.FRM_robuf_0.buflng						:= SIZEOF(InternalTP9030ReadBlocks.Para.FrameOut);;
				InternalTP9030ReadBlocks.FUB.FRM_robuf_0.ident						:= Ident;
				InternalTP9030ReadBlocks.FUB.FRM_robuf_0();
				
				IF (InternalTP9030ReadBlocks.FUB.FRM_robuf_0.status <> ERR_FUB_BUSY) AND (InternalTP9030ReadBlocks.FUB.FRM_robuf_0.status <> ERR_FUB_ENABLE_FALSE) THEN
					InternalTP9030ReadBlocks.FUB.FRM_robuf_0.enable					:= FALSE;
					InternalTP9030ReadBlocks.FUB.FRM_robuf_0();
					InternalTP9030ReadBlocks.FUB.FRM_write_0.enable					:= FALSE;
					InternalTP9030ReadBlocks.FUB.FRM_write_0();
					
					InternalTP9030ReadBlocks.StepReadBlocks							:= READ_BLOCKS_INIT;
				END_IF;
			(******************************************************************************************************************************************************************)
				
			
			TP_ERROR:
			(* Do nothing. Only block further execution of the FUB *)
			(* Error reset of the FUB done by calling it with Enable := FALSE; *)
			
		END_CASE;
		(******************************************************************************************************************************************************************)
		
		
		(**********************************************************************)
		(*					Check for transponder error message				  *)
		(**********************************************************************)
		InternalTP9030ReadBlocks.FUB.GetTPErrors_0.pFrameIn							:= ADR(InternalTP9030ReadBlocks.Para.FrameIn);
		InternalTP9030ReadBlocks.FUB.GetTPErrors_0();
		IF InternalTP9030ReadBlocks.FUB.GetTPErrors_0.TPErrorActive > InternalTP9030ReadBlocks.Status.TPErrorActiveOld THEN
			brsmemcpy(ADR(ErrorMessage), ADR(InternalTP9030ReadBlocks.FUB.GetTPErrors_0.ErrorMessage), brsstrlen(ADR(InternalTP9030ReadBlocks.FUB.GetTPErrors_0.ErrorMessage))-2); // -2 => Cut off $r$n from string end
			InternalTP9030ReadBlocks.StepReadBlocks									:= TP_ERROR;
		END_IF;
		InternalTP9030ReadBlocks.Status.TPErrorActiveOld							:= InternalTP9030ReadBlocks.FUB.GetTPErrors_0.TPErrorActive;
		
		
		
		(******************************************************************************************************************************************************************)
		(******************************************************************************************************************************************************************)	
	ELSE
		Done																		:= FALSE;
		Error																		:= FALSE;
		Busy																		:= FALSE;
		ErrorID																		:= 0;
		
		(* Clear Buffer *)
		brsmemset(	ADR(InternalTP9030ReadBlocks.Para), 0, SIZEOF(InternalTP9030ReadBlocks.Para)	);
		brsmemset(	ADR(InternalTP9030ReadBlocks.Status), 0, SIZEOF(InternalTP9030ReadBlocks.Status)	);
		brsmemset(	ADR(ErrorMessage), 0, SIZEOF(ErrorMessage)	);
		
		(* Reset all FUBs *)
		InternalTP9030ReadBlocks.FUB.FRM_write_0.enable								:= FALSE;
		InternalTP9030ReadBlocks.FUB.FRM_write_0();
		InternalTP9030ReadBlocks.FUB.FRM_robuf_0.enable								:= FALSE;
		InternalTP9030ReadBlocks.FUB.FRM_robuf_0();
		InternalTP9030ReadBlocks.FUB.FRM_rbuf_0.enable								:= FALSE;
		InternalTP9030ReadBlocks.FUB.FRM_rbuf_0();
		InternalTP9030ReadBlocks.FUB.FRM_read_0.enable								:= FALSE;
		InternalTP9030ReadBlocks.FUB.FRM_read_0();
		
		InternalTP9030ReadBlocks.StepReadBlocks										:= REQUEST_BLOCKS; //READ_BLOCKS_INIT;
	END_IF;
END_FUNCTION_BLOCK
