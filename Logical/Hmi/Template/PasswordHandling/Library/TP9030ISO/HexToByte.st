(*********************************************************************************
 * Copyright: Bernecker + Rainer
 * Author:    A. Festerling
 * Created:   October 24, 2017/11:07 AM 
 *********************************************************************************)

(* Converts Hex values to byte *)
FUNCTION HexToByte
	
	IntHexValue0 ACCESS pHex;
	IntHexValue1 ACCESS (pHex + 1);		// Plus 1 because USINT variable is a 1Byte one! => Switch to next aray element
	
	//	Str -> Dec
	//	'0' -> 48
	//	'9' -> 57
	//	'a' -> 97
	//	'f' -> 102
	//	'A' -> 65
	//	'F' -> 70
	
	
	IF (IntHexValue0 >= 48 AND IntHexValue0 <= 57)	THEN	
		retval 													:= IntHexValue0 - 48;
	ELSIF (IntHexValue0 >= 97 AND IntHexValue0 <= 102) THEN
		retval 													:= IntHexValue0 - 97 + 10;
	ELSIF (IntHexValue0 >= 65 AND IntHexValue0 <= 70) THEN
		retval 													:= IntHexValue0 - 65 + 10;
	ELSE
		retval 													:= 0;
	END_IF;
	
	retval 														:= SHL(retval,4); 
	
	IF (IntHexValue1 >= 48 AND IntHexValue1 <= 57) THEN
		retval 													:= retval OR (IntHexValue1 - 48);		//OR(retval, (IntHexValue1 - 48));
	ELSIF (IntHexValue1 >= 97 AND IntHexValue1 <= 102) THEN
		retval 													:= retval OR (IntHexValue1 - 97 + 10);	//OR(retval, (IntHexValue1 - 97 + 10)); 
	ELSIF (IntHexValue1 >= 65 AND IntHexValue1 <= 70) THEN
		retval 													:= retval OR (IntHexValue1 - 65 + 10);	//OR(retval, (IntHexValue1 - 65 + 10));
	ELSE
		retval 													:= retval OR 0;							//OR(retval, 0);
	END_IF;
	
	HexToByte 													:= retval;	
	
END_FUNCTION
