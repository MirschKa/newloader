(************************************************************************************************************************)
(* Object name: PasswordIsMatching                                                                                      *)
(* Author:      H. Hoffmann                                                                                             *)
(* Site:        CFS Germany GmbH, Niederlassung Wallau                                                                  *)
(* Created:     07-may-2008                                                                                             *)
(* Restriction: Developed on base of AS 2.7.0.10 SP3 (but should work with any other)                                   *)
(*              String inputs to be passed as parameters of the function (not longer than 80 characters).               *)
(* Description: Provides functions and tool-box for the implementation of the "CFS Login System" based on               *)
(*              RFID technology                                                                                         *)
(*                                                                                                                      *)
(*----------------------------------------------------------------------------------------------------------------------*)
(* Additional information:                                                                                              *)
(*                                                                                                                      *)
(*      The function compares the both passed input strings in the way, that the parameter for InStrKeyUserPassword     *)
(*      is stripped from leading zeros as usually included from the RFID read process and the compares the result       *)
(*      against the second parameter InStrPasswoordInput that stands for the password as the users input on the panel   *)
(*      for the login process.                                                                                          *)
(*                                                                                                                      *)
(*----------------------------------------------------------------------------------------------------------------------*)
(* Version 3.00.0  18-jan-2012  Samuel Otto                     Development tool: B&R Automation Studio V3.0.90.19 SP01 *)
(*                                                                                                                      *)
(* Cleaned up this revision history and set version to V3.00.0                                                          *)
(*--------------------------------------------------------------------------------------------------------------------- *)
(************************************************************************************************************************)

FUNCTION PasswordIsMatching
IF Enabled THEN
	(* make local work-copy of entered password (merely for debug purposes) *)
	tmpStringPasswordInput:= InStrPasswordInput;
	(* Trim leading zeros from the readen key stored password*)
	TrimStrLeadingZeros_0(Enabled:= TRUE, InString:= InStrKeyUserPassword);
	tmpStringPasswordOnKey:= TrimStrLeadingZeros_0.OutString;
	(* compare trimmed result string with manual password input *)
	strcmp_status := brsstrcmp(ADR(tmpStringPasswordOnKey), ADR(tmpStringPasswordInput));
	IF strcmp_status = 0 THEN
		PasswordIsMatching := TRUE;
	ELSE
		PasswordIsMatching := FALSE;
	END_IF;
ELSE
	PasswordIsMatching := FALSE; (* make sure we have a defined function result *)
END_IF;

END_FUNCTION
