(************************************************************************************************************************)
(* Object name: ValidateKeyExpirationDate                                                                               *)
(* Author:      H. Hoffmann                                                                                             *)
(* Site:        CFS Germany GmbH, Niederlassung Wallau                                                                  *)
(* Created:     24-apr-2008                                                                                             *)
(* Restriction: For use with AutomationStudio 2.7.0.10 SP3, upgrade packs CFS-AS27-UGP1 & CFS-AS27-UGP2 applied         *)
(*              Requires CFS Login System based on URS of Gert-Jan Langen, mar 2008                                     *)
(* Description: Provides functions and tool-box for the implementation of the "CFS Login System" based on
(*              RFID technology.                                                                                        *)
(*                                                                                                                      *)
(*----------------------------------------------------------------------------------------------------------------------*)
(* Additional information:                                                                                              *)
(*                                                                                                                      *)
(*      The function block "ValidateTagExpirationDate" checks the given expiration date read from a RFID aTAG as a      *)
(*      string value, 8 characters long, format "YYYYMMDD" (e.g. 20080504 for 4 may 2008) against the current date      *)
(*      retrieved from the PLCs real-time clock. In case the TAG's expiration date exceeds the current plc real-time    *)
(*      clock date, the output "Expired" of the function block states TRUE otherwise FALSE.                             *)
(*                                                                                                                      *)
(*----------------------------------------------------------------------------------------------------------------------*)
(* Version 3.00.0  18-jan-2012  Samuel Otto                     Development tool: B&R Automation Studio V3.0.90.19 SP01 *)
(*                                                                                                                      *)
(* Cleaned up this revision history and set version to V3.00.0                                                          *)
(*--------------------------------------------------------------------------------------------------------------------- *)
(************************************************************************************************************************)

FUNCTION_BLOCK ValidateKeyExpirationDate
IF Enabled THEN
	OutFailed:= FALSE;
	(* convert the TAGs expiration date given as string into DATE_AND_TIME type *)
	KeyExpDateStrToDate_0(Enabled:= TRUE, InKeyExpirationDateStr:= InKeyExpirationDateStr);

	IF (KeyExpDateStrToDate_0.OutFailed = FALSE) THEN
		(* get converted expiration date in DATE_AND_TIME format *)
		tmpKeyExpirationDate:= KeyExpDateStrToDate_0.OutKeyExpirationDate; (* get the converted expiration date as DATE_AND_TIME data type *)
		OutExpirationDate:= tmpKeyExpirationDate; (* pass the expiration date here also to an out var to be able to show the expiration date somewhere else *)
		DTGetTime_0(enable:= TRUE);
		IF DTGetTime_0.status = 0 THEN (* PLCs date/time got without errors ? *)
			tmpCurrentPlcDateTime:= DTGetTime_0.DT1;
			tmpDiffDT2_DT1:= DiffDT(tmpKeyExpirationDate, tmpCurrentPlcDateTime); (* calc difference between the both timestamps in seconds *)

			IF (tmpDiffDT2_DT1 = 0) OR (tmpDiffDT2_DT1 = 16#FFFFFFFF) THEN
				OutKeyExpired:= TRUE; (* is difference = 0 or contains B&R's constant to show a negative value (UDINT can't get negative so the constant is used by B&R *)
			ELSE
				OutKeyExpired:= FALSE; (* no, difference still greater 0 => still not expired *)
			END_IF;

		ELSE (* error during retrieving PLC's current date/time from real time clock *)
			OutErrorSource := ERR_GETTING_PLC_DATE;
			OutError:= DTGetTime_0.status;
			OutFailed:= TRUE;
		END_IF;
	ELSE (* error during converting the TAG's expiration date string into DATE_AND_TIME *)
		OutErrorSource := ERR_STR_TO_DATE_CONVERSION;
		OutError:= KeyExpDateStrToDate_0.OutError;
		OutFailed:= TRUE;
	END_IF;

END_IF;

END_FUNCTION_BLOCK
