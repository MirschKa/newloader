(************************************************************************************************************************)
(* Object name: KeyExpDateStrToDate                                                                                     *)
(* Author:      H. Hoffmann                                                                                             *)
(* Site:        CFS Germany GmbH, Niederlassung Wallau                                                                  *)
(* Created:     24-apr-2008                                                                                             *)
(* Restriction: For use with AutomationStudio 2.7.0.10 SP3 upgrade packs CFS-AS27-UGP1 and CFS-AS27-UGP2 applied        *)
(*              Requires CFS Login System based on URS of Gert-Jan Langen, mar 2008                                     *)
(*              Expiration date in var to be passed in format "YYYYMMDD", always 8 characters long                      *)
(* Description: Provides functions and tool-box for the implementation of the "CFS Login System" based on               *)
(*              RFID technology.                                                                                        *)
(*                                                                                                                      *)
(*----------------------------------------------------------------------------------------------------------------------*)
(* Additional information:                                                                                              *)
(*                                                                                                                      *)
(*      The function block "TAGExpDateStrToDate" converts a expiration date read from a RFID TAG as a string value,     *)
(*      8 characters long, format YYYYMMDD (e.g. 20080504 for 4 may 2008) into a date type output var.                  *)
(*                                                                                                                      *)
(*----------------------------------------------------------------------------------------------------------------------*)
(* Version 3.00.0  18-jan-2012  Samuel Otto                     Development tool: B&R Automation Studio V3.0.90.19 SP01 *)
(*                                                                                                                      *)
(* Cleaned up this revision history and set version to V3.00.0                                                          *)
(*--------------------------------------------------------------------------------------------------------------------- *)
(************************************************************************************************************************)

FUNCTION_BLOCK KeyExpDateStrToDate
IF Enabled THEN
	OutFailed:= FALSE; (* pre-set , we start fresh *)

	(* Check if the String has the required content length*)
	IF (brsstrlen(ADR(InKeyExpirationDateStr)) = REQUIRED_EXP_DATE_STRLEN) THEN

		(* parse the input string and get the elements year, month, day out as temporary internal string vars *)
		brsmemcpy(ADR(tmpstrExpYear),ADR(InKeyExpirationDateStr),4); (* get year out from the input expiration date *)
		brsmemcpy(ADR(tmpstrExpMonth),ADR(InKeyExpirationDateStr) + 4, 2);(* get month out from the input expiration date *)
		brsmemcpy(ADR(tmpstrExpDay),ADR(InKeyExpirationDateStr) + 6, 2);(* get day out from the input expiration date *)

		(* convert string elements to UDINT temporary vars later to be used for date type conversion *)
		tmpudintExpYear:= brsatoi(ADR(tmpstrExpYear));
		tmpudintExpMonth:= brsatoi(ADR(tmpstrExpMonth));
		tmpudintExpDay:= brsatoi(ADR(tmpstrExpDay));

		(* check if the converted number for year, month and date are in valid range  *)

		(*----------- check given year value -------------------*)
		IF (tmpudintExpYear > 0) AND (tmpudintExpYear < 10000) THEN
			tmpDTStructure.year:= UDINT_TO_UINT(tmpudintExpYear);

			(*----------- check given month value -------------------*)
			IF (tmpudintExpMonth > 0) AND (tmpudintExpMonth < 13) THEN
				tmpDTStructure.month:= UDINT_TO_USINT(tmpudintExpMonth);
			ELSE (* month is wrong -> set error code *)
				OutError:= ERR_MONTH_OUT_OF_RANGE;
				OutFailed:= TRUE;
			END_IF;

				(*----------- check given day value -------------------*)
				IF (tmpudintExpDay > 0) AND (tmpudintExpDay < 32) THEN
					tmpDTStructure.day:= UDINT_TO_USINT(tmpudintExpDay);

					(* year, month and day was in range -> convert to DATE_AND_TIME type *)
					tmpDTStructure.wday		:= 0; 	(* pre-set because not given from TAG *)
					tmpDTStructure.hour		:= 0;	(* pre-set because not given from TAG *)
					tmpDTStructure.minute	:= 0;	(* pre-set because not given from TAG *)
					tmpDTStructure.second	:= 0;	(* pre-set because not given from TAG *)
					tmpDTStructure.millisec	:= 0;	(* pre-set because not given from TAG *)
					tmpDTStructure.microsec	:= 0;	(* pre-set because not given from TAG *)

					(* run conversion to DATE_AND_TIME *)
					OutKeyExpirationDate:=DTStructure_TO_DT(ADR (tmpDTStructure));

				ELSE (* day is wrong -> set error code *)
					OutError:= ERR_DAY_OUT_OF_RANGE;
					OutFailed:= TRUE;
				END_IF;

		ELSE (* year already wrong -> set error code *)
			OutError:= ERR_YEAR_OUT_OF_RANGE;
			OutFailed:= TRUE;
		END_IF;

	ELSE
		OutError:= ERR_EXP_DATE_STR_LENGTH;
		OutFailed:= TRUE;
	END_IF;

END_IF;

END_FUNCTION_BLOCK
