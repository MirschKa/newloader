FUNCTION_BLOCK KeyExpDateStrToDate
VAR
		tmpstrExpYear	:STRING[10];	
		tmpstrExpMonth	:STRING[10];	
		tmpstrExpDay	:STRING[10];	
		tmpudintExpYear	:UDINT;	
		tmpudintExpMonth	:UDINT;	
		tmpudintExpDay	:UDINT;	
		tmpDTStructure	:DTStructure;	
	END_VAR
	VAR_INPUT
		Enabled	:BOOL;	
		InKeyExpirationDateStr	:STRING[10];	
	END_VAR
	VAR_OUTPUT
		OutError	:INT;	
		OutFailed	:BOOL;	
		OutKeyExpirationDate	:DATE_AND_TIME;	
	END_VAR
END_FUNCTION_BLOCK
FUNCTION_BLOCK ValidateKeyExpirationDate
VAR
		DTGetTime_0	:DTGetTime;	
		tmpKeyExpirationDate	:DATE_AND_TIME;	
		tmpDiffDT2_DT1	:UDINT;	
		tmpCurrentPlcDateTime	:DATE_AND_TIME;	
	END_VAR
	VAR_INPUT
		Enabled	:BOOL;	
		InKeyExpirationDateStr	:STRING[80];	
		KeyExpDateStrToDate_0	:KeyExpDateStrToDate;	
	END_VAR
	VAR_OUTPUT
		OutFailed	:BOOL;	
		OutErrorSource	:UINT;	
		OutError	:INT;	
		OutKeyExpired	:BOOL;	
		OutExpirationDate	:DATE_AND_TIME;	
	END_VAR
END_FUNCTION_BLOCK
FUNCTION_BLOCK TrimStrLeadingZeros
VAR
		tmpString	:STRING[255];	
		LenInString	:UINT;	
		TrimFinished	:BOOL;	
	END_VAR
	VAR_INPUT
		Enabled	:BOOL;	
		InString	:STRING[255];	
	END_VAR
	VAR_OUTPUT
		OutString	:STRING[255];	
	END_VAR
END_FUNCTION_BLOCK
FUNCTION PasswordIsMatching : BOOL 
VAR
		TrimStrLeadingZeros_0	:TrimStrLeadingZeros;	
		strcmp_status	:DINT;	
		tmpStringPasswordOnKey	:STRING[80];	
		tmpStringPasswordInput	:STRING[80];	
	END_VAR
	VAR_INPUT
		Enabled	:BOOL;	
		InStrKeyUserPassword	:STRING[80];	
		InStrPasswordInput	:STRING[80];	
	END_VAR
END_FUNCTION
