(************************************************************************************************************************)
(* Object name: TrimStrLeadingZeros                                                                                     *)
(* Author:      H. Hoffmann	                                                                                            *)
(* Site:        CFS Germany GmbH, Niederlassung Wallau                                                                  *)
(* Created:     07-may-2008                                                                                             *)
(* Restriction: Developed on base of AS 2.7.0.10 SP3 (but should work with any other)                                   *)
(*              String input to be passed as parameter of the function                                                  *)
(* Description: Provides functions and tool-box for the implementation of the "CFS Login System" based on               *)
(*              RFID technology.                                                                                        *)
(*                                                                                                                      *)
(*----------------------------------------------------------------------------------------------------------------------*)
(* Additional information:                                                                                              *)
(*                                                                                                                      *)
(* The function strips leading zeros from the input string and returns the remaining part as the function result        *)
(*                                                                                                                      *)
(*----------------------------------------------------------------------------------------------------------------------*)
(* Version 3.00.0  18-jan-2012  Samuel Otto                     Development tool: B&R Automation Studio V3.0.90.19 SP01 *)
(*                                                                                                                      *)
(* Cleaned up this revision history and set version to V3.00.0                                                          *)
(*--------------------------------------------------------------------------------------------------------------------- *)
(************************************************************************************************************************)

FUNCTION_BLOCK TrimStrLeadingZeros
IF Enabled THEN
	TrimFinished:= FALSE; (* pre-init, we are starting the job *)
	tmpString:= InString; (* get a work-copy of the input string *)
	LenInString:= LEN(InString); (* Get length of the input string ; as an own var for better debug purposes; *)

	IF(LenInString > 0) THEN (* check input string for content *)
		WHILE NOT TrimFinished DO
			IF (LEN(tmpString) > 0) THEN
				IF (LEFT(tmpString, 1) = '0') THEN (* get the left most character of the string and check if the result is a zero '0' *)
					tmpString:= DELETE(tmpString, 1,1); (* if yes, was a zero, then delete the left most char of the input strings temporary work-copy *)
				ELSE
					TrimFinished:= TRUE; (* it's done, pass to output *)
					OutString:= tmpString;
				END_IF;
			ELSE
				TrimFinished:= TRUE; (* it's done, pass to output *)
				OutString:= tmpString;
			END_IF;
		END_WHILE;
	ELSE
		OutString:= tmpString; (* pass to output, it was just an empty string *)
	END_IF;
END_IF;
END_FUNCTION_BLOCK
