
TYPE
	T_CFSLoginSystemModeCfg : 	STRUCT 
		SelectionCtrlsStatus : UINT;
		SelectionCtrlsInvertetStatus : UINT;
		SelectionCompleted : BOOL;
		SelectedMode : USINT;
		MaxAllowedMode : USINT;
		btnSaveSettings : BOOL;
	END_STRUCT;
END_TYPE
