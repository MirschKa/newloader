(************************************************************************************************************************)
(* Object name: system                                                                                                  *)
(* Author:      Samuel Otto                                                                                             *)
(* Site:        B&R Bad Homburg                                                                                         *)
(* Created:     11-may-2011                                                                                             *)
(* Restriction: consider task order.                                                                                    *)
(* Description: System settings                                                                                         *)
(*                                                                                                                      *)
(*----------------------------------------------------------------------------------------------------------------------*)
(* Additional information:                                                                                              *)
(*                                                                                                                      *)
(* No additional information available.                                                                                 *)
(*                                                                                                                      *)
(*----------------------------------------------------------------------------------------------------------------------*)
(* Version 3.00.0  18-jan-2012  Samuel Otto                     Development tool: B&R Automation Studio V3.0.90.19 SP01 *)
(*                                                                                                                      *)
(* Cleaned up this revision history and set version to V3.00.0                                                          *)
(*--------------------------------------------------------------------------------------------------------------------- *)
(* Version 3.00.1  04-apr-2012  Frank Thomas Greeb              Development tool: B&R Automation Studio V3.0.90.19 SP01 *)
(* in VNC-Password-Management in VNC_Step=1 : FOR-loop only 0 TO 1 (not 0 TO 2)                                         *)
(*--------------------------------------------------------------------------------------------------------------------- *)
(*         3.00.2  17-sep-2012  FTG                      Development tool: B&R Automation Studio V3.0.90.21 SP03    	  *)
(* default init of config para in INIT changed to allow machine task to predefine                                       *)
(*--------------------------------------------------------------------------------------------------------------------- *)
(*         3.00.3  13-dec-2012  FTG                      Development tool: B&R Automation Studio V3.0.90.21 SP03    	  *)
(* add ip-adress to header of vnc and limit length of this string                                                       *)
(*--------------------------------------------------------------------------------------------------------------------- *)
(*         3.00.4  12-sep-2013  EC                      Development tool: B&R Automation Studio V3.0.90.21 SP03    		  *)
(* IssueList Item [#68] Make SDM Button default visible																	*)
(* IssueList Item [#60] Symbol for local / remote control missing														*)
(*  - TPL_System In and Out expand with Remote for the Operation Mode Remote functionality								*)
(*  	create RemoteModeBitmap	to show state in visu depending to the in state											*)
(************************************************************************************************************************)
(* Version 3.04.1  20-apr-2015  SO                              Development tool: B&R Automation Studio V3.0.90.28 SP10 *)
(* [#461] HMI flashing on Dynamic Menu page																				*)
(*		  On very high CPU load it could be happaned, that the screensaver rest time becomes 0 and the screensaver		    *)
(*		  changes the page to page 1000. With a Tolerence in TC #8 the maximum time this task "system" is pending are	    *)
(*		  30 seconds. So additional 30 seconds were added.																*)
(************************************************************************************************************************)
(* Version 3.04.2 23.01.2018 FB                                 Development tool: B&R Automation Studio V4.1.12.58 SP   *)
(* [491] Screenchange to 1000
(*  At some machines there is sporadically a page change TO page 1000. The reason is the autologout function. 
(*  Sometime the VC does NOT recognize the new setting OF ScreenSaveTime.
(*    
(*  Use rising edge to detect new setting of ScreenSaveTime
(************************************************************************************************************************)
(* Version 3.04.2 23.01.2018 FB                                 Development tool: B&R Automation Studio V4.5.3.86 SP    *)
(* [538] Cleaning display - timing problem
(* TPL_Pageh.In.SwitchToPage = TPL_PAGE_CLNG2 instead of TPL_Pageh.Out.ActualPage
(************************************************************************************************************************)
PROGRAM _INIT
// Cleaning Display
	CleaningTime := 30;
	CleaningTimeInMs:= UDINT_TO_TIME(USINT_TO_UDINT(CleaningTime)*1000);

	// Languagehandling
	LanguageIndex := 0;
	TPL_System.In.Cfg.ActualLanguage; (* This is the actual language, connected to Visu and saved in hmi_cfg *)

	// VNC-Password-Management
	// Set Ini-passwords, if no other passwords are set
	VNC_Password[0]	:= '615531';
	VNC_Password[1]	:= 'c6f1s9';
	// Encode the passwords for saving in hmi_cfg
	FOR i := 0 TO 1 DO
		brsmemset(ADR(VNC_Password[i]) + brsstrlen(ADR(VNC_Password[i])) + 1, 0,SIZEOF(VNC_Password[i]) - brsstrlen(ADR(VNC_Password[i])) - 1 );	// Delete characters after binary 0	
		brsmemcpy(ADR(VNCcodeBuffer[i]),    ADR(VNC_Password[i]),   2);
		brsmemset(ADR(VNCcodeBuffer[i])+2,  66 + i,                 1);
		brsmemcpy(ADR(VNCcodeBuffer[i])+3,  ADR(VNC_Password[i])+2, 6);
		brsmemset(ADR(VNCcodeBuffer[i])+9,  66 + i,                 1);
		brsmemcpy(ADR(VNCcodeBuffer[i])+10, ADR(VNC_Password[i])+8, 2);
		EncodeB64(ADR(VNCcodeBuffer[i]), SIZEOF(VNCcodeBuffer[i]), ADR(TPL_System.In.Cfg.VNC_Password[i]), SIZEOF(TPL_System.In.Cfg.VNC_Password[i]), ADR(VNC_PasswordLength));
	END_FOR
	RfbExtInit(ADR(VNC_pLib), 500);	// Initializes the library for VNC-Password-Management

	// Autologout after TIME
	TPL_System.In.Cfg.AutoLogoutOn					:= 1;
	TPL_System.In.Cfg.AutoLogoutTimeInMin			:= 5;
	ScreenSaveTime := TPL_System.In.Cfg.AutoLogoutTimeInMin * 60;	// Here it will be initialized with the (wrong) default value. But if the config is ready, it will be updated automatically

	// Logbook
	Counter:= 10000;
	// Visibility SDM button on page 3200
	TPL_System.In.Cfg.SDMBtnVisible := 1;
END_PROGRAM

PROGRAM _CYCLIC
(* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	Cleaning Display
   ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*)
	TON_CleaningDisplay(IN:=StartCleaningTimer, PT:=CleaningTimeInMs);
	IF TPL_Pageh.In.SwitchToPage = TPL_PAGE_CLNG2 THEN
		StartCleaningTimer := TRUE;		// start timer if PAGE_CLNG2 is open
		CleaningTime := UDINT_TO_USINT(30 - (TIME_TO_UDINT(TON_CleaningDisplay.ET)/1000));	// calc value for bargraph
		IF TON_CleaningDisplay.Q = TRUE THEN	// if time is over go back to PAGE_CLNG
			StartCleaningTimer := 0;
			TPL_Pageh.In.SwitchToPage := TPL_PAGE_CLNG;
		END_IF
	END_IF

(* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	Languagehandling
   ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*)
	(* Check for available languages *)
	IF (TPL_Pageh.Out.VC_Handle <> 0) AND LanguageCheckReady = FALSE THEN
		IF LanguageIndex < (SIZEOF(StatusLanguageList) / SIZEOF(StatusLanguageList[0])) THEN
			IF (VA_Saccess(1,TPL_Pageh.Out.VC_Handle) = 0) THEN
				IF VA_LangIsAvailable(1,TPL_Pageh.Out.VC_Handle,LanguageIndex,ADR(LanguageStatus)) = 0 THEN
					IF (LanguageStatus = 1) THEN
						StatusLanguageList[LanguageIndex] := 0; (*show language*)
					ELSE
						StatusLanguageList[LanguageIndex] := 2; (*hide language*)
					END_IF
					LanguageIndex := LanguageIndex + 1;
				END_IF
				VA_Srelease(1,TPL_Pageh.Out.VC_Handle);
			END_IF
		ELSE
			LanguageCheckReady := TRUE;
		END_IF
	END_IF

(* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	VNC-Password-Management
   ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*)
	CASE VNC_Step OF
		0: // Initialize connection to the VNC-Visu
			IF RfbExtConnect(ADR(VNC_pLib), ADR('panel')) = 0 THEN
				VNC_Step := 1;
			END_IF
		1:	// Decode passwords from CSV-File
			IF TPL_Config.Out.StartUpFinished = TRUE THEN // Connection is established
				FOR i := 0 TO 1 DO
					statusDecode := DecodeB64(ADR(TPL_System.In.Cfg.VNC_Password[i]), VNC_PasswordLength, ADR(VNCcodeBuffer[i]), SIZEOF(VNCcodeBuffer[i]));
					testByte := 66 + i;
					IF
						statusDecode = 0 AND
						(brsmemcmp(ADR(VNCcodeBuffer[i])+2, ADR(testByte) , 1)) = 0 AND // Check the two testbytes
						(brsmemcmp(ADR(VNCcodeBuffer[i])+9, ADR(testByte) , 1)) = 0 // Check the two testbytes
						THEN	// passwords ok
						brsmemcpy(ADR(VNC_Password[i]),   ADR(VNCcodeBuffer[i]),    2);
						brsmemcpy(ADR(VNC_Password[i])+2, ADR(VNCcodeBuffer[i])+3,  6);
						brsmemcpy(ADR(VNC_Password[i])+8, ADR(VNCcodeBuffer[i])+10, 2);
						VNC_Step := 3;
					ELSE	// something was wrong with the password, maybe someone edited the csv-file. So let's save the default value
						VNC_Step := 2;
					END_IF
				END_FOR
			END_IF
		
		2:	// Encode and save new passwords to CSV-File
			// Later we can use the following lines for changing VNC-Passwords by the visualisation. Maybe triggered by change-datapoint.
			FOR i := 0 TO 1 DO
				brsmemset(ADR(VNC_Password[i]) + brsstrlen(ADR(VNC_Password[i])) + 1, 0,SIZEOF(VNC_Password[i]) - brsstrlen(ADR(VNC_Password[i])) - 1 );	// Delete characters after binary 0	
				brsmemcpy(ADR(VNCcodeBuffer[i]),    ADR(VNC_Password[i]),   2);
				brsmemset(ADR(VNCcodeBuffer[i])+2,  66 + i,                 1);
				brsmemcpy(ADR(VNCcodeBuffer[i])+3,  ADR(VNC_Password[i])+2, 6);
				brsmemset(ADR(VNCcodeBuffer[i])+9,  66 + i,                 1);
				brsmemcpy(ADR(VNCcodeBuffer[i])+10, ADR(VNC_Password[i])+8, 2);
				EncodeB64(ADR(VNCcodeBuffer[i]), SIZEOF(VNCcodeBuffer[i]), ADR(TPL_System.In.Cfg.VNC_Password[i]), SIZEOF(TPL_System.In.Cfg.VNC_Password[i]), ADR(VNC_PasswordLength));
			END_FOR
			TPL_Config.In.Cmd.WriteFile := TRUE;
			VNC_Step := 3;	// Now we must set the new passwords!

		3:	// Set VNC-Passwords
			RfbExtSetPassword(VNC_pLib, 0, ADR(VNC_Password[0]));
			RfbExtSetPassword(VNC_pLib, 1, ADR(VNC_Password[1]));
			VNC_Step := 10;	// wait for changes
					
		4:	// Set Headline of the VNC-Viewer
			VNC_Header:=CONCAT (TPL_Netweth.Out.Para.IpAddress,' ');
			VNC_Header:=CONCAT (VNC_Header,TPL_SWID.Out.MachineNameTypeNumber);
			VNC_Header:=LEFT(VNC_Header,49);	// max 49 chars
			RfbExtSetClientCaption(VNC_pLib, ADR(VNC_Header));
			VNC_Step := 10;	// All done, do nothing.

		10:	// Wait
			IF VNC_PasswordsChanged = TRUE THEN
				VNC_PasswordsChanged := FALSE;
				VNC_Step := 2;
			END_IF
			IF EDGEPOS (TPL_Netweth.Out.Stat.EthAddressReady) THEN	// ip has changed -> set VNC headline
				VNC_Step := 4;
			END_IF
	END_CASE

(* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	Time functions
   ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*)
	(* Function to get the Date and Time in DTStructure format *)
	GetTime_0(enable:= 1);
	DT_TO_DTStructure(GetTime_0.DT1, ADR(TPL_System.Out.Info.DateTimeActual));
	(* Function to get the Date and Time in String format *)
	OUTDandT(ADR(TPL_System.Out.Info.DateOut), ADR(TPL_System.Out.Info.TimeOut));

(* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	Remotemode 
   ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*)
	//Bitmapstatus
	IF TPL_System.In.Cfg.OperatingMode = 1 THEN
		RemoteModeButtonStatus := 0;       (* 0 = Visible *)
	ELSE
		RemoteModeButtonStatus := 1;       (* 1 = Invisible *)
	END_IF;
	//Remote Mode to Out variable
	TPL_System.Out.Stat.Remote.Mode :=  TPL_System.In.Cfg.OperatingMode;
	//Remote Status to Out variable
	TPL_System.Out.Stat.Remote.Status :=  TPL_System.In.Remote.Status;
	//Remote Status to Out variable
	RemoteModeBitmap := BOOL_TO_UINT(TPL_System.Out.Stat.Remote.Status);
(* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	Visibility SDM-Button
   ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*)
	StatusGoToSDMPageBtn := SEL(TPL_System.In.Cfg.SDMBtnVisible, 1, 0);
   
(* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	Autologout after Time
   ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*)
	// Uses the screensaver page change function in visualisation.	
	// This function can not be used as it is intended by B+R because of the pagehandling of the template. A page change has to be done only by page handling and not via visualisation functions.
	// So we can us it for the autologout after time function.
	// If the setting is changed the rest time is reset to this setting value.
	// Additional 30 seconds --> see [#461] in rev. Hist.
	TPL_System.In.Cfg.AutoLogoutTimeInMin := MAX(TPL_System.In.Cfg.AutoLogoutTimeInMin, 1);	// The Time should be at least 1 minute.
	IF EDGEPOS(ScreenSaveRestTime < 32) THEN			// Screensaver is about to time out -> reset it
		ScreenSaveTime := TPL_System.In.Cfg.AutoLogoutTimeInMin * 60 + 30 + BOOL_TO_UINT(Toggle);	// the "Toggle" makes shure that the setting changes (1 sec more or less)
		Toggle := NOT(Toggle);
		IF TPL_Config.In.Data.Password.Login.LoginSystem = 1 AND (TPL_Config.In.Data.Password.Login.LoginSystemMode = 2) THEN
			IF (TPL_Password.Out.UserLevel <> 0) AND (TPL_System.In.Cfg.AutoLogoutOn = 1) AND (TPL_UserManager.In.Info.HMI_Ctrl.Status = 0) THEN		// if Autologout is ON than logout
				TPL_Password.In.Cmd.Logout:= 1;
			END_IF
		ELSE
			IF (TPL_Password.Out.UserLevel <> 0) AND (TPL_System.In.Cfg.AutoLogoutOn = 1) THEN		// if Autologout is ON than logout
				TPL_Password.In.Cmd.Logout:= 1;
			END_IF
		END_IF
	END_IF
	IF TPL_System.In.Cfg.AutoLogoutTimeInMin <> OldTimeInMin THEN								// if the AutoLogoutTimeInMin has changes (new value entered)  ...
		OldTimeInMin := TPL_System.In.Cfg.AutoLogoutTimeInMin;									// remember new setting  ...
		ScreenSaveTime := TPL_System.In.Cfg.AutoLogoutTimeInMin * 60 + 30 + BOOL_TO_UINT(Toggle);	// and set screensaver setting time
		Toggle := NOT (Toggle);
	END_IF
	
	IF TPL_Config.In.Data.Password.Login.LoginSystemMode < 2 THEN
		StatusAutologoutTime.0 := NOT(UINT_TO_BOOL(TPL_System.In.Cfg.AutoLogoutOn));				// Show the time-inputfield, if autologout is enabled
		// Show a dialog if the locked inputfield is pressed
		IF StatusAutologoutTime.13 THEN
			StatusAutologoutTime.13 := FALSE;
			TPL_Lockh.In.Cmd.StatusForLockingInputFields := 8192;
		END_IF
	ELSIF TPL_Password.Out.UserLevel > 5 THEN
		StatusAutologoutTime.0 := NOT(UINT_TO_BOOL(TPL_System.In.Cfg.AutoLogoutOn));				// Show the time-inputfield, if autologout is enabled
		IF StatusAutologoutTime.14 THEN
			StatusAutologoutTime.14 := FALSE;
		END_IF
	ELSE
		StatusAutologoutTime := 5;
		TPL_System.In.Cfg.AutoLogoutOn := 1;
	END_IF

(* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	Logbook
   ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*)
	(* Read out logbook every 10000 cycles (100 sec) *)
	IF Counter < 10000 THEN
		Counter:= Counter + 1;
		IF (Counter = 9999) THEN	// last step before reading logbook
			brsmemset(ADR(TPL_System.Out.Info.LogBook), 0, SIZEOF(TPL_System.Out.Info.LogBook));
		END_IF
	ELSIF Counter >= 10000 THEN
		IF entry_nr < (SIZEOF(TPL_System.Out.Info.LogBook) / SIZEOF(TPL_System.Out.Info.LogBook[0])) THEN
			(* Read logbook information *)
			ERRxreadStatus := ERRxread(entry_nr, ADR(ERR_xtyp_struct));

			(* Error type *)
			IF ((ERR_xtyp_struct.err_type = 0) OR (ERR_xtyp_struct.err_type = 1)) THEN
				brsstrcat(ADR(TPL_System.Out.Info.LogBook[entry_nr]), ADR('E'));
			ELSIF (ERR_xtyp_struct.err_type = 2) THEN
				brsstrcat(ADR(TPL_System.Out.Info.LogBook[entry_nr]), ADR('W'));
			ELSIF (ERR_xtyp_struct.err_type = 3) THEN
				brsstrcat(ADR(TPL_System.Out.Info.LogBook[entry_nr]), ADR('I'));
			END_IF
			brsstrcat(ADR(TPL_System.Out.Info.LogBook[entry_nr]), ADR(' '));

			(* Day *)
			brsitoa(ERR_xtyp_struct.err_day, ADR(TmpDate));
			IF brsstrlen(ADR(TmpDate)) = 1 THEN
				brsstrcat(ADR(TPL_System.Out.Info.LogBook[entry_nr]), ADR('0'));
			END_IF
			brsstrcat(ADR(TPL_System.Out.Info.LogBook[entry_nr]), ADR(TmpDate));
			brsstrcat(ADR(TPL_System.Out.Info.LogBook[entry_nr]), ADR('.'));

			(* Month *)
			brsitoa(ERR_xtyp_struct.err_month, ADR(TmpDate));
			IF brsstrlen(ADR(TmpDate)) = 1 THEN
				brsstrcat(ADR(TPL_System.Out.Info.LogBook[entry_nr]), ADR('0'));
			END_IF
			brsstrcat(ADR(TPL_System.Out.Info.LogBook[entry_nr]), ADR(TmpDate));
			brsstrcat(ADR(TPL_System.Out.Info.LogBook[entry_nr]), ADR('.'));

			(* year *)
			brsitoa(ERR_xtyp_struct.err_year, ADR(TmpDate));
			IF brsstrlen(ADR(TmpDate)) = 1 THEN
				brsstrcat(ADR(TPL_System.Out.Info.LogBook[entry_nr]), ADR('0'));
			END_IF
			brsstrcat(ADR(TPL_System.Out.Info.LogBook[entry_nr]), ADR(TmpDate) + 2);
			brsstrcat(ADR(TPL_System.Out.Info.LogBook[entry_nr]), ADR(' '));

			(* hour*)
			brsitoa(ERR_xtyp_struct.err_hour, ADR(TmpDate));
			IF brsstrlen(ADR(TmpDate)) = 1 THEN
				brsstrcat(ADR(TPL_System.Out.Info.LogBook[entry_nr]), ADR('0'));
			END_IF
			brsstrcat(ADR(TPL_System.Out.Info.LogBook[entry_nr]), ADR(TmpDate));
			brsstrcat(ADR(TPL_System.Out.Info.LogBook[entry_nr]), ADR(':'));

			(* minute *)
			brsitoa(ERR_xtyp_struct.err_minute, ADR(TmpDate));
			IF brsstrlen(ADR(TmpDate)) = 1 THEN
				brsstrcat(ADR(TPL_System.Out.Info.LogBook[entry_nr]), ADR('0'));
			END_IF
			brsstrcat(ADR(TPL_System.Out.Info.LogBook[entry_nr]), ADR(TmpDate));
			brsstrcat(ADR(TPL_System.Out.Info.LogBook[entry_nr]), ADR(' '));

			(* error number *)
			brsitoa(ERR_xtyp_struct.err_nr, ADR(TmpDate));
			IF brsstrlen(ADR(TmpDate)) = 4 THEN
				brsstrcat(ADR(TPL_System.Out.Info.LogBook[entry_nr]), ADR('0'));
			END_IF
			brsstrcat(ADR(TPL_System.Out.Info.LogBook[entry_nr]), ADR(TmpDate));
			brsstrcat(ADR(TPL_System.Out.Info.LogBook[entry_nr]), ADR(' '));

			(* module *)
			brsstrcat(ADR(TPL_System.Out.Info.LogBook[entry_nr]), ADR(ERR_xtyp_struct.t_name));
			brsstrcat(ADR(TPL_System.Out.Info.LogBook[entry_nr]), ADR(' '));

			(* module *)
			brsstrcat(ADR(TPL_System.Out.Info.LogBook[entry_nr]), ADR(ERR_xtyp_struct.err_string));

			entry_nr:= entry_nr + 1;
		ELSE
			entry_nr:= 0;
			Counter:= 0;
		END_IF
	END_IF
END_PROGRAM

