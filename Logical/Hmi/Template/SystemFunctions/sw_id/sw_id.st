(************************************************************************************************************************)
(* Object name: sw_id                                                                                                   *)
(* Author:      Samuel Otto                                                                                             *)
(* Site:        B&R Bad Homburg                                                                                         *)
(* Created:     10-feb-2011                                                                                             *)
(* Restriction: This task MUST be the FIRST (or 2nd) task of the project.                                               *)
(* Description: Machine and template software identification and default configuration.                                 *)
(*                                                                                                                      *)
(*----------------------------------------------------------------------------------------------------------------------*)
(* Additional information:                                                                                              *)
(*                                                                                                                      *)
(* No additional information available.                                                                                 *)
(*                                                                                                                      *)
(*----------------------------------------------------------------------------------------------------------------------*)
(* Version 3.00.0  18-jan-2012  Samuel Otto                     Development tool: B&R Automation Studio V3.0.90.19 SP01 *)
(*                                                                                                                      *)
(* Cleaned up this revision history and set version to V3.00.0                                                          *)
(*--------------------------------------------------------------------------------------------------------------------- *)
(*         3.00.1  17-sep-2012  FTG                      Development tool: B&R Automation Studio V3.0.90.21 SP03    	*)
(* all default init of config para in INIT                                                                              *)
(*--------------------------------------------------------------------------------------------------------------------- *)
(*         3.01.0  29-janp-2013  FTG                      Development tool: B&R Automation Studio V3.0.90.21 SP03    	*)
(* template SW-version etc updated                                                                                      *)
(*--------------------------------------------------------------------------------------------------------------------- *)
(*         3.01.1  30-apr-2013  FTG                      Development tool: B&R Automation Studio V3.0.90.21 SP03    	*)
(* hide status line if machine is not template                                                                          *)
(*--------------------------------------------------------------------------------------------------------------------- *)
(*         3.01.2  15-nov-2013  EC      	                Development tool: B&R Automation Studio V3.0.90.21 SP03    	*)
(* wait for set the machine informations on the task mach_id and then create the id informations                        *)
(* delete the machine informations in the task (will be set in the task sw_id)					                        *)
(*--------------------------------------------------------------------------------------------------------------------- *)
(*         3.01.3  22-aug-2014  EC      	                Development tool: B&R Automation Studio V3.0.90.28 SP10    	*)
(* [#416]  Machine name, type and number are not displayed right on page 1						                        *)
(*    - Change condition to return task															                        *)
(* [#422]  Handling of TPL_GlobalStatusText														                        *)
(*    - Remove "TPL_SWID.Out.StatusMachineStatus" because not needed anymore					                        *)
(* Adjust Software version																		                        *)
(************************************************************************************************************************)

PROGRAM _INIT
	(*----------------------------------------------------------------------------------------------------------------------*)
	(* Template information.                                                                                                *)
	(*----------------------------------------------------------------------------------------------------------------------*)
	(* for the following two lines see the task for each configuration in package "configs" *)
	TPL_SWID.In.Cfg.Template.SoftwareVersion		:= 'V4.03.0';				(* Software version number (max  9 Characters) *)
	TPL_SWID.In.Cfg.Template.SoftwareDate			:= '17-JUN-2020';			(* Software date           (max 11 Characters) *)
	TPL_SWID.In.Cfg.Template.SoftwareAuthor			:= 'GEA';	                (* Software engineer       (max 24 Characters) *)
END_PROGRAM

PROGRAM _CYCLIC
	(*----------------------------------------------------------------------------------------------------------------------*)
	(* Wait for Template start up finished and machine information set info                                                 *)
	(*----------------------------------------------------------------------------------------------------------------------*)
	IF NOT(TPL_Config.Out.StartUpFinished) OR NOT(TPL_SWID.In.SetMachineInfo) THEN
		RETURN;
	END_IF
	(*----------------------------------------------------------------------------------------------------------------------*)
	(* Read machine data and make MachineNameTypeNumber                                                                     *)
	(*----------------------------------------------------------------------------------------------------------------------*)
	brsmemset(ADR(TPL_SWID.Out.MachineNameTypeNumber),0,SIZEOF(TPL_SWID.Out.MachineNameTypeNumber)); (* Clear the string *)
	
	IF (brsstrlen(ADR(TPL_SWID.In.Cfg.Machine.Name)) <> 0) AND(brsstrlen(ADR(TPL_SWID.In.Cfg.Machine.Type)) <> 0) AND (brsstrlen(ADR(TPL_SWID.In.Cfg.Machine.Number)) <> 0) THEN
	  brsstrcat(ADR(TPL_SWID.Out.MachineNameTypeNumber), ADR(TPL_SWID.In.Cfg.Machine.Name));
	  brsstrcat(ADR(TPL_SWID.Out.MachineNameTypeNumber), ADR('-'));
	  brsstrcat(ADR(TPL_SWID.Out.MachineNameTypeNumber), ADR(TPL_SWID.In.Cfg.Machine.Type));
	  brsstrcat(ADR(TPL_SWID.Out.MachineNameTypeNumber), ADR('-'));
	  brsstrcat(ADR(TPL_SWID.Out.MachineNameTypeNumber), ADR(TPL_SWID.In.Cfg.Machine.Number));
	ELSIF (brsstrlen(ADR(TPL_SWID.In.Cfg.Machine.Name)) <> 0) AND (brsstrlen(ADR(TPL_SWID.In.Cfg.Machine.Number)) <> 0) THEN
	  brsstrcat(ADR(TPL_SWID.Out.MachineNameTypeNumber), ADR(TPL_SWID.In.Cfg.Machine.Name));
	  brsstrcat(ADR(TPL_SWID.Out.MachineNameTypeNumber), ADR('-'));
	  brsstrcat(ADR(TPL_SWID.Out.MachineNameTypeNumber), ADR(TPL_SWID.In.Cfg.Machine.Number));
	ELSE
	  brsstrcat(ADR(TPL_SWID.Out.MachineNameTypeNumber), ADR(TPL_SWID.In.Cfg.Machine.Name));
	END_IF;
	
	(* Write file, because of Name, Type and Numer first must be read, until the string could be build *)
	TPL_Config.In.Cmd.WriteFile := TRUE;
	
	(*----------------------------------------------------------------------------------------------------------------------*)
	(* Stop executing this task.                                                                                            *)
	(*----------------------------------------------------------------------------------------------------------------------*)
	ST_tmp_suspend(0);
END_PROGRAM


