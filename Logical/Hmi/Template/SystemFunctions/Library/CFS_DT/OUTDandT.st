(************************************************************************************************************************)
(* Object name: Function OUTDandT                                                                                       *)
(* Author:      Samuel Otto                                                                                             *)
(* Site:        B&R Bad Homburg                                                                                         *)
(* Created:     30-nov-2010                                                                                             *)
(* Restriction: No restrictions.                                                                                        *)
(* Description: Function for getting date an time in strings in format '11:49:13', '01.12.10'                           *)
(*                                                                                                                      *)
(*----------------------------------------------------------------------------------------------------------------------*)
(* Additional information:                                                                                              *)
(*                                                                                                                      *)
(* No additional information available.                                                                                 *)
(*                                                                                                                      *)
(*----------------------------------------------------------------------------------------------------------------------*)
(* Version 3.00.0  18-jan-2012  Samuel Otto                     Development tool: B&R Automation Studio V3.0.90.19 SP01 *)
(*                                                                                                                      *)
(* Cleaned up this revision history and set version to V3.00.0                                                          *)
(*--------------------------------------------------------------------------------------------------------------------- *)
(************************************************************************************************************************)

FUNCTION OUTDandT
	(* check for nullpointer *)
	IF AdrDate = 0 OR AdrTime = 0 THEN
		OUTDandT := TPL_ERR_NULLPOINTER;
		RETURN;
	END_IF
	DTGetTime_0(enable := 1);
	IF DTGetTime_0.status <> 0 THEN
		OUTDandT := DTGetTime_0.status;
		RETURN;
	END_IF
	DT_TO_DTStructure(DTGetTime_0.DT1, ADR(DTStructActual));

	(* Hour *)
	cpyStrlength := brsitoa(DTStructActual.hour, ADR(cpyString));
	IF cpyStrlength = 1 THEN
		pCharacter ACCESS AdrTime;
		pCharacter := 48;
		brsstrcpy(AdrTime+1, ADR(cpyString));
	ELSIF cpyStrlength = 2 THEN
		brsstrcpy(AdrTime, ADR(cpyString));
	END_IF
	
	pCharacter ACCESS AdrTime+2;
	pCharacter := 58;	(* : *)

	(* Minute *)
	cpyStrlength := brsitoa(DTStructActual.minute, ADR(cpyString));
	IF cpyStrlength = 1 THEN
		pCharacter ACCESS AdrTime+3;
		pCharacter := 48;
		brsstrcpy(AdrTime+4, ADR(cpyString));
	ELSIF cpyStrlength = 2 THEN
		brsstrcpy(AdrTime+3, ADR(cpyString));
	END_IF
	
	pCharacter ACCESS AdrTime+5;
	pCharacter := 58;	(* : *)

	(* Second *)
	cpyStrlength := brsitoa(DTStructActual.second, ADR(cpyString));
	IF cpyStrlength = 1 THEN
		pCharacter ACCESS AdrTime+6;
		pCharacter := 48;
		brsstrcpy(AdrTime+7, ADR(cpyString));
	ELSIF cpyStrlength = 2 THEN
		brsstrcpy(AdrTime+6, ADR(cpyString));
	END_IF
	
	pCharacter ACCESS AdrTime+8;
	pCharacter := 0;	(* /0 *)


	(* Day *)
	cpyStrlength := brsitoa(DTStructActual.day, ADR(cpyString));
	IF cpyStrlength = 1 THEN
		pCharacter ACCESS AdrDate;
		pCharacter := 48;
		brsstrcpy(AdrDate+1, ADR(cpyString));
	ELSIF cpyStrlength = 2 THEN
		brsstrcpy(AdrDate, ADR(cpyString));
	END_IF
	
	pCharacter ACCESS AdrDate+2;
	pCharacter := 46;	(* . *)

	(* Month *)
	cpyStrlength := brsitoa(DTStructActual.month, ADR(cpyString));
	IF cpyStrlength = 1 THEN
		pCharacter ACCESS AdrDate+3;
		pCharacter := 48;
		brsstrcpy(AdrDate+4, ADR(cpyString));
	ELSIF cpyStrlength = 2 THEN
		brsstrcpy(AdrDate+3, ADR(cpyString));
	END_IF
	
	pCharacter ACCESS AdrDate+5;
	pCharacter := 46;	(* . *)

	(* Year *)
	cpyStrlength := brsitoa(DTStructActual.year, ADR(cpyString)); (* length of Year is always 4... until year 10000... *)
	brsmemcpy(AdrDate+6, ADR(cpyString)+2 ,2); (* copy only the last two digits *)
	
	pCharacter ACCESS AdrDate+8;
	pCharacter := 0;	(* /0 *)

	OUTDandT := 0;
	RETURN;
END_FUNCTION
