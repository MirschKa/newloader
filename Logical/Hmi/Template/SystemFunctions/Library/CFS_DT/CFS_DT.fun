
FUNCTION OUTDandT : UINT
	VAR_INPUT
		AdrDate : UDINT;
		AdrTime : UDINT;
	END_VAR
	VAR
		DTGetTime_0 : DTGetTime; (*Function Block for reading system Time*)
		DTStructActual : DTStructure; (*structure with the system Time*)
		pCharacter : REFERENCE TO USINT; (*internal pointer to a character*)
		cpyString : STRING[4]; (*internal copy-string (year has 4 signs)*)
		cpyStrlength : UINT;
	END_VAR
END_FUNCTION
