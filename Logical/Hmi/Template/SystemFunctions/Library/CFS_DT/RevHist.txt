
(* !!! Note here, in which FBKs and functions changes were made. Note the exact describtion in the FBKs !!! *)
(* !!! Use after every change a new version-number of the complete library, also if not all FBKs were changed !!! *)


(************************************************************************************************************************)
(* Object name: CFS_DT                                                                                                  *)
(* Description:                                                                                                         *)
(*                                                                                                                      *)
(*----------------------------------------------------------------------------------------------------------------------*)
(* Additional information:                                                                                              *)
(*                                                                                                                      *)
(* No additional information available.                                                                                 *)
(*                                                                                                                      *)
(*----------------------------------------------------------------------------------------------------------------------*)
(* make your entries here                                                                                               *)
(*--------------------------------------------------------------------------------------------------------------------- *)
(************************************************************************************************************************)




(* Sample entry *)


(* Version 3.01.0  18-jan-2012  Samuel Otto                     Development tool: B&R Automation Studio V3.0.90.19 SP01 *)
(*                                                                                                                      *)
(* Changes on FBK a, b, c... For more details see the RevHist in each FBK.                                              *)
(*--------------------------------------------------------------------------------------------------------------------- *)