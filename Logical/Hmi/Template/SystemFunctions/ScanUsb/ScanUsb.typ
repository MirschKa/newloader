
TYPE
	ScanUsbEnumType : 
		(
		ScanUsbStep_Init := 0,
		ScanUsbStep_SearchForStick := 10,
		ScanUsbStep_SearchNextStick := 11,
		ScanUsbStep_GetNode := 20,
		ScanUsbStep_MountDevice := 30,
		ScanUsbStep_WaitForUsbUnplugged := 40,
		ScanUsbStep_UnmountDevice := 50,
		ScanUsbStep_Clear := 60,
		ScanUsbStep_Error := 100
		);
	ScanUsbType : 	STRUCT 
		Sequenze : ScanUsbEnumType;
		UsbNodeListGet : UsbNodeListGet;
		UsbNodeGet : UsbNodeGet;
		DevLink : DevLink;
		DevUnlink : DevUnlink;
		USB_Stick : usbNode_typ;
		USBNodeListBuf : ARRAY[0..24]OF UDINT;
		DeviceParameter : STRING[150];
		i : USINT;
	END_STRUCT;
END_TYPE
