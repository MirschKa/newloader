(**************************************************************************************************************************)
(* Object name: ScanUsb                                                                                                   *)
(* Author:      Samuel Otto                                                                                               *)
(* Site:        B&R Bad Homburg                                                                                           *)
(* Created:     15-okt-2013                                                                                               *)
(* Restriction: -                                                                                                         *)
(* Description: Searching and mounting usb-sticks                                                                         *)
(*                                                                                                                        *)
(*------------------------------------------------------------------------------------------------------------------------*)
(* Additional information:                                                                                                *)
(*                                                                                                                        *)
(* No additional information available.                                                                                   *)
(*                                                                                                                        *)
(*------------------------------------------------------------------------------------------------------------------------*)
(* Version 1.00.0  15-okt-2013  Samuel Otto                     Development tool: B&R Automation Studio V3.0.90.27 SP09   *)
(*                                                                                                                        *)
(* Created                                                                                                                *)
(*----------------------------------------------------------------------------------------------------------------------- *)
(* Version 	1.00.1  04-sep-2015  Erdal Cilingir                  Development tool: B&R Automation Studio V3.0.90.30 SP12  *)
(* [#468] USB Backup not work if Technology Guard is plugged															  *)
(*		  Do optimazation inside task (Enumeration, naming, comments,..)												  *)
(*		  If Usb Device is find then it will be checked for vendorId and productId to now if it is B&R Technology Guard	  *)
(*		  Make Usb information available for others inside TPL_UsbScan global variable                             	      *)
(**************************************************************************************************************************)
PROGRAM _INIT
END_PROGRAM

PROGRAM _CYCLIC
	CASE Scan_Usb.Sequenze OF  
        //**********************************************************************************************************
        //          Step: INIT
        //**********************************************************************************************************
        ScanUsbStep_Init:
            brsmemset(ADR(TPL_UsbScan),0,SIZEOF(TPL_UsbScan));
            Scan_Usb.Sequenze := ScanUsbStep_SearchForStick;
            
        //**********************************************************************************************************
        //          Step: Seach for Stick
        //**********************************************************************************************************
		ScanUsbStep_SearchForStick:
			Scan_Usb.UsbNodeListGet.enable						:= TRUE;
			Scan_Usb.UsbNodeListGet.pBuffer					    := ADR(Scan_Usb.USBNodeListBuf);
			Scan_Usb.UsbNodeListGet.bufferSize					:= SIZEOF(Scan_Usb.USBNodeListBuf);
			Scan_Usb.UsbNodeListGet.filterInterfaceClass		:= asusb_CLASS_MASS_STORAGE;
			Scan_Usb.UsbNodeListGet.filterInterfaceSubClass	    := asusb_SUBCLASS_SCSI_COMMAND_SET;
			Scan_Usb.UsbNodeListGet();
			IF Scan_Usb.UsbNodeListGet.status = 0 THEN
                TPL_UsbScan.NumberOfUsbDevices := Scan_Usb.UsbNodeListGet.listNodes;
				IF Scan_Usb.UsbNodeListGet.listNodes = 1 OR Scan_Usb.UsbNodeListGet.listNodes = 2 THEN
					Scan_Usb.Sequenze           := ScanUsbStep_GetNode;
                    Scan_Usb.i                  :=0;
				ELSE
					Scan_Usb.Sequenze           := ScanUsbStep_Clear;
				END_IF
			ELSIF Scan_Usb.UsbNodeListGet.status = asusbERR_USB_NOTFOUND THEN
                TPL_UsbScan.NumberOfUsbDevices  := 0;
				Scan_Usb.Sequenze               := ScanUsbStep_Clear;
			ELSIF Scan_Usb.UsbNodeListGet.status <> 65535 THEN
				Scan_Usb.Sequenze               := ScanUsbStep_Error;
			END_IF
        //**********************************************************************************************************
        //          Step: GetNode
        //**********************************************************************************************************
		ScanUsbStep_GetNode: 
            Scan_Usb.UsbNodeGet.enable		    := TRUE;
			Scan_Usb.UsbNodeGet.nodeId		    := Scan_Usb.USBNodeListBuf[Scan_Usb.i];
			Scan_Usb.UsbNodeGet.pBuffer	        := ADR(Scan_Usb.USB_Stick);
			Scan_Usb.UsbNodeGet.bufferSize	    := SIZEOF(Scan_Usb.USB_Stick);
			Scan_Usb.UsbNodeGet();
			IF Scan_Usb.UsbNodeGet.status = 0 THEN
                IF Scan_Usb.USB_Stick.vendorId = BR_TG_VENDOR_ID AND Scan_Usb.USB_Stick.productId = BR_TG_PRODUCT_ID THEN
                    Scan_Usb.Sequenze           := ScanUsbStep_SearchNextStick;
                    brsmemset(ADR(Scan_Usb.USB_Stick),0,SIZEOF(Scan_Usb.USB_Stick));
                ELSE
    				Scan_Usb.DeviceParameter	:= '/DEVICE=';
    				brsstrcat(ADR(Scan_Usb.DeviceParameter), ADR(Scan_Usb.USB_Stick.ifName));
    				Scan_Usb.Sequenze           := ScanUsbStep_MountDevice;
                END_IF
			ELSIF Scan_Usb.UsbNodeGet.status = asusbERR_USB_NOTFOUND THEN
				Scan_Usb.Sequenze               := ScanUsbStep_Clear;
			ELSIF Scan_Usb.UsbNodeGet.status <> 65535 THEN
				Scan_Usb.Sequenze               := ScanUsbStep_Error;
			END_IF
        //**********************************************************************************************************
        //          Step: Seach for Next Stick
        //**********************************************************************************************************
        ScanUsbStep_SearchNextStick:  
            Scan_Usb.i                  :=  Scan_Usb.i+1; 
            Scan_Usb.Sequenze           :=  ScanUsbStep_GetNode;
        //**********************************************************************************************************
        //          Step: Mount USB Device
        //**********************************************************************************************************
        ScanUsbStep_MountDevice:
			Scan_Usb.DevLink.enable	    :=  TRUE;
			Scan_Usb.DevLink.pDevice	:=  ADR('USB1');
			Scan_Usb.DevLink.pParam	    :=  ADR(Scan_Usb.DeviceParameter);
			Scan_Usb.DevLink();
			IF Scan_Usb.DevLink.status = 0 THEN
				Scan_Usb.Sequenze       :=  ScanUsbStep_WaitForUsbUnplugged;
			ELSIF Scan_Usb.DevLink.status <> 65535 THEN
				Scan_Usb.Sequenze       :=  ScanUsbStep_Error;
			END_IF
        //**********************************************************************************************************
        //          Step: Wait for unplug of usb device
        //**********************************************************************************************************
		ScanUsbStep_WaitForUsbUnplugged:
			Scan_Usb.UsbNodeGet.enable		:= TRUE;
			Scan_Usb.UsbNodeGet.nodeId		:= Scan_Usb.USBNodeListBuf[Scan_Usb.i];
			Scan_Usb.UsbNodeGet.pBuffer	    := ADR(Scan_Usb.USB_Stick);
			Scan_Usb.UsbNodeGet.bufferSize	:= SIZEOF(Scan_Usb.USB_Stick);
			Scan_Usb.UsbNodeGet();
            TPL_UsbScan.ProductId           := Scan_Usb.USB_Stick.productId;
            TPL_UsbScan.VendorId            := Scan_Usb.USB_Stick.vendorId;
            TPL_UsbScan.UsbStickAvailable   := TRUE;
            TPL_UsbScan.MountedUsbNode      := Scan_Usb.UsbNodeGet.nodeId;
            TPL_UsbScan.MountedUsbInterface := Scan_Usb.DeviceParameter;
			IF Scan_Usb.UsbNodeGet.status = 0 THEN
				// nichts machen, stick ist noch da.
			ELSIF Scan_Usb.UsbNodeGet.status <> 65535 THEN
				Scan_Usb.Sequenze           := ScanUsbStep_UnmountDevice;
			END_IF
        //**********************************************************************************************************
        //          Step: unmount device
        //**********************************************************************************************************
		ScanUsbStep_UnmountDevice:	// unmount
			Scan_Usb.DevUnlink.enable	:= TRUE;
			Scan_Usb.DevUnlink.handle	:= Scan_Usb.DevLink.handle;
			Scan_Usb.DevUnlink();
			IF Scan_Usb.DevUnlink.status = 0 THEN
				Scan_Usb.Sequenze       := ScanUsbStep_Clear;
			ELSIF Scan_Usb.DevUnlink.status <> ERR_FUB_BUSY THEN
				Scan_Usb.Sequenze       := ScanUsbStep_Error;
			END_IF
        //**********************************************************************************************************
        //          Step: Clear
        //**********************************************************************************************************
		ScanUsbStep_Clear:
            TPL_UsbScan.ProductId           := 0;
            TPL_UsbScan.VendorId            := 0;
            TPL_UsbScan.UsbStickAvailable   := FALSE;
            TPL_UsbScan.MountedUsbNode      := 0;
            TPL_UsbScan.MountedUsbInterface := '';
			Scan_Usb.Sequenze               := ScanUsbStep_SearchForStick;
        //**********************************************************************************************************
        //          Step: Error
        //**********************************************************************************************************
		ScanUsbStep_Error:;
	END_CASE
END_PROGRAM

