Option Explicit

' --------------------------------------------------------------------------
' Objectname		: "BuildInfo_Tool.vbs"
' Author 			: Nelson da Costa Moreira
' Site				: GEA CFS Germany GmbH - Niederlassung GEA CFS Wallau
' Created			: 2011-09-26
' Restriction		: builddat.prg in Projectfolder "BuildInfo";
'					  Project path may contain only one folder named "Logical".
'
' Description		: This script updates specific parts in the file "buildInfo.st"
'					  and will be executed by the Automation Studio _before_ a build process is started.
'					  It inserts the project name, date/time, windows user name and computer name.
'
' Version	Date		Author						Comment
' --------------------------------------------------------------------------
' 1.00		2011-09-26	Nelson da Costa Moreira		Official release
' --------------------------------------------------------------------------


Const ForAppending = 8
Const ForReading = 1
Const ForWriting = 2

Const TristateFalse = 0
Const TristateMixed = -2
Const TristateTrue = -1
Const TristateUseDefault = -2


Dim oFSO
Dim oSourceFile, oUpdatedFile
Dim oRegExp
Dim strFileContent, strReplaced, strProjectName, strConfiguration
Dim strAT, dtAT
Dim oWshNetwork
Dim strSourceFile, strUserName, strComputerName
Dim strDestinationFile
Dim argsNamed



	' Display info about this script
	WScript.Echo "------------------------------------------------------------------------------------"
	WScript.Echo " BuildInfo_Tool.vbs, (c) 2011 GEA CT"
	WScript.Echo "------------------------------------------------------------------------------------"
	WScript.Echo
	WScript.Echo " Updating build information in file <buildInfo.st> ..."
	WScript.Echo
	
	' Get named parameters passed to the script
	Set argsNamed = WScript.Arguments.Named

	' Build absolute file name to the task file "buildInfo.st"
	strSourceFile = Left(WScript.ScriptFullName, Len(Wscript.ScriptFullName) - Len(WScript.ScriptName)) & "buildInfo.txt"
	strDestinationFile = Left(WScript.ScriptFullName, Len(Wscript.ScriptFullName) - Len(WScript.ScriptName)) & "buildInfo.st"
	
	Set oFSO = CreateObject("Scripting.FileSystemObject")
	
	' Open BuiltInf.st to set the variables
	If (oFSO.FileExists(strSourceFile)) Then
		
		Set oSourceFile = oFSO.OpenTextFile(strSourceFile, ForReading, False, TristateFalse)
		strFileContent = oSourceFile.ReadAll
		oSourceFile.Close
		Set oSourceFile = Nothing
		
		' Create regular expression object that helps us finding special tokens
		Set oRegExp = New RegExp
		
		oRegExp.Global = True
		oRegExp.IgnoreCase = False
		oRegExp.MultiLine = True
		
		' ==========================================================
		' Replace "BuildInfo_Timestamp" assignment.
		' ==========================================================
		oRegExp.Pattern = "BuildInfo_Timestamp[ \t]* :=[ \t]*'.*'[ \t]*;"
		
		dtAT = Now()
		strAT = Year(dtAT) & "-" & Right("0" & Month(dtAT), 2) & "-" & Right("0"& Day(dtAT), 2) & _
				"T" & Right("0" & Hour(dtAT), 2) & ":" & Right("0" & Minute(dtAT), 2) & ":" & Right("0" & Second(dtAT), 2)
		
		strReplaced = oRegExp.Replace(strFileContent, "BuildInfo_Timestamp := '" & Left(strAT, 31) & "';")

		WScript.Echo " BuildInfo_Timestamp := '" & Left(strAT, 31) & "';"
		
		' ==========================================================
		' Replace "BuildInfo_ProjectName" assignment.
		' ==========================================================
		oRegExp.Pattern = "BuildInfo_ProjectName[ \t]* :=[ \t]*'.*'[ \t]*;"

		' The project name is passed to script via commandline parameter /ProjectName:"projectName".
		' Example: > BuildInfo_Tool.vbs /ProjectPath:"$(WIN32_AS_PROJECT_NAME)"
		If ( argsNamed.Exists("ProjectName") ) Then
			
			strProjectName = argsNamed.Item("ProjectName")
			
			If (strProjectName = "") Then
				strProjectName = "<empty>"
			End If
			
		Else
			' Logical folder cannot be found. This is an invalid AS project folder structure.
			strProjectName = "<missing>"
		End If
		
		
		strReplaced = oRegExp.Replace(strReplaced, "BuildInfo_ProjectName := '" & Left(strProjectName, 31) & "';")
		
		WScript.Echo " BuildInfo_ProjectName := '" & Left(strProjectName, 31) & "';"
		
		' ==========================================================
		' Replace "BuildInfo_Configuration" assignment.
		' ==========================================================
		oRegExp.Pattern = "BuildInfo_Configuration[ \t]* :=[ \t]*'.*'[ \t]*;"

		' The Configuration is passed to the script via commandline parameter /Configuration:"configuration".
		If ( argsNamed.Exists("Configuration") ) Then
			
			strConfiguration = argsNamed.Item("Configuration")
			
			If (strConfiguration = "") Then
				strConfiguration = "<empty>"
			End If
			
		Else
			' Logical folder cannot be found. This is an invalid AS project folder structure.
			strConfiguration = "<missing>"
		End If
		
		
		strReplaced = oRegExp.Replace(strReplaced, "BuildInfo_Configuration := '" & Left(strConfiguration, 31) & "';")
		
		WScript.Echo " BuildInfo_Configuration := '" & Left(strConfiguration, 31) & "';"

		' ==========================================================
		' Replace "BuildInfo_UserName" assignment.
		' ==========================================================
		Set oWshNetwork = CreateObject("WScript.Network")
		
		oRegExp.Pattern = "BuildInfo_UserName[ \t]* :=[ \t]*'.*'[ \t]*;"
		
		strUserName = oWshNetwork.UserName
		
		strReplaced = oRegExp.Replace(strReplaced, "BuildInfo_UserName := '" & Left(strUserName, 31) & "';")
		
		WScript.Echo " BuildInfo_UserName := '" & Left(strUserName, 31) & "';"
		
		' ==========================================================
		' Replace "BuildInfo_ComputerName" assignment.
		' ==========================================================
		oRegExp.Pattern = "BuildInfo_ComputerName[ \t]* :=[ \t]*'.*'[ \t]*;"
		
		strComputerName = oWshNetwork.ComputerName
		
		strReplaced = oRegExp.Replace(strReplaced, "BuildInfo_ComputerName := '" & Left(strComputerName, 31) & "';")
		
		WScript.Echo " BuildInfo_ComputerName := '" & Left(strComputerName, 31) & "';"
		
		Set oWshNetwork = Nothing
		
'###		' Backup the original file and replace it with new file
		' Remove old file, if existing
		If (oFSO.FileExists(strDestinationFile)) Then
			Call oFSO.DeleteFile(strDestinationFile)
		End If
		
		' Rename original
'		Set oSourceFile = oFSO.GetFile(strSourceFile)
'		oSourceFile.Name = "buildInfo.st" 'oSourceFile.Name & ".bak"
'		Set oSourceFile = Nothing
		
		' Write the new file
		Set oUpdatedFile = oFSO.OpenTextFile(strDestinationFile, ForWriting, True, TristateFalse)
		oUpdatedFile.Write strReplaced
		oUpdatedFile.Close
		
		WScript.Echo
		WScript.Echo " ... OK!"
		
	Else	
		Set oFSO = Nothing
		
		WScript.Echo
		WScript.Echo "Error: builtInfo.st could not be found!"
		
	End If
	
	Set argsNamed = Nothing
	
	WScript.Echo
	WScript.Echo "------------------------------------------------------------------------------------"
