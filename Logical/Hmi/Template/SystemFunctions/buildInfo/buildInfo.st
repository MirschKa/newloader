(************************************************************************************************************************)
(* Object name: buildInfo                                                                                               *)
(* Author:      Nelson da Costa Moreira                                                                                 *)
(* Site:        GEA CFS Germany GmbH - Niederlassung GEA CFS Wallau                                                     *)
(* Created:     26-sep-2011                                                                                             *)
(* Restriction: None.                                                                                                   *)
(* Description: Build information                                                                                       *)
(*                                                                                                                      *)
(*----------------------------------------------------------------------------------------------------------------------*)
(* Additional information:                                                                                              *)
(*    This task contains information about the user that has initiated the last build run of this project.              *)
(*    The task will automatically be updated before a build is executed (pre-build event of AS3.x).                     *)
(*                                                                                                                      *)
(*----------------------------------------------------------------------------------------------------------------------*)
(* Version 3.00.0  18-jan-2012  Samuel Otto                     Development tool: B&R Automation Studio V3.0.90.19 SP01 *)
(*                                                                                                                      *)
(* Cleaned up this revision history and set version to V3.00.0                                                          *)
(*--------------------------------------------------------------------------------------------------------------------- *)
(* Version 3.04.1  20-apr-2015  Nelson da Costa Moreira         Development tool: B&R Automation Studio V3.0.90.28 SP10 *)
(*                                                                                                                      *)
(* [#460] BuildInfo package causes commit overhead with Subversion/Git													*)
(*--------------------------------------------------------------------------------------------------------------------- *)
(************************************************************************************************************************)

PROGRAM _INIT
(*
	DO NOT CHANGE ANYTHING IN THIS SECTION.
	THIS FILE IS BEING CHANGED PROGRAMMATICALLY!
*)
	// Set the BuildInfo variables


	BuildInfo_Timestamp := '2020-06-17T11:20:19';

	BuildInfo_ProjectName := 'Template_V4_03_0';
	BuildInfo_Configuration := 'APC2100_BY44_10';

	BuildInfo_UserName := 'Becker.Fl';
	BuildInfo_ComputerName := 'NB43112WAL9140';
	
(*----------------------------------------------------------------------------------------------------------------------*)
(* Stop executing this task.                                                                                            *)
(*----------------------------------------------------------------------------------------------------------------------*)
ST_tmp_suspend(0);

END_PROGRAM

PROGRAM _CYCLIC
	
	// This task has no cyclic code!
	
END_PROGRAM
