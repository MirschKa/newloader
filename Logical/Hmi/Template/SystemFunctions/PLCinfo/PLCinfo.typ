
TYPE
	PLCinternal_Type : 	STRUCT 
		SerialNumber_CPU : UDINT;
		ModuleID_CPU : UINT;
		PowerOnCycles_CPU : UDINT;
		OperatingHours_CPU : UDINT;
		MtcxVersMajor : USINT;
		MtcxVersMinor : USINT;
		Temperature_CPU : UINT;
		Temperature_RAM : UINT;
		FreeSRAM : UDINT;
		FreeUserROM : UDINT;
		FreeDRAM : UDINT;
		FreeSystemROM : UDINT;
		Version_AR : STRING[12];
	END_STRUCT;
END_TYPE
