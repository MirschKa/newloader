(************************************************************************************************************************)
(* Object name: PLCinfo                                                                                                 *)
(* Author:      Dominik Benner                                                                                          *)
(* Site:        GEA Food Solutions Germany GmbH Wallau                                                                  *)
(* Created:     21-april-2020                                                                                           *)
(* Restriction: consider task order.                                                                                    *)
(* Description: Get PLC hardware information                                                                            *)
(*                                                                                                                      *)
(*----------------------------------------------------------------------------------------------------------------------*)
(* Additional information:                                                                                              *)
(*                                                                                                                      *)
(* No additional information available.                                                                                 *)
(*                                                                                                                      *)
(*----------------------------------------------------------------------------------------------------------------------*)
(* Version 1.00.0  21-april-2020  Dominik Benner                Development tool: B&R Automation Studio V4.5.3.86 SP    *)
(*                                                                                                                      *)
(* First version                                                     													*)
(*--------------------------------------------------------------------------------------------------------------------- *)
PROGRAM _INIT
	// enable initial information pickup process for MEM and AR
	startupMEM	:= TRUE;
	startupAR 	:= TRUE;
	 
END_PROGRAM

PROGRAM _CYCLIC

	// Common PLC information
	// CPU serial number 
	IF PLCinternal.SerialNumber_CPU <> 0 THEN
		TPL_PLCinfo.Out.SerialNumber_CPU := UDINT_TO_STRING(PLCinternal.SerialNumber_CPU);
	ELSE
		TPL_PLCinfo.Out.SerialNumber_CPU := '-';
	END_IF
	
	
	// CPU module ID
	IF PLCinternal.ModuleID_CPU <> 0 THEN
		TPL_PLCinfo.Out.ModuleID_CPU := UDINT_TO_STRING(PLCinternal.ModuleID_CPU);
	ELSE
		TPL_PLCinfo.Out.ModuleID_CPU := '-';
	END_IF
	
	
	// PowerOnCycles CPU
	IF PLCinternal.PowerOnCycles_CPU <> 0 THEN
		TPL_PLCinfo.Out.PowerOnCycles_CPU := UDINT_TO_STRING(PLCinternal.PowerOnCycles_CPU);
	ELSE
		TPL_PLCinfo.Out.PowerOnCycles_CPU := '-';
	END_IF
	
	
	// OperatingHours CPU
	IF PLCinternal.OperatingHours_CPU <> 0 THEN
		TPL_PLCinfo.Out.OperatingHours_CPU := UDINT_TO_STRING(PLCinternal.OperatingHours_CPU);
	ELSE
		TPL_PLCinfo.Out.OperatingHours_CPU := '-';
	END_IF
	
	
	// Temperature CPU
	IF PLCinternal.Temperature_CPU <> 0 THEN
		//Output of cpu temp in 1/10 degree Celsius ->adjust displayed value
		TempCPU := PLCinternal.Temperature_CPU;
		TempCPU := TempCPU/10;
		TPL_PLCinfo.Out.Temperature_CPU := REAL_TO_STRING(TempCPU);
	ELSE
		TPL_PLCinfo.Out.Temperature_CPU := '-';
	END_IF
	
	
	// Temperature RAM
	IF PLCinternal.Temperature_RAM <> 0 THEN
		//Output of ram temp in 1/10 degree Celsius ->adjust displayed value
		TempRAM := PLCinternal.Temperature_RAM;
		TempRAM := TempRAM/10;
		TPL_PLCinfo.Out.Temperature_RAM := REAL_TO_STRING(TempRAM);
	ELSE
		TPL_PLCinfo.Out.Temperature_RAM := '-';
	END_IF
		
	
	// Mtcx version
	IF PLCinternal.MtcxVersMajor <> 0 OR PLCinternal.MtcxVersMinor <> 0 THEN
		TPL_PLCinfo.Out.MtcxVersion := USINT_TO_STRING(PLCinternal.MtcxVersMajor);
		MtcxMinor := USINT_TO_STRING(PLCinternal.MtcxVersMinor);
		brsstrcat(ADR(TPL_PLCinfo.Out.MtcxVersion),ADR("."));
		brsstrcat(ADR(TPL_PLCinfo.Out.MtcxVersion),ADR(MtcxMinor));
	ELSE
		TPL_PLCinfo.Out.MtcxVersion := '-';
	END_IF
	
	
	// Get PLC memory info - Refresh every 15min
	TON_RefreshMem(IN:=TRUE, PT:=T#15m);
	IF TON_RefreshMem.Q OR startupMEM THEN
		startupMEM := FALSE;
		TON_RefreshMem(IN:=FALSE);
		
		MEMInfo_0(enable := TRUE);
		IF MEMInfo_0.status = ERR_OK THEN
			PLCinternal.FreeSRAM 		:= MEMInfo_0.FreeUSR_Ram;
			PLCinternal.FreeUserROM 	:= MEMInfo_0.FreeUSR_Prom;
			PLCinternal.FreeSystemROM	:= MEMInfo_0.FreeSYS_Prom;
			PLCinternal.FreeDRAM		:= MEMInfo_0.FreeTMP_Ram;
			IF PLCinternal.FreeSRAM <> 0 THEN 
				TPL_PLCinfo.Out.FreeSRAM := UDINT_TO_STRING(PLCinternal.FreeSRAM);
			ELSE
				TPL_PLCinfo.Out.FreeSRAM := '';
			END_IF
			IF PLCinternal.FreeUserROM <> 0 THEN 
				TPL_PLCinfo.Out.FreeUserROM := UDINT_TO_STRING(PLCinternal.FreeUserROM);
			ELSE
				TPL_PLCinfo.Out.FreeUserROM := '';
			END_IF	
			IF PLCinternal.FreeSystemROM <> 0 THEN 
				TPL_PLCinfo.Out.FreeSystemROM := UDINT_TO_STRING(PLCinternal.FreeSystemROM);
			ELSE
				TPL_PLCinfo.Out.FreeSystemROM := '';
			END_IF				
			IF PLCinternal.FreeDRAM <> 0 THEN 
				TPL_PLCinfo.Out.FreeDRAM := UDINT_TO_STRING(PLCinternal.FreeDRAM);
			ELSE
				TPL_PLCinfo.Out.FreeDRAM := '';
			END_IF					
		ELSE
			TPL_PLCinfo.Out.FreeSRAM := TPL_PLCinfo.Out.FreeUserROM := TPL_PLCinfo.Out.FreeSystemROM := TPL_PLCinfo.Out.FreeDRAM := '';
		END_IF
	END_IF
	
	
	// Get AR version of target
	IF startupAR THEN
		startupAR := FALSE;
		TARGETInfo_0(enable:=TRUE, pOSVersion:=ADR(PLCinternal.Version_AR));
		IF TARGETInfo_0.status = ERR_OK THEN
			TPL_PLCinfo.Out.Version_AR := PLCinternal.Version_AR;
		ELSE
			TPL_PLCinfo.Out.Version_AR := '';
		END_IF
	END_IF
	

END_PROGRAM


