(************************************************************************************************************************)
(* Object name: netweth                                                                                                 *)
(* Author:      Peter Hauser                                                                                            *)
(* Site:        B&R Eggelsberg                                                                                          *)
(* Created:     05-sep-2003                                                                                             *)
(* Restriction: Consider task order; see also template help file.                                                       *)
(* Description: Get / Set Properties for Ethernet.                                                                      *)
(*                                                                                                                      *)
(*----------------------------------------------------------------------------------------------------------------------*)
(* Additional information:                                                                                              *)
(*                                                                                                                      *)
(* No additional information available.                                                                                 *)
(*                                                                                                                      *)
(*----------------------------------------------------------------------------------------------------------------------*)
(* Version 3.00.0  18-jan-2012  Samuel Otto                     Development tool: B&R Automation Studio V3.0.90.19 SP01 *)
(*                                                                                                                      *)
(* Cleaned up this revision history and set version to V3.00.0                                                          *)
(*----------------------------------------------------------------------------------------------------------------------*)
(* Version 3.00.1  19-Sep-2012  FTG                             Development tool: B&R Automation Studio V3.0.90.21 SP03 *)
(* - Ethernet device name for windows terminal                                                                          *)
(*----------------------------------------------------------------------------------------------------------------------*)
(* Version 3.00.2  14-Dec-2012  FTG                             Development tool: B&R Automation Studio V3.0.90.21 SP03 *)
(* - set TPL_Netweth.Out.Stat.Busy correctly                                                                            *)
(*--------------------------------------------------------------------------------------------------------------------- *)
(* Version 3.04.1  22-apr-2015  SO                              Development tool: B&R Automation Studio V3.0.90.28 SP10 *)
(* - PanelType TPL_SIM hast the same ethernet device (IF3) like TPL_APC                                                 *)
(*--------------------------------------------------------------------------------------------------------------------- *)
(* Version 4.00.0  05-oct-2015  SO                              Development tool: B&R Automation Studio V4.1.7.61       *)
(* - IssueList Item [#450] SDM not working on PP520 in AS 4.0.19.069													*)
(* 		-  In AS4 the ETH-Interface was remaned from IF5 to IF3.														*)
(*--------------------------------------------------------------------------------------------------------------------- *)
(* Version 4.00.1	 11-mar-2018	AJ							Development tool: B&R Automation Studio V4.3.4.121      *)
(* - Added Message during saving of system settings on page 5300														*)
(*--------------------------------------------------------------------------------------------------------------------- *)
(* Version 4.01.0	 08-aug-2019	DOB							Development tool: B&R Automation Studio V4.5.2.102      *)
(* - ID-507:Added check of IP/ PWL settings to avaoid address conflicts between interfaces (mod code from FTG)			*)
(* - Integrated NodeNr Setter/Getter sequence into IP Getter/Setter sequence 											*)
(* - Added messages																										*)
(*--------------------------------------------------------------------------------------------------------------------- *)
(************************************************************************************************************************)

PROGRAM _INIT
//	TPL_Config.In.Data.Netweth.IPAddressRemoteStationValue[0] := 0;	(* Set the IP-Adress for the remote partner 0.0.0.0 *)
//	TPL_Config.In.Data.Netweth.IPAddressRemoteStationValue[1] := 0;
//	TPL_Config.In.Data.Netweth.IPAddressRemoteStationValue[2] := 0;
//	TPL_Config.In.Data.Netweth.IPAddressRemoteStationValue[3] := 0;
//
//	TPL_Config.In.Data.Sys.NodeNumber := 2;		 (* Set the node number of the panel (Between 1 and 255) *)          
//	
	// Preset Ethstep
	Ethstep:= WAIT;
	
	//	Preset NodeStep
	NodeStep := NODENR_WAIT_FOR_FILE;
	INT_Netweth.OptionVolatile := cfgOPTION_NON_VOLATILE;
	
	// calc network part of PLK/ PLK NAT address
	ethInetAton(ADR(PLK_IPstring),ADR(PLK_IP));
	ethInetAton(ADR(PLK_SNstring),ADR(PLK_SN));
	PLK_Net 	:= PLK_IP AND PLK_SN;	
	ethInetAton(ADR(PLK_NAT_IPstring),ADR(PLK_NAT_IP));
	ethInetAton(ADR(PLK_NAT_SNstring),ADR(PLK_NAT_SN));
	PLK_NAT_Net	:= PLK_NAT_IP AND PLK_NAT_SN;

END_PROGRAM


PROGRAM _CYCLIC
	
(*-----External signals-----*)
	INT_Netweth.ActualPage			:= TPL_Pageh.Out.ActualPage;
	INT_Netweth.UpdateIpSettings	:= TPL_Config.Out.StartUpFinished;
	INT_Netweth.PanelSize			:= TPL_Config.Out.PanelSize;
	INT_Netweth.PanelType			:= TPL_Config.Out.PanelType;

	(* Enter Device names to the HMIConfig - structure *)
	CASE INT_Netweth.PanelType OF
		TPL_APC, TPL_PPC2100 :
			EthDeviceName_01 := 'IF3';
			EthDeviceName_02 := 'IF4';
		TPL_POWER_PANEL_5XX, TPL_SIM :	
			EthDeviceName_01 := 'IF3';
			EthDeviceName_02 := '';			
		TPL_X20_VCWT :
			EthDeviceName_01 := 'IF2';
			EthDeviceName_02 := '';
		ELSE
			EthDeviceName_01 := 'IF5';
			EthDeviceName_02 := '';
	END_CASE;


	(*********************************)
	(* Get / Set Ethernet parameters *)
	(*********************************)

	(* Dialog Info for Ethernet *)
	DialogInfo.HeaderTextGroupNumber	:= TPL_DIALOG_HEADERTEXT;
	DialogInfo.HeaderTextNumber			:= DHT_ETHERNET;			(* Ethernet Settings *)
	DialogInfo.DialogTextGroupNumber	:= TPL_DIALOG_TEXT;
	DialogInfo.NumberOfButton			:= 2;
	DialogInfo.ButtonTextGroupNumber	:= TPL_DIALOG_BUTTONTEXT;
	DialogInfo.ButtonTextNumberOne		:= DBT_OK; 					(* OK *)
	DialogInfo.ButtonTextNumberTwo		:= DBT_CANCEL;				(* Cancel *)

	
	CASE Ethstep OF

		WAIT:
			//	TPL_Netweth.Out.Stat.Busy 				:= 0; do NOT set busy here AND at end of task !
			(* Call dialog when Exit Button is pressed an changes are not saved *)
			IF ((TPL_Netweth.In.Cmd.LeavePageIpSettings <> 0) AND (TPL_Netweth.In.Stat.AddressChanged = 1)) THEN
				Ethstep := LEAVE_MENU;
				TPL_Netweth.In.Cmd.LeavePageIpSettings 	:= 0;
				
			ELSIF ((TPL_Netweth.In.Cmd.LeavePageIpSettings <> 0) AND (TPL_Netweth.In.Stat.AddressChanged = 0)) THEN
				IF TPL_Netweth.In.Cmd.LeavePageIpSettings = 2 THEN	// Home Button was pressed, go to Main-Page
					INT_Netweth.NewPage := TPL_PAGE_MAIN;
				ELSIF INT_Netweth.PanelSize = TPL_BIG_PANEL THEN
					INT_Netweth.NewPage := TPL_PAGE_SYST;
				ELSIF	INT_Netweth.PanelSize = TPL_MEDIUM_PANEL THEN
					INT_Netweth.NewPage := TPL_PAGE_SYST;
				ELSIF INT_Netweth.PanelSize = TPL_SMALL_PANEL THEN
					INT_Netweth.NewPage := TPL_PAGE_SYST;
				END_IF
				TPL_Netweth.In.Cmd.LeavePageIpSettings := 0;
				TPL_Pageh.In.SwitchToPage:= INT_Netweth.NewPage;
			END_IF
			
			// Check addresses first when Save Button is pressed 
			IF TPL_Netweth.In.Cmd.SaveSettingsHMI = 1 THEN	
				TPL_Netweth.In.Cmd.SaveSettingsHMI := FALSE;
				Ethstep := CHECK_ADDRESSES;
			END_IF	
	
			(* there are limits for some values *)
			IF TPL_Netweth.In.Cfg.IpAddress[0] < 1 THEN 
				TPL_Netweth.In.Cfg.IpAddress[0] := 1;
			ELSIF TPL_Netweth.In.Cfg.IpAddress[0] > 247 THEN 
				TPL_Netweth.In.Cfg.IpAddress[0] := 247;
			ELSIF TPL_Netweth.In.Cfg.IpAddress[3] < 1 THEN 
				TPL_Netweth.In.Cfg.IpAddress[3] := 1;
			END_IF

			IF TPL_Netweth.In.Cfg.SubnetMask[0] < 248 THEN 
				TPL_Netweth.In.Cfg.SubnetMask[0] := 248;
			ELSIF TPL_Netweth.In.Cfg.SubnetMask[3] > 254 THEN 
				TPL_Netweth.In.Cfg.SubnetMask[3] := 254;
			END_IF

			IF TPL_Netweth.In.Cfg.DefGateway[0] < 1 THEN 
				TPL_Netweth.In.Cfg.DefGateway[0] := 1;
			ELSIF TPL_Netweth.In.Cfg.DefGateway[0] > 247 THEN 
				TPL_Netweth.In.Cfg.DefGateway[0] := 247;
			END_IF

			IF TPL_Netweth.Out.Para.BroadcastAddressValue[0] < 1 THEN 
				TPL_Netweth.Out.Para.BroadcastAddressValue[0] := 1;
			ELSIF TPL_Netweth.Out.Para.BroadcastAddressValue[3] < 1 THEN 
				TPL_Netweth.Out.Para.BroadcastAddressValue[3] := 1;
			END_IF

			IF TPL_Netweth.In.Cfg.NodeNumber < 1 THEN
				TPL_Netweth.In.Cfg.NodeNumber := 1;
			ELSIF TPL_Netweth.In.Cfg.NodeNumber > 255 THEN
				TPL_Netweth.In.Cfg.NodeNumber := 255;				
			END_IF
			
			
			IF (TPL_Netweth.In.Cmd.UpdateIpSettings = 1) OR (EDGEPOS(INT_Netweth.UpdateIpSettings) = 1) THEN
				DiagUpdateIpSettings := DiagUpdateIpSettings + 1;
				brsmemcpy(ADR (TPL_Netweth.In.Cfg), ADR (TPL_Config.In.Data.Netweth), SIZEOF (TPL_Netweth.In.Cfg));
				TPL_Netweth.In.Cmd.UpdateIpSettings	:= 0;
				Ethstep := SET_ADDRESSES;
			ELSIF (TPL_Netweth.In.Cmd.ReadIpSettings = 1) THEN
				DiagReadIpSettings := DiagReadIpSettings + 1;
				TPL_Netweth.In.Cmd.ReadIpSettings 		:= 0;
				TPL_Netweth.Out.Stat.EthAddressReady 	:= 0;
				Ethstep := READ_ADDRESSES_01; //Read addresses of ETH1 and Node
			END_IF

			// ------------------------------------------------------------------------------------------------------------------------------------
		READ_ADDRESSES_01:
			(* read IP address of ETH1 interface *)
			CfgGetIPAddr_01(enable := 1, pDevice := ADR(EthDeviceName_01), pIPAddr := ADR(IPAddressString_01), Len := 16);
			IF (CfgGetIPAddr_01.status = ERR_OK) THEN
				valcnt	:= 0;
				memxcnt	:= 0;
				FOR xcnt	:= 0 TO (SIZEOF(IPAddressString_01)-1) DO
					IF ((IPAddressString_01[xcnt] <> 0) AND (IPAddressString_01[xcnt] <> 46)) THEN 	(* dec 46 = ASCII '.' *)
						EthAddressBuffer[xcnt-memxcnt] := IPAddressString_01[xcnt];
					ELSIF ((IPAddressString_01[xcnt] = 46) OR (IPAddressString_01[xcnt] = 0)) THEN
						TPL_Netweth.In.Cfg.IpAddress[valcnt] 		:= UDINT_TO_USINT(brsatoi(ADR(EthAddressBuffer)));
						TPL_Netweth.Out.Para.IpAddressValue[valcnt]	:= UDINT_TO_USINT(brsatoi(ADR(EthAddressBuffer)));
						EthAddressBuffer[0] := 0;
						EthAddressBuffer[1] := 0;
						EthAddressBuffer[2] := 0;
						EthAddressBuffer[3] := 0;
						valcnt	:= valcnt + 1;
						memxcnt	:= xcnt + 1;
					END_IF
					IF valcnt > 3 THEN
						EXIT;
					END_IF
				END_FOR
				Ethstep := READ_ADDRESSES_SUBNET_MASK_01;
			ELSIF (CfgGetIPAddr_01.status <> ERR_FUB_BUSY) THEN (* Any error occurred *)
				Ethstep := READ_ADDRESSES_SUBNET_MASK_01;
			END_IF
			brsstrcpy(ADR(TPL_Netweth.Out.Para.IpAddress),ADR(IPAddressString_01));

			
		READ_ADDRESSES_SUBNET_MASK_01:
			(* read subnet mask of ETH1 *)
			CfgGetSubnetMask_01(enable := 1, pDevice:= ADR(EthDeviceName_01), pSubnetMask:= ADR(SubnetMaskString_01), Len := 16);
			IF (CfgGetSubnetMask_01.status = ERR_OK) THEN
				valcnt	:= 0;
				memxcnt	:= 0;
				FOR xcnt := 0 TO (SIZEOF(SubnetMaskString_01)-1) DO
					IF ((SubnetMaskString_01[xcnt] <> 0) AND (SubnetMaskString_01[xcnt] <> 46)) THEN 	(* dec 46 = ASCII '.' *)
						EthAddressBuffer[xcnt-memxcnt] := SubnetMaskString_01[xcnt];
					ELSIF ((SubnetMaskString_01[xcnt] = 46) OR (SubnetMaskString_01[xcnt] = 0)) THEN
						TPL_Netweth.In.Cfg.SubnetMask[valcnt]			:= UDINT_TO_USINT(brsatoi(ADR(EthAddressBuffer)));
						TPL_Netweth.Out.Para.SubnetMaskValue[valcnt]	:= UDINT_TO_USINT(brsatoi(ADR(EthAddressBuffer)));
						EthAddressBuffer[0] := 0;
						EthAddressBuffer[1] := 0;
						EthAddressBuffer[2] := 0;
						EthAddressBuffer[3] := 0;
						valcnt	:= valcnt + 1;
						memxcnt	:= xcnt + 1;
					END_IF
					IF valcnt > 3 THEN
						EXIT;
					END_IF
				END_FOR
				Ethstep := READ_ADDRESSES_02;
			ELSIF (CfgGetSubnetMask_01.status <> ERR_FUB_BUSY) THEN (* Any error occurred *)
				Ethstep := READ_ADDRESSES_02;
			END_IF
			brsstrcpy(ADR(TPL_Netweth.Out.Para.SubnetMask),ADR(SubnetMaskString_01));

					
		READ_ADDRESSES_02:
			(* read IP address of ETH2 interface *)
			CfgGetIPAddr_02(enable := 1, pDevice := ADR(EthDeviceName_02), pIPAddr := ADR(IPAddressString_02), Len := 16);
			IF (CfgGetIPAddr_02.status = ERR_OK) THEN
				ethInetAton(ADR(IPAddressString_02),ADR(ETH2_IP));
				Ethstep := READ_ADDRESSES_SUBNET_MASK_02;
			ELSIF (CfgGetIPAddr_02.status <> ERR_FUB_BUSY) THEN (* Any error occurred or cfgERR_DEVICE_NOT_EXIST = ETH2 not activated *)
				ETH2_IP := ETH2_Net := 0;
				Ethstep := READ_ADDRESSES_DEF_GATEWAY;
			END_IF
			
			
		READ_ADDRESSES_SUBNET_MASK_02:
			(* read subnet mask of ETH2 *)
			CfgGetSubnetMask_02(enable := 1, pDevice:= ADR(EthDeviceName_02), pSubnetMask:= ADR(SubnetMaskString_02), Len := 16);
			IF (CfgGetSubnetMask_02.status = ERR_OK) THEN
				ethInetAton(ADR(SubnetMaskString_02),ADR(ETH2_SN));
				ETH2_Net := ETH2_IP AND ETH2_SN; // calc network part of ETH2 
				Ethstep := READ_ADDRESSES_DEF_GATEWAY;
			ELSIF (CfgGetSubnetMask_02.status <> ERR_FUB_BUSY) THEN (* Any error occurred/ cfgERR_DEVICE_NOT_EXIST = ETH2 not activated *)
				ETH2_IP := ETH2_Net := 0;
				Ethstep := READ_ADDRESSES_DEF_GATEWAY;
			END_IF
						
			
		READ_ADDRESSES_DEF_GATEWAY:
			(* Default Gateway *)
			CfgGetDefaultGateway_01(enable:= 1, pDevice:= ADR(EthDeviceName_01), pGateway:= ADR(DefGatewayString), Len:= 16);
			IF (CfgGetDefaultGateway_01.status = ERR_OK) THEN
				valcnt:= 0;
				memxcnt:= 0;
				FOR xcnt:= 0 TO (SIZEOF(DefGatewayString)-1) DO
					IF ((DefGatewayString[xcnt] <> 0) AND (DefGatewayString[xcnt] <> 46)) THEN 	(* dec 46 = ASCII '.' *)
						EthAddressBuffer[xcnt-memxcnt] := DefGatewayString[xcnt];
					ELSIF ((DefGatewayString[xcnt] = 46) OR (DefGatewayString[xcnt] = 0)) THEN
						TPL_Netweth.In.Cfg.DefGateway[valcnt] 		:= UDINT_TO_USINT(brsatoi(ADR(EthAddressBuffer)));
						TPL_Netweth.Out.Para.DefGatewayValue[valcnt] := UDINT_TO_USINT(brsatoi(ADR(EthAddressBuffer)));
						EthAddressBuffer[0] := 0;
						EthAddressBuffer[1] := 0;
						EthAddressBuffer[2] := 0;
						EthAddressBuffer[3] := 0;
						valcnt	:= valcnt + 1;
						memxcnt	:= xcnt + 1;
					END_IF
					IF valcnt > 3 THEN
						EXIT;
					END_IF
				END_FOR
				Ethstep := CALCULATE_ADDRESSES_BROADCAST;
			ELSIF (CfgGetDefaultGateway_01.status <> ERR_FUB_BUSY) THEN (* Any error occurred *)
				Ethstep := CALCULATE_ADDRESSES_BROADCAST;
			END_IF
			brsstrcpy(ADR(TPL_Netweth.Out.Para.DefGateway),ADR(DefGatewayString));


		CALCULATE_ADDRESSES_BROADCAST:
			(* Calculate Broadcast Adress *)
			TPL_Netweth.Out.Para.BroadcastAddressValue[0]:= ((TPL_Netweth.In.Cfg.IpAddress[0] AND TPL_Netweth.In.Cfg.SubnetMask[0]) OR (NOT(TPL_Netweth.In.Cfg.SubnetMask[0])));
			TPL_Netweth.Out.Para.BroadcastAddressValue[1]:= ((TPL_Netweth.In.Cfg.IpAddress[1] AND TPL_Netweth.In.Cfg.SubnetMask[1]) OR (NOT(TPL_Netweth.In.Cfg.SubnetMask[1])));
			TPL_Netweth.Out.Para.BroadcastAddressValue[2]:= ((TPL_Netweth.In.Cfg.IpAddress[2] AND TPL_Netweth.In.Cfg.SubnetMask[2]) OR (NOT(TPL_Netweth.In.Cfg.SubnetMask[2])));
			TPL_Netweth.Out.Para.BroadcastAddressValue[3]:= ((TPL_Netweth.In.Cfg.IpAddress[3] AND TPL_Netweth.In.Cfg.SubnetMask[3]) OR (NOT(TPL_Netweth.In.Cfg.SubnetMask[3])));
			Ethstep := SET_ADDRESSES_BROADCAST;


		SET_ADDRESSES_BROADCAST:
			(* Set new Broadcast address *)
			Length:= brsitoa(TPL_Netweth.Out.Para.BroadcastAddressValue[0], ADR(BroadcastAddressString[0]));	(* convert dec values to character strings *)
			NextAddr:= brsstrcat((ADR(BroadcastAddressString) + Length), ADR('.'));		(* add '.' to first value *)
			Length:= brsitoa(TPL_Netweth.Out.Para.BroadcastAddressValue[1], NextAddr);							(* convert second value and attach to first *)
			NextAddr:= brsstrcat((NextAddr + Length), ADR('.'));
			Length:= brsitoa(TPL_Netweth.Out.Para.BroadcastAddressValue[2], NextAddr);
			NextAddr:= brsstrcat((NextAddr + Length), ADR('.'));
			brsitoa(TPL_Netweth.Out.Para.BroadcastAddressValue[3], NextAddr);

			CfgSetBroadcastAddr_01(enable:= 1, pDevice:= ADR(EthDeviceName_01), pBroadcastAddr:= ADR(BroadcastAddressString), Option:= INT_Netweth.OptionVolatile);
			IF (CfgSetBroadcastAddr_01.status = ERR_OK) OR (cnt >= 5) THEN
				cnt := 0;
				Ethstep := READ_IP_ADDRESSES_REMOTE_STATION;
			ELSE
				cnt := cnt + 1;
			END_IF
			brsstrcpy(ADR(TPL_Netweth.Out.Para.BroadcastAddress),ADR(BroadcastAddressString));

		READ_IP_ADDRESSES_REMOTE_STATION:
			(* IP Address Remote Station*)
			brsstrcpy(ADR(IPAddressRemoteStationString),ADR(TPL_Netweth.Out.Para.IpAddressRemoteStation));
			valcnt	:= 0;
			memxcnt	:= 0;
			FOR xcnt	:= 0 TO (SIZEOF(IPAddressRemoteStationString)-1) DO
				IF ((IPAddressRemoteStationString[xcnt] <> 0) AND (IPAddressRemoteStationString[xcnt] <> 46)) THEN 	(* dec 46 = ASCII '.' *)
					EthAddressBuffer[xcnt-memxcnt] := IPAddressRemoteStationString[xcnt];
				ELSIF ((IPAddressRemoteStationString[xcnt] = 46) OR (IPAddressRemoteStationString[xcnt] = 0)) THEN
					TPL_Netweth.In.Cfg.IpAddressRemote[valcnt] 			:= UDINT_TO_USINT(brsatoi(ADR(EthAddressBuffer)));
					TPL_Netweth.Out.Para.IpAddressRemoteValue[valcnt]	:= UDINT_TO_USINT(brsatoi(ADR(EthAddressBuffer)));
					EthAddressBuffer[0] := 0;
					EthAddressBuffer[1] := 0;
					EthAddressBuffer[2] := 0;
					EthAddressBuffer[3] := 0;
					valcnt:= valcnt + 1;
					memxcnt:= xcnt + 1;
				END_IF
				IF valcnt > 3 THEN
					EXIT;
				END_IF
			END_FOR
			brsstrcpy(ADR(TPL_Netweth.Out.Para.IpAddressRemoteStation),ADR(IPAddressRemoteStationString));
			Ethstep := READ_BAUDRATE;
		
		
		READ_BAUDRATE:
			CfgGetEthBaudrate_01(enable:= 1, pDevice:= ADR(EthDeviceName_01));
			TPL_Netweth.Out.Para.EthBaudrate := UDINT_TO_USINT(CfgGetEthBaudrate_01.Baudrate);
			Ethstep := READ_NODENR;
				
		READ_NODENR:
			CfgGetInaNode_01(enable := TRUE, pDevice := ADR(EthDeviceName_01));
			IF (CfgGetInaNode_01.status = ERR_OK) THEN
				TPL_Netweth.In.Cfg.NodeNumber := CfgGetInaNode_01.InaNode;
				
				TPL_Netweth.Out.Stat.EthAddressReady 	:= TRUE;
				INT_Netweth.WriteConfFile				:= TRUE;
				
				DialogStatus:= ResetDialog(ADR(BackupDialogIdent), TPL_Dialogh.Out.AdrDialogRingBuffer);
				Ethstep := WAIT;
				
			ELSIF (CfgGetInaNode_01.status <> ERR_FUB_BUSY) THEN // error 
				TPL_Netweth.Out.Stat.EthAddressReady 	:= TRUE;
				INT_Netweth.WriteConfFile				:= TRUE;
				
				DialogStatus:= ResetDialog(ADR(BackupDialogIdent), TPL_Dialogh.Out.AdrDialogRingBuffer);
				Ethstep := WAIT;				
				
			END_IF		

						
		CHECK_ADDRESSES: // ID-507: Check ethernet settings
			(* Check settings of each NIC for conflicts between ETH1-ETH2, ETH1-PLK and ETH1-PLK NAT *)
			(* If there is an conflict, a dialog will be shown *)
			// Combine IP of ETH1 interface
			Length		:= brsitoa(TPL_Netweth.In.Cfg.IpAddress[0], ADR(IPAddressString_01[0]));	
			NextAddr	:= brsstrcat((ADR(IPAddressString_01) + Length), ADR('.'));						
			Length		:= brsitoa(TPL_Netweth.In.Cfg.IpAddress[1], NextAddr);						
			NextAddr	:= brsstrcat((NextAddr + Length), ADR('.'));
			Length		:= brsitoa(TPL_Netweth.In.Cfg.IpAddress[2], NextAddr);
			NextAddr	:= brsstrcat((NextAddr + Length), ADR('.'));
			brsitoa(TPL_Netweth.In.Cfg.IpAddress[3], NextAddr);
			
			// Combine subnet mask of ETH1 interface
			Length		:= brsitoa(TPL_Netweth.In.Cfg.SubnetMask[0], ADR(SubnetMaskString_01[0]));	
			NextAddr	:= brsstrcat((ADR(SubnetMaskString_01) + Length), ADR('.'));						
			Length		:= brsitoa(TPL_Netweth.In.Cfg.SubnetMask[1], NextAddr);							
			NextAddr	:= brsstrcat((NextAddr + Length), ADR('.'));
			Length		:= brsitoa(TPL_Netweth.In.Cfg.SubnetMask[2], NextAddr);
			NextAddr	:= brsstrcat((NextAddr + Length), ADR('.'));
			brsitoa(TPL_Netweth.In.Cfg.SubnetMask[3], NextAddr);
			
			// calc network part of ETH1 address
			ethInetAton(ADR(IPAddressString_01),ADR(ETH1_IP)); 
			ethInetAton(ADR(SubnetMaskString_01),ADR(ETH1_SN));
			ETH1_Net := ETH1_IP AND ETH1_SN; 
			
			// Compare addresses of NICs
			IP_Conflict := 0;
			IF ETH2_IP <> 0 AND ((ETH1_Net AND ETH2_SN) = (ETH2_Net AND ETH1_SN)) AND ETH2_IP <> 0 THEN // Compare ETH1 with ETH2, if ETH2 is enabled
				IP_ConflictTxt[IP_Conflict] := 'ETH2: ';
				brsstrcat(ADR(IP_ConflictTxt[IP_Conflict]),ADR(IPAddressString_02));
				brsstrcat(ADR(IP_ConflictTxt[IP_Conflict]),ADR(' / '));
				brsstrcat(ADR(IP_ConflictTxt[IP_Conflict]),ADR(SubnetMaskString_02));
				IP_Conflict := IP_Conflict + 1;
			ELSE
				IP_ConflictTxt[IP_Conflict] := '';
			END_IF
			
			IF ((ETH1_Net AND PLK_SN) = (PLK_Net AND ETH1_SN)) THEN // Compare ETH1 wit PLK
				IP_ConflictTxt[IP_Conflict] := 'PLK: ';
				brsstrcat(ADR(IP_ConflictTxt[IP_Conflict]),ADR(PLK_IPstring));
				brsstrcat(ADR(IP_ConflictTxt[IP_Conflict]),ADR(' / '));
				brsstrcat(ADR(IP_ConflictTxt[IP_Conflict]),ADR(PLK_SNstring));
				IP_Conflict := IP_Conflict + 1;
			ELSE
				IP_ConflictTxt[IP_Conflict] := '';	
			END_IF	
			
			IF ((ETH1_Net AND PLK_NAT_SN) = (PLK_NAT_Net AND ETH1_SN)) THEN // Compare ETH1 wit PLK NAT
				IP_ConflictTxt[IP_Conflict] := 'PLK-NAT: ';
				brsstrcat(ADR(IP_ConflictTxt[IP_Conflict]),ADR(PLK_NAT_IPstring));
				brsstrcat(ADR(IP_ConflictTxt[IP_Conflict]),ADR(' / '));
				brsstrcat(ADR(IP_ConflictTxt[IP_Conflict]),ADR(PLK_NAT_SNstring));
				IP_Conflict := IP_Conflict + 1;
			ELSE
				IP_ConflictTxt[IP_Conflict] := '';
			END_IF	
			
			IF IP_Conflict > 0 THEN
				Ethstep := ADDRESS_ERROR;
			ELSE
				Ethstep	:= ACK_ADDRESSES;
			END_IF
				
				
		ADDRESS_ERROR:// ID-507: conflict with addresses
			DialogInfo.DialogTextNumber := DT_IP_CONFLICT;
			DialogInfo.NumberOfButton	:= 1;
			DialogStatus:= CallDialog(ADR(DialogIdent), ADR(DialogInfo), TPL_Dialogh.Out.AdrDialogRingBuffer);    
			IF DialogInfo.Return_PressedButton = 1 THEN
				DialogInfo.Return_PressedButton	:= 0;
				Ethstep	:= READ_ADDRESSES_01; //discard settings and restore former settings from HmiSys file
			END_IF
		
			
		ACK_ADDRESSES:		
			DialogInfo.DialogTextNumber := DT_SAVE_SETTINGS;
			DialogStatus := CallDialog(ADR(DialogIdent), ADR(DialogInfo), TPL_Dialogh.Out.AdrDialogRingBuffer);
		
			IF DialogInfo.Return_PressedButton = 1 THEN
				DialogInfo.Return_PressedButton		:= 0;
				TPL_Netweth.Out.Stat.EthAddressReady := 0;
				cnt := 0;
				// Show dialog about backup process
				// Dialog has its own ident variable, since it will be shown over several steps until the settings are read in again
				DialogInfo.DialogTextNumber := DT_BACKUP_PROGRESS;
				DialogInfo.NumberOfButton	:= 0;
				DialogStatus := CallDialog(ADR(BackupDialogIdent), ADR(DialogInfo), TPL_Dialogh.Out.AdrDialogRingBuffer);
				Ethstep := SET_ADDRESSES;
			ELSIF DialogInfo.Return_PressedButton = 2 THEN
				DialogInfo.Return_PressedButton	:= 0;
				Ethstep := WAIT;
			END_IF
	
			
		SET_ADDRESSES:
			(* Set new ETH1 IP address *)
			Length	:= brsitoa(TPL_Netweth.In.Cfg.IpAddress[0], ADR(IPAddressString_01[0]));	(* convert dec values to character strings *)
			NextAddr	:= brsstrcat((ADR(IPAddressString_01) + Length), ADR('.'));						(* add '.' to first value *)
			Length	:= brsitoa(TPL_Netweth.In.Cfg.IpAddress[1], NextAddr);						(* convert second value and attach to first *)
			NextAddr	:= brsstrcat((NextAddr + Length), ADR('.'));
			Length	:= brsitoa(TPL_Netweth.In.Cfg.IpAddress[2], NextAddr);
			NextAddr	:= brsstrcat((NextAddr + Length), ADR('.'));
			brsitoa(TPL_Netweth.In.Cfg.IpAddress[3], NextAddr);

			CfgSetIPAddr_01(enable:= 1, pDevice := ADR(EthDeviceName_01), pIPAddr := ADR(IPAddressString_01), Option := INT_Netweth.OptionVolatile);
			IF (CfgSetIPAddr_01.status = ERR_OK) OR (cnt >=5) THEN
				cnt 		:= 0;
				Ethstep 	:= SET_ADDRESSES_SUBNET_MASK;
			ELSE
				cnt := cnt + 1;
			END_IF


		SET_ADDRESSES_SUBNET_MASK:
			(* Set new ETH1 subnet mask *)
			Length	:= brsitoa(TPL_Netweth.In.Cfg.SubnetMask[0], ADR(SubnetMaskString_01[0]));	(* convert dec values to character strings *)
			NextAddr	:= brsstrcat((ADR(SubnetMaskString_01) + Length), ADR('.'));						(* add '.' to first value *)
			Length	:= brsitoa(TPL_Netweth.In.Cfg.SubnetMask[1], NextAddr);							(* convert second value and attach to first *)
			NextAddr	:= brsstrcat((NextAddr + Length), ADR('.'));
			Length	:= brsitoa(TPL_Netweth.In.Cfg.SubnetMask[2], NextAddr);
			NextAddr	:= brsstrcat((NextAddr + Length), ADR('.'));
			brsitoa(TPL_Netweth.In.Cfg.SubnetMask[3], NextAddr);

			CfgSetSubnetMask_01(enable:= 1, pDevice:= ADR(EthDeviceName_01), pSubnetMask:= ADR(SubnetMaskString_01), Option:= INT_Netweth.OptionVolatile);
			IF (CfgSetSubnetMask_01.status = ERR_OK) OR (cnt >= 5) THEN
				cnt 		:= 0;
				Ethstep 	:= SET_ADDRESSES_DEF_GATEWAY;
			ELSE
				cnt := cnt + 1;
			END_IF


		SET_ADDRESSES_DEF_GATEWAY:
			IF (CpyDone = 0) THEN
				(* Set new Default gateway *)
				Length	:= brsitoa(TPL_Netweth.In.Cfg.DefGateway[0], ADR(DefGatewayString[0]));	(* convert dec values to character strings *)
				NextAddr	:= brsstrcat((ADR(DefGatewayString) + Length), ADR('.'));						(* add '.' to first value *)
				Length	:= brsitoa(TPL_Netweth.In.Cfg.DefGateway[1], NextAddr);							(* convert second value and attach to first *)
				NextAddr	:= brsstrcat((NextAddr + Length), ADR('.'));
				Length	:= brsitoa(TPL_Netweth.In.Cfg.DefGateway[2], NextAddr);
				NextAddr	:= brsstrcat((NextAddr + Length), ADR('.'));
				brsitoa(TPL_Netweth.In.Cfg.DefGateway[3], NextAddr);
			END_IF

			CfgSetDefaultGateway_01(enable:= 1, pDevice:= ADR(EthDeviceName_01), pGateway:= ADR(DefGatewayString), Option:= INT_Netweth.OptionVolatile);
			IF (CfgSetDefaultGateway_01.status = ERR_OK) OR (cnt >= 5) THEN
				CpyDone	:= 0;
				cnt 		:= 0;
				Ethstep 	:= SET_IP_ADDRESSES_REMOTE_STATION;
			ELSIF (CfgSetDefaultGateway_01.status = cfgERR_SET_NOT_POSSIBLE) THEN
				CpyDone := 1;
				brsmemcpy(ADR(DefGatewayString[0]),ADR(IPAddressString_01[0]),SIZEOF(IPAddressString_01));
			ELSE
				CpyDone 	:= 0;
				cnt 		:= cnt + 1;
			END_IF


		SET_IP_ADDRESSES_REMOTE_STATION:
			(* Set new IP Address Remote Station *)
			Length	:= brsitoa(TPL_Netweth.In.Cfg.IpAddressRemote[0], ADR(IPAddressRemoteStationString[0]));	(* convert dec values to character strings *)
			NextAddr	:= brsstrcat((ADR(IPAddressRemoteStationString) + Length), ADR('.'));											(* add '.' to first value *)
			Length	:= brsitoa(TPL_Netweth.In.Cfg.IpAddressRemote[1], NextAddr);										(* convert second value and attach to first *)
			NextAddr	:= brsstrcat((NextAddr + Length), ADR('.'));
			Length	:= brsitoa(TPL_Netweth.In.Cfg.IpAddressRemote[2], NextAddr);
			NextAddr	:= brsstrcat((NextAddr + Length), ADR('.'));
			brsitoa(TPL_Netweth.In.Cfg.IpAddressRemote[3], NextAddr);
			brsstrcpy(ADR(TPL_Netweth.Out.Para.IpAddressRemoteStation),ADR(IPAddressRemoteStationString));

			TPL_Netweth.In.Stat.AddressChanged := 0;
			Ethstep := SET_NODENR;
			

		SET_NODENR:
			IF TPL_Netweth.In.Cfg.NodeNumber < 1 OR TPL_Netweth.In.Cfg.NodeNumber > 255 THEN // NodeNr in HmiSys is not ok, get over to read-steps to get NodeNr from CPU and save to file
				Ethstep := READ_ADDRESSES_01;
			ELSE	// NodeNr in File is ok, set node number
				CfgSetInaNode_01(enable := TRUE, InaNode := TPL_Netweth.In.Cfg.NodeNumber, Option := cfgOPTION_NON_VOLATILE, pDevice := ADR(EthDeviceName_01));
				IF CfgSetInaNode_01.status = ERR_OK THEN
					Ethstep := READ_ADDRESSES_01;
				END_IF
			END_IF
			
			
		LEAVE_MENU:
			(* Asks to continue, if changes were made *)
			DialogInfo.DialogTextNumber := DT_CHANGES_WILL_BE_LOST;
			DialogStatus:= CallDialog(ADR(DialogIdent), ADR(DialogInfo), TPL_Dialogh.Out.AdrDialogRingBuffer);

			IF DialogInfo.Return_PressedButton = 1 THEN
				DialogInfo.Return_PressedButton		:= 0;
				TPL_Netweth.In.Stat.AddressChanged	:= 0;
				Ethstep	:= READ_ADDRESSES_01;
			ELSIF DialogInfo.Return_PressedButton = 2 THEN
				DialogInfo.Return_PressedButton:= 0;
				Ethstep	:= WAIT;	
			END_IF

	END_CASE


	//----------------------------------------------------------------
	//----------------------------------------------------------------
	//----------------------------------------------------------------

	IF (Ethstep <> WAIT) THEN	
		TPL_Netweth.Out.Stat.Busy := 1;
	ELSE
		TPL_Netweth.Out.Stat.Busy := 0;
	END_IF


	(*-----Write external Variables-----*)
	IF (TPL_Config.Out.Stat.Busy = 0) AND (INT_Netweth.WriteConfFile = 1) THEN
		brsmemcpy(ADR (TPL_Config.In.Data.Netweth), ADR (TPL_Netweth.In.Cfg), SIZEOF (TPL_Netweth.In.Cfg));
		INT_Netweth.WriteConfFile 	:= FALSE;
		TPL_Config.In.Cmd.WriteFile	:= TRUE;
	END_IF	


END_PROGRAM

