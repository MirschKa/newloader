
TYPE
	INT_Netweth_Type : 	STRUCT 
		PanelType : USINT;
		PanelSize : USINT;
		ActualPage : UINT;
		OptionVolatile : UDINT;
		NewPage : UINT;
		WriteConfFile : BOOL;
		UpdateIpSettings : BOOL;
	END_STRUCT;
END_TYPE
