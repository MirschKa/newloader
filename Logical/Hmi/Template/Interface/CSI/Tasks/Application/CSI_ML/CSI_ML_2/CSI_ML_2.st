//************************************************************************************************************************
// Company:     GEA Food Solutions Germany GmbH 
//------------------------------------------------------------------------------------------------------------------------
// Name:        CSI_ML_2
// Author:      Florian Becker
// Version:     V1.00.0
// Date:        11-mar-2020 
//------------------------------------------------------------------------------------------------------------------------
// Restriction: None
//------------------------------------------------------------------------------------------------------------------------
// Description: Task for handling the CSI interface regarding the machine specific CSI_MMn_2 part
//************************************************************************************************************************

PROGRAM _INIT
// Normally not used: InitOk
InitOk := FALSE; 

END_PROGRAM

PROGRAM _CYCLIC
	(*----------------------------------------------------------------------------------------------------------------------*)
	(* Initialization.                                                                                                      *)
	(*----------------------------------------------------------------------------------------------------------------------*)
// Release by machine configuration handler
//	IF TPL_Config.In.Data.MachineDataInterface.Available = FALSE THEN
//		RETURN;
//	END_IF;
	//Dummy
	IF InitOk = FALSE THEN
		RETURN;
	END_IF;
	// Release by machine configuration handler
//	IF NOT(InitOk) THEN
//		InitOk := TRUE;
//		Initialization := TRUE;// init program
//	END_IF

	//----------------------------------------------------------------------------------------------------------------------
	// CSI_ML> Out_2: Machine recipe.                                                                                     
	//----------------------------------------------------------------------------------------------------------------------
	//CSI_ML.Out_2.MachineRecipe.Current.Number      	:= ProductionRecipe.Number;
	//CSI_ML.Out_2.MachineRecipe.Current.Type			:= ProductionRecipe.Type;
	//CSI_ML.Out_2.MachineRecipe.Current.Name        	:= ProductionRecipe.Name;
	//CSI_ML.Out_2.MachineRecipe.Current.Comment     	:= ProductionRecipe.Comment;
	//CSI_ML.Out_2.MachineRecipe.Current.Data        	:= ProductionRecipe.Data;

	//----------------------------------------------------------------------------------------------------------------------
	// CSI_ML> Out_2: Maintenance.                                                                                        
	//----------------------------------------------------------------------------------------------------------------------
	CSI_ML.Out_2.Maintenance.TobeDefined          	:= CSI_ML.Out_2.Maintenance.TobeDefined; 


	//----------------------------------------------------------------------------------------------------------------------
	// CSI_MLC> Out_2: Batch
	//---------------------------------------------------------------------------------------------------------------------- 
	CSI_ML.Out_2.Batch.Data[0]    := CSI_ML.Out_2.Batch.Data[0];
	CSI_ML.Out_2.Batch.Handshake  := CSI_ML.Out_2.Batch.Handshake;
  
	
	//----------------------------------------------------------------------------------------------------------------------
	// CSI_ML> In_2: Machine recipe
	//----------------------------------------------------------------------------------------------------------------------
	CSI_ML.In_2.MachineRecipe.TobeDefined		:=	CSI_ML.In_2.MachineRecipe.TobeDefined; 
	
	
	//----------------------------------------------------------------------------------------------------------------------
	// CSI_ML> In_2: Maintenance
	//---------------------------------------------------------------------------------------------------------------------- 
	CSI_ML.In_2.Maintenance.TobeDefined := CSI_ML.In_2.Maintenance.TobeDefined; 
	
	
	//----------------------------------------------------------------------------------------------------------------------
	// CSI_ML> In_2: Batch
	//----------------------------------------------------------------------------------------------------------------------
	CSI_ML.In_2.Batch.Handshake  := CSI_ML.In_2.Batch.Handshake;
 
	
	//----------------------------------------------------------------------------------------------------------------------
	// TCP/IP communication
	//----------------------------------------------------------------------------------------------------------------------   
	// Configuration
//	Config_TCPIP.Installed                   := Config_TCPIP.Installed(*MachConfig.Automation*);
//	Config_TCPIP.Communication.Mode          := L_TCP_CLIENT;
//	Config_TCPIP.Communication.TimeOut       := T#10s;
//
//	Config_TCPIP.Data.ReceiveStructure       := ADR(CSI_ML.In_2);
//	Config_TCPIP.Data.SizeOfReceiveStructure := SIZEOF(CSI_ML.In_2);
//	Config_TCPIP.Data.SendStructure          := ADR(CSI_ML.Out_2);
//	Config_TCPIP.Data.SizeOfSendStructure    := SIZEOF(CSI_ML.Out_2);
//
//	Config_TCPIP.Port.Mode                   := L_TCP_PORTCTRL_AUTO;
//	Config_TCPIP.Port.Automatic.Channel      := 2;
//
//	// Settings
//	Settings_TCPIP.Communication.Enable  := CSI_TPL.Out.Network.EthernetAddressReady;
//	Settings_TCPIP.IP_Address.Server     := CSI_TPL.Out.Network.IP_AddressRemoteStation;
//	Settings_TCPIP.IP_Address.Client     := CSI_TPL.Out.Network.IP_Address;   
//
//	// Start/stop handling 
//	IF Settings_TCPIP.Communication.Enable THEN
//		CommunicationCmd.AutoControl.Start := TRUE;
//	ELSE
//		CommunicationCmd.AutoControl.Stop  := TRUE;
//	END_IF;
//
//	// Communication
//	FB_TCPIP_0(iConfig := Config_TCPIP,iSettings := Settings_TCPIP,ioCmd := CommunicationCmd);
      
	//--------------------------------------
	// OPC UAOPC UA_Client_Config.
	//--------------------------------------
//	OPCUA_Client_Config.SessionConnectInfo_0.SecurityMsgMode 									:= UASecurityMsgMode_SignEncrypt;
//	OPCUA_Client_Config.SessionConnectInfo_0.SecurityPolicy   									:= UASecurityPolicy_Basic256Sha256;
//	OPCUA_Client_Config.SessionConnectInfo_0.TransportProfile 									:= UATP_UATcp;
//	OPCUA_Client_Config.SessionConnectInfo_0.UserIdentityToken.UserIdentityTokenType 			:= UAUITT_Username;
//	OPCUA_Client_Config.SessionConnectInfo_0.UserIdentityToken.TokenParam1 						:= 'GEA_Admin';
//	OPCUA_Client_Config.SessionConnectInfo_0.UserIdentityToken.TokenParam2 						:= 'Admin';
//	OPCUA_Client_Config.SessionConnectInfo_0.SessionTimeout 									:= T#1m;
//	OPCUA_Client_Config.SessionConnectInfo_0.MonitorConnection 									:= T#10s;
//	OPCUA_Client_Config.CycleTime				:= TPL_Config.In.Data.MachineDataInterface.CycleTime;
//	OPCUA_Client_Config.SendNodeList 			:= '::AsGlobalPV:CSI_ML.Out_2'; 
//	OPCUA_Client_Config.ReceiveNodeList 		:= '::AsGlobalPV:CSI_ML.In_2';
//	OPCUA_Client_Config.ReadPunkt				:= '::CSI_ML_2:ReadDummy.In_2';
//	OPCUA_Client_Config.WritePunkt				:= '::CSI_ML_2:WriteDummy.Out_2';
//	OPCUA_Client_Config.ServerEndpointUrl		:= 'opc.tcp://10.211.220.51:4840';
	
//	IF TPL_Config.In.Data.MachineDataInterface.OPCUA_Client AND TPL_Pageh.Out.PanelIsAlive THEN
//		OPCUA_Client_ML2(Execute := TRUE,Config := OPCUA_Client_Config);
//	END_IF
//	CSI_VC.Status.TCPIP.CSI_MMM2_Online := CommunicationCmd.Status.Online;
     
	//----------------------------------------------------------------------------------------------------------------------
	// Reset initialization flag
	//----------------------------------------------------------------------------------------------------------------------
	Initialization := FALSE;

END_PROGRAM
