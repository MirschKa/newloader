
TYPE
	T000_CSI_ML : 	STRUCT 
		Out_1 : T000_CSI_ML_Out_1;
		In_1 : T000_CSI_ML_In_1;
		Out_2 : T000_CSI_ML_Out_2;
		In_2 : T000_CSI_ML_In_2;
	END_STRUCT;
	T000_CSI_ML_In_1 : 	STRUCT 
		Settings : T000_CSI_ML_In_Settings;
	END_STRUCT;
	T000_CSI_ML_In_2 : 	STRUCT 
		MachineRecipe : T000_CSI_ML_In_Recipe;
		Batch : T000_CSI_ML_In_Batch;
		Maintenance : T000_CSI_ML_Out_Mntc;
	END_STRUCT;
	T000_CSI_ML_Out_1 : 	STRUCT 
		Identification : T000_CSI_ML_Out_ID;
		DeviceStatus : T000_CSI_ML_Out_DeviceStatus;
		ProcessValue : T000_CSI_ML_Out_PV;
		Alarm : T000_CSI_ML_Out_Alarm;
		Message : T000_CSI_ML_Out_Message;
		Status : T000_CSI_ML_Out_Status;
	END_STRUCT;
	T000_CSI_ML_Out_2 : 	STRUCT 
		MachineRecipe : T000_CSI_ML_Out_Recipe;
		Maintenance : T000_CSI_ML_Out_Mntc;
		Batch : T000_CSI_ML_Out_Batch;
	END_STRUCT;
	T000_CSI_ML_In_Settings : 	STRUCT 
		ToBeDefined : BOOL;
	END_STRUCT;
	T000_CSI_ML_In_Recipe : 	STRUCT 
		TobeDefined : T000_CSI_ML_Gen_Recipe;
	END_STRUCT;
	T000_CSI_ML_In_Batch : 	STRUCT 
		Handshake : T_HS;
	END_STRUCT;
	T000_CSI_ML_Out_ID : 	STRUCT 
		Interface : T000_CSI_ML_Out_ID_IF;
	END_STRUCT;
	T000_CSI_ML_Out_DeviceStatus : 	STRUCT 
		ToBeDefined : UINT;
	END_STRUCT;
	T000_CSI_ML_Out_PV : 	STRUCT 
		ToBeDefined : UINT;
	END_STRUCT;
	T000_CSI_ML_Out_Alarm : 	STRUCT 
		Current : ARRAY[0..3]OF UDINT;
	END_STRUCT;
	T000_CSI_ML_Out_Message : 	STRUCT 
		TobeDefined : ARRAY[0..3]OF UDINT;
	END_STRUCT;
	T000_CSI_ML_Out_Status : 	STRUCT 
		TobeDefined : UINT;
	END_STRUCT;
	T000_CSI_ML_Out_Recipe : 	STRUCT 
		Current : T000_CSI_ML_Gen_Recipe;
	END_STRUCT;
	T000_CSI_ML_Out_Mntc : 	STRUCT 
		TobeDefined : UINT;
	END_STRUCT;
	T000_CSI_ML_Out_Batch : 	STRUCT 
		Handshake : T_HS;
		Data : ARRAY[0..3]OF REAL;
	END_STRUCT;
	T000_CSI_ML_Out_ID_IF : 	STRUCT 
		Version : STRING[24];
	END_STRUCT;
	T000_CSI_ML_Out_Mntc_Hrs : 	STRUCT 
		RunningTime : UDINT;
	END_STRUCT;
	T000_CSI_ML_Gen_Recipe : 	STRUCT 
		Number : UDINT;
		Name : STRING[20];
		Comment : STRING[30];
		Data : USINT; (*Muss angepasst werden*)
		Type : UINT;
	END_STRUCT;
END_TYPE
