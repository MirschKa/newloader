//************************************************************************************************************************
// Company:     GEA Food Solutions Germany GmbH 
//------------------------------------------------------------------------------------------------------------------------
// Name:        CSI_ML_1
// Author:      Florian Becker
// Version:     V1.00.0
// Date:        11-mar-2020 
//------------------------------------------------------------------------------------------------------------------------
// Restriction: None
//------------------------------------------------------------------------------------------------------------------------
// Description: Task for handling the CSI interface regarding the machine specific CSI_ML_1 part
//************************************************************************************************************************

PROGRAM _INIT
// Normally not used: InitOk
	InitOk := FALSE; 
END_PROGRAM

PROGRAM _CYCLIC
	(*----------------------------------------------------------------------------------------------------------------------*)
	(* Initialization.                                                                                                      *)
	(*----------------------------------------------------------------------------------------------------------------------*)
// Release by machine configuration handler
//	IF TPL_Config.In.Data.MachineDataInterface.Available = FALSE THEN
//		RETURN;
//	END_IF;
//  
	IF InitOk = FALSE THEN
		RETURN;
	END_IF;
	// Release by machine configuration handler
//	IF NOT(InitOk) THEN
//		InitOk := TRUE;
//		Initialization := TRUE;// init program
//	END_IF

	//----------------------------------------------------------------------------------------------------------------------
	// Out_1: Identification
	//----------------------------------------------------------------------------------------------------------------------
	CSI_ML.Out_1.Identification.Interface.Version       := '3.00';
  
	//----------------------------------------------------------------------------------------------------------------------
	// CSI_ML> Out 1: Device status.                                                                                      
	//----------------------------------------------------------------------------------------------------------------------
	CSI_ML.Out_1.DeviceStatus.ToBeDefined               := CSI_ML.Out_1.DeviceStatus.ToBeDefined ;(* To be defined *)
       
	//----------------------------------------------------------------------------------------------------------------------
	// CSI_ML> Out_1: Process value.                                                                                      
	//----------------------------------------------------------------------------------------------------------------------
	CSI_ML.Out_1.ProcessValue.ToBeDefined               := CSI_ML.Out_1.ProcessValue.ToBeDefined ;(* To be defined *)


	//----------------------------------------------------------------------------------------------------------------------
	// CSI_ML> Out_1: Alarm.                                                                                              
	//----------------------------------------------------------------------------------------------------------------------
//	CSI_ML.Out_1.Alarm.Current[0].0   := CSI_ML.Out_1.Alarm.Current[0].0;
//	CSI_ML.Out_1.Alarm.Current[1].0   := CSI_ML.Out_1.Alarm.Current[1].0;
//	CSI_ML.Out_1.Alarm.Current[2].0   := CSI_ML.Out_1.Alarm.Current[2].0;
//	CSI_ML.Out_1.Alarm.Current[3].0   := CSI_ML.Out_1.Alarm.Current[3].0;
//	
//	(* Alarm Active *)
//	CSI_Cmd.Status.AlarmActive := FALSE;
//	FOR i := 0 TO ((SIZEOF(CSI_ML.Out_1.Alarm.Current) / SIZEOF(CSI_ML.Out_1.Alarm.Current[0])) - 1) DO
//		IF ( CSI_ML.Out_1.Alarm.Current[i] > 0 )
//			THEN
//			CSI_Cmd.Status.AlarmActive := TRUE;
//		END_IF;
//	END_FOR;

	//----------------------------------------------------------------------------------------------------------------------
	// CSI_ML> Out_1: Message.                                                                                            
	//----------------------------------------------------------------------------------------------------------------------
	CSI_ML.Out_1.Message.TobeDefined 		:= CSI_ML.Out_1.Message.TobeDefined;						(* To be defined *)

	
	//----------------------------------------------------------------------------------------------------------------------
	// CSI_ML> Out_1: Status.                                                                                            
	//----------------------------------------------------------------------------------------------------------------------
	CSI_ML.Out_1.Status.TobeDefined		:= CSI_ML.Out_1.Status.TobeDefined;								(* To be defined *)
	
	
	//------------------------------------------------------------------------------------------------------------------------
	// In_1:  Settings
	//------------------------------------------------------------------------------------------------------------------------
	CSI_ML.In_1.Settings.ToBeDefined       := CSI_ML.In_1.Settings.ToBeDefined;                			(* To be defined *)
	
	
	//------------------------------------------------------------------------------------------------------------------------
	// TCP/IP Communication
	//------------------------------------------------------------------------------------------------------------------------
	// Configuration
//	Config_TCPIP.Installed                    := Config_TCPIP.Installed(*MachConfig.Automation*);
//	Config_TCPIP.Communication.Mode           := L_TCP_CLIENT;
//	Config_TCPIP.Communication.TimeOut        := T#10s;
//	
//	Config_TCPIP.Data.ReceiveStructure        := ADR(CSI_ML.In_1);
//	Config_TCPIP.Data.SizeOfReceiveStructure  := SIZEOF(CSI_ML.In_1);
//	Config_TCPIP.Data.SendStructure           := ADR(CSI_ML.Out_1);
//	Config_TCPIP.Data.SizeOfSendStructure     := SIZEOF(CSI_ML.Out_1);
//	
//	Config_TCPIP.Port.Mode                    := L_TCP_PORTCTRL_AUTO;
//	Config_TCPIP.Port.Automatic.Channel       := 1;
//	  
//	// Settings
//	Settings_TCPIP.Communication.Enable       := CSI_TPL.Out.Network.EthernetAddressReady;
//	Settings_TCPIP.IP_Address.Server          := CSI_TPL.Out.Network.IP_AddressRemoteStation;
//	Settings_TCPIP.IP_Address.Client          := CSI_TPL.Out.Network.IP_Address;
//	
//	// Start/stop handling
//	IF Settings_TCPIP.Communication.Enable THEN
//		CommunicationCmd.AutoControl.Start  := TRUE;
//	ELSE
//		CommunicationCmd.AutoControl.Stop   := TRUE;
//	END_IF;
//
//	// Communication
//	FB_TCPIP_0(iConfig := Config_TCPIP,iSettings := Settings_TCPIP,ioCmd := CommunicationCmd);
  
	//--------------------------------------
	// OPC UA
	//--------------------------------------
//	OPCUA_Client_Config.SessionConnectInfo_0.SecurityMsgMode 								:= UASecurityMsgMode_SignEncrypt;
//	OPCUA_Client_Config.SessionConnectInfo_0.SecurityPolicy   								:= UASecurityPolicy_Basic256Sha256;
//	OPCUA_Client_Config.SessionConnectInfo_0.TransportProfile 								:= UATP_UATcp;
//	OPCUA_Client_Config.SessionConnectInfo_0.UserIdentityToken.UserIdentityTokenType 		:= UAUITT_Username;
//	OPCUA_Client_Config.SessionConnectInfo_0.UserIdentityToken.TokenParam1 					:= 'GEA_Admin';
//	OPCUA_Client_Config.SessionConnectInfo_0.UserIdentityToken.TokenParam2 					:= 'Admin';
//	OPCUA_Client_Config.SessionConnectInfo_0.SessionTimeout 								:= T#1m;
//	OPCUA_Client_Config.SessionConnectInfo_0.MonitorConnection 								:= T#10s;
//	OPCUA_Client_Config.CycleTime				:= TPL_Config.In.Data.MachineDataInterface.CycleTime;
//	OPCUA_Client_Config.SendNodeList 			:= '::AsGlobalPV:CSI_ML.Out_1'; 
//	OPCUA_Client_Config.ReceiveNodeList 		:= '::AsGlobalPV:CSI_ML.In_1';
//	OPCUA_Client_Config.ReadPunkt				:= '::CSI_ML_1:ReadDummy.In_1';
//	OPCUA_Client_Config.WritePunkt				:= '::CSI_ML_1:WriteDummy.Out_1';
//	OPCUA_Client_Config.ServerEndpointUrl		:= 'opc.tcp://10.211.220.51:4840';
//	
//	IF TPL_Config.In.Data.MachineDataInterface.OPCUA_Client AND TPL_Pageh.Out.PanelIsAlive THEN
//		OPCUA_Client_ML1(Execute := TRUE,Config := OPCUA_Client_Config);
//	END_IF
	
//	CSI_VC.Status.TCPIP.CSI_MMM1_Online := CommunicationCmd.Status.Online;

	//----------------------------------------------------------------------------------------------------------------------
	// Reset initialization flag
	//----------------------------------------------------------------------------------------------------------------------
	Initialization := FALSE;

END_PROGRAM
