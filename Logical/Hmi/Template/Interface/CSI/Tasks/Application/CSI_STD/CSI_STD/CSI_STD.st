//************************************************************************************************************************
// Company:     GEA Food Solutions Bakel
//------------------------------------------------------------------------------------------------------------------------
// Name:        CSI_STD
// Author:      Ivo Raijmakers
// Version:     V1.01.0
// Date:        03-jan-2020
//------------------------------------------------------------------------------------------------------------------------
// Restriction: None
//------------------------------------------------------------------------------------------------------------------------
// Description: Task for handling the CSI interface regarding the standard CSI_STD part
//************************************************************************************************************************

PROGRAM _INIT
	CSI_VC.ManualControl.StatusDialog.TabSelection    := 1; // Hide PopUp
//	TPL_Config.In.Data.MachineDataInterface.CycleTime := T#500ms;
	// Normally not used: InitOk
	InitOk := FALSE; 
	
END_PROGRAM  

PROGRAM _CYCLIC
	(*----------------------------------------------------------------------------------------------------------------------*)
	(* Initialization.                                                                                                      *)
	(*----------------------------------------------------------------------------------------------------------------------*)
//// Release by machine configuration handler
//	IF TPL_Config.In.Data.MachineDataInterface.Available = FALSE THEN
//		RETURN;
//	END_IF;

	// Release by machine configuration handler
//	IF NOT(InitOk) THEN
//		InitOk := TRUE;
//		Initialization := TRUE;// init program
//	END_IF

	//----------------------------------------------------------------------------------------------------------------------
	// Configuration
	//----------------------------------------------------------------------------------------------------------------------
	CSI_Config.Installed                           := TRUE;		
	CSI_Config.General.RemoteStartDisable          := FALSE; // Block commands from upper system if not allowed (e.g. when not safe)
	CSI_Config.Communication.TCPIP                 := FALSE; // Only for TCP/IP (Line Control), not needed for OPC only.
	CSI_Config.VC.Installed                        := FALSE;
	CSI_Config.VC.StatusDialog                     := FALSE;
	CSI_Config.VC.Template_Version                 := 4;     // Template type specific

	//----------------------------------------------------------------------------------------------------------------------
	// Recipe> Status
	//----------------------------------------------------------------------------------------------------------------------
	CSI_Cmd.Recipe.Status.MachineRecipeLoadDisable := FALSE;

	//----------------------------------------------------------------------------------------------------------------------
	// Status> Machine Mode.
	//----------------------------------------------------------------------------------------------------------------------
	IF PLACEHOLDER_BOOL THEN
		CSI_Cmd.Status.MachineMode := L_CSI300_MC_MODE_PRODUCTION;
	//ELSIF PLACEHOLDER_BOOL THEN
	//	CSI_Cmd.Status.MachineMode := L_CSI300_MC_MODE_CLEANING;
	ELSIF PLACEHOLDER_BOOL THEN
		CSI_Cmd.Status.MachineMode := L_CSI300_MC_MODE_MAINTENANCE;
	END_IF;

	//----------------------------------------------------------------------------------------------------------------------
	// Status> Machine State
	//----------------------------------------------------------------------------------------------------------------------
	CASE CSI_Cmd.Status.MachineMode OF
		L_CSI300_MC_MODE_PRODUCTION: // Production
			CSI_Cmd.Status.MachineState := PLACEHOLDER_UDINT;
			IF PLACEHOLDER_BOOL THEN 
				CSI_Cmd.Status.MachineState := L_SM_STATE_RUNNING;
			ELSE
				CSI_Cmd.Status.MachineState := L_SM_STATE_STOPPED;  
			END_IF
		ELSE
			CSI_Cmd.Status.MachineState := L_SM_STATE_STOPPED;  
	END_CASE

	//----------------------------------------------------------------------------------------------------------------------
	// Status> Production Mode
	//----------------------------------------------------------------------------------------------------------------------		
	CASE CSI_Cmd.Status.MachineMode OF
		L_CSI300_MC_MODE_PRODUCTION: // Production
			IF PLACEHOLDER_UINT <> L_SM_STATE_STOPPED THEN
				CSI_Cmd.Status.ProductionMode  := PLACEHOLDER_UDINT;
			ELSIF    (CSI_Cmd.AutoControl.ProductionMode <> 0)AND (CSI_Cmd.Status.MachineState  = L_SM_STATE_STOPPED)AND (CSI_Cmd.Status.OperatingMode = L_CSI300_OP_MODE_AUTO) THEN
				CSI_Cmd.Status.ProductionMode := CSI_Cmd.AutoControl.ProductionMode;
			END_IF; 

		L_CSI300_MC_MODE_CLEANING: // Cleaning
			CSI_Cmd.Status.ProductionMode  := 0;

	END_CASE;

	//----------------------------------------------------------------------------------------------------------------------
	// Status> Others
	//----------------------------------------------------------------------------------------------------------------------
	// Operator action required (bit 0)
	CSI_Cmd.Status.Others.0   :=  FALSE;                                  (* OperatorActionRequired  *)
	CSI_Cmd.Status.Others.1   :=  FALSE;  								  (* Out of material         *)
	CSI_Cmd.Status.Others.2   :=  FALSE;                                  (* GEA_Reserved_2          *)  
	CSI_Cmd.Status.Others.3   :=  FALSE;                                  (* GEA_Reserved_3          *) 
	CSI_Cmd.Status.Others.4   :=  FALSE;                                  (* GEA_Reserved_4          *)  
	CSI_Cmd.Status.Others.5   :=  FALSE;                                  (* GEA_Reserved_5          *)    
	CSI_Cmd.Status.Others.6   :=  FALSE;                                  (* GEA_Reserved_6          *)  
	CSI_Cmd.Status.Others.7   :=  FALSE;                                  (* GEA_Reserved_7          *)
	CSI_Cmd.Status.Others.8   :=  FALSE;                                  (* GEA_Reserved_8          *)      
	CSI_Cmd.Status.Others.9   :=  FALSE;                                  (* Machine_Reserved_9      *)      
	CSI_Cmd.Status.Others.10  :=  FALSE;                                  (* Machine_Reserved_10     *)      
	CSI_Cmd.Status.Others.11  :=  FALSE;                                  (* Machine_Reserved_11     *)      
	CSI_Cmd.Status.Others.12  :=  FALSE;                                  (* Machine_Reserved_12     *)    
	CSI_Cmd.Status.Others.13  :=  FALSE;                                  (* Machine_Reserved_13     *)      
	CSI_Cmd.Status.Others.14  :=  FALSE;                                  (* Machine_Reserved_14     *)      
	CSI_Cmd.Status.Others.15  :=  FALSE;                                  (* Machine_Reserved_15     *)  

	//----------------------------------------------------------------------------------------------------------------------
	// Status> Alarm
	//----------------------------------------------------------------------------------------------------------------------
	// Status Alarm - active in CSI_MMMN.Out_1.Alarm.Current
	CSI_Cmd.Status.Alarm.L_CSI300_STS_ALARM_ALARM     := USINT_TO_BOOL(CSI_Cmd.Status.AlarmActive OR CSI_Cmd.ProgramExistCheck.Alarms.ActiveAlarm[L_REC_ALARM_RECIPE_NOT_EXIST_ERR]); 
	// Status Emergency stop                                                    														  
	CSI_Cmd.Status.Alarm.L_CSI300_STS_ALARM_ESTOP     := PLACEHOLDER_BOOL;   
	// Status Safety	
	CSI_Cmd.Status.Alarm.L_CSI300_STS_ALARM_SAFETY    := PLACEHOLDER_BOOL;             
	// Status Warning (Maybe not visible on machine due to check only if production is started/running)
	CSI_Cmd.Status.Alarm.L_CSI300_STS_ALARM_WARNING   := PLACEHOLDER_BOOL;                                                     

	//----------------------------------------------------------------------------------------------------------------------
	// MachineRecipe
	//----------------------------------------------------------------------------------------------------------------------
	//CSI_Cmd.MachineRecipe.Data.Header.Number  := ProductionRecipe.Number;
	//CSI_Cmd.MachineRecipe.Data.Header.Name    := ProductionRecipe.Name;
	//CSI_Cmd.MachineRecipe.Data.Header.Comment := ProductionRecipe.Comment;

	//----------------------------------------------------------------------------------------------------------------------
	// OperatingData
	//----------------------------------------------------------------------------------------------------------------------
	CSI_Cmd.OperatingData.Status.RunningTime  := PLACEHOLDER_UDINT;

	//----------------------------------------------------------------------------------------------------------------------
	// Batch
	//----------------------------------------------------------------------------------------------------------------------
//	CSI_Cmd.Batch.Command          := 0; // Can be used to restart a batch from a machine (e.g. for temporary stop for inspection *)
//	CSI_Cmd.Batch.Data[0].Progress := 0;
//	CSI_Cmd.Batch.Data[0].Quantity := 0;
//	CSI_Cmd.Batch.Data[0].Status   := 0;

	//----------------------------------------------------------------------------------------------------------------------
	// CSI_STD
	//----------------------------------------------------------------------------------------------------------------------
	FB_CSI300_Mach_0(iInitialization := Initialization,iConfig := CSI_Config,ioCSI_STD := CSI_STD, ioTPL := CSI_TPL,ioCmd := CSI_Cmd);

	//----------------------------------------------------------------------------------------------------------------------
	// AutoControl> MachineCommand, Example Code
	//----------------------------------------------------------------------------------------------------------------------
	//@RK start here 2019-12-02
	//	IF (CSI_Cmd.Status.OperatingMode = L_CSI300_OP_MODE_AUTO)THEN	
	//		CASE CSI_Cmd.Status.ProductionMode OF
	////			BAK_MACH_PROD_MODE_PROCESSING:
	//			  	ProductionCommand.AutoControl.Stop     := CSI_Cmd.AutoControl.MachineCommand.Stop;
	//        		ProductionCommand.AutoControl.Start    := CSI_Cmd.AutoControl.MachineCommand.Start;
	//			  	ProductionCommand.AutoControl.Prepare  := CSI_Cmd.AutoControl.MachineCommand.Prepare;
	//        		ProductionCommand.AutoControl.Complete := CSI_Cmd.AutoControl.MachineCommand.Complete;
	//        		ProductionCommand.AutoControl.Abort    := CSI_Cmd.AutoControl.MachineCommand.Abort;
	//				ProductionCommand.AutoControl.Hold     := CSI_Cmd.AutoControl.MachineCommand.Hold;
	//				ProductionCommand.AutoControl.Reset    := CSI_Cmd.AutoControl.MachineCommand.Reset;
	//				ProductionCommand.AutoControl.Restart  := CSI_Cmd.AutoControl.MachineCommand.Restart;
	//		END_CASE
	//	END_IF
	//	
	//  // Stop machine when LC switches from Manual to Auto mode - Abort command should lead also to reset as in auto mode again
	//  IF ((CSI_Cmd.Status.OperatingMode = L_CSI300_OP_MODE_AUTO) 
	//	   AND (CSI_Cmd_Status_OperatingModeOld = L_CSI300_OP_MODE_MANUAL)) 
	//	   AND (CSI_Cmd.Status.MachineState <> L_SM_STATE_STOPPED) 
	//	THEN
	////		CASE CSI_Cmd.Status.ProductionMode OF
	////			BAK_MACH_PROD_MODE_PROCESSING: // Done in the task
	////    END_CASE;
	//  END_IF;
	//
	//  // Stop machine when LC switches from Auto to Manual mode - Abort command should lead also to reset as in auto mode again
	//  IF ((CSI_Cmd.Status.OperatingMode  = L_CSI300_OP_MODE_MANUAL) 
	//	     OR (CSI_Cmd.Status.OperatingMode = L_CSI300_OP_MODE_LOCAL))
	//     AND (CSI_Cmd_Status_OperatingModeOld = L_CSI300_OP_MODE_AUTO)
	//	   AND (CSI_Cmd.Status.MachineState <> L_SM_STATE_STOPPED) 
	//  THEN
	////		CASE CSI_Cmd.Status.ProductionMode OF
	////				
	////			BAK_MACH_PROD_MODE_PROCESSING:
	////			  ProductionCommand.AutoControl.Stop     := TRUE;
	////    END_CASE;
	//  END_IF;

	//----------------------------------------------------------------------------------------------------------------------
	// AutoControl> Others
	//----------------------------------------------------------------------------------------------------------------------
	// Lamp test (bit 1)
	// Reserved bit. Already taken care of in FB_CSI300_Mach_0;
//	CSI_LampTest  := CSI_Cmd.AutoControl.LampTest;


	//----------------------------------------------------------------------------------------------------------------------
	// Template V4
	//----------------------------------------------------------------------------------------------------------------------
	IF (CSI_Config.VC.Template_Version = 3) THEN	  
		// Template out  
		CSI_TPL.Out.Identification.Machine.Name             := TPL_SWID.In.Cfg.Machine.Name;
		CSI_TPL.Out.Identification.Machine.Type             := TPL_SWID.In.Cfg.Machine.Type;
		CSI_TPL.Out.Identification.Machine.Number           := TPL_SWID.In.Cfg.Machine.Number;
		CSI_TPL.Out.Identification.Machine.ID               := TPL_SWID.In.Cfg.Machine.MachineID;
		CSI_TPL.Out.Identification.Machine.CustomName       := ''; 
		
		CSI_TPL.Out.Identification.Machine.Line             := ''; 
		CSI_TPL.Out.Identification.Machine.Position         := ''; 
		CSI_TPL.Out.Identification.Machine.SoftwareVersion  := TPL_SWID.In.Cfg.Machine.SoftwareVersion;
		CSI_TPL.Out.Identification.Machine.MasterSoftware   := TPL_SWID.In.Cfg.Machine.MasterSoftware;
		CSI_TPL.Out.Identification.Operator.ID              := ''; 
		CSI_TPL.Out.Identification.Operator.PasswordLevel   := TPL_Password.Out.UserLevel;
		CSI_TPL.Out.Network.EthernetAddressReady            := TPL_Netweth.Out.Stat.EthAddressReady;
		CSI_TPL.Out.Network.IP_Address                      := TPL_Netweth.Out.Para.IpAddress;
		CSI_TPL.Out.Network.IP_AddressRemoteStation         := TPL_Netweth.Out.Para.IpAddressRemoteStation;
		CSI_TPL.Out.SystemSettings.OperatingMode            := TPL_System.In.Cfg.OperatingMode;  
		CSI_TPL.Out.SystemSettings.DateTimeActual.Year      := TPL_System.Out.Info.DateTimeActual.year;
		CSI_TPL.Out.SystemSettings.DateTimeActual.Month     := TPL_System.Out.Info.DateTimeActual.month;
		CSI_TPL.Out.SystemSettings.DateTimeActual.Day       := TPL_System.Out.Info.DateTimeActual.day;
		CSI_TPL.Out.SystemSettings.DateTimeActual.DayOfWeek := TPL_System.Out.Info.DateTimeActual.wday;
		CSI_TPL.Out.SystemSettings.DateTimeActual.Hour      := TPL_System.Out.Info.DateTimeActual.hour;
		CSI_TPL.Out.SystemSettings.DateTimeActual.Minute    := TPL_System.Out.Info.DateTimeActual.minute;
		CSI_TPL.Out.SystemSettings.DateTimeActual.Second    := TPL_System.Out.Info.DateTimeActual.second;   
		CSI_TPL.Out.Stat.GlobalStatusIndex                  := TPL_System.Out.Stat.GlobalStatusIndex;

		// Template in
//		TPL_Alarmh.In.Cmd.ExtQuitAllAlarms                  := CSI_TPL.In.AlarmHandling.ExtQuitAllAlarms;
//
//		    IF CSI_Cmd.Recipe.AutoControl.LoadAck THEN
//		     // Load after Ack
//		       TPL_Rec.In.Cmd.LoadRecipeExternalACK             := TRUE;
//		       CSI_Cmd.Recipe.AutoControl.LoadAck               := FALSE;
//		       TPL_Rec.In.Para.FileNumberExternal               := CSI_TPL.In.RecipeHandling.ExtRecipeNumber;
//		    END_IF
//
//		R_TRIG_LoadRecExt(CLK := CSI_TPL.In.RecipeHandling.ExtRecipeLoad);
//		IF R_TRIG_LoadRecExt.Q THEN // Direct load
//			TPL_Rec.In.Cmd.LoadRecipeExternal                 := TRUE;
//			TPL_Rec.In.Para.FileNumberExternal                := CSI_TPL.In.RecipeHandling.ExtRecipeNumber;
//		END_IF;
//
	END_IF;  


	//----------------------------------------------------------------------------------------------------------------------
	// HMI Visual Components
	//----------------------------------------------------------------------------------------------------------------------
//	FB_CSI300_Mach_VC_0(iInitialization:=Initialization, iConfig:=CSI_Config, iActualPage:=TPL_Pageh.Out.ActualPage, ioCSI_STD:=CSI_STD, ioCmd:=CSI_Cmd, ioVC:=CSI_VC);
//
//	CSI_VC.Status.TCPIP.CSI_STD_Online := CSI_Cmd.Status.TCPIP.Online;
//	TPL_System.In.Remote.Status := CSI_Cmd.Status.TCPIP.Online;

	//----------------------------------------------------------------------------------------------------------------------
	// Other.
	//----------------------------------------------------------------------------------------------------------------------
	CSI_Cmd_Status_OperatingModeOld := CSI_Cmd.Status.OperatingMode;
	
	//----------------------------------------------------------------------------------------------------------------------
	// OPC UAOPC UA_Client_Config.
	//----------------------------------------------------------------------------------------------------------------------
//	OPCUA_Client_Config.SessionConnectInfo_0.SecurityMsgMode 								:= UASecurityMsgMode_SignEncrypt;
//	OPCUA_Client_Config.SessionConnectInfo_0.SecurityPolicy   								:= UASecurityPolicy_Basic256Sha256;
//	OPCUA_Client_Config.SessionConnectInfo_0.TransportProfile 								:= UATP_UATcp;
//	OPCUA_Client_Config.SessionConnectInfo_0.UserIdentityToken.UserIdentityTokenType 		:= UAUITT_Username;
//	OPCUA_Client_Config.SessionConnectInfo_0.UserIdentityToken.TokenParam1 					:= 'GEA_Admin';
//	OPCUA_Client_Config.SessionConnectInfo_0.UserIdentityToken.TokenParam2 					:= 'Admin';
//	OPCUA_Client_Config.SessionConnectInfo_0.SessionTimeout 								:= T#1m;
//	OPCUA_Client_Config.SessionConnectInfo_0.MonitorConnection 								:= T#10s;
//	OPCUA_Client_Config.CycleTime			:= TPL_Config.In.Data.MachineDataInterface.CycleTime;
//	OPCUA_Client_Config.SendNodeList 		:= '::AsGlobalPV:CSI_STD.Out'; 
//	OPCUA_Client_Config.ReceiveNodeList 	:= '::AsGlobalPV:CSI_STD.In';
//	OPCUA_Client_Config.ReadPunkt			:= '::CSI_STD:TESTLesen.In';
//	OPCUA_Client_Config.WritePunkt			:= '::CSI_STD:TESTSchreiben.Out';
//	OPCUA_Client_Config.ServerEndpointUrl	:= 'opc.tcp://10.211.220.51:4840';
	
//	IF TPL_Config.In.Data.MachineDataInterface.OPCUA_Client AND TPL_Pageh.Out.PanelIsAlive THEN
//		OPCUA_Client_STD(Execute := TRUE,Config := OPCUA_Client_Config);
//	END_IF

	//----------------------------------------------------------------------------------------------------------------------
	// Reset initialization flag
	//----------------------------------------------------------------------------------------------------------------------
	Initialization := FALSE;

END_PROGRAM
