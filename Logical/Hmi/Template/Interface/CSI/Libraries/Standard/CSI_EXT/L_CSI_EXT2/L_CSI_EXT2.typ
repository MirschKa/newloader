(*Machine*)

TYPE
	T_CSI_EXT2_Gen_Barcode : 	STRUCT 
		Status : T_CSI_EXT2_Gen_Barcode_Status;
		Control : T_CSI_EXT2_Gen_Barcode_Ctrl;
		Data : T_CSI_EXT2_Gen_Barcode_Data;
		Alarms : T_CSI_EXT2_Gen_Alarms;
	END_STRUCT;
	T_CSI_EXT2_Gen_Barcode_Ctrl : 	STRUCT 
		ExternalLoadingDevice : BOOL;
		OneScannerMultipleMachines : BOOL;
		Start : BOOL;
		Stop : BOOL;
		ScannerRequest : BOOL;
		ScannerAssigned : BOOL;
		BarcodeOverruled : BOOL;
		SequenceOrderReceived : BOOL;
		BarcodeScanned : BOOL;
		BarcodeScannedWrongMaterial : BOOL;
		BarcodeScannedWeightTooHigh : BOOL;
		BarcodeScannedUsedBefore : BOOL;
		BarcodeScannedUnknown : BOOL;
		BarcodeScannedNoGS1Code : BOOL;
		ManualAdditionStarted : BOOL;
		ManualAdditionDone : BOOL;
		SelectedMaterial : T_CSI_EXT2_Gen_MaterialInfo;
		ScannedWeight : REAL;
	END_STRUCT;
	T_CSI_EXT2_Gen_Barcode_Data : 	STRUCT 
		BarcodeInfo : T_CSI_EXT2_BarcodeInfo;
		RecipeName : ARRAY[0..39]OF UINT;
		SequenceOrder : UINT;
		Batch_ID : STRING[24];
		ActualBatchSize : REAL;
	END_STRUCT;
	T_CSI_EXT2_Gen_Barcode_Status : 	STRUCT 
		Installed : BOOL;
		Online : BOOL;
		Request : BOOL;
		OperatorRequest : BOOL;
		BarcodeOverruled : BOOL;
		ScanningEnabled : BOOL;
		AcceptBarcodeEnable : BOOL;
		CompleteScanningEnable : BOOL;
		BarcodeAccepted : BOOL;
		BarcodeRejected : BOOL;
		BarcodeCancel : BOOL;
		ScanningCompleted : BOOL;
		SequenceOrderDone : BOOL;
		ManualAdditionAllowed : BOOL;
		ManualAdditionStarted : BOOL;
		ManualAdditionDone : BOOL;
		MaterialSelected : BOOL;
		ContinueOutOfTolerance : BOOL;
		MaterialDone : BOOL;
		Stepper : SINT;
		TotalScannedWeight : INT;
	END_STRUCT;
	T_CSI_EXT2_Gen_MaterialInfo : 	STRUCT 
		ID : STRING[24];
		SequenceOrder : UINT;
		Name : ARRAY[0..39]OF UINT;
	END_STRUCT;
	T_CSI_EXT2_Gen_Alarms : 	STRUCT 
		ResetAllAlarms : BOOL;
		ActiveAlarm : ARRAY[0..7]OF BOOL;
		SetAlarm : ARRAY[0..7]OF BOOL;
		ResetAlarm : ARRAY[0..7]OF BOOL;
	END_STRUCT;
END_TYPE

(*Automation*)

TYPE
	T_CSI_EXT2_Aut_Cmd : 	STRUCT 
		Status : T_CSI_EXT2_Aut_Cmd_Sts;
		AutoControl : T_CSI_EXT2_Aut_Cmd_AutCtrl;
	END_STRUCT;
	T_CSI_EXT2_Aut_Cmd_Sts : 	STRUCT 
		BarcodeAccepted : BOOL;
		BarcodeRejected : BOOL;
		ScanningCompleted : BOOL;
		ManualAdditionDone : BOOL;
		ContinueOutOfTolerance : BOOL;
		SelectedMaterialID : STRING[24];
		RequestScanner : BOOL;
		ScanningEnabled : BOOL;
		TCPIP : T_TCPIP_Comm_Sts;
		BarcodeInfo : T_CSI_EXT2_BarcodeInfo;
	END_STRUCT;
	T_CSI_EXT2_Aut_Cmd_AutCtrl : 	STRUCT 
		ResetBarcodeAccepted : BOOL;
		ResetBarcodeRejected : BOOL;
		ResetScanningCompleted : BOOL;
		ResetManualAdditionDone : BOOL;
		ResetContinueOutOfTolerance : BOOL;
		BarcodeInfo : T_CSI_EXT2_BarcodeInfo;
		ScannerAsigned : BOOL;
		ScanningOverruled : BOOL;
		AcceptBarcodeEnable : BOOL;
		CompleteScanningEnable : BOOL;
		FeedbackAcceptedBarcode : UINT;
		HS_Error_LC : BOOL;
	END_STRUCT;
END_TYPE

(*VC*)

TYPE
	T_CSI_EXT2_VC_Amount : 	STRUCT 
		Setpoint : REAL;
		Actual : REAL;
		Tolerance : REAL;
		Deviation : REAL;
	END_STRUCT;
	T_CSI_EXT2_VC_BarcodeVis : 	STRUCT 
		Material : T_CSI_EXT2_VC_VisMaterial;
		Scanned : T_CSI_EXT2_BarcodeInfo;
		WrongScanned : T_CSI_EXT2_BarcodeInfo;
		StatusDatapoint : INT;
		StatusDataPointRequestButton : INT;
		StatusDatapointAcceptBarcode : INT;
		StatusDatapointRejectBarcode : INT;
		StatusDatapointCOT : UINT;
		StatusDataPointClaimButton : INT;
		StatusDatapointCompleteScanning : INT;
		StatusDatapointClose : INT;
		StatusDatapointBarcodeOverruled : INT;
		SelectedMaterialLine : INT;
		PressedButton : INT;
		ColorDatapointRequestButton : INT;
		ColorDataPointClaimButton : INT;
		Alarms : T_CSI_EXT2_VC_Alarms;
	END_STRUCT;
	T_CSI_EXT2_VC_MaterialAmount : 	STRUCT 
		Setpoint : REAL;
		Actual : REAL;
		Tolerance : REAL;
		Deviation : REAL;
	END_STRUCT;
	T_CSI_EXT2_VC_MaterialInfo : 	STRUCT 
		ID : STRING[24];
		SequenceOrder : UINT;
		Name : ARRAY[0..39]OF UINT;
		Weight : T_CSI_EXT2_VC_MaterialAmount;
	END_STRUCT;
	T_CSI_EXT2_VC_MaterialRecipe : 	STRUCT 
		ID : STRING[24];
		SequenceOrder : UINT;
		Name : ARRAY[0..39]OF UINT;
		Materials : ARRAY[0..4]OF T_CSI_EXT2_VC_VisMaterial;
		Amount : T_CSI_EXT2_VC_Amount;
		Selected1_5 : INT;
		Selected6_10 : INT;
		ColorDatapointLine1_5 : INT;
		ColorDatapointLine6_10 : INT;
		StatusDatapoint : INT;
		ColorDatapointSequenceOrder : INT;
		ColorDatapointActual : INT;
		PressedButton : INT;
		PressedLine : INT;
		OpenScreen : BOOL;
		CloseScreen : BOOL;
		BarcodeRequested : BOOL;
	END_STRUCT;
	T_CSI_EXT2_VC_Materials : 	STRUCT 
		Materials : ARRAY[0..9]OF T_CSI_EXT2_VC_VisMaterial;
	END_STRUCT;
	T_CSI_EXT2_VC_VisMaterial : 	STRUCT 
		ID : STRING[24];
		SequenceOrder : UINT;
		Name : ARRAY[0..39]OF UINT;
		Amount : T_CSI_EXT2_VC_Amount;
		Operation : INT;
		ColorDatapoint : INT;
		ColorDatapointActual : INT;
		ColorDatapointDeviation : INT;
	END_STRUCT;
	T_CSI_EXT2_VC_Alarms : 	STRUCT 
		AlarmImage : ARRAY[0..7]OF BOOL;
		AcknowledgeImage : ARRAY[0..7]OF BOOL;
	END_STRUCT;
END_TYPE

(*CSI EXT2*)

TYPE
	T_CSI_EXT2 : 	STRUCT 
		In : T_CSI_EXT2_In;
		Out : T_CSI_EXT2_Out;
	END_STRUCT;
	T_CSI_EXT2_In : 	STRUCT 
		StructureSize : UDINT;
		BarcodeInfo : T_CSI_EXT2_BarcodeInfo;
		ScannerAsigned : BOOL;
		ScanningOverruled : BOOL;
		AcceptBarcodeEnable : BOOL;
		CompleteScanningEnable : BOOL;
		Handshake : T_HS;
		HS_Error_LC : BOOL;
	END_STRUCT;
	T_CSI_EXT2_Out : 	STRUCT 
		StructureSize : UDINT;
		BarcodeInfo : T_CSI_EXT2_BarcodeInfo;
		SelectedMaterialID : STRING[24];
		RequestScanner : BOOL;
		ScanningEnabled : BOOL;
		Handshake : T_HS;
	END_STRUCT;
	T_CSI_EXT2_BarcodeInfo : 	STRUCT 
		Barcode : STRING[128];
		MaterialID : STRING[24];
		Quantity : REAL;
		BatchID : STRING[32];
		SupplierID : STRING[32];
	END_STRUCT;
END_TYPE
