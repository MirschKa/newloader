/* Automation Studio Generated Header File, Format Version 1.00 */
/* do not change */
#ifndef X_80_H_
#define X_80_H_
#define _WEAK	__attribute__((__weak__))

#include <bur/plctypes.h>

#include <L_CFS200.h>


/* Constants */
_WEAK const signed short L_BARC_COLOR_ACCEPTED = 226;
_WEAK const signed short L_BARC_COLOR_ACTIONREQUIRED = 46;
_WEAK const signed short L_BARC_COLOR_BLACK = 0;
_WEAK const signed short L_BARC_COLOR_DEVIATION = 160;
_WEAK const signed short L_BARC_COLOR_NOTSELECTED = 15;
_WEAK const signed short L_BARC_COLOR_WHITE = 15;
_WEAK const unsigned char L_BARC_DT_BARC_HEADER = 0;
_WEAK const unsigned char L_BARC_DT_BARC_NO_GS1_CODE = 50;
_WEAK const unsigned char L_BARC_DT_BARC_SCANNED_BEFORE = 62;
_WEAK const unsigned char L_BARC_DT_BARC_UNKNOWN = 63;
_WEAK const unsigned char L_BARC_DT_BARC_WEIGHT_TO_HIGH = 61;
_WEAK const unsigned char L_BARC_DT_BARC_WRONG_MATERIAL = 60;
_WEAK const signed short L_BARC_STATUSDATAPOINT_HIDE = 1;
_WEAK const signed short L_BARC_STATUSDATAPOINT_VISIBLE = 0;
_WEAK const unsigned short L_BARC_TG_BARCODE = 200;


/* Datatypes */
typedef struct L_BARC_Amount
{
	float Setpoint;
	float Actual;
	float Tolerance;
	float Deviation;
} L_BARC_Amount;

typedef struct L_BARC_VisMaterial
{
	plcstring ID[24+1];
	unsigned short SequenceOrder;
	unsigned short Name[40];
	L_BARC_Amount Amount;
	signed short Operation;
	signed short ColorDatapoint;
	signed short ColorDatapointActual;
	signed short ColorDatapointDeviation;
} L_BARC_VisMaterial;

typedef struct L_BARC_MaterialRecipe
{
	plcstring ID[24+1];
	unsigned short SequenceOrder;
	unsigned short Name[40];
	L_BARC_VisMaterial Materials[5];
	L_BARC_Amount Amount;
	signed short Selected1_5;
	signed short Selected6_10;
	signed short ColorDatapointLine1_5;
	signed short ColorDatapointLine6_10;
	signed short StatusDatapoint;
	signed short ColorDatapointSequenceOrder;
	signed short ColorDatapointActual;
	signed short PressedButton;
	signed short PressedLine;
	plcbit OpenScreen;
	plcbit CloseScreen;
	plcbit BarcodeRequested;
} L_BARC_MaterialRecipe;

typedef struct L_BARC_Materials
{
	L_BARC_VisMaterial Materials[10];
} L_BARC_Materials;

typedef struct L_BARC_BarcodeVis
{
	L_BARC_VisMaterial Material;
	T200_CSI_CFS_In_BarcodeScanInf Scanned;
	signed short StatusDatapoint;
	signed short StatusDatapointAcceptBarcode;
	signed short StatusDatapointRejectBarcode;
	signed short StatusDatapointMaterialDone;
	signed short StatusDatapointClose;
	signed short SelectedMaterialLine;
	signed short PressedButton;
	signed short ColorDatapointRequestButton;
} L_BARC_BarcodeVis;

typedef struct L_BARC_MaterialAmount
{
	float Setpoint;
	float Actual;
	float Tolerance;
	float Deviation;
} L_BARC_MaterialAmount;

typedef struct L_BARC_MaterialInfo
{
	plcstring ID[24+1];
	unsigned short SequenceOrder;
	unsigned short Name[40];
	L_BARC_MaterialAmount Weight;
} L_BARC_MaterialInfo;



/* Datatypes of function blocks */
typedef struct BarcodeHMI
{
} BarcodeHMI_typ;



/* Prototyping of functions and function blocks */
void BarcodeHMI(BarcodeHMI_typ* inst);



#endif /* X_80_H_ */