//************************************************************************************************************************
// Company:     GEA Food Solutions Bakel
//------------------------------------------------------------------------------------------------------------------------
// Name:        FB_CSI_EXT2_Aut
// Author:      Feike van der Sterren
// Version:     V1.00.0
// Date:        19-nov-2015
//------------------------------------------------------------------------------------------------------------------------
// Restriction: 
//------------------------------------------------------------------------------------------------------------------------
// Description: Function block to handle the barcode functionality on line control level
//************************************************************************************************************************

FUNCTION_BLOCK FB_CSI_EXT2_Aut

	//----------------------------------------------------------------------------------------------------------------------
	// Execution enable input.                                                                                              
	//----------------------------------------------------------------------------------------------------------------------
	IF NOT iEnable THEN
    ;
	  RETURN;
	END_IF;
	
	R_TRIG_0(CLK := iEnable);
	
	//----------------------------------------------------------------------------------------------------------------------
	// Initialization.                                                                                                      
	//----------------------------------------------------------------------------------------------------------------------
	IF R_TRIG_0.Q OR iInitialization THEN
	  Barcode_HS_Cmd.AutoControl.Trigger  := FALSE;
	  Barcode_HS_Cmd.AutoControl.DataIsOK := TRUE;
    ioCmd.Status.BarcodeAccepted        := FALSE;
    ioCmd.Status.BarcodeRejected        := FALSE;
    ioCmd.Status.ContinueOutOfTolerance := FALSE;
    ioCmd.Status.ManualAdditionDone     := FALSE;
    ioCmd.Status.ScanningCompleted      := FALSE;
	END_IF;

	//----------------------------------------------------------------------------------------------------------------------
	// LineControl error HS with MES
	//----------------------------------------------------------------------------------------------------------------------
  ioCSI_EXT2.In.HS_Error_LC := ioCmd.AutoControl.HS_Error_LC;

	//----------------------------------------------------------------------------------------------------------------------
	// CSI barcode to machine
	//----------------------------------------------------------------------------------------------------------------------
  // New barcode scanned
  IF    NOT Barcode_HS_Cmd.Status.Running
    AND NOT Barcode_HS_Cmd.AutoControl.Trigger
  THEN
    IF ( brsmemcmp(ADR(ioCmd.AutoControl.BarcodeInfo), ADR(TempBarcodeInfo), SIZEOF(TempBarcodeInfo)) <> 0)
    THEN
      TempBarcodeInfo                     := ioCmd.AutoControl.BarcodeInfo;
      ioCSI_EXT2.In.BarcodeInfo           := ioCmd.AutoControl.BarcodeInfo;
      Barcode_HS_Cmd.AutoControl.Trigger  := TRUE;
      ioCSI_EXT2.In.Handshake.Parameter   := L_CSI_EXT2_BAR_FR_LC_OK;
    END_IF;
  END_IF;
  // Feedback accepted barcode
  IF    NOT Barcode_HS_Cmd.Status.Running
    AND NOT Barcode_HS_Cmd.AutoControl.Trigger
  THEN
    IF ioCmd.AutoControl.ResetBarcodeAccepted
    THEN
      Barcode_HS_Cmd.AutoControl.Trigger  := TRUE;
      ioCSI_EXT2.In.Handshake.Parameter   := ioCmd.AutoControl.FeedbackAcceptedBarcode;
    END_IF;
  END_IF;
    
  ioCSI_EXT2.In.ScannerAsigned          := ioCmd.AutoControl.ScannerAsigned;
  ioCSI_EXT2.In.ScanningOverruled       := ioCmd.AutoControl.ScanningOverruled;
  ioCSI_EXT2.In.AcceptBarcodeEnable     := ioCmd.AutoControl.AcceptBarcodeEnable;
  ioCSI_EXT2.In.CompleteScanningEnable  := ioCmd.AutoControl.CompleteScanningEnable;
		
	//----------------------------------------------------------------------------------------------------------------------
	// CSI barcode from machine
	//----------------------------------------------------------------------------------------------------------------------
  // 2015-12-24 FvdS Cargill special
  ioCmd.Status.BarcodeInfo.Barcode    := ioCSI_EXT2.Out.BarcodeInfo.Barcode;
  ioCmd.Status.BarcodeInfo.BatchID    := ioCSI_EXT2.Out.BarcodeInfo.BatchID;
  ioCmd.Status.BarcodeInfo.MaterialID := ioCSI_EXT2.Out.BarcodeInfo.MaterialID;
  ioCmd.Status.BarcodeInfo.Quantity   := ioCSI_EXT2.Out.BarcodeInfo.Quantity;
  ioCmd.Status.BarcodeInfo.SupplierID := ioCSI_EXT2.Out.BarcodeInfo.SupplierID;

	//----------------------------------------------------------------------------------------------------------------------
	// Barcode Handshake
	//----------------------------------------------------------------------------------------------------------------------
  Barcode_HS_Cmd.AutoControl.DownloadEnable := ( iCommunicationEnable AND iCommunicationOnline) OR NOT iCommunicationEnable;
  
  // Config  
  Barcode_HS_Config.DataCheck     := FALSE;
  Barcode_HS_Config.FastHandshake := TRUE;
  Barcode_HS_Config.TimeOut       := T#30s; 
  
  FB_HS_Dst_Barcode(
    iEnable           := TRUE,
    iInitialization   := iInitialization,
    iConfig           := Barcode_HS_Config,
    iHandshakeIn      := ioCSI_EXT2.Out.Handshake.Handshake,
    ioHandshakeOut    := ioCSI_EXT2.In.Handshake.Handshake,
    ioCmd             := Barcode_HS_Cmd);

  // Copy received command to control structure 
	IF Barcode_HS_Cmd.Status.HandshakeOK AND NOT Barcode_HS_Cmd.AutoControl.Trigger
  THEN
    IF ( ioCSI_EXT2.Out.Handshake.Parameter = L_CSI_EXT2_BAR_TO_LC_ACCEPTED)
    THEN
      ioCmd.Status.BarcodeAccepted := TRUE;
    ELSIF ( ioCSI_EXT2.Out.Handshake.Parameter = L_CSI_EXT2_BAR_TO_LC_REJECTED)
    THEN
      ioCmd.Status.BarcodeRejected := TRUE;
    ELSIF ( ioCSI_EXT2.Out.Handshake.Parameter = L_CSI_EXT2_BAR_TO_LC_COT)
    THEN
      ioCmd.Status.ContinueOutOfTolerance := TRUE;
    ELSIF ( ioCSI_EXT2.Out.Handshake.Parameter = L_CSI_EXT2_BAR_TO_LC_MANADD_DN)
    THEN
      ioCmd.Status.ManualAdditionDone := TRUE;
    ELSIF ( ioCSI_EXT2.Out.Handshake.Parameter = L_CSI_EXT2_BAR_TO_LC_SCAN_COMPL)
    THEN
      ioCmd.Status.ScanningCompleted := TRUE;
    END_IF;
	END_IF;

  IF Barcode_HS_Cmd.Status.HandshakeOK
  THEN
    Barcode_HS_Cmd.AutoControl.Trigger := FALSE;
    // Reset barcode info, to scan same barcode again and generate trigger
     brsmemset( ADR( ioCmd.AutoControl.BarcodeInfo), 0, SIZEOF( ioCmd.AutoControl.BarcodeInfo)); 
     brsmemset( ADR( TempBarcodeInfo), 0, SIZEOF( TempBarcodeInfo)); 
  END_IF;

  // Reset status
  IF ioCmd.AutoControl.ResetBarcodeAccepted
  THEN
    ioCmd.Status.BarcodeAccepted := FALSE;
  ELSIF ioCmd.AutoControl.ResetBarcodeRejected
  THEN
    ioCmd.Status.BarcodeRejected := FALSE;
  ELSIF ioCmd.AutoControl.ResetContinueOutOfTolerance
  THEN
    ioCmd.Status.ContinueOutOfTolerance := FALSE;
  ELSIF ioCmd.AutoControl.ResetManualAdditionDone
  THEN
    ioCmd.Status.ManualAdditionDone := FALSE;
  ELSIF ioCmd.AutoControl.ResetScanningCompleted
  THEN
    ioCmd.Status.ScanningCompleted := FALSE;
  END_IF;

  //----------------------------------------------------------------------------------------------------------------------
	// Other status info
	//----------------------------------------------------------------------------------------------------------------------
  ioCmd.Status.SelectedMaterialID := ioCSI_EXT2.Out.SelectedMaterialID;
  ioCmd.Status.RequestScanner     := ioCSI_EXT2.Out.RequestScanner;
  ioCmd.Status.ScanningEnabled    := ioCSI_EXT2.Out.ScanningEnabled;

	//----------------------------------------------------------------------------------------------------------------------
	// TCP/IP Communication.                                                                                                
	//----------------------------------------------------------------------------------------------------------------------
  // Configuration
  Config_TCPIP.Installed                     := TRUE;
  Config_TCPIP.Communication.Mode            := L_TCP_SERVER;
  Config_TCPIP.Communication.TimeOut         := T#10s;
  Config_TCPIP.Data.SendStructure            := ADR(ioCSI_EXT2.In);
  Config_TCPIP.Data.SizeOfSendStructure      := SIZEOF(ioCSI_EXT2.In);
  Config_TCPIP.Data.ReceiveStructure         := ADR(ioCSI_EXT2.Out);
  Config_TCPIP.Data.SizeOfReceiveStructure   := SIZEOF(ioCSI_EXT2.Out);

  Config_TCPIP.Port.Mode                     := L_TCP_PORTCTRL_AUTO;
  Config_TCPIP.Port.Automatic.Channel        := 3; 
  Config_TCPIP.Port.Manual.Client            := 0;
  Config_TCPIP.Port.Manual.Server            := 0;

  // Settings
  Settings_TCPIP.Communication.Enable        := ioTPL.Out.Network.EthernetAddressReady AND Config_TCPIP.Installed;
  Settings_TCPIP.IP_Address.Server           := ioTPL.Out.Network.IP_Address;
  Settings_TCPIP.IP_Address.Client           := ioTPL.Out.Network.IP_AddressRemoteStation;

	// Start/stop handling 
	IF Settings_TCPIP.Communication.Enable 
  THEN
	  Communication_Cmd.AutoControl.Start := TRUE;
	ELSE
	  Communication_Cmd.AutoControl.Stop  := TRUE;
	END_IF;

	// Communication 
	FB_TCPIP_0(
	  iConfig       := Config_TCPIP,
    iSettings     := Settings_TCPIP,
	  ioCmd         := Communication_Cmd);

  ioCmd.Status.TCPIP := FB_TCPIP_0.oCommStatus;

	//----------------------------------------------------------------------------------------------------------------------
	// Reset
	//----------------------------------------------------------------------------------------------------------------------
  ioCmd.AutoControl.ResetBarcodeAccepted        := FALSE;
  ioCmd.AutoControl.ResetBarcodeRejected        := FALSE;
  ioCmd.AutoControl.ResetContinueOutOfTolerance := FALSE;
  ioCmd.AutoControl.ResetManualAdditionDone     := FALSE;
  ioCmd.AutoControl.ResetScanningCompleted      := FALSE;

END_FUNCTION_BLOCK