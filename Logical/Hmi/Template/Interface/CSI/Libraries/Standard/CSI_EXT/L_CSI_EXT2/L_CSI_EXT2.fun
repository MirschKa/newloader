
FUNCTION_BLOCK FB_CSI_EXT2_Aut
	VAR_INPUT
		iInitialization : BOOL;
		iEnable : BOOL;
		iCommunicationEnable : BOOL;
		iCommunicationOnline : BOOL;
	END_VAR
	VAR_IN_OUT
		ioCSI_EXT2 : T_CSI_EXT2;
		ioTPL : T300_CSI_TPL;
		ioCmd : T_CSI_EXT2_Aut_Cmd;
	END_VAR
	VAR
		R_TRIG_0 : R_TRIG;
		FB_TCPIP_0 : FB_TCPIP;
		Config_TCPIP : T_TCPIP_Config;
		Settings_TCPIP : T_TCPIP_Settings;
		Communication_Cmd : T_TCPIP_Cmd;
		FB_HS_Dst_Barcode : FB_HS_Dst;
		Barcode_HS_Config : T_HS_Dst_Config;
		Barcode_HS_Cmd : T_HS_Dst_Cmd;
		TempBarcodeInfo : T_CSI_EXT2_BarcodeInfo;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK FB_CSI_EXT2_Mach
	VAR_INPUT
		iEnable : BOOL;
		iInitialization : BOOL;
		iCommunicationEnable : BOOL;
		iCommunicationOnline : BOOL;
		iCSI_In_LineRecipe : T300_CSI_STD_In_Rec;
		iCSI_In_Batch : T300_CSI_STD_In_Batch;
	END_VAR
	VAR_IN_OUT
		ioCSI_EXT2 : T_CSI_EXT2;
		ioTPL : T300_CSI_TPL;
		ioCSI_Barcode : T_CSI_EXT2_Gen_Barcode;
	END_VAR
	VAR
		BarcodeSequence : USINT;
		FB_TCPIP_0 : FB_TCPIP;
		Config_TCPIP : T_TCPIP_Config;
		Settings_TCPIP : T_TCPIP_Settings;
		Communication_Cmd : T_TCPIP_Cmd;
		FB_HS_Src_Barcode : FB_HS_Src;
		Barcode_HS_Config : T_HS_Src_Config;
		Barcode_HS_Cmd : T_HS_Src_Cmd;
		R_TRIG_0 : R_TRIG;
		FB_TON_AlarmHandler : TON;
		i : USINT;
	END_VAR
END_FUNCTION_BLOCK
