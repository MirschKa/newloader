//************************************************************************************************************************
// Company:     GEA Food Solutions Bakel
//------------------------------------------------------------------------------------------------------------------------
// Name:        FB_CSI_EXT2_Mach
// Author:      Feike van der Sterren
// Version:     V1.00.0
// Date:        19-nov-2015
//------------------------------------------------------------------------------------------------------------------------
// Restriction: 
//------------------------------------------------------------------------------------------------------------------------
// Description: Function block to handle the barcode functionality on machine level
//************************************************************************************************************************

FUNCTION_BLOCK FB_CSI_EXT2_Mach

	//----------------------------------------------------------------------------------------------------------------------
	// Execution enable input.                                                                                              
	//----------------------------------------------------------------------------------------------------------------------
	IF NOT iEnable THEN
	  brsmemset(ADR(ioCSI_Barcode.Status)  , 0, SIZEOF(ioCSI_Barcode.Status));
	  brsmemset(ADR(ioCSI_Barcode.Control) , 0, SIZEOF(ioCSI_Barcode.Control));
	  brsmemset(ADR(ioCSI_Barcode.Data)    , 0, SIZEOF(ioCSI_Barcode.Data));
	  brsmemset(ADR(ioCSI_EXT2.Out)       , 0, SIZEOF(ioCSI_EXT2.Out));
	  ioCSI_Barcode.Status.Installed := TRUE;
	  BarcodeSequence := 0;
	  Barcode_HS_Cmd.AutoControl.Trigger := FALSE;
    ioCSI_EXT2.Out.Handshake.Parameter := 0;
	  RETURN;
	END_IF;
	
	R_TRIG_0(CLK := iEnable);
	
	//----------------------------------------------------------------------------------------------------------------------
	// Initialization.                                                                                                      
	//----------------------------------------------------------------------------------------------------------------------
	IF R_TRIG_0.Q OR iInitialization THEN
	  brsmemset(ADR(ioCSI_Barcode.Status)  , 0, SIZEOF(ioCSI_Barcode.Status));
	  brsmemset(ADR(ioCSI_Barcode.Control) , 0, SIZEOF(ioCSI_Barcode.Control));
	  brsmemset(ADR(ioCSI_Barcode.Data)    , 0, SIZEOF(ioCSI_Barcode.Data));
	  brsmemset(ADR(ioCSI_EXT2.Out)       , 0, SIZEOF(ioCSI_EXT2.Out));
	  BarcodeSequence := 0;
	  Barcode_HS_Cmd.AutoControl.Trigger := FALSE;
	END_IF;
	
	//----------------------------------------------------------------------------------------------------------------------
	// Barcode Handshake
	//----------------------------------------------------------------------------------------------------------------------
	// Send commands via handshake to LC 
	IF ioCSI_Barcode.Status.BarcodeAccepted  THEN
	  ioCSI_EXT2.Out.Handshake.Parameter := L_CSI_EXT2_BAR_TO_LC_ACCEPTED;
	  Barcode_HS_Cmd.AutoControl.Trigger := TRUE;
	ELSIF ioCSI_Barcode.Status.BarcodeRejected THEN
	  ioCSI_EXT2.Out.Handshake.Parameter := L_CSI_EXT2_BAR_TO_LC_REJECTED;
	  Barcode_HS_Cmd.AutoControl.Trigger := TRUE;
	ELSIF ioCSI_Barcode.Status.ManualAdditionStarted THEN
	  ioCSI_EXT2.Out.Handshake.Parameter := L_CSI_EXT2_BAR_TO_LC_MANADD_STRT;
	  Barcode_HS_Cmd.AutoControl.Trigger := TRUE;
	ELSIF ioCSI_Barcode.Status.ManualAdditionDone THEN
	  ioCSI_EXT2.Out.Handshake.Parameter := L_CSI_EXT2_BAR_TO_LC_MANADD_DN;
	  Barcode_HS_Cmd.AutoControl.Trigger := TRUE;
	ELSIF ioCSI_Barcode.Status.ContinueOutOfTolerance THEN
	  ioCSI_EXT2.Out.Handshake.Parameter := L_CSI_EXT2_BAR_TO_LC_COT;
	  Barcode_HS_Cmd.AutoControl.Trigger := TRUE;
	ELSIF ioCSI_Barcode.Status.ScanningCompleted THEN
	  ioCSI_EXT2.Out.Handshake.Parameter := L_CSI_EXT2_BAR_TO_LC_SCAN_COMPL;
	  Barcode_HS_Cmd.AutoControl.Trigger := TRUE;
	END_IF;

  Barcode_HS_Cmd.AutoControl.DownloadEnable         := ( iCommunicationEnable AND iCommunicationOnline) OR NOT iCommunicationEnable;
  Barcode_HS_Cmd.AutoControl.DataTransferCompleted  := ( iCommunicationEnable AND iCommunicationOnline) OR NOT iCommunicationEnable;

  // Config  
  Barcode_HS_Config.FastHandshake   := TRUE;
  Barcode_HS_Config.TimeOut         := T#30s; 
   
  FB_HS_Src_Barcode(
    iEnable           := TRUE,
    iInitialization   := iInitialization,
    iConfig           := Barcode_HS_Config,
    iHandshakeIn      := ioCSI_EXT2.In.Handshake.Handshake,
    ioHandshakeOut    := ioCSI_EXT2.Out.Handshake.Handshake,
    ioCmd             := Barcode_HS_Cmd);
		
	// Copy status data bits to control struct 
	ioCSI_Barcode.Control.ScannerAssigned  := ioCSI_EXT2.In.ScannerAsigned OR (ioCSI_EXT2.In.ScanningOverruled AND BarcodeSequence > 10 AND BarcodeSequence < 20);
	ioCSI_Barcode.Control.BarcodeOverruled := ioCSI_EXT2.In.ScanningOverruled;
	
	// Copy stepper info to status structure 
	ioCSI_Barcode.Status.Stepper := BarcodeSequence;
	
	ioCSI_EXT2.Out.ScanningEnabled  := ioCSI_Barcode.Status.ScanningEnabled;
	ioCSI_EXT2.Out.RequestScanner   := ioCSI_Barcode.Status.Request;
	
	// Copy received command to control structure 
	IF Barcode_HS_Cmd.Status.HandshakeOK AND NOT Barcode_HS_Cmd.AutoControl.Trigger THEN
	  IF    (ioCSI_EXT2.In.Handshake.Parameter = L_CSI_EXT2_BAR_FR_LC_OK) THEN
	    ioCSI_Barcode.Control.BarcodeScanned := TRUE;
	    ioCSI_Barcode.Data.BarcodeInfo := ioCSI_EXT2.In.BarcodeInfo;
	  ELSIF (ioCSI_EXT2.In.Handshake.Parameter = L_CSI_EXT2_BAR_FR_LC_WRONG_MAT) THEN
	    ioCSI_Barcode.Control.BarcodeScannedWrongMaterial := TRUE;
	    ioCSI_Barcode.Data.BarcodeInfo := ioCSI_EXT2.In.BarcodeInfo;
	  ELSIF (ioCSI_EXT2.In.Handshake.Parameter = L_CSI_EXT2_BAR_FR_LC_NOK_WEIGHT) THEN
	    ioCSI_Barcode.Control.BarcodeScannedWeightTooHigh := TRUE;
	    ioCSI_Barcode.Data.BarcodeInfo := ioCSI_EXT2.In.BarcodeInfo;
	  ELSIF (ioCSI_EXT2.In.Handshake.Parameter = L_CSI_EXT2_BAR_FR_LC_USED_BEFORE) THEN
	    ioCSI_Barcode.Control.BarcodeScannedUsedBefore := TRUE;
	    ioCSI_Barcode.Data.BarcodeInfo := ioCSI_EXT2.In.BarcodeInfo;
	  ELSIF (ioCSI_EXT2.In.Handshake.Parameter = L_CSI_EXT2_BAR_FR_LC_UNKNOWN) THEN
	    ioCSI_Barcode.Control.BarcodeScannedUnknown := TRUE;
	    ioCSI_Barcode.Data.BarcodeInfo := ioCSI_EXT2.In.BarcodeInfo;
	  ELSIF (ioCSI_EXT2.In.Handshake.Parameter = L_CSI_EXT2_BAR_FR_LC_NO_GS1) THEN
	    ioCSI_Barcode.Control.BarcodeScannedNoGS1Code := TRUE;
	    ioCSI_Barcode.Data.BarcodeInfo := ioCSI_EXT2.In.BarcodeInfo;
	  END_IF;
  ELSIF   Barcode_HS_Cmd.Status.HandshakeNotOK
       OR ioCSI_EXT2.In.HS_Error_LC
  THEN
    ioCSI_Barcode.Alarms.SetAlarm[0] := TRUE;
	END_IF;

	//----------------------------------------------------------------------------------------------------------------------
	// CSI_GEA> Barcode.                                                                                                    
	//----------------------------------------------------------------------------------------------------------------------
	ioCSI_Barcode.Data.SequenceOrder             := iCSI_In_Batch.ActualSequenceOrder;
	ioCSI_Barcode.Data.ActualBatchSize           := iCSI_In_Batch.ActualBatchSize;

  ioCSI_Barcode.Status.AcceptBarcodeEnable     := ioCSI_EXT2.In.AcceptBarcodeEnable; 
  ioCSI_Barcode.Status.CompleteScanningEnable  := ioCSI_EXT2.In.CompleteScanningEnable;
	  
  ioCSI_EXT2.Out.BarcodeInfo.Quantity   := ioCSI_Barcode.Control.ScannedWeight; // 2015-12-24 FvdS, Cargill special

	CASE BarcodeSequence OF
	  // Waiting step 
	  00:
      ioCSI_Barcode.Status.ManualAdditionAllowed := FALSE; //2015-12-16 FvdS
      ioCSI_EXT2.Out.SelectedMaterialID     := '';

 	    // Machine knows when there is a request for a barcode action 
	    IF ioCSI_Barcode.Control.Start THEN
	      IF ioCSI_Barcode.Control.OneScannerMultipleMachines THEN
	        BarcodeSequence := 1;
	        ioCSI_Barcode.Status.OperatorRequest := TRUE;
	      ELSE
	        ioCSI_Barcode.Status.Request := TRUE;
	        BarcodeSequence := 11;
	      END_IF
	    END_IF;
	
	  // Wait till operator tries to claim the scanner 
	  01:
	    IF ioCSI_Barcode.Control.ScannerRequest THEN
	      ioCSI_Barcode.Status.Request := TRUE;
	      BarcodeSequence := 11;
	    ELSIF ioCSI_Barcode.Control.Stop THEN
	      ioCSI_Barcode.Status.Request := FALSE;
	      ioCSI_Barcode.Status.OperatorRequest := FALSE;
	      BarcodeSequence := 0;
	    END_IF;
	
	  // Wait until 'accepted' is received from LC 
	  11:
	    IF ioCSI_Barcode.Control.ScannerAssigned THEN
	      ioCSI_Barcode.Data.Batch_ID        := iCSI_In_LineRecipe.Header.BatchID;
	      ioCSI_Barcode.Data.RecipeName      := iCSI_In_LineRecipe.Header.RecipeName;
	      BarcodeSequence := 13;
	    // Decline request when stop is given from application 
	    ELSIF ioCSI_Barcode.Control.Stop THEN
	      ioCSI_Barcode.Status.Request := FALSE;
	      ioCSI_Barcode.Status.OperatorRequest := FALSE;
	      BarcodeSequence := 0;
	    END_IF;
	
	  // Wait till operator claims Barcode or LC cancels request 
	  13:
	    IF ioCSI_Barcode.Status.MaterialSelected THEN
	      ioCSI_EXT2.Out.SelectedMaterialID := ioCSI_Barcode.Control.SelectedMaterial.ID;
	      ioCSI_Barcode.Status.ScanningEnabled  := TRUE;
	      ioCSI_Barcode.Status.MaterialSelected := FALSE;
	      BarcodeSequence := 15;
	    ELSIF ioCSI_Barcode.Status.SequenceOrderDone THEN
	      ioCSI_Barcode.Status.ManualAdditionAllowed := TRUE;
	      ioCSI_Barcode.Status.Request := FALSE;
	      ioCSI_Barcode.Status.OperatorRequest := FALSE;
	      BarcodeSequence := 20;
	      ioCSI_Barcode.Status.Request := FALSE;
	    // Decline request when stop is given from application 
	    ELSIF ioCSI_Barcode.Control.Stop THEN
	      ioCSI_Barcode.Status.Request := FALSE;
	      IF ioCSI_Barcode.Control.OneScannerMultipleMachines THEN
	        BarcodeSequence := 1;
	      ELSE
	        BarcodeSequence := 0;
	      END_IF
	    END_IF;
	
	  // Wait for a scan received from LC 
	  15:
	    IF ioCSI_Barcode.Control.BarcodeScanned THEN
	      BarcodeSequence := 16;
	    ELSIF NOT ioCSI_Barcode.Status.ScanningEnabled THEN
	      BarcodeSequence := 13;
	    ELSIF ioCSI_Barcode.Status.SequenceOrderDone THEN
	      ioCSI_Barcode.Status.ManualAdditionAllowed := TRUE;
	      ioCSI_Barcode.Status.OperatorRequest := FALSE;
	      ioCSI_Barcode.Status.Request := FALSE;
	      BarcodeSequence := 20;
	    ELSIF ioCSI_Barcode.Status.ScanningCompleted THEN
	      BarcodeSequence := 13;
	    // Decline request when stop is given from application 
	    ELSIF ioCSI_Barcode.Control.Stop THEN
	      ioCSI_Barcode.Status.Request := FALSE;
	      IF ioCSI_Barcode.Control.OneScannerMultipleMachines THEN
	        BarcodeSequence := 1;
	      ELSE
	        BarcodeSequence := 0;
	      END_IF
	    END_IF;
	
	  // Wait for operator feedback on the scanned code 
	  16:
	    IF NOT ioCSI_Barcode.Status.ScanningEnabled THEN
	      BarcodeSequence := 13;
	    ELSIF ioCSI_Barcode.Status.BarcodeAccepted THEN
	      BarcodeSequence := 15;
	    ELSIF ioCSI_Barcode.Status.BarcodeRejected THEN
	      BarcodeSequence := 15;
	    ELSIF ioCSI_Barcode.Status.ScanningCompleted THEN
	      BarcodeSequence := 13;
	    // Decline request when stop is given from application 
	    ELSIF ioCSI_Barcode.Control.Stop THEN
	      ioCSI_Barcode.Status.Request := FALSE;
	      IF ioCSI_Barcode.Control.OneScannerMultipleMachines THEN
	        BarcodeSequence := 1;
	      ELSE
	        BarcodeSequence := 0;
	      END_IF
	    END_IF;
	
	  // Wait till manual addition is started by application 
	  20:
	    IF ioCSI_Barcode.Control.ManualAdditionStarted THEN
	      ioCSI_Barcode.Status.ManualAdditionStarted := TRUE;
	      BarcodeSequence := 21;
	    // Decline request when stop is given from application 
	    ELSIF ioCSI_Barcode.Control.Stop THEN
	      ioCSI_Barcode.Status.Request := FALSE;
	      IF ioCSI_Barcode.Control.OneScannerMultipleMachines THEN
	        BarcodeSequence := 1;
	      ELSE
	        BarcodeSequence := 0;
	      END_IF
	    END_IF;
	
	  // Wait till manual addition is finished 
	  21:
	    IF ioCSI_Barcode.Control.ManualAdditionDone THEN
	      ioCSI_Barcode.Status.ManualAdditionDone := TRUE;
	      ioCSI_Barcode.Status.ManualAdditionAllowed := FALSE;
	      ioCSI_Barcode.Status.Request := FALSE;
	      BarcodeSequence := 0 ;
	    // Decline request when stop is given from application 
	    ELSIF ioCSI_Barcode.Control.Stop THEN
	      ioCSI_Barcode.Status.Request := FALSE;
	      IF ioCSI_Barcode.Control.OneScannerMultipleMachines THEN
	        BarcodeSequence := 1;
	      ELSE
	        BarcodeSequence := 0;
	      END_IF
	    END_IF;
	
	END_CASE;
	
	// Reset control bits 
	ioCSI_Barcode.Control.Start                 := FALSE;
	ioCSI_Barcode.Control.Stop                  := FALSE;
	ioCSI_Barcode.Control.ScannerRequest        := FALSE;
	ioCSI_Barcode.Control.SequenceOrderReceived := FALSE;
	ioCSI_Barcode.Control.ManualAdditionStarted := FALSE;
	ioCSI_Barcode.Control.ManualAdditionDone    := FALSE;
	ioCSI_Barcode.Status.SequenceOrderDone      := FALSE;
	
	// Reset command when handshake OK 
	IF Barcode_HS_Cmd.Status.HandshakeOK AND Barcode_HS_Cmd.AutoControl.Trigger THEN
	  // Clear status when send to LC 
	  ioCSI_Barcode.Status.BarcodeAccepted          := FALSE;
	  ioCSI_Barcode.Status.BarcodeRejected          := FALSE;
	  ioCSI_Barcode.Status.BarcodeCancel            := FALSE;
	  ioCSI_Barcode.Status.ScanningCompleted        := FALSE;
	  ioCSI_Barcode.Status.SequenceOrderDone        := FALSE;
	  ioCSI_Barcode.Status.ManualAdditionStarted    := FALSE;
	  ioCSI_Barcode.Status.ManualAdditionDone       := FALSE;
	  ioCSI_Barcode.Status.ContinueOutOfTolerance   := FALSE;
	  Barcode_HS_Cmd.AutoControl.Trigger            := FALSE;
	END_IF;

	//----------------------------------------------------------------------------------------------------------------------
	// TCP/IP Communication.                                                                                                
	//----------------------------------------------------------------------------------------------------------------------
  // Configuration
  Config_TCPIP.Installed                     := TRUE;
  Config_TCPIP.Communication.Mode            := L_TCP_CLIENT;
  Config_TCPIP.Communication.TimeOut         := T#10s;
  Config_TCPIP.Data.SendStructure            := ADR(ioCSI_EXT2.Out);
  Config_TCPIP.Data.SizeOfSendStructure      := SIZEOF(ioCSI_EXT2.Out);
  Config_TCPIP.Data.ReceiveStructure         := ADR(ioCSI_EXT2.In);
  Config_TCPIP.Data.SizeOfReceiveStructure   := SIZEOF(ioCSI_EXT2.In);

  Config_TCPIP.Port.Mode                     := L_TCP_PORTCTRL_AUTO;
  Config_TCPIP.Port.Automatic.Channel        := 3; 
  Config_TCPIP.Port.Manual.Client            := 0;
  Config_TCPIP.Port.Manual.Server            := 0;

  // Settings
  Settings_TCPIP.Communication.Enable        := ioTPL.Out.Network.EthernetAddressReady AND Config_TCPIP.Installed;
  Settings_TCPIP.IP_Address.Server           := ioTPL.Out.Network.IP_AddressRemoteStation;
  Settings_TCPIP.IP_Address.Client           := ioTPL.Out.Network.IP_Address;

	// Start/stop handling 
	IF Settings_TCPIP.Communication.Enable 
  THEN
	  Communication_Cmd.AutoControl.Start := TRUE;
	ELSE
	  Communication_Cmd.AutoControl.Stop  := TRUE;
	END_IF;

	// Communication 
	FB_TCPIP_0(
	  iConfig       := Config_TCPIP,
    iSettings     := Settings_TCPIP,
	  ioCmd         := Communication_Cmd);

  ioCSI_Barcode.Status.Online := FB_TCPIP_0.oCommStatus.Online;

  //----------------------------------------------------------------------------------------------------------------------
  // Alarm handling
  //----------------------------------------------------------------------------------------------------------------------
  FB_TON_AlarmHandler(
    IN := NOT FB_TON_AlarmHandler.Q,
    PT := T#1S);
  
  IF FB_TON_AlarmHandler.Q
  THEN
    FOR i := 0 TO (SIZEOF(ioCSI_Barcode.Alarms.ActiveAlarm) / SIZEOF(ioCSI_Barcode.Alarms.ActiveAlarm[0])) - 1 DO
      IF ioCSI_Barcode.Alarms.SetAlarm[i]
      THEN
        ioCSI_Barcode.Alarms.ActiveAlarm[i] := TRUE;
      ELSIF    ioCSI_Barcode.Alarms.ResetAlarm[i]
            OR ioCSI_Barcode.Alarms.ResetAllAlarms
      THEN
        ioCSI_Barcode.Alarms.ActiveAlarm[i] := FALSE;
        ioCSI_Barcode.Alarms.ResetAlarm[i] := FALSE;
      END_IF;
      ioCSI_Barcode.Alarms.SetAlarm[i] := FALSE;
    END_FOR;
    ioCSI_Barcode.Alarms.ResetAllAlarms := FALSE;
  END_IF;

END_FUNCTION_BLOCK