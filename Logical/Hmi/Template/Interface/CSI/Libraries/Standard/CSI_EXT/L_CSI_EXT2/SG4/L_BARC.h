/* Automation Studio Generated Header File, Format Version 1.00 */
/* do not change */
#ifndef L_BARC_H_
#define L_BARC_H_
#define _WEAK	__attribute__((__weak__))

#include <bur/plctypes.h>

#include <L_CFS200.h>
#include <CFS_Dlg.h>
#include <X_30.h>
#include <standard.h>
#include <GEA_HS.h>
#include <L_CFS200.h>


/* Constants */
_WEAK const signed short L_BARC_COLOR_ACCEPTED = 226;
_WEAK const signed short L_BARC_COLOR_ACTIONREQUIRED = 46;
_WEAK const signed short L_BARC_COLOR_BLACK = 0;
_WEAK const signed short L_BARC_COLOR_DEVIATION = 160;
_WEAK const signed short L_BARC_COLOR_GREY = 8;
_WEAK const signed short L_BARC_COLOR_NOTSELECTED = 15;
_WEAK const signed short L_BARC_COLOR_WHITE = 15;
_WEAK const unsigned char L_BARC_DT_BARC_CANCEL_REQUEST = 65;
_WEAK const unsigned char L_BARC_DT_BARC_HEADER = 18;
_WEAK const unsigned char L_BARC_DT_BARC_NO_GS1_CODE = 64;
_WEAK const unsigned char L_BARC_DT_BARC_SCANNED_BEFORE = 62;
_WEAK const unsigned char L_BARC_DT_BARC_UNKNOWN = 63;
_WEAK const unsigned char L_BARC_DT_BARC_WEIGHT_TO_HIGH = 61;
_WEAK const unsigned char L_BARC_DT_BARC_WRONG_MATERIAL = 60;
_WEAK const signed short L_BARC_STATUSDATAPOINT_HIDE = 1;
_WEAK const signed short L_BARC_STATUSDATAPOINT_VISIBLE = 0;
_WEAK const unsigned short L_BARC_TG_BARCODE = 200;


/* Datatypes */
typedef struct L_BARC_MaterialAmount
{
	float Setpoint;
	float Actual;
	float Tolerance;
	float Deviation;
} L_BARC_MaterialAmount;

typedef struct L_BARC_MaterialInfo
{
	plcstring ID[24+1];
	unsigned short SequenceOrder;
	unsigned short Name[40];
	L_BARC_MaterialAmount Weight;
} L_BARC_MaterialInfo;

typedef struct L_BARC_Amount
{
	float Setpoint;
	float Actual;
	float Tolerance;
	float Deviation;
} L_BARC_Amount;

typedef struct L_BARC_VisMaterial
{
	plcstring ID[24+1];
	unsigned short SequenceOrder;
	unsigned short Name[40];
	L_BARC_Amount Amount;
	signed short Operation;
	signed short ColorDatapoint;
	signed short ColorDatapointActual;
	signed short ColorDatapointDeviation;
} L_BARC_VisMaterial;

typedef struct L_BARC_BarcodeVis
{
	L_BARC_VisMaterial Material;
	T220_CSI_GEA_In_Barcode_Scan Scanned;
	T220_CSI_GEA_In_Barcode_Scan WrongScanned;
	signed short StatusDatapoint;
	signed short StatusDataPointRequestButton;
	signed short StatusDatapointAcceptBarcode;
	signed short StatusDatapointRejectBarcode;
	signed short StatusDataPointClaimButton;
	signed short StatusDatapointMaterialDone;
	signed short StatusDatapointClose;
	signed short StatusDatapointBarcodeOverruled;
	signed short SelectedMaterialLine;
	signed short PressedButton;
	signed short ColorDatapointRequestButton;
	signed short ColorDataPointClaimButton;
} L_BARC_BarcodeVis;

typedef struct L_BARC_Materials
{
	L_BARC_VisMaterial Materials[10];
} L_BARC_Materials;

typedef struct L_BARC_MaterialRecipe
{
	plcstring ID[24+1];
	unsigned short SequenceOrder;
	unsigned short Name[40];
	L_BARC_VisMaterial Materials[5];
	L_BARC_Amount Amount;
	signed short Selected1_5;
	signed short Selected6_10;
	signed short ColorDatapointLine1_5;
	signed short ColorDatapointLine6_10;
	signed short StatusDatapoint;
	signed short ColorDatapointSequenceOrder;
	signed short ColorDatapointActual;
	signed short PressedButton;
	signed short PressedLine;
	plcbit OpenScreen;
	plcbit CloseScreen;
	plcbit BarcodeRequested;
} L_BARC_MaterialRecipe;



/* Datatypes of function blocks */
typedef struct FB_CSI_Barcode_HMI
{
	/* VAR_INOUT (analogous) */
	struct L_BARC_VisMaterial (*Ingredients)[10];
	struct T220_CSI_GEA_Gen_Barcode* CSI_Barcode;
	struct dialogringbuffer* DialogRingBuffer;
	struct L_BARC_BarcodeVis* VisDialogBarcodeHandling;
	struct L_BARC_MaterialRecipe* VisDialogIngredients;
	/* VAR (analogous) */
	struct PopUpDialog PopUpDialog_BarcodeWrongMaterial;
	struct PopUpDialog PopUpDialog_BarcodeWeightHigh;
	struct PopUpDialog PopUpDialog_BarcodeUsedBefore;
	struct PopUpDialog PopUpDialog_BarcodeUnknown;
	struct PopUpDialog PopUpDialog_BarcodeNoGS1Code;
	struct PopUpDialog PopUpDialog_BarcodeResetRequest;
	struct DialogCommand CMD_PopUp_BarcodeWrongMaterial;
	struct DialogCommand CMD_PopUp_BarcodeWeightTooHigh;
	struct DialogCommand CMD_PopUp_BarcodeUsedBefore;
	struct DialogCommand CMD_PopUp_BarcodeUnknown;
	struct DialogCommand CMD_PopUp_BarcodeNoGS1Code;
	struct DialogCommand CMD_PopUp_BarcodeResetRequest;
	struct TON TON_Clock_2_sec;
	struct TON TON_NOT_Clock_2_sec;
	signed short i;
	signed short TotalScannedWeight;
	struct R_TRIG R_TRIG_1;
	/* VAR_INPUT (digital) */
	plcbit EN;
	plcbit Initialization;
	/* VAR (digital) */
	plcbit Clock_2_sec;
	plcbit zzEdge00000;
	plcbit LastBarcodeRequested;
} FB_CSI_Barcode_HMI_typ;

typedef struct FB_CSI_Barcode
{
	/* VAR_INOUT (analogous) */
	struct T220_CSI_GEA* ioCSI;
	struct T220_CSI_GEA_Gen_Barcode_Status* ioCSI_BarcodeStatus;
	struct T220_CSI_GEA_Gen_Barcode_Ctrl* ioCSI_BarcodeControl;
	struct T220_CSI_GEA_Gen_Barcode_Data* ioCSI_BarcodeData;
	/* VAR (analogous) */
	unsigned char BarcodeSequence;
	struct FB_Handshake FB_Handshake_Barcode;
	struct R_TRIG R_TRIG_1;
	/* VAR_INPUT (digital) */
	plcbit iEN;
	plcbit iInitialization;
	plcbit iCommunicationEnable;
	plcbit iCommunicationOnline;
	/* VAR (digital) */
	plcbit TriggerHandshakeBarcode;
} FB_CSI_Barcode_typ;



/* Prototyping of functions and function blocks */
void FB_CSI_Barcode_HMI(FB_CSI_Barcode_HMI_typ* inst);
void FB_CSI_Barcode(FB_CSI_Barcode_typ* inst);



#endif /* L_BARC_H_ */