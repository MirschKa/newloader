
FUNCTION_BLOCK FB_ProgramExistCheck
	VAR_INPUT
		iEnable : BOOL;
		iInitialization : BOOL;
		iConfig : T_ProgCheck_Config;
	END_VAR
	VAR_IN_OUT
		ioCmd : T_ProgCheck_Cmd;
	END_VAR
	VAR
		FileName : STRING[32];
		FolderName : STRING[32];
		SizeOfProgramNumber : UDINT;
		Stepper : USINT;
		SearchFileName_Program : SearchFileName;
		ProgramNumberConvertStatus : UINT;
		R_TRIG_0 : R_TRIG;
		TON_AlarmHandler : TON;
		i : USINT;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK FB_ProgramExistCheck_VC (* *) (*$GROUP=User*)
	VAR_INPUT
		iInitialization : BOOL;
		iEnable : BOOL;
	END_VAR
	VAR_IN_OUT
		ioCmd : T_ProgCheck_Cmd;
		ioVC : T_ProgCheck_VC;
	END_VAR
	VAR
		Temp : USINT;
		TON_AlarmHandler : TON;
		i : USINT;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK FB_FilterRecipeOperations (* *) (*$GROUP=User*)
	VAR_INPUT
		iInitialization : BOOL;
		iEnable : BOOL;
		iConfigWorkPlaceID : ARRAY[0..3] OF UINT;
		iRecipeOperations : ARRAY[0..31] OF T300_CSI_STD_Gen_Rec_Operations;
	END_VAR
	VAR_OUTPUT
		oRecipeOperations : ARRAY[0..31] OF T300_CSI_STD_Gen_Rec_Operations;
		oIndexOriginalRecipe : ARRAY[0..31] OF UINT;
	END_VAR
	VAR
		i : UINT;
		j : UINT;
		WorkPlaceID : UINT;
		Temp_STRING : STRING[8];
	END_VAR
END_FUNCTION_BLOCK
