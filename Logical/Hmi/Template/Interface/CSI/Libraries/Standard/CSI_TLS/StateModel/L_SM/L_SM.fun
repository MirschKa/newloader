
FUNCTION_BLOCK FB_SM
	VAR_INPUT
		iEnable : BOOL;
		iInitialization : BOOL;
	END_VAR
	VAR_IN_OUT
		ioStateModel : T_SM;
	END_VAR
	VAR
		R_TRIG_0 : R_TRIG;
		RTInfo_0 : RTInfo;
		TimeBetweenBlockCalls : UDINT;
		HoldState : UDINT;
		Sequence : FB_Sequencer;
	END_VAR
END_FUNCTION_BLOCK
