//************************************************************************************************************************
// Company:     GEA Food Solutions
//------------------------------------------------------------------------------------------------------------------------
// Name:        FB_SM
// Author:      Bart van Leuken 
// Version:     V1.03.0
// Date:        19-nov-2015
//------------------------------------------------------------------------------------------------------------------------
// Restriction: None 
//
//------------------------------------------------------------------------------------------------------------------------
// Add info:    Compliant with GEA State model V0.80 
//
//------------------------------------------------------------------------------------------------------------------------
// Description: Function block to handle the GEA State Model 
//************************************************************************************************************************

FUNCTION_BLOCK FB_SM

	//----------------------------------------------------------------------------------------------------------------------
	// Execution enable input.                                                                                              
	//----------------------------------------------------------------------------------------------------------------------
	IF NOT iEnable THEN
	  iInitialization := iInitialization;
	END_IF;
	
	R_TRIG_0(CLK := iEnable);
	
	IF NOT iEnable THEN RETURN; END_IF;

	//----------------------------------------------------------------------------------------------------------------------
	// Commands - Priority.                                                                                                 
	//----------------------------------------------------------------------------------------------------------------------
	IF R_TRIG_0.Q OR iInitialization THEN
    RTInfo_0(enable := TRUE ); 
    TimeBetweenBlockCalls := RTInfo_0.cycle_time / 1000; 
    Sequence.iInitialization := TRUE;
  END_IF;
	
	//----------------------------------------------------------------------------------------------------------------------
	// Commands - Priority.                                                                                                 
	//----------------------------------------------------------------------------------------------------------------------
	IF ioStateModel.Command.Abort THEN
	  ioStateModel.Command.Prepare   := FALSE;
	  ioStateModel.Command.Start     := FALSE;
	  ioStateModel.Command.Stop      := FALSE;
	  ioStateModel.Command.Hold      := FALSE;
	  ioStateModel.Command.Restart   := FALSE;
	  ioStateModel.Command.Reset     := FALSE;
	  ioStateModel.Command.Complete  := FALSE;
	ELSIF ioStateModel.Command.Stop THEN
	  ioStateModel.Command.Prepare   := FALSE;
	  ioStateModel.Command.Start     := FALSE;
	  ioStateModel.Command.Hold      := FALSE;
	  ioStateModel.Command.Restart   := FALSE;
	  ioStateModel.Command.Complete  := FALSE;
	END_IF;
	
	//----------------------------------------------------------------------------------------------------------------------
	// Commands - Block.                                                                                                    
	//----------------------------------------------------------------------------------------------------------------------
	IF ioStateModel.Block.Prepare THEN
	  ioStateModel.Command.Prepare  := FALSE;
	END_IF;
	
	IF ioStateModel.Block.Start THEN
	  ioStateModel.Command.Start    := FALSE;
	END_IF;
	
	IF ioStateModel.Block.Restart THEN
	  ioStateModel.Command.Restart  := FALSE;
	END_IF;
	
	IF ioStateModel.Block.Reset THEN
	  ioStateModel.Command.Reset    := FALSE;
	END_IF;
	
	IF ioStateModel.Block.Complete THEN
	  ioStateModel.Command.Complete := FALSE;
	END_IF;
	
	//----------------------------------------------------------------------------------------------------------------------
	// State model - Transitions.                                                                                           
	//----------------------------------------------------------------------------------------------------------------------
	CASE Sequence.oStep OF
	
	  L_SM_STATE_NOT_AVAILABLE :  // S0000 
	
	    IF ioStateModel.Transition.NotAvailableToStopped THEN          // T0000-0002 
	      Sequence.iNewStep := L_SM_STATE_STOPPED;
	    END_IF;
	
	  L_SM_STATE_STOPPING :       // S0001 
	
	    // Reset hold state 
	    HoldState := 0;
	
	    // Transitions 
	    IF    ioStateModel.Command.Abort THEN                          // T0001-2048 
	      Sequence.iNewStep := L_SM_STATE_ABORTING;
	    ELSIF ioStateModel.Command.Prepare THEN                        // T0001-0004 
	      Sequence.iNewStep := L_SM_STATE_PREPARING;
	    ELSIF ioStateModel.Transition.StoppingToStopped THEN           // T0001-0002 
	      Sequence.iNewStep := L_SM_STATE_STOPPED;
	    END_IF;
	
	  L_SM_STATE_STOPPED :        // S0002 
	
	    // Transitions 
	    IF    ioStateModel.Command.Abort THEN                          // T0002-2048 
	      Sequence.iNewStep := L_SM_STATE_ABORTING;
	    ELSIF ioStateModel.Command.Prepare THEN                        // T0002-0004 
	      Sequence.iNewStep := L_SM_STATE_PREPARING;
	    END_IF;
	
	  L_SM_STATE_PREPARING :      // S0004 
	
	    // Transitions 
	    IF    ioStateModel.Command.Abort THEN                          // T0004-2048 
	      Sequence.iNewStep := L_SM_STATE_ABORTING;
	    ELSIF ioStateModel.Command.Stop THEN                           // T0004-0001 
	      Sequence.iNewStep := L_SM_STATE_STOPPING;
	    ELSIF ioStateModel.Command.Hold THEN                           // T0004-0064 
	      HoldState         := L_SM_STATE_PREPARING; 
	      Sequence.iNewStep := L_SM_STATE_HOLDING;
	    ELSIF ioStateModel.Transition.PreparingToReady THEN            // T0004-0008 
	      Sequence.iNewStep := L_SM_STATE_READY;
	    END_IF;
	
	  L_SM_STATE_READY :          // S0008 
	
	    // Transitions 
	    IF    ioStateModel.Command.Abort THEN                          // T0008-2048 
	      Sequence.iNewStep := L_SM_STATE_ABORTING;
	    ELSIF ioStateModel.Command.Stop THEN                           // T0008-0001 
	      Sequence.iNewStep := L_SM_STATE_STOPPING;
	    ELSIF ioStateModel.Command.Hold THEN                           // T0008-0064 
	      HoldState         := L_SM_STATE_READY;  
	      Sequence.iNewStep := L_SM_STATE_HOLDING;
	    ELSIF ioStateModel.Command.Start THEN                          // T0008-0016 
	      Sequence.iNewStep := L_SM_STATE_STANDBY;
	    END_IF;
	
	  L_SM_STATE_STANDBY :        // S0016 
	
	    // Transitions 
	    IF    ioStateModel.Command.Abort THEN                          // T0016-2048 
	      Sequence.iNewStep := L_SM_STATE_ABORTING;
	    ELSIF ioStateModel.Command.Stop THEN                           // T0016-0001 
	      Sequence.iNewStep := L_SM_STATE_STOPPING;
	    ELSIF ioStateModel.Command.Hold THEN                           // T0016-0064 
	      HoldState         := L_SM_STATE_STANDBY;  
	      Sequence.iNewStep := L_SM_STATE_HOLDING;
	    ELSIF ioStateModel.Transition.StandbyToRunning THEN            // T0016-0032 
	      Sequence.iNewStep := L_SM_STATE_RUNNING;
	    END_IF;
	
	  L_SM_STATE_RUNNING :        // S0032 
	
	    // Transitions 
	    IF    ioStateModel.Command.Abort THEN                          // T0032-2048 
	      Sequence.iNewStep := L_SM_STATE_ABORTING;
	    ELSIF ioStateModel.Command.Stop THEN                           // T0032-0001 
	      Sequence.iNewStep := L_SM_STATE_STOPPING;
	    ELSIF ioStateModel.Command.Hold THEN                           // T0032-0064 
	      HoldState         := L_SM_STATE_RUNNING;  
	      Sequence.iNewStep := L_SM_STATE_HOLDING;
	    ELSIF ioStateModel.Command.Complete                            // T0032-0512 
	       OR ioStateModel.Transition.RunningToCompleting THEN         // T0032-0512 
	      Sequence.iNewStep := L_SM_STATE_COMPLETING;
	    ELSIF ioStateModel.Transition.RunningToStandby THEN            // T0032-0016 
	      Sequence.iNewStep := L_SM_STATE_STANDBY;
	    END_IF;
	
	  L_SM_STATE_HOLDING :        // S0064 
	
	    // Transitions 
	    IF    ioStateModel.Command.Abort THEN                          // T0064-2048 
	      Sequence.iNewStep := L_SM_STATE_ABORTING;
	    ELSIF ioStateModel.Command.Stop THEN                           // T0064-0001 
	      Sequence.iNewStep := L_SM_STATE_STOPPING;
	    ELSIF ioStateModel.Command.Restart THEN                        // T0064-0256 
	      Sequence.iNewStep := L_SM_STATE_RESTARTING;
	    ELSIF ioStateModel.Transition.HoldingToHeld THEN               // T0064-0128 
	      Sequence.iNewStep := L_SM_STATE_HELD;
	    END_IF;
	
	  L_SM_STATE_HELD :           // S0128 
	
	    // Transitions 
	    IF    ioStateModel.Command.Abort THEN                          // T0128-2048 
	      Sequence.iNewStep := L_SM_STATE_ABORTING;
	    ELSIF ioStateModel.Command.Stop THEN                           // T0128-0001 
	      Sequence.iNewStep := L_SM_STATE_STOPPING;
	    ELSIF ioStateModel.Command.Restart THEN                        // T0128-0256 
	      Sequence.iNewStep := L_SM_STATE_RESTARTING;
	    END_IF;
	
	  L_SM_STATE_RESTARTING :     // S0256 
	
	    // Transitions 
	    IF    ioStateModel.Command.Abort THEN                          // T0256-2048 
	      Sequence.iNewStep := L_SM_STATE_ABORTING;
	    ELSIF ioStateModel.Command.Stop THEN                           // T0256-0001 
	      Sequence.iNewStep := L_SM_STATE_STOPPING;
	    ELSIF ioStateModel.Command.Hold THEN                           // T0256-0064 
	      Sequence.iNewStep := L_SM_STATE_HOLDING;
	    ELSIF ioStateModel.Transition.RestartingToPrevious THEN        // T0256-0004/0008/0016/0032/0512/1024 
	      Sequence.iNewStep := HoldState;
	    END_IF;
	
	  L_SM_STATE_COMPLETING :     // S0512 
	
	    // Transitions 
	    IF    ioStateModel.Command.Abort THEN                          // T0512-2048 
	      Sequence.iNewStep := L_SM_STATE_ABORTING;
	    ELSIF ioStateModel.Command.Stop THEN                           // T0512-0001 
	      Sequence.iNewStep := L_SM_STATE_STOPPING;
	    ELSIF ioStateModel.Command.Hold THEN                           // T0512-0064 
	      HoldState         := L_SM_STATE_COMPLETING;
	      Sequence.iNewStep := L_SM_STATE_HOLDING;
	    ELSIF ioStateModel.Transition.CompletingToCompleted THEN       // T0512-1024 
	      Sequence.iNewStep := L_SM_STATE_COMPLETED;
	    END_IF;
	
	  L_SM_STATE_COMPLETED :      // S1024 
	
	    // Transitions 
	    IF    ioStateModel.Command.Abort THEN                          // T1024-2048 
	      Sequence.iNewStep := L_SM_STATE_ABORTING;
	    ELSIF ioStateModel.Command.Stop THEN                           // T1024-0001 
	      Sequence.iNewStep := L_SM_STATE_STOPPING;
	    ELSIF ioStateModel.Command.Hold THEN                           // T1024-0064 
	      HoldState    := L_SM_STATE_COMPLETED;  
	      Sequence.iNewStep := L_SM_STATE_HOLDING;
	    END_IF;
	
	  L_SM_STATE_ABORTING :       // S2048 
	
	    // Reset hold state 
	    HoldState := 0;
	
	    // Transitions 
	    IF ioStateModel.Transition.AbortingToAborted THEN              // T2048-4096 
	      Sequence.iNewStep := L_SM_STATE_ABORTED;
	    END_IF;
	
	  L_SM_STATE_ABORTED :        // S4096 
	
	    // Transitions 
	    IF ioStateModel.Command.Reset THEN                             // T4096-8192 
	      Sequence.iNewStep := L_SM_STATE_CLEARING;
	    END_IF;
	
	  L_SM_STATE_CLEARING :       // S8192 
	
	    // Transitions 
	    IF    ioStateModel.Command.Abort THEN                          // T8192-2048 
	      Sequence.iNewStep := L_SM_STATE_ABORTING;
	    ELSIF ioStateModel.Transition.ClearingToStopped THEN           // T8192-0002 
	      Sequence.iNewStep := L_SM_STATE_STOPPED;
	    END_IF;
	
	ELSE
	  Sequence.iNewStep := L_SM_STATE_STOPPED;
	END_CASE;

	//----------------------------------------------------------------------------------------------------------------------
	// Sequence.                                                                                                    
	//----------------------------------------------------------------------------------------------------------------------
  Sequence(
    iInitialization := iInitialization 
    );

	//----------------------------------------------------------------------------------------------------------------------
	// Update variables.                                                                                                    
	//----------------------------------------------------------------------------------------------------------------------
	ioStateModel.Status.State            := Sequence.oStep;
  ioStateModel.Status.StateElapsedTime := TIME_TO_UDINT(Sequence.oStepElapsed);

  ioStateModel.Command.Prepare   := FALSE;
	ioStateModel.Command.Start     := FALSE;
	ioStateModel.Command.Stop      := FALSE;
	ioStateModel.Command.Abort     := FALSE;
	ioStateModel.Command.Hold      := FALSE;
	ioStateModel.Command.Restart   := FALSE;
	ioStateModel.Command.Reset     := FALSE;
	ioStateModel.Command.Complete  := FALSE;
	
	ioStateModel.Transition.NotAvailableToStopped  := FALSE;
	ioStateModel.Transition.StoppingToStopped      := FALSE;
	ioStateModel.Transition.PreparingToReady       := FALSE;
	ioStateModel.Transition.StandbyToRunning       := FALSE;
	ioStateModel.Transition.RunningToCompleting    := FALSE;
	ioStateModel.Transition.RunningToStandby       := FALSE;
	ioStateModel.Transition.HoldingToHeld          := FALSE;
	ioStateModel.Transition.RestartingToPrevious   := FALSE;
	ioStateModel.Transition.CompletingToCompleted  := FALSE;
	ioStateModel.Transition.AbortingToAborted      := FALSE;
	ioStateModel.Transition.ClearingToStopped      := FALSE;

END_FUNCTION_BLOCK
