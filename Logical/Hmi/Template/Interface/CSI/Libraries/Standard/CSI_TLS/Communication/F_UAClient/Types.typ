
TYPE
	UA_Config_type : 	STRUCT 
		ReceiveNodeList : STRING[255];
		SendNodeList : {REDUND_UNREPLICABLE} STRING[255];
		WritePunkt : STRING[255];
		ReadPunkt : STRING[255];
		ServerEndpointUrl : STRING[255];
		SessionConnectInfo_0 : UASessionConnectInfo;
		CycleTime : TIME;
	END_STRUCT;
	UA_CLIENT_Out_Type : 	STRUCT 
		Status : UINT;
		ErrorID : DWORD;
	END_STRUCT;
	ClientConnectionStatus_TYP : 	STRUCT 
		eConnectionStatus : UAConnectionStatus;
		eServerState : UAServerState;
		eServerLevel : BYTE;
	END_STRUCT;
END_TYPE
