
FUNCTION_BLOCK FB_Sequencer
	VAR_INPUT
		iInitialization : BOOL;
		iStepWatchDog : TIME;
		iDisableWatchDog : BOOL;
		iNewStep : UDINT;
	END_VAR
	VAR_OUTPUT
		oStep : UDINT;
		oPrevStep : UDINT;
		oPulseStep : BOOL;
		oStepLogger : ARRAY[0..19] OF UDINT;
		oHistory : ARRAY[0..19] OF STRING[22];
		oStepElapsed : TIME;
	END_VAR
	VAR
		fTON_Step : TON;
		PrevStepElapsed : TIME;
		StepStart : TIME;
		LastScanStep : UDINT;
		Temp : STRING[8];
		TS_StepStart : TIMEStructure;
		WatchDogError : BOOL;
	END_VAR
END_FUNCTION_BLOCK
