**************************************************************************************************************************
L_HS - Version History
**************************************************************************************************************************

V1.08.0  17-sep-2018 Feike van der Sterren

  Function block 'FB_HS_Src'          V1.03.0 *
  Function block 'FB_HS_Dst'          V1.04.0 * 
  Function block 'FB_HS_FIFO'         V1.03.0  

  Description:
  -  Bug fix, enable low -> reset HS to initial state
     
V1.07.0  29-sep-2016 Bart van Leuken

  Function block 'FB_HS_Src'          V1.02.0 
  Function block 'FB_HS_Dst'          V1.03.0 * 
  Function block 'FB_HS_FIFO'         V1.03.0  

  Description:
  - Added reset of :
     - ioCmd.AutoControl.Trigger
     - ioCmd.AutoControl.DataIsNotOK
     - ioCmd.AutoControl.DataIsOK

V1.06.0  15-aug-2016 Geert Schreurs

  Function block 'FB_HS_Src'          V1.02.0 
  Function block 'FB_HS_Dst'          V1.02.0 
  Function block 'FB_HS_FIFO'         V1.03.0 *  

  Description:
  - Added output oEmpty to FB_HS_FIFO.

V1.05.0  14-jul-2016 Geert Schreurs

  Function block 'FB_HS_Src'          V1.02.0 
  Function block 'FB_HS_Dst'          V1.02.0 
  Function block 'FB_HS_FIFO'         V1.02.0 *  

  Description:
  - Added output oFull to FB_HS_FIFO. This output will be set when only one
      remaining fifo-record is empty, so even though full is reported, still one record could be added!
  
V1.04.0  19-nov-2015 Bart van Leuken

  Function block 'FB_HS_Src'          V1.02.0 *
  Function block 'FB_HS_Dst'          V1.02.0 *
  Function block 'FB_HS_FIFO'         V1.01.0 *  

  Description:
  - If not iEnable then reset handshake output
  - Removed Settings, Moved iSettings.DownloadEnabled to ioCmd.AutoControl.DownloadEnable

V1.03.0  11-nov-2015 Bart van Leuken

  Function block 'FB_HS_Src'          V1.01.0
  Function block 'FB_HS_Dst'          V1.01.0  

  Description:
  - Removed 'FB_Handshake' as it has to be replaced by either 'FB_HS_Src' or 'FB_HS_Dst'

V1.02.0  09-sep-2014 Bart van Leuken

  Function block 'FB_HS_Src'          V1.01.0
  Function block 'FB_HS_Dst'          V1.01.0 
  Function block 'FB_Handshake'       V1.01.0 

  Description:
  - Added 'FB_HS_Src' and 'FB_HS_Src'

V1.01.0  09-sep-2014 Bart van Leuken

  Function block 'FB_Handshake'       V1.01.0

  Description:
  - Updated comments and version history.

V1.00.0  17-apr-2013 Bart van Leuken

  Function block 'FB_Handshake'       V1.00.0

  Description:
  - First release, copied from 'CFS_HS' (V2.00).




