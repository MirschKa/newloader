
TYPE
	T_ProgCheck_Cmd : 	STRUCT 
		Status : T_ProgCheck_Cmd_Sts;
		AutoControl : T_ProgCheck_Cmd_AutCtrl;
		Alarms : T_ProgCheck_Cmd_Alarms;
	END_STRUCT;
	T_ProgCheck_Cmd_Sts : 	STRUCT 
		Installed : BOOL;
		Ready : BOOL;
		FileExists : BOOL;
		AlarmStatus : USINT;
	END_STRUCT;
	T_ProgCheck_Cmd_AutCtrl : 	STRUCT 
		Check : BOOL;
		ProgramNumber : UDINT;
	END_STRUCT;
	T_ProgCheck_Cmd_Alarms : 	STRUCT 
		ResetAllAlarms : BOOL;
		ActiveAlarm : ARRAY[0..7]OF BOOL;
		SetAlarm : ARRAY[0..7]OF BOOL;
		ResetAlarm : ARRAY[0..7]OF BOOL;
	END_STRUCT;
	T_ProgCheck_VC : 	STRUCT 
		Alarms : T_ProgCheck_VC_Alarms;
	END_STRUCT;
	T_ProgCheck_VC_Alarms : 	STRUCT 
		AlarmImage : ARRAY[0..7]OF BOOL;
		AcknowledgeImage : ARRAY[0..7]OF BOOL;
	END_STRUCT;
	T_ProgCheck_Config : 	STRUCT 
		FolderName : STRING[32];
	END_STRUCT;
END_TYPE
