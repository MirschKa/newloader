
TYPE
	T_HS : 	STRUCT 
		Handshake : UINT;
		Feedback : UINT;
		Parameter : UDINT;
	END_STRUCT;
	T_HS_Bits : 	STRUCT 
		MailboxLocked : BOOL; (*Bit 0*)
		DataIsNotOK : BOOL; (*Bit 1*)
		DataIsOK : BOOL; (*Bit 2*)
		DataTransferCompleted : BOOL; (*Bit 3*)
		RequestData : BOOL; (*Bit 4*)
		RequestReadMailbox : BOOL; (*Bit 5*)
		RequestLockMailbox : BOOL; (*Bit 6*)
		ReadMailboxCompleted : BOOL; (*Bit 7*)
		DownloadEnabled : BOOL; (*Bit 8*)
		Reserved_09 : BOOL; (*Bit 9*)
		SimplifiedDataRequest : BOOL; (*Bit 10*)
		SimplifiedDataConfirm : BOOL; (*Bit 11*)
		SimplifiedRequestSend : BOOL; (*Bit 12*)
		Reserved_13 : BOOL; (*Bit 13*)
		Reserved_14 : BOOL; (*Bit 14*)
		Reserved_15 : BOOL; (*Bit 15*)
	END_STRUCT;
	T_HS_Src_Cmd : 	STRUCT 
		Status : T_HS_Src_Cmd_Sts;
		AutoControl : T_HS_Src_Cmd_AutCtrl;
	END_STRUCT;
	T_HS_Src_Cmd_Sts : 	STRUCT 
		Running : BOOL;
		TimeOut : BOOL;
		State : T_HS_Src_State;
		HandshakeOK : BOOL;
		HandshakeNotOK : BOOL;
		DataTransferActive : BOOL;
		DataIsNotOK : BOOL;
	END_STRUCT;
	T_HS_Src_Cmd_AutCtrl : 	STRUCT 
		Trigger : BOOL;
		DataTransferCompleted : BOOL;
		DownloadEnable : BOOL;
	END_STRUCT;
	T_HS_Src_Config : 	STRUCT 
		FastHandshake : BOOL;
		TimeOut : TIME;
	END_STRUCT;
	T_HS_Dst_Cmd : 	STRUCT 
		Status : T_HS_Dst_Cmd_Sts;
		AutoControl : T_HS_Dst_Cmd_AutCtrl;
	END_STRUCT;
	T_HS_Dst_Cmd_Sts : 	STRUCT 
		Running : BOOL;
		TimeOut : BOOL;
		State : T_HS_Dst_State;
		HandshakeOK : BOOL;
		HandshakeNotOK : BOOL;
		DataTransferCompleted : BOOL;
	END_STRUCT;
	T_HS_Dst_Cmd_AutCtrl : 	STRUCT 
		Trigger : BOOL;
		DataIsNotOK : BOOL;
		DataIsOK : BOOL;
		DownloadEnable : BOOL;
	END_STRUCT;
	T_HS_Dst_Config : 	STRUCT 
		FastHandshake : BOOL;
		TimeOut : TIME;
		DataCheck : BOOL; (*Not for FastHandshake*)
	END_STRUCT;
	T_HS_Src_State : 
		(
		L_HS_STATE_SRC_STOPPED := 0,
		L_HS_STATE_SRC_WAIT_BITSET_00 := 1,
		L_HS_STATE_SRC_TRANSFER_ACTIVE := 2,
		L_HS_STATE_SRC_WAIT_BITSET_01_02 := 3,
		L_HS_STATE_SRC_WAIT_BITCLR_01_02 := 4,
		L_HS_STATE_SRC_WAIT_BITSET_10 := 10,
		L_HS_STATE_SRC_WAIT_DATA_COPIED := 11,
		L_HS_STATE_SRC_WAIT_BITCLR_10 := 12,
		L_HS_STATE_SRC_ERROR := 20
		);
	T_HS_Dst_State : 
		(
		L_HS_STATE_DST_STOPPED := 0,
		L_HS_STATE_DST_WAIT_BITSET_06 := 1,
		L_HS_STATE_DST_WAIT_BITSET_03 := 2,
		L_HS_STATE_DST_CHECK_DATA := 3,
		L_HS_STATE_DST_WAIT_BITSET_02 := 4,
		L_HS_STATE_DST_WAIT_BITCLR_02 := 5,
		L_HS_STATE_DST_WAIT_BITSET_11 := 10,
		L_HS_STATE_DST_WAIT_BITCLR_11 := 11,
		L_HS_STATE_DST_ERROR := 20
		);
END_TYPE
