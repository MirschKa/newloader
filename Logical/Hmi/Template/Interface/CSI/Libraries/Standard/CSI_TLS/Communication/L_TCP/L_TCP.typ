
TYPE
	T_TCPIP_Cmd : 	STRUCT 
		Status : T_TCPIP_Cmd_Sts;
		AutoControl : T_TCPIP_Cmd_AutCtrl;
	END_STRUCT;
	T_TCPIP_Cmd_Sts : 	STRUCT 
		Installed : BOOL;
		Online : BOOL;
		Alarm : BOOL;
		Running : BOOL;
		Server_IP_Address : STRING[15];
		Client_IP_Address : STRING[15];
		ServerPort : UINT;
		ClientPort : UINT;
		State : T_TCPIP_State;
		StateElapsedTime : UDINT;
	END_STRUCT;
	T_TCPIP_Cmd_AutCtrl : 	STRUCT 
		Start : BOOL;
		Stop : BOOL;
	END_STRUCT;
	T_TCPIP_Config : 	STRUCT 
		Installed : BOOL;
		Communication : T_TCPIP_Config_Comm;
		Data : T_TCPIP_Config_Data;
		Port : T_TCPIP_Config_Port;
	END_STRUCT;
	T_TCPIP_Config_Comm : 	STRUCT 
		Mode : USINT; (*[0] Client or [1] Server*)
		TimeOut : TIME;
	END_STRUCT;
	T_TCPIP_Config_Data : 	STRUCT 
		SendStructure : UDINT;
		SizeOfSendStructure : UDINT;
		ReceiveStructure : UDINT;
		SizeOfReceiveStructure : UDINT;
	END_STRUCT;
	T_TCPIP_Config_Port : 	STRUCT 
		Mode : USINT; (*[0] Automatic or [1] Manual*)
		Automatic : T_TCPIP_Config_Port_Auto;
		Manual : T_TCPIP_Config_Port_Man;
	END_STRUCT;
	T_TCPIP_Config_Port_Auto : 	STRUCT 
		Channel : UINT; (*[0..9] Automatic port control: channel*)
	END_STRUCT;
	T_TCPIP_Config_Port_Man : 	STRUCT 
		Server : UINT; (*Manual port control: Server port number*)
		Client : UINT; (*Manual port control: Client port number*)
	END_STRUCT;
	T_TCPIP_Settings : 	STRUCT 
		Communication : T_TCPIP_Settings_Comm;
		IP_Address : T_TCPIP_Settings_IP;
	END_STRUCT;
	T_TCPIP_Settings_Comm : 	STRUCT 
		Enable : BOOL;
	END_STRUCT;
	T_TCPIP_Settings_IP : 	STRUCT 
		Server : STRING[15]; (*Server IP address*)
		Client : STRING[15]; (*Client IP address*)
	END_STRUCT;
	T_TCPIP_Comm_Sts : 	STRUCT 
		Online : BOOL;
		StructureSizeNotOK : BOOL;
		ReceiveOK : BOOL;
		Fragmented : BOOL;
		HMI_Color : UINT;
	END_STRUCT;
	T_TCPIP_State : 
		(
		L_TCP_STATE_CLIENT := 5,
		L_TCP_STATE_CLOSE_CONNECTION := 3,
		L_TCP_STATE_SERVER_CLOSE := 7,
		L_TCP_STATE_SENDING_RECEIVING := 8,
		L_TCP_STATE_GET_SOCKET_LIST := 1,
		L_TCP_STATE_GET_SOCKET := 2,
		L_TCP_STATE_LINGER_LOCAL_IDENT := 10,
		L_TCP_STATE_LINGER_SERVER_IDENT := 12,
		L_TCP_STATE_NEW_CONNECTION := 4,
		L_TCP_STATE_NO_REQUEST := 0,
		L_TCP_STATE_SERVER_START := 6,
		L_TCP_STATE_STOP := 9,
		L_TCP_STATE_CLOSE_LOCAL_IDENT := 11,
		L_TCP_STATE_CLOSE_SERVER_IDENT := 13
		);
END_TYPE
