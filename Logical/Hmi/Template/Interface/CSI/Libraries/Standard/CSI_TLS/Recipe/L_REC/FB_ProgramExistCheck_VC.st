
//************************************************************************************************************************
// Company:     GEA Food Solutions Bakel
//------------------------------------------------------------------------------------------------------------------------
// Name:        FB_ProgramExistCheck_VC 
// Author:      Bart van Leuken
// Version:     V1.00.0
// Date:        05-jul-2016
//------------------------------------------------------------------------------------------------------------------------
// Restriction: 
// 
//------------------------------------------------------------------------------------------------------------------------
// Description: Function block to check if a program number exists Visual Components interface.
//************************************************************************************************************************

FUNCTION_BLOCK FB_ProgramExistCheck_VC

  //----------------------------------------------------------------------------------------------------------------------
  // Initialization.
  //----------------------------------------------------------------------------------------------------------------------
  IF iInitialization
  THEN
  ;
  END_IF;
  //----------------------------------------------------------------------------------------------------------------------
  // Enable
  //----------------------------------------------------------------------------------------------------------------------
  IF    NOT ioCmd.Status.Installed
     OR NOT iEnable
  THEN   
    FOR i := 0 TO (SIZEOF(ioVC.Alarms.AlarmImage) / SIZEOF(ioVC.Alarms.AlarmImage[0])) - 1 DO
      ioVC.Alarms.AlarmImage[i]       := FALSE;
      ioVC.Alarms.AcknowledgeImage[i] := FALSE;
    END_FOR;
    
    RETURN;
  END_IF;  

  //----------------------------------------------------------------------------------------------------------------------
  // Alarms
  //----------------------------------------------------------------------------------------------------------------------
  TON_AlarmHandler(
    IN := NOT TON_AlarmHandler.Q,
    PT := T#1s);
  
  IF TON_AlarmHandler.Q
  THEN
    FOR i := 0 TO (SIZEOF(ioVC.Alarms.AlarmImage) / SIZEOF(ioVC.Alarms.AlarmImage[0])) - 1 DO
      IF     NOT ioVC.Alarms.AcknowledgeImage[i]
         AND ioVC.Alarms.AlarmImage[i]
      THEN
        ioCmd.Alarms.ResetAlarm[i] := TRUE;
      END_IF;
      ioVC.Alarms.AlarmImage[i] := ioCmd.Alarms.ActiveAlarm[i];
    END_FOR;
  END_IF;

END_FUNCTION_BLOCK
