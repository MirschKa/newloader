
//************************************************************************************************************************
// Company:     GEA Food Solutions Bakel
//------------------------------------------------------------------------------------------------------------------------
// Name:        FB_FilterRecipeOperations 
// Author:      Feike van der Sterren
// Version:     V1.00.0
// Date:        28-Oct-2016
//------------------------------------------------------------------------------------------------------------------------
// Restriction: 
// 
//------------------------------------------------------------------------------------------------------------------------
// Description: Function block to filter recipe operations by Workplace ID.
//************************************************************************************************************************

FUNCTION_BLOCK FB_FilterRecipeOperations

  //----------------------------------------------------------------------------------------------------------------------
  // Initialization.
  //----------------------------------------------------------------------------------------------------------------------
  IF iInitialization
  THEN
  ;
  END_IF;

  //----------------------------------------------------------------------------------------------------------------------
  // Enable
  //----------------------------------------------------------------------------------------------------------------------
  IF NOT iEnable
  THEN     
    RETURN;
  END_IF;  

  //----------------------------------------------------------------------------------------------------------------------
  // Filter
  //----------------------------------------------------------------------------------------------------------------------
  j := 0;
  brsmemset( ADR(oRecipeOperations), 0, SIZEOF(oRecipeOperations));
  brsmemset( ADR(oIndexOriginalRecipe), 0, SIZEOF(oIndexOriginalRecipe));

  FOR i := 0 TO ( SIZEOF( iRecipeOperations) / SIZEOF( iRecipeOperations[0]) - 1) DO
    Temp_STRING := RIGHT( iRecipeOperations[i].Workplace, 4);
    WorkPlaceID := STRING_TO_UINT( Temp_STRING);
    IF        ( WorkPlaceID <> 0 )
      AND (   ( WorkPlaceID =  iConfigWorkPlaceID[0]) 
           OR ( WorkPlaceID =  iConfigWorkPlaceID[1]) 
           OR ( WorkPlaceID =  iConfigWorkPlaceID[2]) 
           OR ( WorkPlaceID =  iConfigWorkPlaceID[3])) 
    THEN
     oIndexOriginalRecipe[j]  := i; 
     oRecipeOperations[j]     := iRecipeOperations[i];        
      j := j + 1;
    END_IF;
  END_FOR;

END_FUNCTION_BLOCK
