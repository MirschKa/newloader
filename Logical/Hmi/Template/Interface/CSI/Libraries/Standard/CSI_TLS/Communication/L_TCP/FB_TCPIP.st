//************************************************************************************************************************
// Company:     GEA Food Solutions
//------------------------------------------------------------------------------------------------------------------------
// Name:        FB_TCPIP
// Author:      Corne Geerts (B&R) / Bart van Leuken 
// Version:     V1.03.0
// Date:        08-sep-2014
//------------------------------------------------------------------------------------------------------------------------
// Restriction: First UDINT of structure contains the actual structure size to compare with structure size on other side. 
//
//------------------------------------------------------------------------------------------------------------------------
// Description: Function block to handle TCP/IP communication (bi-directional)
//************************************************************************************************************************

FUNCTION_BLOCK FB_TCPIP
	
	//----------------------------------------------------------------------------------------------------------------------
  // Initialization.                                                                                             
	//----------------------------------------------------------------------------------------------------------------------
  IF (iConfig.Installed <> ioCmd.Status.Installed)
  THEN
    ioCmd.Status.Installed          := iConfig.Installed;

    RTInfo_0( enable := TRUE ); 
    TimeBetweenBlockCalls           := RTInfo_0.cycle_time / 1000; 
	  ioCmd.Status.Installed          := TRUE;
	  ioCmd.Status.State              := L_TCP_STATE_NO_REQUEST;
	  ioCmd.Status.Online             := FALSE;
	  ioCmd.AutoControl.Start         := FALSE;
	  ServerIP_AddressOld             := iSettings.IP_Address.Server;
	  ClientIP_AddressOld             := iSettings.IP_Address.Client;
	  PortModeOld                     := iConfig.Port.Mode;
	  Restart                         := FALSE;
	  RestartTimeOut                  := T#30s;
	  SendTimeOut                     := T#2s;
	  iConfig.Communication.TimeOut := T#10s; // Default value
	  TimeOutAlarm                    := FALSE;
	  tcpLINGER.lOnOff                := 1; // Close as fast as possible 
	  tcpLINGER.lLinger               := 0; // 0 second 
	  AddressCalculation              := TRUE; // Generate IP Addresses and port numbers 
	  InternalRecBufLenCnt            := 0;
	  brsmemset(ADR(RecvBuf), 0, SIZEOF(RecvBuf)); // Reset receive buffer 
    ioCmd.Status.StateElapsedTime   := 0;
	  IF (iConfig.Data.SendStructure <> 0) THEN
	    ActualSizeOfSendStructure ACCESS iConfig.Data.SendStructure;
	  END_IF;
	END_IF;

  IF NOT ioCmd.Status.Installed OR NOT iSettings.Communication.Enable
  THEN
    RETURN;
  END_IF;
	
	//----------------------------------------------------------------------------------------------------------------------
	// Time out and send time out.                                                                                          
	//----------------------------------------------------------------------------------------------------------------------
	// Recalculate SendTimeOut - Needed for usage in e.g. 1000ms task class, minimum set to 2 sec
	IF (TimeBetweenBlockCalls > 200) THEN
	  SendTimeOut := REAL_TO_TIME(10 * TimeBetweenBlockCalls);
	END_IF;
	
	IF (iConfig.Communication.TimeOut < (SendTimeOut + T#1s)) THEN
	  TimeOutAlarm := TRUE;
	ELSE
	  TimeOutAlarm := FALSE;
	END_IF;
	
	//----------------------------------------------------------------------------------------------------------------------
	// Copy Structure size in first UDINT of Send structure.                                                                
	//----------------------------------------------------------------------------------------------------------------------
	ActualSizeOfSendStructure := iConfig.Data.SizeOfSendStructure;
	
	//----------------------------------------------------------------------------------------------------------------------
	// Auto restart communication.                                                                                          
	//----------------------------------------------------------------------------------------------------------------------
	// Change of IP Address or port number
	IF   (iSettings.IP_Address.Server <> ServerIP_AddressOld) // ServerIP_Address has changed
	  OR (iSettings.IP_Address.Client <> ClientIP_AddressOld) // ClientIP_Address has changed
	  OR (iConfig.Port.Mode <> PortModeOld) // ManualPortNumberControl has changed 
	THEN
	  Restart := TRUE;
	  AddressCalculation := TRUE; // (Re)generate IP Addresses and port numbers 
	END_IF;
	
	//----------------------------------------------------------------------------------------------------------------------
	// Address calculation.                                                                                                 
	//----------------------------------------------------------------------------------------------------------------------
	IF AddressCalculation THEN
	  // Manual port number generation 
	  IF (iConfig.Port.Mode = L_TCP_PORTCTRL_MANUAL) THEN
	    // Port number definition 
	    ServerPort := iConfig.Port.Manual.Server;
	    ClientPort := iConfig.Port.Manual.Client;
	  ELSE
	    // Automatic port number generation 
	    ClientPortOffset := 10 * FC_PORTGEN(iSettings.IP_Address.Client);
	    // Channel limits 
	    IF iConfig.Port.Automatic.Channel > 9 THEN
	      iConfig.Port.Automatic.Channel := 0;
	    END_IF;
	    // Port number definition 
	    ServerPort := L_TCP_SERVERPORT + ClientPortOffset + iConfig.Port.Automatic.Channel;
	    ClientPort := L_TCP_CLIENTPORT + ClientPortOffset + iConfig.Port.Automatic.Channel;
	  END_IF;
	  IF (iConfig.Communication.Mode = L_TCP_SERVER) THEN
	    Port := ServerPort;
	  ELSE
	    Port := ClientPort;
	  END_IF;
	  AddressCalculation := FALSE;
	END_IF;
	
	//----------------------------------------------------------------------------------------------------------------------
	// Time out.                                                                                                            
	//----------------------------------------------------------------------------------------------------------------------
	TON_TimeOut_Restart(
		IN := (ioCmd.Status.State > L_TCP_STATE_NO_REQUEST) AND (ioCmd.Status.State < L_TCP_STATE_SENDING_RECEIVING),
		PT := RestartTimeOut);
	
	IF TON_TimeOut_Restart.Q THEN
	  Restart := TRUE;
	END_IF;
	
	TON_TimeOut_Send(PT := SendTimeOut);
	TON_TimeOut(PT := iConfig.Communication.TimeOut);
	
	//----------------------------------------------------------------------------------------------------------------------
	// Start/stop handling (automatic control).                                                                             
	//----------------------------------------------------------------------------------------------------------------------
	IF ioCmd.AutoControl.Start AND ioCmd.AutoControl.Stop THEN
	  ioCmd.AutoControl.Start := FALSE;
	END_IF;
	
	IF Restart AND ioCmd.AutoControl.Stop THEN
	  Restart := FALSE;
	END_IF;
	
	//----------------------------------------------------------------------------------------------------------------------
	// Status information.                                                                                                  
	//----------------------------------------------------------------------------------------------------------------------
	ioCmd.Status.Online  := (ioCmd.Status.State = L_TCP_STATE_SENDING_RECEIVING);
	ioCmd.Status.Running := (ioCmd.Status.State <> L_TCP_STATE_NO_REQUEST);

	//----------------------------------------------------------------------------------------------------------------------
	//----------------------------------------------------------------------------------------------------------------------
	//----------------------------------------------------------------------------------------------------------------------
	//----------------------------------------------------------------------------------------------------------------------
	// Stepper program.                                                                                                     
	//----------------------------------------------------------------------------------------------------------------------
	//----------------------------------------------------------------------------------------------------------------------
	//----------------------------------------------------------------------------------------------------------------------
	CASE ioCmd.Status.State OF
	
	  L_TCP_STATE_NO_REQUEST:
	    // Stepper = 0 
	
	    IF ioCmd.AutoControl.Start OR Restart THEN
	      Restart := FALSE;
	      ioCmd.Status.State := L_TCP_STATE_GET_SOCKET_LIST;
	    END_IF;
	
	  L_TCP_STATE_GET_SOCKET_LIST:
	    // Stepper = 1 
	
	    // Get list of open sockets 
	    TcpIoctl_0(
	      enable  := TRUE,
	      ident   := tcpINVALID_IDENT,
	      ioctl   := tcpID_LIST_GET,
	      pData   := ADR(tcpID_LIST),
	      datalen :=SIZEOF(tcpID_LIST));
	
	    IF (TcpIoctl_0.status = 0) THEN
	      IF (TcpIoctl_0.outlen = 0) THEN // No connections open 
	        // Clear all parameters 
	        brsmemset(ADR(tcpID_LIST), 0, SIZEOF(tcpID_LIST));
	        maxID := 0;
	        ioCmd.Status.State := L_TCP_STATE_NEW_CONNECTION; // Open a new connection 
	      ELSE // Sockets found 
	        maxID                  := TcpIoctl_0.outlen;
	        ioCmd.Status.State := L_TCP_STATE_GET_SOCKET; // Check if this is the correct connection 
	      END_IF;
	      indID := 0;
	    ELSIF (TcpIoctl_0.status <> 65535) THEN
	      ioCmd.Status.State := L_TCP_STATE_NO_REQUEST; // Error try again 
	    END_IF;
	
	    IF ioCmd.AutoControl.Stop OR Restart THEN
	      brsmemset(ADR(RecvBuf), 0, SIZEOF(RecvBuf)); // Reset receive structure 
	      ioCmd.Status.State := L_TCP_STATE_STOP;
	    END_IF;
	
	  L_TCP_STATE_GET_SOCKET:
	    // Stepper = 2 
	
	    tcpSO_ADDRESS.pPort   := ADR(soPort);
	    tcpSO_ADDRESS.pIpAddr := ADR(soIPAddr);
	
	    TcpIoctl_0(
	      enable  := TRUE,
	      ident   := tcpID_LIST[indID],
	      ioctl   := tcpSO_ADDRESS_GET,
	      pData   := ADR(tcpSO_ADDRESS),
	      datalen := SIZEOF(tcpSO_ADDRESS));
	
	    IF (TcpIoctl_0.status = 0) THEN
	      IF (soPort = Port) THEN
	        ioCmd.Status.State := L_TCP_STATE_CLOSE_CONNECTION;
	        Local_Ident            := tcpID_LIST[indID];
	      ELSE
	        indID := indID + 1;
	        IF (indID > (maxID - 1)) THEN
	          // End of list with sockets 
	          ioCmd.Status.State := L_TCP_STATE_NEW_CONNECTION; // Open a new socket 
	        END_IF;
	      END_IF;
	    ELSIF (TcpIoctl_0.status = tcpERR_PARAMETER) THEN
	      ioCmd.Status.State := L_TCP_STATE_CLOSE_CONNECTION;
	      Local_Ident            := tcpID_LIST[indID];
	    ELSIF (TcpIoctl_0.status <> 65535) THEN
	      ioCmd.Status.State :=L_TCP_STATE_NO_REQUEST;
	    END_IF;
	
	    IF ioCmd.AutoControl.Stop OR Restart THEN
	      brsmemset(ADR(RecvBuf), 0, SIZEOF(RecvBuf)); // Reset receive structure 
	      ioCmd.Status.State := L_TCP_STATE_STOP;
	    END_IF;
	
	  L_TCP_STATE_CLOSE_CONNECTION:
	    // Stepper = 3 
	
	    TcpClose_0(
	      enable := TRUE,
	      ident  := Local_Ident,
	      how    := 0);
	
	    IF (TcpClose_0.status = 0) THEN
	      ioCmd.Status.State := L_TCP_STATE_NO_REQUEST;
	      Local_Ident            := 0;
	    ELSIF (TcpClose_0.status <> 65535) THEN
	      ioCmd.Status.State := L_TCP_STATE_NO_REQUEST;
	    END_IF;
	
	    IF ioCmd.AutoControl.Stop OR Restart THEN
	      brsmemset(ADR(RecvBuf), 0, SIZEOF(RecvBuf)); // Reset receive structure 
	      ioCmd.Status.State := L_TCP_STATE_STOP;
	    END_IF;
	
	  L_TCP_STATE_NEW_CONNECTION:
	    // Stepper = 4 
	
	    IF (iConfig.Communication.Mode = L_TCP_SERVER) THEN
	      TcpOpen_0(
	        enable  := TRUE,
	        pIfAddr := 0,
	        port    := ServerPort,
	        options := 0);
	    ELSE
	      TcpOpen_0(
	        enable  := TRUE,
	        pIfAddr := ADR(iSettings.IP_Address.Client),
	        port    := ClientPort,
	        options := 0);
	    END_IF;
	
	    IF (TcpOpen_0.status = 0) THEN
	      IF (iConfig.Communication.Mode = L_TCP_SERVER) THEN
	        ioCmd.Status.State := L_TCP_STATE_SERVER_START;
	        Server_Ident           := TcpOpen_0.ident;
	      ELSE
	        ioCmd.Status.State := L_TCP_STATE_CLIENT;
	        Local_Ident            := TcpOpen_0.ident;
	      END_IF;
	    ELSIF (TcpOpen_0.status <> 65535) AND (TcpOpen_0.status <> 65534) THEN
	      ioCmd.Status.State := L_TCP_STATE_STOP;
	    END_IF;
	
	    IF ioCmd.AutoControl.Stop OR Restart THEN
	      brsmemset(ADR(RecvBuf),0,SIZEOF(RecvBuf)); // Reset receive structure 
	      ioCmd.Status.State := L_TCP_STATE_STOP;
	    END_IF;
	
	  L_TCP_STATE_CLIENT:
	    // Stepper = 5 
	
	    TcpClient_0(
	      enable   := TRUE,
	      ident    := Local_Ident,
	      pServer  := ADR(iSettings.IP_Address.Server),
	      portserv :=ServerPort);
	
	    IF (TcpClient_0.status = 0) OR (TcpClient_0.status = tcpERR_ALREADY_CONNECTED) THEN
	      ioCmd.Status.State := L_TCP_STATE_SENDING_RECEIVING;
	    ELSIF (TcpClient_0.status <> 65535) THEN
	      ioCmd.Status.State := L_TCP_STATE_STOP;
	    END_IF;
	
	    IF ioCmd.AutoControl.Stop OR Restart THEN
	      brsmemset(ADR(RecvBuf),0,SIZEOF(RecvBuf)); // Reset receive structure 
	      ioCmd.Status.State := L_TCP_STATE_STOP;
	    END_IF;
	
	  L_TCP_STATE_SERVER_START:
	    // Stepper = 6 
	
	    TcpServer_0(
	      enable  := TRUE,
	      backlog := 1,
	      ident   := Server_Ident,
	      pIpAddr := ADR(RemoteIP_Address));
	
	    IF (TcpServer_0.status = 0) THEN
	      Local_Ident := TcpServer_0.identclnt;
	      ioCmd.Status.State := L_TCP_STATE_SERVER_CLOSE;
	    ELSIF (TcpServer_0.status <> 65535) THEN
	      ioCmd.Status.State := L_TCP_STATE_STOP;
	    END_IF;
	
	    IF ioCmd.AutoControl.Stop OR Restart THEN
	      brsmemset(ADR(RecvBuf), 0, SIZEOF(RecvBuf)); // Reset receive structure 
	      ioCmd.Status.State := L_TCP_STATE_STOP;
	    END_IF;
	
	  L_TCP_STATE_SERVER_CLOSE:
	    // Stepper = 7 
	
	    TcpClose_0(
	      enable := TRUE,
	      ident  := Server_Ident,
	      how    := 0);
	
	    IF (TcpClose_0.status = 0) THEN
	      ioCmd.Status.State := L_TCP_STATE_SENDING_RECEIVING;
	      InternalReceiveBufLen  := 0; // Start fresh 
	      Server_Ident           := 0;
	    ELSIF (TcpClose_0.status <> 65535) THEN
	      ioCmd.Status.State := L_TCP_STATE_STOP;
	    END_IF;
	
	    IF ioCmd.AutoControl.Stop OR Restart THEN
	      brsmemset(ADR(RecvBuf), 0, SIZEOF(RecvBuf)); // Reset receive structure 
	      ioCmd.Status.State := L_TCP_STATE_STOP;
	    END_IF;
	
	  L_TCP_STATE_SENDING_RECEIVING:
	    // Stepper = 8 
	
	    TON_TimeOut_Send.IN := TRUE;
	    oCommStatus.StructureSizeNotOK := FALSE;
	    oCommStatus.ReceiveOK          := FALSE;
	    oCommStatus.Fragmented         := FALSE;
	
	    TcpRecv_0(
	      enable  := TRUE,
	      ident   := Local_Ident,
	      pData   := ADR(RecvBuf) + InternalReceiveBufLen,
	      datamax := SIZEOF(RecvBuf) - InternalReceiveBufLen,
	      flags   := 0);
	
	    IF (TcpRecv_0.status = 0) AND (TcpRecv_0.recvlen > 0) THEN
	      InternalReceiveBufLen := InternalReceiveBufLen + TcpRecv_0.recvlen;
	      IF (InternalReceiveBufLen >= iConfig.Data.SizeOfReceiveStructure) THEN // Enough bytes received 
	        ReceiveStructureSize ACCESS ADR(RecvBuf);
	        IF (ReceiveStructureSize = iConfig.Data.SizeOfReceiveStructure) THEN
	          brsmemcpy(iConfig.Data.ReceiveStructure, ADR(RecvBuf), iConfig.Data.SizeOfReceiveStructure);
	          oCommStatus.ReceiveOK     := TRUE;
	          SendEnable     := TRUE;
	          TON_TimeOut.IN := FALSE;
	          InternalReceiveBufLen := InternalReceiveBufLen - iConfig.Data.SizeOfReceiveStructure;
	          IF (InternalReceiveBufLen >= iConfig.Data.SizeOfReceiveStructure) THEN
	            InternalRecBufLenCnt := InternalRecBufLenCnt + 1;
	          END_IF;
	          IF (InternalReceiveBufLen > 0) AND (InternalRecBufLenCnt < 5) THEN // Still bytes available in the receive buffer 
	            brsmemmove(ADR(RecvBuf), ADR(RecvBuf) + iConfig.Data.SizeOfReceiveStructure, SIZEOF(RecvBuf) - iConfig.Data.SizeOfReceiveStructure);// Move remaining data to beginning of receive buffer 
	          ELSE
	            InternalReceiveBufLen := 0; // Try to receive data from the beginning 
	            brsmemset(ADR(RecvBuf), 0, SIZEOF(RecvBuf));
	            InternalRecBufLenCnt  := 0;
	          END_IF;
	        ELSE
	          oCommStatus.StructureSizeNotOK   := TRUE;
	          TON_TimeOut.IN	      := TRUE;
	          InternalReceiveBufLen := 0;
	          brsmemset(ADR(RecvBuf),0,SIZEOF(RecvBuf));
	        END_IF;
	      ELSIF (InternalReceiveBufLen > 0) AND (InternalReceiveBufLen < iConfig.Data.SizeOfReceiveStructure) THEN
	        oCommStatus.Fragmented := TRUE; // Wait until enough bytes are received 
	      END_IF;
	    ELSIF (TcpRecv_0.status = tcpERR_NO_DATA) THEN
	      IF (InternalReceiveBufLen >= iConfig.Data.SizeOfReceiveStructure) THEN // Clean up additional data, already or still available 
	        ReceiveStructureSize ACCESS ADR(RecvBuf);
	        IF (ReceiveStructureSize = iConfig.Data.SizeOfReceiveStructure) THEN
	          brsmemcpy(iConfig.Data.ReceiveStructure, ADR(RecvBuf), iConfig.Data.SizeOfReceiveStructure);
	          oCommStatus.ReceiveOK     := TRUE;
	          SendEnable     := TRUE;
	          TON_TimeOut.IN := FALSE;
	          InternalReceiveBufLen := InternalReceiveBufLen - iConfig.Data.SizeOfReceiveStructure;
	          IF (InternalReceiveBufLen >= iConfig.Data.SizeOfReceiveStructure) THEN
	            InternalRecBufLenCnt := InternalRecBufLenCnt + 1;
	          END_IF;
	          IF (InternalReceiveBufLen>0)AND (InternalRecBufLenCnt < 5) THEN // Still bytes available in the receive buffer 
	            brsmemmove(ADR(RecvBuf), ADR(RecvBuf) + iConfig.Data.SizeOfReceiveStructure, SIZEOF(RecvBuf) - iConfig.Data.SizeOfReceiveStructure);// Move remaining data to beginning of receive buffer 
	          ELSE
	            InternalReceiveBufLen := 0;
	            brsmemset(ADR(RecvBuf), 0, SIZEOF(RecvBuf));
	            InternalRecBufLenCnt  := 0;
	          END_IF;
	        ELSE
	          oCommStatus.StructureSizeNotOK   := TRUE;
	          TON_TimeOut.IN	      := TRUE;
	          InternalReceiveBufLen := 0;
	          brsmemset(ADR(RecvBuf), 0, SIZEOF(RecvBuf));
	        END_IF;
	      ELSIF (InternalReceiveBufLen > 0) AND (InternalReceiveBufLen < iConfig.Data.SizeOfReceiveStructure) THEN
	        oCommStatus.Fragmented := TRUE; // Wait until enough bytes are received 
	      END_IF;
	      TON_TimeOut.IN := TRUE;
	    ELSIF (TcpRecv_0.status <> 65535) THEN
	      InternalReceiveBufLen    := 0;
	      SendEnable               := FALSE;
	      TON_TimeOut_Send.IN      := FALSE;
	      TON_TimeOut.IN           := FALSE;
	      ioCmd.Status.State := L_TCP_STATE_STOP;
	      brsmemset(ADR(RecvBuf),0,SIZEOF(RecvBuf));
	    END_IF;
	
	    IF TON_TimeOut.Q THEN // Seperateley from TcpReceive call 
	      InternalReceiveBufLen    := 0;
	      SendEnable               := FALSE;
	      TON_TimeOut_Send.IN      := FALSE;
	      TON_TimeOut.IN           := FALSE;
	      ioCmd.Status.State := L_TCP_STATE_STOP;
	      brsmemset(ADR(RecvBuf),0,SIZEOF(RecvBuf));
	    END_IF;
	
	    IF (ioCmd.Status.State = L_TCP_STATE_SENDING_RECEIVING) THEN // Not already in error state 
	      IF TON_TimeOut_Send.Q THEN // Send is not restarted, try again 
	        SendEnable          := TRUE;
	        TON_TimeOut_Send.IN := FALSE;
	      END_IF;
	
	      IF SendEnable THEN
	        TcpSend_0(
	          enable  := TRUE,
	          ident   := Local_Ident,
	          pData   := iConfig.Data.SendStructure,
	          datalen := iConfig.Data.SizeOfSendStructure,
	          flags   := 0);
	
	        IF (TcpSend_0.status = 0) THEN
	          IF (TcpSend_0.sentlen = TcpSend_0.datalen) THEN
	            SendEnable           := FALSE; // Try to send only after a receive 
	            TON_TimeOut_Send.IN  := FALSE;
	          END_IF;
	        ELSIF(TcpSend_0.status = tcpERR_WOULDBLOCK) OR (TcpSend_0.status = tcpERR_SENTLEN) THEN
	          TON_TimeOut.IN := TRUE;
	        ELSIF (TcpSend_0.status <> 65535) THEN
	          InternalReceiveBufLen    := 0;
	          SendEnable               := FALSE;
	          TON_TimeOut_Send.IN      := FALSE;
	          TON_TimeOut.IN	         := FALSE;
	          ioCmd.Status.State := L_TCP_STATE_STOP;
	          brsmemset(ADR(RecvBuf), 0, SIZEOF(RecvBuf));
	        END_IF;
	      END_IF;
	    END_IF;
	
	    IF ioCmd.AutoControl.Stop OR Restart THEN
	      brsmemset(ADR(RecvBuf),0,SIZEOF(RecvBuf)); // Reset receive structure 
	      ioCmd.Status.State := L_TCP_STATE_STOP;
	    END_IF;
	
	  L_TCP_STATE_STOP:
	    // Stepper = 9 
	
	    IF    (Local_Ident <> 0) THEN
	      ioCmd.Status.State := L_TCP_STATE_LINGER_LOCAL_IDENT;
	    ELSIF (Server_Ident <> 0) THEN
	      ioCmd.Status.State := L_TCP_STATE_LINGER_SERVER_IDENT;
	    ELSE
	      ioCmd.Status.State := L_TCP_STATE_NO_REQUEST;
	    END_IF;
	
	  L_TCP_STATE_LINGER_LOCAL_IDENT:
	    // Stepper = 10 
	
	    TcpIoctl_0(
	      enable  := TRUE,
	      ident   := Local_Ident,
	      ioctl   := tcpSO_LINGER_SET,
	      pData   := ADR(tcpLINGER),
	      datalen := SIZEOF(tcpLINGER));
	
	    IF    (TcpIoctl_0.status = 0) THEN
	      ioCmd.Status.State := L_TCP_STATE_CLOSE_LOCAL_IDENT;
	    ELSIF (TcpIoctl_0.status <> 65535) THEN
	      ioCmd.Status.State := L_TCP_STATE_CLOSE_LOCAL_IDENT;
	    END_IF;
	
	  L_TCP_STATE_CLOSE_LOCAL_IDENT:
	    // Stepper = 11 
	
	    TcpClose_0(
	      enable := TRUE,
	      ident  := Local_Ident,
	      how    := 0);
	
	    IF (TcpClose_0.status = 0) THEN
	      InternalReceiveBufLen    := 0; // Start fresh 
	      Local_Ident              := 0;
	      IF (Server_Ident <> 0) THEN
	        ioCmd.Status.State := L_TCP_STATE_LINGER_SERVER_IDENT;
	      ELSE
	        ioCmd.Status.State := L_TCP_STATE_NO_REQUEST;
	      END_IF;
	    ELSIF (TcpClose_0.status <> 65535) THEN
	      IF (Server_Ident <> 0) THEN // Try other connection 
	        ioCmd.Status.State := L_TCP_STATE_LINGER_SERVER_IDENT;
	      ELSE
	        ioCmd.Status.State := L_TCP_STATE_NO_REQUEST;
	      END_IF;
	    END_IF;
	
	  L_TCP_STATE_LINGER_SERVER_IDENT:
	    // Stepper = 12 
	
	    TcpIoctl_0(
	      enable  := TRUE,
	      ident   := Server_Ident,
	      ioctl   := tcpSO_LINGER_SET,
	      pData   := ADR(tcpLINGER),
	      datalen := SIZEOF(tcpLINGER));
	
	    IF    (TcpIoctl_0.status = 0) THEN
	      ioCmd.Status.State := L_TCP_STATE_CLOSE_SERVER_IDENT;
	    ELSIF (TcpIoctl_0.status <> 65535) THEN
	      ioCmd.Status.State := L_TCP_STATE_CLOSE_SERVER_IDENT;
	    END_IF;
	
	  L_TCP_STATE_CLOSE_SERVER_IDENT:
	    // Stepper = 13 
	
	    TcpClose_0(
	      enable := TRUE,
	      ident  := Server_Ident,
	      how    := 0);
	
	    IF    (TcpClose_0.status = 0) THEN
	      ioCmd.Status.State := L_TCP_STATE_NO_REQUEST;
	      InternalReceiveBufLen  := 0; // Start fresh 
	      Server_Ident           := 0;
	    ELSIF (TcpClose_0.status <> 65535) THEN
	      ioCmd.Status.State := L_TCP_STATE_NO_REQUEST;
	    END_IF;
	
	END_CASE;

	(*----------------------------------------------------------------------------------------------------------------------*)
	(* Timer.                                                                                                               *)
	(*----------------------------------------------------------------------------------------------------------------------*)
	IF (StateOld = ioCmd.Status.State) THEN
	  ioCmd.Status.StateElapsedTime := ioCmd.Status.StateElapsedTime + TimeBetweenBlockCalls;
	ELSE
	  ioCmd.Status.StateElapsedTime := 0;
	END_IF;	
	StateOld := ioCmd.Status.State;
	
	//----------------------------------------------------------------------------------------------------------------------
	// Not online: Clear receive structure.                                                                                 
	//----------------------------------------------------------------------------------------------------------------------
  IF ioCmd.Status.Running AND NOT ioCmd.Status.Online THEN
	  brsmemset(iConfig.Data.ReceiveStructure,0,iConfig.Data.SizeOfReceiveStructure);
	END_IF;
	
	//----------------------------------------------------------------------------------------------------------------------
	// Update status bits.                                                                                                  
	//----------------------------------------------------------------------------------------------------------------------
	ioCmd.Status.Alarm             := oCommStatus.StructureSizeNotOK OR TimeOutAlarm;
	ioCmd.Status.Server_IP_Address := iSettings.IP_Address.Server;
	ioCmd.Status.Client_IP_Address := iSettings.IP_Address.Client;
	ioCmd.Status.ClientPort        := ClientPort;
	ioCmd.Status.ServerPort        := ServerPort;
	ioCmd.AutoControl.Start        := FALSE;
	ioCmd.AutoControl.Stop         := FALSE;
	
	ServerIP_AddressOld            := iSettings.IP_Address.Server;
	ClientIP_AddressOld            := iSettings.IP_Address.Client;
	PortModeOld                    := iConfig.Port.Mode;

	//----------------------------------------------------------------------------------------------------------------------
	// Communication status.                                                                                                  
	//----------------------------------------------------------------------------------------------------------------------	
	oCommStatus.Online := ioCmd.Status.Online;
  IF NOT ioCmd.Status.Online THEN
	  oCommStatus.HMI_Color := 8;      // GRAY        
	ELSIF ioCmd.Status.Alarm THEN
	  oCommStatus.HMI_Color := 51;     // RED         
	ELSE
	  oCommStatus.HMI_Color := 226;    // GREEN       
	END_IF;

END_FUNCTION_BLOCK