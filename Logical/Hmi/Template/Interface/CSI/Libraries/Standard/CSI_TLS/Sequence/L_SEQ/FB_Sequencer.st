//************************************************************************************************************************
// Company:     GEA Food Solutions
//------------------------------------------------------------------------------------------------------------------------
// Name:        FB_Sequencer
// Author:      Bart van Leuken 
// Version:     V1.01.0
// Date:        24-mar-2016
//------------------------------------------------------------------------------------------------------------------------
// Restriction: None 
//
//------------------------------------------------------------------------------------------------------------------------
// Description: Function block to handle sequences 
//************************************************************************************************************************

FUNCTION_BLOCK FB_Sequencer

	//----------------------------------------------------------------------------------------------------------------------
	// Initialization.                                                                                              
	//----------------------------------------------------------------------------------------------------------------------
  IF iInitialization THEN
    iDisableWatchDog := FALSE;
  END_IF;

	//----------------------------------------------------------------------------------------------------------------------
  //	Step change                                                                                      
	//----------------------------------------------------------------------------------------------------------------------
  IF ( iNewStep <> oStep)
  THEN
    oStep := iNewStep;
  END_IF;
  	
  oPulseStep := FALSE;
	
  //----------------------------------------------------------------------------------------------------------------------
  //	Housekeeping if step has changed
  //----------------------------------------------------------------------------------------------------------------------
  IF 	 ( oStep <> LastScanStep
  	OR iInitialization)
  THEN
    // Set oneshot puls step changed
  	oPulseStep := TRUE;
  
   	oPrevStep := LastScanStep;
  	// Last call to fTON to inlude last scan
  	fTON_Step( IN := TRUE, PT := T#24d);
  	PrevStepElapsed := fTON_Step.ET;
  	// Ensure timer is reset during transition of step
  	fTON_Step(IN := FALSE, PT := iStepWatchDog);
  	
  	// Fill the StepLogger array with stepnrs
  	// Shift down the array
  	brsmemmove( ADR(oStepLogger[1]), ADR(oStepLogger[0]), SIZEOF(oStepLogger) - SIZEOF(oStepLogger[0]));
  	// Fill the first element
  	oStepLogger[0] := oStep;
  	
  	// Fill the History array with timestamps etc
  	StepStart := clock_ms();
  	// Shift down the history array
  	brsmemmove(	ADR(oHistory[1]), ADR(oHistory[0]), SIZEOF(oHistory) - SIZEOF(oHistory[0]));
  	
    // Create History[0]
  	oHistory[0] := 's000_tt:tt:tt.000';
  	// Insert step nr in s000
  	brsitoa(oStep,ADR(Temp));
  	brsmemcpy(ADR(oHistory[0])+4-brsstrlen(ADR(Temp)),ADR(Temp),brsstrlen(ADR(Temp)));
  	/// Insert time in tt:tt:tt
  	TIME_TO_TIMEStructure(StepStart,ADR(TS_StepStart));
  	ascTIMEStructure(ADR(TS_StepStart),ADR(Temp),9);
  	brsmemcpy(ADR(oHistory[0])+5,ADR(Temp),brsstrlen(ADR(Temp)));
  	// Insert ms in 000
  	brsitoa(TS_StepStart.millisec,ADR(Temp));
  	brsmemcpy(ADR(oHistory[0])+17-brsstrlen(ADR(Temp)),ADR(Temp),brsstrlen(ADR(Temp)));
  END_IF;

  //----------------------------------------------------------------------------------------------------------------------
  // Store Step for next scan
  //----------------------------------------------------------------------------------------------------------------------
  LastScanStep  := oStep;
  	
  //----------------------------------------------------------------------------------------------------------------------
  // Step timer
  //----------------------------------------------------------------------------------------------------------------------
  IF 	 ( iStepWatchDog = T#0s)
    OR iDisableWatchDog
  THEN
    fTON_Step(IN := TRUE, PT := T#24d);
  ELSE
    fTON_Step(IN := TRUE (*Step*) > 0, PT := iStepWatchDog);
  END_IF;
  oStepElapsed := fTON_Step.ET;
  	 	
  //----------------------------------------------------------------------------------------------------------------------
  // Set WatchDog bit
  //----------------------------------------------------------------------------------------------------------------------
  WatchDogError :=    ( iStepWatchDog > T#0s)
  								AND	fTON_Step.Q
  								AND ( oStep > 0)
      						AND NOT iDisableWatchDog;
  	
END_FUNCTION_BLOCK