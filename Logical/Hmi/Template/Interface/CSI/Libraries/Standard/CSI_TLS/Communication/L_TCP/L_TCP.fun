
FUNCTION_BLOCK FB_TCPIP
	VAR_INPUT
		iConfig : T_TCPIP_Config;
		iSettings : T_TCPIP_Settings;
	END_VAR
	VAR_OUTPUT
		oCommStatus : T_TCPIP_Comm_Sts;
	END_VAR
	VAR_IN_OUT
		ioCmd : T_TCPIP_Cmd;
	END_VAR
	VAR
		RTInfo_0 : RTInfo;
		ServerPort : UINT;
		ClientPort : UINT;
		ClientPortOffset : UINT;
		Port : UINT;
		TimeBetweenBlockCalls : UDINT;
		ServerIP_AddressOld : STRING[15];
		ClientIP_AddressOld : STRING[15];
		PortModeOld : USINT;
		Restart : BOOL;
		RestartTimeOut : TIME;
		SendTimeOut : TIME;
		TON_TimeOut : TON;
		TON_TimeOut_Restart : TON;
		TON_TimeOut_Send : TON;
		AddressCalculation : BOOL;
		TcpIoctl_0 : TcpIoctl;
		tcpLINGER : tcpLINGER_typ;
		indID : UDINT;
		maxID : UDINT;
		tcpID_LIST : ARRAY[0..127] OF UDINT;
		tcpSO_ADDRESS : tcpSO_ADDRESS_typ;
		soPort : UINT;
		soIPAddr : STRING[15];
		Local_Ident : UDINT;
		Server_Ident : UDINT;
		TcpClose_0 : TcpClose;
		TcpOpen_0 : TcpOpen;
		TcpClient_0 : TcpClient;
		TcpServer_0 : TcpServer;
		TcpSend_0 : TcpSend;
		TcpRecv_0 : TcpRecv;
		RemoteIP_Address : STRING[15];
		InternalReceiveBufLen : UDINT;
		InternalRecBufLenCnt : USINT;
		SendEnable : BOOL;
		RecvBuf : ARRAY[0..4095] OF UDINT;
		ReceiveStructureSize : REFERENCE TO UDINT;
		ActualSizeOfSendStructure : REFERENCE TO UDINT;
		TimeOutAlarm : BOOL;
		StateOld : T_TCPIP_State;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK FB_TCPIP_Sim
	VAR_INPUT
		iConfig : T_TCPIP_Config;
		iSettings : T_TCPIP_Settings;
	END_VAR
	VAR_OUTPUT
		oCommStatus : T_TCPIP_Comm_Sts;
	END_VAR
	VAR_IN_OUT
		ioCmd : T_TCPIP_Cmd;
	END_VAR
	VAR
		ServerPort : UINT;
		ClientPort : UINT;
		ClientPortOffset : UINT;
		Port : UINT;
		TimeBetweenBlockCalls : DINT;
		ServerIP_AddressOld : STRING[15];
		ClientIP_AddressOld : STRING[15];
		PortModeOld : USINT;
		AddressCalculation : BOOL;
		StateOld : T_TCPIP_State;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION FC_PORTGEN : UINT
	VAR_INPUT
		iIP_Address : STRING[15];
	END_VAR
	VAR
		len : INT;
		PortStr : STRING[3];
	END_VAR
END_FUNCTION
