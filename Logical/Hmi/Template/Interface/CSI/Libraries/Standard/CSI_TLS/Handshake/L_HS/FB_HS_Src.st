//************************************************************************************************************************
// Company:     GEA Food Solutions
//------------------------------------------------------------------------------------------------------------------------
// Name:        FB_HS_Src
// Author:      Bart van Leuken 
// Version:     V1.03.0
// Date:        17-sept-2018
//------------------------------------------------------------------------------------------------------------------------
// Restriction: 
//
//------------------------------------------------------------------------------------------------------------------------
// Description: Function block to handle the handshake protocol - Source side
//************************************************************************************************************************

FUNCTION_BLOCK FB_HS_Src
	
	//----------------------------------------------------------------------------------------------------------------------
	// Execution enable input.                                                                                              
	//----------------------------------------------------------------------------------------------------------------------
  F_TRIG_0(CLK := iEnable);  // 2018-09-17 RK, bug fix, enable low reset HS to initial state
	IF NOT iEnable
  THEN
	  ioHandshakeOut := 0;
    Handshake_Out.DownloadEnabled := FALSE;
    
    IF F_TRIG_0.Q
    THEN
      Reset := TRUE;  // 2018-09-17 FvdS, bug fix, enable low reset HS to initial state
    END_IF;
	END_IF;
	
	R_TRIG_0(CLK := iEnable);
	
	IF    NOT iEnable 
    AND NOT Reset   // 2018-09-17 FvdS, bug fix, enable low reset HS to initial state
  THEN 
    RETURN;
  END_IF;
	
	//----------------------------------------------------------------------------------------------------------------------
	// Initialization.                                                                                                      
	//----------------------------------------------------------------------------------------------------------------------
	IF R_TRIG_0.Q OR iInitialization 
  THEN
;
	END_IF;
	
	//----------------------------------------------------------------------------------------------------------------------
	// Read handshake word (In).                                                                                            
	//----------------------------------------------------------------------------------------------------------------------
	Handshake_In.MailboxLocked            := iHandshakeIn.0;
	Handshake_In.DataIsNotOK              := iHandshakeIn.1;
	Handshake_In.DataIsOK                 := iHandshakeIn.2;
	Handshake_In.DataTransferCompleted    := iHandshakeIn.3;
	Handshake_In.RequestData              := iHandshakeIn.4;
	Handshake_In.RequestReadMailbox       := iHandshakeIn.5;
	Handshake_In.RequestLockMailbox       := iHandshakeIn.6;
	Handshake_In.ReadMailboxCompleted     := iHandshakeIn.7;
	Handshake_In.DownloadEnabled          := iHandshakeIn.8;
	Handshake_In.SimplifiedDataRequest    := iHandshakeIn.10;
	Handshake_In.SimplifiedDataConfirm    := iHandshakeIn.11;
	Handshake_In.SimplifiedRequestSend    := iHandshakeIn.12;
	
	//----------------------------------------------------------------------------------------------------------------------
	// Read handshake word (Out) (if handshake trigger bits are locally set on Source).                                     
	//----------------------------------------------------------------------------------------------------------------------
	Handshake_Out.RequestLockMailbox      := ioHandshakeOut.6;
	Handshake_Out.SimplifiedDataRequest   := ioHandshakeOut.10;
	Handshake_Out.SimplifiedDataConfirm   := ioHandshakeOut.11;
	Handshake_Out.SimplifiedRequestSend   := ioHandshakeOut.12;
	
	//----------------------------------------------------------------------------------------------------------------------
	// Reset status information (single shot).                                                                                         
	//----------------------------------------------------------------------------------------------------------------------
	ioCmd.Status.DataIsNotOK              := FALSE;
	ioCmd.Status.HandshakeOK              := FALSE;
	ioCmd.Status.HandshakeNotOK           := FALSE;
	ioCmd.Status.TimeOut                  := FALSE;
	
	//----------------------------------------------------------------------------------------------------------------------
	// Transfer Timeout protection.                                                                                         
	//----------------------------------------------------------------------------------------------------------------------
	TON_TimeOut (
	  IN := ioCmd.Status.Running,
	  PT := iConfig.TimeOut);
	
	IF TON_TimeOut.Q 
  THEN
	  Reset := TRUE;
	  ioCmd.Status.TimeOut := TRUE;
	END_IF;
	
	//----------------------------------------------------------------------------------------------------------------------
	// Source: Normal handshake.                                                                                              
	//----------------------------------------------------------------------------------------------------------------------
  IF NOT iConfig.FastHandshake 
  THEN
    CASE ioCmd.Status.State OF
      L_HS_STATE_SRC_STOPPED: // Check for initiation of handshake 
        IF ioCmd.AutoControl.DownloadEnable AND Handshake_In.RequestData 
        THEN // Request from destination 
          Handshake_Out.RequestLockMailbox := TRUE;
          ioCmd.Status.State := L_HS_STATE_SRC_WAIT_BITSET_00;
        ELSIF ioCmd.AutoControl.DownloadEnable AND ioCmd.AutoControl.Trigger
        THEN // Trigger from source 
          Handshake_Out.RequestLockMailbox := TRUE;
          ioCmd.Status.State := L_HS_STATE_SRC_WAIT_BITSET_00;
        END_IF;
  	
      L_HS_STATE_SRC_WAIT_BITSET_00: // State 1 
        IF Handshake_In.MailboxLocked
        THEN // Mail box is locked by destination 
          ioCmd.Status.State := L_HS_STATE_SRC_TRANSFER_ACTIVE;
        ELSIF Handshake_In.DataIsNotOK
        THEN // Mail box not locked by destination 
          Handshake_Out.RequestReadMailbox := FALSE;
          ioCmd.Status.State := L_HS_STATE_SRC_ERROR;
        END_IF;
  	
      L_HS_STATE_SRC_TRANSFER_ACTIVE: // State 2 
        ioCmd.Status.DataTransferActive := TRUE;
        IF ioCmd.AutoControl.DataTransferCompleted
        THEN // Data transfer complete 
          ioCmd.Status.DataTransferActive     := FALSE;
          Handshake_Out.DataTransferCompleted := TRUE;
          ioCmd.Status.State := L_HS_STATE_SRC_WAIT_BITSET_01_02;
        END_IF;
  	
      L_HS_STATE_SRC_WAIT_BITSET_01_02: // State 3 
        IF Handshake_In.DataIsOK 
        THEN
          Handshake_Out.DataIsOK := TRUE;
          ioCmd.Status.State := L_HS_STATE_SRC_WAIT_BITCLR_01_02;
        ELSIF Handshake_In.DataIsNotOK
        THEN
          Handshake_Out.DataIsOK := TRUE;
          ioCmd.Status.DataIsNotOK := TRUE;
          ioCmd.Status.State := L_HS_STATE_SRC_WAIT_BITCLR_01_02;
        END_IF;
  	
      L_HS_STATE_SRC_WAIT_BITCLR_01_02: // State 4 
        IF     NOT Handshake_In.MailboxLocked
           AND NOT Handshake_In.DataIsNotOK
           AND NOT Handshake_In.DataIsOK
           AND NOT Handshake_In.RequestData
        THEN
          Handshake_Out.MailboxLocked         := FALSE;
          Handshake_Out.DataIsNotOK           := FALSE;
          Handshake_Out.DataIsOK              := FALSE;
          Handshake_Out.DataTransferCompleted := FALSE;
          Handshake_Out.RequestData           := FALSE;
          Handshake_Out.RequestReadMailbox    := FALSE;
          Handshake_Out.RequestLockMailbox    := FALSE;
          Handshake_Out.ReadMailboxCompleted  := FALSE;
          Handshake_Out.DownloadEnabled       := FALSE;
          Handshake_Out.SimplifiedDataRequest := FALSE;
          Handshake_Out.SimplifiedDataConfirm := FALSE;
          Handshake_Out.SimplifiedRequestSend := FALSE;
          ioCmd.Status.HandshakeOK            := TRUE;
          ioCmd.Status.State                  := L_HS_STATE_SRC_STOPPED;
        END_IF;
  	
      L_HS_STATE_SRC_ERROR: // State 20 
        IF NOT Handshake_In.DataIsNotOK
        THEN
          Reset := TRUE;
          ioCmd.Status.HandshakeNotOK := TRUE;
          ioCmd.Status.State := L_HS_STATE_SRC_STOPPED;
  	    END_IF;
	
      ELSE
        ioCmd.Status.State := L_HS_STATE_SRC_STOPPED;
    END_CASE;
  END_IF;
	
	//----------------------------------------------------------------------------------------------------------------------
	// Source: Fast handshake.                                                                                        
	//----------------------------------------------------------------------------------------------------------------------
  IF iConfig.FastHandshake
  THEN
  	CASE ioCmd.Status.State OF
  	  L_HS_STATE_SRC_STOPPED: // State 0 
  	    // Restriction: It is not allowed to have a send request from source and a data request from destination at the same time 
        IF ioCmd.AutoControl.DownloadEnable AND Handshake_In.SimplifiedDataRequest 
        THEN // Request from destination 
          ioCmd.Status.DataTransferActive := TRUE;
          ioCmd.Status.State := L_HS_STATE_SRC_WAIT_DATA_COPIED;
        ELSIF ioCmd.AutoControl.DownloadEnable AND (ioCmd.AutoControl.Trigger OR Handshake_Out.SimplifiedRequestSend)
        THEN // Trigger from source 
          Handshake_Out.SimplifiedRequestSend := TRUE;
          ioCmd.Status.State := L_HS_STATE_SRC_WAIT_BITSET_10;
        END_IF;

  	  L_HS_STATE_SRC_WAIT_BITSET_10: // State 10 
        IF Handshake_In.SimplifiedDataRequest
        THEN
          Handshake_Out.SimplifiedRequestSend := FALSE;        
          ioCmd.Status.DataTransferActive := TRUE;
          ioCmd.Status.State := L_HS_STATE_SRC_WAIT_DATA_COPIED;
        END_IF;    
  	  
  	  L_HS_STATE_SRC_WAIT_DATA_COPIED: // State 11 
        IF ioCmd.AutoControl.DataTransferCompleted
        THEN
          ioCmd.Status.DataTransferActive := FALSE;
          Handshake_Out.SimplifiedDataConfirm := TRUE; // Data has been send to destination 
          ioCmd.Status.State := L_HS_STATE_SRC_WAIT_BITCLR_10;
        END_IF;      
  	      
  	  L_HS_STATE_SRC_WAIT_BITCLR_10: // State 12 
        IF NOT Handshake_In.SimplifiedDataRequest AND ioCmd.AutoControl.DownloadEnable AND Handshake_In.DownloadEnabled 
        THEN
          Handshake_Out.SimplifiedDataConfirm := FALSE;
          ioCmd.Status.State := L_HS_STATE_SRC_STOPPED;
          ioCmd.Status.HandshakeOK := TRUE;
  	    END_IF;
    	
      ELSE
        ioCmd.Status.State := L_HS_STATE_SRC_STOPPED;  	

  	END_CASE;
  END_IF;
	
	//----------------------------------------------------------------------------------------------------------------------
	// Download enabled.                                                                                                    
	//----------------------------------------------------------------------------------------------------------------------
  Handshake_Out.DownloadEnabled := ioCmd.AutoControl.DownloadEnable;
	
	F_TRIG_DownloadEnabled(CLK := ioCmd.AutoControl.DownloadEnable);
	
	IF F_TRIG_DownloadEnabled.Q
  THEN
	  Reset := TRUE;
	END_IF;
	
	//----------------------------------------------------------------------------------------------------------------------
	// Reset handshake.                                                                                                     
	//----------------------------------------------------------------------------------------------------------------------
	IF Reset 
  THEN
	  Handshake_Out.MailboxLocked           := FALSE;
	  Handshake_Out.DataIsNotOK             := FALSE;
	  Handshake_Out.DataIsOK                := FALSE;
	  Handshake_Out.DataTransferCompleted   := FALSE;
	  Handshake_Out.RequestData             := FALSE;
	  Handshake_Out.RequestReadMailbox      := FALSE;
	  Handshake_Out.RequestLockMailbox      := FALSE;
	  Handshake_Out.ReadMailboxCompleted    := FALSE;
	  Handshake_Out.DownloadEnabled         := FALSE;
	  Handshake_Out.SimplifiedDataRequest   := FALSE;
	  Handshake_Out.SimplifiedDataConfirm   := FALSE;
	  Handshake_Out.SimplifiedRequestSend   := FALSE;	  
	  ioCmd.Status.DataTransferActive       := FALSE;
	  ioCmd.Status.DataIsNotOK              := FALSE;
	  ioCmd.Status.HandshakeOK              := FALSE;
	  ioCmd.Status.HandshakeNotOK           := TRUE;
	  ioCmd.Status.State                    := L_HS_STATE_SRC_STOPPED;
    ioHandshakeOut                        := 0;
	  Reset                                 := FALSE;
	END_IF;
	
	//----------------------------------------------------------------------------------------------------------------------
	// Handshake running.                                                                                                   
	//----------------------------------------------------------------------------------------------------------------------
	ioCmd.Status.Running := (ioCmd.Status.State > L_HS_STATE_SRC_STOPPED);

	//----------------------------------------------------------------------------------------------------------------------
	// Reset commands                                                                                                   
	//----------------------------------------------------------------------------------------------------------------------  
  IF ioCmd.Status.Running 
  THEN
    ioCmd.AutoControl.Trigger := FALSE;
  END_IF;
  IF NOT ioCmd.Status.Running 
  THEN
    ioCmd.AutoControl.DataTransferCompleted := FALSE;
  END_IF;
  	
	//----------------------------------------------------------------------------------------------------------------------
	// Write handshake word.                                                                                                
	//----------------------------------------------------------------------------------------------------------------------
  ioHandshakeOut.0  := Handshake_Out.MailboxLocked;          // Bit 0  MailBoxLocked                                      
  ioHandshakeOut.1  := Handshake_Out.DataIsNotOK;            // Bit 1  DataIsNotOK                                 
  ioHandshakeOut.2  := Handshake_Out.DataIsOK;               // Bit 2  DataIsOK                                     
  ioHandshakeOut.3  := Handshake_Out.DataTransferCompleted;  // Bit 3  DataTransferCompleted                              
  ioHandshakeOut.4  := Handshake_Out.RequestData;            // Bit 4  RequestData                                        
  ioHandshakeOut.5  := Handshake_Out.RequestReadMailbox;     // Bit 5  RequestReadMailbox                                 
  ioHandshakeOut.6  := Handshake_Out.RequestLockMailbox;     // Bit 6  RequestLockMailbox                                 
  ioHandshakeOut.7  := Handshake_Out.ReadMailboxCompleted;   // Bit 7  ReadMailboxCompleted                               
  ioHandshakeOut.8  := Handshake_Out.DownloadEnabled;        // Bit 8  DownloadEnabled                                    
  ioHandshakeOut.10 := Handshake_Out.SimplifiedDataRequest;  // Bit 10 SimplifiedDataRequest                              
  ioHandshakeOut.11 := Handshake_Out.SimplifiedDataConfirm;  // Bit 11 SimplifiedDataConfirm                              
  ioHandshakeOut.12 := Handshake_Out.SimplifiedRequestSend;  // Bit 12 SimplifiedRequestSend                              

END_FUNCTION_BLOCK