//************************************************************************************************************************
// Company:     GEA Food Solutions
//------------------------------------------------------------------------------------------------------------------------
// Name:        FB_HS_Dst
// Author:      Bart van Leuken 
// Version:     V1.04.0
// Date:        17-sep-2018
//------------------------------------------------------------------------------------------------------------------------
// Restriction:  
//
//------------------------------------------------------------------------------------------------------------------------
// Description: Function block to handle the handshake protocol - Destination side
//************************************************************************************************************************

FUNCTION_BLOCK FB_HS_Dst
	
	//----------------------------------------------------------------------------------------------------------------------
	// Execution enable input.                                                                                              
	//----------------------------------------------------------------------------------------------------------------------
  F_TRIG_0(CLK := iEnable); // 2018-09-17 RK, bug fix, enable low reset HS to initial state
	IF NOT iEnable
  THEN
	  ioHandshakeOut := 0;
    Handshake_Out.DownloadEnabled := FALSE;

    IF F_TRIG_0.Q
    THEN
      Reset := TRUE;  // 2018-09-17 FvdS, bug fix, enable low reset HS to initial state
    END_IF;
	END_IF;
	
	R_TRIG_0(CLK := iEnable);
	
	IF    NOT iEnable 
    AND NOT Reset   // 2018-09-17 FvdS, bug fix, enable low reset HS to initial state
  THEN 
    RETURN;
  END_IF;
	
	//----------------------------------------------------------------------------------------------------------------------
	// Initialization.                                                                                                      
	//----------------------------------------------------------------------------------------------------------------------
	IF R_TRIG_0.Q OR iInitialization
  THEN
;
	END_IF;
	
	//----------------------------------------------------------------------------------------------------------------------
	// Read handshake word (In).                                                                                            
	//----------------------------------------------------------------------------------------------------------------------
	Handshake_In.MailboxLocked            := iHandshakeIn.0;
	Handshake_In.DataIsNotOK              := iHandshakeIn.1;
	Handshake_In.DataIsOK                 := iHandshakeIn.2;
	Handshake_In.DataTransferCompleted    := iHandshakeIn.3;
	Handshake_In.RequestData              := iHandshakeIn.4;
	Handshake_In.RequestReadMailbox       := iHandshakeIn.5;
	Handshake_In.RequestLockMailbox       := iHandshakeIn.6;
	Handshake_In.ReadMailboxCompleted     := iHandshakeIn.7;
	Handshake_In.DownloadEnabled          := iHandshakeIn.8;
	Handshake_In.SimplifiedDataRequest    := iHandshakeIn.10;
	Handshake_In.SimplifiedDataConfirm    := iHandshakeIn.11;
	Handshake_In.SimplifiedRequestSend    := iHandshakeIn.12;
	
	//----------------------------------------------------------------------------------------------------------------------
	// Read handshake word (Out) (if handshake trigger bits are locally set on Source).                                     
	//----------------------------------------------------------------------------------------------------------------------
	Handshake_Out.RequestLockMailbox      := ioHandshakeOut.6;
	Handshake_Out.SimplifiedDataRequest   := ioHandshakeOut.10;
	Handshake_Out.SimplifiedDataConfirm   := ioHandshakeOut.11;
	Handshake_Out.SimplifiedRequestSend   := ioHandshakeOut.12;
	
	//----------------------------------------------------------------------------------------------------------------------
	// Reset outputs (single shot).                                                                                         
	//----------------------------------------------------------------------------------------------------------------------
	ioCmd.Status.DataTransferCompleted    := FALSE;
	ioCmd.Status.HandshakeOK              := FALSE;
	ioCmd.Status.HandshakeNotOK           := FALSE;
	ioCmd.Status.TimeOut                  := FALSE;
	
	//----------------------------------------------------------------------------------------------------------------------
	// Transfer Timeout protection.                                                                                         
	//----------------------------------------------------------------------------------------------------------------------
	TON_TimeOut (
	  IN := ioCmd.Status.Running,
	  PT := iConfig.TimeOut);
	
	IF TON_TimeOut.Q
  THEN
	  Reset := TRUE;
	  ioCmd.Status.TimeOut := TRUE;
	END_IF;
	
	//----------------------------------------------------------------------------------------------------------------------
	// Destination: Normal handshake
	//----------------------------------------------------------------------------------------------------------------------
  IF NOT iConfig.FastHandshake 
  THEN
    CASE ioCmd.Status.State OF
      L_HS_STATE_DST_STOPPED: // State 0
        IF ioCmd.AutoControl.DownloadEnable AND Handshake_In.RequestLockMailbox
        THEN // Request from source 
          Handshake_Out.MailboxLocked := TRUE;
          ioCmd.Status.State := L_HS_STATE_DST_WAIT_BITSET_03;
        ELSIF ioCmd.AutoControl.DownloadEnable AND ioCmd.AutoControl.Trigger
        THEN // Trigger from destination 
          Handshake_Out.RequestData := TRUE;
          ioCmd.Status.State := L_HS_STATE_DST_WAIT_BITSET_06;
        END_IF;
	
      L_HS_STATE_DST_WAIT_BITSET_06: // State 1
        IF Handshake_In.RequestLockMailbox
        THEN
          Handshake_Out.MailboxLocked := TRUE;
          ioCmd.Status.State := L_HS_STATE_DST_WAIT_BITSET_03;
        END_IF;
	
      L_HS_STATE_DST_WAIT_BITSET_03: // State 2 - Wait for set Data transfer completed
        IF Handshake_In.DataTransferCompleted 
        THEN
          ioCmd.Status.DataTransferCompleted := TRUE;
          ioCmd.Status.State := L_HS_STATE_DST_CHECK_DATA;
        END_IF;
	
      L_HS_STATE_DST_CHECK_DATA: // State 3 
        IF ioCmd.AutoControl.DataIsOK OR NOT iConfig.DataCheck
        THEN // Data contains no errors 
          Handshake_Out.DataIsOK       := TRUE;
          ioCmd.Status.DataTransferCompleted := FALSE;
          ioCmd.Status.State := L_HS_STATE_DST_WAIT_BITSET_02;
        ELSIF ioCmd.AutoControl.DataIsNotOK AND iConfig.DataCheck
        THEN // Data contains errors 
          Handshake_Out.DataIsNotOK := TRUE;
          ioCmd.Status.DataTransferCompleted := FALSE;
          ioCmd.Status.State := L_HS_STATE_DST_WAIT_BITSET_02;
        END_IF;
	
      L_HS_STATE_DST_WAIT_BITSET_02: // State 4 - Wait for set Data is OK
        IF Handshake_In.DataIsOK
        THEN
          Handshake_Out.MailboxLocked         := FALSE;
          Handshake_Out.DataIsNotOK           := FALSE;
          Handshake_Out.DataIsOK              := FALSE;
          Handshake_Out.DataTransferCompleted := FALSE;
          Handshake_Out.RequestData           := FALSE;
          Handshake_Out.RequestReadMailbox    := FALSE;
          Handshake_Out.RequestLockMailbox    := FALSE;
          Handshake_Out.ReadMailboxCompleted  := FALSE;
          Handshake_Out.DownloadEnabled       := FALSE;
          Handshake_Out.SimplifiedDataRequest := FALSE;
          Handshake_Out.SimplifiedDataConfirm := FALSE;
          Handshake_Out.SimplifiedRequestSend := FALSE;
          ioCmd.Status.State := L_HS_STATE_DST_WAIT_BITCLR_02;
        END_IF;
      
      L_HS_STATE_DST_WAIT_BITCLR_02: // State 5 - Wait for reset Data is OK
        IF NOT Handshake_In.DataIsOK
        THEN
          ioCmd.Status.HandshakeOK := TRUE;
          ioCmd.Status.State := L_HS_STATE_DST_STOPPED;
        END_IF;
      
      L_HS_STATE_DST_ERROR: // State 20 
        IF NOT Handshake_In.RequestLockMailbox
        THEN
          Reset := TRUE;
          ioCmd.Status.HandshakeNotOK  := TRUE;
          ioCmd.Status.State := L_HS_STATE_DST_STOPPED;
        END_IF;
      
      ELSE
        ioCmd.Status.State := L_HS_STATE_DST_STOPPED;
    END_CASE;
  END_IF;
		
	//----------------------------------------------------------------------------------------------------------------------
	// Destination: Fast handshake.                                                                                   
	//----------------------------------------------------------------------------------------------------------------------
  IF iConfig.FastHandshake 
  THEN    
    CASE ioCmd.Status.State OF
      L_HS_STATE_DST_STOPPED: // State 0
        // Restriction: Not allowed to have a send request from source and a data request from destination at the same time 
        IF      ioCmd.AutoControl.DownloadEnable
          AND (   Handshake_In.SimplifiedRequestSend
               OR (ioCmd.AutoControl.Trigger OR Handshake_Out.SimplifiedDataRequest))
        THEN
          Handshake_Out.SimplifiedDataRequest := TRUE;
          ioCmd.Status.State := L_HS_STATE_DST_WAIT_BITSET_11;
        END_IF;
    
      L_HS_STATE_DST_WAIT_BITSET_11: // State 10
        IF Handshake_In.SimplifiedDataConfirm AND NOT Handshake_In.SimplifiedRequestSend
        THEN
          Handshake_Out.SimplifiedDataRequest := FALSE;      
          ioCmd.Status.DataTransferCompleted := TRUE;
          ioCmd.Status.State := L_HS_STATE_DST_WAIT_BITCLR_11;
        END_IF;    
    
      L_HS_STATE_DST_WAIT_BITCLR_11: // State 11
        IF NOT Handshake_In.SimplifiedDataConfirm AND ioCmd.AutoControl.DownloadEnable AND Handshake_In.DownloadEnabled
        THEN
          ioCmd.Status.DataTransferCompleted := FALSE;
          ioCmd.Status.HandshakeOK := TRUE;
          ioCmd.Status.State := L_HS_STATE_DST_STOPPED;
        END_IF;

      ELSE
        ioCmd.Status.State := L_HS_STATE_DST_STOPPED;      
    END_CASE;
  END_IF;
	
	//----------------------------------------------------------------------------------------------------------------------
	// Download enabled.                                                                                                    
	//----------------------------------------------------------------------------------------------------------------------
  Handshake_Out.DownloadEnabled := ioCmd.AutoControl.DownloadEnable;
	
	F_TRIG_DownloadEnabled(CLK := ioCmd.AutoControl.DownloadEnable);
	
	IF F_TRIG_DownloadEnabled.Q 
  THEN
	  Reset := TRUE;
	END_IF;
	
	//----------------------------------------------------------------------------------------------------------------------
	// Reset handshake.                                                                                                     
	//----------------------------------------------------------------------------------------------------------------------
	IF Reset 
  THEN
	  Handshake_Out.MailboxLocked           := FALSE;
	  Handshake_Out.DataIsNotOK             := FALSE;
	  Handshake_Out.DataIsOK                := FALSE;
	  Handshake_Out.DataTransferCompleted   := FALSE;
	  Handshake_Out.RequestData             := FALSE;
	  Handshake_Out.RequestReadMailbox      := FALSE;
	  Handshake_Out.RequestLockMailbox      := FALSE;
	  Handshake_Out.ReadMailboxCompleted    := FALSE;
	  Handshake_Out.DownloadEnabled         := FALSE;
	  Handshake_Out.SimplifiedDataRequest   := FALSE;
	  Handshake_Out.SimplifiedDataConfirm   := FALSE;
	  Handshake_Out.SimplifiedRequestSend   := FALSE;	  
	  ioCmd.Status.DataTransferCompleted    := FALSE;
	  ioCmd.Status.HandshakeOK              := FALSE;
	  ioCmd.Status.HandshakeNotOK           := TRUE;
    ioCmd.Status.State                    := L_HS_STATE_DST_STOPPED;
	  ioHandshakeOut                        := 0;
	  Reset                                 := FALSE;
	END_IF;
	
	//----------------------------------------------------------------------------------------------------------------------
	// Handshake running.                                                                                                   
	//----------------------------------------------------------------------------------------------------------------------
	ioCmd.Status.Running := (ioCmd.Status.State > L_HS_STATE_DST_STOPPED);
	
	//----------------------------------------------------------------------------------------------------------------------
	// Reset commands                                                                                                   
	//----------------------------------------------------------------------------------------------------------------------  
  IF ioCmd.Status.Running 
  THEN
    ioCmd.AutoControl.Trigger := FALSE;
  END_IF;
  ioCmd.AutoControl.DataIsNotOK := FALSE;  
  ioCmd.AutoControl.DataIsOK    := FALSE;    
  
	//----------------------------------------------------------------------------------------------------------------------
	// Write handshake word.                                                                                                
	//----------------------------------------------------------------------------------------------------------------------
  ioHandshakeOut.0  := Handshake_Out.MailboxLocked;          // Bit 0  MailBoxLocked                                      
  ioHandshakeOut.1  := Handshake_Out.DataIsNotOK;            // Bit 1  DataIsNotOK                                 
  ioHandshakeOut.2  := Handshake_Out.DataIsOK;               // Bit 2  DataIsOK                                     
  ioHandshakeOut.3  := Handshake_Out.DataTransferCompleted;  // Bit 3  DataTransferCompleted                              
  ioHandshakeOut.4  := Handshake_Out.RequestData;            // Bit 4  RequestData                                        
  ioHandshakeOut.5  := Handshake_Out.RequestReadMailbox;     // Bit 5  RequestReadMailbox                                 
  ioHandshakeOut.6  := Handshake_Out.RequestLockMailbox;     // Bit 6  RequestLockMailbox                                 
  ioHandshakeOut.7  := Handshake_Out.ReadMailboxCompleted;   // Bit 7  ReadMailboxCompleted                               
  ioHandshakeOut.8  := Handshake_Out.DownloadEnabled;        // Bit 8  DownloadEnabled                                    
  ioHandshakeOut.10 := Handshake_Out.SimplifiedDataRequest;  // Bit 10 SimplifiedDataRequest                              
  ioHandshakeOut.11 := Handshake_Out.SimplifiedDataConfirm;  // Bit 11 SimplifiedDataConfirm                              
  ioHandshakeOut.12 := Handshake_Out.SimplifiedRequestSend;  // Bit 12 SimplifiedRequestSend                              

END_FUNCTION_BLOCK