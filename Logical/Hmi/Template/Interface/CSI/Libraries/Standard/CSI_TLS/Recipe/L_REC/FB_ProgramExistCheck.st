
//************************************************************************************************************************
// Company:     GEA Food Solutions Bakel
//------------------------------------------------------------------------------------------------------------------------
// Name:        FB_ProgramExistCheck 
// Author:      Bart van Leuken
// Version:     V1.02.0
// Date:        23-sep-2016
//------------------------------------------------------------------------------------------------------------------------
// Restriction: Program calls via Recipe interface will only be executed if corresponding sequence order = actual sequence order 
//              So for non batch controlled machines, sequence order for the program calls should be 0 (which is default).  
//------------------------------------------------------------------------------------------------------------------------
// Description: Function block to check if a program number exists (CSV-file available in folder 'RECIPE' or another folder).
//************************************************************************************************************************

FUNCTION_BLOCK FB_ProgramExistCheck
		
	//----------------------------------------------------------------------------------------------------------------------
	// Execution enable input.                                                                                              
	//----------------------------------------------------------------------------------------------------------------------
	IF NOT iEnable THEN
	  ;
	END_IF;
	
	R_TRIG_0(CLK := iEnable);
	
	IF NOT iEnable THEN RETURN; END_IF;
	
	//----------------------------------------------------------------------------------------------------------------------
	// Initialization.                                                                                                      
	//----------------------------------------------------------------------------------------------------------------------
	IF R_TRIG_0.Q OR iInitialization THEN
    ioCmd.Status.Installed  := TRUE;
	  ioCmd.Status.FileExists := FALSE;
    ioCmd.Status.Ready      := FALSE;
	  SizeOfProgramNumber     := 0;
	  Stepper                 := 0;
    FOR i := 0 TO (SIZEOF(ioCmd.Alarms.ActiveAlarm) / SIZEOF(ioCmd.Alarms.ActiveAlarm[0])) - 1 DO
      ioCmd.Alarms.ActiveAlarm[i] := FALSE;
      ioCmd.Alarms.SetAlarm[i]    := FALSE;
      ioCmd.Alarms.ResetAlarm[i]  := FALSE;
    END_FOR;
	END_IF;

	//----------------------------------------------------------------------------------------------------------------------
	// Stepper                                                                                           
	//----------------------------------------------------------------------------------------------------------------------
	CASE Stepper OF
		                                                                                    
    0 : // Stepper 0 : PROGRAM FILE NAME.   

      // Transitions
  	  IF ioCmd.AutoControl.Check
      THEN
        IF iConfig.FolderName = ''
        THEN
          FolderName := 'RECIPE';
        ELSE
          FolderName := iConfig.FolderName;        
        END_IF;      
	      ioCmd.Status.FileExists := FALSE;
        ioCmd.Status.Ready      := FALSE;
  	    ProgramNumberConvertStatus := brsitoa(UDINT_TO_DINT(ioCmd.AutoControl.ProgramNumber), ADR(FileName));
  	    brsstrcat(ADR(FileName), ADR('.CSV'));
  	    SizeOfProgramNumber := brsstrlen(ADR(FileName));	
  	    IF (SizeOfProgramNumber > 32)
        THEN
  	      ioCmd.Status.FileExists := FALSE;
  	      ioCmd.Status.Ready      := TRUE;
  	      Stepper                 := 2;
  	    ELSE
  	      Stepper                 := 1;
  	    END_IF;
  	  END_IF;

    1 : // Stepper 1 : PROGRAM FILE CHECK.

      // Actions
  	  SearchFileName_Program(
  	    pFileName := ADR(FileName),
  	    pDevice   := ADR(FolderName));
  	
      // Transitions
  	  IF (SearchFileName_Program.status = 1)
      THEN // File found 
  	    ioCmd.Status.FileExists := TRUE;
  	    ioCmd.Status.Ready      := TRUE;
  	    Stepper                 := 2;
  	  ELSIF (SearchFileName_Program.status <> 65535)
      THEN // File not found 
  	    ioCmd.Status.FileExists  := FALSE;
  	    ioCmd.Status.Ready       := TRUE;
  	    Stepper                  := 2;
        ioCmd.Alarms.SetAlarm[L_REC_ALARM_RECIPE_NOT_EXIST_ERR] := TRUE;
  	  END_IF;	

    2 : // Stepper 2 : RESET

      // Transitions
  	  IF NOT ioCmd.AutoControl.Check
      THEN
  	    ioCmd.Status.FileExists := FALSE;
  	    ioCmd.Status.Ready      := FALSE;
  	    SizeOfProgramNumber     := 0;
  	    Stepper                 := 0;
  	  END_IF;

  END_CASE; 
  
  //----------------------------------------------------------------------------------------------------------------------
  // Status
  //----------------------------------------------------------------------------------------------------------------------  
  ioCmd.Status.AlarmStatus.0 := ioCmd.Alarms.ActiveAlarm[L_REC_ALARM_RECIPE_NOT_EXIST_ERR];  
  
  //----------------------------------------------------------------------------------------------------------------------
  // Alarm handling
  //----------------------------------------------------------------------------------------------------------------------
  TON_AlarmHandler(
    IN := NOT TON_AlarmHandler.Q,
    PT := T#1s);
  
  IF TON_AlarmHandler.Q
  THEN
    FOR i := 0 TO (SIZEOF(ioCmd.Alarms.ActiveAlarm) / SIZEOF(ioCmd.Alarms.ActiveAlarm[0])) - 1 DO
      IF ioCmd.Alarms.SetAlarm[i]
      THEN
        ioCmd.Alarms.ActiveAlarm[i] := TRUE;
      ELSIF    ioCmd.Alarms.ResetAlarm[i]
            OR ioCmd.Alarms.ResetAllAlarms
      THEN
        ioCmd.Alarms.ActiveAlarm[i] := FALSE;
        ioCmd.Alarms.ResetAlarm[i] := FALSE;
      END_IF;
      ioCmd.Alarms.SetAlarm[i] := FALSE;
    END_FOR;
  ioCmd.Alarms.ResetAllAlarms := FALSE;
  END_IF;

END_FUNCTION_BLOCK