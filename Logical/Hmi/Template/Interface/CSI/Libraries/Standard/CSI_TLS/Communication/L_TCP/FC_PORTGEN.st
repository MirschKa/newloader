//************************************************************************************************************************
// Company:     GEA Food Solutions
//------------------------------------------------------------------------------------------------------------------------
// Name:        FC_PORTGEN
// Author:      Bart van Leuken
// Version:     V1.03.0
// Date:        08-sep-2014
//------------------------------------------------------------------------------------------------------------------------
// Restriction: None 
//
//------------------------------------------------------------------------------------------------------------------------
// Description: Function to automatically generate a port number (based on the IP-address).
//************************************************************************************************************************

FUNCTION FC_PORTGEN
	
	//----------------------------------------------------------------------------------------------------------------------
	// Generate port number.                                                                                                
	//----------------------------------------------------------------------------------------------------------------------
	FC_PORTGEN := 0;  // Clear port offset value 
	PortStr     := ''; // Empty string 
	len	        := LEN(iIP_Address);
	
	IF (len > 6) THEN // 0.0.0.0 = smallest string possible 
	  PortStr := RIGHT(iIP_Address, 3);
	  len := FIND(PortStr, '.');// Possible strings 000, .00, 0.0 
	  IF (len <> 0 ) THEN
	    PortStr := DELETE(PortStr, len , 1); // Delete portion with point 
	  END_IF;
	  FC_PORTGEN := DINT_TO_UINT(brsatoi(ADR(PortStr)));
	END_IF;

END_FUNCTION