
FUNCTION_BLOCK FB_HS_Src
	VAR_INPUT
		iEnable : BOOL;
		iInitialization : BOOL;
		iConfig : T_HS_Src_Config;
		iHandshakeIn : UINT;
	END_VAR
	VAR_IN_OUT
		ioHandshakeOut : UINT;
		ioCmd : T_HS_Src_Cmd;
	END_VAR
	VAR
		TON_TimeOut : TON;
		Handshake_In : T_HS_Bits;
		Handshake_Out : T_HS_Bits;
		Reset : BOOL;
		F_TRIG_0 : F_TRIG;
		R_TRIG_0 : R_TRIG;
		F_TRIG_DownloadEnabled : F_TRIG;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK FB_HS_Dst
	VAR_INPUT
		iEnable : BOOL;
		iInitialization : BOOL;
		iConfig : T_HS_Dst_Config;
		iHandshakeIn : UINT;
	END_VAR
	VAR_IN_OUT
		ioHandshakeOut : UINT;
		ioCmd : T_HS_Dst_Cmd;
	END_VAR
	VAR
		TON_TimeOut : TON;
		Handshake_In : T_HS_Bits;
		Handshake_Out : T_HS_Bits;
		Reset : BOOL;
		F_TRIG_0 : F_TRIG;
		R_TRIG_0 : R_TRIG;
		F_TRIG_DownloadEnabled : F_TRIG;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK FB_HS_FIFO
	VAR_INPUT
		iTrigger : BOOL;
		iData : ARRAY[0..3] OF REAL;
		iParameter : UDINT;
		iHandshakeFinished : BOOL;
	END_VAR
	VAR_OUTPUT
		oEmpty : BOOL; (*Empty, no records present*)
		oFull : BOOL; (*Full, only one remaining record free*)
		oParameter : UDINT;
		oData : ARRAY[0..3] OF REAL;
	END_VAR
	VAR
		ParameterBuffer : ARRAY[0..19] OF UDINT;
		DataBuffer : ARRAY[0..19,0..3] OF REAL;
		Index : USINT;
		LastParameterRecord : USINT;
		LastDataRecord : USINT;
		j : USINT;
		i : USINT;
	END_VAR
END_FUNCTION_BLOCK
