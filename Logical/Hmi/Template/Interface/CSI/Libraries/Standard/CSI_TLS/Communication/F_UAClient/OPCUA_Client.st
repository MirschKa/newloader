
(* OPC UA Client *)
FUNCTION_BLOCK OPCUA_Client

(* UA_Connect - establish connection to OPC-UA Server *)
	CASE COMStep OF
		INIT_CONNECTION:
			
			fbTrigger.PT := Config.CycleTime;
			OUT.Status := BUSY;
			IF Execute = TRUE THEN
				COMStep := CONNECT;
			END_IF	
			
		CONNECT:
			UA_Connect_0.ServerEndpointUrl 	:= Config.ServerEndpointUrl;
			UA_Connect_0.SessionConnectInfo 	:= Config.SessionConnectInfo_0 ;
			UA_Connect_0.Timeout 				:= T#10s;
			UA_Connect_0.Execute 				:= TRUE;
			COMStep := CONNECT_WAIT;

		CONNECT_WAIT:	
			IF (UA_Connect_0.Busy = FALSE) THEN
				UA_Connect_0.Execute := FALSE;
				IF (UA_Connect_0.Done = TRUE) THEN
					ErrorID := 0;
					ConnectionHdl := UA_Connect_0.ConnectionHdl;
					COMStep := GET_NAME_SPACE;		
				END_IF
				IF (UA_Connect_0.Error = 1) THEN
					ErrorID := UA_Connect_0.ErrorID;
					ConnectionHdl := 0;
					COMStep := ERROR;
				END_IF
			END_IF

		GET_NAME_SPACE:
			(* UA_GetNamespaceIndex - read index of required namespace for PVs *)
			UA_GetNamespaceIndex_0.ConnectionHdl 	:= ConnectionHdl;
			UA_GetNamespaceIndex_0.NamespaceUri 	:= 'http://br-automation.com/OpcUa/PLC/PV/';
			UA_GetNamespaceIndex_0.Timeout 			:= T#5s;
			UA_GetNamespaceIndex_0.Execute 			:= TRUE;
			COMStep := GET_NAME_SPACE_WAIT;

		GET_NAME_SPACE_WAIT:	
			IF (UA_GetNamespaceIndex_0.Busy = FALSE) THEN
				UA_GetNamespaceIndex_0.Execute := FALSE;
				IF (UA_GetNamespaceIndex_0.Done = TRUE) THEN
					ErrorID:= 0;
					NamespaceIndex := UA_GetNamespaceIndex_0.NamespaceIndex;
					COMStep := GET_HANDLE_READ_LIST;
				END_IF
				IF (UA_GetNamespaceIndex_0.Error = 1) THEN
					ErrorID:= UA_GetNamespaceIndex_0.ErrorID;
					NamespaceIndex := 0;
					COMStep := ERROR;
				END_IF
			END_IF

			(* UA_NodeGetHandleList - get handles for required nodes *)
		GET_HANDLE_READ_LIST:
			NodeIDsRead_0[0].NamespaceIndex 	:= NamespaceIndex;
			NodeIDsRead_0[0].Identifier 		:= Config.ReceiveNodeList;					//e.g '::AsGlobalPV:CSI_MMn.In_2'
			NodeIDsRead_0[0].IdentifierType 	:= UAIdentifierType_String;			 	

			UA_NodeGetHandleListRead_0.ConnectionHdl 	:= ConnectionHdl;
			UA_NodeGetHandleListRead_0.NodeIDCount 	:= 1;
			UA_NodeGetHandleListRead_0.NodeIDs 			:= NodeIDsRead_0;
			UA_NodeGetHandleListRead_0.Timeout 			:= T#10s;
			UA_NodeGetHandleListRead_0.Execute 			:= TRUE;
			COMStep := GET_HANDLE_READ_LIST_WAIT;

		GET_HANDLE_READ_LIST_WAIT:	
			IF (UA_NodeGetHandleListRead_0.Busy = FALSE) THEN
				UA_NodeGetHandleListRead_0.Execute := FALSE;
				IF (UA_NodeGetHandleListRead_0.Done = TRUE) THEN
					ErrorID:= 0;
					NodeHdlsRead := UA_NodeGetHandleListRead_0.NodeHdls;
					COMStep := GET_HANDLE_WRITE_LIST;
				END_IF
				IF (UA_NodeGetHandleListRead_0.Error = 1) THEN
					ErrorID:= UA_NodeGetHandleListRead_0.ErrorID;
					NodeHdlsRead[0] := 0;
					COMStep := ERROR;
				END_IF
			END_IF	

		GET_HANDLE_WRITE_LIST:
			NodeIDsWrite_0[0].NamespaceIndex 	:= NamespaceIndex;
			NodeIDsWrite_0[0].Identifier 			:= Config.SendNodeList;					//e.g ::AsGlobalPV:CSI_MMn.Out_2'; 
			NodeIDsWrite_0[0].IdentifierType 	:= UAIdentifierType_String;			 	
			
			UA_NodeGetHandleListWrite_0.ConnectionHdl 	:= ConnectionHdl;
			UA_NodeGetHandleListWrite_0.NodeIDCount 		:= 1;
			UA_NodeGetHandleListWrite_0.NodeIDs 			:= NodeIDsWrite_0;
			UA_NodeGetHandleListWrite_0.Timeout 			:= T#10s;
			UA_NodeGetHandleListWrite_0.Execute 			:= TRUE;
			COMStep := GET_HANDLE_WRITE_LIST_WAIT;

		GET_HANDLE_WRITE_LIST_WAIT:	
			IF (UA_NodeGetHandleListWrite_0.Busy = FALSE) THEN
				UA_NodeGetHandleListWrite_0.Execute := FALSE;
				IF (UA_NodeGetHandleListWrite_0.Done = TRUE) THEN
					ErrorID:= 0;
					NodeHdlsWrite := UA_NodeGetHandleListWrite_0.NodeHdls;
					COMStep := WRITE_LIST;
				END_IF
				IF (UA_NodeGetHandleListWrite_0.Error = 1) THEN
					ErrorID:= UA_NodeGetHandleListWrite_0.ErrorID;
					NodeHdlsWrite[0] := 0;
					COMStep := ERROR;
				END_IF
			END_IF		
			
		RUNNING:	
			IF UA_ConnectionGetStatus_0.Execute = TRUE THEN
				IF ConnectionStatus.eConnectionStatus <> UACS_Connected THEN
					// Der Server ist nicht mehr verbunden. In diesem Fall kann die Verbindung sp�ter vom Client automatisch wieder komplett reastauriert, wenn der Server wieder erreichbar ist.
					COMStep := CONNECTION_ERROR;
				ELSE
					// Trigger f�r zyklisches Lesen + Schreiben
					fbTrigger.IN := TRUE;
					IF fbTrigger.Q = TRUE THEN
						fbTrigger.IN := FALSE;
						COMStep := READ_LIST;
					END_IF
				END_IF
			END_IF

		READ_LIST:
			(* UA_ReadList - read required nodes from OPC-UA Server and write it in local plc variables *)
			IF ConnectionStatus.eConnectionStatus <> UACS_Connected THEN
				COMStep := RUNNING;
			ELSE
				NodeAddInfoRead_0[0].AttributeId := UAAI_Value;
				NodeAddInfoRead_0[0].IndexRangeCount := 0;
				Variables_Rd0[0] := Config.ReadPunkt ;//'::AsGlobalPV:CSI_MMn.In_2'
				
				UA_ReadList_0.ConnectionHdl 	:= ConnectionHdl;
				UA_ReadList_0.NodeHdlCount 	:= 1;
				UA_ReadList_0.NodeHdls 			:= NodeHdlsRead;
				UA_ReadList_0.NodeAddInfo 		:= NodeAddInfoRead_0;
				UA_ReadList_0.Timeout 			:= T#10s;
				UA_ReadList_0.Execute :=TRUE;
				COMStep := READ_LIST_WAIT;
			END_IF

		READ_LIST_WAIT:
			IF ConnectionStatus.eConnectionStatus <> UACS_Connected THEN
				COMStep := RUNNING;
				UA_ReadList_0.Execute := FALSE;
			ELSE
				IF (UA_ReadList_0.Busy = FALSE) THEN
					UA_ReadList_0.Execute := FALSE;
					IF (UA_ReadList_0.Done = TRUE) THEN
						ErrorID:= 0;
					END_IF
				END_IF
				COMStep := WRITE_LIST;
				IF (UA_ReadList_0.Error = 1 AND UA_ReadList_0.ErrorID <> 0) THEN
					ErrorID:= UA_ReadList_0.ErrorID;
					COMStep := ERROR;				
				END_IF
			END_IF

			(* UA_WriteList - write local plc variables to required nodes from OPC-UA Server *)
		WRITE_LIST:
			IF ConnectionStatus.eConnectionStatus <> UACS_Connected THEN
				COMStep := RUNNING;
			ELSE
				NodeAddInfoWrite_0[0].AttributeId := UAAI_Value;
				NodeAddInfoWrite_0[0].IndexRangeCount := 0;
				Variables_Wr0[0] := Config.WritePunkt;

				UA_WriteList_0.Execute := TRUE;
				UA_WriteList_0.ConnectionHdl := ConnectionHdl;
				UA_WriteList_0.NodeHdlCount := 1;
				UA_WriteList_0.NodeHdls := NodeHdlsWrite;
				UA_WriteList_0.NodeAddInfo := NodeAddInfoWrite_0;
				UA_WriteList_0.Timeout := T#10s;
				COMStep := WRITE_LIST_WAIT;			
			END_IF	

		WRITE_LIST_WAIT:
			IF ConnectionStatus.eConnectionStatus <> UACS_Connected THEN
				COMStep := RUNNING;
				UA_WriteList_0.Execute := FALSE;
			ELSE	
				IF (UA_WriteList_0.Busy = 0) THEN
					UA_WriteList_0.Execute := FALSE;
					IF (UA_WriteList_0.Done = TRUE) THEN
						ErrorID:= 0;
					END_IF
				END_IF
				COMStep:= RUNNING;
				IF (UA_WriteList_0.Error = TRUE AND UA_WriteList_0.ErrorID <> 0) THEN
					ErrorID:= UA_WriteList_0.ErrorID;
					COMStep := ERROR;
				END_IF	
			END_IF
			(* UA_NodeReleaseHandleList - release the handles for the nodes *)
			
		NODE_RELEASE_HANDLE_RLIST:
			UA_NodeReleaseHandleReadList_0.Execute := TRUE;
			UA_NodeReleaseHandleReadList_0.ConnectionHdl := ConnectionHdl;
			UA_NodeReleaseHandleReadList_0.NodeHdlCount := 1;
			UA_NodeReleaseHandleReadList_0.NodeHdls := NodeHdlsRead;
			UA_NodeReleaseHandleReadList_0.Timeout := T#10s;
			COMStep:= NODE_RELEASE_HANDLE_RLIST_WAIT;

		NODE_RELEASE_HANDLE_RLIST_WAIT:
			IF (UA_NodeReleaseHandleReadList_0.Busy = 0) THEN
				UA_NodeReleaseHandleReadList_0.Execute := FALSE;
				IF (UA_NodeReleaseHandleReadList_0.Done = 1) THEN
					ErrorID:= 0;
					NodeHdlsRead[0] := 0;
				END_IF
				IF (UA_NodeReleaseHandleReadList_0.Error = 1) THEN
					ErrorID:= UA_NodeReleaseHandleReadList_0.ErrorID;
				END_IF
			END_IF
		NODE_RELEASE_HANDLE_WLIST:
			UA_NodeReleaseHandleWriteList_0.Execute := TRUE;
			UA_NodeReleaseHandleWriteList_0.ConnectionHdl := ConnectionHdl;
			UA_NodeReleaseHandleWriteList_0.NodeHdlCount := 1;
			UA_NodeReleaseHandleWriteList_0.NodeHdls := NodeHdlsWrite;
			UA_NodeReleaseHandleWriteList_0.Timeout := T#10s;
			COMStep:= NODE_RELEASE_HANDLE_WLIST_WAIT;

		NODE_RELEASE_HANDLE_WLIST_WAIT:
			IF (UA_NodeReleaseHandleWriteList_0.Busy = 0) THEN
				UA_NodeReleaseHandleWriteList_0.Execute := FALSE;
				IF (UA_NodeReleaseHandleWriteList_0.Done = 1) THEN
					ErrorID:= 0;
					NodeHdlsWrite[0] := 0;
				END_IF
				IF (UA_NodeReleaseHandleWriteList_0.Error = 1) THEN
					ErrorID:= UA_NodeReleaseHandleWriteList_0.ErrorID;
				END_IF
			END_IF
						
		ERROR:
			OUT.ErrorID := ErrorID;
			OUT.Status := 0;
			IF Execute = TRUE THEN
				COMStep := DISCONNECT;
			END_IF
			(* UA_Disconnect - disconnect from OPC-UA Server *)
		DISCONNECT:
			UA_Disconnect_0.Execute := TRUE;
			UA_Disconnect_0.ConnectionHdl := ConnectionHdl;
			UA_Disconnect_0.Timeout := T#10s;
			COMStep := DISCONNECT_WAIT;
			
		DISCONNECT_WAIT:	
			IF (UA_Disconnect_0.Busy = 0) THEN
				UA_Disconnect_0.Execute := FALSE;
				IF (UA_Disconnect_0.Done = 1) THEN
					ErrorID:= 0;
					ConnectionHdl := 0;
				END_IF			
				IF (UA_Disconnect_0.Error = 1) THEN 
					ErrorID:= UA_Disconnect_0.ErrorID;
					COMStep := ERROR;
				END_IF
				COMStep := INIT_CONNECTION;
			END_IF	
			
		CONNECTION_ERROR:
			IF UA_ConnectionGetStatus_0.Execute = TRUE THEN
				IF ConnectionStatus.eConnectionStatus = UACS_Connected THEN
					// Die Verbindung konnte vom Client automatisch wieder komplett reastauriert werden (mit allen Subscriptions und MonitoredItems)
					COMStep := RUNNING;		
				END_IF
			END_IF
		ELSE

	END_CASE	

	UA_Connect_0();
	UA_ConnectionGetStatus_0(Execute := (ConnectionHdl <> 0), ConnectionHdl := ConnectionHdl , Timeout := T#10s);
	UA_GetNamespaceIndex_0();
	UA_NodeGetHandleListRead_0();
	UA_NodeGetHandleListWrite_0();
	UA_ReadList_0(Variables := Variables_Rd0);
	UA_WriteList_0(Variables:= Variables_Wr0);
	UA_NodeReleaseHandleReadList_0();
	UA_NodeReleaseHandleWriteList_0();
	UA_Disconnect_0();
	fbTrigger();
END_FUNCTION_BLOCK
