
{REDUND_ERROR} FUNCTION_BLOCK OPCUA_Client (*OPC UA Client*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_INPUT
		Execute : BOOL;
		Config : UA_Config_type;
	END_VAR
	VAR_OUTPUT
		OUT : UA_CLIENT_Out_Type;
	END_VAR
	VAR
		ConnectionHdl : DWORD := 0;
		SessionConnectInfo_0 : UASessionConnectInfo;
		ErrorID : DWORD;
		NamespaceIndex : UINT := 0;
		NodeHdlsRead : ARRAY[0..MAX_INDEX_NODELIST] OF DWORD;
		NodeIDsRead_0 : ARRAY[0..MAX_INDEX_NODELIST] OF UANodeID;
		NodeHdlsWrite : ARRAY[0..MAX_INDEX_NODELIST] OF DWORD;
		NodeIDsWrite_0 : ARRAY[0..MAX_INDEX_NODELIST] OF UANodeID;
		Variables_Rd0 : ARRAY[0..MAX_INDEX_NODELIST] OF STRING[255];
		Variables_Wr0 : ARRAY[0..MAX_INDEX_NODELIST] OF STRING[255];
		ConnectionStatus : ClientConnectionStatus_TYP;
		COMStep : USINT;
		NodeAddInfoRead_0 : ARRAY[0..MAX_INDEX_NODELIST] OF UANodeAdditionalInfo;
		NodeAddInfoWrite_0 : ARRAY[0..MAX_INDEX_NODELIST] OF UANodeAdditionalInfo;
		UA_Connect_0 : UA_Connect;
		UA_GetNamespaceIndex_0 : UA_GetNamespaceIndex;
		UA_NodeGetHandleListRead_0 : UA_NodeGetHandleList;
		UA_NodeGetHandleListWrite_0 : UA_NodeGetHandleList;
		UA_ConnectionGetStatus_0 : UA_ConnectionGetStatus;
		UA_ReadList_0 : UA_ReadList;
		UA_WriteList_0 : UA_WriteList;
		UA_NodeReleaseHandleReadList_0 : UA_NodeReleaseHandleList;
		UA_NodeReleaseHandleWriteList_0 : UA_NodeReleaseHandleList;
		UA_Disconnect_0 : UA_Disconnect;
		fbTrigger : TON;
	END_VAR
	VAR CONSTANT
		INIT_CONNECTION : USINT := 0;
		CONNECT : USINT := 1;
		CONNECT_WAIT : USINT := 2;
		GET_NAME_SPACE : USINT := 3;
		GET_NAME_SPACE_WAIT : USINT := 4;
		GET_HANDLE_READ_LIST : USINT := 5;
		GET_HANDLE_READ_LIST_WAIT : USINT := 6;
		GET_HANDLE_WRITE_LIST : USINT := 7;
		GET_HANDLE_WRITE_LIST_WAIT : USINT := 8;
		RUNNING : USINT := 10;
		READ_LIST : USINT := 11;
		READ_LIST_WAIT : USINT := 12;
		WRITE_LIST : USINT := 13;
		WRITE_LIST_WAIT : USINT := 14;
		NODE_RELEASE_HANDLE_RLIST : USINT := 20;
		NODE_RELEASE_HANDLE_RLIST_WAIT : USINT := 21;
		NODE_RELEASE_HANDLE_WLIST : USINT := 22;
		NODE_RELEASE_HANDLE_WLIST_WAIT : USINT := 23;
		DISCONNECT : USINT := 24;
		DISCONNECT_WAIT : USINT := 25;
		ERROR : USINT := 30;
		CONNECTION_ERROR : USINT := 31;
		BUSY : UINT := 65535;
	END_VAR
END_FUNCTION_BLOCK
