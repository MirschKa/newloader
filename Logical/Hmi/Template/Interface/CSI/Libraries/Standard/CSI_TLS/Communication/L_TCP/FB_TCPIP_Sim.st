//************************************************************************************************************************
// Company:     GEA Food Solutions
//------------------------------------------------------------------------------------------------------------------------
// Name:        FB_TCPIP_Sim
// Author:      Bart van Leuken 
// Version:     V1.00.0
// Date:        22-sep-2014
//------------------------------------------------------------------------------------------------------------------------
// Restriction: First UDINT of structure contains the actual structure size to compare with structure size on other side. 
//
//------------------------------------------------------------------------------------------------------------------------
// Description: Function block to handle simulation of TCP/IP communication (bi-directional)
//************************************************************************************************************************

FUNCTION_BLOCK FB_TCPIP_Sim
	
	//----------------------------------------------------------------------------------------------------------------------
  // Initialization.                                                                                             
	//----------------------------------------------------------------------------------------------------------------------
  IF (iConfig.Installed <> ioCmd.Status.Installed)
  THEN
    ioCmd.Status.Installed          := iConfig.Installed;
	  ioCmd.Status.State              := L_TCP_STATE_NO_REQUEST;
	  ioCmd.Status.Online             := FALSE;
    ioCmd.Status.Alarm              := FALSE;
	  ioCmd.AutoControl.Start         := FALSE;
	  PortModeOld                     := iConfig.Port.Mode;
	END_IF;

  IF NOT ioCmd.Status.Installed
  THEN
    RETURN;
  END_IF;
  
	//----------------------------------------------------------------------------------------------------------------------
	// Auto restart communication
	//----------------------------------------------------------------------------------------------------------------------
	// Change of IP Address or port number
	IF   (iSettings.IP_Address.Server <> ServerIP_AddressOld) // ServerIP_Address has changed
	  OR (iSettings.IP_Address.Client <> ClientIP_AddressOld) // ClientIP_Address has changed
	  OR (iConfig.Port.Mode <> PortModeOld) // ManualPortNumberControl has changed 
	THEN
	  AddressCalculation := TRUE; // (Re)generate IP Addresses and port numbers 
	END_IF;

	//----------------------------------------------------------------------------------------------------------------------
	// Address calculation.                                                                                                 
	//----------------------------------------------------------------------------------------------------------------------
	IF AddressCalculation THEN
	  // Manual port number generation 
	  IF (iConfig.Port.Mode = L_TCP_PORTCTRL_MANUAL) THEN
	    // Port number definition 
	    ServerPort := iConfig.Port.Manual.Server;
	    ClientPort := iConfig.Port.Manual.Client;
	  ELSE
	    // Automatic port number generation 
	    ClientPortOffset := 10 * FC_PORTGEN(iSettings.IP_Address.Client);
	    // Channel limits 
	    IF iConfig.Port.Automatic.Channel > 9 THEN
	      iConfig.Port.Automatic.Channel := 0;
	    END_IF;
	    // Port number definition 
	    ServerPort := L_TCP_SERVERPORT + ClientPortOffset + iConfig.Port.Automatic.Channel;
	    ClientPort := L_TCP_CLIENTPORT + ClientPortOffset + iConfig.Port.Automatic.Channel;
	  END_IF;
	  IF (iConfig.Communication.Mode = L_TCP_SERVER) THEN
	    Port := ServerPort;
	  ELSE
	    Port := ClientPort;
	  END_IF;
	  AddressCalculation := FALSE;
	END_IF;
	
	//----------------------------------------------------------------------------------------------------------------------
	// Start/stop handling (automatic control).                                                                             
	//----------------------------------------------------------------------------------------------------------------------
	IF ioCmd.AutoControl.Start AND ioCmd.AutoControl.Stop THEN
	  ioCmd.AutoControl.Start := FALSE;
	END_IF;
  
	//----------------------------------------------------------------------------------------------------------------------
	// Simulation
	//----------------------------------------------------------------------------------------------------------------------
  IF ioCmd.AutoControl.Stop
  THEN
    ioCmd.Status.State := L_TCP_STATE_NO_REQUEST;
  END_IF;
  IF ioCmd.AutoControl.Start
  THEN
    ioCmd.Status.State := L_TCP_STATE_SENDING_RECEIVING;
  END_IF;       

	//----------------------------------------------------------------------------------------------------------------------
	// Update status bits.                                                                                                  
	//----------------------------------------------------------------------------------------------------------------------
	oCommStatus.Online             := ioCmd.Status.Online;
	ioCmd.Status.Server_IP_Address := iSettings.IP_Address.Server;
	ioCmd.Status.Client_IP_Address := iSettings.IP_Address.Client;
	ioCmd.Status.ClientPort        := ClientPort;
	ioCmd.Status.ServerPort        := ServerPort;
  ioCmd.Status.Online            := (ioCmd.Status.State = L_TCP_STATE_SENDING_RECEIVING);
  
  IF (ioCmd.Status.State = L_TCP_STATE_NO_REQUEST)
  THEN
    oCommStatus.ReceiveOK := FALSE;
  END_IF;
	IF (ioCmd.Status.State = L_TCP_STATE_SENDING_RECEIVING)
  THEN
    oCommStatus.ReceiveOK := TRUE;
  END_IF;
	
	ioCmd.AutoControl.Start := FALSE;
	ioCmd.AutoControl.Stop  := FALSE;	
  
	ServerIP_AddressOld                := iSettings.IP_Address.Server;
	ClientIP_AddressOld                := iSettings.IP_Address.Client;
	PortModeOld                        := iConfig.Port.Mode;

	(*----------------------------------------------------------------------------------------------------------------------*)
	(* Timer.                                                                                                               *)
	(*----------------------------------------------------------------------------------------------------------------------*)
	IF (StateOld = ioCmd.Status.State) THEN
	  ioCmd.Status.StateElapsedTime := ioCmd.Status.StateElapsedTime + TimeBetweenBlockCalls;
	ELSE
	  ioCmd.Status.StateElapsedTime := 0;
	END_IF;	
	StateOld := ioCmd.Status.State;
	
END_FUNCTION_BLOCK