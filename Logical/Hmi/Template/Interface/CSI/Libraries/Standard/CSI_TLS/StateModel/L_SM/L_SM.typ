
TYPE
	T_SM : 	STRUCT 
		Status : T_SM_Sts;
		Block : T_SM_Block;
		Command : T_SM_Cmd;
		Transition : T_SM_Transition;
	END_STRUCT;
	T_SM_Sts : 	STRUCT 
		State : T_SM_State;
		StateElapsedTime : UDINT;
	END_STRUCT;
	T_SM_Block : 	STRUCT 
		Prepare : BOOL;
		Start : BOOL;
		Restart : BOOL;
		Reset : BOOL;
		Complete : BOOL;
	END_STRUCT;
	T_SM_Cmd : 	STRUCT 
		Prepare : BOOL;
		Start : BOOL;
		Stop : BOOL;
		Abort : BOOL;
		Hold : BOOL;
		Restart : BOOL;
		Reset : BOOL;
		Complete : BOOL;
	END_STRUCT;
	T_SM_Transition : 	STRUCT 
		NotAvailableToStopped : BOOL;
		StoppingToStopped : BOOL;
		PreparingToReady : BOOL;
		StandbyToRunning : BOOL;
		RunningToCompleting : BOOL;
		RunningToStandby : BOOL;
		HoldingToHeld : BOOL;
		RestartingToPrevious : BOOL;
		CompletingToCompleted : BOOL;
		AbortingToAborted : BOOL;
		ClearingToStopped : BOOL;
	END_STRUCT;
	T_SM_State : 
		(
		L_SM_STATE_NOT_AVAILABLE := 0,
		L_SM_STATE_STOPPING := 1,
		L_SM_STATE_STOPPED := 2,
		L_SM_STATE_PREPARING := 4,
		L_SM_STATE_READY := 8,
		L_SM_STATE_STANDBY := 16,
		L_SM_STATE_RUNNING := 32,
		L_SM_STATE_HOLDING := 64,
		L_SM_STATE_HELD := 128,
		L_SM_STATE_RESTARTING := 256,
		L_SM_STATE_COMPLETING := 512,
		L_SM_STATE_COMPLETED := 1024,
		L_SM_STATE_ABORTING := 2048,
		L_SM_STATE_ABORTED := 4096,
		L_SM_STATE_CLEARING := 8192
		);
END_TYPE
