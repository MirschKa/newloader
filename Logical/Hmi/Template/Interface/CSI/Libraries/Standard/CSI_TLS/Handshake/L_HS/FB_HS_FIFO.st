//************************************************************************************************************************
// Company:     GEA Food Solutions
//------------------------------------------------------------------------------------------------------------------------
// Name:        FB_HS_FIFO
// Author:      Joost Scheepers 
// Version:     V1.03.0
// Date:        29-sep-2014
//------------------------------------------------------------------------------------------------------------------------
// Restriction: 
//
//------------------------------------------------------------------------------------------------------------------------
// Description: Function block to buffer in case multiple handshakes in same time frame are required
//************************************************************************************************************************

FUNCTION_BLOCK FB_HS_FIFO
  LastParameterRecord := (SIZEOF(ParameterBuffer) / SIZEOF(ParameterBuffer[0])) - 1;
  LastDataRecord      := (SIZEOF(oData) / SIZEOF(oData[0])) - 1;
  
  IF iTrigger AND iParameter <> 0 AND Index <= LastParameterRecord
  THEN
    ParameterBuffer[Index] := iParameter;
    FOR j := 0 TO LastDataRecord DO
      DataBuffer[Index,j] := iData[j];
    END_FOR
    Index := Index + 1;
    iTrigger := FALSE;
  END_IF
  
  IF iHandshakeFinished
  THEN
    FOR i := 0 TO (LastParameterRecord - 1) DO
      ParameterBuffer[i]  := ParameterBuffer [i + 1];
      FOR j := 0 TO LastDataRecord DO
        DataBuffer[i,j]     := DataBuffer[i+1,j];
      END_FOR;
    END_FOR;
    IF Index > 0
    THEN
      Index := Index - 1;
    END_IF;
    //Reset last record
    ParameterBuffer[LastParameterRecord] := 0;
    FOR j := 0 TO LastDataRecord DO
      DataBuffer[LastParameterRecord,j]     := 0;
    END_FOR;
    
  END_IF;
  
  
  //Reset input variables in case they remain high
  iTrigger := FALSE;
  iHandshakeFinished := FALSE;
  
  oParameter := ParameterBuffer[0];
  FOR j := 0 TO LastDataRecord DO
    oData[j] := DataBuffer[0,j];
  END_FOR;
  
  oFull   := ( Index >= (LastParameterRecord-1) ); //leave one record free @full, so one message can still be added
  oEmpty  := ( Index = 0 );
END_FUNCTION_BLOCK