//************************************************************************************************************************
// Company:     GEA Food Solutions Bakel
//------------------------------------------------------------------------------------------------------------------------
// Name:        FB_CSI300_Mach_VC
// Author:      Bart van Leuken
// Version:     V1.01.0
// Date:        05-jul-2016
//------------------------------------------------------------------------------------------------------------------------
// Restriction: None 
//              
//------------------------------------------------------------------------------------------------------------------------
// Description: Function block to handle the HMI (Visual Components) of a machine for the CSI_STD interface
//************************************************************************************************************************

FUNCTION_BLOCK FB_CSI300_Mach_VC

  //----------------------------------------------------------------------------------------------------------------------
  // Initialization.
  //----------------------------------------------------------------------------------------------------------------------
  IF (iConfig.VC.Installed <> ioVC.Status.Installed)
  THEN
    ioVC.Status.Installed                                 := iConfig.VC.Installed;
    ioVC.Status.StatusDialog.StatusDatapoint.Tab_CSI      := 1;
    ioVC.Status.StatusDialog.StatusDatapoint.Tab_Recipe_1 := 1;
    ioVC.Status.StatusDialog.StatusDatapoint.Tab_Recipe_2 := 1;
  END_IF;

  IF NOT ioVC.Status.Installed
  THEN
    RETURN;
  END_IF;

  //----------------------------------------------------------------------------------------------------------------------
  // CSI TCP/IP Dialog button/dialog handling
  //----------------------------------------------------------------------------------------------------------------------
  IF (        (iActualPage = 1)       // Main page
      OR (    (iActualPage >= 1000)   // Production pages
          AND (iActualPage <  1099))) // Production pages
  THEN
    ioVC.Status.StatusDialog.StatusDatapoint.Button := 0;
  ELSE
    ioVC.Status.StatusDialog.StatusDatapoint.Button := 1;
  END_IF;

  IF ioVC.ManualControl.StatusDialog.SetOpen THEN
    ioVC.ManualControl.StatusDialog.TabSelection := 1;
    ioVC.ManualControl.StatusDialog.SetOpen := FALSE;
  END_IF;

  IF ioVC.ManualControl.StatusDialog.SetClose THEN
    ioVC.ManualControl.StatusDialog.TabSelection := 0;
    ioVC.ManualControl.StatusDialog.SetClose := FALSE;
  END_IF;

  CASE ioVC.ManualControl.StatusDialog.TabSelection OF   
    0 :  ioVC.Status.StatusDialog.StatusDatapoint.Tab_CSI      := 1; (* No dialog       *)
         ioVC.Status.StatusDialog.StatusDatapoint.Tab_Recipe_1 := 1;
         ioVC.Status.StatusDialog.StatusDatapoint.Tab_Recipe_2 := 1;
    1 :  ioVC.Status.StatusDialog.StatusDatapoint.Tab_CSI      := 0; (* CSI dialog      *)
         ioVC.Status.StatusDialog.StatusDatapoint.Tab_Recipe_1 := 1;
         ioVC.Status.StatusDialog.StatusDatapoint.Tab_Recipe_2 := 1;
    2 :  ioVC.Status.StatusDialog.StatusDatapoint.Tab_CSI      := 1; (* Recipe dialog 1 *)
         ioVC.Status.StatusDialog.StatusDatapoint.Tab_Recipe_1 := 0;
         ioVC.Status.StatusDialog.StatusDatapoint.Tab_Recipe_2 := 1;
    3 :  ioVC.Status.StatusDialog.StatusDatapoint.Tab_CSI      := 1; (* Recipe dialog 2 *)
         ioVC.Status.StatusDialog.StatusDatapoint.Tab_Recipe_1 := 1;
         ioVC.Status.StatusDialog.StatusDatapoint.Tab_Recipe_2 := 0;
  END_CASE;

  //----------------------------------------------------------------------------------------------------------------------
  // LineRecipe
  //----------------------------------------------------------------------------------------------------------------------
  ioVC.Status.LineRecipe.Header     := ioCSI_STD.In.Recipe.Header;
  ioVC.Status.LineRecipe.Operations := ioCSI_STD.In.Recipe.Operations;

  //----------------------------------------------------------------------------------------------------------------------
  // Status information
  //----------------------------------------------------------------------------------------------------------------------
  ioVC.Status.MachineMode        := ioCSI_STD.Out.Status.MachineMode;
  ioVC.Status.MachineState       := ioCSI_STD.Out.Status.MachineState;
  ioVC.Status.OperatingMode      := ioCSI_STD.Out.Status.OperatingMode;
  ioVC.Status.ProductionMode     := ioCSI_STD.Out.Status.ProductionMode;
  ioVC.Status.MachineCommand     := ioCSI_STD.In.Command.MachineCommand;
	IF     NOT (ioCSI_STD.Out.Status.OperatingMode = L_CSI300_OP_MODE_LOCAL)
	   AND NOT (ioCSI_STD.Out.Status.OperatingMode = L_CSI300_OP_MODE_MANUAL)
	THEN
	  IF (ioCSI_STD.In.Command.MachineCommand <> L_SM_CMD_NONE) THEN
	    ioVC.Status.LastMachineCommand := ioCSI_STD.In.Command.MachineCommand;
	  END_IF;
  ELSE
    ioVC.Status.LastMachineCommand := 0;
  END_IF;
  
  //----------------------------------------------------------------------------------------------------------------------
  // VC Control Modules
  //----------------------------------------------------------------------------------------------------------------------
//  FB_ProgramExistCheck_VC_0(
//    iInitialization := iInitialization, 
//    iEnable         := ioCmd.ProgramExistCheck.Status.Installed,
//    ioCmd           := ioCmd.ProgramExistCheck, 
//    ioVC            := ioVC.ProgramExistCheck);  
  
  //----------------------------------------------------------------------------------------------------------------------
  // Alarm handling
  //----------------------------------------------------------------------------------------------------------------------  
  IF iInitialization THEN     
    CM_AlarmOffset1 := 0; 
  END_IF;
  
  brsmemcpy( ADR(ioVC.ProgramExistCheck.Alarms.AcknowledgeImage), 
             ADR(ioVC.Alarms.AcknowledgeImage[CM_AlarmOffset1]), 
             SIZEOF(ioVC.ProgramExistCheck.Alarms.AcknowledgeImage));    
  
  brsmemcpy( ADR(ioVC.Alarms.AlarmImage[CM_AlarmOffset1]), 
             ADR(ioVC.ProgramExistCheck.Alarms.AlarmImage), 
             SIZEOF(ioVC.ProgramExistCheck.Alarms.AlarmImage));    

END_FUNCTION_BLOCK