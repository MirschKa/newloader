
TYPE
	T300_CSI_Aut_Cmd : 	STRUCT 
		Status : T300_CSI_Aut_Cmd_Sts;
		AutoControl : T300_CSI_Aut_Cmd_AutCtrl;
		Recipe : T300_CSI_Aut_Cmd_Rec;
	END_STRUCT;
	T300_CSI_Aut_Cmd_Sts : 	STRUCT 
		Installed : BOOL;
		TCPIP : T_TCPIP_Comm_Sts;
		CSI_Status : T300_CSI_STD_Out_Sts;
	END_STRUCT;
	T300_CSI_Aut_Cmd_AutCtrl : 	STRUCT 
		CSI_Command : T300_CSI_STD_In_Cmd;
	END_STRUCT;
	T300_CSI_Aut_Cmd_Rec : 	STRUCT 
		Status : T300_CSI_Aut_Cmd_Rec_Sts;
		AutoControl : T300_CSI_Aut_Cmd_Rec_AutCtrl;
		Handshake : T300_CSI_Aut_Cmd_Rec_HS;
	END_STRUCT;
	T300_CSI_Aut_Cmd_Rec_HS : 	STRUCT 
		Status : T_HS_Src_Cmd_Sts;
	END_STRUCT;
	T300_CSI_Aut_Cmd_Rec_Sts : 	STRUCT 
		Header : T300_CSI_STD_Gen_Rec_Header;
		HS_Feedback : UINT;
	END_STRUCT;
	T300_CSI_Aut_Cmd_Rec_AutCtrl : 	STRUCT 
		Recipe : T300_CSI_Aut_Cmd_Rec_AutCtrl_Rec;
		Program : T300_CSI_Aut_Cmd_Rec_AutCtrl_Prg;
	END_STRUCT;
	T300_CSI_Aut_Cmd_Rec_AutCtrl_Prg : 	STRUCT 
		ExistCheck : BOOL; (*Program number check if exists on machine*)
		Load : BOOL; (*Program number load on machine*)
		Number : UDINT; (*Program number to check or load on machine*)
	END_STRUCT;
	T300_CSI_Aut_Cmd_Rec_AutCtrl_Rec : 	STRUCT 
		Copy : BOOL; (*Recipe copy to machine*)
		Check : BOOL; (*Recipe check on machine (can be limits of values and if program exists)*)
		Data : T300_CSI_Aut_Cmd_Rec_Data;
	END_STRUCT;
	T300_CSI_Aut_Cmd_Rec_Data : 	STRUCT 
		Header : T300_CSI_STD_Gen_Rec_Header;
		Operations : ARRAY[0..31]OF T300_CSI_STD_Gen_Rec_Operations;
	END_STRUCT;
	T300_CSI_Aut_Rec_HS_Feedback : 
		(
		L_CSI300_HS_NOP := 0,
		L_CSI300_HS_REC_CHK_NOT_ENABLED := 1,
		L_CSI300_HS_REC_CHK_ERRORS := 2,
		L_CSI300_HS_REC_CHK_NO_ERRORS := 3,
		L_CSI300_HS_PROG_CHK_EXISTS := 4,
		L_CSI300_HS_PROG_CHK_NOT_EXISTS := 5,
		L_CSI300_HS_PROG_LOAD_OK := 6,
		L_CSI300_HS_PROG_LOAD_NOT_OK := 7,
		L_CSI300_HS_PROG_LOAD_DISABLED := 8,
		L_CSI300_HS_NOT_REMOTE_MODE := 9
		);
	T300_CSI_Aut_Config : 	STRUCT 
		Installed : BOOL;
		WorkPlaceID : ARRAY[0..3]OF UINT;
		CommunicationType : T300_CSI_CommunicationType;
	END_STRUCT;
	T300_CSI_Aut_Settings : 	STRUCT 
		TCPIP : T_TCPIP_Settings;
	END_STRUCT;
	T300_CSI_Mach_Cmd : 	STRUCT 
		Status : T300_CSI_Mach_Cmd_Sts;
		AutoControl : T300_CSI_Mach_Cmd_AutCtrl;
		Recipe : T300_CSI_Mach_Cmd_Rec;
		MachineRecipe : T300_CSI_Mach_Cmd_MachRec;
		OperatingData : T300_CSI_Mach_Cmd_OperData;
		Batch : T300_CSI_Mach_Cmd_Batch;
		ProgramExistCheck : T_ProgCheck_Cmd;
		Alarms : T300_CSI_Mach_Cmd_Alarms;
	END_STRUCT;
	T300_CSI_Mach_Cmd_MachRec : 	STRUCT 
		Data : T300_CSI_Mach_Cmd_MachRec_Data;
	END_STRUCT;
	T300_CSI_Mach_Cmd_MachRec_Data : 	STRUCT 
		Header : T300_CSI_Mach_Cmd_MachRec_Header;
	END_STRUCT;
	T300_CSI_Mach_Cmd_MachRec_Header : 	STRUCT 
		Number : UDINT;
		Name : STRING[20];
		Comment : STRING[40];
	END_STRUCT;
	T300_CSI_Mach_Cmd_OperData : 	STRUCT 
		Status : T300_CSI_Mach_Cmd_OperData_Sts;
	END_STRUCT;
	T300_CSI_Mach_Cmd_OperData_Sts : 	STRUCT 
		RunningTime : UDINT;
	END_STRUCT;
	T300_CSI_Mach_Cmd_Batch : 	STRUCT 
		Command : UINT;
		Data : ARRAY[0..31]OF T300_CSI_STD_Gen_Batch_OperData;
	END_STRUCT;
	T300_CSI_Mach_Cmd_Sts : 	STRUCT 
		Installed : BOOL;
		OperatingMode : UINT;
		MachineMode : UINT;
		MachineState : UDINT;
		ProductionMode : UDINT;
		CleaningMode : UDINT;
		Others : UINT;
		Alarm : UINT;
		AlarmActive : BOOL; (*Not set by FB*)
		TCPIP : T_TCPIP_Comm_Sts;
	END_STRUCT;
	T300_CSI_Mach_Cmd_AutCtrl : 	STRUCT 
		MachineMode : UINT;
		ProductionMode : UDINT;
		CleaningMode : UDINT;
		MachineCommand : T_SM_Cmd;
		Others : UINT;
		LampTest : BOOL;
	END_STRUCT;
	T300_CSI_Mach_Cmd_Alarms : 	STRUCT 
		ResetAllAlarms : BOOL;
		ActiveAlarm : ARRAY[0..15]OF BOOL;
		SetAlarm : ARRAY[0..15]OF BOOL;
		ResetAlarm : ARRAY[0..15]OF BOOL;
	END_STRUCT;
	T300_CSI_Mach_Cmd_Rec : 	STRUCT 
		Status : T300_CSI_Mach_Cmd_Rec_Sts;
		AutoControl : T300_CSI_Mach_Cmd_Rec_AutCtrl;
		Handshake : T300_CSI_Mach_Cmd_Rec_HS;
		Data : T300_CSI_Mach_Cmd_Rec_Data;
	END_STRUCT;
	T300_CSI_Mach_Cmd_Rec_AutCtrl : 	STRUCT 
		Update : BOOL;
		LoadAck : BOOL;
	END_STRUCT;
	T300_CSI_Mach_Cmd_Rec_HS : 	STRUCT 
		Status : T_HS_Dst_Cmd_Sts;
		AutoControl : T300_CSI_Mach_Cmd_Rec_HS_AutCtrl;
	END_STRUCT;
	T300_CSI_Mach_Cmd_Rec_HS_AutCtrl : 	STRUCT 
		Feedback : UINT;
	END_STRUCT;
	T300_CSI_Mach_Cmd_Rec_Data : 	STRUCT 
		Header : T300_CSI_STD_Gen_Rec_Header;
		Operations : ARRAY[0..31]OF T300_CSI_STD_Gen_Rec_Operations;
	END_STRUCT;
	T300_CSI_RecipeState : 
		(
		L_CSI300_RECSTATE_IDLE := 0, (*Idle*)
		L_CSI300_RECSTATE_PROGCALL_CHK := 1, (*Program call check*)
		L_CSI300_RECSTATE_PROGEXIST_CHK := 2, (*Program exist check*)
		L_CSI300_RECSTATE_REC_CHK_EXT_TR := 10, (*Trigger external recipe content check *)
		L_CSI300_RECSTATE_REC_CHK_EXT_FB := 11, (*Wait for recipe content checked (external)*)
		L_CSI300_RECSTATE_MACHREC_CHK := 20, (*Check machine recipe load disable and file exists*)
		L_CSI300_RECSTATE_MACHREC_LOAD := 21, (*Trigger machine recipe load*)
		L_CSI300_RECSTATE_MACHREC_FB := 22, (*Machine recipe loading*)
		L_CSI300_RECSTATE_COLLECT_FB := 90, (*Collect feedback*)
		L_CSI300_RECSTATE_READY := 99 (*Ready*)
		);
	T300_CSI_Mach_Cmd_Rec_Sts : 	STRUCT 
		Loaded : BOOL;
		RecipeContainsErrors : BOOL;
		RecipeContainsNoErrors : BOOL;
		MachineRecipeLoadDisable : BOOL;
	END_STRUCT;
	T300_CSI_Mach_Config : 	STRUCT 
		Installed : BOOL;
		General : T300_CSI_Config_General;
		Communication : T300_CSI_Config_Comm;
		HS_Recipe : T_HS_Dst_Config;
		VC : T300_CSI_Config_VC;
	END_STRUCT;
	T300_CSI_Config_Comm : 	STRUCT 
		TCPIP : BOOL;
	END_STRUCT;
	T300_CSI_Config_General : 	STRUCT 
		RemoteStartDisable : BOOL;
	END_STRUCT;
	T300_CSI_Config_VC : 	STRUCT 
		Installed : BOOL;
		Template_Version : USINT;
		StatusDialog : BOOL;
	END_STRUCT;
	T300_CSI_RecipeDataFeedback : 	STRUCT 
		DataCheckEnable : BOOL;
		DataContainsErrors : BOOL;
		DataContainsNoErrors : BOOL;
		CSI_Reserved_3 : BOOL;
		CSI_Reserved_4 : BOOL;
		CSI_Reserved_5 : BOOL;
		CSI_Reserved_6 : BOOL;
		CSI_Reserved_7 : BOOL;
		Machine_Reserved_8 : BOOL;
		Machine_Reserved_9 : BOOL;
		Machine_Reserved_10 : BOOL;
		Machine_Reserved_11 : BOOL;
		Machine_Reserved_12 : BOOL;
		Machine_Reserved_13 : BOOL;
		Machine_Reserved_14 : BOOL;
		Machine_Reserved_15 : BOOL;
	END_STRUCT;
	T300_CSI_DateTime : 	STRUCT 
		Data : DTStructure;
		Set : BOOL;
	END_STRUCT;
	T300_CSI_TPL : 	STRUCT 
		In : T300_CSI_TPL_In;
		Out : T300_CSI_TPL_Out;
	END_STRUCT;
	T300_CSI_TPL_In : 	STRUCT 
		AlarmHandling : T300_CSI_TPL_In_AlarmHandling;
		RecipeHandling : T300_CSI_TPL_In_RecipeHandling;
		SystemSettings : T300_CSI_TPL_In_SystemSettings;
	END_STRUCT;
	T300_CSI_TPL_In_AlarmHandling : 	STRUCT 
		ExtQuitAllAlarms : BOOL;
	END_STRUCT;
	T300_CSI_TPL_In_RecipeHandling : 	STRUCT 
		ExtRecipeLoad : BOOL;
		ExtRecipeNumber : UDINT;
	END_STRUCT;
	T300_CSI_TPL_In_SystemSettings : 	STRUCT 
		RemoteModeButtonColor : UINT;
	END_STRUCT;
	T300_CSI_TPL_Out : 	STRUCT 
		Identification : T300_CSI_TPL_Out_ID;
		Network : T300_CSI_TPL_Out_Network;
		SystemSettings : T300_CSI_TPL_Out_SystemSettings;
		Stat : T300_CSI_TPL_Out_Stat;
	END_STRUCT;
	T300_CSI_TPL_Out_ID : 	STRUCT 
		Machine : T300_CSI_STD_Out_ID_Machine;
		Operator : T300_CSI_STD_Out_ID_Operator;
	END_STRUCT;
	T300_CSI_TPL_Out_Network : 	STRUCT 
		EthernetAddressReady : BOOL;
		IP_Address : STRING[15];
		IP_AddressRemoteStation : STRING[15];
	END_STRUCT;
	T300_CSI_TPL_Out_SystemSettings : 	STRUCT 
		DateTimeActual : T300_CSI_STD_Out_DT;
		OperatingMode : UINT;
	END_STRUCT;
	T300_CSI_TPL_Out_Stat : 	STRUCT 
		GlobalStatusIndex : UINT;
	END_STRUCT;
	T300_CSI_VC : 	STRUCT 
		Status : T300_CSI_VC_Sts;
		ManualControl : T300_CSI_VC_ManCtrl;
		ProgramExistCheck : T_ProgCheck_VC;
		Alarms : T300_CSI_VC_Alarms;
	END_STRUCT;
	T300_CSI_VC_Alarms : 	STRUCT 
		AlarmImage : ARRAY[0..15]OF BOOL;
		AcknowledgeImage : ARRAY[0..15]OF BOOL;
	END_STRUCT;
	T300_CSI_VC_Dialog_Sts : 	STRUCT 
		StatusDatapoint : T300_CSI_VC_Dialog_Sts_Dp;
	END_STRUCT;
	T300_CSI_VC_Dialog_Sts_Dp : 	STRUCT 
		Button : UINT;
		Tab_CSI : UINT;
		Tab_Recipe_1 : UINT;
		Tab_Recipe_2 : UINT;
	END_STRUCT;
	T300_CSI_VC_ManCtrl : 	STRUCT 
		StatusDialog : T300_CSI_VC_ManCtrl_Dialog;
	END_STRUCT;
	T300_CSI_VC_ManCtrl_Dialog : 	STRUCT 
		SetOpen : BOOL;
		SetClose : BOOL;
		TabSelection : USINT; (*[1 = CSI, 2 = Recipe 1, 3 = Recipe 2]*)
	END_STRUCT;
	T300_CSI_VC_Recipe_Sts : 	STRUCT 
		Header : T300_CSI_STD_Gen_Rec_Header;
		Operations : ARRAY[0..31]OF T300_CSI_STD_Gen_Rec_Operations;
	END_STRUCT;
	T300_CSI_VC_Sts : 	STRUCT 
		Installed : BOOL;
		TCPIP : T300_CSI_VC_TCPIP_Sts;
		StatusDialog : T300_CSI_VC_Dialog_Sts;
		LineRecipe : T300_CSI_VC_Recipe_Sts;
		OperatingMode : UINT;
		MachineMode : UINT;
		MachineState : UDINT;
		ProductionMode : UDINT;
		MachineCommand : UINT;
		LastMachineCommand : UINT;
	END_STRUCT;
	T300_CSI_VC_TCPIP_Sts : 	STRUCT 
		CSI_STD_Online : BOOL;
		CSI_MMM1_Online : BOOL;
		CSI_MMM2_Online : BOOL;
	END_STRUCT;
	T300_CSI_CommunicationType : 
		(
		L_CSI300_COMM_TYPE_TCPIP := 0,
		L_CSI300_COMM_TYPE_OPC := 1
		);
	T300_CSI_Docu : 	STRUCT  (*Datatype is used for CSI Documentation*)
		Motor : UINT; (*Motor*)
		Valve : UINT; (*Valve*)
		General : UINT; (*General (system)*)
		SafetySwitch_GSZ : USINT; (*Safety switch (GSZ)*)
		PositionSwitch_GSB : USINT; (*Position switch (GSB)*)
		Transmitter : USINT; (*Transmitter (LT/PT/TT (Level/Pressure/Temperature)*)
	END_STRUCT;
END_TYPE
