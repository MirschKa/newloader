
FUNCTION_BLOCK FB_CSI300_Mach
	VAR_INPUT
		iInitialization : BOOL;
		iConfig : T300_CSI_Mach_Config;
	END_VAR
	VAR_IN_OUT
		ioCSI_STD : T300_CSI_STD;
		ioTPL : T300_CSI_TPL;
		ioCmd : T300_CSI_Mach_Cmd;
	END_VAR
	VAR
		CSI_STD_In_StructureSize : UDINT;
		CSI_STD_Out_StructureSize : UDINT;
		R_TRIG_RecipeCompleted : R_TRIG;
		SetDateTime : DATE_AND_TIME;
		DTSetTime : DTSetTime;
		ResetAlarmsHelpFlag : BOOL;
		Config_TCPIP : T_TCPIP_Config;
		Settings_TCPIP : T_TCPIP_Settings;
		Config_HS_Recipe : T_HS_Dst_Config;
		Recipe_HS_Cmd : T_HS_Dst_Cmd;
		Communication_Cmd : T_TCPIP_Cmd;
		i : UINT;
		RecipeLoad : BOOL;
		TON_RecipeLoadTimeOut : TON;
		TON_RecipeLoadDelay : TON;
		ProductionModeOld : UDINT;
		CleaningModeOld : UDINT;
		NewMachineCommand : BOOL;
		LastProductionMode : UDINT;
		OperatingModeOld : UINT;
		MachineCommandOld : UINT;
		NewStopCommand : BOOL;
		FB_HS_Dst_Recipe : FB_HS_Dst;
		FB_TCPIP_0 : FB_TCPIP;
		FB_ProgramExistCheck_0 : FB_ProgramExistCheck;
		DateAndTime : T300_CSI_DateTime;
		RecipeState : T300_CSI_RecipeState;
		RecipeStart : BOOL;
		RecipeMode : USINT;
		ProgramNumber : UDINT;
		ProgramFound : BOOL;
		Sequence_Recipe : FB_Sequencer;
		ExtQuitAllAlarms : BOOL;
		MachineRecipeComment : STRING[30];
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK FB_CSI300_Aut
	VAR_INPUT
		iInitialization : BOOL;
		iConfig : T300_CSI_Aut_Config;
		iSettings : T300_CSI_Aut_Settings;
	END_VAR
	VAR_IN_OUT
		ioCSI_STD : T300_CSI_STD;
		ioCmd : T300_CSI_Aut_Cmd;
	END_VAR
	VAR
		CSI_STD_In_StructureSize : UDINT;
		CSI_STD_Out_StructureSize : UDINT;
		Communication_Cmd : T_TCPIP_Cmd;
		FB_HS_Src_Recipe : FB_HS_Src;
		FB_TCPIP_0 : FB_TCPIP;
		Config_HS_Recipe : T_HS_Src_Config;
		Config_TCPIP : T_TCPIP_Config;
		Recipe_HS_Cmd : T_HS_Src_Cmd;
		FB_FilterRecipeOperations_0 : FB_FilterRecipeOperations;
		FillRecipeData : BOOL;
		CopyRecipeData : BOOL;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK FB_CSI300_Mach_VC
	VAR_INPUT
		iInitialization : BOOL;
		iConfig : T300_CSI_Mach_Config;
		iActualPage : UINT;
	END_VAR
	VAR_IN_OUT
		ioCSI_STD : T300_CSI_STD;
		ioCmd : T300_CSI_Mach_Cmd;
		ioVC : T300_CSI_VC;
	END_VAR
	VAR
		FB_ProgramExistCheck_VC_0 : FB_ProgramExistCheck_VC;
		CM_AlarmOffset1 : USINT;
	END_VAR
END_FUNCTION_BLOCK
