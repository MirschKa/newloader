
TYPE
	T300_CSI_STD : 	STRUCT 
		Out : T300_CSI_STD_Out;
		In : T300_CSI_STD_In;
	END_STRUCT;
	T300_CSI_STD_Gen_Batch_OperData : 	STRUCT 
		Status : UINT;
		Progress : UINT;
		Quantity : REAL;
		TEST : testtype;
	END_STRUCT;
	T300_CSI_STD_Gen_Rec_Header : 	STRUCT 
		RecipeName : ARRAY[0..39]OF UINT;
		RecipeVersion : UINT;
		RecipeComment : ARRAY[0..127]OF UINT;
		RecipeID : STRING[24];
		BatchID : STRING[24];
		BatchSize : REAL;
	END_STRUCT;
	T300_CSI_STD_Gen_Rec_Operations : 	STRUCT 
		SequenceOrder : UINT;
		SequencePath : UINT;
		Operation : UINT;
		Workplace : STRING[8];
		OperationParameters : STRING[32];
		MaterialNumber : STRING[24];
		MaterialName : ARRAY[0..39]OF UINT;
		MaterialQuantity : REAL;
		Tolerance : REAL;
	END_STRUCT;
	T300_CSI_STD_In : 	STRUCT 
		StructureSize : UDINT;
		Command : T300_CSI_STD_In_Cmd;
		DateAndTime : T300_CSI_STD_In_DT;
		Recipe : T300_CSI_STD_In_Rec;
		Batch : T300_CSI_STD_In_Batch;
	END_STRUCT;
	T300_CSI_STD_In_Batch : 	STRUCT 
		ActualSequenceOrder : UINT;
		ActualBatchSize : REAL;
		OperationData : ARRAY[0..31]OF T300_CSI_STD_Gen_Batch_OperData;
	END_STRUCT;
	T300_CSI_STD_In_Cmd : 	STRUCT 
		ResetAlarms : BOOL;
		OperatingMode : UINT;
		MachineMode : UINT;
		ProductionMode : UDINT;
		CleaningMode : UDINT;
		MachineCommand : UINT;
		Others : UINT;
	END_STRUCT;
	T300_CSI_STD_In_Rec : 	STRUCT 
		Header : T300_CSI_STD_Gen_Rec_Header;
		Operations : ARRAY[0..31]OF T300_CSI_STD_Gen_Rec_Operations;
		Handshake : T_HS;
	END_STRUCT;
	T300_CSI_STD_In_DT : 	STRUCT 
		Year : UINT;
		Month : USINT;
		Day : USINT;
		Hour : USINT;
		Minute : USINT;
		Second : USINT;
		Set : BOOL;
	END_STRUCT;
	T300_CSI_STD_Out : 	STRUCT 
		StructureSize : UDINT;
		Identification : T300_CSI_STD_Out_ID;
		Status : T300_CSI_STD_Out_Sts;
		DateAndTime : T300_CSI_STD_Out_DT;
		Recipe : T300_CSI_STD_Out_Rec;
		MachineRecipe : T300_CSI_STD_Out_MachRec;
		OperatingData : T300_CSI_STD_Out_OperatingData;
		Batch : T300_CSI_STD_Out_Batch;
	END_STRUCT;
	T300_CSI_STD_Out_Batch : 	STRUCT 
		ID : STRING[24];
		Command : UINT;
		OperationData : ARRAY[0..31]OF T300_CSI_STD_Gen_Batch_OperData;
	END_STRUCT;
	T300_CSI_STD_Out_DT : 	STRUCT 
		Year : UINT;
		Month : USINT;
		Day : USINT;
		DayOfWeek : USINT;
		Hour : USINT;
		Minute : USINT;
		Second : USINT;
	END_STRUCT;
	T300_CSI_STD_Out_ID : 	STRUCT 
		Interface : T300_CSI_STD_Out_ID_IF;
		Machine : T300_CSI_STD_Out_ID_Machine;
		Operator : T300_CSI_STD_Out_ID_Operator;
	END_STRUCT;
	T300_CSI_STD_Out_ID_IF : 	STRUCT 
		Version : STRING[24];
	END_STRUCT;
	T300_CSI_STD_Out_ID_Machine : 	STRUCT 
		Name : STRING[24];
		Type : STRING[24];
		Number : STRING[24];
		ID : STRING[24];
		CustomName : STRING[24];
		Line : STRING[24];
		Position : STRING[24];
		SoftwareVersion : STRING[24];
		MasterSoftware : STRING[24];
	END_STRUCT;
	T300_CSI_STD_Out_ID_Operator : 	STRUCT 
		ID : STRING[24];
		PasswordLevel : USINT;
	END_STRUCT;
	T300_CSI_STD_Out_Rec : 	STRUCT 
		Header : T300_CSI_STD_Gen_Rec_Header;
		Handshake : T_HS;
	END_STRUCT;
	T300_CSI_STD_Out_MachRec : 	STRUCT 
		Header : T300_CSI_STD_Out_MachRec_Header;
	END_STRUCT;
	T300_CSI_STD_Out_MachRec_Header : 	STRUCT 
		Number : UDINT;
		Name : ARRAY[0..19]OF UINT;
		Comment : ARRAY[0..29]OF UINT;
	END_STRUCT;
	T300_CSI_STD_Out_OperatingData : 	STRUCT 
		RunningTime : UDINT;
	END_STRUCT;
	T300_CSI_STD_Out_Sts : 	STRUCT 
		OperatingMode : UINT;
		MachineMode : UINT;
		MachineState : UDINT;
		ProductionMode : UDINT;
		CleaningMode : UDINT;
		Others : UINT;
		Alarm : UINT;
	END_STRUCT;
	testtype : 	STRUCT 
		HAHA : HAHAtype;
	END_STRUCT;
	HAHAtype : 	STRUCT 
		HIHI : HIHItype;
	END_STRUCT;
	HIHItype : 	STRUCT 
		New_Member : USINT;
		HOHO : HOHOtype;
	END_STRUCT;
	HOHOtype : 	STRUCT 
		New_Member3 : USINT;
		New_Member2 : USINT;
		New_Member1 : USINT;
		New_Member : USINT;
	END_STRUCT;
END_TYPE
