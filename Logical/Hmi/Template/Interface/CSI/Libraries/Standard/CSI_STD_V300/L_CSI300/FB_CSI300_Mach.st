//************************************************************************************************************************
// Company:     GEA Food Solutions Bakel
//------------------------------------------------------------------------------------------------------------------------
// Name:        FB_CSI300_Mach
// Author:      Bart van Leuken
// Version:     V1.03.0
// Date:        06-jan-2017
//------------------------------------------------------------------------------------------------------------------------
// Restriction: 
//              
//------------------------------------------------------------------------------------------------------------------------
// Description: Function block to handle the CSI_STD interface for a PLC controlled machine (to communication with an
//              upper system (like Automation).
//************************************************************************************************************************

FUNCTION_BLOCK FB_CSI300_Mach
	
//----------------------------------------------------------------------------------------------------------------------
// Initialization.                                                                                             
//----------------------------------------------------------------------------------------------------------------------
	IF iInitialization
		THEN
		ioCmd.Status.Installed := iConfig.Installed;

		(* Reset in/out Structures *)
		brsmemset(ADR(ioCSI_STD), 0, SIZEOF(ioCSI_STD));

		// Reset Outputs 
		ioCmd.AutoControl.MachineCommand.Prepare             := FALSE;
		ioCmd.AutoControl.MachineCommand.Start               := FALSE;
		ioCmd.AutoControl.MachineCommand.Stop                := FALSE;
		ioCmd.AutoControl.MachineCommand.Abort               := FALSE;
		ioCmd.AutoControl.MachineCommand.Hold                := FALSE;
		ioCmd.AutoControl.MachineCommand.Restart             := FALSE;
		ioCmd.AutoControl.MachineCommand.Reset               := FALSE;
		ioCmd.AutoControl.MachineCommand.Complete            := FALSE;

		// Reset local variables 
		ResetAlarmsHelpFlag                                              := FALSE;
		Communication_Cmd.Status.Online                                  := FALSE;
		Communication_Cmd.Status.Server_IP_Address                       := '';
		Communication_Cmd.Status.Client_IP_Address                       := '';
		Communication_Cmd.Status.ServerPort                              := 0;
		Communication_Cmd.Status.ClientPort                              := 0;
		Communication_Cmd.Status.State                                   := 0;
		Communication_Cmd.Status.StateElapsedTime                        := 0;
		RecipeLoad                                                       := FALSE;
		CSI_STD_Out_StructureSize                                        := SIZEOF(ioCSI_STD.Out);
		CSI_STD_In_StructureSize                                         := SIZEOF(ioCSI_STD.In);    
		ioCSI_STD.Out.Status.OperatingMode                               := L_CSI300_OP_MODE_MANUAL;
	END_IF;

	IF NOT ioCmd.Status.Installed
		THEN
		RETURN;
	END_IF;

	//----------------------------------------------------------------------------------------------------------------------
	// CSI_STD> Out> Structure size.                                                                                        
	//----------------------------------------------------------------------------------------------------------------------
	// Filled in by communication library L_TCP 

	//----------------------------------------------------------------------------------------------------------------------
	// CSI_STD> Out> Identification.                                                                                        
	//----------------------------------------------------------------------------------------------------------------------
	// Interface 
	ioCSI_STD.Out.Identification.Interface.Version                := L_CSI300_IF_VERSION;

	// Machine 
	ioCSI_STD.Out.Identification.Machine                          := ioTPL.Out.Identification.Machine;

	// Operator 
	ioCSI_STD.Out.Identification.Operator                         := ioTPL.Out.Identification.Operator;

	//----------------------------------------------------------------------------------------------------------------------
	// CSI_STD> Out: Status.                                                                                                
	//----------------------------------------------------------------------------------------------------------------------
	// Set Operating mode 
	IF (ioTPL.Out.SystemSettings.OperatingMode = L_CSI300_LC_MODE_LOCAL)
		THEN
		ioCmd.Status.OperatingMode   := L_CSI300_OP_MODE_LOCAL;
	ELSIF (OperatingModeOld = L_CSI300_LC_MODE_LOCAL) 
		AND (ioTPL.Out.SystemSettings.OperatingMode = L_CSI300_LC_MODE_REMOTE)
		THEN 
		ioCmd.Status.OperatingMode   := L_CSI300_OP_MODE_MANUAL;
	ELSIF (ioCSI_STD.In.Command.OperatingMode  = L_CSI300_OP_MODE_MANUAL)
		OR (ioCSI_STD.In.Command.OperatingMode = L_CSI300_OP_MODE_SEMI_AUTO)
		OR (ioCSI_STD.In.Command.OperatingMode = L_CSI300_OP_MODE_AUTO)
		THEN
		ioCmd.Status.OperatingMode   := ioCSI_STD.In.Command.OperatingMode;
	END_IF;
	OperatingModeOld := ioTPL.Out.SystemSettings.OperatingMode;

	// Operating mode 
	ioCSI_STD.Out.Status.OperatingMode  := ioCmd.Status.OperatingMode;

	// Machine mode 
	ioCSI_STD.Out.Status.MachineMode    := ioCmd.Status.MachineMode;

	// Machine state 
	ioCSI_STD.Out.Status.MachineState   := ioCmd.Status.MachineState;

	// Production mode 
	ioCSI_STD.Out.Status.ProductionMode := ioCmd.Status.ProductionMode;

	// Cleaning mode 
	ioCSI_STD.Out.Status.CleaningMode   := ioCmd.Status.CleaningMode; 

	// Others 
	ioCSI_STD.Out.Status.Others         := ioCmd.Status.Others;

	// Alarm 
	ioCSI_STD.Out.Status.Alarm          := ioCmd.Status.Alarm;

	//----------------------------------------------------------------------------------------------------------------------
	// CSI_STD> Out: Date and time.                                                                                         
	//----------------------------------------------------------------------------------------------------------------------
	ioCSI_STD.Out.DateAndTime           := ioTPL.Out.SystemSettings.DateTimeActual;

	//----------------------------------------------------------------------------------------------------------------------
	// CSI_STD> Out: Recipe.                                                                                           
	//----------------------------------------------------------------------------------------------------------------------
	IF ioCmd.Recipe.Status.Loaded
		THEN
		ioCSI_STD.Out.Recipe.Header := ioCSI_STD.In.Recipe.Header;
	END_IF;

	//----------------------------------------------------------------------------------------------------------------------
	// CSI_STD> Out: Machine recipe.                                                                                           
	//----------------------------------------------------------------------------------------------------------------------  
	// Clear data
	brsmemset(ADR(ioCSI_STD.Out.MachineRecipe.Header.Name),0,SIZEOF(ioCSI_STD.Out.MachineRecipe.Header.Name));
	brsmemset(ADR(ioCSI_STD.Out.MachineRecipe.Header.Comment),0,SIZEOF(ioCSI_STD.Out.MachineRecipe.Header.Comment)); 
	brsmemset(ADR(MachineRecipeComment),0,SIZEOF(MachineRecipeComment));
	// Convert data (string with length 40 to string with length 30) 
	brsmemcpy(ADR(MachineRecipeComment),ADR(ioCmd.MachineRecipe.Data.Header.Comment),SIZEOF(MachineRecipeComment)); 
	// Copy data
	ioCSI_STD.Out.MachineRecipe.Header.Number := ioCmd.MachineRecipe.Data.Header.Number;  
	brwcsconv(ADR(ioCSI_STD.Out.MachineRecipe.Header.Name),ADR(ioCmd.MachineRecipe.Data.Header.Name),0);  // Needed as template is not setup with UniCode
	brwcsconv(ADR(ioCSI_STD.Out.MachineRecipe.Header.Comment),ADR(MachineRecipeComment),0);  // Needed as template is not setup with UniCode

	//----------------------------------------------------------------------------------------------------------------------
	// CSI_STD> Out: Operating data.                                                                                        
	//----------------------------------------------------------------------------------------------------------------------
	ioCSI_STD.Out.OperatingData.RunningTime  := ioCmd.OperatingData.Status.RunningTime;

	//----------------------------------------------------------------------------------------------------------------------
	// CSI_STD> Out: Batch.                                                                                                 
	//----------------------------------------------------------------------------------------------------------------------
	ioCSI_STD.Out.Batch.ID                   := ioCSI_STD.Out.Recipe.Header.BatchID;
	ioCSI_STD.Out.Batch.Command              := ioCmd.Batch.Command;
	ioCSI_STD.Out.Batch.OperationData        := ioCmd.Batch.Data;  

	//----------------------------------------------------------------------------------------------------------------------
	// CSI_STD> In: Command.                                                                                                
	//----------------------------------------------------------------------------------------------------------------------
	IF NOT (ioCmd.Status.OperatingMode = L_CSI300_OP_MODE_LOCAL)
		THEN
		// Reset alarms -> template 
		IF ioCSI_STD.In.Command.ResetAlarms AND NOT ResetAlarmsHelpFlag
			THEN
			ExtQuitAllAlarms  := TRUE;
		ELSE
			ExtQuitAllAlarms  := FALSE;
		END_IF;
		ResetAlarmsHelpFlag := ioCSI_STD.In.Command.ResetAlarms;
	ELSE
		ExtQuitAllAlarms := FALSE;
		ioCSI_STD.In.Command.ResetAlarms := FALSE;
		//ioCSI_STD.In.Command.Others := ioCSI_STD.In.Command.Others AND 2#0000_0000_0000_0010; // Reset Others, except lamp test boolean 1    
	END_IF;

	IF     NOT (ioCmd.Status.OperatingMode = L_CSI300_OP_MODE_LOCAL)
		AND NOT (ioCmd.Status.OperatingMode = L_CSI300_OP_MODE_MANUAL)
		THEN
		// Operating mode 
		ioCSI_STD.In.Command.OperatingMode  := ioCSI_STD.In.Command.OperatingMode;

		// Compare last command with current 
		IF (ioCSI_STD.In.Command.MachineCommand = L_SM_CMD_STOP) AND (MachineCommandOld <> L_SM_CMD_STOP)
			THEN
			NewStopCommand     := TRUE;
		ELSIF (ioCSI_STD.In.Command.MachineCommand <> L_SM_CMD_STOP)
			THEN
			NewStopCommand     := FALSE;
		END_IF;

		// Reset NewStopCommand when production mode was changed 
		IF (ioCmd.Status.ProductionMode <> LastProductionMode)
			THEN
			LastProductionMode := ioCmd.Status.ProductionMode;
			NewStopCommand     := FALSE;
		END_IF;
		MachineCommandOld := ioCSI_STD.In.Command.MachineCommand;

		// Production mode 
		IF    (ioCmd.Status.MachineState = L_SM_STATE_STOPPED)
			OR (ioCmd.Status.MachineState = L_SM_STATE_NOT_AVAILABLE)
			OR (ioCmd.Status.MachineState = L_SM_STATE_ABORTED)
			OR (ProductionModeOld = 0)
			THEN
			IF (ioCSI_STD.In.Command.ProductionMode <> ProductionModeOld)
				THEN
				ioCmd.AutoControl.ProductionMode := ioCSI_STD.In.Command.ProductionMode;
				ProductionModeOld := ioCSI_STD.In.Command.ProductionMode;
			END_IF;
		END_IF;

		// Cleaning mode 
		IF    (ioCmd.Status.MachineState = L_SM_STATE_STOPPED)
			OR (ioCmd.Status.MachineState = L_SM_STATE_NOT_AVAILABLE)
			OR (ioCmd.Status.MachineState = L_SM_STATE_ABORTED)
			OR (CleaningModeOld = 0)
			THEN
			IF (ioCSI_STD.In.Command.CleaningMode <> CleaningModeOld)
				THEN
				ioCmd.AutoControl.CleaningMode := ioCSI_STD.In.Command.CleaningMode;
				CleaningModeOld := ioCSI_STD.In.Command.CleaningMode;
			END_IF;    
		END_IF;

		// Machine command 
		IF NOT iConfig.General.RemoteStartDisable
			THEN
			ioCmd.AutoControl.MachineCommand.Prepare  := ioCSI_STD.In.Command.MachineCommand.0;
			ioCmd.AutoControl.MachineCommand.Start    := ioCSI_STD.In.Command.MachineCommand.1;
			IF NewStopCommand
				THEN
				ioCmd.AutoControl.MachineCommand.Stop   := ioCSI_STD.In.Command.MachineCommand.2;
			ELSE
				ioCmd.AutoControl.MachineCommand.Stop   := FALSE;
			END_IF;
			ioCmd.AutoControl.MachineCommand.Abort    := ioCSI_STD.In.Command.MachineCommand.3;
			ioCmd.AutoControl.MachineCommand.Hold     := ioCSI_STD.In.Command.MachineCommand.4;
			ioCmd.AutoControl.MachineCommand.Restart  := ioCSI_STD.In.Command.MachineCommand.5;
			ioCmd.AutoControl.MachineCommand.Reset    := ioCSI_STD.In.Command.MachineCommand.6;
			ioCmd.AutoControl.MachineCommand.Complete := ioCSI_STD.In.Command.MachineCommand.7;
		ELSE 
			ioCmd.AutoControl.MachineCommand.Prepare  := FALSE;
			ioCmd.AutoControl.MachineCommand.Start    := FALSE;
			ioCmd.AutoControl.MachineCommand.Restart  := FALSE;
		END_IF;		
	ELSE
		ioCmd.AutoControl.MachineCommand.Prepare     := FALSE;
		ioCmd.AutoControl.MachineCommand.Start       := FALSE;
		ioCmd.AutoControl.MachineCommand.Stop        := FALSE;
		ioCmd.AutoControl.MachineCommand.Abort       := FALSE;
		ioCmd.AutoControl.MachineCommand.Hold        := FALSE;
		ioCmd.AutoControl.MachineCommand.Restart     := FALSE;
		ioCmd.AutoControl.MachineCommand.Reset       := FALSE;
		ioCmd.AutoControl.MachineCommand.Complete    := FALSE;
		ioCmd.AutoControl.MachineCommand.Reset       := FALSE;
		ioCSI_STD.In.Command.MachineMode    := 0;
		ioCSI_STD.In.Command.ProductionMode := 0;
		ioCSI_STD.In.Command.MachineCommand := 0;
		//ioCSI_STD.In.Command.Others         := ioCSI_STD.In.Command.Others AND 2#0000_0000_0000_0010; // Except LampTest
		NewMachineCommand                   := FALSE;
		LastProductionMode                  := 0;
		NewStopCommand                      := FALSE;
	END_IF;

	// Others
	ioCmd.AutoControl.LampTest := ioCSI_STD.In.Command.Others.L_CSI300_CTRL_OTHERS_LAMPTEST; // Bit 1 LampTest
	ioCmd.AutoControl.Others   := ioCSI_STD.In.Command.Others;

	//----------------------------------------------------------------------------------------------------------------------
	// CSI_STD> In: Date and time.                                                                                          
	//----------------------------------------------------------------------------------------------------------------------
	IF ioCSI_STD.In.DateAndTime.Set
		THEN
		DateAndTime.Data.year   := ioCSI_STD.In.DateAndTime.Year;
		DateAndTime.Data.month  := ioCSI_STD.In.DateAndTime.Month;
		DateAndTime.Data.day    := ioCSI_STD.In.DateAndTime.Day;
		DateAndTime.Data.hour   := ioCSI_STD.In.DateAndTime.Hour;
		DateAndTime.Data.minute := ioCSI_STD.In.DateAndTime.Minute;
		DateAndTime.Data.second := ioCSI_STD.In.DateAndTime.Second;
		DateAndTime.Set         := TRUE;
		SetDateTime             := DTStructure_TO_DT(ADR(DateAndTime.Data));
		DTSetTime(enable := TRUE, DT1 := SetDateTime);
		ioCSI_STD.In.DateAndTime.Set := FALSE;
	END_IF;

	//----------------------------------------------------------------------------------------------------------------------
	// CSI_STD> In: Recipe.                                                                                            
	//---------------------------------------------------------------------------------------------------------------------- 
	// Download enable  
	Recipe_HS_Cmd.AutoControl.DownloadEnable := NOT ioCmd.Recipe.Status.MachineRecipeLoadDisable;

	// Config  
	Config_HS_Recipe.DataCheck         := TRUE;
	Config_HS_Recipe.FastHandshake     := FALSE;
	Config_HS_Recipe.TimeOut           := T#30s;  

	// Handshake    
	FB_HS_Dst_Recipe(
	iEnable                              := ( ((Settings_TCPIP.Communication.Enable AND ioCmd.Status.TCPIP.Online) OR NOT Settings_TCPIP.Communication.Enable)
	AND NOT (ioCmd.Status.OperatingMode = L_CSI300_OP_MODE_LOCAL)
	AND NOT (ioCmd.Status.OperatingMode = L_CSI300_OP_MODE_MANUAL)),
	iInitialization                      := iInitialization,
	iConfig                              := Config_HS_Recipe, 
	iHandshakeIn                         := ioCSI_STD.In.Recipe.Handshake.Handshake,
	ioHandshakeOut                       := ioCSI_STD.Out.Recipe.Handshake.Handshake, 
	ioCmd                                := Recipe_HS_Cmd);

	R_TRIG_RecipeCompleted(CLK := Recipe_HS_Cmd.Status.DataTransferCompleted);

	IF R_TRIG_RecipeCompleted.Q
		THEN 
		ioCmd.Recipe.Data.Header     := ioCSI_STD.In.Recipe.Header;
		ioCmd.Recipe.Data.Operations := ioCSI_STD.In.Recipe.Operations;    

		CASE ioCSI_STD.In.Recipe.Handshake.Parameter OF

			L_CSI300_HS_PAR_RECIPE_COPY:
				RecipeStart := TRUE;
				RecipeMode  := L_CSI300_HS_PAR_RECIPE_COPY;

			L_CSI300_HS_PAR_PROG_EXISTCHECK:
				RecipeStart := TRUE;
				RecipeMode  := L_CSI300_HS_PAR_PROG_EXISTCHECK;
   
			L_CSI300_HS_PAR_PROG_LOAD:
				RecipeStart := TRUE;
				RecipeMode  := L_CSI300_HS_PAR_PROG_LOAD;     

		END_CASE;   
	END_IF;  

	//----------------------------------------------------------------------------------------------------------------------
	// Recipe stepper                                                                                            
	//---------------------------------------------------------------------------------------------------------------------- 
	CASE Sequence_Recipe.oStep OF

		L_CSI300_RECSTATE_IDLE: // Idle

			// Actions
			ProgramFound                := FALSE;
			ProgramNumber               := 0;    
			ioCmd.Recipe.Status.Loaded  := FALSE;

			// Transitions     
			IF RecipeStart
				THEN
				Sequence_Recipe.iNewStep := L_CSI300_RECSTATE_PROGCALL_CHK;
				RecipeStart := FALSE;
			END_IF;

		L_CSI300_RECSTATE_PROGCALL_CHK: // Program call check

			// Actions
			ioCmd.Recipe.Handshake.AutoControl.Feedback := 0;

			FOR i := 0 TO ( L_CSI300_RCP_OPERATIONS - 1 ) DO 
				IF (ioCmd.Recipe.Data.Operations[i].Operation = L_REC_OP_PROGRAM_AUTO_CALL)
					THEN
					ProgramFound  := TRUE;
					ProgramNumber := (brsatoi(ADR(ioCmd.Recipe.Data.Operations[i].OperationParameters)));        
					EXIT;
				END_IF;
			END_FOR; 

			// Transitions        
			IF ProgramFound
				THEN
				Sequence_Recipe.iNewStep := L_CSI300_RECSTATE_PROGEXIST_CHK;
			ELSE
				Sequence_Recipe.iNewStep := L_CSI300_RECSTATE_REC_CHK_EXT_TR;
			END_IF;

		L_CSI300_RECSTATE_PROGEXIST_CHK: // Program exist check

			// Actions 
			ioCmd.ProgramExistCheck.AutoControl.Check         := TRUE;
			ioCmd.ProgramExistCheck.AutoControl.ProgramNumber := ProgramNumber;        

			IF ioCmd.ProgramExistCheck.Status.Ready 
				THEN    
				IF ioCmd.ProgramExistCheck.Status.FileExists 
					THEN
					ioCmd.Recipe.Handshake.AutoControl.Feedback.L_CSI300_HS_FB_PROG_CHK_EXISTS   := TRUE;    
				ELSE
					ioCmd.Recipe.Handshake.AutoControl.Feedback.L_CSI300_HS_FB_PROG_CHK_NOTEXIST := TRUE;       
				END_IF;
				ioCmd.ProgramExistCheck.AutoControl.Check := FALSE;      
			END_IF;

			// Transitions
			IF ioCmd.ProgramExistCheck.Status.Ready 
				THEN
				IF (RecipeMode = L_CSI300_HS_PAR_RECIPE_COPY)
					THEN
					Sequence_Recipe.iNewStep := L_CSI300_RECSTATE_REC_CHK_EXT_TR;
				ELSIF (RecipeMode = L_CSI300_HS_PAR_PROG_LOAD)
					THEN
					Sequence_Recipe.iNewStep := L_CSI300_RECSTATE_MACHREC_CHK;
				ELSE
					Sequence_Recipe.iNewStep := L_CSI300_RECSTATE_COLLECT_FB;        
				END_IF;
			END_IF;     

		L_CSI300_RECSTATE_REC_CHK_EXT_TR: // Trigger external recipe content check 

			// Actions  
			IF iConfig.HS_Recipe.DataCheck 
				THEN
				ioCmd.Recipe.AutoControl.Update := TRUE;       
			ELSE
				ioCmd.Recipe.Handshake.AutoControl.Feedback.L_CSI300_HS_FB_REC_CHK_DISABLED := TRUE;              
			END_IF;     

			// Transitions  
			IF iConfig.HS_Recipe.DataCheck 
				THEN
				Sequence_Recipe.iNewStep := L_CSI300_RECSTATE_REC_CHK_EXT_FB;        
			ELSE
				Sequence_Recipe.iNewStep := L_CSI300_RECSTATE_COLLECT_FB; 
			END_IF; 

		L_CSI300_RECSTATE_REC_CHK_EXT_FB: // Wait for recipe content checked (external)

			// Actions    
			IF ioCmd.Recipe.Status.RecipeContainsErrors
				THEN
				ioCmd.Recipe.Handshake.AutoControl.Feedback.L_CSI300_HS_FB_REC_CHK_ERRORS := TRUE;
			ELSIF ioCmd.Recipe.Status.RecipeContainsNoErrors
				THEN
				ioCmd.Recipe.Handshake.AutoControl.Feedback.L_CSI300_HS_FB_REC_CHK_NO_ERRORS := TRUE;      
			END_IF;    

			// Transitions       
			IF   ioCmd.Recipe.Status.RecipeContainsErrors
				OR ioCmd.Recipe.Status.RecipeContainsNoErrors
				THEN
				Sequence_Recipe.iNewStep := L_CSI300_RECSTATE_COLLECT_FB; 
			END_IF;

		L_CSI300_RECSTATE_MACHREC_CHK: // Check machine recipe load disable and file exists

			// Actions  
			IF ioCmd.Recipe.Status.MachineRecipeLoadDisable
				THEN
				ioCmd.Recipe.Handshake.AutoControl.Feedback.L_CSI300_HS_FB_PROG_LOAD_DISABLE := TRUE;              
			END_IF;     

			// Transitions           
			IF ioCmd.Recipe.Handshake.AutoControl.Feedback.L_CSI300_HS_FB_PROG_LOAD_DISABLE
				THEN
				Sequence_Recipe.iNewStep := L_CSI300_RECSTATE_COLLECT_FB;       
			ELSIF ioCmd.Recipe.Handshake.AutoControl.Feedback.L_CSI300_HS_FB_PROG_CHK_EXISTS
				THEN
				Sequence_Recipe.iNewStep := L_CSI300_RECSTATE_MACHREC_LOAD;       
			ELSE
				Sequence_Recipe.iNewStep := L_CSI300_RECSTATE_COLLECT_FB;       
			END_IF;

		L_CSI300_RECSTATE_MACHREC_LOAD: // Trigger machine recipe load

			// Actions       
			RecipeLoad := TRUE;

			// Transitions                 
			Sequence_Recipe.iNewStep := L_CSI300_RECSTATE_MACHREC_FB;       

		L_CSI300_RECSTATE_MACHREC_FB: // Machine recipe loading

			// Actions
			IF RecipeLoad AND (ioCmd.MachineRecipe.Data.Header.Number = ProgramNumber)
				THEN
				ioCmd.Recipe.Handshake.AutoControl.Feedback.L_CSI300_HS_FB_PROG_LOAD_OK := TRUE;
				RecipeLoad := FALSE;
			END_IF;

			IF TON_RecipeLoadTimeOut.Q OR ioCmd.Recipe.Handshake.Status.TimeOut OR ioCmd.Recipe.Status.MachineRecipeLoadDisable
				THEN
				ioCmd.Recipe.Handshake.AutoControl.Feedback.L_CSI300_HS_FB_PROG_LOAD_NOT_OK := TRUE;
				RecipeLoad := FALSE;
			END_IF; 

			// Transitions       
			IF NOT RecipeLoad
				THEN
				Sequence_Recipe.iNewStep := L_CSI300_RECSTATE_COLLECT_FB; 
			END_IF;           

		L_CSI300_RECSTATE_COLLECT_FB: // Collect feedback 

			// Actions     
			ioCSI_STD.Out.Recipe.Handshake.Feedback  := ioCmd.Recipe.Handshake.AutoControl.Feedback;
			ioCSI_STD.Out.Recipe.Handshake.Parameter := ioCSI_STD.In.Recipe.Handshake.Parameter;
			IF   ioCmd.Recipe.Handshake.AutoControl.Feedback.L_CSI300_HS_FB_PROG_CHK_NOTEXIST
				OR ioCmd.Recipe.Handshake.AutoControl.Feedback.L_CSI300_HS_FB_REC_CHK_ERRORS
				THEN
				Recipe_HS_Cmd.AutoControl.DataIsNotOK := TRUE;
			ELSE
				Recipe_HS_Cmd.AutoControl.DataIsOK := TRUE;
				ioCmd.Recipe.Status.Loaded         := TRUE;
			END_IF;

			// Transitions        
			Sequence_Recipe.iNewStep := L_CSI300_RECSTATE_READY; 

		L_CSI300_RECSTATE_READY: // Handshake ready

			// Transitions     
			IF NOT Recipe_HS_Cmd.Status.Running
				THEN
				Sequence_Recipe.iNewStep := L_CSI300_RECSTATE_IDLE;       
			END_IF;

	END_CASE;

	// Step logger
	Sequence_Recipe(
		iInitialization := iInitialization
		);  

	//----------------------------------------------------------------------------------------------------------------------
	// Program exist check                                                                                            
	//---------------------------------------------------------------------------------------------------------------------- 
//	FB_ProgramExistCheck_0(
//	iEnable            := ((Settings_TCPIP.Communication.Enable AND ioCmd.Status.TCPIP.Online) OR NOT Settings_TCPIP.Communication.Enable), 
//	iInitialization    := iInitialization,
//	ioCmd              := ioCmd.ProgramExistCheck); 	

	//----------------------------------------------------------------------------------------------------------------------
	// Program Load                                                                                           
	//----------------------------------------------------------------------------------------------------------------------        
	// Recipe load time out 
	TON_RecipeLoadTimeOut(
		IN := RecipeLoad,
		PT := T#60s);

	// Recipe Load: delay 
	TON_RecipeLoadDelay(
	IN := NOT TON_RecipeLoadDelay.Q AND RecipeLoad,
	PT := T#30s);     

	//----------------------------------------------------------------------------------------------------------------------
	// TCP/IP Communication.                                                                                                
	//----------------------------------------------------------------------------------------------------------------------
	// Configuration
	Config_TCPIP.Installed                     := iConfig.Communication.TCPIP;
	Config_TCPIP.Communication.Mode            := L_TCP_CLIENT;
	Config_TCPIP.Communication.TimeOut         := T#10s;
	Config_TCPIP.Data.SendStructure            := ADR(ioCSI_STD.Out);
	Config_TCPIP.Data.SizeOfSendStructure      := SIZEOF(ioCSI_STD.Out);
	Config_TCPIP.Data.ReceiveStructure         := ADR(ioCSI_STD.In);
	Config_TCPIP.Data.SizeOfReceiveStructure   := SIZEOF(ioCSI_STD.In);

	Config_TCPIP.Port.Mode                     := L_TCP_PORTCTRL_AUTO;
	Config_TCPIP.Port.Automatic.Channel        := 0; 
	Config_TCPIP.Port.Manual.Client            := 0;
	Config_TCPIP.Port.Manual.Server            := 0;

	// Settings
	Settings_TCPIP.Communication.Enable        := ioTPL.Out.Network.EthernetAddressReady AND Config_TCPIP.Installed;
	Settings_TCPIP.IP_Address.Server           := ioTPL.Out.Network.IP_AddressRemoteStation;
	Settings_TCPIP.IP_Address.Client           := ioTPL.Out.Network.IP_Address;

	// Start/stop handling 
	IF Settings_TCPIP.Communication.Enable 
		THEN
		Communication_Cmd.AutoControl.Start := TRUE;
	ELSE
		Communication_Cmd.AutoControl.Stop  := TRUE;
	END_IF;

	// Communication 
	FB_TCPIP_0(
		iConfig       := Config_TCPIP,
		iSettings     := Settings_TCPIP,
		ioCmd         := Communication_Cmd);	

	//----------------------------------------------------------------------------------------------------------------------
	// Template
	//----------------------------------------------------------------------------------------------------------------------        
	ioTPL.In.AlarmHandling.ExtQuitAllAlarms       := ExtQuitAllAlarms;
	ioTPL.In.RecipeHandling.ExtRecipeLoad         := RecipeLoad AND (TON_RecipeLoadDelay.ET < T#2s);     
	ioTPL.In.RecipeHandling.ExtRecipeNumber       := ProgramNumber; 
	ioTPL.In.SystemSettings.RemoteModeButtonColor := ioCmd.Status.TCPIP.HMI_Color;

	//----------------------------------------------------------------------------------------------------------------------
	// Status information
	//----------------------------------------------------------------------------------------------------------------------
	ioCmd.Status.TCPIP                            := FB_TCPIP_0.oCommStatus;
	ioCmd.Recipe.Handshake.Status                 := Recipe_HS_Cmd.Status;
	RecipeState                                   := Sequence_Recipe.oStep;  

	//----------------------------------------------------------------------------------------------------------------------
	// Reset commands
	//----------------------------------------------------------------------------------------------------------------------
	ioCmd.Recipe.Status.RecipeContainsErrors      := FALSE;
	ioCmd.Recipe.Status.RecipeContainsNoErrors    := FALSE;      
             
END_FUNCTION_BLOCK