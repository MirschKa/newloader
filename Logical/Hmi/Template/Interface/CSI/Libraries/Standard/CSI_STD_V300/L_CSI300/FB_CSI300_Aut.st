//************************************************************************************************************************
// Company:     GEA Food Solutions Bakel
//------------------------------------------------------------------------------------------------------------------------
// Name:        FB_CSI300_Aut
// Author:      Bart van Leuken
// Version:     V1.03.0
// Date:        29-oct-2016
//------------------------------------------------------------------------------------------------------------------------
// Restriction: 
//              
//------------------------------------------------------------------------------------------------------------------------
// Description: Function block to handle the CSI_STD interface for GEA Automation (to communicate with a PLC controlled 
//              machine.
//************************************************************************************************************************

FUNCTION_BLOCK FB_CSI300_Aut

//----------------------------------------------------------------------------------------------------------------------
// Initialization.                                                                                             
//----------------------------------------------------------------------------------------------------------------------
	IF iInitialization
		THEN
		ioCmd.Status.Installed          := iConfig.Installed;

		(* Reset in/out Structures *)
		brsmemset(ADR(ioCSI_STD), 0, SIZEOF(ioCSI_STD));

		(* Reset local variables *)
		Communication_Cmd.Status.Online                   := FALSE;
		Communication_Cmd.Status.Server_IP_Address        := '';
		Communication_Cmd.Status.Client_IP_Address        := '';
		Communication_Cmd.Status.ServerPort               := 0;
		Communication_Cmd.Status.ClientPort               := 0;
		Communication_Cmd.Status.State                    := 0;
		Communication_Cmd.Status.StateElapsedTime           := 0;
		CSI_STD_Out_StructureSize                         := SIZEOF(ioCSI_STD.Out);
		CSI_STD_In_StructureSize                          := SIZEOF(ioCSI_STD.In);        
	END_IF;

	IF NOT ioCmd.Status.Installed
		THEN
		RETURN;
	END_IF;

	//----------------------------------------------------------------------------------------------------------------------
	// CSI_STD> In: Command.
	//----------------------------------------------------------------------------------------------------------------------
	ioCSI_STD.In.Command := ioCmd.AutoControl.CSI_Command;

	//----------------------------------------------------------------------------------------------------------------------
	// CSI_STD> In: Date and time
	//----------------------------------------------------------------------------------------------------------------------    
	// Handled by WonderWare
	IF ioCSI_STD.In.DateAndTime.Set 
		THEN 
		IF    ioCSI_STD.In.DateAndTime.Year   = ioCSI_STD.Out.DateAndTime.Year 
			AND ioCSI_STD.In.DateAndTime.Month  = ioCSI_STD.Out.DateAndTime.Month 
			AND ioCSI_STD.In.DateAndTime.Day    = ioCSI_STD.Out.DateAndTime.Day 
			AND ioCSI_STD.In.DateAndTime.Hour   = ioCSI_STD.Out.DateAndTime.Hour 
			AND ioCSI_STD.In.DateAndTime.Minute = ioCSI_STD.Out.DateAndTime.Minute 
			AND ioCSI_STD.In.DateAndTime.Second = ioCSI_STD.Out.DateAndTime.Second 
			THEN 
			ioCSI_STD.In.DateAndTime.Set := FALSE; 
		END_IF;   
	END_IF;   

	//----------------------------------------------------------------------------------------------------------------------
	// CSI_STD> In: Recipe.
	//----------------------------------------------------------------------------------------------------------------------
	// Triggers
	IF NOT ioCmd.Recipe.Handshake.Status.Running THEN
		IF    ioCmd.Recipe.AutoControl.Recipe.Copy
			THEN
			ioCSI_STD.In.Recipe.Handshake.Parameter    := L_CSI300_HS_PAR_RECIPE_COPY;  // Bit 0 RecipeCopy
			Recipe_HS_Cmd.AutoControl.Trigger          := TRUE;
			CopyRecipeData                             := TRUE;  
			ioCmd.Recipe.Status.HS_Feedback            := 0;  
		ELSIF ioCmd.Recipe.AutoControl.Program.Load
			AND ioCmd.Recipe.AutoControl.Program.Number > 0     
			THEN
			ioCSI_STD.In.Recipe.Handshake.Parameter    := L_CSI300_HS_PAR_PROG_LOAD;    // Bit 2 ProgramLoad
			Recipe_HS_Cmd.AutoControl.Trigger          := TRUE;
			FillRecipeData                             := TRUE; 
			ioCmd.Recipe.Status.HS_Feedback            := 0;  
		ELSIF ioCmd.Recipe.AutoControl.Program.ExistCheck
			AND ioCmd.Recipe.AutoControl.Program.Number > 0 
			THEN
			ioCSI_STD.In.Recipe.Handshake.Parameter    := L_CSI300_HS_PAR_PROG_EXISTCHECK;   // Bit 3 ProgramExistCheck
			Recipe_HS_Cmd.AutoControl.Trigger          := TRUE;   
			FillRecipeData                             := TRUE;      
			ioCmd.Recipe.Status.HS_Feedback            := 0;  
		END_IF;
	END_IF;   

	// Copy recipe Data
	IF CopyRecipeData
		THEN
		// Reset/clear variables
		Recipe_HS_Cmd.AutoControl.DataTransferCompleted := FALSE;  
		brsmemset( ADR(ioCSI_STD.In.Recipe.Operations), 0, SIZEOF(ioCSI_STD.In.Recipe.Operations));

		// Fill recipe data
		ioCSI_STD.In.Recipe.Header := ioCmd.Recipe.AutoControl.Recipe.Data.Header;

//		FB_FilterRecipeOperations_0(
//			iInitialization     := iInitialization,
//			iEnable             := TRUE,
//		iConfigWorkPlaceID  := iConfig.WorkPlaceID,
//		iRecipeOperations   := ioCmd.Recipe.AutoControl.Recipe.Data.Operations);

		(*ioCSI_STD.In.Recipe.Operations := FB_FilterRecipeOperations_0.oRecipeOperations;*)

		// Data transfer completed
		Recipe_HS_Cmd.AutoControl.DataTransferCompleted := TRUE;
		CopyRecipeData := FALSE;
	END_IF;  

	// Fill recipe Data
	IF FillRecipeData
		THEN  
		// Reset/clear variables
		Recipe_HS_Cmd.AutoControl.DataTransferCompleted := FALSE;      
		brsmemset( ADR(ioCSI_STD.In.Recipe.Operations), 0, SIZEOF(ioCSI_STD.In.Recipe.Operations));

		// Send only operation 100 with program number
		brsitoa(ioCmd.Recipe.AutoControl.Program.Number,ADR(ioCSI_STD.In.Recipe.Operations[0].OperationParameters));
		ioCSI_STD.In.Recipe.Operations[0].Operation := L_REC_OP_PROGRAM_AUTO_CALL;

		// Data transfer completed        
		Recipe_HS_Cmd.AutoControl.DataTransferCompleted := TRUE;
		FillRecipeData := FALSE;
	END_IF;

	// Download enable
	Recipe_HS_Cmd.AutoControl.DownloadEnable := 
	ioCmd.Status.TCPIP.Online AND NOT (ioCSI_STD.Out.Status.OperatingMode = L_CSI300_OP_MODE_LOCAL); // TO BE CHECKED -> COULD DIFFER PER PARAMETER

	// Config
	Config_HS_Recipe.FastHandshake      := FALSE;
	Config_HS_Recipe.TimeOut            := T#30s;

	// Handshake  
	FB_HS_Src_Recipe(
		iEnable                         := TRUE,
		iInitialization                 := iInitialization,  
		iConfig                           := Config_HS_Recipe,
	iHandshakeIn                      := ioCSI_STD.Out.Recipe.Handshake.Handshake,
	ioHandshakeOut                    := ioCSI_STD.In.Recipe.Handshake.Handshake,
	ioCmd                             := Recipe_HS_Cmd);

	IF Recipe_HS_Cmd.Status.HandshakeOK 
		THEN
		ioCmd.Recipe.Status.HS_Feedback := ioCSI_STD.Out.Recipe.Handshake.Feedback;
	ELSIF Recipe_HS_Cmd.Status.HandshakeNotOK 
		THEN
		ioCmd.Recipe.Status.HS_Feedback := 0;
		ioCmd.Recipe.Status.HS_Feedback.L_CSI300_HS_FB_PROG_HS_NOT_OK := TRUE;  
	END_IF;            

	//----------------------------------------------------------------------------------------------------------------------
	// TCP/IP Communication.                                                                                                
	//----------------------------------------------------------------------------------------------------------------------
	// Configuration
	IF iConfig.CommunicationType = L_CSI300_COMM_TYPE_TCPIP
		THEN
		Config_TCPIP.Installed                    := TRUE;
	ELSE
		Config_TCPIP.Installed                    := FALSE;
	END_IF
	Config_TCPIP.Communication.Mode             := L_TCP_SERVER;
	Config_TCPIP.Communication.TimeOut          := T#10s;
	Config_TCPIP.Data.SendStructure             := ADR(ioCSI_STD.In);
	Config_TCPIP.Data.SizeOfSendStructure       := SIZEOF(ioCSI_STD.In);
	Config_TCPIP.Data.ReceiveStructure          := ADR(ioCSI_STD.Out);
	Config_TCPIP.Data.SizeOfReceiveStructure    := SIZEOF(ioCSI_STD.Out);

	Config_TCPIP.Port.Mode                      := L_TCP_PORTCTRL_AUTO;
	Config_TCPIP.Port.Automatic.Channel         := 0; 
	Config_TCPIP.Port.Manual.Client             := 0;
	Config_TCPIP.Port.Manual.Server             := 0;

	// Start/stop handling 
	IF iSettings.TCPIP.Communication.Enable THEN
		Communication_Cmd.AutoControl.Start := TRUE;
	ELSE
		Communication_Cmd.AutoControl.Stop  := TRUE;
	END_IF;

	// Communication 
	FB_TCPIP_0(
		iConfig                    := Config_TCPIP,
	iSettings                    := iSettings.TCPIP,
	ioCmd                      := Communication_Cmd);

	//----------------------------------------------------------------------------------------------------------------------
	// Status
	//----------------------------------------------------------------------------------------------------------------------
	ioCmd.Status.TCPIP                        := FB_TCPIP_0.oCommStatus;
	ioCmd.Status.CSI_Status                     := ioCSI_STD.Out.Status;
	ioCmd.Recipe.Handshake.Status               := Recipe_HS_Cmd.Status;

	//----------------------------------------------------------------------------------------------------------------------
	// Reset Commands
	//----------------------------------------------------------------------------------------------------------------------  
	ioCmd.Recipe.AutoControl.Program.ExistCheck := FALSE;
	ioCmd.Recipe.AutoControl.Program.Load       := FALSE;
	ioCmd.Recipe.AutoControl.Recipe.Copy        := FALSE;
	ioCmd.Recipe.AutoControl.Recipe.Check       := FALSE;  

END_FUNCTION_BLOCK