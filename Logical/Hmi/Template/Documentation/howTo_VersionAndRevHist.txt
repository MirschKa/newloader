(*
This document describes how version numbers are assigned and the RevHist is maintained.


Construction of version numbers for tasks and libraries: Vx.yy.z

"x" is the main version of the task/ library.
With release of the new template V3 all tasks and template-libraries have V3.00.0. This digit should normally not be increased.

"yy" is the version of the task/ library.
Whenever a new version (with bugfixes, some new functions...) of the template will be released and a task is changed, the version (y digit) must be increased.

"z" is the develompent-version.
When a new template-version will be released ("yy" has changed), this digit must be 0! It can be used for template-developers while working on tasks. Even if the template is used in machines and little things are changed, this digit can be used.


Revision histories and header
Every task and library in the template has the same header (see sample below)

Tasks:
With release of template V3 all old revHists were moved to Package "Documentation", file "oldVersionHistoryTasks.txt".
Each task got a new header with V3.00.0

Libraries:
Wiht release of template V3 all old revHists of the FBKs and functions were moved to the files "oldRevHist.txt" in every(!) library.
Every library has a own file "revHist.txt". There is only documented in which FBK or function a change is made.
The exactly modification is documented in each FBK/ function.
Sample: A Library has 10 FBKs. 3 of them are changed. "yy" is incremented by 1. Then in "revHist.txt" it will be only documented in
which 3 FBKs changes were made. The exact changes are documented in each of the three FBKs. In the 7 unchanged FBKs for this version number is nothing documented.
*)




(* Sample header: *)

(************************************************************************************************************************)
(* Object name: dlg_IF                                                                                                  *)
(* Author:      Samuel Otto                                                                                             *)
(* Site:        B&R Bad Homburg                                                                                         *)
(* Created:     08-feb-2011                                                                                             *)
(* Restriction: consider task order.                                                                                    *)
(* Description: Opens one dialog with Interface.                                                                        *)
(*                                                                                                                      *)
(*----------------------------------------------------------------------------------------------------------------------*)
(* Additional information:                                                                                              *)
(*                                                                                                                      *)
(* No additional information available.                                                                                 *)
(*                                                                                                                      *)
(*----------------------------------------------------------------------------------------------------------------------*)
(* Version 3.00.0  27-sep-2011  Samuel Otto                     Development tool: B&R Automation Studio V3.0.81.27 SP05 *)
(*                                                                                                                      *)
(* Cleaned up this revision history and set version to V3.00.0                                                          *)
(*--------------------------------------------------------------------------------------------------------------------- *)
(************************************************************************************************************************)