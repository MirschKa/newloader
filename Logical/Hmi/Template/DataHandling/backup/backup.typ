
TYPE
	INT_Backup_Type : 	STRUCT 
		In : INT_BackupInType;
	END_STRUCT;
	INT_BackupInType : 	STRUCT 
		ActualPage : UINT;
		UserLevel : USINT;
		StartUpFinished : BOOL;
	END_STRUCT;
END_TYPE
