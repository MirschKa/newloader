(************************************************************************************************************************)
(* Object name: ExportData                                                                                              *)
(* Author:      Arthur Boosten, CFS                                                                                     *)
(* Site:        PAD System products Bakel                                                                               *)
(* Created:     10-Jun-2005                                                                                             *)
(* Restriction:                                                                                                         *)
(* Description: Reading out the logbook and and alarm and export to csv files.                                          *)
(*                                                                                                                      *)
(*----------------------------------------------------------------------------------------------------------------------*)
(* Additional information:                                                                                              *)
(*                                                                                                                      *)
(* No additional information available.                                                                                 *)
(*                                                                                                                      *)
(*----------------------------------------------------------------------------------------------------------------------*)
(* Version 3.00.0  18-jan-2012  Samuel Otto                     Development tool: B&R Automation Studio V3.0.90.19 SP01 *)
(*                                                                                                                      *)
(* Cleaned up this revision history and set version to V3.00.0                                                          *)
(*----------------------------------------------------------------------------------------------------------------------*)
(* Version 3.01.0  28-oct-2013  FTG                             Development tool: B&R Automation Studio V3.0.90.21 SP03 *)
(* in step DATA_RESET_DIALOG call ResetDialog only if the info-dialog was shown.                                        *)
(*--------------------------------------------------------------------------------------------------------------------- *)
(* Version 3.02.0  21-aug-2014  EC                             Development tool: B&R Automation Studio V3.0.90.28 SP10  *)
(* Issue List Item [#406] long respond time for backup/restore button			                                        *)
(*   - If fShowDilaog becomes true during Logbook and alarm procedure the Dialog for alarm export is displayed			*)
(*--------------------------------------------------------------------------------------------------------------------- *)
(************************************************************************************************************************)

FUNCTION_BLOCK ExportData
	
	IF EDGEPOS(fShowDialog) AND (bStepExport = DATA_LOGBOOK_READ OR bStepExport = DATA_ALARM_READ) THEN
		bNextStep 	:= bStepExport;
		bStepExport := DATA_SHOW_DIALOG;
	END_IF;
	(******************************************************************************************************************************)
	(* Exporting the logbook and alarmhistory data (from page 3202)														 		  *)
	(******************************************************************************************************************************)
	CASE bStepExport OF

		(**************************************************************************************************************************)
		(* EXPORT_INI (0)																										  *)
		(**************************************************************************************************************************)
		DATA_INI:
			IF fShowDialog THEN
				bStepExport := DATA_SHOW_DIALOG;
				bNextStep 	:= DATA_LOGBOOK_READ;
			ELSE
				bStepExport := DATA_LOGBOOK_READ;
			END_IF;
			wStatus := FBUSY;
		
		(**************************************************************************************************************************)
		(* EXPORT_SHOW_DIALOG (1) Show dialog: 'Exporting logbook and alarms...' 												  *)
		(**************************************************************************************************************************)
		DATA_SHOW_DIALOG:
				(* Prepare dialog text *)
				DialogInfo.HeaderTextGroupNumber:= TPL_DIALOG_HEADERTEXT;
				DialogInfo.HeaderTextNumber		:= DHT_ALARM_INFO;
				DialogInfo.DialogTextGroupNumber:= TPL_DIALOG_TEXT;
				DialogInfo.DialogTextNumber		:= DT_EXPORT_DATA;
				DialogInfo.NumberOfButton		:= 0;
				(* Call Dialog function *)
				StatusDialog:= CallDialog(ADR(DialogIdent), ADR(DialogInfo), dwDialogRingBuffer );

				IF ( StatusDialog = 500 ) THEN
					//bStepExport := DATA_LOGBOOK_READ;
					bStepExport := bNextStep;
				END_IF;
		(**************************************************************************************************************************)
		(* EXPORT_LOGBOOK_READ (2) Export logbook 																				  *)
		(**************************************************************************************************************************)
		DATA_LOGBOOK_READ:

				udtExportLogbook (fEnable:= 1, dwPath:= ADR('DATA'), dwFileName:= ADR ('LogBook.txt'),
								pLogBook := dwLogbook );

				(* Export logbook ready and ok, reset dialog *)
				IF udtExportLogbook.wStatus = FREADY_OK THEN
						bStepExport := DATA_ALARM_READ;
				(* Error not enough space on CF*)
				ELSIF  udtExportLogbook.wStatus = fiERR_SPACE THEN
					wBackupStatus := udtExportLogbook.wStatus;	(* Copy status from logbook read out function *)
					bStepExport := DATA_RESET_DIALOG;			(* reset dialog first*)
					bNextStep := DATA_INI;	(*go back to init*)
				(* Error in logbook, show dialog *)
				ELSIF udtExportLogbook.wStatus <> FBUSY THEN
						wBackupStatus := udtExportLogbook.wStatus;	(* Copy status from logbook read out function *)
						bStepExport := DATA_RESET_DIALOG;			(* reset dialog first*)
						bNextStep := DATA_ERROR_DIALOG;			(* next step after reset*)
				END_IF;

		(**************************************************************************************************************************)
		(* EXPORT_ALARM_READ (3) Call function for exporting alarmhistory 														  *)
		(**************************************************************************************************************************)
		DATA_ALARM_READ:

				(* Call function for exporting alarmhistory *)
				udtExportAlarm(fEnable:= 1, dwPath:= ADR('ALARMS'), dwFileName:=ADR('AlarmHistory.txt'),
										dwHandle := dwDataHandle );

				(* Exporting of alarms is ready *)
				IF ( udtExportAlarm.wStatus = FREADY_OK ) THEN
						bStepExport := DATA_RESET_DIALOG;
						bNextStep := DATA_INI;
						wBackupStatus := FREADY_OK;
				(* Error not enough space on CF*)
				ELSIF  udtExportAlarm.wStatus = fiERR_SPACE THEN
					wBackupStatus := udtExportAlarm.wStatus;	(* Copy status from logbook read out function *)
					bStepExport := DATA_RESET_DIALOG;			(* reset dialog first*)
					bNextStep := DATA_INI;	(*go back to init*)
				(* Error in read out alarm, show dialog *)
				ELSIF ( udtExportAlarm.wStatus <> FBUSY ) THEN
						wBackupStatus := udtExportAlarm.wStatus;	(* Copy status from alarm read out function *)
						bStepExport := DATA_RESET_DIALOG;			(* reset dialog first*)
						bNextStep := DATA_ERROR_DIALOG;			(* next step after reset*)
				END_IF;


		(**************************************************************************************************************************)
		(* EXPORT_ERROR_DIALOG (4) There was an error during operation, show dialog 											  *)
		(**************************************************************************************************************************)
		DATA_ERROR_DIALOG:

				(* Prepare dialog text *)
				DialogInfo.HeaderTextGroupNumber:= TPL_DIALOG_HEADERTEXT;
				DialogInfo.HeaderTextNumber		:= DHT_ERROR;
				DialogInfo.DialogTextGroupNumber:= TPL_DIALOG_TEXT;
				DialogInfo.DialogTextNumber		:= DT_EXPORT_ERROR;
				DialogInfo.ButtonTextGroupNumber:= TPL_DIALOG_BUTTONTEXT;
				DialogInfo.ButtonTextNumberOne	:= DBT_OK;
				DialogInfo.NumberOfButton		:= 1;
				(* Call Dialog function *)
				StatusDialog:= CallDialog(ADR(DialogIdent), ADR(DialogInfo), dwDialogRingBuffer );

				(* waiting for event button pressed *)
				IF (DialogInfo.Return_PressedButton = 1) THEN
					bStepExport := DATA_INI;
				END_IF


		(**************************************************************************************************************************)
		(* EXPORT_RESET_DIALOG (5) Reset dialog 																				  *)
		(**************************************************************************************************************************)
		DATA_RESET_DIALOG:
			IF fShowDialog THEN		(* if info-dialog was shown ... *)
				(* reset dialog *)
				StatusDialog := ResetDialog(ADR(DialogIdent), dwDialogRingBuffer);
	
				IF (StatusDialog = 0 ) THEN
					bStepExport := bNextStep;
					wStatus := wBackupStatus;
				END_IF
			ELSE		(* no info-dialog was shown, just return *)
				bStepExport := bNextStep;
				wStatus := wBackupStatus;
			END_IF
	END_CASE
END_FUNCTION_BLOCK
