
FUNCTION_BLOCK ExportLogbook
	VAR_INPUT
		fEnable : BOOL;
		dwPath : UDINT;
		dwFileName : UDINT;
		pLogBook : UDINT;
	END_VAR
	VAR_OUTPUT
		wStatus : UINT;
	END_VAR
	VAR
		bStep : USINT;
		udtFDelete : FileDelete;
		udtFCreate : FileCreate;
		udtFWrite : FileWrite;
		udtFClose : FileClose;
		dwIdent : UDINT;
		bEntryNo : USINT;
		dwOffsetLogBook : UDINT;
		wLenLogBook : UINT;
		strLogBookList : STRING[80];
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK ExportAlarm
	VAR_INPUT
		fEnable : BOOL;
		dwPath : UDINT;
		dwFileName : UDINT;
		dwHandle : UDINT;
	END_VAR
	VAR_OUTPUT
		wStatus : UINT;
		wNoAlarms : UINT;
	END_VAR
	VAR
		udtFDelete : FileDelete;
		udtFOpen : FileOpen;
		udtFCreate : FileCreate;
		udtFWrite : FileWrite;
		udtFClose : FileClose;
		bStep : USINT;
		dwAlarmOffset : UDINT;
		wLenAlarm : DINT;
		wAlarmListStatus : UINT;
		bSeparator : USINT;
		wFunction : UINT;
		AwAlarmList : ARRAY[0..149] OF UINT;
		bDateTimeFormat : USINT;
		dwIdent : UDINT; (*		strAlarmListLine	:STRING[150];						outcommented because not used FTG 16.12.2010	*)
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK ExportData
	VAR_INPUT
		dwDialogRingBuffer : UDINT;
		dwLogbook : UDINT;
		dwDataHandle : UDINT;
		fShowDialog : BOOL;
	END_VAR
	VAR_OUTPUT
		wStatus : UINT;
	END_VAR
	VAR
		DialogIdent : UDINT;
		bStepExport : USINT;
		bNextStep : USINT;
		DialogInfo : dialoginfo;
		StatusDialog : UINT;
		wBackupStatus : UINT;
		udtExportLogbook : ExportLogbook;
		udtExportAlarm : ExportAlarm;
		zzEdge00000 : BOOL;
	END_VAR
END_FUNCTION_BLOCK
