(************************************************************************************************************************)
(* Object name: ExportAlarm                                                                                             *)
(* Author:      Arthur Boosten, CFS                                                                                     *)
(* Site:        PAD System products Bakel                                                                               *)
(* Created:     10-Jun-2005                                                                                             *)
(* Restriction: consider task order.                                                                                    *)
(* Description: Reading out the alarm list and export to a csv file.                                                    *)
(*                                                                                                                      *)
(*----------------------------------------------------------------------------------------------------------------------*)
(* Additional information:                                                                                              *)
(*                                                                                                                      *)
(* No additional information available.                                                                                 *)
(*                                                                                                                      *)
(*----------------------------------------------------------------------------------------------------------------------*)
(* Version 3.00.0  18-jan-2012  Samuel Otto                     Development tool: B&R Automation Studio V3.0.90.19 SP01 *)
(*                                                                                                                      *)
(* Cleaned up this revision history and set version to V3.00.0                                                          *)
(*--------------------------------------------------------------------------------------------------------------------- *)
(************************************************************************************************************************)

FUNCTION_BLOCK ExportAlarm
(* Implementation of ExportAlarm *)

	(* If enable is false then reset step *)
	IF ( fEnable = FALSE ) THEN
		bStep := 0;
	END_IF;

	CASE bStep OF

		(**************************************************************************************************************)
		(* ALARMEX_IDLE (step 0) waiting for the trigger to export the alarmhistory									  *)
		(**************************************************************************************************************)
		ALARM_IDLE:
				IF (fEnable = TRUE) THEN
					wStatus := FBUSY;				(* Function is busy *)
					bStep := ALARM_SELECT_FIRST;
					wNoAlarms := 0;
				END_IF;

		(**************************************************************************************************************)
		(* SELECT FIRST (step 1) select the first alarm from the list and go to next step							  *)
		(**************************************************************************************************************)
		ALARM_SELECT_FIRST:
				wFunction:= READ_FIRST_ALARM;	(* Select first alarm to be read *)
				dwAlarmOffset := 0;				(* Alarm offset is used as offset to write in the output file *)
				bStep:= ALARM_DELETE_FILE;		(* Select next step *)


		(**************************************************************************************************************)
		(* DELETE FILE (step 2) 																					  *)
		(**************************************************************************************************************)
		ALARM_DELETE_FILE:
				(* Call function to delete output file *)
				udtFDelete(enable := 1, pDevice:= dwPath , pName:= dwFileName );

				(* If file is not existing or deleted goto open file *)
				IF (udtFDelete.status = fiERR_FILE_NOT_FOUND ) OR (udtFDelete.status = 0) THEN
						bStep := ALARM_CREATE_FILE;
				(* If there was an error, copy error no, and leave function *)
				ELSIF (udtFDelete.status <> FBUSY ) THEN
						wStatus := udtFDelete.status;
						bStep := ALARM_IDLE;
				END_IF;

		(**************************************************************************************************************)
		(* CREATE FILE (step 3) 																					  *)
		(**************************************************************************************************************)
		ALARM_CREATE_FILE:
				(* Call function to create output file *)
				udtFCreate ( enable := 1, pDevice := dwPath, pFile := dwFileName );

				(* identifier is needed when writing to file *)
				dwIdent := udtFCreate.ident;
				(* Verify status *)
				IF ( udtFCreate.status = 0) THEN
					bStep := ALARM_READ_NEXT_ALARM;
				ELSIF ( udtFCreate.status <> FBUSY ) THEN
					wStatus := udtFCreate.status;
					bStep := ALARM_IDLE;
				END_IF;


		(**************************************************************************************************************)
		(* OPEN FILE (step 4) 																						  *)
		(**************************************************************************************************************)
		ALARM_OPEN_FILE:
				(* Call function to open file *)
				udtFOpen (enable := 1, pDevice := dwPath, pFile := dwFileName, mode := FILE_RW );

				(* Copy identifier, this is needed when writing to file *)
				dwIdent := udtFOpen.ident;
				(* Write data to file *)
				IF (udtFOpen.status = 0) THEN
					bStep := ALARM_READ_NEXT_ALARM;
				(* If an error occurs while opening, copy error number and go leave fucntion *)
				ELSIF (udtFOpen.status <> FBUSY) THEN
					wStatus := udtFOpen.status;
					bStep := ALARM_IDLE;
				END_IF;

		(**************************************************************************************************************)
		(* READ NEXT ALARM (step 5) 																			  	  *)
		(**************************************************************************************************************)
		ALARM_READ_NEXT_ALARM:
				(* If access is ok, then read out alarm *)
				IF ( ( VA_Saccess (1,dwHandle)) = 0 ) THEN
						wLenAlarm := USINT_TO_DINT(ALARM_LENGHT);
						bSeparator := STR_SEMICOLON;
						bDateTimeFormat := 0;
						wAlarmListStatus := VA_wcGetExAlarmList(1, dwHandle, ADR(AwAlarmList),
											ADR(wLenAlarm), wFunction, bSeparator, bDateTimeFormat );

					(* Release the connection *)
					VA_Srelease(1,dwHandle);

					(*  Terminate string with CR + LF! *)
					AwAlarmList[wLenAlarm]:= 16#000D;
					AwAlarmList[wLenAlarm + 1]:= 16#000A;
					AwAlarmList[wLenAlarm + 2]:= 16#0000;

					(* Check if last alarm (240)is read out, if yes goto idle step else write data to csv *)
					IF wAlarmListStatus = LAST_ALARM THEN
						bStep := ALARM_CLOSE_FILE;
					ELSIF (wAlarmListStatus = NEXT_ALARM) THEN
						bStep := ALARM_WRITE_TO_FILE;
						wFunction := READ_NEXT_ALARM;
					ELSIF (wAlarmListStatus = ALARM_SYSTEM_BUSY) THEN (* alarmsystem is busy (status 247) - call again *)
						bStep := ALARM_READ_NEXT_ALARM;
						wStatus := FBUSY; (* patch output status to "busy" to get caller informed that he has to wait and try again *)
					ELSE											(* There was an error, copy error and leave function *)
						wStatus := wAlarmListStatus;
						bStep := ALARM_IDLE;
					END_IF;
				END_IF;

		(**************************************************************************************************************)
		(* WRITE_TO_FILE (step 6) 																					  *)
		(**************************************************************************************************************)
		ALARM_WRITE_TO_FILE:

				(* Call function to write data to the output file *)
				udtFWrite(enable:= 1, ident:= dwIdent, offset:= dwAlarmOffset, pSrc:=ADR (AwAlarmList),
					len:= ( (wLenAlarm + 2)*2) );


				(* If function is OK, goto next step *)
				IF (udtFWrite.status = 0) THEN
					bStep := ALARM_READ_NEXT_ALARM;
					dwAlarmOffset := dwAlarmOffset + ((wLenAlarm + 2)*2);
					wNoAlarms := wNoAlarms + 1;											(* COUNTING NUMBER OF ALARMS *)
				(* If there is an error, copy errornumber and leave function *)
				ELSIF (udtFWrite.status <> FBUSY )THEN
					wStatus := udtFWrite.status;
					bStep := ALARM_IDLE;
				END_IF;


		(**************************************************************************************************************)
		(* CLOSE FILE (step 7) 																					  	  *)
		(**************************************************************************************************************)
		ALARM_CLOSE_FILE:
				(* Call function to close file *)
				udtFClose(enable:=1 , ident:= dwIdent);

				(* Closing is OK, goto ALARMEX_READ_ALARM *)
				IF (udtFClose.status = 0) THEN
					bStep := ALARM_IDLE;
					wStatus := FREADY_OK;
				(* if there is an error, copy error number and leave function *)
				ELSIF (udtFClose.status <> FBUSY) THEN
					wStatus := udtFClose.status;
					bStep := ALARM_IDLE;
				END_IF;

	END_CASE
END_FUNCTION_BLOCK
