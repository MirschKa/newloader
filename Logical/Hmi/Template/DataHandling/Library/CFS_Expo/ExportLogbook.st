(************************************************************************************************************************)
(* Object name: ExportLogbook                                                                                           *)
(* Author:      Arthur Boosten, CFS                                                                                     *)
(* Site:        PAD System products Bakel                                                                               *)
(* Created:     10-Jun-2005                                                                                             *)
(* Restriction:                                                                                                         *)
(* Description: Reading out the logbook and export to a csv file.                                                       *)
(*                                                                                                                      *)
(*----------------------------------------------------------------------------------------------------------------------*)
(* Additional information:                                                                                              *)
(*                                                                                                                      *)
(* No additional information available.                                                                                 *)
(*                                                                                                                      *)
(*----------------------------------------------------------------------------------------------------------------------*)
(* Version 3.00.0  18-jan-2012  Samuel Otto                     Development tool: B&R Automation Studio V3.0.90.19 SP01 *)
(*                                                                                                                      *)
(* Cleaned up this revision history and set version to V3.00.0                                                          *)
(*--------------------------------------------------------------------------------------------------------------------- *)
(************************************************************************************************************************)

FUNCTION_BLOCK ExportLogbook
	(* If enable is false then reset step *)
	IF ( fEnable = FALSE ) THEN
		bStep := 0;
	END_IF;

	CASE bStep OF

		(**************************************************************************************************************)
		(* LOGBOOK_HANDLING (step 0) 																			  	  *)
		(**************************************************************************************************************)
		LOGBOOK_INI:

			IF (fEnable = TRUE) THEN
				bStep := LOGBOOK_DELETE;
				wStatus := FBUSY;				(* Function is busy *)
			END_IF;

		(**************************************************************************************************************)
		(* LOGBOOK_DELETE(step 1) 																			 	 	  *)
		(**************************************************************************************************************)
		LOGBOOK_DELETE:

			udtFDelete(enable:=1, pDevice:= dwPath, pName:= dwFileName );
			(* If file is not existing or deleted goto open file *)
			IF (udtFDelete.status = fiERR_FILE_NOT_FOUND ) OR (udtFDelete.status = 0) THEN
				bStep := LOGBOOK_CREATE;
			(* If error then copy status and goto LOGBOOK_END *)
			ELSIF (udtFDelete.status <> FBUSY ) THEN
				wStatus := udtFDelete.status;
				bStep := LOGBOOK_INI;
			END_IF

		(**************************************************************************************************************)
		(* LOGBOOK_CREATE (step 2) 																				  	  *)
		(**************************************************************************************************************)
		LOGBOOK_CREATE:

			udtFCreate(enable:=1, pDevice:=dwPath, pFile:=dwFileName );
			(* identifier is needed when writing to file *)
			dwIdent := udtFCreate.ident;
			(* If ready then goto step LOGBOOK_PREPARE_WRITE *)
			IF ( udtFCreate.status = 0) THEN
				bStep := LOGBOOK_PREPARE_WRITE;
				bEntryNo := 0;
				dwOffsetLogBook := 0;
			(* If error then copy status and goto LOGBOOK_INI *)
			ELSIF ( udtFCreate.status <> FBUSY ) THEN
				wStatus := udtFCreate.status;
				bStep := LOGBOOK_INI;
			END_IF


		(**************************************************************************************************************)
		(* LOGBOOK_PREPARE_WRITE (step 3) 																			  *)
		(**************************************************************************************************************)
		LOGBOOK_PREPARE_WRITE:

			(* Preparation off the string to be writen. *)
			brsstrcpy(ADR (strLogBookList) , (pLogBook + (LOGBOOKOFFSET * bEntryNo) ) );
			brsstrcat(ADR(strLogBookList), ADR(STR_NEWLINE) );
			wLenLogBook := UDINT_TO_UINT(brsstrlen( ADR (strLogBookList) ));
			bStep := LOGBOOK_WRITE;

		(**************************************************************************************************************)
		(* LOGBOOK_WRITE (step 4) 																				  	  *)
		(**************************************************************************************************************)
		LOGBOOK_WRITE:

			udtFWrite(enable:= 1, ident:= dwIdent, offset:= dwOffsetLogBook, pSrc:=ADR (strLogBookList),
					len:= wLenLogBook ) ;
			(* If ready goto LOGBOOK_WRITE_NEXT *)
			IF (udtFWrite.status = 0) THEN
				bStep := LOGBOOK_WRITE_NEXT;
				dwOffsetLogBook :=  dwOffsetLogBook + wLenLogBook;
			(* If status is error, copy status and go to step LOGBOOKINI *)
			ELSIF (udtFWrite.status <> FBUSY ) THEN
				wStatus := udtFWrite.status;
				bStep := LOGBOOK_INI;
			END_IF

		(**************************************************************************************************************)
		(* LOGBOOK_WRITE_NEXT (step 5) 																				  *)
		(**************************************************************************************************************)
		LOGBOOK_WRITE_NEXT:
			(* If not all data copied, go back step LOGBOOK_PREPARE_WRITE *)
			IF bEntryNo < MAX_NO_ENTRIES THEN
				bEntryNo := bEntryNo + 1;
				bStep := LOGBOOK_PREPARE_WRITE;
			(* If all data is copied, goto LOGBOOK (close file....) *)
			ELSE
				bStep := LOGBOOK_CLOSE;
			END_IF

		(**************************************************************************************************************)
		(* LOGBOOK_CLOSE (step 6) 																				  	  *)
		(**************************************************************************************************************)
		LOGBOOK_CLOSE:

			udtFClose(enable:=1, ident:= dwIdent );
			(* If status is ready go back to step LOGBOOK_INI *)
			IF (udtFClose.status = 0) THEN
				wStatus := FREADY_OK;
				bStep := LOGBOOK_INI;
			(* If status is error, copy errorcode and go to step LOGBOOK_INI *)
			ELSIF ( udtFClose.status <> FBUSY ) THEN
				wStatus := udtFClose.status;
				bStep := LOGBOOK_INI;
			END_IF;

	END_CASE

END_FUNCTION_BLOCK
