(********************************************************************
 * COPYRIGHT -- GEA Group
 ********************************************************************
 * Library: GEA_FileIo
 * File: GEA_FileIo.fun
 * Author: cilingir.er
 * Created: September 03, 2015
 ********************************************************************
 * Functions and function blocks of library GEA_FileIo
 ********************************************************************)

FUNCTION CreateFileDevice : UINT
	VAR_INPUT
		PanelType : USINT;
		SimulationType : USINT;
		DeviceName : STRING[20];
		ParameterString : STRING[50];
	END_VAR
	VAR
		ParameterStringToDevice : STRING[50];
		FileDevStr : STRING[50];
		DeviceLink : DevLink;
	END_VAR
END_FUNCTION
