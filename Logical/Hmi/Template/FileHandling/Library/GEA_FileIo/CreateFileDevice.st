(************************************************************************************************************************)
(* Object name: GEA_FileIo                                                                                              *)
(* Author:      Erdal Cilingir                                                                                          *)
(* Site:        GEA Geermany GmbH                                                                                       *)
(* Created:     04-sep-2015                                                                                             *)
(* Restriction: No restrictions.                                                                                        *)
(* Description: Function is Creating File Devices                                                                       *)
(*                                                                                                                      *)
(*----------------------------------------------------------------------------------------------------------------------*)
(* Additional information:                                                                                              *)
(*                                                                                                                      *)
(* No additional information available.                                                                                 *)
(*                                                                                                                      *)
(*----------------------------------------------------------------------------------------------------------------------*)
(* Version 1.00.0  04-sep-2015  Erdal Cilingir                  Development tool: B&R Automation Studio V3.0.90.30 SP12 *)
(*                                                                                                                      *)
(* First Release                                                                                                        *)
(*--------------------------------------------------------------------------------------------------------------------- *)
(************************************************************************************************************************)

FUNCTION CreateFileDevice
    IF PanelType = SimulationType THEN
    	FileDevStr := '/DEVICE=C:\BRsim\';
    ELSE
    	FileDevStr := '/DEVICE=C:\';
    END_IF
    ParameterStringToDevice:='';
    brsstrcat(ADR(ParameterStringToDevice), ADR(FileDevStr));
    brsstrcat(ADR(ParameterStringToDevice), ADR(ParameterString));
    
	DeviceLink.enable	:= TRUE;
	DeviceLink.pDevice	:= ADR(DeviceName);
	DeviceLink.pParam	:= ADR(ParameterStringToDevice);
	DeviceLink();
	IF DeviceLink.status <> 0 AND DeviceLink.status <> fiERR_DEVICE_ALREADY_EXIST THEN
		ERRxwarning(50000, DeviceLink.status, ADR('GEA FileIo: CreateFileDevice failed'));
		CreateFileDevice := DeviceLink.status;
    ELSE
        CreateFileDevice := 0;
	END_IF

	RETURN;
END_FUNCTION
