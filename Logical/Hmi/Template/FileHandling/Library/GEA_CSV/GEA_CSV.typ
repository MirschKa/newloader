
TYPE
	ListboxColumn_type : 	STRUCT 
		FileNumberAscii : STRING[10];
		FileName : STRING[40];
		FileComment : STRING[40];
		FileDateTime : STRING[15];
		FileRecipeType : STRING[2];
	END_STRUCT;
	Listbox_typ : 	STRUCT 
		FileNum : UINT;
		FocusPos : UINT;
		NameLen : USINT;
		Command : USINT;
		FileName : ARRAY[0..199]OF ListboxColumn_type;
		IncFocusPos : USINT;
		DecFocusPos : USINT;
		PageUp : USINT;
		Pagedown : USINT;
	END_STRUCT;
	_csv_exfilelist_internal2 : 	STRUCT 
		Step : USINT;
		DownLines : UINT;
		UpLines : UINT;
		PosFirstLine : UINT;
		FileNumOnDisk : UINT;
		FileNumAct : UINT;
		Offset : UDINT;
		OffsetDateTime : UDINT; (*new in Template V3*)
		OffsetFileMode : UDINT; (*new in Template V3*)
		TmpExtension : STRING[4];
		FileInfo : fiDIR_READ_DATA;
		i : UINT;
		result1 : DINT;
		result2 : DINT;
		FileNumberAsDINT : DINT;
		FileNumberAsUSINT : ARRAY[0..254]OF USINT;
		OptionStruct : CSV_ListFileOption_typ;
		TmpString : ARRAY[0..50]OF USINT;
		dateandtime : DTStructure;
		status_alloc : UINT;
		pFileData : UDINT;
		FileIdent : UDINT;
		FileWithError : BOOL;
		FileFound : BOOL;
		PositionFound : BOOL;
		FirstSeperatorFound : BOOL;
		SecondSeperatorFound : BOOL;
		EndPositionFound : BOOL;
		StartPositionComment : UDINT;
		StartPositionName : UDINT;
		EndPositionName : UDINT;
		CRLF : ARRAY[0..2]OF USINT;
		InternalFileName : ARRAY[0..254]OF USINT;
		VarNameSearch : ARRAY[0..5]OF USINT;
		InternalFileComment : ARRAY[0..254]OF USINT;
		CompleteEntry : ARRAY[0..254]OF USINT;
		InternalFileType : STRING[5];
	END_STRUCT;
END_TYPE
