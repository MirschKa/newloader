;*****************************************************************
;FUB: 			ExFileList2
;*****************************************************************
;Discription:
;	This FUB is based on "ExListFile" from V0.01.9 - 11 August 2003
;	CSV-Init in a CSV-File.
;
;Parameter:
;
;Author:	Tilmann N�her
;			CFS B�hl
;
;History:	2.02.3
;			File data are read out in a structure (Filenumber, Filename, DateAndTime, Filetype) to show in four different listboxes
;			(former version the data were read in one big string)
;*****************************************************************
