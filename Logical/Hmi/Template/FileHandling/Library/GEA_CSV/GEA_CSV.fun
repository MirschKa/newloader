
FUNCTION_BLOCK ExListFile2
	VAR_INPUT
		pFileDevice : UDINT;
		MaxNameLenght : USINT;
		MaxFileNumber : UDINT; (*Maximum number of recipes to add to listbox*)
		command : USINT;
		pOptions : UDINT;
	END_VAR
	VAR_OUTPUT
		status : UINT;
		CSV_FileNum : UINT;
		ListboxFocusPos : UINT;
		ScrollUp : BOOL;
		ScrollDown : BOOL;
	END_VAR
	VAR_IN_OUT
		RecipeInfos : Listbox_typ;
	END_VAR
	VAR
		Internal : _csv_exfilelist_internal2;
		DirInfo_CSV : DirInfo;
		DirRead_CSV : DirRead;
		FileOpen_CSV : FileOpen;
		FileRead_CSV : FileRead;
		FileClose_CSV : FileClose;
		DynUsint : REFERENCE TO USINT;
	END_VAR
END_FUNCTION_BLOCK
