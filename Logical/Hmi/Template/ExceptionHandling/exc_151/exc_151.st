(************************************************************************************************************************)
(* Object name: exc_151                                                                                                 *)
(* Author:      Mario Felius                                                                                            *)
(* Site:        CFS Bakel                                                                                               *)
(* Created:     16-may-2007                                                                                             *)
(* Restriction: Must be called by exception 151 only. Only to be used for SG4 targets.                                  *)
(* Description: Task for handling exception 151 (cycle time violation task class #1).                                   *)
(*                                                                                                                      *)
(*----------------------------------------------------------------------------------------------------------------------*)
(* Additional information:                                                                                              *)
(*                                                                                                                      *)
(* The Automation Runtime operating system triggers the cycle time violation exception only if the tasks in the task    *)
(* class are not yet finished processing before the allowed cycle time violation period runs out.                       *)
(*                                                                                                                      *)
(*----------------------------------------------------------------------------------------------------------------------*)
(* Version 3.00.0  18-jan-2012  Samuel Otto                     Development tool: B&R Automation Studio V3.0.90.19 SP01 *)
(*                                                                                                                      *)
(* Cleaned up this revision history and set version to V3.00.0                                                          *)
(*--------------------------------------------------------------------------------------------------------------------- *)
(************************************************************************************************************************)

PROGRAM _INIT

END_PROGRAM

PROGRAM _CYCLIC
(*----------------------------------------------------------------------------------------------------------------------*)
(* Exception alarm trigger.                                                                                             *)
(*----------------------------------------------------------------------------------------------------------------------*)
SG4_ExceptionAlarmTrigger_151(
  ExceptionNumber := 151,
  Command         := TPL_Exception);
END_PROGRAM

