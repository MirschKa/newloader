(************************************************************************************************************************)
(* Object name: PP400_05                                                                                                *)
(* Author:      Samuel Otto                                                                                             *)
(* Site:        B&R Bad Homburg                                                                                         *)
(* Created:     15-nov-2010                                                                                             *)
(* Restriction: This task MUST be the SECOND task of the project.                                                        *)
(* Description: One specific task for every configuration. - Here: PP400, 05"                                           *)
(*                                                                                                                      *)
(*----------------------------------------------------------------------------------------------------------------------*)
(* Additional information:                                                                                              *)
(*                                                                                                                      *)
(* No additional information available.                                                                                 *)
(*                                                                                                                      *)
(*----------------------------------------------------------------------------------------------------------------------*)
(* Version 4.00.0  15-jul-2016  GEA                            Development tool: B&R Automation Studio V4.1.9.44 SP #51 *)
(*                                                                                                                      *)
(*--------------------------------------------------------------------------------------------------------------------- *)
(************************************************************************************************************************)

PROGRAM _INIT
	TPL_Config.Out.PanelSize				:= TPL_SMALL_PANEL;		(* Panel Size: SMALL_PANEL or MEDIUM_PANEL or BIG_PANEL *)
	TPL_Config.Out.PanelType				:= TPL_PPC2100;		  (* Panel Type: POWER_PANEL or APC *)
	TPL_SWID.In.Cfg.Template.Name			:= '5" Template';	(* Machine name            (max 24 Characters) *)
	TPL_SWID.In.Cfg.Template.MasterSoftware	:= 'TMPL05';		(* Master software release (max 24 Characters) *)
	
(*----------------------------------------------------------------------------------------------------------------------*)
(* Stop executing this task.                                                                                            *)
(*----------------------------------------------------------------------------------------------------------------------*)
ST_tmp_suspend(0);
END_PROGRAM

PROGRAM _CYCLIC

END_PROGRAM
