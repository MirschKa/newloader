(************************************************************************************************************************)
(* Object name: APC_15                                                                                                  *)
(* Author:      Samuel Otto                                                                                             *)
(* Site:        B&R Bad Homburg                                                                                         *)
(* Created:     15-nov-2010                                                                                             *)
(* Restriction: This task MUST be the SECOND task of the project.                                                        *)
(* Description: One specific task for every configuration. - Here: APC, 15"                                             *)
(*                                                                                                                      *)
(*----------------------------------------------------------------------------------------------------------------------*)
(* Additional information:                                                                                              *)
(*                                                                                                                      *)
(* No additional information available.                                                                                 *)
(*                                                                                                                      *)
(*----------------------------------------------------------------------------------------------------------------------*)
(* Version 3.00.0  18-jan-2012  Samuel Otto                     Development tool: B&R Automation Studio V3.0.90.19 SP01 *)
(*                                                                                                                      *)
(* Cleaned up this revision history and set version to V3.00.0                                                          *)
(*--------------------------------------------------------------------------------------------------------------------- *)
(************************************************************************************************************************)

PROGRAM _INIT
	TPL_Config.Out.PanelSize				:= TPL_BIG_PANEL;		(* Panel Size: SMALL_PANEL or MEDIUM_PANEL or BIG_PANEL *)
	TPL_Config.Out.PanelType				:= TPL_APC;				(* Panel Type: POWER_PANEL or APC *)
	TPL_SWID.In.Cfg.Template.Name			:= '15" Template';	(* Machine name            (max 24 Characters) *)
	TPL_SWID.In.Cfg.Template.MasterSoftware	:= 'TMPL15';		(* Master software release (max 24 Characters) *)

(*----------------------------------------------------------------------------------------------------------------------*)
(* Stop executing this task.                                                                                            *)
(*----------------------------------------------------------------------------------------------------------------------*)
ST_tmp_suspend(0);
END_PROGRAM

PROGRAM _CYCLIC

END_PROGRAM
