(********************************************************************
 * COPYRIGHT --  
 ********************************************************************
 * Program: X20VCWT_15
 * File: X20VCWT_15.st
 * Author: SWC-VMware
 * Created: October 16, 2012
 ********************************************************************
 * Implementation of program X20VCWT_15
 ********************************************************************)

PROGRAM _INIT
	TPL_Config.Out.PanelSize				:= TPL_BIG_PANEL;		(* Panel Size: SMALL_PANEL or MEDIUM_PANEL or BIG_PANEL *)
	TPL_Config.Out.PanelType				:= TPL_X20_VCWT;		(* Panel Type: POWER_PANEL or APC or Windows Terminal *)
	TPL_SWID.In.Cfg.Template.Name			:= '15" Template';	(* Machine name            (max 24 Characters) *)
	TPL_SWID.In.Cfg.Template.MasterSoftware	:= 'TMPL15';		(* Master software release (max 24 Characters) *)

(*----------------------------------------------------------------------------------------------------------------------*)
(* Stop executing this task.                                                                                            *)
(*----------------------------------------------------------------------------------------------------------------------*)
ST_tmp_suspend(0);
END_PROGRAM

PROGRAM _CYCLIC

END_PROGRAM
