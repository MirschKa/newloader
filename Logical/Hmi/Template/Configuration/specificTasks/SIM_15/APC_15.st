(************************************************************************************************************************)
(* Object name: APC_15                                                                                                  *)
(* Author:      Samuel Otto                                                                                             *)
(* Site:        B&R Bad Homburg                                                                                         *)
(* Created:     15-nov-2010                                                                                             *)
(* Restriction: This task MUST be the SECOND task of the project.                                                        *)
(* Description: One specific task for every configuration. - Here: APC, 15"                                             *)
(*                                                                                                                      *)
(*----------------------------------------------------------------------------------------------------------------------*)
(* Additional information:                                                                                              *)
(*                                                                                                                      *)
(* No additional information available.                                                                                 *)
(*                                                                                                                      *)
(*----------------------------------------------------------------------------------------------------------------------*)
(* Version 1.00.0  14-june-2019  DoB                    			Development tool: B&R Automation Studio V4.5.2.102	*)
(*                                                                                                                      *)
(* Based on APC_15 task                                                         										*)
(*--------------------------------------------------------------------------------------------------------------------- *)
(************************************************************************************************************************)

PROGRAM _INIT
	TPL_Config.Out.PanelSize				:= TPL_BIG_PANEL;		(* Panel Size: SMALL_PANEL or MEDIUM_PANEL or BIG_PANEL *)
	TPL_Config.Out.PanelType				:= TPL_SIM;				(* Panel Type: POWER_PANEL or APC *)
	TPL_SWID.In.Cfg.Template.Name			:= '15" Template';		(* Machine name            (max 24 Characters) *)
	TPL_SWID.In.Cfg.Template.MasterSoftware	:= 'TMPL15';			(* Master software release (max 24 Characters) *)

(*----------------------------------------------------------------------------------------------------------------------*)
(* Stop executing this task.                                                                                            *)
(*----------------------------------------------------------------------------------------------------------------------*)
ST_tmp_suspend(0);
END_PROGRAM

PROGRAM _CYCLIC

END_PROGRAM
