(************************************************************************************************************************)
(* Object name: hmi_cfg                                                                                                 *)
(* Author:      Wiedenmann Patrick                                                                                      *)
(* Site:        CFS B�hl                                                                                                *)
(* Created:     01-feb-2011                                                                                             *)
(* Restriction: No restrictions.                                                                                        *)
(* Description: HMI System configuration.                                                                               *)
(*                                                                                                                      *)
(*----------------------------------------------------------------------------------------------------------------------*)
(* Additional information:                                                                                              *)
(*                                                                                                                      *)
(* No additional information available.                                                                                 *)
(*                                                                                                                      *)
(*----------------------------------------------------------------------------------------------------------------------*)
(* Version 3.00.0  18-jan-2012  Samuel Otto                 Development tool: B&R Automation Studio V3.0.90.19 SP01    	*)
(*                                                          Cleaned up this revision history and set version to V3.00.0	*)
(*----------------------------------------------------------------------------------------------------------------------*)  
(*         3.00.1  19-jun-2012  Tilmann Naeher				-removed the following unused structure elements from task	*)  
(*                                                           main structure: 											*)
(*                                                          	-TPL_Config.In.Para. 									*)
(*															-Folder "MENU" is created at statup	additionaly				*)
(*																														*)
(*--------------------------------------------------------------------------------------------------------------------- *)
(* Version 3.00.2  19-sep-2012  Frank Thomas Greeb              Development tool: B&R Automation Studio V3.0.90.21 SP03 *)
(* - new config PV for TPL_Dialogh                                                                                      *)
(*--------------------------------------------------------------------------------------------------------------------- *)
(* Version 3.00.3  13-nov-2012  Tilmann Naeher                  Development tool: B&R Automation Studio V3.0.90.22 SP04 *)
(* - error in loading the HmiSysV3.csv - no data																		*)
(*   distribution after loading the file (language problem)																*)
(*--------------------------------------------------------------------------------------------------------------------- *)
(* Version 3.00.4  22-aug-2014  Erdal Cilingir                  Development tool: B&R Automation Studio V3.0.90.28 SP10 *)
(* Issue List Item: [#422] Handling of TPL_GlobalStatusText																*)
(*   - 	Create New Varaible "TPL_System.Out.Stat.GlobalStatusIndex" and "TPL_System.In.Cmd.GlobalStatusIndex"			*)
(*      Copy in information to out Information																			*)
(*--------------------------------------------------------------------------------------------------------------------- *)
(* Version 3.04.1  04-sep-2015  Erdal Cilingir                  Development tool: B&R Automation Studio V3.0.90.28 SP10 *)
(* [#471] Do Filedevice creation programmatically   																	*)
(*		  Create File Device over the function CreateFileDevice 														*)
(* [#473] Template doesn't start up if HmiSysV3.csv is corrupted														*)
(*		  If file is corrupted (size = 0), it will be deleted and created with the default values 						*)
(*		  and message with hint text will be displayed                                          						*)
(*		  Wait 5 second after reboot because of Panel has to boot in terminal mode                 						*)
(************************************************************************************************************************)
(* Version 3.04.2  15-mar-2018  Dominik Bener                   Development tool: B&R Automation Studio V4.1.15.54 SP 	*)
(* - Due to an additional parameter in template configuration (caused by issue #504) the configuration file's 			*)
(*   versionioning is increased fom HmiSysV3.csv to HmiSysV4.csv														*)
(************************************************************************************************************************)
(* Version 3.04.3  17-june-2019  Dominik Bener                   Development tool: B&R Automation Studio V4.5.2.102 	*)
(* - Due to an additional parameter in template configuration (caused by issue #519) the configuration file's 			*)
(*   versionioning is increased fom HmiSysV4.csv to HmiSysV5.csv														*)
(************************************************************************************************************************)

PROGRAM _INIT
(*initiate*)
TPL_Config.Out.StartUpFinished := FALSE;

// Create Template File Devices
CreateFileDevice(TPL_Config.Out.PanelType,TPL_SIM,'CF','');
CreateFileDevice(TPL_Config.Out.PanelType,TPL_SIM,'RECIPE','RECIPE\');
CreateFileDevice(TPL_Config.Out.PanelType,TPL_SIM,'CONFIG','CONFIG\');
CreateFileDevice(TPL_Config.Out.PanelType,TPL_SIM,'DATA','DATA\');
CreateFileDevice(TPL_Config.Out.PanelType,TPL_SIM,'ALARMS','ALARMS\');
CreateFileDevice(TPL_Config.Out.PanelType,TPL_SIM,'Temp','Temp\');
CreateFileDevice(TPL_Config.Out.PanelType,TPL_SIM,'PBI','PBI\');

(* Directories on the mass storage device *)
(* Device Name of the mass storage device (needs to be defined as file-device) *)
DirStruct.HarddiskDevice := 'CF';
(* Define Directory names *)
DirStruct.Directory[0]:= 'ALARMS';
DirStruct.Directory[1]:= 'CONFIG';
DirStruct.Directory[2]:= 'DATA';
DirStruct.Directory[3]:= 'HELP';
DirStruct.Directory[4]:= 'WWW';
DirStruct.Directory[5]:= 'PBI';
DirStruct.Directory[6]:= 'M_DATA';
DirStruct.Directory[7]:= 'RECIPE';
DirStruct.Directory[8]:= 'MENU';
(*********************************************************************************************************************)
ReadFile := FALSE;

pNames := ADR('TPL_Config.In.Data');
brsstrcpy(ADR(Filename),ADR('HmiSysV6'));
brsstrcpy(ADR(FilenameCSV),ADR('HmiSysV6.CSV'));
CSV_InitSysData(pStructName:=ADR(pNames), NumOfStructs:=1, FullNameLenght:=0, Options:=0);

TPL_Config.In.Cmd.ReadFile := TRUE;

(*****create all template directories***************************************************************************************)
WHILE Step < _2_WAIT DO
	CASE Step OF
		_0_INIT:

			(* Check if recipe directories already exist *)
			IF (xcnt <= 8) THEN
				DirInfo_0(enable:= 1, pDevice:= ADR(DirStruct.HarddiskDevice), pPath:= ADR(DirStruct.Directory[xcnt]));
				(* Reading out status ready *)
				IF DirInfo_0.status <> fiERR_FUB_BUSY THEN
					DirStruct.DirectoryStatus[xcnt]:= DirInfo_0.status;
					xcnt := xcnt + 1;
				END_IF
			ELSE	(* All directories are checked, goto cyclic part *)
				Step := _1_DIR_CREATE;
				xcnt:= 0;
			END_IF

		(* create direcgtory if it does not exist *)
		_1_DIR_CREATE:
			IF xcnt <= 8 THEN
				IF (DirStruct.DirectoryStatus[xcnt] = fiERR_DIR_NOT_EXIST) THEN
					DirCreate_0(enable:= 1, pDevice:= ADR(DirStruct.HarddiskDevice), pName:= ADR(DirStruct.Directory[xcnt]));
				END_IF
				IF (DirCreate_0.status = 0) THEN
					DirStruct.DirectoryStatus[xcnt]:= 0;
					xcnt:= xcnt + 1;
				END_IF
			ELSE
				xcnt:= 0;
				Step := _2_WAIT;
			END_IF
			
		(* creating of new folders is finished *)
		_2_WAIT:
		
	END_CASE
END_WHILE

(*check PBI directory *)

IF (DirStruct.DirectoryStatus[5] = fiERR_DIR_NOT_EXIST) THEN
	(*deactivate the handling for PBI*)
	TPL_Config.Out.Stat.PBIActivated := FALSE;
ELSIF (DirStruct.DirectoryStatus[5] = 0) THEN
	(*activate the handling for PBI*)
	TPL_Config.Out.Stat.PBIActivated  := TRUE;
END_IF;

(****reset values for cyclic task ****)
Step := _0_INIT;
xcnt:= 0;

(* backupt machine-SWID from Task to local structure. If set TPL_SWID.Out.LoadSoftwareID_FromTask = TPL_ENABLED, it will be copied to TPL_SWID-structure after load from csv-file *)
brsmemcpy(ADR(INT_Config.Data.SWID.Machine), ADR(TPL_SWID.In.Cfg.Machine), SIZEOF(INT_Config.Data.SWID.Machine)); (* Save machine-SWID if load from task *)
END_PROGRAM

PROGRAM _CYCLIC
(*************************************************************************************************************)
(* 		Function call																						 *)
(*************************************************************************************************************)

(* - - - - - commands - - - - -  *)
IF (TPL_Backup.Out.Stat.NewDataLoaded = TRUE) AND  (CmdLoadData = FALSE) THEN
	TPL_Config.In.Cmd.ReadFile := TRUE;
	CmdLoadData := TRUE;
END_IF

IF TPL_Backup.Out.Stat.NewDataLoaded = FALSE THEN
	CmdLoadData := FALSE;
END_IF

(* - - - - - Input parameter - - - - -  *)
INT_Config.In.TcpIpReady := TPL_Netweth.Out.Stat.EthAddressReady;
INT_Config.In.MachineNameTypeNumber := TPL_SWID.Out.MachineNameTypeNumber;

(*************************************************************************************************************)
(* 		End function call													 																					 *)
(*************************************************************************************************************)
CASE Step OF

	_0_INIT:
		(* Check if recipe directories already exist *)
		IF (xcnt <= 8) THEN
			DirInfo_0(enable:= 1, pDevice:= ADR(DirStruct.HarddiskDevice), pPath:= ADR(DirStruct.Directory[xcnt]));
			(* Reading out status ready *)
			IF DirInfo_0.status <> fiERR_FUB_BUSY THEN
				DirStruct.DirectoryStatus[xcnt]:= DirInfo_0.status;
				xcnt := xcnt + 1;
			END_IF
		ELSE	(* All directories are checked, goto cyclic part *)
			Step := _1_DIR_CREATE;
			xcnt:= 0;
		END_IF

	(* create direcgtory if it does not exist *)
	_1_DIR_CREATE:
		IF xcnt <= 8 THEN
			IF (DirStruct.DirectoryStatus[xcnt] = fiERR_DIR_NOT_EXIST) THEN
				DirCreate_0(enable:= 1, pDevice:= ADR(DirStruct.HarddiskDevice), pName:= ADR(DirStruct.Directory[xcnt]));
			END_IF
			IF (DirCreate_0.status = 0) THEN
				DirStruct.DirectoryStatus[xcnt]:= 0;
				xcnt:= xcnt + 1;
			END_IF
		ELSE
			xcnt:= 0;
			Step := _2_WAIT;
		END_IF
	(* waiting for new command *)

	_2_WAIT:
		wait := wait + 1;
		(* read file is executed after booting *)
		IF (TPL_Config.In.Cmd.ReadFile = TRUE) THEN
            FileInfo_0(enable := 1, pDevice := ADR('CONFIG'), pName := ADR(FilenameCSV), pInfo := ADR(FILE_INFO));
            // delete command after file info is available
            IF FileInfo_0.status <> 65535 THEN
            	TPL_Config.In.Cmd.ReadFile := FALSE;
    			TPL_Config.Out.FileReady := FALSE;
    			ReadFile:= FALSE;
            END_IF
            // file info is ok
            IF FileInfo_0.status = 0 THEN
                //delete file, if size is 0
                IF FILE_INFO.size=0 THEN
                    Step := _8_DELETE_CSV_FILE;
                ELSE
			        Step := _3_READ_CSV_FILE;
                END_IF
             // file is not available
             ELSIF FileInfo_0.status = 20708 THEN 
                Step := _3_READ_CSV_FILE;
             // error durring file info
             ELSIF FileInfo_0.status <> 65535 THEN
             	TPL_Config.Out.Stat.Error := TRUE;
			    TPL_Config.Out.Stat.ErrorNumber := FileInfo_0.status;
			    Step := _7_ERROR_DURING_OPERATION;
             END_IF
		(* write file is executed after changing one of the stored parameters *)
		ELSIF (TPL_Config.In.Cmd.WriteFile = TRUE) THEN
			TPL_Config.In.Cmd.WriteFile := FALSE;
			TPL_Config.Out.FileReady := FALSE; 			(* "HmiSys.csv" file is being written *)
			Step := _5_SEARCH_CSV_DATA;
			write := write + 1;
		ELSE
			TPL_Config.Out.FileReady := TRUE; 			(* "HmiSys.csv" file is ready *)
			TPL_Config.Out.StartUpFinished := TRUE;
		END_IF

	(* reading file content *)
	_3_READ_CSV_FILE:
		CSV_ReadSysData.pFileName := ADR(Filename);
		CSV_ReadSysData.pDevice   := ADR('CONFIG');
		CSV_ReadSysData.No_DatatypControl := 0;
		CSV_ReadSysData.Options   := 0;
		CSV_ReadSysData.Separator := TPL_CSV_SEPERATOR;
		CSV_ReadSysData.IgnoreError:= FALSE; 
		CSV_ReadSysData();

		IF (CSV_ReadSysData.status = 0) THEN	(* file was read successful *)
			ReadFile:= TRUE;
			IF (TPL_SWID.Out.LoadSoftwareID_FromTask = TPL_ENABLED) THEN
				(* copy backuped data to runtime values and write values into file *)
				brsmemcpy(ADR(TPL_Config.In.Data.SWID.Machine), ADR(INT_Config.Data.SWID.Machine), SIZEOF(TPL_Config.In.Data.SWID.Machine));
				TPL_Config.In.Cmd.WriteFile := TRUE;
			ELSE (* load from file *)
				(* nothing to do, swid was read from file an is now in runtime-structure TPL_SWID.In.Config and will be copied to TPL_SWID.In.Config *)
			END_IF;
			TPL_Config.Out.UpdateTcpIp := TRUE;	
			Step := _2_WAIT;
		ELSIF (CSV_ReadSysData.status = fiERR_FILE_NOT_FOUND) THEN
			ReadFile := FALSE;
			INT_Config.Out.ReadTcpIp := TRUE;
			INT_Config.In.TcpIpReady := FALSE;
			Step := _4_WAIT_FOR_ETH_ADDRESS;
		ELSIF (CSV_ReadSysData.status = fiERR_PV_NOT_FOUND) THEN
			ReadFile := FALSE;
			Step := _8_DELETE_CSV_FILE;
		ELSIF ((CSV_ReadSysData.status <> 65535) AND (CSV_ReadSysData.status <> 0)) THEN
			TPL_Config.Out.Stat.Error := TRUE;
			TPL_Config.Out.Stat.ErrorNumber := CSV_ReadSysData.status;		
			Step := _7_ERROR_DURING_OPERATION;
		END_IF

	(* wait until ethernet-settings are read in netweth-task *)
	_4_WAIT_FOR_ETH_ADDRESS:
		IF (INT_Config.In.TcpIpReady = TRUE) THEN
			INT_Config.In.TcpIpReady := FALSE;
			Step:= _5_SEARCH_CSV_DATA;
		END_IF

	(* creating file content *)
	_5_SEARCH_CSV_DATA:
		(* create amount of data to be written into the file *)

		// Encode Passwords
		FOR i := 0 TO 8 DO
			brsmemset(ADR(TPL_Password.InOut.Password[i]) + brsstrlen(ADR(TPL_Password.InOut.Password[i])) + 1, 0,SIZEOF(TPL_Password.InOut.Password[i]) - brsstrlen(ADR(TPL_Password.InOut.Password[i])) - 1 );	// Delete characters after binary 0
			brsmemcpy(ADR(codeBuffer[i]),    ADR(TPL_Password.InOut.Password[i]),   2);
			brsmemset(ADR(codeBuffer[i])+2,  49 + i,                                1);
			brsmemcpy(ADR(codeBuffer[i])+3,  ADR(TPL_Password.InOut.Password[i])+2, 6);
			brsmemset(ADR(codeBuffer[i])+9,  49 + i,                                1);
			brsmemcpy(ADR(codeBuffer[i])+10, ADR(TPL_Password.InOut.Password[i])+8, 2);
			statusEnDecode := EncodeB64(ADR(codeBuffer[i]), SIZEOF(codeBuffer[i]), ADR(TPL_Password.In.Cfg.codedPwds[i]), SIZEOF(TPL_Password.In.Cfg.codedPwds[i]), ADR(TPL_Password.In.Cfg.codeLength));
		END_FOR

(************************************************************************************************************)
(************************************************************************************************************)
(* 		BEGIN Read Config-Data from TPL_[task].In.Config-Structures		 									*)
(************************************************************************************************************)
(************************************************************************************************************)
			brsmemcpy(ADR(TPL_Config.In.Data.SWID),			ADR(TPL_SWID.In.Cfg),			SIZEOF(TPL_Config.In.Data.SWID));
			brsmemcpy(ADR(TPL_Config.In.Data.System),		ADR(TPL_System.In.Cfg),			SIZEOF(TPL_Config.In.Data.System));
			brsmemcpy(ADR(TPL_Config.In.Data.Recipe),		ADR(TPL_Rec.In.Cfg),			SIZEOF(TPL_Config.In.Data.Recipe));
			brsmemcpy(ADR(TPL_Config.In.Data.Backup),		ADR(TPL_Backup.In.Cfg),			SIZEOF(TPL_Config.In.Data.Backup));
			brsmemcpy(ADR(TPL_Config.In.Data.Password),		ADR(TPL_Password.In.Cfg),		SIZEOF(TPL_Config.In.Data.Password));
			brsmemcpy(ADR(TPL_Config.In.Data.UserManagement),ADR(TPL_UserManager.In.Cfg),	SIZEOF(TPL_Config.In.Data.UserManagement));
			brsmemcpy(ADR(TPL_Config.In.Data.Alarm),		ADR(TPL_Alarmh.In.Cfg),			SIZEOF(TPL_Config.In.Data.Alarm));
			brsmemcpy(ADR(TPL_Config.In.Data.Dialogh),		ADR(TPL_Dialogh.In.Cfg),		SIZEOF(TPL_Config.In.Data.Dialogh));
(************************************************************************************************************)
(************************************************************************************************************)
(* 		END Read Config-Data from TPL_[task].In.Config-Structures		 									*)
(************************************************************************************************************)
(************************************************************************************************************)

		(* If the file does not exist an will be created, ReadFile is FALSE, otherwise the file exists an ReadFile is TRUE *)
		IF (ReadFile = FALSE) THEN

		END_IF

		CSV_SearchSysData.pStructName := ADR(pNames);
		CSV_SearchSysData.NumOfStructs:= CSV_InitSysData.NumOfStructs;
		CSV_SearchSysData.pStructMem  := CSV_InitSysData.pStructMem;
		CSV_SearchSysData.MemLen      := CSV_InitSysData.MemCounter;
		CSV_SearchSysData.FileHeader  := 1;
		CSV_SearchSysData.Separator	  := TPL_CSV_SEPERATOR;
		CSV_SearchSysData.pMaschineNo    := ADR(INT_Config.In.MachineNameTypeNumber);		(* open *)
		CSV_SearchSysData.pComment    := ADR('HMI system configuration file');

		CSV_SearchSysData();

		IF (CSV_SearchSysData.status = 0 ) THEN
			//TPL_Config.In.Data.Password.Password := PasswordBuffer;
			Step := _6_WRITE_CSV_FILE;
		ELSIF (CSV_SearchSysData.status <> 65535) THEN
			TPL_Config.Out.Stat.Error := TRUE;
			TPL_Config.Out.Stat.ErrorNumber := CSV_SearchSysData.status;
			Step := _7_ERROR_DURING_OPERATION;
		END_IF

	(* writing data into file *)
	_6_WRITE_CSV_FILE:
		cntWrite := cntWrite+1;
		CSV_WriteSysData(pFileName:= ADR(Filename), pDevice:= ADR('CONFIG'), pStructMem:= CSV_InitSysData.pStructMem,
						 MemLen:= CSV_SearchSysData.FileLen, Mode:= FILE_OW);

		IF (CSV_WriteSysData.status = 0) THEN
			Step := _2_WAIT;
		ELSIF (CSV_WriteSysData.status <> 65535) THEN
			TPL_Config.Out.Stat.Error := TRUE;
			TPL_Config.Out.Stat.ErrorNumber := CSV_WriteSysData.status;
			Step := _7_ERROR_DURING_OPERATION;
		END_IF

	(* if any unexpected error / warning happens during operation *)
	_7_ERROR_DURING_OPERATION:	
			ReadFile := FALSE;
			Step := _8_DELETE_CSV_FILE;

	(* delete CSV file if PV not found in file *) 	
	_8_DELETE_CSV_FILE:
		DeleteCSVFile(enable:=1, pDevice:=ADR('CONFIG'), pName:=ADR(FilenameCSV));
		IF (DeleteCSVFile.status <> 65535 ) THEN
			Step := _2_WAIT;
			IF (DeleteCSVFile.status = 0 OR DeleteCSVFile.status = 20708) THEN
				TPL_Config.In.Cmd.WriteFile := TRUE;
                IF DeleteCSVFile.status = 0 THEN
                    ShowNewFileCreatedDialog    :=  TRUE;
                END_IF
			ELSIF (DeleteCSVFile.status <> 65535) THEN
				TPL_Config.Out.Stat.Error := TRUE;
				TPL_Config.Out.Stat.ErrorNumber := DeleteCSVFile.status;
				Step := _7_ERROR_DURING_OPERATION;				
			END_IF
		END_IF	
		
END_CASE

(*************************************************************************************************************)
(* 		DIALOG TO SHOW THAT FILE HMI_CONFIG IS RECREATED													 *)
(*************************************************************************************************************)
TON_Dialog.IN:= ShowNewFileCreatedDialog AND TPL_Pageh.Out.PanelIsAlive;
TON_Dialog.PT:=t#5s;
TON_Dialog();

IF TPL_Config.Out.StartUpFinished AND TON_Dialog.Q THEN
	DialogInfo.HeaderTextGroupNumber        := TPL_DIALOG_HEADERTEXT;
	DialogInfo.HeaderTextNumber		        := DHT_ERROR;
	DialogInfo.DialogTextGroupNumber        := TPL_DIALOG_TEXT;
	DialogInfo.DialogTextNumber		        := DT_HMI_CONFIG_RECREATED;
	DialogInfo.ButtonTextGroupNumber        := TPL_DIALOG_BUTTONTEXT;
	DialogInfo.ButtonTextNumberOne	        := DBT_OK;
	DialogInfo.NumberOfButton		        := 1;
	// Call Dialog function 
	StatusDialog:= CallDialog(ADR(DialogIdent), ADR(DialogInfo), TPL_Dialogh.Out.AdrDialogRingBuffer);

	// waiting for event button pressed 
	IF (DialogInfo.Return_PressedButton = 1) THEN
		ShowNewFileCreatedDialog:=FALSE;
	END_IF
END_IF
(*************************************************************************************************************)
(* 		Function outputs																					 *)
(*************************************************************************************************************)

(* - - - - - Output parameter - - - - -  *)
IF (EDGEPOS(TPL_Config.Out.StartUpFinished) = TRUE) OR (EDGEPOS(ReadFile) = TRUE) THEN
	ReadDone:= ReadDone + 1;	//only for status purposes
	
(************************************************************************************************************)
(************************************************************************************************************)
(* 		BEGIN Write Config-Data to TPL_[task].In.Config-Structures		 									*)
(************************************************************************************************************)
(************************************************************************************************************)
		brsmemcpy(ADR(TPL_SWID.In.Cfg),				ADR(TPL_Config.In.Data.SWID),		SIZEOF(TPL_SWID.In.Cfg));
		brsmemcpy(ADR(TPL_System.In.Cfg),			ADR(TPL_Config.In.Data.System),		SIZEOF(TPL_System.In.Cfg));
		brsmemcpy(ADR(TPL_Rec.In.Cfg),				ADR(TPL_Config.In.Data.Recipe),		SIZEOF(TPL_Rec.In.Cfg));
		brsmemcpy(ADR(TPL_Backup.In.Cfg),			ADR(TPL_Config.In.Data.Backup),		SIZEOF(TPL_Backup.In.Cfg));
		brsmemcpy(ADR(TPL_Password.In.Cfg),			ADR(TPL_Config.In.Data.Password),	SIZEOF(TPL_Password.In.Cfg));
		brsmemcpy(ADR(TPL_UserManager.In.Cfg),		ADR(TPL_Config.In.Data.UserManagement),	SIZEOF(TPL_UserManager.In.Cfg));
		brsmemcpy(ADR(TPL_Alarmh.In.Cfg),			ADR(TPL_Config.In.Data.Alarm),		SIZEOF(TPL_Alarmh.In.Cfg));
		brsmemcpy(ADR(TPL_Dialogh.In.Cfg),			ADR(TPL_Config.In.Data.Dialogh),	SIZEOF(TPL_Dialogh.In.Cfg));

(************************************************************************************************************)
(************************************************************************************************************)
(* 		END Write Config-Data to TPL_[task].In.Config-Structures		 									*)
(************************************************************************************************************)
(************************************************************************************************************)

		// Decode Passwords
		FOR i := 0 TO 8 DO
			statusEnDecode := DecodeB64(ADR(TPL_Password.In.Cfg.codedPwds[i]), TPL_Password.In.Cfg.codeLength, ADR(codeBuffer[i]), SIZEOF(codeBuffer[i]));
			testByte := 49 + i;
			IF	statusEnDecode = 0 AND
				(brsmemcmp(ADR(codeBuffer[i])+2, ADR(testByte) , 1)) = 0 AND // Check the two testbytes
				(brsmemcmp(ADR(codeBuffer[i])+9, ADR(testByte) , 1)) = 0 // Check the two testbytes
			THEN
				brsmemcpy(ADR(TPL_Password.InOut.Password[i]),   ADR(codeBuffer[i]),    2);
				brsmemcpy(ADR(TPL_Password.InOut.Password[i])+2, ADR(codeBuffer[i])+3,  6);
				brsmemcpy(ADR(TPL_Password.InOut.Password[i])+8, ADR(codeBuffer[i])+10, 2);
			ELSE	// something was wrong with the password, maybe someone edited the csv-file. So let's save the default value
				TPL_Config.In.Cmd.WriteFile := TRUE;
			END_IF
		END_FOR
END_IF


IF INT_Config.Out.ReadTcpIp THEN
	INT_Config.Out.ReadTcpIp :=FALSE;
	TPL_Netweth.In.Cmd.ReadIpSettings := TRUE;
END_IF
TPL_Config.Out.Stat.StepNumber := Step;
IF (TPL_Config.Out.Stat.StepNumber <> _2_WAIT) THEN
	TPL_Config.Out.Stat.Busy := TRUE;
ELSE
	TPL_Config.Out.Stat.Busy := FALSE;
	TPL_Config.Out.Stat.Error := FALSE;
	TPL_Config.Out.Stat.ErrorNumber := 0;
END_IF
	
	
// Copy in of GlobalStatus Index to Out
TPL_System.Out.Stat.GlobalStatusIndex := TPL_System.In.Cmd.GlobalStatusIndex;

(*************************************************************************************************************)
(* 		End function call													 																					 *)
(*************************************************************************************************************)
END_PROGRAM

