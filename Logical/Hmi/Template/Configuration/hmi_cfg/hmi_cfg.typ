
TYPE
	INT_Config_Type : 	STRUCT 
		In : INT_ConfigIn_Type;
		Out : INT_ConfigOut_Type;
		Data : INT_ConfigData_Type;
		New_Member : USINT;
	END_STRUCT;
	INT_ConfigIn_Type : 	STRUCT 
		MachineNameTypeNumber : STRING[74];
		TcpIpReady : BOOL;
	END_STRUCT;
	INT_ConfigOut_Type : 	STRUCT 
		ReadTcpIp : BOOL;
	END_STRUCT;
	INT_ConfigData_Type : 	STRUCT 
		SWID : TPL_SWID_InCfg_Type;
	END_STRUCT;
	directory_structure : 	STRUCT 
		HarddiskDevice : STRING[6];
		Directory : ARRAY[0..8]OF STRING[6];
		DirectoryStatus : ARRAY[0..8]OF UINT;
		DirectoryRecipeStatus : ARRAY[0..9]OF UINT;
	END_STRUCT;
	Device_Type : 	STRUCT 
		DeviceName : STRING[20];
		ParameterString : STRING[50];
	END_STRUCT;
END_TYPE
