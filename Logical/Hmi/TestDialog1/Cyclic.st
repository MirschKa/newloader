
PROGRAM _CYCLIC
	(* Insert code here *)
	CASE DialogStep OF
	
		0://Aufruf
			DialogInfo.DialogTextNumber := DT_FILES_USB_OVERWR_CONTINUE;
			
			
			//(*
			IF(Start) THEN
				
				Start := FALSE;
				
				DialogStep := 1;
				
			END_IF
			//*)
		
		1://Bestätigen/Rückmeldung
			DialogStatus := CallDialog(ADR(DialogIdent), ADR(DialogInfo), TPL_Dialogh.Out.AdrDialogRingBuffer);//Standardaufruf von Dialog
			//Optional ResetDialog(ADR(DialogIdent), TPL_Dialogh.Out.AdrDialogRingBuffer)
			

			
			IF(DialogInfo.Return_PressedButton = 1(*Buttonnummer*)) THEN
				
				DialogStep := 2;
				DialogInfo.Return_PressedButton := 0;
				strResult := 'Button 1';
				// Maschinenspezifischer Ablauf
			
			
			ELSIF(DialogInfo.Return_PressedButton = 2(*Buttonnummer*)) THEN
				
				DialogStep := 2;
				DialogInfo.Return_PressedButton := 0;
				strResult := 'Button 2';
				// Maschinenspezifischer Ablauf
				
			END_IF
			
			
		2://Reset
			//DialogInfo.Return_PressedButton := 0;
			DialogStep := 0;
		
		
	END_CASE
		
END_PROGRAM
