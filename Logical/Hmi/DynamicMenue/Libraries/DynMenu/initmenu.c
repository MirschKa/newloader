//***********************************************************************************************************************)
// Object name: InitMenu                                                                                       			*)
// Author:      ?                                                                                             			*)
// Site:        B&R ?                                                                                       			*)
// Created:     ?                                                                                           		    *)
// Restriction: ?                                                                            				            *)
// Description: Dynamic Menu Creation														                            *)
//                                                                                                                      *)
//----------------------------------------------------------------------------------------------------------------------*)
// Version 3.00.0  06-FEB-2013 E.Cilingir                       Development tool: B&R Automation Studio V3.0.90.21 SP03 *)
//                                                                                                                      *)
// + Add User Level Functionality to the Standard for every Element                                                     *)
// + Add the Function HeadLine as Element						                                                        *)
// 	 -- + Add Type EmptyLine for the Function Headline, to get 2 Lines in Visu for the New Funtion HeadLine             *)
//   -- + Add FunctionHeadlineBitmap as Infomration for the Element 			                                        *)
//   -- + Add FunctionHeadlineStatusAdr, FunctionHeadlineStatusType, FunctionHeadlineStatusLength 						*)
//		  for the Variable of the Status																				*)
//   -- + Add LevelStatus for Show the Status depending to the Status                                                   *)
// 																														*)
//--------------------------------------------------------------------------------------------------------------------- *)
// Version 3.00.1  18-FEB-2013 E.Cilingir                       Development tool: B&R Automation Studio V3.0.90.21 SP03 *)
// + Expand UINT FOR Enable Variable									                                                *)
// + Expand Function for Enable if Value is 0 or Value is 1																*)
// + Expand Function for add Fraction digits																			*)
// + Expand Variable Type TIME																							*)
// + If Max Value is 0 then put in the max value of the Variable Type													*)
// + Expand Scale function in dependency to the number of fraction digits [SINT,USINT,INT,UINT,DINT,UDINT]				*)
//				10^Fraction digits																						*)
// + Min and Max Value Types can be different, For every min and max type a selection will be done						*)
// 																														*)
//--------------------------------------------------------------------------------------------------------------------- *)
// Version 3.00.2  05-MAR-2013 E.Cilingir                       Development tool: B&R Automation Studio V3.0.90.21 SP03 *)
// + Add #IP for Show Input States										                                                *)
// + Add #OP for Show Output States										                                                *)
// + Function Headline Button is show disabled, if enabl variable is 0	                                                *)
// 																														*)
//***********************************************************************************************************************)
// Version 3.00.3  18-MAR-2013 E.Cilingir                       Development tool: B&R Automation Studio V3.0.90.21 SP03 *)
// + Functionality for scale is use for 10^n scaling for every Type														*)
// + Add fraction Digits for show number of fractions after the comma													*)
// 																														*)
//***********************************************************************************************************************)
// Version 3.00.4  20-MAR-2013 E.Cilingir                       Development tool: B&R Automation Studio V3.0.90.21 SP03 *)
// + Value of UPDOWN without comletition pointer was initialized but not connected to the value							*)
// 																														*)
//***********************************************************************************************************************)
// Version 3.00.5  02-APR-2013 E.Cilingir                       Development tool: B&R Automation Studio V3.0.90.21 SP03 *)
// + Nummeric Input expanded with MinMax Calculation. A reference can be configured and a Min and Max Value will be 	*)
//	 calculated of the configured values																				*)
// + Name of the Variables checked an changed to more meaningful names												 	*)
// + Max Values are now in Constant Variable																			*)
// 																														*)
//***********************************************************************************************************************)
// Version 3.00.6  03-APR-2013 E.Cilingir                       Development tool: B&R Automation Studio V3.0.90.21 SP03 *)
// + Add Variable to the Info Struct to make Elements Visible/Invisible depending to the number of enabled entries		*)
// 																														*)
//***********************************************************************************************************************)
// Version 3.00.7  09-APR-2013 E.Cilingir                       Development tool: B&R Automation Studio V3.0.90.21 SP03 *)
// Bugfix:	Min Max Calculation for Real Value was not working right													*)
// + Add Help Function	as HTML Browser to the DynMenu																	*)
// + Change Steps of Case in Init to be able to Get Infomration parallel												*)
// 																														*)
//***********************************************************************************************************************)
// Version 3.01.0  05-JUN-2013 E.Cilingir                       Development tool: B&R Automation Studio V3.0.90.21 SP03 *)
// Bugfix:	Min Max Calculation for Real Value was not working right													*)
// + Add calculation Functionality for Nummeric input																	*)
// + Expand the Up Down Function, it is possible to increase/drcrease variable in limits								*)
// + Expecting Version Information in File and checking if it is supported by the Libiary Version						*)
//																														*)
//***********************************************************************************************************************)
// Version 3.01.1  05-JUL-2013 E.Cilingir                       Development tool: B&R Automation Studio V3.0.90.21 SP03 *)
// Bugfix:	Convert Stepwidth from string to float																		*)
// + Add Maintainance Functionality																						*)
//																														*)
//																														*)
//***********************************************************************************************************************)
// Version 3.01.2  19-SEP-2013 E.Cilingir                       Development tool: B&R Automation Studio V3.0.90.21 SP03 *)
// Bugfix:	Scale for NI of Real was missing																			*)
// Bugfix:	Scale of Input Value to PLC for NI of Time was missing														*)
// Bugfix:	MT Function output Element of Type Bool was Missing															*)
// Bugfix:	MT Function output Element was the Fraction information missing												*)
// Bugfix:	Sensing Device in non comilation was not working right for production										*)
// Bugfix:	Disabled View for Momentary button missing																	*)
// Bugfix:	Disabled View for Up/Down Element missing																	*)
// Bugfix:	Character cast to MO_Ver change																				*)
//	+	Add Headline possibility to Maintanance function																*)
//	+	Add Groupbox visibility automatic out of the enties per page info, Give also the possility to set groupbox to 	*)
//			a fixed height																								*)
//	+	Add Message, if no of entries to show are 0 																	*)
//	+	Change page_info_typ to in/out/internal/info struct																*)
// Bugfix:	Number of Pages not right																					*)
//																														*)
//***********************************************************************************************************************)
// Version 3.02.0  13-NOV-2013 E.Cilingir                       Development tool: B&R Automation Studio V3.0.90.21 SP03 *)
//  +  Add #SW to Maintenance Page 																						*)
//  +  Add Status Variable configuration parameter to #SW/#SD/#NI														*)
// Bugfix:	order of NO elements in Production was wrong																*)
//***********************************************************************************************************************)
// Version 3.03.0  27-aug-2014 E.Cilingir                       Development tool: B&R Automation Studio V3.0.90.28 SP10 *)
// Improvement:	[#437] Using scaling + unit groups in Dynamic Menu														*)
// 				   - Read out scaling also as variable	[PD](#NI,#NO,#UD)[MT](#NI,#SW,#SD)								*)
// 				   - Read out unit index also as variable																*)
// 				   - Read out description index also as variable for all elements										*)
// 				   - Add auto max value, if 0 also for #UD																*)
//																														*)
//***********************************************************************************************************************)
// Version 3.04.0  06-sep-2014 E.Cilingir                       Development tool: B&R Automation Studio V3.0.90.30 SP12 *)
// Improvement:	[#452] Change fraction digits by variable inside Dynamic Menu											*)
// 				   - Read out fraction also as variable	[PD](#NI,#NO,#UD)[MT](#NI,#SW,#SD)								*)
// Bugfix:	Check variable type for #AI if string																		*)
// Improvement:	[#454] Dynamic Menu #AO with text index of textgroup and only description text							*)
// 				   - 0 = No text; STRING = Show value out of string; UINT,USINT = Show text out of text group			*)
//																														*)
//***********************************************************************************************************************)

//######################################################################################################################################
//#################################################### INCLUDE LIBIARIES ###############################################################
#include <bur/plctypes.h>
#include <DynMenu.h>
#include <fileIO.h>
#include <sys_lib.h>
#include <string.h>

//######################################################################################################################################
//#################################################### STEPS ###########################################################################
#define INIT			 0
#define FILE_OPEN		 1
#define FILE_MEMALLOC    2
#define FILE_READ		 3
#define FILE_CLOSE		 4
#define ENTRY_MARK		 5
#define ENTRY_SEARCH	 6
#define FREE_MEMALLOC	 7
#define ERROR			 8
#define INIT_OK			 9
#define ERROR_FILE_CLOSE 10

//######################################################################################################################################
//#################################################### CONSTANTS #######################################################################
#define	MAX_PARSER_STRING		255

//######################################################################################################################################
//#################################################### FUNCTION INIT MENU ##############################################################
//######################################################################################################################################
UINT initmenu(UDINT AdrFileDeviceString, UDINT AdrFileNameString, UDINT AdrMenuStructure) {
	//######################################################################################################################################		
	//#################################################### Structure Types #################################################################
	DYN_Menu_Type 	*pMenu;
	DYN_Entry_Type 	*MenuEntry;
	//######################################################################################################################################		
	//#################################################### Variables #######################################################################
	USINT	*Parser = 0;
	UINT	DimVariable = 0, Status = 0, xcnt = 0, ycnt = 0, Count=0; 
	UDINT 	TempLength=0;
	DINT	Result = 0;
	UDINT	TypeEnableVariable = 0, WorkVariableLength = 0;
	UDINT 	AdrEnableFunctionHeadLine=0;
	INT 	EnableFunctionType=0;
	INT 	TempEnableType1=0,TempEnableType2=0;
	UDINT 	TempLevel=0;
	UDINT	TempAdrEnable1=0,TempAdrEnable2=0;
	BOOL	TempShowLogic1=0,TempShowLogic2=0;
	USINT	TempNumOfEnable=0; 
	UDINT AdrMenuEntryStructure=0;
	//####################################################AdrMenuEntryStructure##################################################################################		
	//#################################################### Pointer Adresses ################################################################
	*(UDINT*)&pMenu = AdrMenuStructure;
	AdrMenuEntryStructure = (UDINT)&(pMenu->Entry);
	*(UDINT*)&MenuEntry = (AdrMenuEntryStructure + (sizeof(*MenuEntry)*pMenu->Internal.ActEntryNumber));
	//######################################################################################################################################		
	//#################################################### SWITCH STEP #####################################################################
	pMenu->Out.InitStep=pMenu->Internal.InitStep;
	switch(pMenu->Internal.InitStep)	{
		//######################################################################################################################################		
		//#################################################### CASE INIT #######################################################################			
		case INIT:
			MO_ver("DynMenu", 0, &pMenu->Internal.LibiaryInfo); 
			brsmemcpy((UDINT)&pMenu->Info.LibVersion,(UDINT)&pMenu->Internal.LibiaryInfo.version,5);
			pMenu->Internal.InitStep = FILE_OPEN;
			pMenu->Out.Status = FUNCTION_BUSY;
			return(pMenu->Out.Status);
			break;
		//#####################################################################################################################################
		//#################################################### CASE FILE OPEN #################################################################		
		case FILE_OPEN:
			/* Reset old variables */
			memset(&pMenu->Info, 0, sizeof(&pMenu->Info));
			// Create Filename
			brsstrcpy((UDINT)&pMenu->Info.FileName,AdrFileNameString);
			brsstrcat((UDINT)&pMenu->Info.FileName, (UDINT)&EXTENSION_FILE);
			// Create Help Path for HTML
			brsstrcat((UDINT)&pMenu->Info.HelpPath, (UDINT)&"file://");
			brsstrcat((UDINT)&pMenu->Info.HelpPath, AdrFileDeviceString);
			brsstrcat((UDINT)&pMenu->Info.HelpPath, (UDINT)&":/");
			brsstrcat((UDINT)&pMenu->Info.HelpPath, (UDINT)&"HELP/");
			// Create Help Name for HTML
			brsstrcpy((UDINT)&pMenu->Info.HelpName, AdrFileNameString);
			brsstrcat((UDINT)&pMenu->Info.HelpName, (UDINT)&EXTENSION_HELP);

			pMenu->Internal.FOpen.enable = 1;
			pMenu->Internal.FOpen.pDevice = AdrFileDeviceString;
			pMenu->Internal.FOpen.pFile = (UDINT)&pMenu->Info.FileName;
			pMenu->Internal.FOpen.mode = FILE_RW;
					
			FileOpen(&pMenu->Internal.FOpen);

			if(!pMenu->Internal.FOpen.status){
				pMenu->Internal.FileIdent = pMenu->Internal.FOpen.ident;
				pMenu->Internal.FileSize = pMenu->Internal.FOpen.filelen;
				pMenu->Internal.InitStep = FILE_MEMALLOC;
				pMenu->Out.Status	= FUNCTION_BUSY;
				return(pMenu->Out.Status);
			}
			else if(pMenu->Internal.FOpen.status != 65535){	
				pMenu->Internal.InitStep = ERROR_FILE_CLOSE;
				pMenu->Out.Status	= pMenu->Internal.FOpen.status;
				return(FUNCTION_BUSY);			
			}
			else{
				pMenu->Out.Status	= pMenu->Internal.FOpen.status;
				return(pMenu->Out.Status);	
			}
			break;
		//#########################################################################################################################################
		//#################################################### CASE FILE MEMALLOC #################################################################
		/* Allocate memory for file data */
		case FILE_MEMALLOC:
			if (pMenu->Internal.FileSize == 0){
				pMenu->Internal.InitStep = ERROR_FILE_CLOSE;
				pMenu->Internal.ErrorNumber	= EMPTY_FILE;
				return(FUNCTION_BUSY);
			}
			
			Status = TMP_alloc(pMenu->Internal.FileSize, (void*)&pMenu->Internal.StartAdrDataBuffer);
			if(!Status)	{	
				pMenu->Internal.InitStep = FILE_READ;
				pMenu->Out.Status = FUNCTION_BUSY;
				return(pMenu->Out.Status);
			}
			else if(Status != 65535){
				pMenu->Internal.InitStep = ERROR_FILE_CLOSE;
				pMenu->Out.Status = Status;
				return(FUNCTION_BUSY);
			}
			else{
				pMenu->Out.Status = Status;
				return(pMenu->Out.Status);
			}
			break;
		//##########################################################################################################################################
		//#################################################### CASE READ FILE DATA #################################################################
		case FILE_READ:
			pMenu->Internal.FRead.enable = 1;
			pMenu->Internal.FRead.ident = pMenu->Internal.FileIdent;
			pMenu->Internal.FRead.offset = 0;
			pMenu->Internal.FRead.pDest = pMenu->Internal.StartAdrDataBuffer;
			pMenu->Internal.FRead.len = pMenu->Internal.FileSize;
			FileRead(&pMenu->Internal.FRead);

			if(!pMenu->Internal.FRead.status){
				pMenu->Internal.InitStep = FILE_CLOSE;
				pMenu->Out.Status = FUNCTION_BUSY;
				return(pMenu->Out.Status);
			}
			else if(pMenu->Internal.FRead.status != 65535){
				pMenu->Internal.InitStep = ERROR_FILE_CLOSE;
				pMenu->Out.Status	= pMenu->Internal.FRead.status;
				return(FUNCTION_BUSY);	
			}
			else{
				pMenu->Out.Status	= pMenu->Internal.FRead.status;
				return(pMenu->Out.Status);	
			}
			break;
		//##########################################################################################################################################
		//#################################################### CLOSE FILE ##########################################################################
		case FILE_CLOSE:
			pMenu->Internal.FClose.enable = 1;
			pMenu->Internal.FClose.ident = pMenu->Internal.FileIdent;
			FileClose(&pMenu->Internal.FClose);

			if(!pMenu->Internal.FClose.status){
				pMenu->Internal.ActAdrDataBuffer = pMenu->Internal.StartAdrDataBuffer;
				pMenu->Internal.InitStep = ENTRY_MARK;
				pMenu->Out.Status = FUNCTION_BUSY;
				return(pMenu->Out.Status);
			}
			else if(pMenu->Internal.FClose.status != 65535){
				pMenu->Internal.InitStep = FREE_MEMALLOC;
				pMenu->Out.Status	= pMenu->Internal.FClose.status;
				return(FUNCTION_BUSY);	
			}
			else{
				pMenu->Out.Status	= pMenu->Internal.FClose.status;
				return(pMenu->Out.Status);	
			}
			break;
		//##########################################################################################################################################
		//#################################################### ENTRY MARK ##########################################################################
		case ENTRY_MARK:
			*(UDINT*)&Parser = pMenu->Internal.ActAdrDataBuffer;
			if(*Parser == '#'){
				pMenu->Internal.ActiveReadEntry++;
				for(xcnt = 0; xcnt < (MAX_PARSER_STRING - 1); xcnt++){
					*(UDINT*)&Parser = pMenu->Internal.ActAdrDataBuffer;
					/* Check for ',' or ';' --> indicates end of type definition or end of line */
					if(*Parser == ',' || *Parser == ';'){
						pMenu->Internal.ParserString[xcnt] = 0;
						pMenu->Internal.ActAdrDataBuffer++;
						pMenu->Internal.InitStep = ENTRY_SEARCH;
						pMenu->Out.Status = FUNCTION_BUSY;
						return(pMenu->Out.Status);
					}
						/* Copy data to ParserString */
					else{
						pMenu->Internal.ParserString[xcnt] = *Parser;
						pMenu->Internal.ActAdrDataBuffer++;
					}
				}
			}
				/* Increase Uer address */
			else
			{	pMenu->Internal.ActAdrDataBuffer++;
				pMenu->Out.Status = FUNCTION_BUSY;
				return(pMenu->Out.Status);
			}
			break;
		//##########################################################################################################################################
		//#################################################### Case ENTRY SEARCH ###################################################################
		case ENTRY_SEARCH:
			// Check if FileVersion is Available
			if (pMenu->Internal.ActEntryNumber > 0){
				Result = strcmp("", pMenu->Info.FileVersion);
				if(!Result){
					pMenu->Internal.InitStep = FREE_MEMALLOC;
					pMenu->Out.Status = NO_FILE_VERSION;
					return(FUNCTION_BUSY);
				}
			}
			//-------------------------------------------------------------------------------------------------------------
			//---------------------------------------- FileVersion Infomration --------------------------------------------
			Result = strcmp("#VERSION", pMenu->Internal.ParserString);
			if(!Result)	{
			/* Check entries for headline */
			for(ycnt = 0; ycnt < ELEMENTS_FOR_VERSION_ENTRY; ycnt++){	
			/* Check data from file */
			for(xcnt = 0; xcnt < (MAX_PARSER_STRING - 1); xcnt++){	
			*(UDINT*)&Parser = pMenu->Internal.ActAdrDataBuffer;
			/* Check for ',' or ';' --> indicates new element or end of line */
			if(*Parser == ',' || *Parser == ';'){	
			pMenu->Internal.ParserString[xcnt] = 0;
			pMenu->Internal.ActAdrDataBuffer++;
			break;
			}
			/* Copy data to ParserString */
			else{	
			pMenu->Internal.ParserString[xcnt] = *Parser;
			pMenu->Internal.ActAdrDataBuffer++;
			}
			}
			/* Enable variable */
			if(!ycnt){
			brsstrcpy((UDINT)&pMenu->Info.FileVersion,(UDINT)&pMenu->Internal.ParserString);		
			for(Count = 1; Count < 5; Count++){
			if (pMenu->Info.FileVersion[Count-1] != pMenu->Internal.LibiaryInfo.version[Count]){
			pMenu->Internal.InitStep = FREE_MEMALLOC;
			pMenu->Out.Status = WRONG_FILE_VERSION;
			return(FUNCTION_BUSY);
			break;
			}
			}
			}
			if(ycnt == 1){
			if (strcmp(pMenu->Internal.ParserString, "MT") == 0){
			brsstrcpy((UDINT)&pMenu->Info.FunctionType,(UDINT)&pMenu->Internal.ParserString);
			}
			else if (strcmp(pMenu->Internal.ParserString, "PD") == 0){
			brsstrcpy((UDINT)&pMenu->Info.FunctionType,(UDINT)&pMenu->Internal.ParserString);
			}
			else{
			pMenu->Internal.InitStep = FREE_MEMALLOC;
			pMenu->Out.Status = UNSUPPORTED_FUNCTION_TYPE;
			return(FUNCTION_BUSY);
			}
			}
			}
			pMenu->Internal.ActEntryNumber++;
			pMenu->Internal.InitStep = ENTRY_MARK;
			pMenu->Out.Status = FUNCTION_BUSY;
			return(pMenu->Out.Status);
			}
			
			//-------------------------------------------------------------------------------------------------------------
			//---------------------------------------- Production Page ----------------------------------------------------
			//-------------------------------------------------------------------------------------------------------------
			Result = strcmp("PD", pMenu->Info.FunctionType);
			if(!Result)	{
				//-------------------------------------------------------------------------------------------------------------
				//---------------------------------------- NUMMERIC INPUT -----------------------------------------------------
				Result = strcmp("#NI", pMenu->Internal.ParserString);
				if(!Result)
				{	MenuEntry->DynMenuEnryType = PD_NUMERIC_INPUT_INDEX;
					/* Check entries for numeric input */
					for(ycnt = 0; ycnt < ELEMENTS_FOR_PD_NI_ENTRY; ycnt++)
					{	/* Check data from file */
						for(xcnt = 0; xcnt < (MAX_PARSER_STRING - 1); xcnt++)
						{	*(UDINT*)&Parser = pMenu->Internal.ActAdrDataBuffer;
							/* Check for ',' or ';' --> indicates new element or end of line */
							if(*Parser == ',' || *Parser == ';')
							{	
								pMenu->Internal.ParserString[xcnt] = 0;
								pMenu->Internal.ActAdrDataBuffer++;
								break;
							}
								/* Copy data to ParserString */
							else
							{	pMenu->Internal.ParserString[xcnt] = *Parser;
								pMenu->Internal.ActAdrDataBuffer++;
							}
						}
						/* Enable variable */
						if(!ycnt)
						{
							Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->ShowEntryFunction1Adr), &(WorkVariableLength));
							if(Status)
							{	pMenu->Internal.InitStep = FREE_MEMALLOC;
								pMenu->Out.Status = Status;
								return(FUNCTION_BUSY); 
							}

							/* Data type from enable variable */
							Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(TypeEnableVariable),&(WorkVariableLength), &(DimVariable));
							if(Status)
							{	pMenu->Internal.InitStep = FREE_MEMALLOC;
								pMenu->Out.Status = Status;
								return(FUNCTION_BUSY); 
							}
							else if(TypeEnableVariable != VAR_TYPE_USINT && TypeEnableVariable != VAR_TYPE_UINT)
							{	pMenu->Internal.InitStep = FREE_MEMALLOC;
								pMenu->Out.Status	= WRONG_TYPE_OF_ENABLE_VAR;
								return(FUNCTION_BUSY); 	
								pMenu->Internal.ErrorNumber=(WRONG_TYPE_OF_ENABLE_VAR);
							}
							MenuEntry->ShowEntryFunction1Type=TypeEnableVariable;
						}
							// Enable Function 2013-02-13 E.Cilingir
						else if(ycnt == 1)
						{
							if (brsatoi((UDINT)&pMenu->Internal.ParserString)==0){
								MenuEntry->ShowEntryFunction1=LOW_SHOW;
							}
							else{
								MenuEntry->ShowEntryFunction1=HIGH_SHOW;
							}
						}
						if(ycnt == 2)
						{
							Status = strcmp("0", pMenu->Internal.ParserString);
							if (Status){
								Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->ShowEntryFunction2Adr), &(WorkVariableLength));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								/* Data type from enable variable */
								Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(TypeEnableVariable),&(WorkVariableLength), &(DimVariable));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								else if(TypeEnableVariable != VAR_TYPE_USINT && TypeEnableVariable != VAR_TYPE_UINT)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = WRONG_TYPE_OF_ENABLE_VAR;
									return(FUNCTION_BUSY); 
									pMenu->Out.Status	= WRONG_TYPE_OF_ENABLE_VAR;
									return(FUNCTION_BUSY); 			
								}
								MenuEntry->ShowEntryFunction2Type=TypeEnableVariable;
								MenuEntry->ShowEntryFunctionNumOfEnable=MenuEntry->ShowEntryFunctionNumOfEnable+1;
							}
						}
							// Enable Function 2013-02-13 E.Cilingir
						else if(ycnt == 3)
						{
							if (brsatoi((UDINT)&pMenu->Internal.ParserString)==0){
								MenuEntry->ShowEntryFunction2=LOW_SHOW;
							}
							else{
								MenuEntry->ShowEntryFunction2=HIGH_SHOW;
							}
						}
							/* Variable name */
						else if(ycnt == 4)
						{	
							Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->WorkVariableAdr), &(MenuEntry->WorkVariableLength));
							if(Status)
							{	pMenu->Internal.InitStep = FREE_MEMALLOC;
								pMenu->Out.Status = Status;
								return(FUNCTION_BUSY); 
							}

							/* Data type from variable */
							Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->WorkVariableType),&(MenuEntry->WorkVariableLength), &(DimVariable));
							if(Status)
							{	pMenu->Internal.InitStep = FREE_MEMALLOC;
								pMenu->Out.Status = Status;
								return(FUNCTION_BUSY); 
							}
						}
							/* Index number for unit description */
						else if(ycnt == 5)
						{
							//MenuEntry->UnitIndex = brsatoi((UDINT)&pMenu->Internal.ParserString);
							if((pMenu->Internal.ParserString[0] >= 48 && pMenu->Internal.ParserString[0] <= 57) || pMenu->Internal.ParserString[0] == 45 || pMenu->Internal.ParserString[0] == 42)
							{
								MenuEntry->UnitIndex = brsatoi((UDINT)&pMenu->Internal.ParserString);
							}
								/* variable */
							else
							{	Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->UnitIndexAdr), &(MenuEntry->UnitIndexLength));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->UnitIndexType),&(TempLength), &(DimVariable));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								else if(MenuEntry->UnitIndexType != VAR_TYPE_USINT && MenuEntry->UnitIndexType != VAR_TYPE_UINT)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = WRONG_TYPE_OF_SCALE_VAR;
									return(FUNCTION_BUSY); 
									pMenu->Out.Status	= WRONG_TYPE_OF_SCALE_VAR;
									return(FUNCTION_BUSY); 			
								}
							}
						}
							/* Index number for description */
						else if(ycnt == 6)
						{
							//MenuEntry->DescriptionIndex = brsatoi((UDINT)&pMenu->Internal.ParserString);
							if((pMenu->Internal.ParserString[0] >= 48 && pMenu->Internal.ParserString[0] <= 57) || pMenu->Internal.ParserString[0] == 45)
							{
								MenuEntry->DescriptionIndex = brsatoi((UDINT)&pMenu->Internal.ParserString);
							}
								/* variable */
							else
							{	Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->DescriptionIndexAdr), &(WorkVariableLength));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								/* Data type from variable */
								Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->DescriptionIndexType),&(MenuEntry->DescriptionIndexLength), &(DimVariable));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								else if(MenuEntry->DescriptionIndexType != VAR_TYPE_USINT && MenuEntry->DescriptionIndexType != VAR_TYPE_UINT)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = WRONG_TYPE_OF_SWITCH_VAR;
									return(FUNCTION_BUSY); 
									pMenu->Internal.ErrorNumber=(WRONG_TYPE_OF_SWITCH_VAR);	
								}
							}
						}
						if(ycnt == 7)
						{
							Status = strcmp("0", pMenu->Internal.ParserString);
							if (Status){
								MenuEntry->CalcFunctionMinMax=1;
								Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->CalcFunctionAdrVar), &(WorkVariableLength));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								/* Data type from enable variable */
								Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->CalcFunctionTypeVar),&(WorkVariableLength), &(DimVariable));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								else if(MenuEntry->CalcFunctionTypeVar!=MenuEntry->WorkVariableType)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = WRONG_TYPE_OF_CALC_FUNCTION_VAR;
									return(FUNCTION_BUSY); 
									pMenu->Out.Status	= WRONG_TYPE_OF_CALC_FUNCTION_VAR;
									return(FUNCTION_BUSY); 			
								}

							}
						}
							/* Fix data or variable name for minimum */
						else if(ycnt == 8)
						{	/* Fix data */
							if((pMenu->Internal.ParserString[0] >= 48 && pMenu->Internal.ParserString[0] <= 57) || pMenu->Internal.ParserString[0] == 45 || pMenu->Internal.ParserString[0] == 42)
							{
								MenuEntry->LimitsMinValue = brsatoi((UDINT)&pMenu->Internal.ParserString);
								MenuEntry->LimitsMinType = MenuEntry->WorkVariableType;
							}
								/* variable */
							else
							{	Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->LimitsMinAdr), &(WorkVariableLength));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->LimitsMinType),&(TempLength), &(DimVariable));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
							}
						}
							/* Fix data or variable name for maximum */
						else if(ycnt == 9)
						{	/* Fix data */
							if((pMenu->Internal.ParserString[0] >= 48 && pMenu->Internal.ParserString[0] <= 57) || pMenu->Internal.ParserString[0] == 45 )
							{
								MenuEntry->LimitsMaxValue = brsatoi((UDINT)&pMenu->Internal.ParserString);
								MenuEntry->LimitsMaxType = MenuEntry->WorkVariableType;
								// 2013-02-18 E.Cilingir Select Max Value out of the Variable if input Value is 0
								if (MenuEntry->LimitsMaxValue==0 && MenuEntry->CalcFunctionMinMax==0){	
									switch (MenuEntry->WorkVariableType){
										case VAR_TYPE_BOOL:		MenuEntry->LimitsMaxValue	=	DYN_MAX_VALUE_BOOL;		break;
										case VAR_TYPE_DINT:		MenuEntry->LimitsMaxValue	=	DYN_MAX_VALUE_DINT;		break;
										case VAR_TYPE_UDINT:	MenuEntry->LimitsMaxValue	=	DYN_MAX_VALUE_UDINT;	break;					
										case VAR_TYPE_INT:		MenuEntry->LimitsMaxValue	=	DYN_MAX_VALUE_INT;		break;					
										case VAR_TYPE_UINT:		MenuEntry->LimitsMaxValue	=	DYN_MAX_VALUE_UINT;		break;					
										case VAR_TYPE_SINT:		MenuEntry->LimitsMaxValue	=	DYN_MAX_VALUE_SINT;		break;					
										case VAR_TYPE_USINT:	MenuEntry->LimitsMaxValue	=	DYN_MAX_VALUE_USINT;	break;					
										case VAR_TYPE_REAL:		MenuEntry->LimitsMaxValue	=	DYN_MAX_VALUE_REAL;		break;				
										case VAR_TYPE_TIME:		MenuEntry->LimitsMaxValue	=	DYN_MAX_VALUE_TIME;		break;
									}
								}
							}
								/* variable */
							else
							{	Status= PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->LimitsMaxAdr), &(WorkVariableLength));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->LimitsMaxType),&(TempLength), &(DimVariable));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
							}
						}
			
						else if(ycnt == 10 && MenuEntry->CalcFunctionMinMax==1)
						{	/* Fix data */
							if((pMenu->Internal.ParserString[0] >= 48 && pMenu->Internal.ParserString[0] <= 57) || pMenu->Internal.ParserString[0] == 45 || pMenu->Internal.ParserString[0] == 42)
							{
								MenuEntry->CalcFunctionLimitsMinValue = brsatoi((UDINT)&pMenu->Internal.ParserString);
								MenuEntry->CalcFunctionLimitsMinType = MenuEntry->WorkVariableType;
							}
								/* variable */
							else
							{	Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->CalcFunctionLimitsMinAdr), &(WorkVariableLength));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->CalcFunctionLimitsMinType),&(TempLength), &(DimVariable));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
							}
						}
			
			
						else if(ycnt == 11 && MenuEntry->CalcFunctionMinMax==1)
						{	/* Fix data */
							if((pMenu->Internal.ParserString[0] >= 48 && pMenu->Internal.ParserString[0] <= 57) || pMenu->Internal.ParserString[0] == 45 || pMenu->Internal.ParserString[0] == 42)
							{
								MenuEntry->CalcFunctionLimitsMaxValue = brsatoi((UDINT)&pMenu->Internal.ParserString);
								MenuEntry->CalcFunctionLimitsMaxType = MenuEntry->WorkVariableType;
							}
								/* variable */
							else
							{	Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->CalcFunctionLimitsMaxAdr), &(WorkVariableLength));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->CalcFunctionLimitsMaxType),&(TempLength), &(DimVariable));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
							}
						}			
							// ADD Scale Value	E.Cilingir 2013-03-13
						else if(ycnt == 12)
						{
							//MenuEntry->ScaleValue = brsatoi((UDINT)&pMenu->Internal.ParserString);
							if((pMenu->Internal.ParserString[0] >= 48 && pMenu->Internal.ParserString[0] <= 57) || pMenu->Internal.ParserString[0] == 45 || pMenu->Internal.ParserString[0] == 42)
							{
								MenuEntry->ScaleValue = brsatoi((UDINT)&pMenu->Internal.ParserString);
							}
								/* variable */
							else
							{	Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->ScaleValueAdr), &(MenuEntry->ScaleValueLength));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->ScaleValueType),&(TempLength), &(DimVariable));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								else if(MenuEntry->ScaleValueType != VAR_TYPE_USINT && MenuEntry->ScaleValueType != VAR_TYPE_UINT)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = WRONG_TYPE_OF_SCALE_VAR;
									return(FUNCTION_BUSY); 
									pMenu->Out.Status	= WRONG_TYPE_OF_SCALE_VAR;
									return(FUNCTION_BUSY); 			
								}
							}	
						}	
						else if(ycnt == 13)
						{
							if((pMenu->Internal.ParserString[0] >= 48 && pMenu->Internal.ParserString[0] <= 57) || pMenu->Internal.ParserString[0] == 45 || pMenu->Internal.ParserString[0] == 42)
							{
								MenuEntry->AddFraction = brsatoi((UDINT)&pMenu->Internal.ParserString);
							}
								/* variable */
							else
							{	Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->AddFractionAdr), &(MenuEntry->AddFractionLength));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->AddFractionType),&(TempLength), &(DimVariable));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								else if(MenuEntry->AddFractionType != VAR_TYPE_USINT && MenuEntry->AddFractionType != VAR_TYPE_UINT)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = WRONG_TYPE_OF_FRACTION_VAR;
									return(FUNCTION_BUSY); 
									pMenu->Out.Status	= WRONG_TYPE_OF_FRACTION_VAR;
									return(FUNCTION_BUSY); 			
								}
							}
						}	
							/* Help ID */
						else if(ycnt == 14)
						{
							MenuEntry->HelpID = brsatoi((UDINT)&pMenu->Internal.ParserString);
						}						
							// ADD USER LEVEL FUNCTION INFORMATION	E.Cilingir 2013-02-06
						else if(ycnt == 15)
						{
							MenuEntry->Level = brsatoi((UDINT)&pMenu->Internal.ParserString);
						}
					}
					/* Increase number of Entries */
					pMenu->Internal.ActEntryNumber++;
					/* Search for next type definition */
					pMenu->Internal.InitStep = ENTRY_MARK;
					pMenu->Out.Status = FUNCTION_BUSY;
					return(pMenu->Out.Status);
				}
				//-------------------------------------------------------------------------------------------------------------
				//---------------------------------------- NUMMERIC OUTPUT ----------------------------------------------------
				Result = strcmp("#NO", pMenu->Internal.ParserString);
				if(!Result)
				{	MenuEntry->DynMenuEnryType = PD_NUMERIC_OUTPUT_INDEX;
					/* Check entries for numeric output */
					for(ycnt = 0; ycnt < ELEMENTS_FOR_PD_NO_ENTRY; ycnt++)
					{	/* Check data from file */
						for(xcnt = 0; xcnt < (MAX_PARSER_STRING - 1); xcnt++)
						{	*(UDINT*)&Parser = pMenu->Internal.ActAdrDataBuffer;
							/* Check for ',' or ';' --> indicates new element or end of line */
							if(*Parser == ',' || *Parser == ';')
							{	pMenu->Internal.ParserString[xcnt] = 0;
								pMenu->Internal.ActAdrDataBuffer++;
								break;
							}
								/* Copy data to ParserString */
							else
							{	pMenu->Internal.ParserString[xcnt]  = *Parser;
								pMenu->Internal.ActAdrDataBuffer++;
							}
						}
						/* Enable variable */
						if(!ycnt)
						{	Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->ShowEntryFunction1Adr), &(WorkVariableLength));
							if(Status)
							{	pMenu->Internal.InitStep = FREE_MEMALLOC;
								pMenu->Out.Status = Status;
								return(FUNCTION_BUSY); 
							}

							/* Data type from enable variable */
							Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(TypeEnableVariable),&(WorkVariableLength), &(DimVariable));
							if(Status)
							{	pMenu->Internal.InitStep = FREE_MEMALLOC;
								pMenu->Out.Status = Status;
								return(FUNCTION_BUSY); 
							}
							else if(TypeEnableVariable != VAR_TYPE_USINT && TypeEnableVariable != VAR_TYPE_UINT)
							{	pMenu->Internal.InitStep = FREE_MEMALLOC;
								pMenu->Out.Status = WRONG_TYPE_OF_ENABLE_VAR;
								return(FUNCTION_BUSY); 
								pMenu->Internal.ErrorNumber=(WRONG_TYPE_OF_ENABLE_VAR);	
							}
							MenuEntry->ShowEntryFunction1Type=TypeEnableVariable;
						}
							// Enable Function 2013-02-13 E.Cilingir
						else if(ycnt == 1)
						{
							if (brsatoi((UDINT)&pMenu->Internal.ParserString)==0){
								MenuEntry->ShowEntryFunction1=LOW_SHOW;
							}
							else{
								MenuEntry->ShowEntryFunction1=HIGH_SHOW;
							}
						}
						if(ycnt == 2)
						{
							Status = strcmp("0", pMenu->Internal.ParserString);
							if (Status){
								Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->ShowEntryFunction2Adr), &(WorkVariableLength));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								/* Data type from enable variable */
								Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(TypeEnableVariable),&(WorkVariableLength), &(DimVariable));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								else if(TypeEnableVariable != VAR_TYPE_USINT && TypeEnableVariable != VAR_TYPE_UINT)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = WRONG_TYPE_OF_ENABLE_VAR;
									return(FUNCTION_BUSY); 
									pMenu->Internal.ErrorNumber=(WRONG_TYPE_OF_ENABLE_VAR);	
								}
								MenuEntry->ShowEntryFunction2Type=TypeEnableVariable;
								MenuEntry->ShowEntryFunctionNumOfEnable=MenuEntry->ShowEntryFunctionNumOfEnable+1;
							}
						}
							// Enable Function 2013-02-13 E.Cilingir
						else if(ycnt == 3)
						{
							if (brsatoi((UDINT)&pMenu->Internal.ParserString)==0){
								MenuEntry->ShowEntryFunction2=LOW_SHOW;
							}
							else{
								MenuEntry->ShowEntryFunction2=HIGH_SHOW;
							}
						}
							/* Variable name */
						else if(ycnt == 4)
						{	Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->WorkVariableAdr), &(MenuEntry->WorkVariableLength));
							if(Status)
							{	pMenu->Internal.InitStep = FREE_MEMALLOC;
								pMenu->Out.Status = Status;
								return(FUNCTION_BUSY); 
							}

							/* Data type from variable */
							Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->WorkVariableType),&(MenuEntry->WorkVariableLength), &(DimVariable));
							if(Status)
							{	pMenu->Internal.InitStep = FREE_MEMALLOC;
								pMenu->Out.Status = Status;
								return(FUNCTION_BUSY); 
							}
						}
							/* Index number for unit description */
						else if(ycnt == 5)
						{
							//MenuEntry->UnitIndex = brsatoi((UDINT)&pMenu->Internal.ParserString);
							if((pMenu->Internal.ParserString[0] >= 48 && pMenu->Internal.ParserString[0] <= 57) || pMenu->Internal.ParserString[0] == 45 || pMenu->Internal.ParserString[0] == 42)
							{
								MenuEntry->UnitIndex = brsatoi((UDINT)&pMenu->Internal.ParserString);
							}
								/* variable */
							else
							{	Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->UnitIndexAdr), &(MenuEntry->UnitIndexLength));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->UnitIndexType),&(TempLength), &(DimVariable));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								else if(MenuEntry->UnitIndexType != VAR_TYPE_USINT && MenuEntry->UnitIndexType != VAR_TYPE_UINT)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = WRONG_TYPE_OF_SCALE_VAR;
									return(FUNCTION_BUSY); 
									pMenu->Out.Status	= WRONG_TYPE_OF_SCALE_VAR;
									return(FUNCTION_BUSY); 			
								}
							}
						}
							/* Index number for description */
						else if(ycnt == 6)
						{
							//MenuEntry->DescriptionIndex = brsatoi((UDINT)&pMenu->Internal.ParserString);
							if((pMenu->Internal.ParserString[0] >= 48 && pMenu->Internal.ParserString[0] <= 57) || pMenu->Internal.ParserString[0] == 45)
							{
								MenuEntry->DescriptionIndex = brsatoi((UDINT)&pMenu->Internal.ParserString);
							}
								/* variable */
							else
							{	Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->DescriptionIndexAdr), &(WorkVariableLength));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								/* Data type from variable */
								Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->DescriptionIndexType),&(MenuEntry->DescriptionIndexLength), &(DimVariable));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								else if(MenuEntry->DescriptionIndexType != VAR_TYPE_USINT && MenuEntry->DescriptionIndexType != VAR_TYPE_UINT)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = WRONG_TYPE_OF_SWITCH_VAR;
									return(FUNCTION_BUSY); 
									pMenu->Internal.ErrorNumber=(WRONG_TYPE_OF_SWITCH_VAR);	
								}
							}
						}
							// ADD Scale Value	E.Cilingir 2013-03-13
						else if(ycnt == 7)
						{
							//MenuEntry->ScaleValue = brsatoi((UDINT)&pMenu->Internal.ParserString);
							if((pMenu->Internal.ParserString[0] >= 48 && pMenu->Internal.ParserString[0] <= 57) || pMenu->Internal.ParserString[0] == 45 || pMenu->Internal.ParserString[0] == 42)
							{
								MenuEntry->ScaleValue = brsatoi((UDINT)&pMenu->Internal.ParserString);
							}
								/* variable */
							else
							{	Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->ScaleValueAdr), &(MenuEntry->ScaleValueLength));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->ScaleValueType),&(TempLength), &(DimVariable));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								else if(MenuEntry->ScaleValueType != VAR_TYPE_USINT && MenuEntry->ScaleValueType != VAR_TYPE_UINT)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = WRONG_TYPE_OF_SCALE_VAR;
									return(FUNCTION_BUSY); 
									pMenu->Out.Status	= WRONG_TYPE_OF_SCALE_VAR;
									return(FUNCTION_BUSY); 			
								}
							}
						}	
						else if(ycnt == 8)
						{
							if((pMenu->Internal.ParserString[0] >= 48 && pMenu->Internal.ParserString[0] <= 57) || pMenu->Internal.ParserString[0] == 45 || pMenu->Internal.ParserString[0] == 42)
							{
								MenuEntry->AddFraction = brsatoi((UDINT)&pMenu->Internal.ParserString);
							}
								/* variable */
							else
							{	Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->AddFractionAdr), &(MenuEntry->AddFractionLength));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->AddFractionType),&(TempLength), &(DimVariable));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								else if(MenuEntry->AddFractionType != VAR_TYPE_USINT && MenuEntry->AddFractionType != VAR_TYPE_UINT)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = WRONG_TYPE_OF_FRACTION_VAR;
									return(FUNCTION_BUSY); 
									pMenu->Out.Status	= WRONG_TYPE_OF_FRACTION_VAR;
									return(FUNCTION_BUSY); 			
								}
							}
						}	
							/* Help ID */
						else if(ycnt == 9)
						{
							MenuEntry->HelpID = brsatoi((UDINT)&pMenu->Internal.ParserString);
						}
							// ADD USER LEVEL FUNCTION INFORMATION	E.Cilingir 2013-02-06
						else if(ycnt == 10)
						{
							MenuEntry->Level = brsatoi((UDINT)&pMenu->Internal.ParserString);
						}
					}
					/* Increase number of Entries */
					pMenu->Internal.ActEntryNumber++;
					/* Search for next type definition */
					pMenu->Internal.InitStep = ENTRY_MARK;
					pMenu->Out.Status = FUNCTION_BUSY;
					return(pMenu->Out.Status);
				}
				//-------------------------------------------------------------------------------------------------------------
				//---------------------------------------- ALPHANUMMERIC INPUT ------------------------------------------------
				Result = strcmp("#AI", pMenu->Internal.ParserString);
				if(!Result)
				{	MenuEntry->DynMenuEnryType = PD_ALPHANUMERIC_INPUT_INDEX;
					/* Check entries for alphanumeric input */
					for(ycnt = 0; ycnt < ELEMENTS_FOR_PD_AI_ENTRY; ycnt++)
					{	/* Check data from file */
						for(xcnt = 0; xcnt < (MAX_PARSER_STRING - 1); xcnt++)
						{	*(UDINT*)&Parser = pMenu->Internal.ActAdrDataBuffer;
							/* Check for ',' or ';' --> indicates new element or end of line */
							if(*Parser == ',' || *Parser == ';')
							{	pMenu->Internal.ParserString[xcnt] = 0;
								pMenu->Internal.ActAdrDataBuffer++;
								break;
							}
								/* Copy data to ParserString */
							else
							{	pMenu->Internal.ParserString[xcnt] = *Parser;
								pMenu->Internal.ActAdrDataBuffer++;
							}
						}
						/* Enable variable */
						if(!ycnt)
						{	Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->ShowEntryFunction1Adr), &(WorkVariableLength));
							if(Status)
							{	pMenu->Internal.InitStep = FREE_MEMALLOC;
								pMenu->Out.Status = Status;
								return(FUNCTION_BUSY); 
							}

							/* Data type from enable variable */
							Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(TypeEnableVariable),&(WorkVariableLength), &(DimVariable));
							if(Status)
							{	pMenu->Internal.InitStep = FREE_MEMALLOC;
								pMenu->Out.Status = Status;
								return(FUNCTION_BUSY); 
							}
							else if(TypeEnableVariable != VAR_TYPE_USINT && TypeEnableVariable != VAR_TYPE_UINT)
							{	pMenu->Internal.InitStep = FREE_MEMALLOC;
								pMenu->Out.Status = WRONG_TYPE_OF_ENABLE_VAR;
								return(FUNCTION_BUSY); 
								pMenu->Internal.ErrorNumber=(WRONG_TYPE_OF_ENABLE_VAR);
							}
							MenuEntry->ShowEntryFunction1Type=TypeEnableVariable;
						}
							// Enable Function 2013-02-13 E.Cilingir
						else if(ycnt == 1)
						{
							if (brsatoi((UDINT)&pMenu->Internal.ParserString)==0){
								MenuEntry->ShowEntryFunction1=LOW_SHOW;
							}
							else{
								MenuEntry->ShowEntryFunction1=HIGH_SHOW;
							}
						}
						if(ycnt == 2)
						{
							Status = strcmp("0", pMenu->Internal.ParserString);
							if (Status){
								Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->ShowEntryFunction2Adr), &(WorkVariableLength));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								/* Data type from enable variable */
								Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(TypeEnableVariable),&(WorkVariableLength), &(DimVariable));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								else if(TypeEnableVariable != VAR_TYPE_USINT && TypeEnableVariable != VAR_TYPE_UINT)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = WRONG_TYPE_OF_ENABLE_VAR;
									return(FUNCTION_BUSY); 
									pMenu->Internal.ErrorNumber=(WRONG_TYPE_OF_ENABLE_VAR);
								}
								MenuEntry->ShowEntryFunction2Type=TypeEnableVariable;
								MenuEntry->ShowEntryFunctionNumOfEnable=MenuEntry->ShowEntryFunctionNumOfEnable+1;
							}
						}
							// Enable Function 2013-02-13 E.Cilingir
						else if(ycnt == 3)
						{
							if (brsatoi((UDINT)&pMenu->Internal.ParserString)==0){
								MenuEntry->ShowEntryFunction2=LOW_SHOW;
							}
							else{
								MenuEntry->ShowEntryFunction2=HIGH_SHOW;
							}
						}
							/* Variable name */
						else if(ycnt == 4)
						{	Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->WorkVariableAdr), &(MenuEntry->WorkVariableLength));
							if(Status)
							{	pMenu->Internal.InitStep = FREE_MEMALLOC;
								pMenu->Out.Status = Status;
								return(FUNCTION_BUSY); 
							}

							/* Data type from variable */
							Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->WorkVariableType),&(MenuEntry->WorkVariableLength), &(DimVariable));
							if(Status)
							{	pMenu->Internal.InitStep = FREE_MEMALLOC;
								pMenu->Out.Status = Status;
								return(FUNCTION_BUSY); 
							}
							else if(MenuEntry->WorkVariableType != VAR_TYPE_STRING)
							{	pMenu->Internal.InitStep = FREE_MEMALLOC;
								pMenu->Out.Status = WRONG_TYPE_OF_ALPHA_NUM_VAR;
								return(FUNCTION_BUSY); 
								pMenu->Internal.ErrorNumber=(WRONG_TYPE_OF_ALPHA_NUM_VAR);	 			
							}
						}
							/* Index number for unit description */
						else if(ycnt == 5)
						{
							//MenuEntry->UnitIndex = brsatoi((UDINT)&pMenu->Internal.ParserString);
							if((pMenu->Internal.ParserString[0] >= 48 && pMenu->Internal.ParserString[0] <= 57) || pMenu->Internal.ParserString[0] == 45 || pMenu->Internal.ParserString[0] == 42)
							{
								MenuEntry->UnitIndex = brsatoi((UDINT)&pMenu->Internal.ParserString);
							}
								/* variable */
							else
							{	Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->UnitIndexAdr), &(MenuEntry->UnitIndexLength));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->UnitIndexType),&(TempLength), &(DimVariable));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								else if(MenuEntry->UnitIndexType != VAR_TYPE_USINT && MenuEntry->UnitIndexType != VAR_TYPE_UINT)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = WRONG_TYPE_OF_SCALE_VAR;
									return(FUNCTION_BUSY); 
									pMenu->Out.Status	= WRONG_TYPE_OF_SCALE_VAR;
									return(FUNCTION_BUSY); 			
								}
							}
						}
							/* Index number for description */
						else if(ycnt == 6)
						{
							//MenuEntry->DescriptionIndex = brsatoi((UDINT)&pMenu->Internal.ParserString);
							if((pMenu->Internal.ParserString[0] >= 48 && pMenu->Internal.ParserString[0] <= 57) || pMenu->Internal.ParserString[0] == 45)
							{
								MenuEntry->DescriptionIndex = brsatoi((UDINT)&pMenu->Internal.ParserString);
							}
								/* variable */
							else
							{	Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->DescriptionIndexAdr), &(WorkVariableLength));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								/* Data type from variable */
								Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->DescriptionIndexType),&(MenuEntry->DescriptionIndexLength), &(DimVariable));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								else if(MenuEntry->DescriptionIndexType != VAR_TYPE_USINT && MenuEntry->DescriptionIndexType != VAR_TYPE_UINT)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = WRONG_TYPE_OF_SWITCH_VAR;
									return(FUNCTION_BUSY); 
									pMenu->Internal.ErrorNumber=(WRONG_TYPE_OF_SWITCH_VAR);	
								}
							}
						}
							/* Help ID */
						else if(ycnt == 7)
						{
							MenuEntry->HelpID = brsatoi((UDINT)&pMenu->Internal.ParserString);
						}
							// ADD USER LEVEL FUNCTION INFORMATION	E.Cilingir 2013-02-06
						else if(ycnt == 8)
						{
							MenuEntry->Level = brsatoi((UDINT)&pMenu->Internal.ParserString);
						}
					}
					/* Increase number of Entries */
					pMenu->Internal.ActEntryNumber++;
					/* Search for next type definition */
					pMenu->Internal.InitStep = ENTRY_MARK;
					pMenu->Out.Status = FUNCTION_BUSY;
					return(pMenu->Out.Status);
				}
				//-------------------------------------------------------------------------------------------------------------
				//---------------------------------------- ALPHANUMMERIC OUTPUT -----------------------------------------------
				Result = strcmp("#AO", pMenu->Internal.ParserString);
				if(!Result)
				{	MenuEntry->DynMenuEnryType = PD_ALPHANUMERIC_OUTPUT_INDEX;
					/* Check entries for alphanumeric output */
					for(ycnt = 0; ycnt < ELEMENTS_FOR_PD_AO_ENTRY; ycnt++)
					{	/* Check data from file */
						for(xcnt = 0; xcnt < (MAX_PARSER_STRING - 1); xcnt++)
						{	*(UDINT*)&Parser = pMenu->Internal.ActAdrDataBuffer;
							/* Check for ',' or ';' --> indicates new element or end of line */
							if(*Parser == ',' || *Parser == ';')
							{	pMenu->Internal.ParserString[xcnt] = 0;
								pMenu->Internal.ActAdrDataBuffer++;
								break;
							}
								/* Copy data to ParserString */
							else
							{	pMenu->Internal.ParserString[xcnt] = *Parser;
								pMenu->Internal.ActAdrDataBuffer++;
							}
						}
						/* Enable variable */
						if(!ycnt)
						{	Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->ShowEntryFunction1Adr), &(WorkVariableLength));
							if(Status)
							{	pMenu->Internal.InitStep = FREE_MEMALLOC;
								pMenu->Out.Status = Status;
								return(FUNCTION_BUSY); 
							}

							/* Data type from enable variable */
							Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(TypeEnableVariable),&(WorkVariableLength), &(DimVariable));
							if(Status)
							{	pMenu->Internal.InitStep = FREE_MEMALLOC;
								pMenu->Out.Status = Status;
								return(FUNCTION_BUSY); 
							}
							else if(TypeEnableVariable != VAR_TYPE_USINT && TypeEnableVariable != VAR_TYPE_UINT)
							{	pMenu->Internal.InitStep = FREE_MEMALLOC;
								pMenu->Out.Status = WRONG_TYPE_OF_ENABLE_VAR;
								return(FUNCTION_BUSY); 
								pMenu->Internal.ErrorNumber=(WRONG_TYPE_OF_ENABLE_VAR);
							}
							MenuEntry->ShowEntryFunction1Type=TypeEnableVariable;
						}
							// Enable Function 2013-02-13 E.Cilingir
						else if(ycnt == 1)
						{
							if (brsatoi((UDINT)&pMenu->Internal.ParserString)==0){
								MenuEntry->ShowEntryFunction1=LOW_SHOW;
							}
							else{
								MenuEntry->ShowEntryFunction1=HIGH_SHOW;
							}
						}
						if(ycnt == 2)
						{
							Status = strcmp("0", pMenu->Internal.ParserString);
							if (Status){
								Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->ShowEntryFunction2Adr), &(WorkVariableLength));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								/* Data type from enable variable */
								Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(TypeEnableVariable),&(WorkVariableLength), &(DimVariable));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								else if(TypeEnableVariable != VAR_TYPE_USINT && TypeEnableVariable != VAR_TYPE_UINT)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = WRONG_TYPE_OF_ENABLE_VAR;
									return(FUNCTION_BUSY); 
									pMenu->Internal.ErrorNumber=(WRONG_TYPE_OF_ENABLE_VAR);
								}
								MenuEntry->ShowEntryFunction2Type=TypeEnableVariable;
								MenuEntry->ShowEntryFunctionNumOfEnable=MenuEntry->ShowEntryFunctionNumOfEnable+1;
							}
						}
							// Enable Function 2013-02-13 E.Cilingir
						else if(ycnt == 3)
						{
							if (brsatoi((UDINT)&pMenu->Internal.ParserString)==0){
								MenuEntry->ShowEntryFunction2=LOW_SHOW;
							}
							else{
								MenuEntry->ShowEntryFunction2=HIGH_SHOW;
							}
						}
							/* Variable name */
						else if(ycnt == 4)	
						{
							if(pMenu->Internal.ParserString[0] == 48)
							{
								MenuEntry->WorkVariableAdr = 0;
								MenuEntry->WorkVariableType = VAR_TYPE_NONE;
							}
							else
							{						
								Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->WorkVariableAdr), &(MenuEntry->WorkVariableLength));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
	
								/* Data type from variable */
								Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->WorkVariableType),&(MenuEntry->WorkVariableLength), &(DimVariable));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								else if(MenuEntry->WorkVariableType != VAR_TYPE_USINT && MenuEntry->WorkVariableType != VAR_TYPE_UINT && MenuEntry->WorkVariableType != VAR_TYPE_STRING)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = WRONG_TYPE_OF_ALPHA_NUM_VAR;
									return(FUNCTION_BUSY); 
									pMenu->Internal.ErrorNumber=(WRONG_TYPE_OF_ALPHA_NUM_VAR);	
								 			
								}
							}
						}
							/* Index number for unit description */
						else if(ycnt == 5)
						{
							//MenuEntry->UnitIndex = brsatoi((UDINT)&pMenu->Internal.ParserString);
							if((pMenu->Internal.ParserString[0] >= 48 && pMenu->Internal.ParserString[0] <= 57) || pMenu->Internal.ParserString[0] == 45 || pMenu->Internal.ParserString[0] == 42)
							{
								MenuEntry->UnitIndex = brsatoi((UDINT)&pMenu->Internal.ParserString);
							}
								/* variable */
							else
							{	Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->UnitIndexAdr), &(MenuEntry->UnitIndexLength));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->UnitIndexType),&(TempLength), &(DimVariable));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								else if(MenuEntry->UnitIndexType != VAR_TYPE_USINT && MenuEntry->UnitIndexType != VAR_TYPE_UINT)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = WRONG_TYPE_OF_SCALE_VAR;
									return(FUNCTION_BUSY); 
									pMenu->Internal.ErrorNumber=(WRONG_TYPE_OF_SCALE_VAR);	
								 			
								}
							}
						}
							/* Index number for description */
						else if(ycnt == 6)
						{
							//MenuEntry->DescriptionIndex = brsatoi((UDINT)&pMenu->Internal.ParserString);
							if((pMenu->Internal.ParserString[0] >= 48 && pMenu->Internal.ParserString[0] <= 57) || pMenu->Internal.ParserString[0] == 45)
							{
								MenuEntry->DescriptionIndex = brsatoi((UDINT)&pMenu->Internal.ParserString);
							}
								/* variable */
							else
							{	Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->DescriptionIndexAdr), &(WorkVariableLength));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								/* Data type from variable */
								Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->DescriptionIndexType),&(MenuEntry->DescriptionIndexLength), &(DimVariable));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								else if(MenuEntry->DescriptionIndexType != VAR_TYPE_USINT && MenuEntry->DescriptionIndexType != VAR_TYPE_UINT)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = WRONG_TYPE_OF_SWITCH_VAR;
									return(FUNCTION_BUSY); 
									pMenu->Internal.ErrorNumber=(WRONG_TYPE_OF_SWITCH_VAR);	
								}
							}
						}
							/* Help ID */
						else if(ycnt == 7)
						{
							MenuEntry->HelpID = brsatoi((UDINT)&pMenu->Internal.ParserString);
						}
							//------------------------------------------------------------------------------------------------------------------------------------------------
							/* 													LEVEL 																						*/
							//------------------------------------------------------------------------------------------------------------------------------------------------
						else if(ycnt == 8)
						{
							MenuEntry->Level = brsatoi((UDINT)&pMenu->Internal.ParserString);
						}
					}
					/* Increase number of Entries */
					pMenu->Internal.ActEntryNumber++;
					/* Search for next type definition */
					pMenu->Internal.InitStep = ENTRY_MARK;
					pMenu->Out.Status = FUNCTION_BUSY;
					return(pMenu->Out.Status);
				}
				//-------------------------------------------------------------------------------------------------------------
				//---------------------------------------- SENSING DEVICE -----------------------------------------------------
				Result = strcmp("#SD", pMenu->Internal.ParserString);
				if(!Result)
				{	MenuEntry->DynMenuEnryType = PD_SENSING_DEVICE_INDEX;
					/* Check entries for sensing device */
					for(ycnt = 0; ycnt < ELEMENTS_FOR_PD_SD_ENTRY; ycnt++)
					{	/* Check data from file */
						for(xcnt = 0; xcnt < (MAX_PARSER_STRING - 1); xcnt++)
						{	*(UDINT*)&Parser = pMenu->Internal.ActAdrDataBuffer;
							/* Check for ',' or ';' --> indicates new element or end of line */
							if(*Parser == ',' || *Parser == ';')
							{	pMenu->Internal.ParserString[xcnt] = 0;
								pMenu->Internal.ActAdrDataBuffer++;
								break;
							}
								/* Copy data to ParserString */
							else
							{	pMenu->Internal.ParserString[xcnt] = *Parser;
								pMenu->Internal.ActAdrDataBuffer++;
							}
						}
						/* Enable variable */
						if(!ycnt)
						{	Status  = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->ShowEntryFunction1Adr), &(WorkVariableLength));
							if(Status)
							{	pMenu->Internal.InitStep = FREE_MEMALLOC;
								pMenu->Out.Status = Status;
								return(FUNCTION_BUSY); 
							}

							/* Data type from enable variable */
							Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(TypeEnableVariable),&(WorkVariableLength), &(DimVariable));
							if(Status)
							{	pMenu->Internal.InitStep = FREE_MEMALLOC;
								pMenu->Out.Status = Status;
								return(FUNCTION_BUSY); 
							}
							else if(TypeEnableVariable != VAR_TYPE_USINT && TypeEnableVariable != VAR_TYPE_UINT)
							{	pMenu->Internal.InitStep = FREE_MEMALLOC;
								pMenu->Out.Status = WRONG_TYPE_OF_ENABLE_VAR;
								return(FUNCTION_BUSY); 
								pMenu->Internal.ErrorNumber=(WRONG_TYPE_OF_ENABLE_VAR);
							}
							MenuEntry->ShowEntryFunction1Type=TypeEnableVariable;
						}
							// Enable Function 2013-02-13 E.Cilingir
						else if(ycnt == 1)
						{
							if (brsatoi((UDINT)&pMenu->Internal.ParserString)==0){
								MenuEntry->ShowEntryFunction1=LOW_SHOW;
							}
							else{
								MenuEntry->ShowEntryFunction1=HIGH_SHOW;
							}
						}
						if(ycnt == 2)
						{
							Status = strcmp("0", pMenu->Internal.ParserString);
							if (Status){
								Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->ShowEntryFunction2Adr), &(WorkVariableLength));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								/* Data type from enable variable */
								Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(TypeEnableVariable),&(WorkVariableLength), &(DimVariable));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								else if(TypeEnableVariable != VAR_TYPE_USINT && TypeEnableVariable != VAR_TYPE_UINT)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = WRONG_TYPE_OF_ENABLE_VAR;
									return(FUNCTION_BUSY); 
									pMenu->Internal.ErrorNumber=(WRONG_TYPE_OF_ENABLE_VAR);
								}
								MenuEntry->ShowEntryFunction2Type=TypeEnableVariable;
								MenuEntry->ShowEntryFunctionNumOfEnable=MenuEntry->ShowEntryFunctionNumOfEnable+1;
							}
						}	
							// Enable Function 2013-02-13 E.Cilingir
						else if(ycnt == 3)
						{
							if (brsatoi((UDINT)&pMenu->Internal.ParserString)==0){
								MenuEntry->ShowEntryFunction2=LOW_SHOW;
							}
							else{
								MenuEntry->ShowEntryFunction2=HIGH_SHOW;
							}
						}
							/* Variable name */
						else if(ycnt == 4)
						{	Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->WorkVariableAdr), &(MenuEntry->WorkVariableLength));
							if(Status)
							{	pMenu->Internal.InitStep = FREE_MEMALLOC;
								pMenu->Out.Status = Status;
								return(FUNCTION_BUSY); 
							}

							/* Data type from variable */
							Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->WorkVariableType),&(MenuEntry->WorkVariableLength), &(DimVariable));
							if(Status)
							{	pMenu->Internal.InitStep = FREE_MEMALLOC;
								pMenu->Out.Status = Status;
								return(FUNCTION_BUSY); 
							}
							else if(MenuEntry->WorkVariableType != VAR_TYPE_USINT && MenuEntry->WorkVariableType != VAR_TYPE_BOOL)
							{	pMenu->Internal.InitStep = FREE_MEMALLOC;
								pMenu->Out.Status = WRONG_TYPE_OF_SENSING_VAR;
								return(FUNCTION_BUSY); 
								pMenu->Internal.ErrorNumber=(WRONG_TYPE_OF_SENSING_VAR);
							}
						}
							/* Index number for unit description */
						else if(ycnt == 5)
						{
							//MenuEntry->UnitIndex = brsatoi((UDINT)&pMenu->Internal.ParserString);
							if((pMenu->Internal.ParserString[0] >= 48 && pMenu->Internal.ParserString[0] <= 57) || pMenu->Internal.ParserString[0] == 45 || pMenu->Internal.ParserString[0] == 42)
							{
								MenuEntry->UnitIndex = brsatoi((UDINT)&pMenu->Internal.ParserString);
							}
								/* variable */
							else
							{	Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->UnitIndexAdr), &(MenuEntry->UnitIndexLength));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->UnitIndexType),&(TempLength), &(DimVariable));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								else if(MenuEntry->UnitIndexType != VAR_TYPE_USINT && MenuEntry->UnitIndexType != VAR_TYPE_UINT)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = WRONG_TYPE_OF_SCALE_VAR;
									return(FUNCTION_BUSY); 
									pMenu->Out.Status	= WRONG_TYPE_OF_SCALE_VAR;
									return(FUNCTION_BUSY); 			
								}
							}
						}
							/* Index number for description */
						else if(ycnt == 6)
						{
							//MenuEntry->DescriptionIndex = brsatoi((UDINT)&pMenu->Internal.ParserString);
							if((pMenu->Internal.ParserString[0] >= 48 && pMenu->Internal.ParserString[0] <= 57) || pMenu->Internal.ParserString[0] == 45)
							{
								MenuEntry->DescriptionIndex = brsatoi((UDINT)&pMenu->Internal.ParserString);
							}
								/* variable */
							else
							{	Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->DescriptionIndexAdr), &(WorkVariableLength));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								/* Data type from variable */
								Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->DescriptionIndexType),&(MenuEntry->DescriptionIndexLength), &(DimVariable));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								else if(MenuEntry->DescriptionIndexType != VAR_TYPE_USINT && MenuEntry->DescriptionIndexType != VAR_TYPE_UINT)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = WRONG_TYPE_OF_SWITCH_VAR;
									return(FUNCTION_BUSY); 
									pMenu->Internal.ErrorNumber=(WRONG_TYPE_OF_SWITCH_VAR);	
								}
							}
						}
							/* Index for button released text */
						else if(ycnt == 7)
						{
							MenuEntry->ButtonReleasedTextIndex = brsatoi((UDINT)&pMenu->Internal.ParserString);
						}
							/* Index for button pressed text */
						else if(ycnt == 8)
						{
							MenuEntry->ButtonPressedTextIndex = brsatoi((UDINT)&pMenu->Internal.ParserString);
						}
							/* Help ID */
						else if(ycnt == 9)
						{
							MenuEntry->HelpID = brsatoi((UDINT)&pMenu->Internal.ParserString);
						}
							// ADD USER LEVEL FUNCTION INFORMATION	E.Cilingir 2013-02-06
						else if(ycnt == 10)
						{
							MenuEntry->Level = brsatoi((UDINT)&pMenu->Internal.ParserString);
						}
					}
					/* Increase number of Entries */
					pMenu->Internal.ActEntryNumber++;
					/* Search for next type definition */
					pMenu->Internal.InitStep = ENTRY_MARK;
					pMenu->Out.Status = FUNCTION_BUSY;
					return(pMenu->Out.Status);
				}
				//-------------------------------------------------------------------------------------------------------------
				//---------------------------------------- SWITCH -------------------------------------------------------------
				Result = strcmp("#SW", pMenu->Internal.ParserString);
				if(!Result)
				{	MenuEntry->DynMenuEnryType = PD_SWITCH_INDEX;
					/* Check entries for switch */
					for(ycnt = 0; ycnt < ELEMENTS_FOR_PD_SW_ENTRY; ycnt++)
					{	/* Check data from file */
						for(xcnt = 0; xcnt < (MAX_PARSER_STRING - 1); xcnt++)
						{	*(UDINT*)&Parser = pMenu->Internal.ActAdrDataBuffer;
							/* Check for ',' or ';' --> indicates new element or end of line */
							if(*Parser == ',' || *Parser == ';')
							{	pMenu->Internal.ParserString[xcnt] = 0;
								pMenu->Internal.ActAdrDataBuffer++;
								break;
							}
								/* Copy data to ParserString */
							else
							{	pMenu->Internal.ParserString[xcnt] = *Parser;
								pMenu->Internal.ActAdrDataBuffer++;
							}
						}
						/* Enable variable */
						if(!ycnt)
						{	Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->ShowEntryFunction1Adr), &(WorkVariableLength));
							if(Status)
							{	pMenu->Internal.InitStep = FREE_MEMALLOC;
								pMenu->Out.Status = Status;
								return(FUNCTION_BUSY); 
							}

							/* Data type from enable variable */
							Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(TypeEnableVariable),&(WorkVariableLength), &(DimVariable));
							if(Status)
							{	pMenu->Internal.InitStep = FREE_MEMALLOC;
								pMenu->Out.Status = Status;
								return(FUNCTION_BUSY); 
							}
							else if(TypeEnableVariable != VAR_TYPE_USINT && TypeEnableVariable != VAR_TYPE_UINT)
							{	pMenu->Internal.InitStep = FREE_MEMALLOC;
								pMenu->Out.Status = WRONG_TYPE_OF_ENABLE_VAR;
								return(FUNCTION_BUSY); 
								pMenu->Internal.ErrorNumber=(WRONG_TYPE_OF_ENABLE_VAR);
							}
							MenuEntry->ShowEntryFunction1Type=TypeEnableVariable;
						}
							// Enable Function 2013-02-13 E.Cilingir
						else if(ycnt == 1)
						{
							if (brsatoi((UDINT)&pMenu->Internal.ParserString)==0){
								MenuEntry->ShowEntryFunction1=LOW_SHOW;
							}
							else{
								MenuEntry->ShowEntryFunction1=HIGH_SHOW;
							}
						}
						if(ycnt == 2)
						{
							Status = strcmp("0", pMenu->Internal.ParserString);
							if (Status){
								Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->ShowEntryFunction2Adr), &(WorkVariableLength));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								/* Data type from enable variable */
								Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(TypeEnableVariable),&(WorkVariableLength), &(DimVariable));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								else if(TypeEnableVariable != VAR_TYPE_USINT && TypeEnableVariable != VAR_TYPE_UINT)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = WRONG_TYPE_OF_ENABLE_VAR;
									return(FUNCTION_BUSY); 
									pMenu->Internal.ErrorNumber=(WRONG_TYPE_OF_ENABLE_VAR);
								}
								MenuEntry->ShowEntryFunction2Type=TypeEnableVariable;
								MenuEntry->ShowEntryFunctionNumOfEnable=MenuEntry->ShowEntryFunctionNumOfEnable+1;
							}
						}
							// Enable Function 2013-02-13 E.Cilingir
						else if(ycnt == 3)
						{
							if (brsatoi((UDINT)&pMenu->Internal.ParserString)==0){
								MenuEntry->ShowEntryFunction2=LOW_SHOW;
							}
							else{
								MenuEntry->ShowEntryFunction2=HIGH_SHOW;
							}
						}
							/* Variable name */
						else if(ycnt == 4)
						{	Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->WorkVariableAdr), &(MenuEntry->WorkVariableLength));
							if(Status)
							{	pMenu->Internal.InitStep = FREE_MEMALLOC;
								pMenu->Out.Status = Status;
								return(FUNCTION_BUSY); 
							}

							/* Data type from variable */
							Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->WorkVariableType),&(MenuEntry->WorkVariableLength), &(DimVariable));
							if(Status)
							{	pMenu->Internal.InitStep = FREE_MEMALLOC;
								pMenu->Out.Status = Status;
								return(FUNCTION_BUSY); 
							}
							else if(MenuEntry->WorkVariableType != VAR_TYPE_USINT && MenuEntry->WorkVariableType != VAR_TYPE_BOOL)
							{	pMenu->Internal.InitStep = FREE_MEMALLOC;
								pMenu->Out.Status = WRONG_TYPE_OF_SWITCH_VAR;
								return(FUNCTION_BUSY); 
								pMenu->Internal.ErrorNumber=(WRONG_TYPE_OF_SWITCH_VAR);
							}
						}
							/* Index number for unit description */
						else if(ycnt == 5)
						{
							//MenuEntry->UnitIndex = brsatoi((UDINT)&pMenu->Internal.ParserString);
							if((pMenu->Internal.ParserString[0] >= 48 && pMenu->Internal.ParserString[0] <= 57) || pMenu->Internal.ParserString[0] == 45 || pMenu->Internal.ParserString[0] == 42)
							{
								MenuEntry->UnitIndex = brsatoi((UDINT)&pMenu->Internal.ParserString);
							}
								/* variable */
							else
							{	Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->UnitIndexAdr), &(MenuEntry->UnitIndexLength));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->UnitIndexType),&(TempLength), &(DimVariable));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								else if(MenuEntry->UnitIndexType != VAR_TYPE_USINT && MenuEntry->UnitIndexType != VAR_TYPE_UINT)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = WRONG_TYPE_OF_SCALE_VAR;
									return(FUNCTION_BUSY); 
									pMenu->Out.Status	= WRONG_TYPE_OF_SCALE_VAR;
									return(FUNCTION_BUSY); 			
								}
							}
						}
							/* Index number for description */
						else if(ycnt == 6)
						{
							//MenuEntry->DescriptionIndex = brsatoi((UDINT)&pMenu->Internal.ParserString);
							if((pMenu->Internal.ParserString[0] >= 48 && pMenu->Internal.ParserString[0] <= 57) || pMenu->Internal.ParserString[0] == 45)
							{
								MenuEntry->DescriptionIndex = brsatoi((UDINT)&pMenu->Internal.ParserString);
							}
								/* variable */
							else
							{	Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->DescriptionIndexAdr), &(WorkVariableLength));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								/* Data type from variable */
								Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->DescriptionIndexType),&(MenuEntry->DescriptionIndexLength), &(DimVariable));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								else if(MenuEntry->DescriptionIndexType != VAR_TYPE_USINT && MenuEntry->DescriptionIndexType != VAR_TYPE_UINT)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = WRONG_TYPE_OF_SWITCH_VAR;
									return(FUNCTION_BUSY); 
									pMenu->Internal.ErrorNumber=(WRONG_TYPE_OF_SWITCH_VAR);	
								}
							}
						}
							/* Index for button released text */
						else if(ycnt == 7)
						{
							MenuEntry->ButtonReleasedTextIndex = brsatoi((UDINT)&pMenu->Internal.ParserString);
						}
							/* Index for button pressed text */
						else if(ycnt == 8)
						{
							MenuEntry->ButtonPressedTextIndex = brsatoi((UDINT)&pMenu->Internal.ParserString);
						}
							/* Help ID */
						else if(ycnt == 9)
						{
							MenuEntry->HelpID = brsatoi((UDINT)&pMenu->Internal.ParserString);
						}
							// ADD USER LEVEL FUNCTION INFORMATION	E.Cilingir 2013-02-06
						else if(ycnt == 10)
						{
							MenuEntry->Level = brsatoi((UDINT)&pMenu->Internal.ParserString);
						}
					}
					/* Increase number of Entries */
					pMenu->Internal.ActEntryNumber++;
					/* Search for next type definition */
					pMenu->Internal.InitStep = ENTRY_MARK;
					pMenu->Out.Status = FUNCTION_BUSY;
					return(pMenu->Out.Status);
				}
				//-------------------------------------------------------------------------------------------------------------
				//---------------------------------------- FUNCTION HEADLINE --------------------------------------------------
				Result = strcmp("#FH", pMenu->Internal.ParserString);
				if(!Result)
				{	MenuEntry->DynMenuEnryType = PD_FUNCTIONHEADLINE_INDEX;
					/* Check entries for switch */
					for(ycnt = 0; ycnt < ELEMENTS_FOR_PD_FH_ENTRY; ycnt++)
					{	/* Check data from file */
						for(xcnt = 0; xcnt < (MAX_PARSER_STRING - 1); xcnt++)
						{	*(UDINT*)&Parser = pMenu->Internal.ActAdrDataBuffer;
							/* Check for ',' or ';' --> indicates new element or end of line */
							if(*Parser == ',' || *Parser == ';')
							{	pMenu->Internal.ParserString[xcnt] = 0;
								pMenu->Internal.ActAdrDataBuffer++;
								break;
							}
								/* Copy data to ParserString */
							else
							{	pMenu->Internal.ParserString[xcnt] = *Parser;
								pMenu->Internal.ActAdrDataBuffer++;
							}
						}
						/* Enable variable */
						if(!ycnt)
						{	Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->ShowEntryFunction1Adr), &(WorkVariableLength));
							if(Status)
							{	pMenu->Internal.InitStep = FREE_MEMALLOC;
								pMenu->Out.Status = Status;
								return(FUNCTION_BUSY); 
							}

							/* Data type from enable variable */
							Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(TypeEnableVariable),&(WorkVariableLength), &(DimVariable));
							if(Status)
							{	pMenu->Internal.InitStep = FREE_MEMALLOC;
								pMenu->Out.Status = Status;
								return(FUNCTION_BUSY); 
							}
							else if(TypeEnableVariable != VAR_TYPE_USINT && TypeEnableVariable != VAR_TYPE_UINT)
							{	pMenu->Internal.InitStep = FREE_MEMALLOC;
								pMenu->Out.Status = WRONG_TYPE_OF_ENABLE_VAR;
								return(FUNCTION_BUSY); 
								pMenu->Internal.ErrorNumber=(WRONG_TYPE_OF_ENABLE_VAR);
							}
							MenuEntry->ShowEntryFunction1Type=TypeEnableVariable;
							TempEnableType1=TypeEnableVariable;
							TempAdrEnable1=MenuEntry->ShowEntryFunction1Adr;
						}
							// Enable Function 2013-02-13 E.Cilingir
						else if(ycnt == 1)
						{
							if (brsatoi((UDINT)&pMenu->Internal.ParserString)==0){
								MenuEntry->ShowEntryFunction1=LOW_SHOW;
							}
							else{
								MenuEntry->ShowEntryFunction1=HIGH_SHOW;
							}
							TempShowLogic1=MenuEntry->ShowEntryFunction1;

						}
						if(ycnt == 2)
						{
							Status = strcmp("0", pMenu->Internal.ParserString);
							if (Status){
								Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->ShowEntryFunction2Adr), &(WorkVariableLength));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								/* Data type from enable variable */
								Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(TypeEnableVariable),&(WorkVariableLength), &(DimVariable));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								else if(TypeEnableVariable != VAR_TYPE_USINT && TypeEnableVariable != VAR_TYPE_UINT)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = WRONG_TYPE_OF_ENABLE_VAR;
									return(FUNCTION_BUSY); 
									pMenu->Internal.ErrorNumber=(WRONG_TYPE_OF_ENABLE_VAR);
								}
								MenuEntry->ShowEntryFunction2Type=TypeEnableVariable;
								MenuEntry->ShowEntryFunctionNumOfEnable=MenuEntry->ShowEntryFunctionNumOfEnable+1;
								TempNumOfEnable=MenuEntry->ShowEntryFunctionNumOfEnable;
								TempEnableType2=TypeEnableVariable;
								TempAdrEnable2=MenuEntry->ShowEntryFunction2Adr;
							}
						}
							// Enable Function 2013-02-13 E.Cilingir
						else if(ycnt == 3)
						{
							if (brsatoi((UDINT)&pMenu->Internal.ParserString)==0){
								MenuEntry->ShowEntryFunction2=LOW_SHOW;
							}
							else{
								MenuEntry->ShowEntryFunction2=HIGH_SHOW;
							}
							TempShowLogic2=MenuEntry->ShowEntryFunction2;
						}
							/* Variable name On/Off */
						else if(ycnt == 4)
						{	
							Status = strcmp("-1", pMenu->Internal.ParserString);
							if (Status){
								Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->WorkVariableAdr), &(MenuEntry->WorkVariableLength));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
	
								/* Data type from variable */
								Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->WorkVariableType),&(MenuEntry->WorkVariableLength), &(DimVariable));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								else if(MenuEntry->WorkVariableType != VAR_TYPE_USINT && MenuEntry->WorkVariableType != VAR_TYPE_BOOL)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = WRONG_TYPE_OF_SWITCH_VAR;
									return(FUNCTION_BUSY); 
									pMenu->Internal.ErrorNumber=(WRONG_TYPE_OF_SWITCH_VAR);								
								}
							
							}
						}
							/* Variable name Status Line */
						else if(ycnt == 5)
						{	
							Status = strcmp("-1", pMenu->Internal.ParserString);
							if (Status){
								Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->FunctionHeadlineStatusAdr), &(MenuEntry->FunctionHeadlineStatusLength));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}

								/* Data type from variable */
								Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->FunctionHeadlineStatusType),&(MenuEntry->FunctionHeadlineStatusLength), &(DimVariable));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								else if(MenuEntry->FunctionHeadlineStatusType != VAR_TYPE_USINT && MenuEntry->FunctionHeadlineStatusType != VAR_TYPE_UINT)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = WRONG_TYPE_OF_SWITCH_VAR;
									return(FUNCTION_BUSY); 
									pMenu->Internal.ErrorNumber=(WRONG_TYPE_OF_SWITCH_VAR);	
								}
							}
							else
							{
								MenuEntry->FunctionHeadlineStatusAdr=0;
							}
						}						
						else if(ycnt == 6)
						{
							if((pMenu->Internal.ParserString[0] >= 48 && pMenu->Internal.ParserString[0] <= 57) || pMenu->Internal.ParserString[0] == 45)
							{
								MenuEntry->DescriptionIndex = brsatoi((UDINT)&pMenu->Internal.ParserString);
							}
								/* variable */
							else
							{	Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->DescriptionIndexAdr), &(WorkVariableLength));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								/* Data type from variable */
								Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->DescriptionIndexType),&(MenuEntry->DescriptionIndexLength), &(DimVariable));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								else if(MenuEntry->DescriptionIndexType != VAR_TYPE_USINT && MenuEntry->DescriptionIndexType != VAR_TYPE_UINT)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = WRONG_TYPE_OF_SWITCH_VAR;
									return(FUNCTION_BUSY); 
									pMenu->Internal.ErrorNumber=(WRONG_TYPE_OF_SWITCH_VAR);	
								}
							}
						}
						else if(ycnt == 7)
						{
							MenuEntry->FunctionHeadlineBitmap = brsatoi((UDINT)&pMenu->Internal.ParserString);
						}
						else if(ycnt == 8)
						{
							MenuEntry->HelpID = brsatoi((UDINT)&pMenu->Internal.ParserString);
						}
						else if(ycnt == 9)
						{
							MenuEntry->Level = brsatoi((UDINT)&pMenu->Internal.ParserString);
							MenuEntry->LevelStatus = brsatoi((UDINT)&pMenu->Internal.ParserString);
							MenuEntry->LevelButton = brsatoi((UDINT)&pMenu->Internal.ParserString);
							TempLevel=MenuEntry->Level;
						}
						else if(ycnt == 10)
						{
							Status = strcmp("-1", pMenu->Internal.ParserString);
							if (Status){
								MenuEntry->LevelStatus = brsatoi((UDINT)&pMenu->Internal.ParserString);
							}
						}
						else if(ycnt == 11)
						{
							Status = strcmp("-1", pMenu->Internal.ParserString);
							if (Status){
								MenuEntry->LevelButton = brsatoi((UDINT)&pMenu->Internal.ParserString);
							}
						}
					}
					/* Increase number of Entries */
					AdrEnableFunctionHeadLine=MenuEntry->ShowEntryFunction1Adr;
					pMenu->Internal.ActEntryNumber++;
					// Put in a Empty Line for the Function HeadLine
					*(UDINT*)&MenuEntry = (AdrMenuEntryStructure + (sizeof(*MenuEntry)*pMenu->Internal.ActEntryNumber));
					MenuEntry->ShowEntryFunction1Adr=AdrEnableFunctionHeadLine;
					MenuEntry->DynMenuEnryType = PD_EMPTYLINE_INDEX;
					MenuEntry->Level = TempLevel;
					MenuEntry->ShowEntryFunction1=EnableFunctionType;
			
					MenuEntry->ShowEntryFunction1Type = 	TempEnableType1;
					MenuEntry->ShowEntryFunction2Type = 	TempEnableType2;
			
					MenuEntry->ShowEntryFunction1Adr=TempAdrEnable1;			
					MenuEntry->ShowEntryFunction2Adr=TempAdrEnable2;			
				
					MenuEntry->ShowEntryFunctionNumOfEnable=TempNumOfEnable;
			
					MenuEntry->ShowEntryFunction1=TempShowLogic1;
					MenuEntry->ShowEntryFunction2=TempShowLogic2;
					pMenu->Internal.ActEntryNumber++;
					/* Search for next type definition */
					pMenu->Internal.InitStep = ENTRY_MARK;
					pMenu->Out.Status = FUNCTION_BUSY;
					return(pMenu->Out.Status);
				}
				//-------------------------------------------------------------------------------------------------------------
				//---------------------------------------- CHECKBOX -----------------------------------------------------------
				Result = strcmp("#CB", pMenu->Internal.ParserString);
				if(!Result)
				{	MenuEntry->DynMenuEnryType = PD_CHECKBOX_INDEX;
					/* Check entries for checkbox */
					for(ycnt = 0; ycnt < ELEMENTS_FOR_PD_CB_ENTRY; ycnt++)
					{	/* Check data from file */
						for(xcnt = 0; xcnt < (MAX_PARSER_STRING - 1); xcnt++)
						{	*(UDINT*)&Parser = pMenu->Internal.ActAdrDataBuffer;
							/* Check for ',' or ';' --> indicates new element or end of line */
							if(*Parser == ',' || *Parser == ';')
							{	pMenu->Internal.ParserString[xcnt] = 0;
								pMenu->Internal.ActAdrDataBuffer++;
								break;
							}
								/* Copy data to ParserString */
							else
							{	pMenu->Internal.ParserString[xcnt] = *Parser;
								pMenu->Internal.ActAdrDataBuffer++;
							}
						}
						/* Enable variable */
						if(!ycnt)
						{	Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->ShowEntryFunction1Adr), &(WorkVariableLength));
							if(Status)
							{	pMenu->Internal.InitStep = FREE_MEMALLOC;
								pMenu->Out.Status = Status;
								return(FUNCTION_BUSY); 
							}

							/* Data type from enable variable */
							Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(TypeEnableVariable),&(WorkVariableLength), &(DimVariable));
							if(Status)
							{	pMenu->Internal.InitStep = FREE_MEMALLOC;
								pMenu->Out.Status = Status;
								return(FUNCTION_BUSY); 
							}
							else if(TypeEnableVariable != VAR_TYPE_USINT && TypeEnableVariable != VAR_TYPE_UINT)
							{	pMenu->Internal.InitStep = FREE_MEMALLOC;
								pMenu->Out.Status = WRONG_TYPE_OF_ENABLE_VAR;
								return(FUNCTION_BUSY); 
								pMenu->Internal.ErrorNumber=(WRONG_TYPE_OF_ENABLE_VAR);	
							}
							MenuEntry->ShowEntryFunction1Type=TypeEnableVariable;
						}
							// Enable Function 2013-02-13 E.Cilingir
						else if(ycnt == 1)
						{
							if (brsatoi((UDINT)&pMenu->Internal.ParserString)==0){
								MenuEntry->ShowEntryFunction1=LOW_SHOW;
							}
							else{
								MenuEntry->ShowEntryFunction1=HIGH_SHOW;
							}
						}
						if(ycnt == 2)
						{
							Status = strcmp("0", pMenu->Internal.ParserString);
							if (Status){
								Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->ShowEntryFunction2Adr), &(WorkVariableLength));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								/* Data type from enable variable */
								Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(TypeEnableVariable),&(WorkVariableLength), &(DimVariable));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								else if(TypeEnableVariable != VAR_TYPE_USINT && TypeEnableVariable != VAR_TYPE_UINT)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = WRONG_TYPE_OF_ENABLE_VAR;
									return(FUNCTION_BUSY); 
									pMenu->Internal.ErrorNumber=(WRONG_TYPE_OF_ENABLE_VAR);	
								}
								MenuEntry->ShowEntryFunction2Type=TypeEnableVariable;
								MenuEntry->ShowEntryFunctionNumOfEnable=MenuEntry->ShowEntryFunctionNumOfEnable+1;
							}
						}
							// Enable Function 2013-02-13 E.Cilingir
						else if(ycnt == 3)
						{
							if (brsatoi((UDINT)&pMenu->Internal.ParserString)==0){
								MenuEntry->ShowEntryFunction2=LOW_SHOW;
							}
							else{
								MenuEntry->ShowEntryFunction2=HIGH_SHOW;
							}
						}
							/* Variable name */
						else if(ycnt == 4)
						{	Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->WorkVariableAdr), &(MenuEntry->WorkVariableLength));
							if(Status)
							{	pMenu->Internal.InitStep = FREE_MEMALLOC;
								pMenu->Out.Status = Status;
								return(FUNCTION_BUSY); 
							}

							/* Data type from variable */
							Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->WorkVariableType),&(MenuEntry->WorkVariableLength), &(DimVariable));
							if(Status)
							{	pMenu->Internal.InitStep = FREE_MEMALLOC;
								pMenu->Out.Status = Status;
								return(FUNCTION_BUSY); 
							}
							else if(MenuEntry->WorkVariableType != VAR_TYPE_USINT && MenuEntry->WorkVariableType != VAR_TYPE_BOOL)
							{	pMenu->Internal.InitStep = FREE_MEMALLOC;
								pMenu->Out.Status = WRONG_TYPE_OF_CHECKBOX;
								return(FUNCTION_BUSY); 
								pMenu->Internal.ErrorNumber=(WRONG_TYPE_OF_CHECKBOX);	
							}
						}
							/* Index number for unit description */
						else if(ycnt == 5)
						{
							//MenuEntry->UnitIndex = brsatoi((UDINT)&pMenu->Internal.ParserString);
							if((pMenu->Internal.ParserString[0] >= 48 && pMenu->Internal.ParserString[0] <= 57) || pMenu->Internal.ParserString[0] == 45 || pMenu->Internal.ParserString[0] == 42)
							{
								MenuEntry->UnitIndex = brsatoi((UDINT)&pMenu->Internal.ParserString);
							}
								/* variable */
							else
							{	Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->UnitIndexAdr), &(MenuEntry->UnitIndexLength));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->UnitIndexType),&(TempLength), &(DimVariable));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								else if(MenuEntry->UnitIndexType != VAR_TYPE_USINT && MenuEntry->UnitIndexType != VAR_TYPE_UINT)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = WRONG_TYPE_OF_SCALE_VAR;
									return(FUNCTION_BUSY); 
									pMenu->Out.Status	= WRONG_TYPE_OF_SCALE_VAR;
									return(FUNCTION_BUSY); 			
								}
							}
						}
							/* Index number for description */
						else if(ycnt == 6)
						{
							//MenuEntry->DescriptionIndex = brsatoi((UDINT)&pMenu->Internal.ParserString);
							if((pMenu->Internal.ParserString[0] >= 48 && pMenu->Internal.ParserString[0] <= 57) || pMenu->Internal.ParserString[0] == 45)
							{
								MenuEntry->DescriptionIndex = brsatoi((UDINT)&pMenu->Internal.ParserString);
							}
								/* variable */
							else
							{	Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->DescriptionIndexAdr), &(WorkVariableLength));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								/* Data type from variable */
								Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->DescriptionIndexType),&(MenuEntry->DescriptionIndexLength), &(DimVariable));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								else if(MenuEntry->DescriptionIndexType != VAR_TYPE_USINT && MenuEntry->DescriptionIndexType != VAR_TYPE_UINT)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = WRONG_TYPE_OF_SWITCH_VAR;
									return(FUNCTION_BUSY); 
									pMenu->Internal.ErrorNumber=(WRONG_TYPE_OF_SWITCH_VAR);	
								}
							}
						}
							/* Help ID */
						else if(ycnt == 7)
						{
							MenuEntry->HelpID = brsatoi((UDINT)&pMenu->Internal.ParserString);
						}
							// ADD USER LEVEL FUNCTION INFORMATION	E.Cilingir 2013-02-06
						else if(ycnt == 8)
						{
							MenuEntry->Level = brsatoi((UDINT)&pMenu->Internal.ParserString);
						}
					}
					/* Increase number of Entries */
					pMenu->Internal.ActEntryNumber++;
					/* Search for next type definition */
					pMenu->Internal.InitStep = ENTRY_MARK;
					pMenu->Out.Status = FUNCTION_BUSY;
					return(pMenu->Out.Status);
				}	
				//-------------------------------------------------------------------------------------------------------------
				//---------------------------------------- OUTBOX/INPUT/OUTPUT ------------------------------------------------
				if (!(Result = strcmp("#OB", pMenu->Internal.ParserString)))
				{
					MenuEntry->DynMenuEnryType = PD_OUTBOX_INDEX;
				}
				else if (!(Result = strcmp("#IP", pMenu->Internal.ParserString)))
				{
					MenuEntry->DynMenuEnryType = PD_INPUT_INDEX;
				}
				else if (!(Result = strcmp("#OP", pMenu->Internal.ParserString)))
				{
					MenuEntry->DynMenuEnryType = PD_OUTPUT_INDEX;
				}
				if(!Result)
				{	
					/* Check entries for outbox */
					for(ycnt = 0; ycnt < ELEMENTS_FOR_PD_OB_ENTRY; ycnt++)
					{	/* Check data from file */
						for(xcnt = 0; xcnt < (MAX_PARSER_STRING - 1); xcnt++)
						{	*(UDINT*)&Parser = pMenu->Internal.ActAdrDataBuffer;
							/* Check for ',' or ';' --> indicates new element or end of line */
							if(*Parser == ',' || *Parser == ';')
							{	pMenu->Internal.ParserString[xcnt] = 0;
								pMenu->Internal.ActAdrDataBuffer++;
								break;
							}
								/* Copy data to ParserString */
							else
							{	pMenu->Internal.ParserString[xcnt] = *Parser;
								pMenu->Internal.ActAdrDataBuffer++;
							}
						}
						/* Enable variable */
						if(!ycnt)
						{	Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->ShowEntryFunction1Adr), &(WorkVariableLength));
							if(Status)
							{	pMenu->Internal.InitStep = FREE_MEMALLOC;
								pMenu->Out.Status = Status;
								return(FUNCTION_BUSY); 
							}

							/* Data type from enable variable */
							Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(TypeEnableVariable),&(WorkVariableLength), &(DimVariable));
							if(Status)
							{	pMenu->Internal.InitStep = FREE_MEMALLOC;
								pMenu->Out.Status = Status;
								return(FUNCTION_BUSY); 
							}
							else if(TypeEnableVariable != VAR_TYPE_USINT && TypeEnableVariable != VAR_TYPE_UINT)
							{	pMenu->Internal.InitStep = FREE_MEMALLOC;
								pMenu->Out.Status = WRONG_TYPE_OF_ENABLE_VAR;
								return(FUNCTION_BUSY); 
								pMenu->Internal.ErrorNumber=(WRONG_TYPE_OF_ENABLE_VAR);	
							}
							MenuEntry->ShowEntryFunction1Type=TypeEnableVariable;
						}	
							// Enable Function 2013-02-13 E.Cilingir
						else if(ycnt == 1)
						{
							if (brsatoi((UDINT)&pMenu->Internal.ParserString)==0){
								MenuEntry->ShowEntryFunction1=LOW_SHOW;
							}
							else{
								MenuEntry->ShowEntryFunction1=HIGH_SHOW;
							}
						}
						if(ycnt == 2)
						{
							Status = strcmp("0", pMenu->Internal.ParserString);
							if (Status){
								Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->ShowEntryFunction2Adr), &(WorkVariableLength));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								/* Data type from enable variable */
								Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(TypeEnableVariable),&(WorkVariableLength), &(DimVariable));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								else if(TypeEnableVariable != VAR_TYPE_USINT && TypeEnableVariable != VAR_TYPE_UINT)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = WRONG_TYPE_OF_ENABLE_VAR;
									return(FUNCTION_BUSY); 
									pMenu->Internal.ErrorNumber=(WRONG_TYPE_OF_ENABLE_VAR);	
								}
								MenuEntry->ShowEntryFunction2Type=TypeEnableVariable;
								MenuEntry->ShowEntryFunctionNumOfEnable=MenuEntry->ShowEntryFunctionNumOfEnable+1;
							}
						}
							// Enable Function 2013-02-13 E.Cilingir
						else if(ycnt == 3)
						{
							if (brsatoi((UDINT)&pMenu->Internal.ParserString)==0){
								MenuEntry->ShowEntryFunction2=LOW_SHOW;
							}
							else{
								MenuEntry->ShowEntryFunction2=HIGH_SHOW;
							}
						}
							/* Variable name */
						else if(ycnt == 4)
						{	Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->WorkVariableAdr), &(MenuEntry->WorkVariableLength));
							if(Status)
							{	pMenu->Internal.InitStep = FREE_MEMALLOC;
								pMenu->Out.Status = Status;
								return(FUNCTION_BUSY); 
							}

							/* Data type from variable */
							Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->WorkVariableType),&(MenuEntry->WorkVariableLength), &(DimVariable));
							if(Status)
							{	pMenu->Internal.InitStep = FREE_MEMALLOC;
								pMenu->Out.Status = Status;
								return(FUNCTION_BUSY); 
							}
							else if(MenuEntry->WorkVariableType != VAR_TYPE_USINT && MenuEntry->WorkVariableType != VAR_TYPE_BOOL)
							{	pMenu->Internal.InitStep = FREE_MEMALLOC;
								pMenu->Out.Status = WRONG_TYPE_OF_OUTBOX;
								return(FUNCTION_BUSY); 
								pMenu->Internal.ErrorNumber=(WRONG_TYPE_OF_OUTBOX);	
							}
						}
							/* Index number for unit description */
						else if(ycnt == 5)
						{
							//MenuEntry->UnitIndex = brsatoi((UDINT)&pMenu->Internal.ParserString);
							if((pMenu->Internal.ParserString[0] >= 48 && pMenu->Internal.ParserString[0] <= 57) || pMenu->Internal.ParserString[0] == 45 || pMenu->Internal.ParserString[0] == 42)
							{
								MenuEntry->UnitIndex = brsatoi((UDINT)&pMenu->Internal.ParserString);
							}
								/* variable */
							else
							{	Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->UnitIndexAdr), &(MenuEntry->UnitIndexLength));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->UnitIndexType),&(TempLength), &(DimVariable));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								else if(MenuEntry->UnitIndexType != VAR_TYPE_USINT && MenuEntry->UnitIndexType != VAR_TYPE_UINT)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = WRONG_TYPE_OF_SCALE_VAR;
									return(FUNCTION_BUSY); 
									pMenu->Out.Status	= WRONG_TYPE_OF_SCALE_VAR;
									return(FUNCTION_BUSY); 			
								}
							}
						}
							/* Index number for description */
						else if(ycnt == 6)
						{
							//MenuEntry->DescriptionIndex = brsatoi((UDINT)&pMenu->Internal.ParserString);
							if((pMenu->Internal.ParserString[0] >= 48 && pMenu->Internal.ParserString[0] <= 57) || pMenu->Internal.ParserString[0] == 45)
							{
								MenuEntry->DescriptionIndex = brsatoi((UDINT)&pMenu->Internal.ParserString);
							}
								/* variable */
							else
							{	Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->DescriptionIndexAdr), &(WorkVariableLength));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								/* Data type from variable */
								Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->DescriptionIndexType),&(MenuEntry->DescriptionIndexLength), &(DimVariable));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								else if(MenuEntry->DescriptionIndexType != VAR_TYPE_USINT && MenuEntry->DescriptionIndexType != VAR_TYPE_UINT)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = WRONG_TYPE_OF_SWITCH_VAR;
									return(FUNCTION_BUSY); 
									pMenu->Internal.ErrorNumber=(WRONG_TYPE_OF_SWITCH_VAR);	
								}
							}
						}
							/* Help ID */
						else if(ycnt == 7)
						{
							MenuEntry->HelpID = brsatoi((UDINT)&pMenu->Internal.ParserString);
						}
							// ADD USER LEVEL FUNCTION INFORMATION	E.Cilingir 2013-02-06
						else if(ycnt == 8)
						{
							MenuEntry->Level = brsatoi((UDINT)&pMenu->Internal.ParserString);
						}
					}
					/* Increase number of Entries */
					pMenu->Internal.ActEntryNumber++;
					/* Search for next type definition */
					pMenu->Internal.InitStep = ENTRY_MARK;
					pMenu->Out.Status = FUNCTION_BUSY;
					return(pMenu->Out.Status);
				}
				//-------------------------------------------------------------------------------------------------------------
				//---------------------------------------- HEADLINE -----------------------------------------------------------
				Result = strcmp("#HD", pMenu->Internal.ParserString);
				if(!Result)
				{	MenuEntry->DynMenuEnryType = PD_HEADLINE_INDEX;
					/* Check entries for headline */
					for(ycnt = 0; ycnt < ELEMENTS_FOR_PD_HD_ENTRY; ycnt++)
					{	/* Check data from file */
						for(xcnt = 0; xcnt < (MAX_PARSER_STRING - 1); xcnt++)
						{	*(UDINT*)&Parser = pMenu->Internal.ActAdrDataBuffer;
							/* Check for ',' or ';' --> indicates new element or end of line */
							if(*Parser == ',' || *Parser == ';')
							{	pMenu->Internal.ParserString[xcnt] = 0;
								pMenu->Internal.ActAdrDataBuffer++;
								break;
							}
								/* Copy data to ParserString */
							else
							{	pMenu->Internal.ParserString[xcnt] = *Parser;
								pMenu->Internal.ActAdrDataBuffer++;
							}
						}
						/* Enable variable */
						if(!ycnt)
						{	Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->ShowEntryFunction1Adr), &(WorkVariableLength));
							if(Status)
							{	pMenu->Internal.InitStep = FREE_MEMALLOC;
								pMenu->Out.Status = Status;
								return(FUNCTION_BUSY); 
							}

							/* Data type from enable variable */
							Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(TypeEnableVariable),&(WorkVariableLength), &(DimVariable));
							if(Status)
							{	pMenu->Internal.InitStep = FREE_MEMALLOC;
								pMenu->Out.Status = Status;
								return(FUNCTION_BUSY); 
							}
							else if(TypeEnableVariable != VAR_TYPE_USINT && TypeEnableVariable != VAR_TYPE_UINT)
							{	pMenu->Internal.InitStep = FREE_MEMALLOC;
								pMenu->Out.Status = WRONG_TYPE_OF_ENABLE_VAR;
								return(FUNCTION_BUSY); 
								pMenu->Internal.ErrorNumber=(WRONG_TYPE_OF_ENABLE_VAR);			
							}
							MenuEntry->ShowEntryFunction1Type=TypeEnableVariable;
						}
							// Enable Function 2013-02-13 E.Cilingir
						else if(ycnt == 1)
						{
							if (brsatoi((UDINT)&pMenu->Internal.ParserString)==0){
								MenuEntry->ShowEntryFunction1=LOW_SHOW;
							}
							else{
								MenuEntry->ShowEntryFunction1=HIGH_SHOW;
							}
						}
						if(ycnt == 2)
						{
							Status = strcmp("0", pMenu->Internal.ParserString);
							if (Status){
								Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->ShowEntryFunction2Adr), &(WorkVariableLength));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								/* Data type from enable variable */
								Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(TypeEnableVariable),&(WorkVariableLength), &(DimVariable));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								else if(TypeEnableVariable != VAR_TYPE_USINT && TypeEnableVariable != VAR_TYPE_UINT)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = WRONG_TYPE_OF_ENABLE_VAR;
									return(FUNCTION_BUSY); 
									pMenu->Internal.ErrorNumber=(WRONG_TYPE_OF_ENABLE_VAR);	
								}
								MenuEntry->ShowEntryFunction2Type=TypeEnableVariable;
								MenuEntry->ShowEntryFunctionNumOfEnable=MenuEntry->ShowEntryFunctionNumOfEnable+1;
							}
						}
							// Enable Function 2013-02-13 E.Cilingir
						else if(ycnt == 3)
						{
							if (brsatoi((UDINT)&pMenu->Internal.ParserString)==0){
								MenuEntry->ShowEntryFunction2=LOW_SHOW;
							}
							else{
								MenuEntry->ShowEntryFunction2=HIGH_SHOW;
							}
						}
							/* Index number for description */
						else if(ycnt == 4)
						{
							//MenuEntry->DescriptionIndex = brsatoi((UDINT)&pMenu->Internal.ParserString);
							if((pMenu->Internal.ParserString[0] >= 48 && pMenu->Internal.ParserString[0] <= 57) || pMenu->Internal.ParserString[0] == 45)
							{
								MenuEntry->DescriptionIndex = brsatoi((UDINT)&pMenu->Internal.ParserString);
							}
								/* variable */
							else
							{	Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->DescriptionIndexAdr), &(WorkVariableLength));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								/* Data type from variable */
								Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->DescriptionIndexType),&(MenuEntry->DescriptionIndexLength), &(DimVariable));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								else if(MenuEntry->DescriptionIndexType != VAR_TYPE_USINT && MenuEntry->DescriptionIndexType != VAR_TYPE_UINT)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = WRONG_TYPE_OF_SWITCH_VAR;
									return(FUNCTION_BUSY); 
									pMenu->Internal.ErrorNumber=(WRONG_TYPE_OF_SWITCH_VAR);	
								}
							}
						}
							/* Help ID */
						else if(ycnt == 5)
						{
							MenuEntry->HelpID = brsatoi((UDINT)&pMenu->Internal.ParserString);
						}
							// ADD USER LEVEL FUNCTION INFORMATION	E.Cilingir 2013-02-06
						else if(ycnt == 6)
						{
							MenuEntry->Level = brsatoi((UDINT)&pMenu->Internal.ParserString);
						}
					}
					/* Increase number of Entries */
					pMenu->Internal.ActEntryNumber++;
					/* Search for next type definition */
					pMenu->Internal.InitStep = ENTRY_MARK;
					pMenu->Out.Status = FUNCTION_BUSY;
					return(pMenu->Out.Status);
				}
				//-------------------------------------------------------------------------------------------------------------
				//---------------------------------------- UPDOWN -------------------------------------------------------------
				Result = strcmp("#UD", pMenu->Internal.ParserString);
				if(!Result)
				{	MenuEntry->DynMenuEnryType = PD_UPDOWN_INDEX;
					/* Check entries for sensing device */
					for(ycnt = 0; ycnt < ELEMENTS_FOR_PD_UD_ENTRY; ycnt++)
					{	/* Check data from file */
						for(xcnt = 0; xcnt < (MAX_PARSER_STRING - 1); xcnt++)
						{	*(UDINT*)&Parser = pMenu->Internal.ActAdrDataBuffer;
							/* Check for ',' or ';' --> indicates new element or end of line */
							if(*Parser == ',' || *Parser == ';')
							{	pMenu->Internal.ParserString[xcnt] = 0;
								pMenu->Internal.ActAdrDataBuffer++;
								break;
							}
								/* Copy data to ParserString */
							else
							{	pMenu->Internal.ParserString[xcnt] = *Parser;
								pMenu->Internal.ActAdrDataBuffer++;
							}
						}
						/* Enable variable */
						if(!ycnt)
						{	Status  = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->ShowEntryFunction1Adr), &(WorkVariableLength));
							if(Status)
							{	pMenu->Internal.InitStep = FREE_MEMALLOC;
								pMenu->Out.Status = Status;
								return(FUNCTION_BUSY); 
							}

							/* Data type from enable variable */
							Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(TypeEnableVariable),&(WorkVariableLength), &(DimVariable));
							if(Status)
							{	pMenu->Internal.InitStep = FREE_MEMALLOC;
								pMenu->Out.Status = Status;
								return(FUNCTION_BUSY); 
							}
							else if(TypeEnableVariable != VAR_TYPE_USINT && TypeEnableVariable != VAR_TYPE_UINT)
							{	pMenu->Internal.InitStep = FREE_MEMALLOC;
								pMenu->Out.Status = WRONG_TYPE_OF_ENABLE_VAR;
								return(FUNCTION_BUSY); 
								pMenu->Internal.ErrorNumber=(WRONG_TYPE_OF_ENABLE_VAR);	
							}
							MenuEntry->ShowEntryFunction1Type=TypeEnableVariable;
						}
							// Enable Function 2013-02-13 E.Cilingir
						else if(ycnt == 1)
						{
							if (brsatoi((UDINT)&pMenu->Internal.ParserString)==0){
								MenuEntry->ShowEntryFunction1=LOW_SHOW;
							}
							else{
								MenuEntry->ShowEntryFunction1=HIGH_SHOW;
							}
						}
						if(ycnt == 2)
						{
							Status = strcmp("0", pMenu->Internal.ParserString);
							if (Status){
								Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->ShowEntryFunction2Adr), &(WorkVariableLength));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								/* Data type from enable variable */
								Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(TypeEnableVariable),&(WorkVariableLength), &(DimVariable));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								else if(TypeEnableVariable != VAR_TYPE_USINT && TypeEnableVariable != VAR_TYPE_UINT)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = WRONG_TYPE_OF_ENABLE_VAR;
									return(FUNCTION_BUSY); 
									pMenu->Internal.ErrorNumber=(WRONG_TYPE_OF_ENABLE_VAR);	
								}
								MenuEntry->ShowEntryFunction2Type=TypeEnableVariable;
								MenuEntry->ShowEntryFunctionNumOfEnable=MenuEntry->ShowEntryFunctionNumOfEnable+1;
							}
						}
							// Enable Function 2013-02-13 E.Cilingir
						else if(ycnt == 3)
						{
							if (brsatoi((UDINT)&pMenu->Internal.ParserString)==0){
								MenuEntry->ShowEntryFunction2=LOW_SHOW;
							}
							else{
								MenuEntry->ShowEntryFunction2=HIGH_SHOW;
							}
						}
							/* Variable name */
						else if(ycnt == 4)
						{	Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->WorkVariableAdr), &(MenuEntry->WorkVariableLength));
							if(Status)
							{	pMenu->Internal.InitStep = FREE_MEMALLOC;
								pMenu->Out.Status = Status;
								return(FUNCTION_BUSY); 
							}

							/* Data type from variable */
							Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->WorkVariableType),&(MenuEntry->WorkVariableLength), &(DimVariable));
							if(Status)
							{	pMenu->Internal.InitStep = FREE_MEMALLOC;
								pMenu->Out.Status = Status;
								return(FUNCTION_BUSY); 
							}
					/*	else if(MenuEntry->WorkVariableType != VAR_TYPE_USINT && MenuEntry->WorkVariableType != VAR_TYPE_BOOL)
						{	pMenu->Internal.InitStep = FREE_MEMALLOC;
							pMenu->Out.Status = WRONG_TYPE_OF_SENSING_VAR;
							return(FUNCTION_BUSY); 
							pMenu->Internal.ErrorNumber=(WRONG_TYPE_OF_SENSING_VAR);	
						}*/
						}
							/* Index number for unit description */
						else if(ycnt == 5)
						{
							//MenuEntry->UnitIndex = brsatoi((UDINT)&pMenu->Internal.ParserString);
							if((pMenu->Internal.ParserString[0] >= 48 && pMenu->Internal.ParserString[0] <= 57) || pMenu->Internal.ParserString[0] == 45 || pMenu->Internal.ParserString[0] == 42)
							{
								MenuEntry->UnitIndex = brsatoi((UDINT)&pMenu->Internal.ParserString);
							}
								/* variable */
							else
							{	Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->UnitIndexAdr), &(MenuEntry->UnitIndexLength));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->UnitIndexType),&(TempLength), &(DimVariable));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								else if(MenuEntry->UnitIndexType != VAR_TYPE_USINT && MenuEntry->UnitIndexType != VAR_TYPE_UINT)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = WRONG_TYPE_OF_SCALE_VAR;
									return(FUNCTION_BUSY); 
									pMenu->Out.Status	= WRONG_TYPE_OF_SCALE_VAR;
									return(FUNCTION_BUSY); 			
								}
							}
						}
							/* Index number for description */
						else if(ycnt == 6)
						{
							//MenuEntry->DescriptionIndex = brsatoi((UDINT)&pMenu->Internal.ParserString);
							if((pMenu->Internal.ParserString[0] >= 48 && pMenu->Internal.ParserString[0] <= 57) || pMenu->Internal.ParserString[0] == 45)
							{
								MenuEntry->DescriptionIndex = brsatoi((UDINT)&pMenu->Internal.ParserString);
							}
								/* variable */
							else
							{	Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->DescriptionIndexAdr), &(WorkVariableLength));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								/* Data type from variable */
								Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->DescriptionIndexType),&(MenuEntry->DescriptionIndexLength), &(DimVariable));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								else if(MenuEntry->DescriptionIndexType != VAR_TYPE_USINT && MenuEntry->DescriptionIndexType != VAR_TYPE_UINT)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = WRONG_TYPE_OF_SWITCH_VAR;
									return(FUNCTION_BUSY); 
									pMenu->Internal.ErrorNumber=(WRONG_TYPE_OF_SWITCH_VAR);	
								}
							}
						}
							/* Index for button released text */
						else if(ycnt == 7)
						{
							MenuEntry->UpDownIndex = brsatoi((UDINT)&pMenu->Internal.ParserString);
						}
						if(ycnt == 8)
						{
							Status = strcmp("0", pMenu->Internal.ParserString);
							if (Status){
								MenuEntry->CalcFunctionMinMax=1;
								Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->CalcFunctionAdrVar), &(WorkVariableLength));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								/* Data type from enable variable */
								Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->CalcFunctionTypeVar),&(WorkVariableLength), &(DimVariable));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								else if(MenuEntry->CalcFunctionTypeVar!=MenuEntry->WorkVariableType)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = WRONG_TYPE_OF_CALC_FUNCTION_VAR;
									return(FUNCTION_BUSY); 
									pMenu->Out.Status	= WRONG_TYPE_OF_CALC_FUNCTION_VAR;
									return(FUNCTION_BUSY); 			
								}

							}
						}
						else if(ycnt == 9)
						{	/* Fix data */
							if((pMenu->Internal.ParserString[0] >= 48 && pMenu->Internal.ParserString[0] <= 57) || pMenu->Internal.ParserString[0] == 45 || pMenu->Internal.ParserString[0] == 42)
							{
								MenuEntry->LimitsMinValue = brsatoi((UDINT)&pMenu->Internal.ParserString);
								MenuEntry->LimitsMinType = MenuEntry->WorkVariableType;
							}
								/* variable */
							else
							{	Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->LimitsMinAdr), &(WorkVariableLength));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->LimitsMinType),&(TempLength), &(DimVariable));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
							}
						}
							/* Fix data or variable name for maximum */
						else if(ycnt == 10)
						{	/* Fix data */
							if((pMenu->Internal.ParserString[0] >= 48 && pMenu->Internal.ParserString[0] <= 57) || pMenu->Internal.ParserString[0] == 45 )
							{
								MenuEntry->LimitsMaxValue = brsatoi((UDINT)&pMenu->Internal.ParserString);
								MenuEntry->LimitsMaxType = MenuEntry->WorkVariableType;
								if (MenuEntry->LimitsMaxValue==0 && MenuEntry->CalcFunctionMinMax==0){	
									switch (MenuEntry->WorkVariableType){
										case VAR_TYPE_BOOL:		MenuEntry->LimitsMaxValue	=	DYN_MAX_VALUE_BOOL;		break;
										case VAR_TYPE_DINT:		MenuEntry->LimitsMaxValue	=	DYN_MAX_VALUE_DINT;		break;
										case VAR_TYPE_UDINT:	MenuEntry->LimitsMaxValue	=	DYN_MAX_VALUE_UDINT;	break;					
										case VAR_TYPE_INT:		MenuEntry->LimitsMaxValue	=	DYN_MAX_VALUE_INT;		break;					
										case VAR_TYPE_UINT:		MenuEntry->LimitsMaxValue	=	DYN_MAX_VALUE_UINT;		break;					
										case VAR_TYPE_SINT:		MenuEntry->LimitsMaxValue	=	DYN_MAX_VALUE_SINT;		break;					
										case VAR_TYPE_USINT:	MenuEntry->LimitsMaxValue	=	DYN_MAX_VALUE_USINT;	break;					
										case VAR_TYPE_REAL:		MenuEntry->LimitsMaxValue	=	DYN_MAX_VALUE_REAL;		break;				
										case VAR_TYPE_TIME:		MenuEntry->LimitsMaxValue	=	DYN_MAX_VALUE_TIME;		break;
									}
								}
							}
							else
							{	Status= PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->LimitsMaxAdr), &(WorkVariableLength));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->LimitsMaxType),&(TempLength), &(DimVariable));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
							}
						}		
						else if(ycnt == 11 && MenuEntry->CalcFunctionMinMax==1)
						{	/* Fix data */
							if((pMenu->Internal.ParserString[0] >= 48 && pMenu->Internal.ParserString[0] <= 57) || pMenu->Internal.ParserString[0] == 45 || pMenu->Internal.ParserString[0] == 42)
							{
								MenuEntry->CalcFunctionLimitsMinValue = brsatoi((UDINT)&pMenu->Internal.ParserString);
								MenuEntry->CalcFunctionLimitsMinType = MenuEntry->WorkVariableType;
							}
								/* variable */
							else
							{	Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->CalcFunctionLimitsMinAdr), &(WorkVariableLength));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->CalcFunctionLimitsMinType),&(TempLength), &(DimVariable));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
							}
						}
			
			
						else if(ycnt == 12 && MenuEntry->CalcFunctionMinMax==1)
						{	/* Fix data */
							if((pMenu->Internal.ParserString[0] >= 48 && pMenu->Internal.ParserString[0] <= 57) || pMenu->Internal.ParserString[0] == 45 || pMenu->Internal.ParserString[0] == 42)
							{
								MenuEntry->CalcFunctionLimitsMaxValue = brsatoi((UDINT)&pMenu->Internal.ParserString);
								MenuEntry->CalcFunctionLimitsMaxType = MenuEntry->WorkVariableType;
							}
								/* variable */
							else
							{	Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->CalcFunctionLimitsMaxAdr), &(WorkVariableLength));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->CalcFunctionLimitsMaxType),&(TempLength), &(DimVariable));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
							}
						}
						else if(ycnt == 13)
						{
							if((pMenu->Internal.ParserString[0] >= 48 && pMenu->Internal.ParserString[0] <= 57) || pMenu->Internal.ParserString[0] == 45 || pMenu->Internal.ParserString[0] == 42)
							{
								MenuEntry->UpDownStepWidth = brsatof((UDINT)&pMenu->Internal.ParserString);
							}
						}
							// ADD Scale Value	E.Cilingir 2013-03-13
						else if(ycnt == 14)
						{
							//MenuEntry->ScaleValue = brsatoi((UDINT)&pMenu->Internal.ParserString);
							if((pMenu->Internal.ParserString[0] >= 48 && pMenu->Internal.ParserString[0] <= 57) || pMenu->Internal.ParserString[0] == 45 || pMenu->Internal.ParserString[0] == 42)
							{
								MenuEntry->ScaleValue = brsatoi((UDINT)&pMenu->Internal.ParserString);
							}
								/* variable */
							else
							{	Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->ScaleValueAdr), &(MenuEntry->ScaleValueLength));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->ScaleValueType),&(TempLength), &(DimVariable));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								else if(MenuEntry->ScaleValueType != VAR_TYPE_USINT && MenuEntry->ScaleValueType != VAR_TYPE_UINT)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = WRONG_TYPE_OF_SCALE_VAR;
									return(FUNCTION_BUSY); 
									pMenu->Out.Status	= WRONG_TYPE_OF_SCALE_VAR;
									return(FUNCTION_BUSY); 			
								}
							}
						}	
						else if(ycnt == 15)
						{
							if((pMenu->Internal.ParserString[0] >= 48 && pMenu->Internal.ParserString[0] <= 57) || pMenu->Internal.ParserString[0] == 45 || pMenu->Internal.ParserString[0] == 42)
							{
								MenuEntry->AddFraction = brsatoi((UDINT)&pMenu->Internal.ParserString);
							}
								/* variable */
							else
							{	Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->AddFractionAdr), &(MenuEntry->AddFractionLength));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->AddFractionType),&(TempLength), &(DimVariable));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								else if(MenuEntry->AddFractionType != VAR_TYPE_USINT && MenuEntry->AddFractionType != VAR_TYPE_UINT)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = WRONG_TYPE_OF_FRACTION_VAR;
									return(FUNCTION_BUSY); 
									pMenu->Out.Status	= WRONG_TYPE_OF_FRACTION_VAR;
									return(FUNCTION_BUSY); 			
								}
							}
						}	
						else if(ycnt == 16)
						{
							MenuEntry->FunctionHeadlineBitmap = brsatoi((UDINT)&pMenu->Internal.ParserString);
						}
							/* Help ID */
						else if(ycnt == 17)
						{
							MenuEntry->HelpID = brsatoi((UDINT)&pMenu->Internal.ParserString);
						}
							// ADD USER LEVEL FUNCTION INFORMATION	E.Cilingir 2013-02-06
						else if(ycnt == 18)
						{
							MenuEntry->Level = brsatoi((UDINT)&pMenu->Internal.ParserString);
						}
					}
					/* Increase number of Entries */
					pMenu->Internal.ActEntryNumber++;
					/* Search for next type definition */
					pMenu->Internal.InitStep = ENTRY_MARK;
					pMenu->Out.Status = FUNCTION_BUSY;
					return(pMenu->Out.Status);
				}
			}
			
			//-------------------------------------------------------------------------------------------------------------
			//---------------------------------------- Maintanance Page ---------------------------------------------------
			//-------------------------------------------------------------------------------------------------------------
			Result = strcmp("MT", pMenu->Info.FunctionType);
			if(!Result)	{
				//-------------------------------------------------------------------------------------------------------------
				//---------------------------------------- NUMMERIC INPUT -----------------------------------------------------
				Result = strcmp("#NI", pMenu->Internal.ParserString);
				if(!Result)
				{	MenuEntry->DynMenuEnryType = MT_NUMERIC_INPUT_INDEX;
					/* Check entries for numeric input */
					for(ycnt = 0; ycnt < ELEMENTS_FOR_MT_NI_ENTRY; ycnt++)
					{	/* Check data from file */
						for(xcnt = 0; xcnt < (MAX_PARSER_STRING - 1); xcnt++)
						{	*(UDINT*)&Parser = pMenu->Internal.ActAdrDataBuffer;
							/* Check for ',' or ';' --> indicates new element or end of line */
							if(*Parser == ',' || *Parser == ';')
							{	
								pMenu->Internal.ParserString[xcnt] = 0;
								pMenu->Internal.ActAdrDataBuffer++;
								break;
							}
								/* Copy data to ParserString */
							else
							{	pMenu->Internal.ParserString[xcnt] = *Parser;
								pMenu->Internal.ActAdrDataBuffer++;
							}
						}
						/* Enable variable */
						if(!ycnt)
						{
							Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->ShowEntryFunction1Adr), &(WorkVariableLength));
							if(Status)
							{	pMenu->Internal.InitStep = FREE_MEMALLOC;
								pMenu->Out.Status = Status;
								return(FUNCTION_BUSY); 
							}

							/* Data type from enable variable */
							Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(TypeEnableVariable),&(WorkVariableLength), &(DimVariable));
							if(Status)
							{	pMenu->Internal.InitStep = FREE_MEMALLOC;
								pMenu->Out.Status = Status;
								return(FUNCTION_BUSY); 
							}
							else if(TypeEnableVariable != VAR_TYPE_USINT && TypeEnableVariable != VAR_TYPE_UINT)
							{	pMenu->Internal.InitStep = FREE_MEMALLOC;
								pMenu->Out.Status	= WRONG_TYPE_OF_ENABLE_VAR;
								return(FUNCTION_BUSY); 	
								pMenu->Internal.ErrorNumber=(WRONG_TYPE_OF_ENABLE_VAR);
							}
							MenuEntry->ShowEntryFunction1Type=TypeEnableVariable;
						}
							// Enable Function 2013-02-13 E.Cilingir
						else if(ycnt == 1)
						{
							if (brsatoi((UDINT)&pMenu->Internal.ParserString)==0){
								MenuEntry->ShowEntryFunction1=LOW_SHOW;
							}
							else{
								MenuEntry->ShowEntryFunction1=HIGH_SHOW;
							}
						}
						if(ycnt == 2)
						{
							Status = strcmp("0", pMenu->Internal.ParserString);
							if (Status){
								Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->ShowEntryFunction2Adr), &(WorkVariableLength));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								/* Data type from enable variable */
								Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(TypeEnableVariable),&(WorkVariableLength), &(DimVariable));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								else if(TypeEnableVariable != VAR_TYPE_USINT && TypeEnableVariable != VAR_TYPE_UINT)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = WRONG_TYPE_OF_ENABLE_VAR;
									return(FUNCTION_BUSY); 
									pMenu->Out.Status	= WRONG_TYPE_OF_ENABLE_VAR;
									return(FUNCTION_BUSY); 			
								}
								MenuEntry->ShowEntryFunction2Type=TypeEnableVariable;
								MenuEntry->ShowEntryFunctionNumOfEnable=MenuEntry->ShowEntryFunctionNumOfEnable+1;
							}
						}
							// Enable Function 2013-02-13 E.Cilingir
						else if(ycnt == 3)
						{
							if (brsatoi((UDINT)&pMenu->Internal.ParserString)==0){
								MenuEntry->ShowEntryFunction2=LOW_SHOW;
							}
							else{
								MenuEntry->ShowEntryFunction2=HIGH_SHOW;
							}
						}
							/* Variable name */
						else if(ycnt == 4)
						{	
							Status = strcmp("0", pMenu->Internal.ParserString);
							if (Status){
								Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->WorkVariableAdr), &(MenuEntry->WorkVariableLength));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}

								/* Data type from variable */
								Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->WorkVariableType),&(MenuEntry->WorkVariableLength), &(DimVariable));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
							}
							else{
								MenuEntry->WorkVariableAdr = 0;
							}

						}
							/* Variable name of Status Element*/
						else if(ycnt == 5)
						{	Status = strcmp("0", pMenu->Internal.ParserString);
							if (Status){
								Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->MaintVariableStatusAdr), &(MenuEntry->MaintVariableStatusLength));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}

								/* Data type from variable */
								Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->MaintVariableStatusType),&(MenuEntry->MaintVariableStatusLength), &(DimVariable));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								else if(MenuEntry->MaintVariableStatusType != VAR_TYPE_USINT && MenuEntry->MaintVariableStatusType != VAR_TYPE_UINT)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = WRONG_TYPE_OF_STATUS_VAR;
									return(FUNCTION_BUSY); 
									pMenu->Internal.ErrorNumber=(WRONG_TYPE_OF_SENSING_VAR);
								}
							}
						}
							/* Compare Value of Status Element*/
						else if(ycnt == 6)
						{	/* Fix data */
							if (brsatoi((UDINT)&pMenu->Internal.ParserString)==1){
								MenuEntry->MaintVariableStatusCmpValue=1;
							}
							else{
								MenuEntry->MaintVariableStatusCmpValue=0;
							}
						}
							/* Index number for description */
						else if(ycnt == 7)
						{
							//MenuEntry->DescriptionIndex = brsatoi((UDINT)&pMenu->Internal.ParserString);
							if((pMenu->Internal.ParserString[0] >= 48 && pMenu->Internal.ParserString[0] <= 57) || pMenu->Internal.ParserString[0] == 45)
							{
								MenuEntry->DescriptionIndex = brsatoi((UDINT)&pMenu->Internal.ParserString);
							}
								/* variable */
							else
							{	Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->DescriptionIndexAdr), &(WorkVariableLength));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								/* Data type from variable */
								Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->DescriptionIndexType),&(MenuEntry->DescriptionIndexLength), &(DimVariable));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								else if(MenuEntry->DescriptionIndexType != VAR_TYPE_USINT && MenuEntry->DescriptionIndexType != VAR_TYPE_UINT)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = WRONG_TYPE_OF_SWITCH_VAR;
									return(FUNCTION_BUSY); 
									pMenu->Internal.ErrorNumber=(WRONG_TYPE_OF_SWITCH_VAR);	
								}
							}
						}		
							/* Fix data or variable name for minimum */
						else if(ycnt == 8)
						{	/* Fix data */
							if((pMenu->Internal.ParserString[0] >= 48 && pMenu->Internal.ParserString[0] <= 57) || pMenu->Internal.ParserString[0] == 45 || pMenu->Internal.ParserString[0] == 42)
							{
								MenuEntry->LimitsMinValue = brsatoi((UDINT)&pMenu->Internal.ParserString);
								MenuEntry->LimitsMinType = MenuEntry->WorkVariableType;
							}
								/* variable */
							else
							{	Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->LimitsMinAdr), &(WorkVariableLength));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->LimitsMinType),&(TempLength), &(DimVariable));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
							}
						}
							/* Fix data or variable name for maximum */
						else if(ycnt == 9)
						{	/* Fix data */
							if((pMenu->Internal.ParserString[0] >= 48 && pMenu->Internal.ParserString[0] <= 57) || pMenu->Internal.ParserString[0] == 45 )
							{
								MenuEntry->LimitsMaxValue = brsatoi((UDINT)&pMenu->Internal.ParserString);
								MenuEntry->LimitsMaxType = MenuEntry->WorkVariableType;
								// 2013-02-18 E.Cilingir Select Max Value out of the Variable if input Value is 0
								if (MenuEntry->LimitsMaxValue==0 && MenuEntry->CalcFunctionMinMax==0){	
									switch (MenuEntry->WorkVariableType){
										case VAR_TYPE_BOOL:		MenuEntry->LimitsMaxValue	=	DYN_MAX_VALUE_BOOL;		break;
										case VAR_TYPE_DINT:		MenuEntry->LimitsMaxValue	=	DYN_MAX_VALUE_DINT;		break;
										case VAR_TYPE_UDINT:	MenuEntry->LimitsMaxValue	=	DYN_MAX_VALUE_UDINT;	break;					
										case VAR_TYPE_INT:		MenuEntry->LimitsMaxValue	=	DYN_MAX_VALUE_INT;		break;					
										case VAR_TYPE_UINT:		MenuEntry->LimitsMaxValue	=	DYN_MAX_VALUE_UINT;		break;					
										case VAR_TYPE_SINT:		MenuEntry->LimitsMaxValue	=	DYN_MAX_VALUE_SINT;		break;					
										case VAR_TYPE_USINT:	MenuEntry->LimitsMaxValue	=	DYN_MAX_VALUE_USINT;	break;					
										case VAR_TYPE_REAL:		MenuEntry->LimitsMaxValue	=	DYN_MAX_VALUE_REAL;		break;				
										case VAR_TYPE_TIME:		MenuEntry->LimitsMaxValue	=	DYN_MAX_VALUE_TIME;		break;
									}
								}
							}
								/* variable */
							else
							{	Status= PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->LimitsMaxAdr), &(WorkVariableLength));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->LimitsMaxType),&(TempLength), &(DimVariable));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
							}
						}
							// ADD Scale Value	E.Cilingir 2013-03-13
						else if(ycnt == 10)
						{
							//MenuEntry->ScaleValue = brsatoi((UDINT)&pMenu->Internal.ParserString);
							if((pMenu->Internal.ParserString[0] >= 48 && pMenu->Internal.ParserString[0] <= 57) || pMenu->Internal.ParserString[0] == 45 || pMenu->Internal.ParserString[0] == 42)
							{
								MenuEntry->ScaleValue = brsatoi((UDINT)&pMenu->Internal.ParserString);
							}
								/* variable */
							else
							{	Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->ScaleValueAdr), &(MenuEntry->ScaleValueLength));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->ScaleValueType),&(TempLength), &(DimVariable));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								else if(MenuEntry->ScaleValueType != VAR_TYPE_USINT && MenuEntry->ScaleValueType != VAR_TYPE_UINT)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = WRONG_TYPE_OF_SCALE_VAR;
									return(FUNCTION_BUSY); 
									pMenu->Out.Status	= WRONG_TYPE_OF_SCALE_VAR;
									return(FUNCTION_BUSY); 			
								}
							}
						}	
						else if(ycnt == 11)
						{
							if((pMenu->Internal.ParserString[0] >= 48 && pMenu->Internal.ParserString[0] <= 57) || pMenu->Internal.ParserString[0] == 45 || pMenu->Internal.ParserString[0] == 42)
							{
								MenuEntry->AddFraction = brsatoi((UDINT)&pMenu->Internal.ParserString);
							}
								/* variable */
							else
							{	Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->AddFractionAdr), &(MenuEntry->AddFractionLength));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->AddFractionType),&(TempLength), &(DimVariable));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								else if(MenuEntry->AddFractionType != VAR_TYPE_USINT && MenuEntry->AddFractionType != VAR_TYPE_UINT)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = WRONG_TYPE_OF_FRACTION_VAR;
									return(FUNCTION_BUSY); 
									pMenu->Out.Status	= WRONG_TYPE_OF_FRACTION_VAR;
									return(FUNCTION_BUSY); 			
								}
							}
						}		
							/* Index number for unit description */
						else if(ycnt == 12)
						{
							//MenuEntry->UnitIndex = brsatoi((UDINT)&pMenu->Internal.ParserString);
							if((pMenu->Internal.ParserString[0] >= 48 && pMenu->Internal.ParserString[0] <= 57) || pMenu->Internal.ParserString[0] == 45 || pMenu->Internal.ParserString[0] == 42)
							{
								MenuEntry->UnitIndex = brsatoi((UDINT)&pMenu->Internal.ParserString);
							}
								/* variable */
							else
							{	Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->UnitIndexAdr), &(MenuEntry->UnitIndexLength));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->UnitIndexType),&(TempLength), &(DimVariable));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								else if(MenuEntry->UnitIndexType != VAR_TYPE_USINT && MenuEntry->UnitIndexType != VAR_TYPE_UINT)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = WRONG_TYPE_OF_SCALE_VAR;
									return(FUNCTION_BUSY); 
									pMenu->Out.Status	= WRONG_TYPE_OF_SCALE_VAR;
									return(FUNCTION_BUSY); 			
								}
							}
						}
							
							/* Variable name Output */
						else if(ycnt == 13)
						{	
							Status = strcmp("0", pMenu->Internal.ParserString);
							if (Status){
								Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->MaintVariableNOAdr), &(MenuEntry->MaintVariableNOLength));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}

								/* Data type from variable */
								Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->MaintVariableNOType),&(MenuEntry->MaintVariableNOLength), &(DimVariable));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
							}
						}
						else if(ycnt == 14)
						{	
							Status = strcmp("0", pMenu->Internal.ParserString);
							if (Status){
								Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->MaintVariableIPAdr), &(MenuEntry->MaintVariableIPLength));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}

								/* Data type from variable */
								Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->MaintVariableIPType),&(MenuEntry->MaintVariableIPLength), &(DimVariable));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
							}
						}
						else if(ycnt == 15)
						{	
							Status = strcmp("0", pMenu->Internal.ParserString);
							if (Status){
								Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->MaintVariableOPAdr), &(MenuEntry->MaintVariableOPLength));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}

								/* Data type from variable */
								Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->MaintVariableOPType),&(MenuEntry->MaintVariableOPLength), &(DimVariable));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
							}
						}
							/* Help ID */
						else if(ycnt == 16)
						{
							MenuEntry->HelpID = brsatoi((UDINT)&pMenu->Internal.ParserString);
						}						
							// ADD USER LEVEL FUNCTION INFORMATION	E.Cilingir 2013-02-06
						else if(ycnt == 17)
						{
							MenuEntry->Level = brsatoi((UDINT)&pMenu->Internal.ParserString);
							MenuEntry->LevelButton = brsatoi((UDINT)&pMenu->Internal.ParserString);
						}
						else if(ycnt == 18)
						{
							Status = strcmp("-1", pMenu->Internal.ParserString);
							if (Status){
								MenuEntry->LevelButton = brsatoi((UDINT)&pMenu->Internal.ParserString);
							}
						}
					}
					/* Increase number of Entries */
					pMenu->Internal.ActEntryNumber++;
					/* Search for next type definition */
					pMenu->Internal.InitStep = ENTRY_MARK;
					pMenu->Out.Status = FUNCTION_BUSY;
					return(pMenu->Out.Status);
				}
				//-------------------------------------------------------------------------------------------------------------
				//---------------------------------------- SENSING DEVICE -----------------------------------------------------
				Result = strcmp("#SD", pMenu->Internal.ParserString);
				if(!Result)
				{	MenuEntry->DynMenuEnryType = MT_SENSING_DEVICE_INDEX;
					/* Check entries for sensing device */
					for(ycnt = 0; ycnt < ELEMENTS_FOR_MT_SD_ENTRY; ycnt++)
					{	/* Check data from file */
						for(xcnt = 0; xcnt < (MAX_PARSER_STRING - 1); xcnt++)
						{	*(UDINT*)&Parser = pMenu->Internal.ActAdrDataBuffer;
							/* Check for ',' or ';' --> indicates new element or end of line */
							if(*Parser == ',' || *Parser == ';')
							{	pMenu->Internal.ParserString[xcnt] = 0;
								pMenu->Internal.ActAdrDataBuffer++;
								break;
							}
								/* Copy data to ParserString */
							else
							{	pMenu->Internal.ParserString[xcnt] = *Parser;
								pMenu->Internal.ActAdrDataBuffer++;
							}
						}
						/* Enable variable */
						if(!ycnt)
						{	Status  = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->ShowEntryFunction1Adr), &(WorkVariableLength));
							if(Status)
							{	pMenu->Internal.InitStep = FREE_MEMALLOC;
								pMenu->Out.Status = Status;
								return(FUNCTION_BUSY); 
							}

							/* Data type from enable variable */
							Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(TypeEnableVariable),&(WorkVariableLength), &(DimVariable));
							if(Status)
							{	pMenu->Internal.InitStep = FREE_MEMALLOC;
								pMenu->Out.Status = Status;
								return(FUNCTION_BUSY); 
							}
							else if(TypeEnableVariable != VAR_TYPE_USINT && TypeEnableVariable != VAR_TYPE_UINT)
							{	pMenu->Internal.InitStep = FREE_MEMALLOC;
								pMenu->Out.Status = WRONG_TYPE_OF_ENABLE_VAR;
								return(FUNCTION_BUSY); 
								pMenu->Internal.ErrorNumber=(WRONG_TYPE_OF_ENABLE_VAR);
							}
							MenuEntry->ShowEntryFunction1Type=TypeEnableVariable;
						}
							// Enable Function 2013-02-13 E.Cilingir
						else if(ycnt == 1)
						{
							if (brsatoi((UDINT)&pMenu->Internal.ParserString)==0){
								MenuEntry->ShowEntryFunction1=LOW_SHOW;
							}
							else{
								MenuEntry->ShowEntryFunction1=HIGH_SHOW;
							}
						}
						if(ycnt == 2)
						{
							Status = strcmp("0", pMenu->Internal.ParserString);
							if (Status){
								Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->ShowEntryFunction2Adr), &(WorkVariableLength));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								/* Data type from enable variable */
								Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(TypeEnableVariable),&(WorkVariableLength), &(DimVariable));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								else if(TypeEnableVariable != VAR_TYPE_USINT && TypeEnableVariable != VAR_TYPE_UINT)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = WRONG_TYPE_OF_ENABLE_VAR;
									return(FUNCTION_BUSY); 
									pMenu->Internal.ErrorNumber=(WRONG_TYPE_OF_ENABLE_VAR);
								}
								MenuEntry->ShowEntryFunction2Type=TypeEnableVariable;
								MenuEntry->ShowEntryFunctionNumOfEnable=MenuEntry->ShowEntryFunctionNumOfEnable+1;
							}
						}	
							// Enable Function 2013-02-13 E.Cilingir
						else if(ycnt == 3)
						{
							if (brsatoi((UDINT)&pMenu->Internal.ParserString)==0){
								MenuEntry->ShowEntryFunction2=LOW_SHOW;
							}
							else{
								MenuEntry->ShowEntryFunction2=HIGH_SHOW;
							}
						}
							/* Variable name */
						else if(ycnt == 4)
						{	Status = strcmp("0", pMenu->Internal.ParserString);
							if (Status){
								Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->WorkVariableAdr), &(MenuEntry->WorkVariableLength));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}

								/* Data type from variable */
								Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->WorkVariableType),&(MenuEntry->WorkVariableLength), &(DimVariable));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								else if(MenuEntry->WorkVariableType != VAR_TYPE_USINT && MenuEntry->WorkVariableType != VAR_TYPE_BOOL)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = WRONG_TYPE_OF_SENSING_VAR;
									return(FUNCTION_BUSY); 
									pMenu->Internal.ErrorNumber=(WRONG_TYPE_OF_SENSING_VAR);
								}
							}
						}
							/* Variable name of Status Element*/
						else if(ycnt == 5)
						{	Status = strcmp("0", pMenu->Internal.ParserString);
							if (Status){
								Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->MaintVariableStatusAdr), &(MenuEntry->MaintVariableStatusLength));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}

								/* Data type from variable */
								Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->MaintVariableStatusType),&(MenuEntry->MaintVariableStatusLength), &(DimVariable));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}							
								else if(MenuEntry->MaintVariableStatusType != VAR_TYPE_USINT && MenuEntry->MaintVariableStatusType != VAR_TYPE_UINT)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = WRONG_TYPE_OF_STATUS_VAR;
									return(FUNCTION_BUSY); 
									pMenu->Internal.ErrorNumber=(WRONG_TYPE_OF_SENSING_VAR);
								}
							}
						}
							/* Compare Value of Status Element*/
						else if(ycnt == 6)
						{	/* Fix data */
							if (brsatoi((UDINT)&pMenu->Internal.ParserString)==1){
								MenuEntry->MaintVariableStatusCmpValue=1;
							}
							else{
								MenuEntry->MaintVariableStatusCmpValue=0;
							}
						}
							/* Index number for description */
						else if(ycnt == 7)
						{
							//MenuEntry->DescriptionIndex = brsatoi((UDINT)&pMenu->Internal.ParserString);
							if((pMenu->Internal.ParserString[0] >= 48 && pMenu->Internal.ParserString[0] <= 57) || pMenu->Internal.ParserString[0] == 45)
							{
								MenuEntry->DescriptionIndex = brsatoi((UDINT)&pMenu->Internal.ParserString);
							}
								/* variable */
							else
							{	Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->DescriptionIndexAdr), &(WorkVariableLength));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								/* Data type from variable */
								Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->DescriptionIndexType),&(MenuEntry->DescriptionIndexLength), &(DimVariable));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								else if(MenuEntry->DescriptionIndexType != VAR_TYPE_USINT && MenuEntry->DescriptionIndexType != VAR_TYPE_UINT)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = WRONG_TYPE_OF_SWITCH_VAR;
									return(FUNCTION_BUSY); 
									pMenu->Internal.ErrorNumber=(WRONG_TYPE_OF_SWITCH_VAR);	
								}
							}
						}							
							/* Index for button released text */
						else if(ycnt == 8)
						{
							MenuEntry->ButtonReleasedTextIndex = brsatoi((UDINT)&pMenu->Internal.ParserString);
						}
							/* Index for button pressed text */
						else if(ycnt == 9)
						{
							MenuEntry->ButtonPressedTextIndex = brsatoi((UDINT)&pMenu->Internal.ParserString);
						}							
							// ADD Scale Value	E.Cilingir 2013-03-13
						else if(ycnt == 10)
						{
							//MenuEntry->ScaleValue = brsatoi((UDINT)&pMenu->Internal.ParserString);
							if((pMenu->Internal.ParserString[0] >= 48 && pMenu->Internal.ParserString[0] <= 57) || pMenu->Internal.ParserString[0] == 45 || pMenu->Internal.ParserString[0] == 42)
							{
								MenuEntry->ScaleValue = brsatoi((UDINT)&pMenu->Internal.ParserString);
							}
								/* variable */
							else
							{	Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->ScaleValueAdr), &(MenuEntry->ScaleValueLength));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->ScaleValueType),&(TempLength), &(DimVariable));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								else if(MenuEntry->ScaleValueType != VAR_TYPE_USINT && MenuEntry->ScaleValueType != VAR_TYPE_UINT)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = WRONG_TYPE_OF_SCALE_VAR;
									return(FUNCTION_BUSY); 
									pMenu->Out.Status	= WRONG_TYPE_OF_SCALE_VAR;
									return(FUNCTION_BUSY); 			
								}
							}
						}	
						else if(ycnt == 11)
						{
							if((pMenu->Internal.ParserString[0] >= 48 && pMenu->Internal.ParserString[0] <= 57) || pMenu->Internal.ParserString[0] == 45 || pMenu->Internal.ParserString[0] == 42)
							{
								MenuEntry->AddFraction = brsatoi((UDINT)&pMenu->Internal.ParserString);
							}
								/* variable */
							else
							{	Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->AddFractionAdr), &(MenuEntry->AddFractionLength));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->AddFractionType),&(TempLength), &(DimVariable));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								else if(MenuEntry->AddFractionType != VAR_TYPE_USINT && MenuEntry->AddFractionType != VAR_TYPE_UINT)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = WRONG_TYPE_OF_FRACTION_VAR;
									return(FUNCTION_BUSY); 
									pMenu->Out.Status	= WRONG_TYPE_OF_FRACTION_VAR;
									return(FUNCTION_BUSY); 			
								}
							}
						}		
							/* Index number for unit description */
						else if(ycnt == 12)
						{
							//MenuEntry->UnitIndex = brsatoi((UDINT)&pMenu->Internal.ParserString);
							if((pMenu->Internal.ParserString[0] >= 48 && pMenu->Internal.ParserString[0] <= 57) || pMenu->Internal.ParserString[0] == 45 || pMenu->Internal.ParserString[0] == 42)
							{
								MenuEntry->UnitIndex = brsatoi((UDINT)&pMenu->Internal.ParserString);
							}
								/* variable */
							else
							{	Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->UnitIndexAdr), &(MenuEntry->UnitIndexLength));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->UnitIndexType),&(TempLength), &(DimVariable));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								else if(MenuEntry->UnitIndexType != VAR_TYPE_USINT && MenuEntry->UnitIndexType != VAR_TYPE_UINT)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = WRONG_TYPE_OF_SCALE_VAR;
									return(FUNCTION_BUSY); 
									pMenu->Out.Status	= WRONG_TYPE_OF_SCALE_VAR;
									return(FUNCTION_BUSY); 			
								}
							}
						}
							
							/* Variable name Output */
						else if(ycnt == 13)
						{	
							Status = strcmp("0", pMenu->Internal.ParserString);
							if (Status){
								Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->MaintVariableNOAdr), &(MenuEntry->MaintVariableNOLength));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}

								/* Data type from variable */
								Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->MaintVariableNOType),&(MenuEntry->MaintVariableNOLength), &(DimVariable));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
							}
						}
						else if(ycnt == 14)
						{	
							Status = strcmp("0", pMenu->Internal.ParserString);
							if (Status){
								Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->MaintVariableIPAdr), &(MenuEntry->MaintVariableIPLength));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}

								/* Data type from variable */
								Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->MaintVariableIPType),&(MenuEntry->MaintVariableIPLength), &(DimVariable));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
							}
						}
						else if(ycnt == 15)
						{	
							Status = strcmp("0", pMenu->Internal.ParserString);
							if (Status){
								Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->MaintVariableOPAdr), &(MenuEntry->MaintVariableOPLength));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}

								/* Data type from variable */
								Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->MaintVariableOPType),&(MenuEntry->MaintVariableOPLength), &(DimVariable));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
							}
						}
							/* Help ID */
						else if(ycnt == 16)
						{
							MenuEntry->HelpID = brsatoi((UDINT)&pMenu->Internal.ParserString);
						}						
							// ADD USER LEVEL FUNCTION INFORMATION	E.Cilingir 2013-02-06
						else if(ycnt == 17)
						{
							MenuEntry->Level = brsatoi((UDINT)&pMenu->Internal.ParserString);
						}
						else if(ycnt == 18)
						{
							MenuEntry->LevelButton = brsatoi((UDINT)&pMenu->Internal.ParserString);
						}	

					}
					/* Increase number of Entries */
					pMenu->Internal.ActEntryNumber++;
					/* Search for next type definition */
					pMenu->Internal.InitStep = ENTRY_MARK;
					pMenu->Out.Status = FUNCTION_BUSY;
					return(pMenu->Out.Status);
				}
				//-------------------------------------------------------------------------------------------------------------
				//------------------------------------------- SWITCH ----------------------------------------------------------
				Result = strcmp("#SW", pMenu->Internal.ParserString);
				if(!Result)
				{	MenuEntry->DynMenuEnryType = MT_SWITCH_INDEX;
					/* Check entries for sensing device */
					for(ycnt = 0; ycnt < ELEMENTS_FOR_MT_SW_ENTRY; ycnt++)
					{	/* Check data from file */
						for(xcnt = 0; xcnt < (MAX_PARSER_STRING - 1); xcnt++)
						{	*(UDINT*)&Parser = pMenu->Internal.ActAdrDataBuffer;
							/* Check for ',' or ';' --> indicates new element or end of line */
							if(*Parser == ',' || *Parser == ';')
							{	pMenu->Internal.ParserString[xcnt] = 0;
								pMenu->Internal.ActAdrDataBuffer++;
								break;
							}
								/* Copy data to ParserString */
							else
							{	pMenu->Internal.ParserString[xcnt] = *Parser;
								pMenu->Internal.ActAdrDataBuffer++;
							}
						}
						/* Enable variable */
						if(!ycnt)
						{	Status  = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->ShowEntryFunction1Adr), &(WorkVariableLength));
							if(Status)
							{	pMenu->Internal.InitStep = FREE_MEMALLOC;
								pMenu->Out.Status = Status;
								return(FUNCTION_BUSY); 
							}

							/* Data type from enable variable */
							Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(TypeEnableVariable),&(WorkVariableLength), &(DimVariable));
							if(Status)
							{	pMenu->Internal.InitStep = FREE_MEMALLOC;
								pMenu->Out.Status = Status;
								return(FUNCTION_BUSY); 
							}
							else if(TypeEnableVariable != VAR_TYPE_USINT && TypeEnableVariable != VAR_TYPE_UINT)
							{	pMenu->Internal.InitStep = FREE_MEMALLOC;
								pMenu->Out.Status = WRONG_TYPE_OF_ENABLE_VAR;
								return(FUNCTION_BUSY); 
								pMenu->Internal.ErrorNumber=(WRONG_TYPE_OF_ENABLE_VAR);
							}
							MenuEntry->ShowEntryFunction1Type=TypeEnableVariable;
						}
							// Enable Function 2013-02-13 E.Cilingir
						else if(ycnt == 1)
						{
							if (brsatoi((UDINT)&pMenu->Internal.ParserString)==0){
								MenuEntry->ShowEntryFunction1=LOW_SHOW;
							}
							else{
								MenuEntry->ShowEntryFunction1=HIGH_SHOW;
							}
						}
						if(ycnt == 2)
						{
							Status = strcmp("0", pMenu->Internal.ParserString);
							if (Status){
								Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->ShowEntryFunction2Adr), &(WorkVariableLength));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								/* Data type from enable variable */
								Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(TypeEnableVariable),&(WorkVariableLength), &(DimVariable));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								else if(TypeEnableVariable != VAR_TYPE_USINT && TypeEnableVariable != VAR_TYPE_UINT)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = WRONG_TYPE_OF_ENABLE_VAR;
									return(FUNCTION_BUSY); 
									pMenu->Internal.ErrorNumber=(WRONG_TYPE_OF_ENABLE_VAR);
								}
								MenuEntry->ShowEntryFunction2Type=TypeEnableVariable;
								MenuEntry->ShowEntryFunctionNumOfEnable=MenuEntry->ShowEntryFunctionNumOfEnable+1;
							}
						}	
							// Enable Function 2013-02-13 E.Cilingir
						else if(ycnt == 3)
						{
							if (brsatoi((UDINT)&pMenu->Internal.ParserString)==0){
								MenuEntry->ShowEntryFunction2=LOW_SHOW;
							}
							else{
								MenuEntry->ShowEntryFunction2=HIGH_SHOW;
							}
						}
							/* Variable name */
						else if(ycnt == 4)
						{	Status = strcmp("0", pMenu->Internal.ParserString);
							if (Status){
								Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->WorkVariableAdr), &(MenuEntry->WorkVariableLength));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}

								/* Data type from variable */
								Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->WorkVariableType),&(MenuEntry->WorkVariableLength), &(DimVariable));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								else if(MenuEntry->WorkVariableType != VAR_TYPE_USINT && MenuEntry->WorkVariableType != VAR_TYPE_BOOL)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = WRONG_TYPE_OF_SWITCH_VAR;
									return(FUNCTION_BUSY); 
									pMenu->Internal.ErrorNumber=(WRONG_TYPE_OF_SWITCH_VAR);
								}
							}
						}
							/* Variable name of Status Element*/
						else if(ycnt == 5)
						{	Status = strcmp("0", pMenu->Internal.ParserString);
							if (Status){
								Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->MaintVariableStatusAdr), &(MenuEntry->MaintVariableStatusLength));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}

								/* Data type from variable */
								Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->MaintVariableStatusType),&(MenuEntry->MaintVariableStatusLength), &(DimVariable));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}							
								else if(MenuEntry->MaintVariableStatusType != VAR_TYPE_USINT && MenuEntry->MaintVariableStatusType != VAR_TYPE_UINT)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = WRONG_TYPE_OF_STATUS_VAR;
									return(FUNCTION_BUSY); 
									pMenu->Internal.ErrorNumber=(WRONG_TYPE_OF_SENSING_VAR);
								}
							}
						}
							/* Compare Value of Status Element*/
						else if(ycnt == 6)
						{	/* Fix data */
							if (brsatoi((UDINT)&pMenu->Internal.ParserString)==1){
								MenuEntry->MaintVariableStatusCmpValue=1;
							}
							else{
								MenuEntry->MaintVariableStatusCmpValue=0;
							}
						}
							/* Index number for description */
						else if(ycnt == 7)
						{
							//MenuEntry->DescriptionIndex = brsatoi((UDINT)&pMenu->Internal.ParserString);
							if((pMenu->Internal.ParserString[0] >= 48 && pMenu->Internal.ParserString[0] <= 57) || pMenu->Internal.ParserString[0] == 45)
							{
								MenuEntry->DescriptionIndex = brsatoi((UDINT)&pMenu->Internal.ParserString);
							}
								/* variable */
							else
							{	Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->DescriptionIndexAdr), &(WorkVariableLength));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								/* Data type from variable */
								Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->DescriptionIndexType),&(MenuEntry->DescriptionIndexLength), &(DimVariable));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								else if(MenuEntry->DescriptionIndexType != VAR_TYPE_USINT && MenuEntry->DescriptionIndexType != VAR_TYPE_UINT)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = WRONG_TYPE_OF_SWITCH_VAR;
									return(FUNCTION_BUSY); 
									pMenu->Internal.ErrorNumber=(WRONG_TYPE_OF_SWITCH_VAR);	
								}
							}
						}							
							/* Index for button released text */
						else if(ycnt == 8)
						{
							MenuEntry->ButtonReleasedTextIndex = brsatoi((UDINT)&pMenu->Internal.ParserString);
						}
							/* Index for button pressed text */
						else if(ycnt == 9)
						{
							MenuEntry->ButtonPressedTextIndex = brsatoi((UDINT)&pMenu->Internal.ParserString);
						}							
							// ADD Scale Value	E.Cilingir 2013-03-13
						else if(ycnt == 10)
						{
							//MenuEntry->ScaleValue = brsatoi((UDINT)&pMenu->Internal.ParserString);
							if((pMenu->Internal.ParserString[0] >= 48 && pMenu->Internal.ParserString[0] <= 57) || pMenu->Internal.ParserString[0] == 45 || pMenu->Internal.ParserString[0] == 42)
							{
								MenuEntry->ScaleValue = brsatoi((UDINT)&pMenu->Internal.ParserString);
							}
								/* variable */
							else
							{	Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->ScaleValueAdr), &(MenuEntry->ScaleValueLength));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->ScaleValueType),&(TempLength), &(DimVariable));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								else if(MenuEntry->ScaleValueType != VAR_TYPE_USINT && MenuEntry->ScaleValueType != VAR_TYPE_UINT)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = WRONG_TYPE_OF_SCALE_VAR;
									return(FUNCTION_BUSY); 
									pMenu->Out.Status	= WRONG_TYPE_OF_SCALE_VAR;
									return(FUNCTION_BUSY); 			
								}
							}
						}	
						else if(ycnt == 11)
						{
							if((pMenu->Internal.ParserString[0] >= 48 && pMenu->Internal.ParserString[0] <= 57) || pMenu->Internal.ParserString[0] == 45 || pMenu->Internal.ParserString[0] == 42)
							{
								MenuEntry->AddFraction = brsatoi((UDINT)&pMenu->Internal.ParserString);
							}
								/* variable */
							else
							{	Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->AddFractionAdr), &(MenuEntry->AddFractionLength));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->AddFractionType),&(TempLength), &(DimVariable));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								else if(MenuEntry->AddFractionType != VAR_TYPE_USINT && MenuEntry->AddFractionType != VAR_TYPE_UINT)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = WRONG_TYPE_OF_FRACTION_VAR;
									return(FUNCTION_BUSY); 
									pMenu->Out.Status	= WRONG_TYPE_OF_FRACTION_VAR;
									return(FUNCTION_BUSY); 			
								}
							}
						}		
							/* Index number for unit description */
						else if(ycnt == 12)
						{
							//MenuEntry->UnitIndex = brsatoi((UDINT)&pMenu->Internal.ParserString);
							if((pMenu->Internal.ParserString[0] >= 48 && pMenu->Internal.ParserString[0] <= 57) || pMenu->Internal.ParserString[0] == 45 || pMenu->Internal.ParserString[0] == 42)
							{
								MenuEntry->UnitIndex = brsatoi((UDINT)&pMenu->Internal.ParserString);
							}
								/* variable */
							else
							{	Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->UnitIndexAdr), &(MenuEntry->UnitIndexLength));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->UnitIndexType),&(TempLength), &(DimVariable));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								else if(MenuEntry->UnitIndexType != VAR_TYPE_USINT && MenuEntry->UnitIndexType != VAR_TYPE_UINT)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = WRONG_TYPE_OF_SCALE_VAR;
									return(FUNCTION_BUSY); 
									pMenu->Out.Status	= WRONG_TYPE_OF_SCALE_VAR;
									return(FUNCTION_BUSY); 			
								}
							}
						}
							
							/* Variable name Output */
						else if(ycnt == 13)
						{	
							Status = strcmp("0", pMenu->Internal.ParserString);
							if (Status){
								Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->MaintVariableNOAdr), &(MenuEntry->MaintVariableNOLength));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}

								/* Data type from variable */
								Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->MaintVariableNOType),&(MenuEntry->MaintVariableNOLength), &(DimVariable));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
							}
						}
						else if(ycnt == 14)
						{	
							Status = strcmp("0", pMenu->Internal.ParserString);
							if (Status){
								Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->MaintVariableIPAdr), &(MenuEntry->MaintVariableIPLength));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}

								/* Data type from variable */
								Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->MaintVariableIPType),&(MenuEntry->MaintVariableIPLength), &(DimVariable));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
							}
						}
						else if(ycnt == 15)
						{	
							Status = strcmp("0", pMenu->Internal.ParserString);
							if (Status){
								Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->MaintVariableOPAdr), &(MenuEntry->MaintVariableOPLength));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}

								/* Data type from variable */
								Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->MaintVariableOPType),&(MenuEntry->MaintVariableOPLength), &(DimVariable));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
							}
						}
							/* Help ID */
						else if(ycnt == 16)
						{
							MenuEntry->HelpID = brsatoi((UDINT)&pMenu->Internal.ParserString);
						}						
							// ADD USER LEVEL FUNCTION INFORMATION	E.Cilingir 2013-02-06
						else if(ycnt == 17)
						{
							MenuEntry->Level = brsatoi((UDINT)&pMenu->Internal.ParserString);
						}
						else if(ycnt == 18)
						{
							MenuEntry->LevelButton = brsatoi((UDINT)&pMenu->Internal.ParserString);
						}	

					}
					/* Increase number of Entries */
					pMenu->Internal.ActEntryNumber++;
					/* Search for next type definition */
					pMenu->Internal.InitStep = ENTRY_MARK;
					pMenu->Out.Status = FUNCTION_BUSY;
					return(pMenu->Out.Status);
				}
				//-------------------------------------------------------------------------------------------------------------
				//---------------------------------------- FUNCTION HEADLINE --------------------------------------------------
				Result = strcmp("#FH", pMenu->Internal.ParserString);
				if(!Result)
				{	MenuEntry->DynMenuEnryType = MT_FUNCTIONHEADLINE_INDEX;
					/* Check entries for switch */
					for(ycnt = 0; ycnt < ELEMENTS_FOR_MT_FH_ENTRY; ycnt++)
					{	/* Check data from file */
						for(xcnt = 0; xcnt < (MAX_PARSER_STRING - 1); xcnt++)
						{	*(UDINT*)&Parser = pMenu->Internal.ActAdrDataBuffer;
							/* Check for ',' or ';' --> indicates new element or end of line */
							if(*Parser == ',' || *Parser == ';')
							{	pMenu->Internal.ParserString[xcnt] = 0;
								pMenu->Internal.ActAdrDataBuffer++;
								break;
							}
								/* Copy data to ParserString */
							else
							{	pMenu->Internal.ParserString[xcnt] = *Parser;
								pMenu->Internal.ActAdrDataBuffer++;
							}
						}
						/* Enable variable */
						if(!ycnt)
						{	Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->ShowEntryFunction1Adr), &(WorkVariableLength));
							if(Status)
							{	pMenu->Internal.InitStep = FREE_MEMALLOC;
								pMenu->Out.Status = Status;
								return(FUNCTION_BUSY); 
							}

							/* Data type from enable variable */
							Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(TypeEnableVariable),&(WorkVariableLength), &(DimVariable));
							if(Status)
							{	pMenu->Internal.InitStep = FREE_MEMALLOC;
								pMenu->Out.Status = Status;
								return(FUNCTION_BUSY); 
							}
							else if(TypeEnableVariable != VAR_TYPE_USINT && TypeEnableVariable != VAR_TYPE_UINT)
							{	pMenu->Internal.InitStep = FREE_MEMALLOC;
								pMenu->Out.Status = WRONG_TYPE_OF_ENABLE_VAR;
								return(FUNCTION_BUSY); 
								pMenu->Internal.ErrorNumber=(WRONG_TYPE_OF_ENABLE_VAR);
							}
							MenuEntry->ShowEntryFunction1Type=TypeEnableVariable;
							TempEnableType1=TypeEnableVariable;
							TempAdrEnable1=MenuEntry->ShowEntryFunction1Adr;
						}
							// Enable Function 2013-02-13 E.Cilingir
						else if(ycnt == 1)
						{
							if (brsatoi((UDINT)&pMenu->Internal.ParserString)==0){
								MenuEntry->ShowEntryFunction1=LOW_SHOW;
							}
							else{
								MenuEntry->ShowEntryFunction1=HIGH_SHOW;
							}
							TempShowLogic1=MenuEntry->ShowEntryFunction1;

						}
						if(ycnt == 2)
						{
							Status = strcmp("0", pMenu->Internal.ParserString);
							if (Status){
								Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->ShowEntryFunction2Adr), &(WorkVariableLength));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								/* Data type from enable variable */
								Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(TypeEnableVariable),&(WorkVariableLength), &(DimVariable));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								else if(TypeEnableVariable != VAR_TYPE_USINT && TypeEnableVariable != VAR_TYPE_UINT)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = WRONG_TYPE_OF_ENABLE_VAR;
									return(FUNCTION_BUSY); 
									pMenu->Internal.ErrorNumber=(WRONG_TYPE_OF_ENABLE_VAR);
								}
								MenuEntry->ShowEntryFunction2Type=TypeEnableVariable;
								MenuEntry->ShowEntryFunctionNumOfEnable=MenuEntry->ShowEntryFunctionNumOfEnable+1;
								TempNumOfEnable=MenuEntry->ShowEntryFunctionNumOfEnable;
								TempEnableType2=TypeEnableVariable;
								TempAdrEnable2=MenuEntry->ShowEntryFunction2Adr;
							}
						}
							// Enable Function 2013-02-13 E.Cilingir
						else if(ycnt == 3)
						{
							if (brsatoi((UDINT)&pMenu->Internal.ParserString)==0){
								MenuEntry->ShowEntryFunction2=LOW_SHOW;
							}
							else{
								MenuEntry->ShowEntryFunction2=HIGH_SHOW;
							}
							TempShowLogic2=MenuEntry->ShowEntryFunction2;
						}
							/* Variable name Status Line */
						else if(ycnt == 4)
						{	
							Status = strcmp("-1", pMenu->Internal.ParserString);
							if (Status){
								Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->FunctionHeadlineStatusAdr), &(MenuEntry->FunctionHeadlineStatusLength));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}

								/* Data type from variable */
								Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->FunctionHeadlineStatusType),&(MenuEntry->FunctionHeadlineStatusLength), &(DimVariable));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								else if(MenuEntry->FunctionHeadlineStatusType != VAR_TYPE_USINT && MenuEntry->FunctionHeadlineStatusType != VAR_TYPE_UINT)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = WRONG_TYPE_OF_SWITCH_VAR;
									return(FUNCTION_BUSY); 
									pMenu->Internal.ErrorNumber=(WRONG_TYPE_OF_SWITCH_VAR);	
								}
							}
							else
							{
								MenuEntry->FunctionHeadlineStatusAdr=0;
							}
						}						
						else if(ycnt == 5)
						{
							if((pMenu->Internal.ParserString[0] >= 48 && pMenu->Internal.ParserString[0] <= 57) || pMenu->Internal.ParserString[0] == 45)
							{
								MenuEntry->DescriptionIndex = brsatoi((UDINT)&pMenu->Internal.ParserString);
							}
								/* variable */
							else
							{	Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->DescriptionIndexAdr), &(WorkVariableLength));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								/* Data type from variable */
								Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->DescriptionIndexType),&(MenuEntry->DescriptionIndexLength), &(DimVariable));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								else if(MenuEntry->DescriptionIndexType != VAR_TYPE_USINT && MenuEntry->DescriptionIndexType != VAR_TYPE_UINT)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = WRONG_TYPE_OF_SWITCH_VAR;
									return(FUNCTION_BUSY); 
									pMenu->Internal.ErrorNumber=(WRONG_TYPE_OF_SWITCH_VAR);	
								}
							}
						}
						else if(ycnt == 6)
						{
							MenuEntry->FunctionHeadlineBitmap = brsatoi((UDINT)&pMenu->Internal.ParserString);
						}
						else if(ycnt == 7)
						{
							MenuEntry->HelpID = brsatoi((UDINT)&pMenu->Internal.ParserString);
						}
						else if(ycnt == 8)
						{
							MenuEntry->Level = brsatoi((UDINT)&pMenu->Internal.ParserString);
							MenuEntry->LevelStatus = brsatoi((UDINT)&pMenu->Internal.ParserString);
							MenuEntry->LevelButton = brsatoi((UDINT)&pMenu->Internal.ParserString);
							TempLevel=MenuEntry->Level;
						}
					}
					/* Increase number of Entries */
					AdrEnableFunctionHeadLine=MenuEntry->ShowEntryFunction1Adr;
					pMenu->Internal.ActEntryNumber++;
					// Put in a Empty Line for the Function HeadLine
					*(UDINT*)&MenuEntry = (AdrMenuEntryStructure + (sizeof(*MenuEntry)*pMenu->Internal.ActEntryNumber));
					MenuEntry->ShowEntryFunction1Adr=AdrEnableFunctionHeadLine;
					MenuEntry->DynMenuEnryType = MT_EMPTYLINE_INDEX;
					MenuEntry->Level = TempLevel;
					MenuEntry->ShowEntryFunction1=EnableFunctionType;
			
					MenuEntry->ShowEntryFunction1Type = 	TempEnableType1;
					MenuEntry->ShowEntryFunction2Type = 	TempEnableType2;
			
					MenuEntry->ShowEntryFunction1Adr=TempAdrEnable1;			
					MenuEntry->ShowEntryFunction2Adr=TempAdrEnable2;			
				
					MenuEntry->ShowEntryFunctionNumOfEnable=TempNumOfEnable;
			
					MenuEntry->ShowEntryFunction1=TempShowLogic1;
					MenuEntry->ShowEntryFunction2=TempShowLogic2;
					pMenu->Internal.ActEntryNumber++;
					/* Search for next type definition */
					pMenu->Internal.InitStep = ENTRY_MARK;
					pMenu->Out.Status = FUNCTION_BUSY;
					return(pMenu->Out.Status);
				}
				//-------------------------------------------------------------------------------------------------------------
				//---------------------------------------- HEADLINE -----------------------------------------------------------
				Result = strcmp("#HD", pMenu->Internal.ParserString);
				if(!Result)
				{	MenuEntry->DynMenuEnryType = MT_HEADLINE_INDEX;
					/* Check entries for headline */
					for(ycnt = 0; ycnt < ELEMENTS_FOR_PD_HD_ENTRY; ycnt++)
					{	/* Check data from file */
						for(xcnt = 0; xcnt < (MAX_PARSER_STRING - 1); xcnt++)
						{	*(UDINT*)&Parser = pMenu->Internal.ActAdrDataBuffer;
							/* Check for ',' or ';' --> indicates new element or end of line */
							if(*Parser == ',' || *Parser == ';')
							{	pMenu->Internal.ParserString[xcnt] = 0;
								pMenu->Internal.ActAdrDataBuffer++;
								break;
							}
								/* Copy data to ParserString */
							else
							{	pMenu->Internal.ParserString[xcnt] = *Parser;
								pMenu->Internal.ActAdrDataBuffer++;
							}
						}
						/* Enable variable */
						if(!ycnt)
						{	Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->ShowEntryFunction1Adr), &(WorkVariableLength));
							if(Status)
							{	pMenu->Internal.InitStep = FREE_MEMALLOC;
								pMenu->Out.Status = Status;
								return(FUNCTION_BUSY); 
							}

							/* Data type from enable variable */
							Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(TypeEnableVariable),&(WorkVariableLength), &(DimVariable));
							if(Status)
							{	pMenu->Internal.InitStep = FREE_MEMALLOC;
								pMenu->Out.Status = Status;
								return(FUNCTION_BUSY); 
							}
							else if(TypeEnableVariable != VAR_TYPE_USINT && TypeEnableVariable != VAR_TYPE_UINT)
							{	pMenu->Internal.InitStep = FREE_MEMALLOC;
								pMenu->Out.Status = WRONG_TYPE_OF_ENABLE_VAR;
								return(FUNCTION_BUSY); 
								pMenu->Internal.ErrorNumber=(WRONG_TYPE_OF_ENABLE_VAR);			
							}
							MenuEntry->ShowEntryFunction1Type=TypeEnableVariable;
						}
							// Enable Function 2013-02-13 E.Cilingir
						else if(ycnt == 1)
						{
							if (brsatoi((UDINT)&pMenu->Internal.ParserString)==0){
								MenuEntry->ShowEntryFunction1=LOW_SHOW;
							}
							else{
								MenuEntry->ShowEntryFunction1=HIGH_SHOW;
							}
						}
						if(ycnt == 2)
						{
							Status = strcmp("0", pMenu->Internal.ParserString);
							if (Status){
								Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->ShowEntryFunction2Adr), &(WorkVariableLength));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								/* Data type from enable variable */
								Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(TypeEnableVariable),&(WorkVariableLength), &(DimVariable));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								else if(TypeEnableVariable != VAR_TYPE_USINT && TypeEnableVariable != VAR_TYPE_UINT)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = WRONG_TYPE_OF_ENABLE_VAR;
									return(FUNCTION_BUSY); 
									pMenu->Internal.ErrorNumber=(WRONG_TYPE_OF_ENABLE_VAR);	
								}
								MenuEntry->ShowEntryFunction2Type=TypeEnableVariable;
								MenuEntry->ShowEntryFunctionNumOfEnable=MenuEntry->ShowEntryFunctionNumOfEnable+1;
							}
						}
							// Enable Function 2013-02-13 E.Cilingir
						else if(ycnt == 3)
						{
							if (brsatoi((UDINT)&pMenu->Internal.ParserString)==0){
								MenuEntry->ShowEntryFunction2=LOW_SHOW;
							}
							else{
								MenuEntry->ShowEntryFunction2=HIGH_SHOW;
							}
						}
							/* Index number for description */
						else if(ycnt == 4)
						{
							//MenuEntry->DescriptionIndex = brsatoi((UDINT)&pMenu->Internal.ParserString);
							if((pMenu->Internal.ParserString[0] >= 48 && pMenu->Internal.ParserString[0] <= 57) || pMenu->Internal.ParserString[0] == 45)
							{
								MenuEntry->DescriptionIndex = brsatoi((UDINT)&pMenu->Internal.ParserString);
							}
								/* variable */
							else
							{	Status = PV_xgetadr((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->DescriptionIndexAdr), &(WorkVariableLength));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								/* Data type from variable */
								Status = PV_ninfo((STRING*)&(pMenu->Internal.ParserString), &(MenuEntry->DescriptionIndexType),&(MenuEntry->DescriptionIndexLength), &(DimVariable));
								if(Status)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = Status;
									return(FUNCTION_BUSY); 
								}
								else if(MenuEntry->DescriptionIndexType != VAR_TYPE_USINT && MenuEntry->DescriptionIndexType != VAR_TYPE_UINT)
								{	pMenu->Internal.InitStep = FREE_MEMALLOC;
									pMenu->Out.Status = WRONG_TYPE_OF_SWITCH_VAR;
									return(FUNCTION_BUSY); 
									pMenu->Internal.ErrorNumber=(WRONG_TYPE_OF_SWITCH_VAR);	
								}
							}
						}
							/* Help ID */
						else if(ycnt == 5)
						{
							MenuEntry->HelpID = brsatoi((UDINT)&pMenu->Internal.ParserString);
						}
							// ADD USER LEVEL FUNCTION INFORMATION	E.Cilingir 2013-02-06
						else if(ycnt == 6)
						{
							MenuEntry->Level = brsatoi((UDINT)&pMenu->Internal.ParserString);
						}
					}
					/* Increase number of Entries */
					pMenu->Internal.ActEntryNumber++;
					/* Search for next type definition */
					pMenu->Internal.InitStep = ENTRY_MARK;
					pMenu->Out.Status = FUNCTION_BUSY;
					return(pMenu->Out.Status);
				}
			}
			//-------------------------------------------------------------------------------------------------------------
			//---------------------------------------- End Of the File ----------------------------------------------------
			Result = strcmp("#END", pMenu->Internal.ParserString);
			if(!Result){
				//-------------------------------------------------------------------------------------------------------------
				//---------------------------------------- FREE MEM ALLOC -----------------------------------------------------
				Status = TMP_free(pMenu->Internal.FileSize, (void**)pMenu->Internal.StartAdrDataBuffer);
				if(!Status){
					pMenu->Internal.InitStep = INIT_OK;
					return(FUNCTION_BUSY);
				}
				else if(Status != 65535){
					pMenu->Internal.InitStep = FILE_OPEN;
					pMenu->Out.Status = Status;
					return(pMenu->Out.Status);
				}
				else{
					pMenu->Out.Status = Status;
					return(pMenu->Out.Status);
				}
			}
			//-------------------------------------------------------------------------------------------------------------
			//---------------------------------------- Empty Function -----------------------------------------------------
			Result = strcmp("", pMenu->Info.FunctionType);
			if (!Result){	
				pMenu->Internal.InitStep = FREE_MEMALLOC;
				pMenu->Out.Status = EMPTY_FUNCTION_TYPE;
				return(FUNCTION_BUSY);
			}
			//-------------------------------------------------------------------------------------------------------------
			//---------------------------------------- Not supported Function ---------------------------------------------
			if (Result)	{	
				pMenu->Internal.InitStep = FREE_MEMALLOC;
				pMenu->Out.Status = NOT_SUPPORTED_FUNCTION;
				return(FUNCTION_BUSY);
			}
			break;
		//##########################################################################################################################################
		//#################################################### Case Free Memalloc ##################################################################
		case FREE_MEMALLOC:
			Status = TMP_free(pMenu->Internal.FileSize, (void**)pMenu->Internal.StartAdrDataBuffer);
			if(!Status){	
				pMenu->Internal.InitStep = ERROR;
				return(FUNCTION_BUSY);
			}
			else if(Status != 65535){	
				pMenu->Internal.InitStep = ERROR;
				return(FUNCTION_BUSY);
			}
			else{
				pMenu->Internal.InitStep = ERROR;
				return(FUNCTION_BUSY);
			}	
			break;		
		//##########################################################################################################################################
		//#################################################### Case Error Close File ###############################################################
		case ERROR_FILE_CLOSE:
			pMenu->Internal.FClose.enable = 1;
			pMenu->Internal.FClose.ident = pMenu->Internal.FileIdent;
			FileClose(&pMenu->Internal.FClose);

			if(!pMenu->Internal.FClose.status){
				pMenu->Internal.ActAdrDataBuffer = pMenu->Internal.StartAdrDataBuffer;
				pMenu->Internal.InitStep = FREE_MEMALLOC;
				return(FUNCTION_BUSY);
			}
			else if(pMenu->Internal.FClose.status != 65535){
				pMenu->Internal.InitStep = FREE_MEMALLOC;
				pMenu->Out.Status	= pMenu->Internal.FClose.status;
				return(FUNCTION_BUSY);	
			}
			else{
				pMenu->Out.Status	= pMenu->Internal.FClose.status;
				return(pMenu->Out.Status);	
			}
			break;
		//##########################################################################################################################################
		//#################################################### Case Error ##########################################################################
		case ERROR:
			if(pMenu->Internal.ErrorNumber!=0){
				pMenu->Out.Status=pMenu->Internal.ErrorNumber;
				return(pMenu->Out.Status);
			}
			else {
				return(pMenu->Out.Status);
			}
			break;		
		//##########################################################################################################################################
		//#################################################### Case Init Ok ########################################################################
		case INIT_OK:
			pMenu->Out.Status = FUNCTION_OK;
			pMenu->Out.InitOk = 1;
			return(pMenu->Out.Status);
			break;	
	}
	return(FUNCTION_OK);
}


