//***********************************************************************************************************************)
// Object name: RunMenu                                                                                       			*)
// Author:      ?                                                                                             			*)
// Site:        B&R ?                                                                                       			*)
// Created:     ?                                                                                           		    *)
// Restriction: ?                                                                            				            *)
// Description: Dynamic Menu Creation														                            *)
//                                                                                                                      *)
//----------------------------------------------------------------------------------------------------------------------*)
// Version 3.00.0  06-FEB-2013 E.Cilingir                       Development tool: B&R Automation Studio V3.0.90.21 SP03 *)
//                                                                                                                      *)
// + Add User Level Functionality to the Standard for every Element                                                     *)
// + Add the Function HeadLine as Element						                                                        *)
// 	 -- + Add Type EmptyLine for the Function Headline, to get 2 Lines in Visu for the New Funtion HeadLine             *)
//   -- + Add FunctionHeadlineBitmap as Infomration for the Element 			                                        *)
//   -- + Add FunctionHeadlineStatusAdr, FunctionHeadlineStatusType, FunctionHeadlineStatusLength 						*)
//		  for the Variable of the Status																				*)
//   -- + Add LevelStatus for Show the Status depending to the Status                                                   *)
// 																														*)
//--------------------------------------------------------------------------------------------------------------------- *)
// Version 3.00.1  18-FEB-2013 E.Cilingir                       Development tool: B&R Automation Studio V3.0.90.21 SP03 *)
// + Expand UINT FOR Enable Variable									                                                *)
// + Expand Function for Enable if Value is 0 or Value is 1																*)
// + Expand Function for add Fraction digits																			*)
// + Expand Variable Type TIME																							*)
// + If Max Value is 0 then put in the max value of the Variable Type													*)
// + Expand Scale function in dependency to the number of fraction digits [SINT,USINT,INT,UINT,DINT,UDINT]				*)
//				10^Fraction digits																						*)
// + Min and Max Value Types can be different, For every min and max type a selection will be done						*)
// 																														*)
//--------------------------------------------------------------------------------------------------------------------- *)
// Version 3.00.2  05-MAR-2013 E.Cilingir                       Development tool: B&R Automation Studio V3.0.90.21 SP03 *)
// + Add #IP for Show Input States										                                                *)
// + Add #OP for Show Output States										                                                *)
// + Function Headline Button is show disabled, if enabl variable is 0	                                                *)
// 																														*)
//***********************************************************************************************************************)
// Version 3.00.3  18-MAR-2013 E.Cilingir                       Development tool: B&R Automation Studio V3.0.90.21 SP03 *)
// + Functionality for scale is use for 10^n scaling for every Type														*)
// + Add fraction Digits for show number of fractions after the comma													*)
// 																														*)
//***********************************************************************************************************************)
// Version 3.00.4  20-MAR-2013 E.Cilingir                       Development tool: B&R Automation Studio V3.0.90.21 SP03 *)
// + Value of UPDOWN without comletition pointer was initialized but not connected to the value							*)
// 																														*)
//***********************************************************************************************************************)
// Version 3.00.5  02-APR-2013 E.Cilingir                       Development tool: B&R Automation Studio V3.0.90.21 SP03 *)
// + Nummeric Input expanded with MinMax Calculation. A reference can be configured and a Min and Max Value will be 	*)
//	 calculated of the configured values																				*)
// + Name of the Variables checked an changed to more meaningful names												 	*)
// + Max Values are now in Constant Variable																			*)
// 																														*)
//***********************************************************************************************************************)
// Version 3.00.6  03-APR-2013 E.Cilingir                       Development tool: B&R Automation Studio V3.0.90.21 SP03 *)
// + Add Variable to the Info Struct to make Elements Visible/Invisible depending to the number of enabled entries		*)
// 																														*)
//***********************************************************************************************************************)
// Version 3.00.7  09-APR-2013 E.Cilingir                       Development tool: B&R Automation Studio V3.0.90.21 SP03 *)
// Bugfix:	Min Max Calculation for Real Value was not working right													*)
// + Add Help Function	as HTML Browser to the DynMenu																	*)
// + Change Steps of Case in Init to be able to Get Infomration parallel												*)
// 																														*)
//***********************************************************************************************************************)
// Version 3.01.0  05-JUN-2013 E.Cilingir                       Development tool: B&R Automation Studio V3.0.90.21 SP03 *)
// Bugfix:	Min Max Calculation for Real Value was not working right													*)
// + Add calculation Functionality for Nummeric input																	*)
// + Expand the Up Down Function, it is possible to increase/drcrease variable in limits								*)
// + Expecting Version Information in File and checking if it is supported by the Libiary Version						*)
//																														*)
//***********************************************************************************************************************)
// Version 3.01.1  05-JUL-2013 E.Cilingir                       Development tool: B&R Automation Studio V3.0.90.21 SP03 *)
// Bugfix:	Convert Stepwidth from string to float																		*)
// + Add Maintainance Functionality																						*)
//																														*)
//***********************************************************************************************************************)
// Version 3.01.2  19-SEP-2013 E.Cilingir                       Development tool: B&R Automation Studio V3.0.90.21 SP03 *)
// Bugfix:	Scale for NI of Real was missing																			*)
// Bugfix:	Scale of Input Value to PLC for NI of Time was missing														*)
// Bugfix:	MT Function output Element of Type Bool was Missing															*)
// Bugfix:	MT Function output Element was the Fraction information missing												*)
// Bugfix:	Sensing Device in non comilation was not working right for production										*)
// Bugfix:	Disabled View for Momentary button missing																	*)
// Bugfix:	Disabled View for Up/Down Element missing																	*)
// Bugfix:	Character cast to MO_Ver change																				*)
//	+	Add Headline possibility to Maintanance function																*)
//	+	Add Groupbox visibility automatic out of the enties per page info, Give also the possility to set groupbox to 	*)
//			a fixed height																								*)
//	+	Add Message, if no of entries to show are 0 																	*)
//	+	Change page_info_typ to in/out/internal/info struct																*)
// Bugfix:	Number of Pages not right																					*)
//																														*)
//***********************************************************************************************************************)
// Version 3.03.0  27-aug-2014 E.Cilingir                       Development tool: B&R Automation Studio V3.0.90.28 SP10 *)
// Bugfix:		[#440] Dynamic Menu UpDown datapoint is not decreasing/increasing all steps								*)
// 				   Value of Dynamic Menu element must be writen into a temporary variable and this variable has to 		*)
//				   be writen into the configured variable adress														*)
// Improvement:	[#441] Option to show all Elements on Dynamic Menu														*)
// 				   new in parameters DisableUserLevel and DisableShowLogic												*)
// Improvement:	[#437] Using scaling + unit groups in Dynamic Menu														*)
// 				   - Delete NumericInput[#NI] (PD,MT) logic and add to UpDown [#UD]logic								*)
// 				   - Variable read out value for scaling and write to structure											*)
// 				   - Variable read out value for unit index and write to structure										*)
// 				   - Delete calculating of 10^n sacling function, this will be done in visualization by unit groups		*)
// 				   - Delete AsBrMath library, because scaling is done on visualization									*)
// Bugfix:		[#414] Dynamic Menu is not working in Terminal Mode														*)
// 				   - Working with StatusDataPoint and setting the information's by bits							 		*)
// 				   - Disable updateing of the value elements during input is active								 		*)
// Bugfix:		Change Data Type of Point to UINT																		*)
//***********************************************************************************************************************)
// Version 3.04.0  06-sep-2015 E.Cilingir                       Development tool: B&R Automation Studio V3.0.90.30 SP12 *)
// Improvement:	[#452] Change fraction digits by variable inside Dynamic Menu											*)
// 				   - Read out fraction as variable and write value to structure											*)
// 				   - Centalize fraction evaluation and delete out of the elements										*)
// Bugfix:	Check variable type for #AI if string																		*)
// Bugfix:	For Loop to make other elements then MT_NUMERIC_INPUT_INDEX,MT_SENSING_DEVICE_INDEX,MT_SWITCH_INDEX			*)
//			invisible until const MAX_STATUS																			*)
// Bugfix:	Check for more entires per page configured than in const MAX_ELEMENT_PAGE									*)
// Improvement:	[#454] Dynamic Menu #AO with text index of textgroup and only description text							*)
// 				   - 0 = No text; STRING = Show value out of string; UINT,USINT = Show text out of text group			*)
// Bugfix:		[#456] DynamicMenu UpDown not working after disabled view												*)
// 				   - Reset Locking Datapoint after changing to enabled view												*)
// 				   - Add functionality to demo porject page 1010.txt													*)
// Bugfix:		[#470] Dynamic Menu Sensing Device writes value 61 into variable 										*)
// 				   - Do pointing action regarding the variable type                                                     *)
// 				   - Write fix value 0,1 to configured variable                                                         *)
// 				   - #SD - || to && - (Completion && ValueBut == 61)                                                    *)
// Bugfix:		[#444] Shutdown (PP520) when scrolling through Dynamic Menu Page 										*)
// 				   - Delete Line: (VisMenu + (TmpEntriesPerPage - 1 - xcnt))->Status[14] |= 0x0001;                     *)
//***********************************************************************************************************************)
// Version 3.05.1  13-mar-2016 N.Moreira                        Development tool: B&R Automation Studio V4.1.9.44 SP    *)
// Bugfix:	[#482] Visibility of element #SW in maintenance mode fails                             						*)
// 				   - Visibility calculation for #SW handles now indices 23-25 of status array correctly. 				*)
//***********************************************************************************************************************)
// Version 3.05.2  24-apr-2019 D.Benner                       Development tool: B&R Automation Studio V4.5.2.102 	    *)
// Bugfix:	[#525] Pointer in IF-condition                            													*)
//					- Added an additional comparison to the IF-condition to avoid compiler warnings						*)
//***********************************************************************************************************************)
	
//######################################################################################################################################
//#################################################### INCLUDE LIBIARIES ###############################################################
#include <bur/plctypes.h>
#include <DynMenu.h>
#include <string.h>
//######################################################################################################################################
//#################################################### CONSTANTS #######################################################################
#define	COLOR_RELEASED		46
#define	COLOR_PRESSED		226
#define	COLOR_LOCKED		59

#define MIN(a, b) ( (a) < (b) ? (a) : (b) )
#define MAX(a, b) ( (a) > (b) ? (a) : (b) )
#define LIMIT(a, b, c) (MAX(a, MIN(b, c)))
DYN_Page_Type TempPage ;

/* Function to set entry status data */
void EntryStatus(USINT Enable, USINT Type, UINT Point, UDINT AdrUseStatusVar, UDINT AdrStatusVar, UDINT	AdrColorVar, UDINT AdrPoint);

//##########################################################################################################################################
//#################################################### FUNCTION RUNMENU ####################################################################
//##########################################################################################################################################
UINT runmenu(UDINT AdrMenuStructure, UDINT AdrVisMenuStructure, UDINT AdrPageStructure)
{	/* Structure pointer */
	DYN_Visu_Type	*VisMenu;
	DYN_Menu_Type 	*pMenu;
	DYN_Entry_Type 	*MenuEntry;
	DYN_Page_Type	*Page;

	/* Pointer Variables */
	USINT	*enablePointerUSINT = 0;
	USINT	*enablePointerUINT = 0;
	USINT	*enablePointerUSINT2 = 0;
	USINT	*enablePointerUINT2 = 0;
	BOOL	*boolPointer = 0;
	SINT	*sintPointer = 0;
	USINT	*usintPointer = 0;
	USINT	*usintPointer2 = 0;
	INT		*intPointer = 0;
	UINT	*uintPointer = 0;
	DINT	*dintPointer = 0;
	UDINT	*udintPointer = 0;
	REAL	*realPointer = 0;	
	TIME	*TimePointer = 0;
	REAL	 UpDownValStep =0;
	REAL	 UpDownValMin =0;
	REAL	 UpDownValMax =0;
	REAL	 UpDownRefValue =0;
	REAL	 UpDownCalcValNum =0;
	UDINT 	 AdrMenuEntryStructure=0;
	REAL	 MtncCompareValue = 0;

	//STRING	HelpString[50];
	/* Variables */
	USINT	ActEnabledEntries = 0 , TmpGroupBoxNoHeight = 0, TmpEntriesPerPage = 0;
	UINT	xcnt = 0, ycnt = 0, zcnt = 0, ActVisEntryNumber = 0, EntryBefore = 0, Disabled = 0, PageFull = 0, EnabledEntries = 0;
	
	// ADD VARIABLE FOR LEVEL FUNCTION 				E.Cilingir 2013-02-06
	Byte2Bit_typ ByteToBit;                   		// variable of type Byte2Bit_typ
	BOOL PanelLevelArray[16];                       // bit array for Actual Level
	BOOL ValueLevelArray[16];                       // bit array for Configured Level
	BOOL EnableEntry;								// Variable for Show Entries
	BOOL EnableButton;								// Variable for Show Entries
	USINT EnableEntryShow;							// Variable for Show Entries
	USINT* Pointer;									// Pointer variable	
	INT i;											// Loop Variable
	INT k;											// Loop Variable
	BOOL CmdPageUp=0;								// Variable Page Up command	
	BOOL CmdPageDown=0;								// Variable Page Down command	
	BOOL NewHeadLineFound;							// Variable for Show only one Headline for Maintanance Functionality
	BOOL NrLinesOk=0;								// Variable Page Up command	
	BOOL StopUpDate=0;								// Variable for stop updating during active input
	
	//##########################################################################################################################################
	//#################################################### Check ADRESSES ######################################################################
	if(!AdrMenuStructure || !AdrVisMenuStructure ||  !AdrPageStructure){
		return(WRONG_DATA_FOR_FUB);
	}
	else{	/* Pointer to address */

		*(UDINT*)&VisMenu = AdrVisMenuStructure;
		*(UDINT*)&Page = AdrPageStructure;

		*(UDINT*)&pMenu = AdrMenuStructure;
		AdrMenuEntryStructure = (UDINT)&(pMenu->Entry);
		*(UDINT*)&MenuEntry = AdrMenuEntryStructure;
	}
	//##########################################################################################################################################
	//#################################################### RESET USER LEVEL ARRAY ##############################################################
	memset(&PanelLevelArray, 0, sizeof(PanelLevelArray));
	memset(&ValueLevelArray, 0, sizeof(ValueLevelArray));
	//##########################################################################################################################################
	//#################################################### SET USER LEVEL BITS ##################################################################
	for (i = 0; i<= MAXLEVEL; i ++)	{
		if (Page->Level==i){
			PanelLevelArray[i]=1;
			break;
		}
	}
	//##########################################################################################################################################
	//#################################################### Wrong entries to Display ############################################################
	NrLinesOk=1;
	if(!pMenu->Config.EntriesPerPage&&pMenu->Out.InitOk){
		pMenu->Out.Status	=	NO_ENTRIES_TO_DISPLAY;
		NrLinesOk		  	=	0;
	}
	if((pMenu->Config.EntriesPerPage > MAX_ELEMENT_PAGE + 1) && pMenu->Out.InitOk){
		pMenu->Config.EntriesPerPage = MAX_ELEMENT_PAGE + 1;
	}
	//##########################################################################################################################################
	//#################################################### Do visibility of Groupbox ###########################################################
	if(pMenu->Out.InitOk && NrLinesOk){	
		memset(Page->Out.GrpBoxStatus, 1, sizeof(Page->Out.GrpBoxStatus));
		TmpEntriesPerPage = pMenu->Config.EntriesPerPage;
		TmpGroupBoxNoHeight = pMenu->Config.GrpBoxNoHeightElements;
		if(TmpGroupBoxNoHeight == 0){
			TmpGroupBoxNoHeight = TmpEntriesPerPage;
		}
		else if(TmpEntriesPerPage > TmpGroupBoxNoHeight){
			TmpEntriesPerPage = TmpGroupBoxNoHeight;
		}
		Page->Out.GrpBoxStatus[TmpGroupBoxNoHeight-1] = 0;
	}
	//##########################################################################################################################################
	//#################################################### RESET VARIABLE ######################################################################
	Page->OneMorePage = 0;
	if(Page->ResetStructure == 1 && pMenu->Out.InitOk && NrLinesOk){	
		memset(VisMenu, 0, sizeof(DYN_Visu_Type)*TmpEntriesPerPage);
		Page->ResetStructure = 0;
		/* Reset Help ID */
		Page->HelpID = 0;
	}
	//##########################################################################################################################################
	//#################################################### Maintanance Function closed #########################################################
	if(pMenu->Internal.EntryOpendFromPage > 0 && pMenu->Internal.EntryActive == 0){
		// Return to Old Page and set everything to old visibility
		Page->CurrentPage = pMenu->Internal.EntryOpendFromPage;
		memcpy(&Page->ShiftOffset,&TempPage.ShiftOffset,sizeof(TempPage.ShiftOffset));
		pMenu->Internal.EntryOpendFromPage = 0;
	}
	//##########################################################################################################################################
	//#################################################### Shift Offsets of Page ###############################################################
	if((Page->CurrentPage == 0) || (Page->CurrentPage < Page->LastPage)){
		/* Init the ShiftOffset structure dependent on current page */
		for(zcnt = (Page->CurrentPage + 1); zcnt <= Page->NumberOfPages; zcnt++){
			Page->ShiftOffset[zcnt] = 0;
		}
	}
	Page->LastPage = Page->CurrentPage; 
	//##########################################################################################################################################
	//#################################################### CALCULATE NUMBER OF ENABLED ENTRIES #################################################
	ActEnabledEntries=0;
	if (pMenu->Out.InitOk==1 && NrLinesOk){
		NewHeadLineFound=0;
		for(xcnt = 0; xcnt < pMenu->Internal.ActEntryNumber; xcnt++){
			//-------------------------------------------------------------------------------------------------------------
			//------------------------- Check the Enable Configuration for showing the Entry-------------------------------
			if ((MenuEntry + xcnt)->ShowEntryFunction1Type == VAR_TYPE_USINT){
				*(UDINT*)&enablePointerUSINT= (MenuEntry + xcnt)->ShowEntryFunction1Adr;	
				EnableEntryShow=0;
				if (((MenuEntry + xcnt)->ShowEntryFunction1 == LOW_SHOW) && (*enablePointerUSINT==0)){
					EnableEntryShow=1;
				}
				else if (((MenuEntry + xcnt)->ShowEntryFunction1 == HIGH_SHOW) && (*enablePointerUSINT==1)){
					EnableEntryShow=1;
				}
				else if ((*enablePointerUSINT>1)){
					EnableEntryShow=1;
				}
			}//if ((MenuEntry + xcnt)->ShowEntryFunction1Type == VAR_TYPE_USINT)
			if ((MenuEntry + xcnt)->ShowEntryFunction1Type == VAR_TYPE_UINT){
				*(UDINT*)&enablePointerUINT= (MenuEntry + xcnt)->ShowEntryFunction1Adr;	
				EnableEntryShow=0;
				if (((MenuEntry + xcnt)->ShowEntryFunction1 == LOW_SHOW) && (*enablePointerUINT==0)){
					EnableEntryShow=1;
				}
				else if (((MenuEntry + xcnt)->ShowEntryFunction1 == HIGH_SHOW) && (*enablePointerUINT==1)){
					EnableEntryShow=1;
				}
				else if ((*enablePointerUINT>1)){
					EnableEntryShow=1;
				}
			}//if ((MenuEntry + xcnt)->ShowEntryFunction1Type == VAR_TYPE_UINT)
			switch ((MenuEntry+ xcnt)->ShowEntryFunctionNumOfEnable){

				case NUM_ENABLE_1:	break;
			
				case NUM_ENABLE_2: if(EnableEntryShow==1){	
						if ((MenuEntry + xcnt)->ShowEntryFunction2Type == VAR_TYPE_USINT){
							*(UDINT*)&enablePointerUSINT2= (MenuEntry + xcnt)->ShowEntryFunction2Adr;	
							EnableEntryShow=0;
							if (((MenuEntry + xcnt)->ShowEntryFunction2 == LOW_SHOW) && (*enablePointerUSINT2==0)){
								EnableEntryShow=1;
							}
							else if (((MenuEntry + xcnt)->ShowEntryFunction2 == HIGH_SHOW) && (*enablePointerUSINT2==1)){
								EnableEntryShow=1;
							}
							else if ((*enablePointerUSINT2>1)){
								EnableEntryShow=1;
							}
						}
						if ((MenuEntry + xcnt)->ShowEntryFunction2Type == VAR_TYPE_UINT){
							*(UDINT*)&enablePointerUINT2= (MenuEntry + xcnt)->ShowEntryFunction2Adr;	
							EnableEntryShow=0;
							if (((MenuEntry + xcnt)->ShowEntryFunction2 == LOW_SHOW) && (*enablePointerUINT2==0)){
								EnableEntryShow=1;
							}
							else if (((MenuEntry + xcnt)->ShowEntryFunction2 == HIGH_SHOW) && (*enablePointerUINT2==1)){
								EnableEntryShow=1;
							}
							else if ((*enablePointerUINT2>1)){
								EnableEntryShow=1;
							}
						}
					}
					break;	
			}//switch ((MenuEntry+ xcnt)->ShowEntryFunctionNumOfEnable)
			//-------------------------------------------------------------------------------------------------------------
			//-Diplay only the Elements after the selected Headline until the next Headline fo rMaintanance functioniality-
			if (pMenu->Internal.EntryActive > 0){
				if(((MenuEntry + xcnt)->DynMenuEnryType == MT_FUNCTIONHEADLINE_INDEX) && xcnt == pMenu->Internal.EntryActive ){
					NewHeadLineFound=1;
				}
				else if(((MenuEntry + xcnt)->DynMenuEnryType == MT_FUNCTIONHEADLINE_INDEX) && xcnt > pMenu->Internal.EntryActive){
					NewHeadLineFound=0;
				}					
				
				if(!NewHeadLineFound){
					EnableEntryShow=0;
				}
			}//if (pMenu->Internal.EntryActive > 0)
			if (pMenu->In.DisableShowLogic == 1 && (MenuEntry + xcnt)->DynMenuEnryType > 0){
				EnableEntryShow=1;
			}
			//-------------------------------------------------------------------------------------------------------------
			//---------------------------------------- Check the User Level if the Entry is Enabled -----------------------
			if (EnableEntryShow){
				if (pMenu->In.DisableUserLevel == 1 && (MenuEntry + xcnt)->DynMenuEnryType > 0){
					ActEnabledEntries++;
				}
				else{
					ByteToBit.byteadr = (UDINT) &(MenuEntry + xcnt)->Level;        	// store adress of byte array into structure
					ByteToBit.length = 2;                            			   	// store length of byte array into structure
					Byte2Bit(&ByteToBit);                               		   	// convert byte array into bit array
					Pointer = (void *) ByteToBit.bitadr;          				  	// get adress of byte bit
					for (i = 0; i<= MAXLEVEL; i ++){								// Put the Bit Infomration into the Array
						ValueLevelArray[i] = *(Pointer+i);
					}
					for (i = 0; i<= MAXLEVEL; i ++){								// Check if the Bit for the UserLevel and the active Level is set
						if(EnableEntryShow && (ValueLevelArray[i]==1)&&(PanelLevelArray[i]==1)){
							ActEnabledEntries++;
							break;													// Break Loop after increasing the EnabledEntries
						}
					}
				}
			}//if (EnableEntryShow)
		}//for(xcnt = 0; xcnt < pMenu->Internal.ActEntryNumber; xcnt++)
		//-------------------------------------------------------------------------------------------------------------
		//---------------------------------------- Copy number of enabled entries to info structure -------------------
		if(ActEnabledEntries != pMenu->Internal.NumberEnabledEntries){
			pMenu->Internal.NumberEnabledEntries = ActEnabledEntries;
			/* Reset vis-structure */
			for(ycnt = 0; ycnt < TmpEntriesPerPage; ycnt++)	{	
				memset(VisMenu, 0, sizeof(DYN_Visu_Type)*TmpEntriesPerPage);
			}
			Page->HelpID = 0;
		}//if(ActEnabledEntries != pMenu->Internal.NumberEnabledEntries)
	}//if (pMenu->Out.InitOk==1)
	//##########################################################################################################################################
	//#################################################### MAIN WORKING OF THE DYNAMIC MENU ####################################################
	//##########################################################################################################################################
	if (pMenu->Out.InitOk==1 && NrLinesOk){
		NewHeadLineFound=0;
		//-------------------------------------------------------------------------------------------------------------
		//---------------------------------------- Loop the Infomration -----------------------------------------------
		for(xcnt = 0; xcnt < pMenu->Internal.ActEntryNumber; xcnt++)	{
			if(ActVisEntryNumber == TmpEntriesPerPage){
				PageFull = 1;
			}
			EnableEntryShow=0;		
			EnableEntry = 0;		
			//-------------------------------------------------------------------------------------------------------------
			//---------------------------------------- Check the Enable conditions ----------------------------------------			
			if ((MenuEntry + xcnt)->ShowEntryFunction1Type == VAR_TYPE_USINT){
				*(UDINT*)&enablePointerUSINT= (MenuEntry + xcnt)->ShowEntryFunction1Adr;	
				EnableEntryShow=0;
				if (((MenuEntry + xcnt)->ShowEntryFunction1 == LOW_SHOW) && (*enablePointerUSINT==0)){
					EnableEntryShow=1;
				}
				else if (((MenuEntry + xcnt)->ShowEntryFunction1 == HIGH_SHOW) && (*enablePointerUSINT==1)){
					EnableEntryShow=1;
				}
				else if ((*enablePointerUSINT>1)){
					EnableEntryShow=2;
				}
			}//if ((MenuEntry + xcnt)->ShowEntryFunction1Type == VAR_TYPE_USINT)
			if ((MenuEntry + xcnt)->ShowEntryFunction1Type == VAR_TYPE_UINT){
				*(UDINT*)&enablePointerUINT= (MenuEntry + xcnt)->ShowEntryFunction1Adr;	
				EnableEntryShow=0;
				if (((MenuEntry + xcnt)->ShowEntryFunction1 == LOW_SHOW) && (*enablePointerUINT==0)){
					EnableEntryShow=1;
				}
				else if (((MenuEntry + xcnt)->ShowEntryFunction1 == HIGH_SHOW) && (*enablePointerUINT==1)){
					EnableEntryShow=1;
				}
				else if ((*enablePointerUINT>1)){
					EnableEntryShow=2;
				}
			}//if ((MenuEntry + xcnt)->ShowEntryFunction1Type == VAR_TYPE_UINT)
			switch ((MenuEntry+ xcnt)->ShowEntryFunctionNumOfEnable){
				case NUM_ENABLE_1:	break;
				case NUM_ENABLE_2: if(EnableEntryShow==1){	
						if ((MenuEntry + xcnt)->ShowEntryFunction2Type == VAR_TYPE_USINT){
							*(UDINT*)&enablePointerUSINT2= (MenuEntry + xcnt)->ShowEntryFunction2Adr;	
							EnableEntryShow=0;
							if (((MenuEntry + xcnt)->ShowEntryFunction2 == LOW_SHOW) && (*enablePointerUSINT2==0)){
								EnableEntryShow=1;
							}
							else if (((MenuEntry + xcnt)->ShowEntryFunction2 == HIGH_SHOW) && (*enablePointerUSINT2==1)){
								EnableEntryShow=1;
							}
							else if ((*enablePointerUSINT2>1)){
								EnableEntryShow=2;
							}
						}
						if ((MenuEntry + xcnt)->ShowEntryFunction2Type == VAR_TYPE_UINT){
							*(UDINT*)&enablePointerUINT2= (MenuEntry + xcnt)->ShowEntryFunction2Adr;	
							EnableEntryShow=0;
							if (((MenuEntry + xcnt)->ShowEntryFunction2 == LOW_SHOW) && (*enablePointerUINT2==0)){
								EnableEntryShow=1;
							}
							else if (((MenuEntry + xcnt)->ShowEntryFunction2 == HIGH_SHOW) && (*enablePointerUINT2==1)){
								EnableEntryShow=1;
							}
							else if ((*enablePointerUINT2>1)){
								EnableEntryShow=2;
							}
						}
					}
					break;	
			}//switch ((MenuEntry+ xcnt)->ShowEntryFunctionNumOfEnable)
			//-------------------------------------------------------------------------------------------------------------
			//-Diplay only the Elements after the selected Headline until the next Headline fo rMaintanance functioniality-
			if (pMenu->Internal.EntryActive > 0){
				if(((MenuEntry + xcnt)->DynMenuEnryType == MT_FUNCTIONHEADLINE_INDEX) && xcnt == pMenu->Internal.EntryActive ){
					NewHeadLineFound=1;
				}
				else if(((MenuEntry + xcnt)->DynMenuEnryType == MT_FUNCTIONHEADLINE_INDEX) && xcnt > pMenu->Internal.EntryActive){
					NewHeadLineFound=0;
				}					
				if(!NewHeadLineFound){
					EnableEntryShow=0;
				}
			}//if (pMenu->Internal.EntryActive > 0)
			if (pMenu->In.DisableShowLogic == 1){
				EnableEntryShow=1;
			}
			//-------------------------------------------------------------------------------------------------------------
			//---------------------------------------- Check User Enable if Enable is active ------------------------------
			if (EnableEntryShow){
				if (pMenu->In.DisableUserLevel == 1 && (MenuEntry + xcnt)->DynMenuEnryType > 0){
					EnabledEntries++;										// Increase Entry number
					EnableEntry = 1;										// Set the Enable Entry for the following commands
				}
				else{
					// Check additional the userLevel Bits, if the Entry will be displayed				E.Cilingir 2013-02-06
					ByteToBit.byteadr = (UDINT) &(MenuEntry + xcnt)->Level;        	// store adress of byte array into structure
					ByteToBit.length = 2;                            			   	// store length of byte array into structure
					Byte2Bit(&ByteToBit);                               		   	// convert byte array into bit array
					Pointer = (void *) ByteToBit.bitadr;          				  	// get adress of byte bit
					for (i = 0; i<= MAXLEVEL; i ++){								// Put the Bit Infomration into the Array
						ValueLevelArray[i] = *(Pointer+i);
					}
					for (i = 0; i<= MAXLEVEL; i ++){
						if(EnableEntryShow && (ValueLevelArray[i]==1)&&(PanelLevelArray[i]==1)){
							EnabledEntries++;										// Increase Entry number
							EnableEntry = 1;										// Set the Enable Entry for the following commands
							break;													// Break the Loop because Entry will be displayed
						}
					}
				}
			}		
			//-------------------------------------------------------------------------------------------------------------
			//---------------------------------------- Page not Full and Enable Entrie ------------------------------------
			if(EnableEntryShow && EnableEntry && !PageFull){
				(VisMenu + (xcnt - Disabled - EntryBefore))->StatusHeadline 					|= 0x0001;
				(VisMenu + (xcnt - Disabled - EntryBefore))->StatusText 						|= 0x0001;
				(VisMenu + (xcnt - Disabled - EntryBefore))->StatusTextFunctionHeadLine 		|= 0x0001;
				(VisMenu + (xcnt - Disabled - EntryBefore))->StatusNaviFunctionHeadLine 		|= 0x0001;
				//-------------------------------------------------------------------------------------------------------------
				//--------------------------- Check if the Button/Nummeric Input is enabled for the user level ----------------
				EnableButton=0;
				if (( MenuEntry + xcnt)->DynMenuEnryType == MT_NUMERIC_INPUT_INDEX
					||(MenuEntry + xcnt)->DynMenuEnryType == MT_SENSING_DEVICE_INDEX
					||(MenuEntry + xcnt)->DynMenuEnryType == MT_SWITCH_INDEX
					||(MenuEntry + xcnt)->DynMenuEnryType == MT_FUNCTIONHEADLINE_INDEX
				||(MenuEntry + xcnt)->DynMenuEnryType == PD_FUNCTIONHEADLINE_INDEX){
					// Check additional the userLevel Bits, if the Entry will be displayed
					if (pMenu->In.DisableUserLevel == 1){
						EnableButton=1;
					}
					else{
						ByteToBit.byteadr = (UDINT) &(MenuEntry + xcnt)->LevelButton;   // store adress of byte array into structure
						ByteToBit.length = 2;                            			   	// store length of byte array into structure
						Byte2Bit(&ByteToBit);                               		   	// convert byte array into bit array
						Pointer = (void *) ByteToBit.bitadr;          				  	// get adress of byte bit
						for (i = 0; i<= MAXLEVEL; i ++){								// Put the Bit Infomration into the Array
							ValueLevelArray[i] = *(Pointer+i);
						}
						for (i = 0; i<= MAXLEVEL; i ++){
							if((ValueLevelArray[i]==1)&&(PanelLevelArray[i]==1)){
								EnableButton=1;
								break;
							}
						}
					}
				}
				//-------------------------------------------------------------------------------------------------------------
				//---------------------------------------- Entry offsets ------------------------------------------------------
				if(pMenu->Internal.EntryOffset > 0){
					EntryBefore=pMenu->Internal.EntryOffset + TmpEntriesPerPage* Page->CurrentPage;
				}
				if(((Page->CurrentPage * TmpEntriesPerPage) == (EntryBefore + Page->ShiftOffset[Page->CurrentPage]) && pMenu->Internal.EntryOffset==0)){
					//-------------------------------------------------------------------------------------------------------------
					//---------------------------------------- Show the standard Informations -------------------------------------
					(VisMenu + (xcnt - Disabled - EntryBefore))->StatusNaviFunctionHeadLine |= 	0x0001;		
					(VisMenu + (xcnt - Disabled - EntryBefore))->HelpID						=	(MenuEntry + xcnt)->HelpID;	
					//-------------------------------------------------------------------------------------------------------------
					//---------------------------------------- UINIT INDEX --------------------------------------------------------
					if ((MenuEntry + xcnt)->UnitIndexAdr > 0){
						if ((MenuEntry + xcnt)->UnitIndexAdr > 0){
							switch ((MenuEntry + xcnt)->UnitIndexType){				
								case VAR_TYPE_UINT:		*(UDINT*)&uintPointer = (MenuEntry + xcnt)->UnitIndexAdr;
									(VisMenu + (xcnt - Disabled - EntryBefore))->UnitIndex = *uintPointer;
									break;							
								case VAR_TYPE_USINT:	*(UDINT*)&usintPointer = (MenuEntry + xcnt)->UnitIndexAdr;
									(VisMenu + (xcnt - Disabled - EntryBefore))->UnitIndex = *usintPointer;
									break;					
							}
						}
					}
					else{
						(VisMenu + (xcnt - Disabled - EntryBefore))->UnitIndex = (MenuEntry + xcnt)->UnitIndex;
					}
					//-------------------------------------------------------------------------------------------------------------
					//---------------------------------------- FRACTION INDEX -----------------------------------------------------
					if ((MenuEntry + xcnt)->AddFractionAdr > 0){
						if ((MenuEntry + xcnt)->AddFractionAdr > 0){
							switch ((MenuEntry + xcnt)->AddFractionType){				
								case VAR_TYPE_UINT:		*(UDINT*)&uintPointer = (MenuEntry + xcnt)->AddFractionAdr;
									(VisMenu + (xcnt - Disabled - EntryBefore))->AddFraction = *uintPointer;
									break;							
								case VAR_TYPE_USINT:	*(UDINT*)&usintPointer = (MenuEntry + xcnt)->AddFractionAdr;
									(VisMenu + (xcnt - Disabled - EntryBefore))->AddFraction = *usintPointer;
									break;					
							}
						}
					}
					else{
						(VisMenu + (xcnt - Disabled - EntryBefore))->AddFraction = (MenuEntry + xcnt)->AddFraction;
					}
					//-------------------------------------------------------------------------------------------------------------
					//---------------------------------------- DESCRIPTION INDEX --------------------------------------------------
					if ((MenuEntry + xcnt)->DescriptionIndexAdr!=0){
						if((MenuEntry + xcnt)->DescriptionIndexAdr>0){
							if((MenuEntry + xcnt)->DescriptionIndexType == VAR_TYPE_UINT){
								*(UDINT*)&uintPointer = (MenuEntry + xcnt)->DescriptionIndexAdr;
								(VisMenu + (xcnt - Disabled - EntryBefore))->DescriptionIndex = *uintPointer;
							}
							else if((MenuEntry + xcnt)->DescriptionIndexType == VAR_TYPE_USINT)	{
								*(UDINT*)&usintPointer = (MenuEntry + xcnt)->DescriptionIndexAdr;
								(VisMenu + (xcnt - Disabled - EntryBefore))->DescriptionIndex = *usintPointer;
							}
						}
					}
					else{
						(VisMenu + (xcnt - Disabled - EntryBefore))->DescriptionIndex = (MenuEntry + xcnt)->DescriptionIndex;
					}
					//-------------------------------------------------------------------------------------------------------------
					//---------------------------------------- HelpID --------------------------------------------------------
					if (Page->VCHelpID != 0){
						if (Page->VCHelpID == (xcnt + 1 - Disabled - EntryBefore)){
							Page->HelpID = (MenuEntry + xcnt)->HelpID;
							Page->VCHelpID = 0;
						}
					}
					//-------------------------------------------------------------------------------------------------------------
					// #NO ----------------------------------- PRODUCTION NUMMERIC OUTPUT -----------------------------------------
					if((MenuEntry + xcnt)->DynMenuEnryType == PD_NUMERIC_OUTPUT_INDEX)
					{	switch ((MenuEntry + xcnt)->WorkVariableType){
							case VAR_TYPE_BOOL:		*(UDINT*)&boolPointer = (MenuEntry + xcnt)->WorkVariableAdr;								
								(VisMenu + (xcnt - Disabled - EntryBefore))->ValueNum = *boolPointer;
								break;
							case VAR_TYPE_DINT:		*(UDINT*)&dintPointer = (MenuEntry + xcnt)->WorkVariableAdr;
								(VisMenu + (xcnt - Disabled - EntryBefore))->ValueNum = *dintPointer;
								break;
							case VAR_TYPE_UDINT:	*(UDINT*)&udintPointer = (MenuEntry + xcnt)->WorkVariableAdr;
								(VisMenu + (xcnt - Disabled - EntryBefore))->ValueNum = *udintPointer;
								break;				
							case VAR_TYPE_INT:		*(UDINT*)&intPointer = (MenuEntry + xcnt)->WorkVariableAdr;
								(VisMenu + (xcnt - Disabled - EntryBefore))->ValueNum = *intPointer;
								break;					
							case VAR_TYPE_UINT:		*(UDINT*)&uintPointer = (MenuEntry + xcnt)->WorkVariableAdr;
								(VisMenu + (xcnt - Disabled - EntryBefore))->ValueNum = *uintPointer;
								break;				
							case VAR_TYPE_SINT:		*(UDINT*)&sintPointer = (MenuEntry + xcnt)->WorkVariableAdr;
								(VisMenu + (xcnt - Disabled - EntryBefore))->ValueNum = *sintPointer;
								break;				
							case VAR_TYPE_USINT:	*(UDINT*)&usintPointer = (MenuEntry + xcnt)->WorkVariableAdr;
								(VisMenu + (xcnt - Disabled - EntryBefore))->ValueNum = *usintPointer;
								break;					
							case VAR_TYPE_REAL:		*(UDINT*)&realPointer = (MenuEntry + xcnt)->WorkVariableAdr;
								(VisMenu + (xcnt - Disabled - EntryBefore))->ValueNum = *realPointer;
								break;
							case VAR_TYPE_TIME:		*(UDINT*)&TimePointer = (MenuEntry + xcnt)->WorkVariableAdr;
								(VisMenu + (xcnt - Disabled - EntryBefore))->ValueNum = *TimePointer;
								break;	
						}
					}
					//-------------------------------------------------------------------------------------------------------------
					// #AI ----------------------------------- PRODUCTION ALPHANUMMERIC INPUT NO COMPLETITION ---------------------
					if((MenuEntry + xcnt)->DynMenuEnryType == PD_ALPHANUMERIC_INPUT_INDEX 
						&& !(VisMenu + (xcnt - Disabled - EntryBefore))->Completion)
					{
						if((((VisMenu + (xcnt - Disabled - EntryBefore))->Status[1] & 0x1000) == 0x0000)
							||(((VisMenu + (xcnt - Disabled - EntryBefore))->Status[1] & 0x8000) == 0x0000))
						{/* Copy string into vis-structure if length is OK */
							if((MenuEntry + xcnt)->WorkVariableLength <= sizeof((VisMenu + (xcnt - Disabled - EntryBefore))->ValueAlph))
							{
								memcpy(&(VisMenu + (xcnt - Disabled - EntryBefore))->ValueAlph, (USINT*)(MenuEntry + xcnt)->WorkVariableAdr, (MenuEntry + xcnt)->WorkVariableLength);
							}
								/* Copy only defined lenght of string */
							else
							{	memcpy(&(VisMenu + (xcnt - Disabled - EntryBefore))->ValueAlph, (USINT*)(MenuEntry + xcnt)->WorkVariableAdr, sizeof((VisMenu + (xcnt - Disabled - EntryBefore))->ValueAlph));
								(VisMenu + (xcnt - Disabled - EntryBefore))->ValueAlph[sizeof((VisMenu + (xcnt - Disabled - EntryBefore))->ValueAlph)-1] = 0;
							}
						}
					}
						/* Alphanumeric input field and completion is set --> write string */
					else if((MenuEntry + xcnt)->DynMenuEnryType == PD_ALPHANUMERIC_INPUT_INDEX && (VisMenu + (xcnt - Disabled - EntryBefore))->Completion)
					{	/* Copy string back if length is OK */
							if((MenuEntry + xcnt)->WorkVariableLength <= sizeof((VisMenu + (xcnt - Disabled - EntryBefore))->ValueAlph))
							{	(VisMenu + (xcnt - Disabled - EntryBefore))->ValueAlph[(MenuEntry + xcnt)->WorkVariableLength -1] = 0;
								memcpy((USINT*)(MenuEntry + xcnt)->WorkVariableAdr, &(VisMenu + (xcnt - Disabled - EntryBefore))->ValueAlph, (MenuEntry + xcnt)->WorkVariableLength);
								/* Set number of last changed entry */
								pMenu->Internal.LastChangedEntry = xcnt + 1;
								/* Reset completion value */
								(VisMenu + (xcnt - Disabled - EntryBefore))->Completion = 0;
							}
								/* Copy only defined length back */
							else
							{	memcpy((USINT*)(MenuEntry + xcnt)->WorkVariableAdr, &(VisMenu + (xcnt - Disabled - EntryBefore))->ValueAlph, sizeof((VisMenu + (xcnt - Disabled - EntryBefore))->ValueAlph));
								/* Set number of last changed entry */
								pMenu->Internal.LastChangedEntry = xcnt + 1;
								/* Reset completion value */
								(VisMenu + (xcnt - Disabled - EntryBefore))->Completion = 0;
							}
					}
					//-------------------------------------------------------------------------------------------------------------
					// #AO ---------------------------------------- PRODUCTION ALPHANUMMERIC OUTPUT -------------------------------
					if((MenuEntry + xcnt)->DynMenuEnryType == PD_ALPHANUMERIC_OUTPUT_INDEX)
					{	// Switch to datatype of configured AO
						switch ((MenuEntry + xcnt)->WorkVariableType){
							case VAR_TYPE_STRING:// STRING: Copy information out of configured variable adress to visu variable
								if ((MenuEntry + xcnt)->WorkVariableAdr > 0){
									memcpy(&(VisMenu + (xcnt - Disabled - EntryBefore))->ValueAlph, (USINT*)(MenuEntry + xcnt)->WorkVariableAdr, (MenuEntry + xcnt)->WorkVariableLength);
								}
								(VisMenu + (xcnt - Disabled - EntryBefore))->Status[6] 	&= ~0x2001;		//set visible bit
								(VisMenu + (xcnt - Disabled - EntryBefore))->Status[27] &= ~0x02; 		//reset locking bit
								(VisMenu + (xcnt - Disabled - EntryBefore))->Status[27] |= 0x01; 		//set invisible bit
								break;
							case VAR_TYPE_UINT:// UINT: Copy value index to varaible to show index information out of text group
								if ((MenuEntry + xcnt)->WorkVariableAdr>0){
									*(UDINT*)&uintPointer = (MenuEntry + xcnt)->WorkVariableAdr;			
									 (VisMenu + (xcnt - Disabled - EntryBefore))->ValueAlphIndex = *uintPointer;
								 	 (VisMenu + (xcnt - Disabled - EntryBefore))->Status[27] 	&= ~0x2001;		//set visible bit
								 	 (VisMenu + (xcnt - Disabled - EntryBefore))->Status[6]		&= ~0x02; 		//reset locking bit
									 (VisMenu + (xcnt - Disabled - EntryBefore))->Status[6]		|= 0x01; 		//set invisible bit
								}
								break;
							case VAR_TYPE_USINT:// USINT: Copy value index to varaible to show index information out of text group
								if ((MenuEntry + xcnt)->WorkVariableAdr>0){
									*(UDINT*)&usintPointer = (MenuEntry + xcnt)->WorkVariableAdr;			
									 (VisMenu + (xcnt - Disabled - EntryBefore))->ValueAlphIndex = *usintPointer;
								 	 (VisMenu + (xcnt - Disabled - EntryBefore))->Status[27] 	&= ~0x2001;		//set visible bit
								 	 (VisMenu + (xcnt - Disabled - EntryBefore))->Status[6]		&= ~0x02; 		//reset locking bit
									 (VisMenu + (xcnt - Disabled - EntryBefore))->Status[6]		|= 0x01; 		//set invisible bit
								}
								break;	
							default:// DEFAULT: Reset visu information on varaible for HMI
								memset(&(VisMenu + (xcnt - Disabled - EntryBefore))->ValueAlph,0,sizeof((VisMenu + (xcnt - Disabled - EntryBefore))->ValueAlph));
								break;	
						}
					}
					//-------------------------------------------------------------------------------------------------------------
					// #SD ---------------------------------------- PRODUCTION SENSING DEVICE COMPLETITION ------------------------
					// #SD ---------------------------------------- MAINTANANCE SENSING DEVICE COMPLETITION -----------------------
					if(((MenuEntry + xcnt)->DynMenuEnryType == PD_SENSING_DEVICE_INDEX || ((MenuEntry + xcnt)->DynMenuEnryType == MT_SENSING_DEVICE_INDEX && (EnableButton == 1))) 
						&& (!(VisMenu + (xcnt - Disabled - EntryBefore))->Completion && (VisMenu + (xcnt - Disabled - EntryBefore))->ValueBut == 1))
					{	/* Copy button value */
                        if ((MenuEntry + xcnt)->WorkVariableAdr > 0){
    						switch ((MenuEntry + xcnt)->WorkVariableType){
    							case VAR_TYPE_BOOL:		*(UDINT*)&boolPointer = (MenuEntry + xcnt)->WorkVariableAdr;								
    								*(UDINT*)&boolPointer = (MenuEntry + xcnt)->WorkVariableAdr;
    								*boolPointer = 1;
    								break;
    							case VAR_TYPE_USINT:	*(UDINT*)&usintPointer = (MenuEntry + xcnt)->WorkVariableAdr;
    								*(UDINT*)&usintPointer = (MenuEntry + xcnt)->WorkVariableAdr;
    								*usintPointer = 1;
    								break;	
                                    }				
						}
						/* Set number of last changed entry */
						pMenu->Internal.LastChangedEntry = xcnt + 1;
						/* Reset completion value */
						//(VisMenu + (xcnt - Disabled - EntryBefore))->Completion = 0;
			
						/* Copy index for button text */
						(VisMenu + (xcnt - Disabled - EntryBefore))->ButtonReleasedTextIndex = (MenuEntry + xcnt)->ButtonReleasedTextIndex;
						(VisMenu + (xcnt - Disabled - EntryBefore))->ButtonPressedTextIndex = (MenuEntry + xcnt)->ButtonPressedTextIndex;
			
						/* Color for sensing device - show element*/
						if(EnableEntryShow == 1)
						{
							/* Button is pressed */
							(VisMenu + (xcnt - Disabled - EntryBefore))->Color = COLOR_PRESSED;
						}
							/* Color for sensing device - lock element */
						else if(EnableEntryShow == 2)
						{
							(VisMenu + (xcnt - Disabled - EntryBefore))->Color = COLOR_LOCKED;
						}
					}
						/* Sensing device - completion and button value not set */
					else if(((MenuEntry + xcnt)->DynMenuEnryType == PD_SENSING_DEVICE_INDEX || ((MenuEntry + xcnt)->DynMenuEnryType == MT_SENSING_DEVICE_INDEX && (EnableButton == 1))) 
						&& ((VisMenu + (xcnt - Disabled - EntryBefore))->Completion && (VisMenu + (xcnt - Disabled - EntryBefore))->ValueBut==61))
					{	/* Reset button value */
                        if((MenuEntry + xcnt)->WorkVariableAdr > 0){
							switch ((MenuEntry + xcnt)->WorkVariableType){
								case VAR_TYPE_BOOL:		*(UDINT*)&boolPointer = (MenuEntry + xcnt)->WorkVariableAdr;								
									*(UDINT*)&boolPointer = (MenuEntry + xcnt)->WorkVariableAdr;
									*boolPointer = 0;
									break;
								case VAR_TYPE_USINT:	*(UDINT*)&usintPointer = (MenuEntry + xcnt)->WorkVariableAdr;
									*(UDINT*)&usintPointer = (MenuEntry + xcnt)->WorkVariableAdr;
									*usintPointer = 0;
									break;					
							}
							(VisMenu + (xcnt - Disabled - EntryBefore))->ValueBut=0;
                        }
                        (VisMenu + (xcnt - Disabled - EntryBefore))->Completion = 0;
						/* Copy index for button text */
						(VisMenu + (xcnt - Disabled - EntryBefore))->ButtonReleasedTextIndex = (MenuEntry + xcnt)->ButtonReleasedTextIndex;
						(VisMenu + (xcnt - Disabled - EntryBefore))->ButtonPressedTextIndex = (MenuEntry + xcnt)->ButtonPressedTextIndex;
			
						/* Color for sensing device - show element*/
						if(EnableEntryShow == 1)
						{
							/* Button is released */
							(VisMenu + (xcnt - Disabled - EntryBefore))->Color = COLOR_RELEASED;
						}
							/* Color for sensing device - lock element */
						else if(EnableEntryShow == 2)
						{
							(VisMenu + (xcnt - Disabled - EntryBefore))->Color = COLOR_LOCKED;
						}
					}
					else if((MenuEntry + xcnt)->DynMenuEnryType == PD_SENSING_DEVICE_INDEX ||(MenuEntry + xcnt)->DynMenuEnryType == MT_SENSING_DEVICE_INDEX){
						(VisMenu + (xcnt - Disabled - EntryBefore))->ButtonReleasedTextIndex = (MenuEntry + xcnt)->ButtonReleasedTextIndex;
						(VisMenu + (xcnt - Disabled - EntryBefore))->ButtonPressedTextIndex = (MenuEntry + xcnt)->ButtonPressedTextIndex;
					}
					//-------------------------------------------------------------------------------------------------------------
					// #SW ---------------------------------------- PRODUCTION SWITCH COMPLETITION --------------------------------
					// #SW ---------------------------------------- MAINTENANCE SWITCH COMPLETITION -------------------------------
					if(((MenuEntry + xcnt)->DynMenuEnryType == PD_SWITCH_INDEX  || (MenuEntry + xcnt)->DynMenuEnryType == MT_SWITCH_INDEX ) 
					&& (((VisMenu + (xcnt - Disabled - EntryBefore))->Completion)|| ((VisMenu + (xcnt - Disabled - EntryBefore))->ValueBut != (VisMenu + (xcnt - Disabled - EntryBefore))->OldValueBut))){	
						/* Copy button value */
						if ((MenuEntry + xcnt)->WorkVariableAdr){
							// Save old value to run in terminal mode also until value is set
							(VisMenu + (xcnt - Disabled - EntryBefore))->OldValueBut = (VisMenu + (xcnt - Disabled - EntryBefore))->ValueBut;						
							*(UDINT*)&usintPointer = (MenuEntry + xcnt)->WorkVariableAdr;
							*usintPointer = (VisMenu + (xcnt - Disabled - EntryBefore))->ValueBut;
							/* Set number of last changed entry */
							pMenu->Internal.LastChangedEntry = xcnt + 1;
							/* Reset completion value */
							(VisMenu + (xcnt - Disabled - EntryBefore))->Completion = 0;
				
							/* Copy index for button text */
							(VisMenu + (xcnt - Disabled - EntryBefore))->ButtonReleasedTextIndex = (MenuEntry + xcnt)->ButtonReleasedTextIndex;
							(VisMenu + (xcnt - Disabled - EntryBefore))->ButtonPressedTextIndex = (MenuEntry + xcnt)->ButtonPressedTextIndex;
				
							/* Color for switch device - show element*/
							if(EnableEntryShow == 1)
							{	/* If button value is 1 - button is pressed */
								if((VisMenu + (xcnt - Disabled - EntryBefore))->ValueBut == 1)
								{
									(VisMenu + (xcnt - Disabled - EntryBefore))->Color = COLOR_PRESSED;
								}
									/* If button value is 0 - button is released */
								else
								{
									(VisMenu + (xcnt - Disabled - EntryBefore))->Color = COLOR_RELEASED;
								}
							}
								/* Color for switch device - lock element */
							else if(EnableEntryShow == 2)
							{
								(VisMenu + (xcnt - Disabled - EntryBefore))->Color = COLOR_LOCKED;
							}
						}
					}
						/* Switch - completion value not set */
					else if(((MenuEntry + xcnt)->DynMenuEnryType == PD_SWITCH_INDEX  || (MenuEntry + xcnt)->DynMenuEnryType == MT_SWITCH_INDEX ) 
					&& !(VisMenu + (xcnt - Disabled - EntryBefore))->Completion){	
						/* Copy button value */
						if ((MenuEntry + xcnt)->WorkVariableAdr){
							*(UDINT*)&usintPointer = (MenuEntry + xcnt)->WorkVariableAdr;
							(VisMenu + (xcnt - Disabled - EntryBefore))->ValueBut = *usintPointer;
	
							/* Copy index for button text */
							(VisMenu + (xcnt - Disabled - EntryBefore))->ButtonReleasedTextIndex = (MenuEntry + xcnt)->ButtonReleasedTextIndex;
							(VisMenu + (xcnt - Disabled - EntryBefore))->ButtonPressedTextIndex = (MenuEntry + xcnt)->ButtonPressedTextIndex;
				
							/* Color for switch device - show element*/
							if(EnableEntryShow == 1)
							{	/* If button value is 1 - button is pressed */
								if((VisMenu + (xcnt - Disabled - EntryBefore))->ValueBut == 1)
								{
									(VisMenu + (xcnt - Disabled - EntryBefore))->Color = COLOR_PRESSED;
								}
									/* If button value is 0 - button is released */
								else
								{
									(VisMenu + (xcnt - Disabled - EntryBefore))->Color = COLOR_RELEASED;
								}
							}
								/* Color for switch device - lock element */
							else if(EnableEntryShow == 2)
							{
								(VisMenu + (xcnt - Disabled - EntryBefore))->Color = COLOR_LOCKED;
							}
						}
					}
					//-------------------------------------------------------------------------------------------------------------
					// #NI ---------------------------------------- PRODUCTION NUMMERIC INPUT  COMPLETITION -----------------------
					// #NI ---------------------------------------- MAINTANANCE NUMMERIC INPUT COMPLETITION -----------------------
					// #UD ---------------------------------------- PRODUCTION UP DOWN COMPLETITION -------------------------------
					if(((MenuEntry + xcnt)->DynMenuEnryType == PD_UPDOWN_INDEX 
						||(MenuEntry + xcnt)->DynMenuEnryType == PD_NUMERIC_INPUT_INDEX 
						||((MenuEntry + xcnt)->DynMenuEnryType == MT_NUMERIC_INPUT_INDEX))
						&& ((VisMenu + (xcnt - Disabled - EntryBefore))->Completion != 0))
					{
							UpDownValStep=((MenuEntry + xcnt)->UpDownStepWidth);
								
							if ((VisMenu + (xcnt - Disabled - EntryBefore))->Completion ==1 && ((VisMenu + (xcnt - Disabled - EntryBefore))->ValueNum) > (VisMenu + (xcnt - Disabled - EntryBefore))->MinValueNum)
							{
								(VisMenu + (xcnt - Disabled - EntryBefore))->ValueNum =(VisMenu + (xcnt - Disabled - EntryBefore))->ValueNum - UpDownValStep;
								if (((VisMenu + (xcnt - Disabled - EntryBefore))->ValueNum) < (VisMenu + (xcnt - Disabled - EntryBefore))->MinValueNum)
								{
									(VisMenu + (xcnt - Disabled - EntryBefore))->ValueNum = (VisMenu + (xcnt - Disabled - EntryBefore))->MinValueNum;
								}
							}
							if ((VisMenu + (xcnt - Disabled - EntryBefore))->Completion ==2 && (VisMenu + (xcnt - Disabled - EntryBefore))->ValueNum < (VisMenu + (xcnt - Disabled - EntryBefore))->MaxValueNum)
							{
								(VisMenu + (xcnt - Disabled - EntryBefore))->ValueNum =(VisMenu + (xcnt - Disabled - EntryBefore))->ValueNum + UpDownValStep;
								if ((VisMenu + (xcnt - Disabled - EntryBefore))->ValueNum > (VisMenu + (xcnt - Disabled - EntryBefore))->MaxValueNum)
								{
									(VisMenu + (xcnt - Disabled - EntryBefore))->ValueNum = (VisMenu + (xcnt - Disabled - EntryBefore))->MaxValueNum;
								}
							}	
								
							if((MenuEntry + xcnt)->WorkVariableAdr){
								UpDownCalcValNum = (VisMenu + (xcnt - Disabled - EntryBefore))->ValueNum;
								switch ((MenuEntry + xcnt)->WorkVariableType){
									case VAR_TYPE_BOOL:	*(UDINT*)&boolPointer = (MenuEntry + xcnt)->WorkVariableAdr;
										*boolPointer = UpDownCalcValNum;
										break;
									case VAR_TYPE_DINT:	*(UDINT*)&dintPointer = (MenuEntry + xcnt)->WorkVariableAdr;
										*dintPointer = UpDownCalcValNum;
										break;
									case VAR_TYPE_UDINT:*(UDINT*)&udintPointer = (MenuEntry + xcnt)->WorkVariableAdr;
										*udintPointer = UpDownCalcValNum;
										break;				
									case VAR_TYPE_INT:	*(UDINT*)&intPointer = (MenuEntry + xcnt)->WorkVariableAdr;
										*intPointer = UpDownCalcValNum;	
										break;					
									case VAR_TYPE_UINT:	*(UDINT*)&uintPointer = (MenuEntry + xcnt)->WorkVariableAdr;
										*uintPointer = UpDownCalcValNum;
										break;				
									case VAR_TYPE_SINT:	*(UDINT*)&sintPointer = (MenuEntry + xcnt)->WorkVariableAdr;
										*sintPointer = UpDownCalcValNum;
										break;				
									case VAR_TYPE_USINT:*(UDINT*)&usintPointer = (MenuEntry + xcnt)->WorkVariableAdr;
										*usintPointer = UpDownCalcValNum;
										break;					
									case VAR_TYPE_REAL:	*(UDINT*)&realPointer = (MenuEntry + xcnt)->WorkVariableAdr;
										*realPointer = UpDownCalcValNum;
										break;
									case VAR_TYPE_TIME:	*(UDINT*)&TimePointer = (MenuEntry + xcnt)->WorkVariableAdr;
										*TimePointer = UpDownCalcValNum;
										break;	
								}
							}
								
							if( (MenuEntry + xcnt)->UpDownIndex==0){
								(VisMenu + (xcnt - Disabled - EntryBefore))->Status[17]	&= ~0x200F;
							}
							else{
								(VisMenu + (xcnt - Disabled - EntryBefore))->Status[17]	|= 0x0001;
							}
							/* Set number of last changed entry */
							pMenu->Internal.LastChangedEntry = xcnt + 1;
							/* Reset completion value */
							(VisMenu + (xcnt - Disabled - EntryBefore))->Completion = 0;
			
							(VisMenu + (xcnt - Disabled - EntryBefore))->UpDownIndex = (MenuEntry + xcnt)->UpDownIndex;
			
							/* Color for sensing device - show element*/
							if(EnableEntryShow == 1)
							{
								/* Button is pressed */
								(VisMenu + (xcnt - Disabled - EntryBefore))->Color = COLOR_PRESSED;
							}
								/* Color for sensing device - lock element */
							else if(EnableEntryShow == 2)
							{
								(VisMenu + (xcnt - Disabled - EntryBefore))->Color = COLOR_LOCKED;
							}
					}
						/* Sensing device - completion and button value not set */
					else if(((MenuEntry + xcnt)->DynMenuEnryType == PD_UPDOWN_INDEX 
						||(MenuEntry + xcnt)->DynMenuEnryType == PD_NUMERIC_INPUT_INDEX 
						||((MenuEntry + xcnt)->DynMenuEnryType == MT_NUMERIC_INPUT_INDEX))
						&& !((VisMenu + (xcnt - Disabled - EntryBefore))->Completion ))
					{	
						StopUpDate = 1;
						if ((MenuEntry + xcnt)->DynMenuEnryType == PD_NUMERIC_INPUT_INDEX ){
							if ((((VisMenu + (xcnt - Disabled - EntryBefore))->Status[0] & 0x1000) == 0x0000)
							||(((VisMenu + (xcnt - Disabled - EntryBefore))->Status[0] & 0x8000) == 0x0000)){
								StopUpDate=0;
							}							
						}
						else if ((MenuEntry + xcnt)->DynMenuEnryType == MT_NUMERIC_INPUT_INDEX){
							if ((((VisMenu + (xcnt - Disabled - EntryBefore))->Status[18] & 0x1000) == 0x0000)
							||(((VisMenu + (xcnt - Disabled - EntryBefore))->Status[18] & 0x8000) == 0x0000)){
								StopUpDate=0;
							}
						}
						else if ((MenuEntry + xcnt)->DynMenuEnryType == PD_UPDOWN_INDEX){
							if ((((VisMenu + (xcnt - Disabled - EntryBefore))->Status[17] & 0x1000) == 0x0000)
							||(((VisMenu + (xcnt - Disabled - EntryBefore))->Status[17] & 0x8000) == 0x0000)){
								StopUpDate=0;
							}
						}
						
						if( (MenuEntry + xcnt)->UpDownIndex==0){
							(VisMenu + (xcnt - Disabled - EntryBefore))->Status[17]	&= ~0x200F;
						}
						else{
							(VisMenu + (xcnt - Disabled - EntryBefore))->Status[17]	|= 0x0001;
						}						
						
						
						if(!StopUpDate)
						{
							/* Reset button value */
							if((MenuEntry + xcnt)->LimitsMinAdr){
								switch ((MenuEntry + xcnt)->LimitsMinType){
									case VAR_TYPE_BOOL:		*(UDINT*)&boolPointer = (MenuEntry + xcnt)->LimitsMinAdr;
										(MenuEntry + xcnt)->LimitsMinValue = *boolPointer;
										break;
									case VAR_TYPE_DINT:		*(UDINT*)&dintPointer = (MenuEntry + xcnt)->LimitsMinAdr;
										(MenuEntry + xcnt)->LimitsMinValue = *dintPointer;
										break;
									case VAR_TYPE_UDINT:	*(UDINT*)&udintPointer = (MenuEntry + xcnt)->LimitsMinAdr;
										(MenuEntry + xcnt)->LimitsMinValue = *udintPointer;
										break;				
									case VAR_TYPE_INT:		*(UDINT*)&intPointer = (MenuEntry + xcnt)->LimitsMinAdr;
										(MenuEntry + xcnt)->LimitsMinValue = *intPointer;
										break;					
									case VAR_TYPE_UINT:		*(UDINT*)&uintPointer = (MenuEntry + xcnt)->LimitsMinAdr;
										(MenuEntry + xcnt)->LimitsMinValue = *uintPointer;
										break;				
									case VAR_TYPE_SINT:		*(UDINT*)&sintPointer = (MenuEntry + xcnt)->LimitsMinAdr;
										(MenuEntry + xcnt)->LimitsMinValue = *sintPointer;
										break;				
									case VAR_TYPE_USINT:	*(UDINT*)&usintPointer = (MenuEntry + xcnt)->LimitsMinAdr;
										(MenuEntry + xcnt)->LimitsMinValue = *usintPointer;
										break;					
									case VAR_TYPE_REAL:		*(UDINT*)&realPointer = (MenuEntry + xcnt)->LimitsMinAdr;
										(MenuEntry + xcnt)->LimitsMinValue = *realPointer;
										break;
									case VAR_TYPE_TIME:		*(UDINT*)&TimePointer = (MenuEntry + xcnt)->LimitsMinAdr;
										(MenuEntry + xcnt)->LimitsMinValue = *TimePointer;
										break;	
								}
							}
							if ((MenuEntry + xcnt)->CalcFunctionMinMax==1 && (MenuEntry + xcnt)->CalcFunctionLimitsMinAdr ){
								switch ((MenuEntry + xcnt)->CalcFunctionLimitsMinType){
									case VAR_TYPE_BOOL:		*(UDINT*)&boolPointer = (MenuEntry + xcnt)->CalcFunctionLimitsMinAdr;
										(VisMenu + (xcnt - Disabled - EntryBefore))->CalcFunctionLimitsMinValue = *boolPointer;
										break;
									case VAR_TYPE_DINT:		*(UDINT*)&dintPointer = (MenuEntry + xcnt)->CalcFunctionLimitsMinAdr;
										(VisMenu + (xcnt - Disabled - EntryBefore))->CalcFunctionLimitsMinValue = *dintPointer;
										break;
									case VAR_TYPE_UDINT:	*(UDINT*)&udintPointer = (MenuEntry + xcnt)->CalcFunctionLimitsMinAdr;
										(VisMenu + (xcnt - Disabled - EntryBefore))->CalcFunctionLimitsMinValue = *udintPointer;
										break;				
									case VAR_TYPE_INT:		*(UDINT*)&intPointer = (MenuEntry + xcnt)->CalcFunctionLimitsMinAdr;
										(VisMenu + (xcnt - Disabled - EntryBefore))->CalcFunctionLimitsMinValue = *intPointer;
										break;					
									case VAR_TYPE_UINT:		*(UDINT*)&uintPointer = (MenuEntry + xcnt)->CalcFunctionLimitsMinAdr;
										(VisMenu + (xcnt - Disabled - EntryBefore))->CalcFunctionLimitsMinValue = *uintPointer;
										break;				
									case VAR_TYPE_SINT:		*(UDINT*)&sintPointer = (MenuEntry + xcnt)->CalcFunctionLimitsMinAdr;
										(VisMenu + (xcnt - Disabled - EntryBefore))->CalcFunctionLimitsMinValue = *sintPointer;
										break;				
									case VAR_TYPE_USINT:	*(UDINT*)&usintPointer = (MenuEntry + xcnt)->CalcFunctionLimitsMinAdr;
										(VisMenu + (xcnt - Disabled - EntryBefore))->CalcFunctionLimitsMinValue = *usintPointer;
										break;					
									case VAR_TYPE_REAL:		*(UDINT*)&realPointer = (MenuEntry + xcnt)->CalcFunctionLimitsMinAdr;
										(VisMenu + (xcnt - Disabled - EntryBefore))->CalcFunctionLimitsMinValue = *realPointer;
										break;	
									case VAR_TYPE_TIME:		*(UDINT*)&TimePointer = (MenuEntry + xcnt)->CalcFunctionLimitsMinAdr;
										(VisMenu + (xcnt - Disabled - EntryBefore))->CalcFunctionLimitsMinValue = *TimePointer;
										break;		
								}
							}
								
							if((MenuEntry + xcnt)->LimitsMaxAdr){
								switch ((MenuEntry + xcnt)->LimitsMaxType){
									case VAR_TYPE_BOOL:		*(UDINT*)&boolPointer = (MenuEntry + xcnt)->LimitsMaxAdr;
										(MenuEntry + xcnt)->LimitsMaxValue = *boolPointer;
										break;									
									case VAR_TYPE_DINT:		*(UDINT*)&dintPointer = (MenuEntry + xcnt)->LimitsMaxAdr;
										(MenuEntry + xcnt)->LimitsMaxValue = *dintPointer;
										break;
									case VAR_TYPE_UDINT:	*(UDINT*)&udintPointer = (MenuEntry + xcnt)->LimitsMaxAdr;
										(MenuEntry + xcnt)->LimitsMaxValue = *udintPointer;
										break;				
									case VAR_TYPE_INT:		*(UDINT*)&intPointer = (MenuEntry + xcnt)->LimitsMaxAdr;
										(MenuEntry + xcnt)->LimitsMaxValue = *intPointer;
										break;					
									case VAR_TYPE_UINT:		*(UDINT*)&uintPointer = (MenuEntry + xcnt)->LimitsMaxAdr;
										(MenuEntry + xcnt)->LimitsMaxValue = *uintPointer;
										break;				
									case VAR_TYPE_SINT:		*(UDINT*)&sintPointer = (MenuEntry + xcnt)->LimitsMaxAdr;
										(MenuEntry + xcnt)->LimitsMaxValue = *sintPointer;
										break;				
									case VAR_TYPE_USINT:	*(UDINT*)&usintPointer = (MenuEntry + xcnt)->LimitsMaxAdr;
										(MenuEntry + xcnt)->LimitsMaxValue = *usintPointer;
										break;					
									case VAR_TYPE_REAL:		*(UDINT*)&realPointer = (MenuEntry + xcnt)->LimitsMaxAdr;
										(MenuEntry + xcnt)->LimitsMaxValue = *realPointer;
										break;	
									case VAR_TYPE_TIME:		*(UDINT*)&TimePointer = (MenuEntry + xcnt)->LimitsMaxAdr;
										(MenuEntry + xcnt)->LimitsMaxValue = *TimePointer;
										break;		
								}
							}
							if ((MenuEntry + xcnt)->CalcFunctionMinMax==1 && (MenuEntry + xcnt)->CalcFunctionLimitsMaxAdr ){
								switch ((MenuEntry + xcnt)->CalcFunctionLimitsMaxType){
									case VAR_TYPE_BOOL:		*(UDINT*)&boolPointer = (MenuEntry + xcnt)->CalcFunctionLimitsMaxAdr;
										(VisMenu + (xcnt - Disabled - EntryBefore))->CalcFunctionLimitsMaxValue = *boolPointer;
										break;
									case VAR_TYPE_DINT:		*(UDINT*)&dintPointer = (MenuEntry + xcnt)->CalcFunctionLimitsMaxAdr;
										(VisMenu + (xcnt - Disabled - EntryBefore))->CalcFunctionLimitsMaxValue = *dintPointer;
										break;
									case VAR_TYPE_UDINT:	*(UDINT*)&udintPointer = (MenuEntry + xcnt)->CalcFunctionLimitsMaxAdr;
										(VisMenu + (xcnt - Disabled - EntryBefore))->CalcFunctionLimitsMaxValue = *udintPointer;
										break;				
									case VAR_TYPE_INT:		*(UDINT*)&intPointer = (MenuEntry + xcnt)->CalcFunctionLimitsMaxAdr;
										(VisMenu + (xcnt - Disabled - EntryBefore))->CalcFunctionLimitsMaxValue = *intPointer;
										break;					
									case VAR_TYPE_UINT:		*(UDINT*)&uintPointer = (MenuEntry + xcnt)->CalcFunctionLimitsMaxAdr;
										(VisMenu + (xcnt - Disabled - EntryBefore))->CalcFunctionLimitsMaxValue = *uintPointer;
										break;				
									case VAR_TYPE_SINT:		*(UDINT*)&sintPointer = (MenuEntry + xcnt)->CalcFunctionLimitsMaxAdr;
										(VisMenu + (xcnt - Disabled - EntryBefore))->CalcFunctionLimitsMaxValue = *sintPointer;
										break;				
									case VAR_TYPE_USINT:	*(UDINT*)&usintPointer = (MenuEntry + xcnt)->CalcFunctionLimitsMaxAdr;
										(VisMenu + (xcnt - Disabled - EntryBefore))->CalcFunctionLimitsMaxValue = *usintPointer;
										break;					
									case VAR_TYPE_REAL:		*(UDINT*)&realPointer = (MenuEntry + xcnt)->CalcFunctionLimitsMaxAdr;
										(VisMenu + (xcnt - Disabled - EntryBefore))->CalcFunctionLimitsMaxValue = *realPointer;
										break;	
									case VAR_TYPE_TIME:		*(UDINT*)&TimePointer = (MenuEntry + xcnt)->CalcFunctionLimitsMaxAdr;
										(VisMenu + (xcnt - Disabled - EntryBefore))->CalcFunctionLimitsMaxValue = *TimePointer;
										break;		
								}
							}
							if ((MenuEntry + xcnt)->CalcFunctionMinMax==1 && (MenuEntry + xcnt)->CalcFunctionAdrVar ){
								switch ((MenuEntry + xcnt)->CalcFunctionTypeVar){
									case VAR_TYPE_BOOL:		*(UDINT*)&boolPointer = (MenuEntry + xcnt)->CalcFunctionAdrVar;
										UpDownRefValue = *boolPointer;
										break;
									case VAR_TYPE_DINT:		*(UDINT*)&dintPointer = (MenuEntry + xcnt)->CalcFunctionAdrVar;
										UpDownRefValue = *dintPointer;
										break;
									case VAR_TYPE_UDINT:	*(UDINT*)&udintPointer = (MenuEntry + xcnt)->CalcFunctionAdrVar;
										UpDownRefValue = *udintPointer;
										break;				
									case VAR_TYPE_INT:		*(UDINT*)&intPointer = (MenuEntry + xcnt)->CalcFunctionAdrVar;
										UpDownRefValue = *intPointer;
										break;					
									case VAR_TYPE_UINT:		*(UDINT*)&uintPointer = (MenuEntry + xcnt)->CalcFunctionAdrVar;
										UpDownRefValue = *uintPointer;
										break;				
									case VAR_TYPE_SINT:		*(UDINT*)&sintPointer = (MenuEntry + xcnt)->CalcFunctionAdrVar;
										UpDownRefValue = *sintPointer;
										break;				
									case VAR_TYPE_USINT:	*(UDINT*)&usintPointer = (MenuEntry + xcnt)->CalcFunctionAdrVar;
										UpDownRefValue = *usintPointer;
										break;					
									case VAR_TYPE_REAL:		*(UDINT*)&realPointer = (MenuEntry + xcnt)->CalcFunctionAdrVar;
										UpDownRefValue = *realPointer;
										break;	
									case VAR_TYPE_TIME:		*(UDINT*)&TimePointer = (MenuEntry + xcnt)->CalcFunctionAdrVar;
										UpDownRefValue = *TimePointer;
										break;		
								}
							}
								
							if ((MenuEntry + xcnt)->CalcFunctionMinMax==1){		
								(VisMenu + (xcnt - Disabled - EntryBefore))->MaxValueNum =UpDownRefValue +(VisMenu + (xcnt - Disabled - EntryBefore))->CalcFunctionLimitsMaxValue;
								(VisMenu + (xcnt - Disabled - EntryBefore))->MinValueNum =UpDownRefValue + (VisMenu + (xcnt - Disabled - EntryBefore))->CalcFunctionLimitsMinValue;		
							}
								
							/* Calculation Function Min-variable */
							if((MenuEntry + xcnt)->CalcFunctionMinMax==1  && (MenuEntry + xcnt)->CalcFunctionLimitsMinAdr==0)
							{
								(VisMenu + (xcnt - Disabled - EntryBefore))->CalcFunctionLimitsMinValue = (MenuEntry + xcnt)->CalcFunctionLimitsMinValue;
							}
							/* Calculation Function Max-variable */
							if((MenuEntry + xcnt)->CalcFunctionMinMax==1  && (MenuEntry + xcnt)->CalcFunctionLimitsMaxAdr==0)
							{
								(VisMenu + (xcnt - Disabled - EntryBefore))->CalcFunctionLimitsMaxValue = (MenuEntry + xcnt)->CalcFunctionLimitsMaxValue;
							}
							UpDownValStep=((MenuEntry + xcnt)->UpDownStepWidth);
							if ((MenuEntry + xcnt)->CalcFunctionMinMax==1){
								UpDownValMin=(VisMenu + (xcnt - Disabled - EntryBefore))->MinValueNum ;
								UpDownValMax=(VisMenu + (xcnt - Disabled - EntryBefore))->MaxValueNum ;
								if (UpDownValMin<(MenuEntry + xcnt)->LimitsMinValue)
								{
									UpDownValMin =(MenuEntry + xcnt)->LimitsMinValue ;
								}
								if (UpDownValMax >(MenuEntry + xcnt)->LimitsMaxValue)
								{
									UpDownValMax = (MenuEntry + xcnt)->LimitsMaxValue ;
								}
							}
							else{
								UpDownValMin=(MenuEntry + xcnt)->LimitsMinValue ;
								UpDownValMax=(MenuEntry + xcnt)->LimitsMaxValue ;
							}
								
							if(UpDownValMin>UpDownValMax){
								UpDownValMin=UpDownValMax;
							}
								
								
							(VisMenu + (xcnt - Disabled - EntryBefore))->MinValueNum=UpDownValMin;
							(VisMenu + (xcnt - Disabled - EntryBefore))->MaxValueNum=UpDownValMax;
								
								
							
							if((MenuEntry + xcnt)->WorkVariableAdr){
								switch ((MenuEntry + xcnt)->WorkVariableType){
									case VAR_TYPE_BOOL:	*(UDINT*)&boolPointer = (MenuEntry + xcnt)->WorkVariableAdr;			
										(VisMenu + (xcnt - Disabled - EntryBefore))->ValueNum = *boolPointer;
										break;
									case VAR_TYPE_DINT:	*(UDINT*)&dintPointer = (MenuEntry + xcnt)->WorkVariableAdr;			
										(VisMenu + (xcnt - Disabled - EntryBefore))->ValueNum = *dintPointer;
										break;
									case VAR_TYPE_UDINT:*(UDINT*)&udintPointer = (MenuEntry + xcnt)->WorkVariableAdr;			
										(VisMenu + (xcnt - Disabled - EntryBefore))->ValueNum = *udintPointer;
										break;				
									case VAR_TYPE_INT:*(UDINT*)&intPointer = (MenuEntry + xcnt)->WorkVariableAdr;			
										(VisMenu + (xcnt - Disabled - EntryBefore))->ValueNum = *intPointer;	
										break;					
									case VAR_TYPE_UINT:	*(UDINT*)&uintPointer = (MenuEntry + xcnt)->WorkVariableAdr;			
										(VisMenu + (xcnt - Disabled - EntryBefore))->ValueNum = *uintPointer;
										break;				
									case VAR_TYPE_SINT:	*(UDINT*)&sintPointer = (MenuEntry + xcnt)->WorkVariableAdr;			
										(VisMenu + (xcnt - Disabled - EntryBefore))->ValueNum = *sintPointer;
										break;				
									case VAR_TYPE_USINT:*(UDINT*)&usintPointer = (MenuEntry + xcnt)->WorkVariableAdr;			
										(VisMenu + (xcnt - Disabled - EntryBefore))->ValueNum = *usintPointer;
										break;					
									case VAR_TYPE_REAL:	*(UDINT*)&realPointer = (MenuEntry + xcnt)->WorkVariableAdr;			
										(VisMenu + (xcnt - Disabled - EntryBefore))->ValueNum = *realPointer;
										break;
									case VAR_TYPE_TIME:	*(UDINT*)&TimePointer = (MenuEntry + xcnt)->WorkVariableAdr;			
										(VisMenu + (xcnt - Disabled - EntryBefore))->ValueNum = *TimePointer;
										break;	
								}
			
							}						
							(VisMenu + (xcnt - Disabled - EntryBefore))->ValueNum =(VisMenu + (xcnt - Disabled - EntryBefore))->ValueNum ;
							(VisMenu + (xcnt - Disabled - EntryBefore))->UpDownIndex = (MenuEntry + xcnt)->UpDownIndex;					
							
							if ((VisMenu + (xcnt - Disabled - EntryBefore))->ValueNum <=(VisMenu + (xcnt - Disabled - EntryBefore))->MinValueNum)
							{
								(VisMenu + (xcnt - Disabled - EntryBefore))->Bitmap = (MenuEntry + xcnt)->FunctionHeadlineBitmap+200;
								(VisMenu + (xcnt - Disabled - EntryBefore))->UpDownLockingLeft =1;
							}
							else
							{
								(VisMenu + (xcnt - Disabled - EntryBefore))->Bitmap = (MenuEntry + xcnt)->FunctionHeadlineBitmap;
								(VisMenu + (xcnt - Disabled - EntryBefore))->UpDownLockingLeft =0;				
							}
								
							if ((VisMenu + (xcnt - Disabled - EntryBefore))->ValueNum >=(VisMenu + (xcnt - Disabled - EntryBefore))->MaxValueNum)
							{
								(VisMenu + (xcnt - Disabled - EntryBefore))->BitmapUpDownRight = (MenuEntry + xcnt)->FunctionHeadlineBitmap+300;
								(VisMenu + (xcnt - Disabled - EntryBefore))->UpDownLockingRight =1;
							}
							else
							{
								(VisMenu + (xcnt - Disabled - EntryBefore))->BitmapUpDownRight = (MenuEntry + xcnt)->FunctionHeadlineBitmap+100;
								(VisMenu + (xcnt - Disabled - EntryBefore))->UpDownLockingRight =0;				
							}
								

								
							(VisMenu + (xcnt - Disabled - EntryBefore))->Status[24]	= (VisMenu + (xcnt - Disabled - EntryBefore))->Status[17];
								
							/* Color for sensing device - show element*/
							if(EnableEntryShow == 1)
							{
								/* Button is released */
								(VisMenu + (xcnt - Disabled - EntryBefore))->Color = COLOR_RELEASED;
								(VisMenu + (xcnt - Disabled - EntryBefore))->Status[25]	|= 0x0001;

							}
								/* Color for sensing device - lock element */
							else if(EnableEntryShow == 2)
							{
								(VisMenu + (xcnt - Disabled - EntryBefore))->Color = COLOR_LOCKED;
								(VisMenu + (xcnt - Disabled - EntryBefore))->Status[25]	= (VisMenu + (xcnt - Disabled - EntryBefore))->Status[17];
								(VisMenu + (xcnt - Disabled - EntryBefore))->Status[17]	|= 0x0001;
								(VisMenu + (xcnt - Disabled - EntryBefore))->Bitmap = (MenuEntry + xcnt)->FunctionHeadlineBitmap+200;
								(VisMenu + (xcnt - Disabled - EntryBefore))->UpDownLockingLeft =1;
								(VisMenu + (xcnt - Disabled - EntryBefore))->BitmapUpDownRight = (MenuEntry + xcnt)->FunctionHeadlineBitmap+300;
								(VisMenu + (xcnt - Disabled - EntryBefore))->UpDownLockingRight =1;
							}
						}
					}				
					//-------------------------------------------------------------------------------------------------------------
					// #FH ---------------------------------------- PRODUCTION FUNCTION HEADLINE COMPLETITION ---------------------
					// #FH ---------------------------------------- MAINTANANCE FUNCTION HEADLINE COMPLETITION --------------------
					if(((((MenuEntry + xcnt)->DynMenuEnryType == PD_FUNCTIONHEADLINE_INDEX) || (MenuEntry + xcnt)->DynMenuEnryType == MT_FUNCTIONHEADLINE_INDEX) 
						&&( (VisMenu + (xcnt - Disabled - EntryBefore))->Completion || ((VisMenu + (xcnt - Disabled - EntryBefore))->ValueBut != (VisMenu + (xcnt - Disabled - EntryBefore))->OldValueBut)))
						&& (MenuEntry + xcnt)->WorkVariableAdr != 0)
					{	
						// Save old value to run in terminal mode also until value is set
						(VisMenu + (xcnt - Disabled - EntryBefore))->OldValueBut = (VisMenu + (xcnt - Disabled - EntryBefore))->ValueBut;	
						/* Copy button value */
						*(UDINT*)&usintPointer = (MenuEntry + xcnt)->WorkVariableAdr;
						*usintPointer = (VisMenu + (xcnt - Disabled - EntryBefore))->ValueBut;
						(VisMenu + (xcnt - Disabled - EntryBefore))->Completion = 0;
						/* Set number of last changed entry */
						pMenu->Internal.LastChangedEntry = xcnt + 1;
					}
						//-------------------------------------------------------------------------------------------------------------
						//---------------------------------------- PRODUCTION FUNCTION HEADLINE INPUT NO COMPLETITION -----------------
						//---------------------------------------- MAINTANANCE FUNCTION HEADLINE INPUT NO COMPLETITION ----------------
					else if(((MenuEntry + xcnt)->DynMenuEnryType == PD_FUNCTIONHEADLINE_INDEX || (MenuEntry + xcnt)->DynMenuEnryType == MT_FUNCTIONHEADLINE_INDEX) 
						&& !(VisMenu + (xcnt - Disabled - EntryBefore))->Completion && (MenuEntry + xcnt)->WorkVariableAdr != 0)
					{	/* Copy button value */
						*(UDINT*)&usintPointer = (MenuEntry + xcnt)->WorkVariableAdr;
						(VisMenu + (xcnt - Disabled - EntryBefore))->ValueBut = *usintPointer;	
					}
					//-------------------------------------------------------------------------------------------------------------
					// #FH ---------------------------------------- PRODUCTION FUNCTION HEADLINE COMPLETITION ---------------------
					// #FH ---------------------------------------- MAINTANANCE FUNCTION HEADLINE COMPLETITION --------------------
					if(((MenuEntry + xcnt)->DynMenuEnryType == PD_FUNCTIONHEADLINE_INDEX || (MenuEntry + xcnt)->DynMenuEnryType == MT_FUNCTIONHEADLINE_INDEX) 
						&& (VisMenu + (xcnt - Disabled - EntryBefore))->CompletionFunctionHeadLine && (MenuEntry + xcnt)->FunctionHeadlineStatusAdr != 0)
					{	/* Copy button value */
						(VisMenu + (xcnt - Disabled - EntryBefore))->CompletionFunctionHeadLine = 0;
						*(UDINT*)&usintPointer2 = (MenuEntry + xcnt)->FunctionHeadlineStatusAdr;
						//*usintPointer2 = (VisMenu + (xcnt - Disabled - EntryBefore))->ValueStatus;
						if (*usintPointer2 == 1)
						{
							*usintPointer2 = 0;
						}
						else if (*usintPointer2 == 0)
						{
							*usintPointer2 = 1;
						}
						(VisMenu + (xcnt - Disabled - EntryBefore))->ValueStatus=*usintPointer2;
						if ((MenuEntry + xcnt)->DynMenuEnryType == PD_FUNCTIONHEADLINE_INDEX ){
							/* Set number of last changed entry */
							pMenu->Internal.LastChangedEntry = xcnt + 1;
							if((ActVisEntryNumber == (TmpEntriesPerPage - 2) && ((VisMenu + (xcnt - Disabled - EntryBefore))->ValueStatus>0))||(ActVisEntryNumber == (TmpEntriesPerPage - 1) )){
								if (&usintPointer2 > 0){
									CmdPageUp=1;
								}
							}	
							if(Page->ShiftOffset[Page->CurrentPage]>=2&&(VisMenu + (xcnt - Disabled - EntryBefore))->ValueStatus==0){
								if (&usintPointer2 > 0){
									CmdPageDown=1;
								}
							}
						}
						if ((MenuEntry + xcnt)->DynMenuEnryType == MT_FUNCTIONHEADLINE_INDEX ){
							if ((VisMenu + (xcnt - Disabled - EntryBefore))->ValueStatus == 1){
								pMenu->Internal.EntryActive=xcnt;
								pMenu->Internal.EntryOpendFromPage= Page->CurrentPage;
								Page->CurrentPage = 0;
								memcpy(&TempPage.ShiftOffset,&Page->ShiftOffset,sizeof(TempPage.ShiftOffset));
							}
							else{
								pMenu->Internal.EntryActive = 0;
							}
						}
					}
						/* FUNCTIONHEADLINE - completion value not set for Display Status Elements*/
					else if(((MenuEntry + xcnt)->DynMenuEnryType == PD_FUNCTIONHEADLINE_INDEX || (MenuEntry + xcnt)->DynMenuEnryType == MT_FUNCTIONHEADLINE_INDEX ) && !(VisMenu + (xcnt - Disabled - EntryBefore))->CompletionFunctionHeadLine && (MenuEntry + xcnt)->FunctionHeadlineStatusAdr != 0)
					{	/* Copy button value */
						*(UDINT*)&usintPointer2 = (MenuEntry + xcnt)->FunctionHeadlineStatusAdr;
						(VisMenu + (xcnt - Disabled - EntryBefore))->ValueStatus = *usintPointer2;	
			
					}
					//-------------------------------------------------------------------------------------------------------------
					// #CB ---------------------------------------- PRODUCTION CHECKBOX COMPLETITION ------------------------------
					if((MenuEntry + xcnt)->DynMenuEnryType == PD_CHECKBOX_INDEX 
						&&((VisMenu + (xcnt - Disabled - EntryBefore))->Completion
						|| ((VisMenu + (xcnt - Disabled - EntryBefore))->ValueBut != (VisMenu + (xcnt - Disabled - EntryBefore))->OldValueBut))
						)
					{	
						// Save old value to run in terminal mode also until value is set
						(VisMenu + (xcnt - Disabled - EntryBefore))->OldValueBut = (VisMenu + (xcnt - Disabled - EntryBefore))->ValueBut;	
						/* Copy button value */
						*(UDINT*)&usintPointer = (MenuEntry + xcnt)->WorkVariableAdr;
						*usintPointer = (VisMenu + (xcnt - Disabled - EntryBefore))->ValueBut;
						/* Set number of last changed entry */
						pMenu->Internal.LastChangedEntry = xcnt + 1;
						/* Reset completion value */
						(VisMenu + (xcnt - Disabled - EntryBefore))->Completion = 0;
			
						/* Color for switch device - show element*/
						if(EnableEntryShow == 1)
						{	/* If button value is 1 - button is pressed */
							if((VisMenu + (xcnt - Disabled - EntryBefore))->ValueBut == 1)
							{
								(VisMenu + (xcnt - Disabled - EntryBefore))->Color = COLOR_PRESSED;
							}
								/* If button value is 0 - button is released */
							else
							{
								(VisMenu + (xcnt - Disabled - EntryBefore))->Color = COLOR_RELEASED;
							}
						}
							/* Color for switch device - lock element */
						else if(EnableEntryShow == 2)
						{
							(VisMenu + (xcnt - Disabled - EntryBefore))->Color = COLOR_LOCKED;
						}
					}
						/* Checkbox - completion value not set */
					else if((MenuEntry + xcnt)->DynMenuEnryType == PD_CHECKBOX_INDEX && !(VisMenu + (xcnt - Disabled - EntryBefore))->Completion)
					{	/* Copy button value */
						*(UDINT*)&usintPointer = (MenuEntry + xcnt)->WorkVariableAdr;
						(VisMenu + (xcnt - Disabled - EntryBefore))->ValueBut = *usintPointer;
			
						/* Color for switch device - show element*/
						if(EnableEntryShow == 1)
						{	/* If button value is 1 - button is pressed */
							if((VisMenu + (xcnt - Disabled - EntryBefore))->ValueBut == 1)
							{
								(VisMenu + (xcnt - Disabled - EntryBefore))->Color = COLOR_PRESSED;
							}
								/* If button value is 0 - button is released */
							else
							{
								(VisMenu + (xcnt - Disabled - EntryBefore))->Color = COLOR_RELEASED;
							}
						}
							/* Color for switch device - lock element */
						else if(EnableEntryShow == 2)
						{
							(VisMenu + (xcnt - Disabled - EntryBefore))->Color = COLOR_LOCKED;
						}
					}
					//-------------------------------------------------------------------------------------------------------------
					// #OB ---------------------------------------- PRODUCTION OUTBOX ---------------------------------------------
					// #IP ---------------------------------------- PRODUCTION INPUT ----------------------------------------------
					// #OP ---------------------------------------- PRODUCTION OUTPUT ---------------------------------------------
					if((MenuEntry + xcnt)->DynMenuEnryType == PD_OUTBOX_INDEX ||(MenuEntry + xcnt)->DynMenuEnryType == PD_OUTPUT_INDEX || (MenuEntry + xcnt)->DynMenuEnryType == PD_INPUT_INDEX  )
					{	/* Copy button value */
						*(UDINT*)&usintPointer = (MenuEntry + xcnt)->WorkVariableAdr;
						(VisMenu + (xcnt - Disabled - EntryBefore))->ValueBut = LIMIT(0, *usintPointer, 1);
					}
					//-------------------------------------------------------------------------------------------------------------
					// #HD ---------------------------------------- PRODUCTION HEADLINE -------------------------------------------
					// #HD ---------------------------------------- MAINTANANCE HEADLINE  -----------------------------------------
					if((MenuEntry + xcnt)->DynMenuEnryType == PD_HEADLINE_INDEX||((MenuEntry + xcnt)->DynMenuEnryType == MT_HEADLINE_INDEX))
					{	/* Check position of headline and set visible */
						(VisMenu + (xcnt - Disabled - EntryBefore))->StatusText |= 0x0001;
			
						if(ActVisEntryNumber == (TmpEntriesPerPage - MIN_DISPLAYED_PAR_HD))
						{	/* Headline is at last position in Menue */
							(VisMenu + (xcnt - Disabled - EntryBefore))->StatusHeadline = 1;
			
							/* Set incremented ShiftOffset to following page */
							Page->ShiftOffset[Page->CurrentPage + 1] = (Page->ShiftOffset[Page->CurrentPage] + 1);
						}
						else
						{ /* Headline not at last position in Menue */
							(VisMenu + (xcnt - Disabled - EntryBefore))->StatusHeadline &= ~0x200F;
							(VisMenu + (xcnt - Disabled - EntryBefore))->StatusTextFunctionHeadLine |= 0x0001;
			
							/* Set actual ShiftOffset to following page */
							Page->ShiftOffset[Page->CurrentPage + 1] = Page->ShiftOffset[Page->CurrentPage];
						}
					} else if ((MenuEntry + xcnt)->DynMenuEnryType != PD_EMPTYLINE_INDEX && (MenuEntry + xcnt)->DynMenuEnryType != MT_EMPTYLINE_INDEX )									//	ONLY IF IT IS NOT AN EMPY LINE	E.Cilingir 2013-02-06
					{	/* Set text visible */
						(VisMenu + (xcnt - Disabled - EntryBefore))->StatusText &= ~0x200F;
						(VisMenu + (xcnt - Disabled - EntryBefore))->StatusHeadline |= 0x0001;
						(VisMenu + (xcnt - Disabled - EntryBefore))->StatusTextFunctionHeadLine |= 0x0001;	
						/* Set actual ShiftOffset to following page */
						Page->ShiftOffset[Page->CurrentPage + 1] = Page->ShiftOffset[Page->CurrentPage];
					}
					//-------------------------------------------------------------------------------------------------------------
					// #FH ---------------------------------------- PRODUCTION FUNCTION HEADLINE VISIBILITY -----------------------
					// #FH ---------------------------------------- MAINTANANCE FUNCTION HEADLINE VISIBILITY ----------------------
					if((MenuEntry + xcnt)->DynMenuEnryType == PD_FUNCTIONHEADLINE_INDEX || (MenuEntry + xcnt)->DynMenuEnryType == MT_FUNCTIONHEADLINE_INDEX){	
						(VisMenu + (xcnt - Disabled - EntryBefore))->StatusText |= 0x0001;							// Check position of headline and set visible
						(VisMenu + (xcnt - Disabled - EntryBefore))->Bitmap = (MenuEntry + xcnt)->FunctionHeadlineBitmap;		// Copy the FunctionHeadlineBitmap Information
						for(zcnt = 0; zcnt < MAX_STATUS; zcnt++){														// hide all input and output controls
							(VisMenu + (xcnt - Disabled - EntryBefore))->Status[zcnt] |= 0x0001;				
						}				
							
						if(((ActVisEntryNumber == (TmpEntriesPerPage - 2) && ((VisMenu + (xcnt - Disabled - EntryBefore))->ValueStatus>0))||(ActVisEntryNumber == (TmpEntriesPerPage - 1))))
						{	/* FUNCTIONHEADLINE is at last position in Menue */
							(VisMenu + (xcnt - Disabled - EntryBefore))->StatusTextFunctionHeadLine 	|= 0x0001;
							(VisMenu + (xcnt - Disabled - EntryBefore))->StatusNaviFunctionHeadLine 	|= 0x0001;
							EnableEntryShow																= 0;
							/* Set incremented ShiftOffset to following page */
							Page->ShiftOffset[Page->CurrentPage + 1] = (Page->ShiftOffset[Page->CurrentPage] + TmpEntriesPerPage-ActVisEntryNumber);		
							if (CmdPageUp){
								Page->CurrentPage = Page->CurrentPage + 1;
								CmdPageUp=0;
							}
						}
						else
						{  /* FUNCTIONHEADLINE not at last position in Menue */
							(VisMenu + (xcnt - Disabled - EntryBefore))->StatusTextFunctionHeadLine 	&= ~0x200F;							
							// Check additional the userLevel Bits, if the Entry will be displayed				E.Cilingir 2013-02-06
							if (pMenu->In.DisableUserLevel == 1){
								(VisMenu + (xcnt - Disabled - EntryBefore))->StatusNaviFunctionHeadLine &= ~0x200F;
							}
							else{
								ByteToBit.byteadr = (UDINT) &(MenuEntry + xcnt)->LevelStatus;   // store adress of byte array into structure
								ByteToBit.length = 2;                            			   	// store length of byte array into structure
								Byte2Bit(&ByteToBit);                               		   	// convert byte array into bit array
								Pointer = (void *) ByteToBit.bitadr;          				  	// get adress of byte bit
								for (i = 0; i<= MAXLEVEL; i ++){								// Put the Bit Infomration into the Array
									ValueLevelArray[i] = *(Pointer+i);
								}			
								for (i = 0; i<= MAXLEVEL; i ++){
									if(EnableEntryShow && (ValueLevelArray[i]==1)&&(PanelLevelArray[i]==1)){
										(VisMenu + (xcnt - Disabled - EntryBefore))->StatusNaviFunctionHeadLine &= ~0x200F;;
										break;
									}
								}	
							}
							// Check additional the userLevel Bits, if the Entry will be displayed
							if (EnableEntryShow==1){
								EnableEntryShow	= 2;
								if(EnableButton==1){
									(VisMenu +  (xcnt - Disabled - EntryBefore))->Status[15]	&= ~0x200F;
									EnableEntryShow												= 1;
								}	
							}
							/* Set actual ShiftOffset to following page */
							Page->ShiftOffset[Page->CurrentPage + 1] = Page->ShiftOffset[Page->CurrentPage];
			
							if (CmdPageDown && ActVisEntryNumber==2){
								Page->CurrentPage = Page->CurrentPage - 1;
								CmdPageDown=0;
							}
								
						}
								
						//if (Page->ShowHelpSelect!=1 && EnableEntryShow	== 2){
						//	EnableEntryShow	= 1;
						//}
									
						if ((MenuEntry + xcnt)->FunctionHeadlineStatusAdr == 0)	{											// If Empty value then Line is invisible
							(VisMenu + (xcnt - Disabled - EntryBefore))->StatusNaviFunctionHeadLine 	|= 0x0001;						
						}
						if ((MenuEntry + xcnt)->WorkVariableAdr == 0){											// If Empty value then Line is invisible
							(VisMenu + (xcnt - Disabled - EntryBefore))->Status[15] 	|= 0x0001;
							EnableEntryShow												= 0;
						}					
					}
					//-------------------------------------------------------------------------------------------------------------
					//---------------------------------------- PRODUCTION EMPTY LINE ----------------------------------------------
					//---------------------------------------- MAINTANANCE EMPTY LINE ---------------------------------------------	
					if((MenuEntry + xcnt)->DynMenuEnryType == PD_EMPTYLINE_INDEX ||(MenuEntry + xcnt)->DynMenuEnryType == MT_EMPTYLINE_INDEX  ){// Make all invisible in the Line
						(VisMenu + (xcnt - Disabled - EntryBefore))->StatusText 						|= 0x0001;
						(VisMenu + (xcnt - Disabled - EntryBefore))->StatusHeadline 					|= 0x0001;
						(VisMenu + (xcnt - Disabled - EntryBefore))->StatusTextFunctionHeadLine 		|= 0x0001;
						(VisMenu + (xcnt - Disabled - EntryBefore))->StatusNaviFunctionHeadLine 		|= 0x0001;
						for(k = 0; k <= MAX_STATUS; k++)
						{ 
							(VisMenu + (xcnt - Disabled - EntryBefore))->Status[k]					|= 0x0001; 
						}
					}					
					//-------------------------------------------------------------------------------------------------------------		
					//---------------------------------------- MAINTANANCE NO, IP, OP Visibility---- ------------------------------
					if(		(MenuEntry + xcnt)->DynMenuEnryType == MT_NUMERIC_INPUT_INDEX 
						||	(MenuEntry + xcnt)->DynMenuEnryType == MT_SENSING_DEVICE_INDEX
						|| 	(MenuEntry + xcnt)->DynMenuEnryType == MT_SWITCH_INDEX){
						if((MenuEntry + xcnt)->MaintVariableIPAdr){
							(VisMenu + (xcnt - Disabled - EntryBefore))->Status[21] &= ~0x200F;
							*(UDINT*)&usintPointer = (MenuEntry + xcnt)->MaintVariableIPAdr;
							(VisMenu + (xcnt - Disabled - EntryBefore))->ValueMaintIP = LIMIT(0, *usintPointer, 1);
						}
						else{
							(VisMenu + (xcnt - Disabled - EntryBefore))->Status[21] |= 0x0001;
						}
						if((MenuEntry + xcnt)->MaintVariableOPAdr){
							(VisMenu + (xcnt - Disabled - EntryBefore))->Status[22] &= ~0x200F;
							*(UDINT*)&usintPointer = (MenuEntry + xcnt)->MaintVariableOPAdr;
							(VisMenu + (xcnt - Disabled - EntryBefore))->ValueMaintOP = LIMIT(0, *usintPointer, 1);
						}
						else{
							(VisMenu + (xcnt - Disabled - EntryBefore))->Status[22] |= 0x0001;
						}
						if((MenuEntry + xcnt)->MaintVariableNOAdr){
							(VisMenu + (xcnt - Disabled - EntryBefore))->Status[20] 	&= ~0x200F;
							switch ((MenuEntry + xcnt)->MaintVariableNOType){
								case VAR_TYPE_BOOL:		*(UDINT*)&boolPointer = (MenuEntry + xcnt)->MaintVariableNOAdr;
									(VisMenu + (xcnt - Disabled - EntryBefore))->ValueMaintNO = *boolPointer;
									break;
								case VAR_TYPE_DINT:		*(UDINT*)&dintPointer = (MenuEntry + xcnt)->MaintVariableNOAdr;
									(VisMenu + (xcnt - Disabled - EntryBefore))->ValueMaintNO = *dintPointer;
									break;
								case VAR_TYPE_UDINT:	*(UDINT*)&udintPointer = (MenuEntry + xcnt)->MaintVariableNOAdr;
									(VisMenu + (xcnt - Disabled - EntryBefore))->ValueMaintNO = *udintPointer;
									break;				
								case VAR_TYPE_INT:		*(UDINT*)&intPointer = (MenuEntry + xcnt)->MaintVariableNOAdr;
									(VisMenu + (xcnt - Disabled - EntryBefore))->ValueMaintNO = *intPointer;
									break;					
								case VAR_TYPE_UINT:		*(UDINT*)&uintPointer = (MenuEntry + xcnt)->MaintVariableNOAdr;
									(VisMenu + (xcnt - Disabled - EntryBefore))->ValueMaintNO = *uintPointer;
									break;				
								case VAR_TYPE_SINT:		*(UDINT*)&sintPointer = (MenuEntry + xcnt)->MaintVariableNOAdr;
									(VisMenu + (xcnt - Disabled - EntryBefore))->ValueMaintNO = *sintPointer;
									break;				
								case VAR_TYPE_USINT:	*(UDINT*)&usintPointer = (MenuEntry + xcnt)->MaintVariableNOAdr;
									(VisMenu + (xcnt - Disabled - EntryBefore))->ValueMaintNO = *usintPointer;
									break;					
								case VAR_TYPE_REAL:		*(UDINT*)&realPointer = (MenuEntry + xcnt)->MaintVariableNOAdr;
									(VisMenu + (xcnt - Disabled - EntryBefore))->ValueMaintNO = *realPointer;
									break;
								case VAR_TYPE_TIME:		*(UDINT*)&TimePointer = (MenuEntry + xcnt)->MaintVariableNOAdr;
									(VisMenu + (xcnt - Disabled - EntryBefore))->ValueMaintNO = *TimePointer;
									break;	
							}
							(VisMenu + (xcnt - Disabled - EntryBefore))->ValueMaintNO =(VisMenu + (xcnt - Disabled - EntryBefore))->ValueMaintNO ;
						}
						else{
							(VisMenu + (xcnt - Disabled - EntryBefore))->Status[20] |= 0x0001;	
						}

						(VisMenu + (xcnt - Disabled - EntryBefore))->Status[18] |= 0x0001;
						(VisMenu + (xcnt - Disabled - EntryBefore))->Status[19] |= 0x0001;
						(VisMenu + (xcnt - Disabled - EntryBefore))->Status[26] |= 0x0001;		
						// Check additional the userLevel Bits, if the Entry will be displayed
						if(EnableButton==1){
							if ((MenuEntry + xcnt)->DynMenuEnryType == MT_NUMERIC_INPUT_INDEX){
								(VisMenu +  (xcnt - Disabled - EntryBefore))->Status[18]	&= ~0x200F;
							}
							if ((MenuEntry + xcnt)->DynMenuEnryType == MT_SENSING_DEVICE_INDEX){
								(VisMenu +  (xcnt - Disabled - EntryBefore))->Status[19]	&= ~0x200F;
							}
							if ((MenuEntry + xcnt)->DynMenuEnryType == MT_SWITCH_INDEX){
								(VisMenu +  (xcnt - Disabled - EntryBefore))->Status[26]	&= ~0x200F;
							}
						}						
						
						if (!(MenuEntry + xcnt)->WorkVariableAdr || EnableButton==0){
							(VisMenu + (xcnt - Disabled - EntryBefore))->Status[18] |= 0x0001;
							(VisMenu + (xcnt - Disabled - EntryBefore))->Status[19] |= 0x0001;
							(VisMenu + (xcnt - Disabled - EntryBefore))->Status[26] |= 0x0001;
						}

						if((MenuEntry + xcnt)->MaintVariableStatusAdr ){
							switch ((MenuEntry + xcnt)->MaintVariableStatusType){
								case VAR_TYPE_BOOL:		*(UDINT*)&boolPointer = (MenuEntry + xcnt)->MaintVariableStatusAdr;
									MtncCompareValue = *boolPointer;break;
								case VAR_TYPE_DINT:		*(UDINT*)&dintPointer = (MenuEntry + xcnt)->MaintVariableStatusAdr;
									MtncCompareValue = *dintPointer;break;
								case VAR_TYPE_UDINT:	*(UDINT*)&udintPointer = (MenuEntry + xcnt)->MaintVariableStatusAdr;
									MtncCompareValue = *udintPointer;break;				
								case VAR_TYPE_INT:		*(UDINT*)&intPointer  = (MenuEntry + xcnt)->MaintVariableStatusAdr;
									MtncCompareValue = *intPointer;break;					
								case VAR_TYPE_UINT:		*(UDINT*)&uintPointer = (MenuEntry + xcnt)->MaintVariableStatusAdr;
									MtncCompareValue = *uintPointer;break;				
								case VAR_TYPE_SINT:		*(UDINT*)&sintPointer = (MenuEntry + xcnt)->MaintVariableStatusAdr;
									MtncCompareValue = *sintPointer;break;				
								case VAR_TYPE_USINT:	*(UDINT*)&usintPointer = (MenuEntry + xcnt)->MaintVariableStatusAdr;
									MtncCompareValue = *usintPointer;break;					
								case VAR_TYPE_REAL:		*(UDINT*)&realPointer = (MenuEntry + xcnt)->MaintVariableStatusAdr;
									MtncCompareValue = *realPointer;break;
								case VAR_TYPE_TIME:		*(UDINT*)&TimePointer = (MenuEntry + xcnt)->MaintVariableStatusAdr;
									MtncCompareValue = *TimePointer;break;	
							}	
							if  (MtncCompareValue != ((MenuEntry + xcnt)->MaintVariableStatusCmpValue)) {
								if ((MenuEntry + xcnt)->DynMenuEnryType == MT_NUMERIC_INPUT_INDEX){
									(VisMenu + (xcnt - Disabled - EntryBefore))->Status[18] |= 0x0001;
								}
								if ((MenuEntry + xcnt)->DynMenuEnryType == MT_SENSING_DEVICE_INDEX){
									(VisMenu + (xcnt - Disabled - EntryBefore))->Status[19] |= 0x0001;
								}
								if ((MenuEntry + xcnt)->DynMenuEnryType == MT_SWITCH_INDEX){
									(VisMenu + (xcnt - Disabled - EntryBefore))->Status[26] |= 0x0001;
								}
							}
						
						}
					}	
					/* Call function "ENTRYSTATUS", for further information see code below */
					/* Check entry status */
					if ((MenuEntry + xcnt)->ScaleValueAdr>0){
						switch ((MenuEntry + xcnt)->ScaleValueType){				
							case VAR_TYPE_UINT:		*(UDINT*)&uintPointer = (MenuEntry + xcnt)->ScaleValueAdr;
								(MenuEntry + xcnt)->ScaleValue = *uintPointer;
								break;							
							case VAR_TYPE_USINT:	*(UDINT*)&usintPointer = (MenuEntry + xcnt)->ScaleValueAdr;
								(MenuEntry + xcnt)->ScaleValue = *usintPointer;
								break;					
						}
					}
					EntryStatus(EnableEntryShow, (MenuEntry + xcnt)->DynMenuEnryType, (MenuEntry + xcnt)->ScaleValue, (UDINT)&(VisMenu + (xcnt - Disabled - EntryBefore))->UseStatus, (UDINT)&(VisMenu + (xcnt - Disabled - EntryBefore))->Status, (UDINT)&(VisMenu + (xcnt - Disabled - EntryBefore))->Color, (UDINT)&(VisMenu + (xcnt - Disabled - EntryBefore))->Point);
					/* Increase number of displayed entries */
					ActVisEntryNumber++;
				}
					/* Calculate page offset */
				else
				{
					EntryBefore++;/*Count the enabled entries in Menue for the previous pages*/
				}
			}
				/* Entry is disabled and page is not full */
			else if(!PageFull)
			{
				if(EnableEntryShow || !EnableEntry)
				{
					Disabled++;
				}
			}
				/* Entry is enabled and page is already full */
			else if((EnableEntryShow || EnableEntry) && PageFull)
			{
				/* Flag that more pages are avaliable */
				Page->OneMorePage = 1;
			}
		}
		/*End of "FOR..." Loop*/
	}
	/* Reset flag that more pages are avaliable */
	if(xcnt == pMenu->Internal.ActEntryNumber && !PageFull)
	{
		Page->OneMorePage = 0;
	}
	//##########################################################################################################################################
	//#################################################### INFORMATIONS ON PAGE WHILE LOADING ##################################################
	//##########################################################################################################################################
	if (pMenu->Out.InitOk==0 || NrLinesOk==0){
		if (((pMenu->Internal.ActAdrDataBuffer - pMenu->Internal.StartAdrDataBuffer) > 0) && (pMenu->Internal.FileSize > 0)){
			(VisMenu+(0))->ValueProgressBar= (pMenu->Internal.ActAdrDataBuffer - pMenu->Internal.StartAdrDataBuffer)*100/pMenu->Internal.FileSize;
		}
		(VisMenu + (1))->ValueNum =	pMenu->Internal.ActiveReadEntry;
		memcpy(&((VisMenu + (2))->ValueAlph),(pMenu->Internal.ParserString),sizeof((VisMenu + (2))->ValueAlph));
		(VisMenu + (3))->ValueNum = pMenu->Out.Status;
		ActVisEntryNumber = 6;
		TmpEntriesPerPage = 6;
		memset(Page->Out.GrpBoxStatus, 1, sizeof(Page->Out.GrpBoxStatus));
		Page->Out.GrpBoxStatus[ActVisEntryNumber-1] = 0;
		for(xcnt = 0; xcnt < (ActVisEntryNumber); xcnt++)
		{	
			for(k = 0; k <= MAX_STATUS; k++)
			{
				(VisMenu + (xcnt))->Status[k]					|= 0x0001;
				memset(&(VisMenu + (k))->ValueAlph,0,sizeof((VisMenu + (k))->ValueAlph));
			}
			(VisMenu + (xcnt))->StatusHeadline 					|= 0x0001;
			(VisMenu + (xcnt))->StatusText 						|= 0x0001;
			(VisMenu + (xcnt))->StatusTextFunctionHeadLine 		|= 0x0001;
			(VisMenu + (xcnt))->StatusNaviFunctionHeadLine 		|= 0x0001;
		}

		(VisMenu + (0))->StatusHeadline 	&= 	~0x200F;
		(VisMenu + (0))->DescriptionIndex 	=	TEXT_INFOMRATION;
		(VisMenu + (0))->StatusProgressBar 	&= 	~0x200F;
		(VisMenu + (0))->UnitIndex			=	0;
		(VisMenu + (0))->Point				=	0;
		
		(VisMenu + (1))->Status[4] 			&= 	~0x200F;		
		(VisMenu + (1))->AddFraction 		=	0;
		(VisMenu + (1))->StatusText 		&= 	~0x200F;
		(VisMenu + (1))->DescriptionIndex 	=	TEXT_ERROR_IN_ENTRY_NUMBER;
		(VisMenu + (1))->UnitIndex			=	0;	
		(VisMenu + (1))->Point				=	0;
		
		(VisMenu + (2))->Status[6] 			&= 	~0x200F;	
		(VisMenu + (2))->StatusText 		&= 	~0x200F;
		(VisMenu + (2))->DescriptionIndex 	=	TEXT_LAST_PARSED_STRING;
		(VisMenu + (2))->UnitIndex			=	0;
		(VisMenu + (2))->Point				=	0;
		
		(VisMenu + (3))->Status[4] 			&= 	~0x200F;	
		(VisMenu + (3))->AddFraction 		=	0;
		(VisMenu + (3))->StatusText 		&= 	~0x200F;
		(VisMenu + (3))->DescriptionIndex 	=	TEXT_STATUS;
		(VisMenu + (3))->UnitIndex			=	0;
		(VisMenu + (3))->Point				=	0;
		
		(VisMenu + (4))->Status[6] 			&= 	~0x200F;	
		(VisMenu + (4))->AddFraction 		=	0;
		(VisMenu + (4))->StatusText 		&= 	~0x200F;
		(VisMenu + (4))->DescriptionIndex 	=	TEXT_ERROR_DESCRIPTION;
		(VisMenu + (5))->DescriptionIndex 	=	pMenu->Out.Status;
		(VisMenu + (4))->UnitIndex			=	0;	
		(VisMenu + (4))->Point				=	0;
		
		if(pMenu->Internal.InitStep==8){
			(VisMenu + (3))->Status[9] 			&= 	~0x200F;	
			(VisMenu + (3))->StatusText 		&= 	~0x200F;
			(VisMenu + (3))->DescriptionIndex 	=	TEXT_RELOAD_THIS_FILE_ONLY;
			(VisMenu + (3))->UnitIndex			=	0;
			(VisMenu + (3))->Point				=	0;
			if ((VisMenu + (3))->ValueBut==1){
				(VisMenu + (3))->ValueBut		=	0;
				(VisMenu + (3))->Completion		=	0;
				pMenu->Internal.ActiveReadEntry		=	0;
				pMenu->Out.Status				=	CMD_RELOAD;
			}
		}
		
	}
	else
	{		
		(VisMenu+(0))->StatusProgressBar 		|= 0x0001;
	}
	//##########################################################################################################################################
	//#################################################### HIDE UNUSED ELEMENTS ################################################################
	//##########################################################################################################################################
	if(ActVisEntryNumber < TmpEntriesPerPage){	
		for(xcnt = 0; xcnt < (TmpEntriesPerPage - ActVisEntryNumber); xcnt++){	
			for(k = 0; k <= MAX_STATUS; k++)
			{
				(VisMenu + (TmpEntriesPerPage - 1 - xcnt))->Status[k]					|= 0x0001;
			}
			(VisMenu + (TmpEntriesPerPage - 1 - xcnt))->StatusHeadline 					|= 0x0001;
			(VisMenu + (TmpEntriesPerPage - 1 - xcnt))->StatusText 						|= 0x0001;
			(VisMenu + (TmpEntriesPerPage - 1 - xcnt))->StatusTextFunctionHeadLine 		|= 0x0001;
			(VisMenu + (TmpEntriesPerPage - 1 - xcnt))->StatusNaviFunctionHeadLine 		|= 0x0001;
		}
	}
	//##########################################################################################################################################
	//#################################################### HIDE UNUSED ELEMENTS ################################################################
	//##########################################################################################################################################
	if(TmpEntriesPerPage < MAX_ELEMENT_PAGE &&  TmpEntriesPerPage > 0){	
		for(xcnt = TmpEntriesPerPage; xcnt <= MAX_ELEMENT_PAGE; xcnt++){	
			for(k = 0; k <= MAX_STATUS; k++)
			{
				(VisMenu + (xcnt))->Status[k]					|= 0x0001; 
			}
			(VisMenu + (xcnt))->StatusHeadline 					|= 0x0001;
			(VisMenu + (xcnt))->StatusText 						|= 0x0001;
			(VisMenu + (xcnt))->StatusTextFunctionHeadLine 		|= 0x0001;
			(VisMenu + (xcnt))->StatusNaviFunctionHeadLine 		|= 0x0001;
		}
	}
	//##########################################################################################################################################
	//#################################################### RESET VARIABLE ######################################################################
	ActVisEntryNumber = 0;
	Disabled = 0;
	EntryBefore = 0;
	//##########################################################################################################################################
	//#################################################### CALCULATE PAGE NUMBER INFOMRATION ###################################################
	Page->NumberOfPages= (EnabledEntries + Page->ShiftedHeadlines) / TmpEntriesPerPage;
	if((EnabledEntries + Page->ShiftedHeadlines) % TmpEntriesPerPage){
		Page->NumberOfPages++;
	}
	if(Page->NumberOfPages == Page->CurrentPage+1){
		Page->OneMorePage = 0;
	}	
	//##########################################################################################################################################
	//#################################################### RESET SHIFTED HEADLINES #############################################################
	Page->ShiftedHeadlines = 0;
	for(zcnt = 0; zcnt <= Page->NumberOfPages; zcnt++){ /* Check maximum value ShiftedHeadlines */
		if (Page->ShiftOffset[zcnt] > Page->ShiftedHeadlines)
		{
			Page->ShiftedHeadlines = Page->ShiftOffset[zcnt];
		}
	}
	//##########################################################################################################################################
	//#################################################### SHOW HELP LAYER #####################################################################
	//##########################################################################################################################################
	//	if (Page->ShowHelpSelect!=1){
	//		for(xcnt = 0; xcnt < TmpEntriesPerPage; xcnt++){	
	//			if ((VisMenu+(xcnt))->Status[15]==0 && (VisMenu+(xcnt))->HelpID>0 ){
	//				(VisMenu+(xcnt))->Status[15]=0;
	//			}
	//			else{
	//				(VisMenu+(xcnt))->Status[15]=1;	
	//			}
	//		}
	//		if (Page->ShowHelpCommand!=0 && Page->CurrentError!=200 && Page->CurrentUrlBusy==0){
	//			Page->ShowHelpInfo = 0;
	//			brsstrcpy((UDINT)&(Page->ChangeUrl),(UDINT)&(pMenu->Internal.HelpPath));
	//			brsitoa(Page->ActualLanguage , (UDINT)&(HelpString)); 
	//			brsstrcat((UDINT)&(Page->ChangeUrl),(UDINT)&(HelpString));
	//			brsstrcat((UDINT)&(Page->ChangeUrl),(UDINT)&"/");
	//			brsstrcat((UDINT)&(Page->ChangeUrl),(UDINT)&(pMenu->Internal.HelpName));
	//			brsstrcat((UDINT)&(Page->ChangeUrl),(UDINT)&"#");
	//			brsitoa(((VisMenu + (Page->ShowHelpCommand-1))->HelpID), (UDINT)&(HelpString)); 
	//			brsstrcat((UDINT)&(Page->ChangeUrl),(UDINT)&(HelpString));
	//		}
	//		if(Page->CurrentError==200&&Page->CurrentUrlBusy==0 ){
	//			Page->ShowHelpCommand=0;
	//		}
	//		if(Page->CurrentError==404&&Page->CurrentUrlBusy==0 ){
	//			Page->ActualLanguage = 0;
	//			brsstrcpy((UDINT)&(Page->ChangeUrl), (UDINT)&(pMenu->Internal.HelpPath));
	//			brsitoa(Page->ActualLanguage , (UDINT)&(HelpString)); 
	//			brsstrcat((UDINT)&(Page->ChangeUrl),(UDINT)&(HelpString));
	//			brsstrcat((UDINT)&(Page->ChangeUrl),(UDINT)&"/");
	//			brsstrcat((UDINT)&(Page->ChangeUrl),(UDINT)&(pMenu->Internal.HelpName));
	//			brsstrcat((UDINT)&(Page->ChangeUrl),(UDINT)&"#");
	//			brsitoa(((VisMenu + (Page->ShowHelpCommand-1))->HelpID), (UDINT)&(HelpString)); 
	//			brsstrcat((UDINT)&(Page->ChangeUrl),(UDINT)&(HelpString));
	//		}
	//	}
	//	else	{
	//		Page->ShowHelpInfo = 1;
	//		Page->ShowHelpCommand = 0;
	//		brsstrcpy((UDINT)&(Page->ChangeUrl),(UDINT)&"");
	//		Page->CurrentError=0;
	//	}
	/* Finished */
	return(FUNCTION_OK);
}

//##########################################################################################################################################
//#################################################### SET STATUS VISIBILITY ###############################################################
//##########################################################################################################################################
void EntryStatus(USINT Enable, USINT DynMenuEnryType, UINT Point, UDINT AdrUseStatusVar, UDINT AdrStatusVar, UDINT AdrColorVar, UDINT AdrPoint)
{	/* Back ground colors */
#define	INPUTFIELD			46
#define OUTPUTFIELD			226
#define	INPUTFIELD_LOCKED	59

	/* Status information if control is locked and pressed */
#define	CHECK_LOCKING		8192

	/* Variables */
	UINT	*usestatus = 0, (*status)[4], *color = 0, zcnt = 0, StatusInfo = 0;
	UINT	*point= 0;

	/* Pointer to address */
	*(UDINT*)&usestatus = AdrUseStatusVar;
	*(UDINT*)&status = AdrStatusVar;
	*(UDINT*)&color = AdrColorVar;
	*(UDINT*)&point = AdrPoint;
	//-------------------------------------------------------------------------------------------------------------		
	//---------------------------------------- DISCRIPTION OF THE STATUS ELEMENTS ---------------------------------	
	/*
	STATUS[0]	=	Nummeric Input Normal view
	STATUS[1]	=	Alphanummeric Input Normal view		
	STATUS[2]	=	Switch Normal view			
	STATUS[3]	=	Sensing device Normal view		
	STATUS[4]	=	Nummeric Output Normal view		
	STATUS[5]	=	Nummeric Input Locked view		
	STATUS[6]	=	Alphanummeric Output Normal view			
	STATUS[7]	=	Alphanummeric Input Locked view			
	STATUS[8]	=	Switch Locked view						
	STATUS[9]	=	Checkbox Normal view		
	STATUS[10]	=	Checkbox Locked view		
	STATUS[11]	=	Outbox Normal view		
	STATUS[12]	=	Up Down Normal view		
	STATUS[13]	=	Input Normal view		
	STATUS[14]	=	Output Normal view		
	STATUS[15]	=	Function Headline Normal view			
	STATUS[16]	=	Function Headline Locked view
	STATUS[17]	=	Alphanummeric Input Up Down Normal view			
	STATUS[18]	=	Maintanance Numeric Input
	STATUS[19]	=	Maintanance sensing device
	STATUS[20]	=	Maintanance Numeric Output
	STATUS[21]	=	Maintanance Input
	STATUS[22]	=	Maintanance Output
	STATUS[23]	=	Sensing device Locked view
	STATUS[24]	=   Up Down Normal/Locked view unit
	STATUS[25]	=	Up Down Locked view	
	STATUS[26]	=	Maintanance Switch
	STATUS[27]	=	Alphanumeric Output text group view
	*/
	
	//-------------------------------------------------------------------------------------------------------------		
	//---------------------------------------- PRODUCTION NUMMERIC INPUT VISIBILITY -------------------------------
	if(DynMenuEnryType == PD_NUMERIC_INPUT_INDEX){
		if(Enable == 1)	{
			*color = INPUTFIELD;
			*usestatus = 0;
		}
		else if(Enable == 2){
			*color = INPUTFIELD_LOCKED;
			*usestatus = 5;
		}
		*point = Point;
		/* Status of menu entry */
		for(zcnt = 0; zcnt <= MAX_STATUS; zcnt++){
			if(zcnt == *usestatus){
				StatusInfo = (*status)[zcnt] & CHECK_LOCKING;
				if(Enable == 1 && !StatusInfo){
					(*status)[zcnt] &= ~0x2003; //reset locking and invisible bit
				}
				else if(Enable == 2){
					(*status)[zcnt] &= ~0x2003; //reset invisible bit etc.
					(*status)[zcnt] |= 0x02; 	//set locking bit
				}
			}
			else{
				(*status)[zcnt] &= ~0x02; 		//reset locking bit
				(*status)[zcnt] |= 0x01; 		//set invisible bit
			}
		}
	}
	//-------------------------------------------------------------------------------------------------------------		
	//---------------------------------------- PRODUCTION NUMMERIC OUTPUT VISIBILITY ------------------------------
	if(DynMenuEnryType == PD_NUMERIC_OUTPUT_INDEX){	
		*color = OUTPUTFIELD;
		*usestatus = 4;		
		*point = Point;
		for(zcnt = 0; zcnt <= MAX_STATUS; zcnt++){
			if(zcnt == *usestatus){
				(*status)[zcnt] &= ~0x01; 		//reset invisible bit etc.
				(*status)[zcnt] |= 0x02; 		//set locking bit
			}
			else{
				(*status)[zcnt] &= ~0x02; 		//reset locking bit
				(*status)[zcnt] |= 0x01; 		//set invisible bit
			}
		}
	}
	//-------------------------------------------------------------------------------------------------------------		
	//---------------------------------------- PRODUCTION ALPHANUMMERIC INPUT VISIBILITY --------------------------
	if(DynMenuEnryType == PD_ALPHANUMERIC_INPUT_INDEX){
		if(Enable == 1)	{
			*color = INPUTFIELD;
			*usestatus = 1;
		}
		else if(Enable == 2){
			*color = INPUTFIELD_LOCKED;
			*usestatus = 7;
		}
		for(zcnt = 0; zcnt <= MAX_STATUS; zcnt++){
			if(zcnt == *usestatus){
				StatusInfo = (*status)[zcnt] & CHECK_LOCKING;
				if(Enable == 1 && !StatusInfo){
					(*status)[zcnt] &= ~0x2003; //reset locking and invisible bit
				}
				else if(Enable == 2){
					(*status)[zcnt] &= ~0x2003; //reset invisible bit etc.
					(*status)[zcnt] |= 0x02; 	//set locking bit
				}
			}
			else{
				(*status)[zcnt] &= ~0x02; 		//reset locking bit
				(*status)[zcnt] |= 0x01; 		//set invisible bit
			}
		}
	}
	//-------------------------------------------------------------------------------------------------------------		
	//---------------------------------------- PRODUCTION ALPHANUMMERIC OUTPUT VISIBILITY -------------------------
	if(DynMenuEnryType == PD_ALPHANUMERIC_OUTPUT_INDEX){	
		*color= OUTPUTFIELD;
		for(zcnt = 0; zcnt <= MAX_STATUS; zcnt++){
			if(zcnt != 6 && zcnt != 27)	{
				(*status)[zcnt] &= ~0x02; 		//reset locking bit
				(*status)[zcnt] |= 0x01; 		//set invisible bit
			}
		}
	}
	//-------------------------------------------------------------------------------------------------------------		
	//---------------------------------------- PRODUCTION SENSING DEVICE VISIBILITY -------------------------------
	if(DynMenuEnryType == PD_SENSING_DEVICE_INDEX ){	
		if(Enable == 2)	{
			*usestatus = 23;
		}
		else{
			*usestatus = 3;
		}
		for(zcnt = 0; zcnt <= MAX_STATUS; zcnt++){
			if(zcnt == *usestatus){	
				StatusInfo = (*status)[zcnt] & CHECK_LOCKING;
				if(Enable == 1 && !StatusInfo){
					(*status)[zcnt] &= ~0x2003; //reset locking and invisible bit
				}
				else if(Enable == 2){
					(*status)[zcnt] &= ~0x2003; //reset invisible bit etc.
					(*status)[zcnt] |= 0x02; 	//set locking bit
				}
			}
			else{
				(*status)[zcnt] &= ~0x02; 		//reset locking bit
				(*status)[zcnt] |= 0x01; 		//set invisible bit
			}
		}
	}
	//-------------------------------------------------------------------------------------------------------------		
	//---------------------------------------- PRODUCTION UP DOWN VISIBILITY --------------------------------------
	if(DynMenuEnryType == PD_UPDOWN_INDEX){	
		*point = Point;
		if(Enable == 2){	
			*usestatus = 12;
		}
		else{
			*usestatus = 12;
		}
		for(zcnt = 0; zcnt <= MAX_STATUS; zcnt++){
			if(zcnt == *usestatus){
				StatusInfo = (*status)[zcnt] & CHECK_LOCKING;
				if(Enable == 1 && !StatusInfo){
					(*status)[zcnt] &= ~0x2003; //reset locking and invisible bit
				}
				else if(Enable == 2){
					(*status)[zcnt] &= ~0x2003; //reset invisible bit etc.
					(*status)[zcnt] |= 0x02; 	//set locking bit
				}
			}
			else{
				(*status)[zcnt] &= ~0x02; 		//reset locking bit
				(*status)[zcnt] |= 0x01; 		//set invisible bit
			}
			if(zcnt+1==17){
				zcnt++;
			}
			if(zcnt+1==24){
				zcnt++;
				zcnt++;
			}
		}
	}
	//-------------------------------------------------------------------------------------------------------------		
	//---------------------------------------- PRODUCTION SWITCH VISIBILITY ---------------------------------------
	if(DynMenuEnryType == PD_SWITCH_INDEX){	
		if(Enable == 2){
			*usestatus = 8;
		}
		else{
			*usestatus = 2;
		}
		for(zcnt = 0; zcnt <= MAX_STATUS; zcnt++){
			if(zcnt == *usestatus){
				StatusInfo = (*status)[zcnt] & CHECK_LOCKING;
				if(Enable == 1 && !StatusInfo){
					(*status)[zcnt] &= ~0x2003; //reset locking and invisible bit
				}
				else if(Enable == 2){
					(*status)[zcnt] &= ~0x2003; //reset invisible bit etc.
					(*status)[zcnt] |= 0x02; 	//set locking bit
				}
			}
			else{
				(*status)[zcnt] &= ~0x02; 		//reset locking bit
				(*status)[zcnt] |= 0x01; 		//set invisible bit
			}
		}
	}
	//-------------------------------------------------------------------------------------------------------------		
	//---------------------------------------- PRODUCTION FUNCTION HEADLINE VISIBILITY ----------------------------
	//---------------------------------------- MAINTANANCE FUNCTION HEADLINE VISIBILITY ---------------------------
	if(DynMenuEnryType == PD_FUNCTIONHEADLINE_INDEX || DynMenuEnryType == MT_FUNCTIONHEADLINE_INDEX){
		if(Enable == 0){
			*usestatus = MAX_STATUS+1;
		}		
		else if(Enable == 2){
			*usestatus = 16;
		}
		else{
			*usestatus = 15;
		}
		for(zcnt = 0; zcnt <= MAX_STATUS; zcnt++){
			if(zcnt == *usestatus){
				StatusInfo = (*status)[zcnt] & CHECK_LOCKING;
				if(Enable  == 1 && !StatusInfo){
					(*status)[zcnt] &= ~0x2003; 	//reset locking and invisible bit
				}
				else if (Enable == 2){
					(*status)[zcnt] &= ~0x2003; 	//reset locking and invisible bit
					(*status)[zcnt] |= 0x02; 		//set invisible bit
				}
			}	
			else{
				(*status)[zcnt] &= ~0x02; 		//reset locking bit
				(*status)[zcnt] |= 0x01; 		//set invisible bit
			}
		}
	}
	//-------------------------------------------------------------------------------------------------------------		
	//---------------------------------------- PRODUCTION EMPTYLINE VISIBILITY ------------------------------------
	//---------------------------------------- MAINTANANCE EMPTYLINE VISIBILITY -----------------------------------
	if(DynMenuEnryType == PD_EMPTYLINE_INDEX ||DynMenuEnryType == MT_EMPTYLINE_INDEX ){
		for(zcnt = 0; zcnt <= MAX_STATUS; zcnt++){
			(*status)[zcnt] &= ~0x02; 		//reset locking bit
			(*status)[zcnt] |= 0x01; 		//set invisible bit
		}
	}
	//-------------------------------------------------------------------------------------------------------------		
	//---------------------------------------- PRODUCTION CHECKBOX VISIBILITY -------------------------------------
	if(DynMenuEnryType == PD_CHECKBOX_INDEX){
		if(Enable == 2){
			*usestatus = 10;
		}
		else{
			*usestatus = 9;
		}
		for(zcnt = 0; zcnt <= MAX_STATUS; zcnt++){
			if(zcnt == *usestatus){
				StatusInfo = (*status)[zcnt] & CHECK_LOCKING;
				if(Enable == 1 && !StatusInfo){
					(*status)[zcnt] &= ~0x2003; //reset locking and invisible bit
				}
				else if(Enable == 2){
					(*status)[zcnt] &= ~0x2003; //reset invisible bit etc.
					(*status)[zcnt] |= 0x02; 	//set locking bit
				}
			}
			else{
				(*status)[zcnt] &= ~0x02; 		//reset locking bit
				(*status)[zcnt] |= 0x01; 		//set invisible bit
			}
		}
	}
	//-------------------------------------------------------------------------------------------------------------		
	//---------------------------------------- PRODUCTION OUTBOX VISIBILITY ---------------------------------------
	if(DynMenuEnryType == PD_OUTBOX_INDEX){	
		*usestatus = 11;
		for(zcnt = 0; zcnt <= MAX_STATUS; zcnt++){
			if(zcnt == *usestatus){
				(*status)[zcnt] &= ~0x2003; 	//reset locking and invisible bit
			}
			else{
				(*status)[zcnt] &= ~0x02; 		//reset locking bit
				(*status)[zcnt] |= 0x01; 		//set invisible bit
			}
		}
	}
	//-------------------------------------------------------------------------------------------------------------		
	//---------------------------------------- PRODUCTION INPUT VISIBILITY ----------------------------------------
	if(DynMenuEnryType == PD_INPUT_INDEX){	
		*usestatus = 13;
		for(zcnt = 0; zcnt <= MAX_STATUS; zcnt++){
			if(zcnt == *usestatus){
				(*status)[zcnt] &= ~0x2003; 	//reset locking and invisible bit
			}
			else{
				(*status)[zcnt] &= ~0x02; 		//reset locking bit
				(*status)[zcnt] |= 0x01; 		//set invisible bit
			}
		}
	}
	//-------------------------------------------------------------------------------------------------------------		
	//---------------------------------------- PRODUCTION OUTPUT VISIBILITY ---------------------------------------
	if(DynMenuEnryType == PD_OUTPUT_INDEX){	
		*usestatus = 14;
		for(zcnt = 0; zcnt <= MAX_STATUS; zcnt++){
			if(zcnt == *usestatus){
				(*status)[zcnt] &= ~0x2003; 	//reset locking and invisible bit
			}
			else{
				(*status)[zcnt] &= ~0x02; 		//reset locking bit
				(*status)[zcnt] |= 0x01; 		//set invisible bit
			}
		}
	}	
	//-------------------------------------------------------------------------------------------------------------		
	//---------------------------------------- PRODUCTION HEADLINE VISIBILITY -------------------------------------
	//---------------------------------------- MAINTANANCE HEADLINE VISIBILITY ------------------------------------
	if(DynMenuEnryType == PD_HEADLINE_INDEX || DynMenuEnryType == MT_HEADLINE_INDEX){	
		for(zcnt = 0; zcnt <= MAX_STATUS; zcnt++){			
			(*status)[zcnt] &= ~0x02; 		//reset locking bit
			(*status)[zcnt] |= 0x01; 		//set invisible bit
		}
	}
	//-------------------------------------------------------------------------------------------------------------		
	//---------------------------------------- MAINTANANCE NUMMERIC INPUT VISIBILITY ------------------------------
	//---------------------------------------- MAINTANANCE SENSING DEVICE VISIBILITY ------------------------------
	if(DynMenuEnryType == MT_NUMERIC_INPUT_INDEX || DynMenuEnryType == MT_SENSING_DEVICE_INDEX || DynMenuEnryType == MT_SWITCH_INDEX){	
		*point = Point;
		// Make all elements invisible except the maintenance elements
		for(zcnt = 0; zcnt <= 17; zcnt++){
			(*status)[zcnt] &= ~0x02; 		//reset locking bit
			(*status)[zcnt] |= 0x01; 		//set invisible bit
		}
		for(zcnt = 23; zcnt <= 25; zcnt++){ //
			(*status)[zcnt] &= ~0x02; 		//reset locking bit
			(*status)[zcnt] |= 0x01; 		//set invisible bit
		}		
		for(zcnt = 27; zcnt <= MAX_STATUS; zcnt++){ //
			(*status)[zcnt] &= ~0x02; 		//reset locking bit
			(*status)[zcnt] |= 0x01; 		//set invisible bit
		}		
	}	
}


