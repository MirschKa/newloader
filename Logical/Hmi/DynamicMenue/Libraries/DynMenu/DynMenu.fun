
FUNCTION initmenu : UINT
	VAR_INPUT
		AdrFileDeviceString : UDINT;
		AdrFileNameString : UDINT;
		AdrMenuStructure : UDINT;
	END_VAR
END_FUNCTION

FUNCTION runmenu : UINT
	VAR_INPUT
		AdrMenuStructure : UDINT;
		AdrVisMenuStructure : UDINT;
		AdrPageStructure : UDINT;
	END_VAR
END_FUNCTION
