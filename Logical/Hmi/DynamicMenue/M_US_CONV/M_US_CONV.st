(************************************************************************************************************************
 * COPYRIGHT -- GEA FS
 ************************************************************************************************************************
 * PROGRAM: M_US_CONV
 * File: M_US_CONV.st
 * Author: Erdal Cilingir
 * Created: 20-OKT-2013
 ************************************************************************************************************************
 * Implementation OF PROGRAM init_men
 * This task shows an example FOR using the dynamic menu
 * In this task the Page Infomrations are readed AND the example Variable are setted
//***********************************************************************************************************************)
// Version 1.00.0  20-oct-2014 E.Cilingir                       Development tool: B&R Automation Studio V3.0.90.28 SP10 *)
// First Implementation
//***********************************************************************************************************************)
// Version 1.00.1  10-nov-2014 E.Cilingir                       Development tool: B&R Automation Studio V3.0.90.28 SP10 *)
// [#452] Change fraction digits by variable inside Dynamic Menu
//       - Expand Conversion example with varaible .Scale and .Fraction for chanigin also fraction digits during runtime
//***********************************************************************************************************************)
PROGRAM _INIT
	ExampleConversion;
	// Set Last Mode to initialiaze after boot
	MetricUsConversion.Internal.LastMode := MetricUsConversion.In.Mode + 1;
END_PROGRAM


PROGRAM _CYCLIC
	// Check if the mode has changed
	IF MetricUsConversion.Internal.LastMode <> MetricUsConversion.In.Mode THEN
		// Check for the configured mode is available
		IF MetricUsConversion.In.Mode >= 2 THEN
			MetricUsConversion.In.Mode := 0;
		END_IF
		// Calculate the index for all conversions
		FOR i:=0 TO MAX_CONVERSION DO	
			MetricUsConversion.Out.Value[i]	:=	START_CONV_INDEX + i*CONV_INDEX_STEP + MetricUsConversion.In.Mode;
			// Set the index values into variable to have expressiveness named variables
			CASE i OF
				0:	MetricUsConversion.Out.Conv_m_or_ft.Scale 						:= MetricUsConversion.Out.Value[i];
					CASE MetricUsConversion.In.Mode OF
						0:	MetricUsConversion.Out.Conv_m_or_ft.Fraction			:= M_US_FRACTION_METER;
						1:	MetricUsConversion.Out.Conv_m_or_ft.Fraction			:= M_US_FRACTION_FEET;
					END_CASE					
				1:	MetricUsConversion.Out.Conv_mm_or_in.Scale						:= MetricUsConversion.Out.Value[i];
					CASE MetricUsConversion.In.Mode OF
						0:	MetricUsConversion.Out.Conv_mm_or_in.Fraction			:= M_US_FRACTION_MILLIMETER;
						1:	MetricUsConversion.Out.Conv_mm_or_in.Fraction			:= M_US_FRACTION_INCH;
					END_CASE					
				2:	MetricUsConversion.Out.Conv_m_s_or_ft_s.Scale					:= MetricUsConversion.Out.Value[i];
					CASE MetricUsConversion.In.Mode OF
						0:	MetricUsConversion.Out.Conv_m_s_or_ft_s.Fraction		:= M_US_FRACTION_METER;
						1:	MetricUsConversion.Out.Conv_m_s_or_ft_s.Fraction		:= M_US_FRACTION_FEET;
					END_CASE		
				3:	MetricUsConversion.Out.Conv_m_min_or_ft_min.Scale				:= MetricUsConversion.Out.Value[i];
					CASE MetricUsConversion.In.Mode OF
						0:	MetricUsConversion.Out.Conv_m_min_or_ft_min.Fraction	:= M_US_FRACTION_METER;
						1:	MetricUsConversion.Out.Conv_m_min_or_ft_min.Fraction	:= M_US_FRACTION_FEET;
					END_CASE				
				4:	MetricUsConversion.Out.Conv_mm_s_or_in_s.Scale	 				:= MetricUsConversion.Out.Value[i];	
					CASE MetricUsConversion.In.Mode OF
						0:	MetricUsConversion.Out.Conv_mm_s_or_in_s.Fraction		:= M_US_FRACTION_MILLIMETER;
						1:	MetricUsConversion.Out.Conv_mm_s_or_in_s.Fraction		:= M_US_FRACTION_INCH;
					END_CASE	
				5:	MetricUsConversion.Out.Conv_bar_or_psi.Scale		 			:= MetricUsConversion.Out.Value[i];	
					CASE MetricUsConversion.In.Mode OF
						0:	MetricUsConversion.Out.Conv_bar_or_psi.Fraction			:= M_US_FRACTION_BAR;
						1:	MetricUsConversion.Out.Conv_bar_or_psi.Fraction			:= M_US_FRACTION_PSI;
					END_CASE
				6:	MetricUsConversion.Out.Conv_mbar_or_psi.Scale		 			:= MetricUsConversion.Out.Value[i];	
					CASE MetricUsConversion.In.Mode OF
						0:	MetricUsConversion.Out.Conv_mbar_or_psi.Fraction		:= M_US_FRACTION_MILLIBAR;
						1:	MetricUsConversion.Out.Conv_mbar_or_psi.Fraction		:= M_US_FRACTION_PSI;
					END_CASE
				7:	MetricUsConversion.Out.Conv_kg_or_lb.Scale		 				:= MetricUsConversion.Out.Value[i];	
					CASE MetricUsConversion.In.Mode OF
						0:	MetricUsConversion.Out.Conv_kg_or_lb.Fraction			:= M_US_FRACTION_KG;
						1:	MetricUsConversion.Out.Conv_kg_or_lb.Fraction			:= M_US_FRACTION_LB;
					END_CASE
				8:	MetricUsConversion.Out.Conv_kg_hr_or_lb_hr.Scale	 			:= MetricUsConversion.Out.Value[i];	
					CASE MetricUsConversion.In.Mode OF
						0:	MetricUsConversion.Out.Conv_kg_hr_or_lb_hr.Fraction		:= M_US_FRACTION_KG;
						1:	MetricUsConversion.Out.Conv_kg_hr_or_lb_hr.Fraction		:= M_US_FRACTION_GALLON;
					END_CASE
				9:	MetricUsConversion.Out.Conv_l_or_gallon.Scale		 			:= MetricUsConversion.Out.Value[i];	
					CASE MetricUsConversion.In.Mode OF
						0:	MetricUsConversion.Out.Conv_l_or_gallon.Fraction		:= M_US_FRACTION_LITER;
						1:	MetricUsConversion.Out.Conv_l_or_gallon.Fraction		:= M_US_FRACTION_FAHRENHEIT;
					END_CASE
				10:	MetricUsConversion.Out.Conv_l_min_to_gallon_min.Scale			:= MetricUsConversion.Out.Value[i];	
					CASE MetricUsConversion.In.Mode OF
						0:	MetricUsConversion.Out.Conv_l_min_to_gallon_min.Fraction:= M_US_FRACTION_LITER;
						1:	MetricUsConversion.Out.Conv_l_min_to_gallon_min.Fraction:= 4;
					END_CASE
				11:	MetricUsConversion.Out.Conv_C_to_F.Scale				 		:= MetricUsConversion.Out.Value[i];	
					CASE MetricUsConversion.In.Mode OF
						0:	MetricUsConversion.Out.Conv_C_to_F.Fraction				:= M_US_FRACTION_CELSIUS;
						1:	MetricUsConversion.Out.Conv_C_to_F.Fraction				:= 1;
					END_CASE
			END_CASE
		END_FOR
		// Save mode in Last mode
		MetricUsConversion.Internal.LastMode := MetricUsConversion.In.Mode;
	END_IF
END_PROGRAM
