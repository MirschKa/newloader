 (001)  #VERSION,3.04.x,MT;

 (002)  |-#SD,TPL_VISIBLE,0,0,0,DYN_MenuPages.AllPages.In.Reload,0,0,900,5,5,0,0,0,0,0,0,0,128,-1;
 (003)  |-#SD,TPL_VISIBLE,0,0,0,DYN_MenuPages.ActivePage.In.Reload,0,0,901,5,5,0,0,0,0,0,0,0,128,-1;

//General Functions
 (004)#FH,DemoProjMaintVar[0].Available,1,0,0,DemoProjMaintVar[0].Expand,300,1,0,255;
//       Nummeric Input
 (005)  |-#HD,DemoProjMaintVar[0].Expand,1,0,0,500,0,255;
 (006)  |-#NI,DemoProjMaintVar[0].Expand,1,0,0,DemoProjMaintVar[0].NI.IN.TTIME,0,0,1,0,0,0,0,3,DemoProjMaintVar[0].NI.OUT.TTIME,0,0,0,255,-1;
 (007)  |-#NI,DemoProjMaintVar[0].Expand,1,0,0,DemoProjMaintVar[0].NI.IN.TREAL,0,0,2,0,0,0,0,12,DemoProjMaintVar[0].NI.OUT.TREAL,0,0,0,255,-1;
 (008)  |-#NI,DemoProjMaintVar[0].Expand,1,0,0,DemoProjMaintVar[0].NI.IN.TUDINT,0,0,3,0,0,0,0,1,DemoProjMaintVar[0].NI.OUT.TUDINT,0,0,0,255,-1;
 (009)  |-#NI,DemoProjMaintVar[0].Expand,1,0,0,DemoProjMaintVar[0].NI.IN.TDINT,0,0,4,0,0,0,0,13,DemoProjMaintVar[0].NI.OUT.TDINT,0,0,0,255,-1;
 (010)  |-#NI,DemoProjMaintVar[0].Expand,1,0,0,DemoProjMaintVar[0].NI.IN.TINT,0,0,5,0,0,0,0,5,DemoProjMaintVar[0].NI.OUT.TINT,0,0,0,255,-1;
 (011)  |-#NI,DemoProjMaintVar[0].Expand,1,0,0,DemoProjMaintVar[0].NI.IN.TUINT,0,0,6,0,0,0,0,7,DemoProjMaintVar[0].NI.OUT.TUINT,0,0,0,255,-1;
 (012)  |-#NI,DemoProjMaintVar[0].Expand,1,0,0,DemoProjMaintVar[0].NI.IN.TSINT,0,0,7,0,0,0,0,8,DemoProjMaintVar[0].NI.OUT.TSINT,0,0,0,255,-1;
 (013)  |-#NI,DemoProjMaintVar[0].Expand,1,0,0,DemoProjMaintVar[0].NI.IN.TUSINT,0,0,8,0,0,0,0,10,DemoProjMaintVar[0].NI.OUT.TUSINT,DemoProjMaintVar[0].NI.IN.TUSINT,DemoProjMaintVar[0].NI.OUT.TUSINT,0,255,-1;
 (014)  |-#NI,DemoProjMaintVar[0].Expand,1,0,0,DemoProjMaintVar[0].NI.IN.TBOOL,0,0,25,0,0,0,0,10,DemoProjMaintVar[0].NI.OUT.TBOOL,DemoProjMaintVar[0].NI.IN.TBOOL,DemoProjMaintVar[0].NI.OUT.TBOOL,0,255,-1;
//      Momentary
 (015)  |-#HD,DemoProjMaintVar[0].Expand,1,0,0,507,0,255;
 (016)  |-#SD,DemoProjMaintVar[0].Expand,1,0,0,DemoProjMaintVar[0].SD.IN.TBOOL,0,0,21,1,2,0,0,10,DemoProjMaintVar[0].SD.OUT.TBOOL,DemoProjMaintVar[0].SD.IN.TBOOL,DemoProjMaintVar[0].SD.OUT.TBOOL,0,255,-1;
 (017)  |-#SD,DemoProjMaintVar[0].Expand,1,0,0,DemoProjMaintVar[0].SD.IN.TUSINT,0,0,22,1,2,0,0,10,DemoProjMaintVar[0].SD.OUT.TUSINT,DemoProjMaintVar[0].SD.IN.TUSINT,DemoProjMaintVar[0].SD.OUT.TUSINT,0,255,-1;
//      Switch
 (018)  |-#HD,DemoProjMaintVar[0].Expand,1,0,0,508,0,255;
 (019)  |-#SW,DemoProjMaintVar[0].Expand,1,0,0,DemoProjMaintVar[0].SW.IN.TBOOL,0,0,23,1,2,0,0,10,DemoProjMaintVar[0].SW.OUT.TBOOL,DemoProjMaintVar[0].SW.IN.TBOOL,DemoProjMaintVar[0].SW.OUT.TBOOL,0,255,-1;
 (020)  |-#SW,DemoProjMaintVar[0].Expand,1,0,0,DemoProjMaintVar[0].SW.IN.TUSINT,0,0,24,1,2,0,0,10,DemoProjMaintVar[0].SW.OUT.TUSINT,DemoProjMaintVar[0].SW.IN.TUSINT,DemoProjMaintVar[0].SW.OUT.TUSINT,0,255,-1;

//Scaling
 (021)#FH,DemoProjMaintVar[1].Available,1,0,0,DemoProjMaintVar[1].Expand,301,1,0,255;
//       Nummeric Input
 (022)  |-#HD,DemoProjMaintVar[1].Expand,1,0,0,500,0,255;
 (023)  |-#NI,DemoProjMaintVar[1].Expand,1,0,0,DemoProjMaintVar[1].NI.IN.TUDINT,0,0,500,0,0,1,1,3,DemoProjMaintVar[1].NI.IN.TUDINT,0,0,0,255,-1;
 (024)  |-#NI,DemoProjMaintVar[1].Expand,1,0,0,DemoProjMaintVar[1].NI.IN.TUDINT,0,0,500,0,0,2,2,12,DemoProjMaintVar[1].NI.IN.TUDINT,0,0,0,255,-1;
 (025)  |-#NI,DemoProjMaintVar[1].Expand,1,0,0,DemoProjMaintVar[1].NI.IN.TUDINT,0,0,500,0,0,3,3,1,DemoProjMaintVar[1].NI.IN.TUDINT,0,0,0,255,-1;
 (026)  |-#NI,DemoProjMaintVar[1].Expand,1,0,0,DemoProjMaintVar[1].NI.IN.TUDINT,0,0,500,0,0,4,4,13,DemoProjMaintVar[1].NI.IN.TUDINT,0,0,0,255,-1;
 (027)  |-#NI,DemoProjMaintVar[1].Expand,1,0,0,DemoProjMaintVar[1].NI.IN.TUDINT,0,0,500,0,0,5,5,10,DemoProjMaintVar[1].NI.IN.TUDINT,DemoProjMaintVar[1].NI.IN.TUDINT,DemoProjMaintVar[1].NI.IN.TUDINT,0,255,-1;
 (028)  |-#NI,DemoProjMaintVar[1].Expand,1,0,0,DemoProjMaintVar[1].NI.IN.TUDINT,0,0,500,0,0,6,6,10,DemoProjMaintVar[1].NI.IN.TUDINT,DemoProjMaintVar[1].NI.IN.TUDINT,DemoProjMaintVar[1].NI.IN.TUDINT,0,255,-1;
//      Momentary
 (029)  |-#HD,DemoProjMaintVar[1].Expand,1,0,0,507,0,255;
 (030)  |-#SD,DemoProjMaintVar[1].Expand,1,0,0,DemoProjMaintVar[1].SD.IN.TBOOL,0,0,21,1,2,3,3,10,DemoProjMaintVar[1].SD.OUT.TBOOL,DemoProjMaintVar[1].SD.IN.TBOOL,DemoProjMaintVar[1].SD.OUT.TBOOL,0,255,-1;
 (031)  |-#SD,DemoProjMaintVar[1].Expand,1,0,0,DemoProjMaintVar[1].SD.IN.TUSINT,0,0,22,1,2,2,2,10,DemoProjMaintVar[1].SD.OUT.TUSINT,DemoProjMaintVar[1].SD.IN.TUSINT,DemoProjMaintVar[1].SD.OUT.TUSINT,0,255,-1;
//      Switch
 (032)  |-#HD,DemoProjMaintVar[1].Expand,1,0,0,508,0,255;
 (033)  |-#SW,DemoProjMaintVar[1].Expand,1,0,0,DemoProjMaintVar[1].SW.IN.TBOOL,0,0,23,1,2,3,3,10,DemoProjMaintVar[1].SW.OUT.TBOOL,DemoProjMaintVar[1].SW.IN.TBOOL,DemoProjMaintVar[1].SW.OUT.TBOOL,0,255,-1;
 (034)  |-#SW,DemoProjMaintVar[1].Expand,1,0,0,DemoProjMaintVar[1].SW.IN.TUSINT,0,0,24,1,2,2,2,10,DemoProjMaintVar[1].SW.OUT.TUSINT,DemoProjMaintVar[1].SW.IN.TUSINT,DemoProjMaintVar[1].SW.OUT.TUSINT,0,255,-1;



//Scaling
 (035)#FH,DemoProjMaintVar[2].Available,1,0,0,DemoProjMaintVar[2].Expand,302,1,0,255;
//           Nummeric Input
 (036)  |-#HD,DemoProjMaintVar[2].Expand,1,0,0,500,0,255;
 (037)  |-#NI,DemoProjMaintVar[2].Expand,1,0,0,DemoProjMaintVar[2].NI.IN.TUDINT,0,0,500,0,0,1,1,3,DemoProjMaintVar[2].NI.IN.TUDINT,0,0,0,255,240;
 (038)  |-#NI,DemoProjMaintVar[2].Expand,1,0,0,DemoProjMaintVar[2].NI.IN.TUDINT,0,0,500,0,0,2,2,12,DemoProjMaintVar[2].NI.IN.TUDINT,0,0,0,255,240;
 (039)  |-#NI,DemoProjMaintVar[2].Expand,1,0,0,DemoProjMaintVar[2].NI.IN.TUDINT,0,0,500,0,0,3,3,1,DemoProjMaintVar[2].NI.IN.TUDINT,0,0,0,255,240;
 (040)  |-#NI,DemoProjMaintVar[2].Expand,1,0,0,DemoProjMaintVar[2].NI.IN.TUDINT,0,0,500,0,0,4,4,13,DemoProjMaintVar[2].NI.IN.TUDINT,0,0,0,255,240;
 (041)  |-#NI,DemoProjMaintVar[2].Expand,1,0,0,DemoProjMaintVar[2].NI.IN.TUDINT,0,0,500,0,0,5,5,10,DemoProjMaintVar[2].NI.IN.TUDINT,DemoProjMaintVar[2].NI.IN.TUDINT,DemoProjMaintVar[2].NI.IN.TUDINT,0,255,224;
 (042)  |-#NI,DemoProjMaintVar[2].Expand,1,0,0,DemoProjMaintVar[2].NI.IN.TUDINT,0,0,500,0,0,6,6,10,DemoProjMaintVar[2].NI.IN.TUDINT,DemoProjMaintVar[2].NI.IN.TUDINT,DemoProjMaintVar[2].NI.IN.TUDINT,0,255,224;
//      Momentary
 (043)  |-#HD,DemoProjMaintVar[2].Expand,1,0,0,507,0,240;
 (044)  |-#SD,DemoProjMaintVar[2].Expand,1,0,0,DemoProjMaintVar[2].SD.IN.TBOOL,0,0,21,1,2,3,3,10,DemoProjMaintVar[2].SD.OUT.TBOOL,DemoProjMaintVar[2].SD.IN.TBOOL,DemoProjMaintVar[2].SD.OUT.TBOOL,0,240,240;
 (045)  |-#SD,DemoProjMaintVar[2].Expand,1,0,0,DemoProjMaintVar[2].SD.IN.TUSINT,0,0,22,1,2,2,2,10,DemoProjMaintVar[2].SD.OUT.TUSINT,DemoProjMaintVar[2].SD.IN.TUSINT,DemoProjMaintVar[2].SD.OUT.TUSINT,0,240,128;
//      Switch
 (046)  |-#HD,DemoProjMaintVar[2].Expand,1,0,0,508,0,240;
 (047)  |-#SW,DemoProjMaintVar[2].Expand,1,0,0,DemoProjMaintVar[2].SW.IN.TBOOL,0,0,23,1,2,3,3,10,DemoProjMaintVar[2].SW.OUT.TBOOL,DemoProjMaintVar[2].SW.IN.TBOOL,DemoProjMaintVar[2].SW.OUT.TBOOL,0,240,240;
 (048)  |-#SW,DemoProjMaintVar[2].Expand,1,0,0,DemoProjMaintVar[2].SW.IN.TUSINT,0,0,24,1,2,2,2,10,DemoProjMaintVar[2].SW.OUT.TUSINT,DemoProjMaintVar[2].SW.IN.TUSINT,DemoProjMaintVar[2].SW.OUT.TUSINT,0,240,128;

// Unit Index by variable
 (049)#FH,DemoProjMaintVar[3].Available,1,0,0,DemoProjMaintVar[3].Expand,303,1,0,255;
 (050)  |-#NI,DemoProjMaintVar[3].Expand,1,0,0,DemoProjMaintVar[3].NI.IN.TUSINT,0,0,500,0,0,0,0,DemoProjMaintVar[3].NI.IN.TUSINT,DemoProjMaintVar[3].NI.IN.TUSINT,DemoProjMaintVar[3].NI.IN.TUSINT,DemoProjMaintVar[3].NI.IN.TUSINT,0,255,-1;
 (051)  |-#SD,DemoProjMaintVar[3].Expand,1,0,0,DemoProjMaintVar[3].SD.IN.TUSINT,0,0,507,1,2,0,0,DemoProjMaintVar[3].NI.IN.TUSINT,DemoProjMaintVar[3].NI.IN.TUSINT,DemoProjMaintVar[3].NI.IN.TUSINT,DemoProjMaintVar[3].NI.IN.TUSINT,0,255,-1;
 (052)  |-#SW,DemoProjMaintVar[3].Expand,1,0,0,DemoProjMaintVar[3].SW.IN.TUSINT,0,0,508,1,2,0,0,DemoProjMaintVar[3].NI.IN.TUSINT,DemoProjMaintVar[3].NI.IN.TUSINT,DemoProjMaintVar[3].NI.IN.TUSINT,DemoProjMaintVar[3].NI.IN.TUSINT,0,255,-1;

// Scaling by variable
 (053)#FH,DemoProjMaintVar[4].Available,1,0,0,DemoProjMaintVar[4].Expand,304,1,0,255;
 (054)  |-#NI,DemoProjMaintVar[4].Expand,1,0,0,DemoProjMaintVar[4].SW.IN.TUSINT,0,0,500,0,0,0,0,0,0,0,0,0,255,-1;
 (055)  |-#NI,DemoProjMaintVar[4].Expand,1,0,0,DemoProjMaintVar[4].NI.IN.TUSINT,0,0,500,0,0,DemoProjMaintVar[4].SW.IN.TUSINT,2,DemoProjMaintVar[4].NI.IN.TUSINT,DemoProjMaintVar[4].NI.IN.TUSINT,DemoProjMaintVar[4].NI.IN.TUSINT,DemoProjMaintVar[4].NI.IN.TUSINT,0,255,-1;
 (056)  |-#SD,DemoProjMaintVar[4].Expand,1,0,0,DemoProjMaintVar[4].SD.IN.TUSINT,0,0,507,1,2,DemoProjMaintVar[4].SW.IN.TUSINT,3,DemoProjMaintVar[4].NI.IN.TUSINT,DemoProjMaintVar[4].NI.IN.TUSINT,DemoProjMaintVar[4].NI.IN.TUSINT,DemoProjMaintVar[4].NI.IN.TUSINT,0,255,-1;
 (057)  |-#SW,DemoProjMaintVar[4].Expand,1,0,0,DemoProjMaintVar[4].SW.IN.TUSINT,0,0,508,1,2,DemoProjMaintVar[4].SW.IN.TUSINT,4,DemoProjMaintVar[4].NI.IN.TUSINT,DemoProjMaintVar[4].NI.IN.TUSINT,DemoProjMaintVar[4].NI.IN.TUSINT,DemoProjMaintVar[4].NI.IN.TUSINT,0,255,-1;

// Text index by variable
 (058)#FH,DemoProjMaintVar[5].Available,1,0,0,DemoProjMaintVar[5].Expand,305,1,0,255;
 (059)  |-#NI,DemoProjMaintVar[5].Expand,1,0,0,DemoProjMaintVar[5].SW.IN.TUSINT,0,0,DemoProjMaintVar[5].SW.IN.TUSINT,0,0,0,0,0,0,0,0,0,255,-1;
 (060)  |-#HD,DemoProjMaintVar[5].Expand,1,0,0,DemoProjMaintVar[5].SW.IN.TUSINT,0,255;
 (061)  |-#NI,DemoProjMaintVar[5].Expand,1,0,0,DemoProjMaintVar[5].NI.IN.TUSINT,0,0,DemoProjMaintVar[5].SW.IN.TUSINT,0,0,0,0,DemoProjMaintVar[5].NI.IN.TUSINT,DemoProjMaintVar[5].NI.IN.TUSINT,DemoProjMaintVar[5].NI.IN.TUSINT,DemoProjMaintVar[5].NI.IN.TUSINT,0,255,-1;
 (062)  |-#SD,DemoProjMaintVar[5].Expand,1,0,0,DemoProjMaintVar[5].SD.IN.TUSINT,0,0,DemoProjMaintVar[5].SW.IN.TUSINT,1,2,0,0,DemoProjMaintVar[5].NI.IN.TUSINT,DemoProjMaintVar[5].NI.IN.TUSINT,DemoProjMaintVar[5].NI.IN.TUSINT,DemoProjMaintVar[5].NI.IN.TUSINT,0,255,-1;
 (063)  |-#SW,DemoProjMaintVar[5].Expand,1,0,0,DemoProjMaintVar[5].SW.IN.TUSINT,0,0,DemoProjMaintVar[5].SW.IN.TUSINT,1,2,0,0,DemoProjMaintVar[5].NI.IN.TUSINT,DemoProjMaintVar[5].NI.IN.TUSINT,DemoProjMaintVar[5].NI.IN.TUSINT,DemoProjMaintVar[5].NI.IN.TUSINT,0,255,-1;


#END;
