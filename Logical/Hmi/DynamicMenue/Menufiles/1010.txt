 (001)  #VERSION,3.04.x,PD;
 (002)  |-#CB,TPL_VISIBLE,0,0,0,DYN_MenuPages.AllPages.In.Reload,0,900,0,128;
 (003)  |-#CB,TPL_VISIBLE,0,0,0,DYN_MenuPages.ActivePage.In.Reload,0,901,0,128;

    //Nummeric Input
 (004)  |-#HD,DemoProjProdVar[0].Available,1,0,0,500,0,255;
 (005)  |-#NI,DemoProjProdVar[0].Available,1,0,0,DemoProjProdVar[0].NI.TTIME,3,1,0,0,0,0,0,0,0,0,255;
 (006)  |-#NI,DemoProjProdVar[0].Available,1,0,0,DemoProjProdVar[0].NI.TREAL,12,2,0,0,0,0,0,0,0,0,255;
 (007)  |-#NI,DemoProjProdVar[0].Available,1,0,0,DemoProjProdVar[0].NI.TUDINT,1,3,0,0,0,0,0,0,0,0,255;
 (008)  |-#NI,DemoProjProdVar[0].Available,1,0,0,DemoProjProdVar[0].NI.TDINT,13,4,0,0,0,0,0,0,0,0,255;
 (009)  |-#NI,DemoProjProdVar[0].Available,1,0,0,DemoProjProdVar[0].NI.TINT,5,5,0,0,0,0,0,0,0,0,255;
 (010)  |-#NI,DemoProjProdVar[0].Available,1,0,0,DemoProjProdVar[0].NI.TUINT,7,6,0,0,0,0,0,0,0,0,255;
 (011)  |-#NI,DemoProjProdVar[0].Available,1,0,0,DemoProjProdVar[0].NI.TSINT,8,7,0,0,0,0,0,0,0,0,255;
 (012)  |-#NI,DemoProjProdVar[0].Available,1,0,0,DemoProjProdVar[0].NI.TUSINT,10,8,0,0,0,0,0,0,0,0,255;

    //Nummeric Output
 (013)  |-#HD,DemoProjProdVar[0].Available,1,0,0,501,0,255;
 (014)  |-#NO,DemoProjProdVar[0].Available,1,0,0,DemoProjProdVar[0].NO.TTIME,3,9,0,0,0,255;
 (015)  |-#NO,DemoProjProdVar[0].Available,1,0,0,DemoProjProdVar[0].NO.TREAL,12,10,0,0,0,255;
 (016)  |-#NO,DemoProjProdVar[0].Available,1,0,0,DemoProjProdVar[0].NO.TUDINT,1,11,0,0,0,255;
 (017)  |-#NO,DemoProjProdVar[0].Available,1,0,0,DemoProjProdVar[0].NO.TDINT,13,12,0,0,0,255;
 (018)  |-#NO,DemoProjProdVar[0].Available,1,0,0,DemoProjProdVar[0].NO.TINT,5,13,0,0,0,255;
 (019)  |-#NO,DemoProjProdVar[0].Available,1,0,0,DemoProjProdVar[0].NO.TUINT,7,14,0,0,0,255;
 (020)  |-#NO,DemoProjProdVar[0].Available,1,0,0,DemoProjProdVar[0].NO.TSINT,8,15,0,0,0,255;
 (021)  |-#NO,DemoProjProdVar[0].Available,1,0,0,DemoProjProdVar[0].NO.TUSINT,10,16,0,0,0,255;

    //Alphanummeric Input
 (022)  |-#HD,DemoProjProdVar[0].Available,1,0,0,509,0,255;
 (023)  |-#AI,DemoProjProdVar[0].Available,1,0,0,DemoProjProdVar[0].AI.TSTRING,0,100,0,255;

    //Alphanummeric Output
 (024)  |-#HD,DemoProjProdVar[0].Available,1,0,0,510,0,255;
 (025)  |-#AO,DemoProjProdVar[0].Available,1,0,0,0,0,31,0,255;
 (026)  |-#AO,DemoProjProdVar[0].Available,1,0,0,DemoProjProdVar[0].AI.TSTRING,0,32,0,255;
 (027)  |-#AO,DemoProjProdVar[0].Available,1,0,0,DemoProjProdVar[0].AI.TUINT,DemoProjProdVar[0].AI.TUINT,33,0,255;
 (028)  |-#AO,DemoProjProdVar[0].Available,1,0,0,DemoProjProdVar[0].AI.TUSINT,DemoProjProdVar[0].AI.TUSINT,34,0,255;

   //Momentary
 (029)  |-#HD,DemoProjProdVar[0].Available,1,0,0,507,0,255;
 (030)  |-#SD,DemoProjProdVar[0].Available,1,0,0,DemoProjProdVar[0].SD.TBOOL,0,21,1,2,0,255;
 (031)  |-#SD,DemoProjProdVar[0].Available,1,0,0,DemoProjProdVar[0].SD.TUSINT,0,22,1,2,0,255;

   //Switch
 (032)  |-#HD,DemoProjProdVar[0].Available,1,0,0,508,0,255;
 (033)  |-#SW,DemoProjProdVar[0].Available,1,0,0,DemoProjProdVar[0].SW.TBOOL,0,23,1,2,0,255;
 (034)  |-#SW,DemoProjProdVar[0].Available,1,0,0,DemoProjProdVar[0].SW.TUSINT,0,24,1,2,0,255;

   //Checkbox
 (035)  |-#HD,DemoProjProdVar[0].Available,1,0,0,506,0,255;
 (036)  |-#CB,DemoProjProdVar[0].Available,1,0,0,DemoProjProdVar[0].CB.TBOOL,0,17,0,255;
 (037)  |-#CB,DemoProjProdVar[0].Available,1,0,0,DemoProjProdVar[0].CB.TUSINT,0,18,0,255;

   //Outbox
 (038)  |-#HD,DemoProjProdVar[0].Available,1,0,0,505,0,255;
 (039)  |-#OB,DemoProjProdVar[0].Available,1,0,0,DemoProjProdVar[0].OB.TBOOL,0,19,0,255;
 (040)  |-#OB,DemoProjProdVar[0].Available,1,0,0,DemoProjProdVar[0].OB.TUSINT,0,20,0,255;

    //Disabled View Test
 (041)  |-#HD,DemoProjProdVar[9].Available,1,0,0,310,0,255;
 (042)  |-#NI,DemoProjProdVar[9].Available,1,0,0,DemoProjProdVar[0].NI.TTIME,3,1,0,0,0,0,0,0,0,0,255;
 (043)  |-#AI,DemoProjProdVar[9].Available,1,0,0,DemoProjProdVar[0].AI.TSTRING,0,100,0,255;
 (044)  |-#SD,DemoProjProdVar[9].Available,1,0,0,DemoProjProdVar[0].SD.TBOOL,0,21,1,2,0,255;
 (045)  |-#SW,DemoProjProdVar[9].Available,1,0,0,DemoProjProdVar[0].SW.TBOOL,0,23,1,2,0,255;
 (046)  |-#CB,DemoProjProdVar[9].Available,1,0,0,DemoProjProdVar[0].CB.TBOOL,0,17,0,255;
 (047)  |-#UD,DemoProjProdVar[9].Available,1,0,0,DemoProjProdVar[0].NI.TTIME,3,1,0,0,0,0,0,0,1,0,0,0,0,255;

#END;
