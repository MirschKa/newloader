(************************************************************************************************************************
 * COPYRIGHT -- GEA FS
 ************************************************************************************************************************
 * Program: LoadAction.st
 * File: LoadAction
 * Author: Erdal Cilingir
 * Created: 20-SEP-2013
 ************************************************************************************************************************
 * Action for Reading the Maintanance Pages
 ************************************************************************************************************************)

ACTION LoadAction: 
	// ******************************************************************************************************************
	// 												READ PAGE 3310
	// ******************************************************************************************************************


	FOR i:=0 TO CONFIG_NR_PAGES DO
		// Is asked for to Load the File?
		IF  DYN_Menu[i].In.Reload OR Cmd_Reload_All OR DYN_MenuPages.AllPages.In.Reload THEN
			DYN_Menu[i].Out.Step:=READ_PAGE_INIT;
			DYN_Menu[i].Out.InitOk :=0;
			DYN_Menu[i].In.Reload:=0;
		END_IF
			
		// Case for Reading the File 
		CASE DYN_Menu[i].Out.Step OF
			// Init Step
			READ_PAGE_INIT:	
				brsmemset(ADR(DYN_Menu[i].Entry),0,SIZEOF(DYN_Menu[i].Entry));
				brsmemset(ADR(DYN_Menu[i].Internal),0,SIZEOF(DYN_Menu[i].Internal));
				DYN_Menu[i].Out.Step := READ_PAGE;
			// Read the Page Information and evaluate the Feedback Info
			READ_PAGE:	
				
				brsitoa(UINT_TO_DINT(DYN_Menu[i].Config.PageNr),ADR(TmpString)); 
				initmenu(ADR('MENUFILES'), ADR(TmpString), ADR(DYN_Menu[i]));	
				IF (DYN_Menu[i].Out.Status <> FUNCTION_BUSY) THEN
					IF (DYN_Menu[i].Out.Status = 0  )THEN
						AlarmDynMenu[i]:=FALSE;
						DYN_Menu[i].Out.Step := READ_PAGE_EMPTY_STEP;
					ELSIF DYN_Menu[i].Out.Status   = CMD_RELOAD THEN
						DYN_Menu[i].Out.Step := READ_PAGE_INIT;	
					ELSE
						AlarmDynMenu[i]:=TRUE;
					END_IF
				END_IF
			// Step after successful read of the File
			READ_PAGE_EMPTY_STEP:			
				DYN_Menu[i].Out.InitOk :=1;
			// Reset the variable, if other Step
			ELSE
				brsmemset(ADR(DYN_Menu[i].Entry),0,SIZEOF(DYN_Menu[i].Entry));
		END_CASE
		
		// Stop the Task, if the Libiary is allocating memory
		IF  (DYN_Menu[i].Out.InitStep>0 AND DYN_Menu[i].Out.InitStep<=3) THEN RETURN; END_IF
	END_FOR
END_ACTION