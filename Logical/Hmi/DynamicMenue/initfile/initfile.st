(************************************************************************************************************************
 * COPYRIGHT -- GEA FS
 ************************************************************************************************************************
 * Program: init_men
 * File: init_men.st
 * Author: Erdal Cilingir
 * Created: 20-SEP-2013
 ************************************************************************************************************************
 * Implementation of program init_men
 * This task shows an example for using the dynamic menu
 * In this task the Page Infomrations are readed and the example Variable are setted
//***********************************************************************************************************************)
// Version 1.00.1  22-aug-2014 E.Cilingir                       Development tool: B&R Automation Studio V3.0.90.28 SP10 *)
// [#441] New variable with DYN_MenuPages for general in commands to the pages											*)
//		  link general commands for DisableShowLogic & DisableUserLevel to the pages									*)
// [#418] Direct jump to page inside the Dynamic Menu																	*)
//		  Add In/Out structure to DYN_MenuPages for new functions														*)
//***********************************************************************************************************************)
// Version 1.00.2  04-sep-2015 E.Cilingir                       Development tool: B&R Automation Studio V3.0.90.30 SP12 *)
// [#471] Do Filedevice creation programmatically   																	*)
//		  Create File Device over the function CreateFileDevice 														*)
//***********************************************************************************************************************)
// Version 1.00.3  17-june-2019 D. Benner                       Development tool: B&R Automation Studio V4.5.2.102		*)
// 		Added page settings for 5.7"																					*)
(************************************************************************************************************************)

PROGRAM _INIT
	// Set the Command to read all File Informations
	Cmd_Reload_All := 1;
    CreateFileDevice(TPL_Config.Out.PanelType,TPL_SIM,'MENUFILES','MENU\');

	// Coniguration of the first example page
	DYN_Menu[0].Config.PageNr:=1010;
//	DYN_Menu[0].Config.EntriesPerPage:=Entries_16;
	DYN_Menu[0].Config.EntriesPerPage:=Entries_12;
//	DYN_Menu[0].Config.EntriesPerPage:=Entries_6;
	//Configuration of the second example page
	DYN_Menu[1].Config.PageNr:=1011;
//	DYN_Menu[1].Config.EntriesPerPage:=Entries_16;
	DYN_Menu[1].Config.EntriesPerPage:=Entries_10;	
//	DYN_Menu[1].Config.EntriesPerPage:=Entries_6;	
	//Configuration of the third example page
	DYN_Menu[2].Config.PageNr:=3310;
//	DYN_Menu[2].Config.EntriesPerPage:=Entries_16;
//	DYN_Menu[2].Config.GrpBoxNoHeightElements:=Entries_16;
	DYN_Menu[2].Config.EntriesPerPage:=Entries_12;
	DYN_Menu[2].Config.GrpBoxNoHeightElements:=Entries_12;
//	DYN_Menu[2].Config.EntriesPerPage:=Entries_6;
//	DYN_Menu[2].Config.GrpBoxNoHeightElements:=Entries_6;
	
	

	// *************** DEMO PROJECT VARIABLE CAN BE DELETED ***********************************
				// Set available to true of the Example Variable							//*
				DemoProjProdVar[0].Available:=1;											//*
				DemoProjProdVar[1].Available:=1;											//*
				DemoProjProdVar[2].Available:=1;											//*
				DemoProjProdVar[3].Available:=1;											//*
				DemoProjProdVar[4].Available:=1;											//*
				DemoProjProdVar[5].Available:=1;											//*
				DemoProjProdVar[6].Available:=1;											//*
				DemoProjProdVar[7].Available:=1;											//*
				DemoProjProdVar[8].Available:=1;											//*
				DemoProjMaintVar[0].Available:=1;											//*
				DemoProjMaintVar[1].Available:=1;											//*
				DemoProjMaintVar[2].Available:=1;											//*
				DemoProjMaintVar[3].Available:=1;											//*
				DemoProjMaintVar[4].Available:=1;											//*
				DemoProjMaintVar[5].Available:=1;											//*
				DemoProjMaintVar[6].Available:=1;											//*
				DemoProjMaintVar[7].Available:=1;											//*
				DemoProjMaintVar[8].Available:=1;											//*
				DemoProjMaintVar[9].Available:=1;											//*
	//*****************************************************************************************
END_PROGRAM


PROGRAM _CYCLIC	
	// Is VC Handle active?
	IF  (TPL_Pageh.Out.VC_Handle=0) THEN RETURN; END_IF

	// ************************************** DEMO PROJECT VARIABLE CAN BE DELETED ****************************************************
			// Copy the in Informations of example maintanance variable to the out structure										//*
			brsmemcpy(ADR(DemoProjMaintVar[0].NI.OUT),ADR(DemoProjMaintVar[0].NI.IN),SIZEOF(DemoProjMaintVar[0].NI.IN));			//*
			brsmemcpy(ADR(DemoProjMaintVar[0].SD.OUT),ADR(DemoProjMaintVar[0].SD.IN),SIZEOF(DemoProjMaintVar[0].SD.IN));			//*
			brsmemcpy(ADR(DemoProjMaintVar[0].SW.OUT),ADR(DemoProjMaintVar[0].SW.IN),SIZEOF(DemoProjMaintVar[0].SW.IN));			//*
			brsmemcpy(ADR(DemoProjMaintVar[1].NI.OUT),ADR(DemoProjMaintVar[1].NI.IN),SIZEOF(DemoProjMaintVar[1].NI.IN));			//*
			brsmemcpy(ADR(DemoProjMaintVar[1].SD.OUT),ADR(DemoProjMaintVar[1].SD.IN),SIZEOF(DemoProjMaintVar[1].SD.IN));			//*
			brsmemcpy(ADR(DemoProjMaintVar[1].SW.OUT),ADR(DemoProjMaintVar[1].SW.IN),SIZEOF(DemoProjMaintVar[1].SW.IN));			//*
			brsmemcpy(ADR(DemoProjMaintVar[2].NI.OUT),ADR(DemoProjMaintVar[2].NI.IN),SIZEOF(DemoProjMaintVar[2].NI.IN));			//*
			brsmemcpy(ADR(DemoProjMaintVar[2].SD.OUT),ADR(DemoProjMaintVar[2].SD.IN),SIZEOF(DemoProjMaintVar[2].SD.IN));			//*
			brsmemcpy(ADR(DemoProjMaintVar[2].SW.OUT),ADR(DemoProjMaintVar[2].SW.IN),SIZEOF(DemoProjMaintVar[2].SW.IN));			//*
      IF TPL_Password.Out.UserLevel >= 4 THEN
        DemoProjProdVar[9].Available   := 1;
      ELSE
        DemoProjProdVar[9].Available  :=  2;
      END_IF
	//*********************************************************************************************************************************

	//Reset the Reaload all variable after 1 second
	ResetCmdTimer( IN:=Cmd_Reload_All OR DYN_MenuPages.AllPages.In.Reload, PT:=T#1s);
	IF ResetCmdTimer.Q THEN
		Cmd_Reload_All:=0;
		DYN_MenuPages.AllPages.In.Reload:=0;
	END_IF
	
	//Action to Load Demo Project Files
	LoadAction;

END_PROGRAM
