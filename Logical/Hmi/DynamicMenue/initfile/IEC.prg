﻿<?xml version="1.0" encoding="utf-8"?>
<?AutomationStudio Version=4.8.1.174?>
<Program Version="1.00.2" SubType="IEC" xmlns="http://br-automation.co.at/AS/Program">
  <Files>
    <File Description="Reading the configured file informations">initfile.st</File>
    <File Description="Local data types" Private="true">initfile.typ</File>
    <File Description="Local variables" Private="true">initfile.var</File>
    <File Description="Action for loading the files">LoadAction.st</File>
  </Files>
</Program>