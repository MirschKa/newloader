(********************************************************************
 * COPYRIGHT -- CFS
 ********************************************************************
 * Package: DynamicMenue
 * File: trueGlobal.typ
 * Author: 
 * Created: May 02, 2012
 ********************************************************************
 * Data types of package DynamicMenue
 ********************************************************************)
(*Menue*)

TYPE
	DynMenuElementType : 	STRUCT 
		TBOOL : BOOL;
		TTIME : TIME;
		TREAL : REAL;
		TUDINT : UDINT;
		TDINT : DINT;
		TINT : INT;
		TUINT : UINT;
		TSINT : SINT;
		TUSINT : USINT;
		TSTRING : STRING[30];
	END_STRUCT;
	DynMenuFunctionType : 	STRUCT 
		Disabled : UINT;
		Available : UINT;
		ON_OFF : BOOL;
		Expand : UINT;
		Parameter : DynMenuElementType;
		NI : DynMenuElementType;
		NO : DynMenuElementType;
		CB : DynMenuElementType;
		OB : DynMenuElementType;
		SD : DynMenuElementType;
		SW : DynMenuElementType;
		AI : DynMenuElementType;
		AO : DynMenuElementType;
		UD : DynMenuElementType;
		IP : DynMenuElementType;
		OP : DynMenuElementType;
	END_STRUCT;
	DynMenuMaintType : 	STRUCT 
		Available : UINT;
		Expand : UINT;
		NI : DynMenuMaintInOutType;
		SD : DynMenuMaintInOutType;
		SW : DynMenuMaintInOutType;
	END_STRUCT;
	DynMenuMaintInOutType : 	STRUCT 
		IN : DynMenuElementType;
		OUT : DynMenuElementType;
	END_STRUCT;
	Example_Conversion_Type : 	STRUCT 
		Value : ARRAY[0..MAX_CONVERSION]OF REAL;
	END_STRUCT;
	Metric_US_Conversion_Type_In : 	STRUCT 
		Mode : UINT;
	END_STRUCT;
	Metric_US_Conversion_Type_Out_Co : 	STRUCT 
		Scale : UINT;
		Fraction : UINT;
	END_STRUCT;
	Metric_US_Conversion_Type_Out : 	STRUCT 
		Value : ARRAY[0..MAX_CONVERSION]OF UINT;
		Conv_m_or_ft : Metric_US_Conversion_Type_Out_Co;
		Conv_mm_or_in : Metric_US_Conversion_Type_Out_Co;
		Conv_m_s_or_ft_s : Metric_US_Conversion_Type_Out_Co;
		Conv_m_min_or_ft_min : Metric_US_Conversion_Type_Out_Co;
		Conv_mm_s_or_in_s : Metric_US_Conversion_Type_Out_Co;
		Conv_bar_or_psi : Metric_US_Conversion_Type_Out_Co;
		Conv_mbar_or_psi : Metric_US_Conversion_Type_Out_Co;
		Conv_kg_or_lb : Metric_US_Conversion_Type_Out_Co;
		Conv_kg_hr_or_lb_hr : Metric_US_Conversion_Type_Out_Co;
		Conv_l_or_gallon : Metric_US_Conversion_Type_Out_Co;
		Conv_l_min_to_gallon_min : Metric_US_Conversion_Type_Out_Co;
		Conv_C_to_F : Metric_US_Conversion_Type_Out_Co;
	END_STRUCT;
	Metric_US_Conversion_Type_Inter : 	STRUCT 
		LastMode : UINT;
		Expand : ARRAY[0..MAX_CONVERSION]OF UINT;
	END_STRUCT;
	Metric_US_Conversion_Type : 	STRUCT 
		In : Metric_US_Conversion_Type_In;
		Out : Metric_US_Conversion_Type_Out;
		Internal : Metric_US_Conversion_Type_Inter;
	END_STRUCT;
END_TYPE
