(************************************************************************************************************************
 * COPYRIGHT -- GEA FS
 ************************************************************************************************************************
 * Program: Menu
 * File: menuh.st
 * Author: Erdal Cilingir
 * Created: 20-SEP-2013
 ************************************************************************************************************************
 * Implementation of program menuh
 * This task is evaluating, if a dynmaic Mneu page is selected and then linking the right page Info to the dynamic Menu
//***********************************************************************************************************************)
// Version 1.00.1  22-aug-2014 E.Cilingir                       Development tool: B&R Automation Studio V3.0.90.28 SP10 *)
// [#441] New variable with DYN_MenuPages for general in commands to the pages											*)
//		  link general commands for DisableShowLogic & DisableUserLevel to the pages									*)
// [#418] Direct jump to page inside the Dynamic Menu																	*)
//		  Add In/Out structure to DYN_MenuPages for new functions														*)
(************************************************************************************************************************)
(* Version 3.04.1  20-apr-2015  SO                              Development tool: B&R Automation Studio V3.0.90.28 SP10 *)
(* [#461] HMI flashing on Dynamic Menu page																				*)
(*		  In runmenu() the structure "VisMenu" first becomes erased and after this new calculated. If the visualization	*)
(*		  will be calculated exactly at the moment when runemenu will be calculated, a flashing on dynamic menu pages	*)
(*		  could be visible, because of elements in VisMenu are not yet calculated. Now the buffer "VisMenuTemp" will be	*)
(*		  used.																											*)
(************************************************************************************************************************)
(* Version 3.04.2  07-may-2019  DoB                             Development tool: B&R Automation Studio V4.5.2.102		*)
(* [#530] Performance increase of the loading process of a DynMenu page													*)
(* 		  Page query (is page a dyn page y/n?) modified so that the current page and (in addition) the target page 		*)
(*		  will trigger a load of the DynMenu page elements																*)
(************************************************************************************************************************)

PROGRAM _INIT

END_PROGRAM

PROGRAM _CYCLIC
	
	//Copy the Page Information to the local struct
	Page.Level:=TPL_Password.Out.UserLevel;
	Page.ActualLanguage:=TPL_System.In.Cfg.ActualLanguage;
	
	// Check for All Pages commands and copy the command to all several pages
	IF EDGEPOS(DYN_MenuPages.AllPages.In.DisableShowLogic) THEN
		FOR i:=0 TO CONFIG_NR_PAGES DO	
			DYN_Menu[i].In.DisableShowLogic := TRUE;
		END_FOR
	END_IF
	IF EDGEPOS(DYN_MenuPages.AllPages.In.DisableUserLevel) THEN
		FOR i:=0 TO CONFIG_NR_PAGES DO	
			DYN_Menu[i].In.DisableUserLevel := TRUE;
		END_FOR
	END_IF	
	IF EDGENEG(DYN_MenuPages.AllPages.In.DisableShowLogic) THEN
		FOR i:=0 TO CONFIG_NR_PAGES DO	
			DYN_Menu[i].In.DisableShowLogic := FALSE;
		END_FOR
	END_IF
	IF EDGENEG(DYN_MenuPages.AllPages.In.DisableUserLevel) THEN
		FOR i:=0 TO CONFIG_NR_PAGES DO	
			DYN_Menu[i].In.DisableUserLevel := FALSE;
		END_FOR
	END_IF	
	
	//*****************************************************************************************
	//		Access right dynamic menu informations to active page
	//*****************************************************************************************
	FOR i:=0 TO CONFIG_NR_PAGES DO	
		IF 	DYN_Menu[i].Config.PageNr = TPL_Pageh.Out.ActualPage OR DYN_Menu[i].Config.PageNr = TPL_Pageh.In.SwitchToPage THEN
			MenuEntry ACCESS ADR(DYN_Menu[i]);
			VisMenuTemp := VisMenu; // See [#461]
			StatusRun := runmenu(ADR(MenuEntry), ADR(VisMenuTemp), ADR(Page));
			VisMenu := VisMenuTemp; // See [#461]
			// Copy the information's to active page out structre
			DYN_MenuPages.ActivePage.Out.Config := MenuEntry.Config;
			DYN_MenuPages.ActivePage.Out.Init := MenuEntry.Out;
			EXIT;
		ELSIF (i = CONFIG_NR_PAGES) THEN
			brsmemset(ADR(VisMenu),0,SIZEOF(VisMenu));
			brsmemset(ADR(DYN_MenuPages),0,SIZEOF(DYN_MenuPages));
		END_IF
	END_FOR
	
	//*****************************************************************************************
	//		Creating the Subtitle Infomration and checking for pagechange informations
	//*****************************************************************************************
	IF (TPL_Pageh.Out.ActualPage <> pageOld) THEN
		pageOld := TPL_Pageh.Out.ActualPage;
		Page.ResetStructure := 1;
		Page.CurrentPage := 0;
	END_IF

	(* enable page change*)
	IF (Page.OneMorePage = TRUE) THEN
		DisableNextPage := 0;
	ELSE
		DisableNextPage := 1;
	END_IF
	IF (Page.CurrentPage > 0)AND (Page.CurrentPage < 65535 ) THEN
		DisablePreviousPage := 0;
	ELSE
		DisablePreviousPage := 1;
	END_IF

	(* execute page change*)
	IF ((PreviousPage) = TRUE) THEN
		Page.ResetStructure := 1;
		Page.CurrentPage := Page.CurrentPage - 1;
		PreviousPage := 0;
	ELSIF ((NextPage) = TRUE) THEN
		Page.ResetStructure := 1;
		Page.CurrentPage := Page.CurrentPage + 1;
		NextPage := 0;
	ELSE
		PreviousPage := 0;
		NextPage := 0;
	END_IF

	(* Check page number*)
	IF	(Page.CurrentPage >= 60000) THEN
		Page.ResetStructure := 1;
		Page.CurrentPage := 0;
	ELSIF (Page.CurrentPage >= Page.NumberOfPages) THEN
		Page.ResetStructure := 1;
		IF (Page.CurrentPage>0) THEN
			Page.CurrentPage := Page.NumberOfPages - 1;
		END_IF
	END_IF

	Page.CurrentPageShow :=	Page.CurrentPage +1;
	IF Page.NumberOfPages>0 THEN
		Page.NumberOfPagesShow :=  Page.NumberOfPages;
	ELSE
		Page.NumberOfPagesShow :=  1;
	END_IF
		
	(*show sub-pages*)
	IF (Page.NumberOfPages > 1) THEN
		subPageDisable := FALSE;
	ELSE
		subPageDisable := TRUE;
	END_IF
	
	//*****************************************************************************************
	//		Creating the Information for DYN_MenuPages
	//*****************************************************************************************
	IF DYN_MenuPages.ActivePage.Out.Config.PageNr <> 0 THEN
		// Check if page opend and copy in comands of page to active page
		IF DYN_MenuPages.ActivePage.Out.Config.PageNr <> LastPage THEN
			DYN_MenuPages.ActivePage.In.DisableShowLogic 	:= 	MenuEntry.In.DisableShowLogic;
			DYN_MenuPages.ActivePage.In.DisableUserLevel 	:= 	MenuEntry.In.DisableUserLevel;
			LastPage										:=	DYN_MenuPages.ActivePage.Out.Config.PageNr;
		END_IF
		// Check for pagechange inside dynmaic menu
		IF DYN_MenuPages.ActivePage.In.ChangePageTo > 0  THEN
			Page.CurrentPage 							:= 	DYN_MenuPages.ActivePage.In.ChangePageTo-1;
			DYN_MenuPages.ActivePage.In.ChangePageTo	:=	0;
		END_IF
		// check for reloadf command of the actual page
		IF DYN_MenuPages.ActivePage.In.Reload > 0  THEN
			MenuEntry.In.Reload							:= 	TRUE;
			DYN_MenuPages.ActivePage.In.Reload			:=	0;
		END_IF	
		// Check if page command change, then copy to active page command
		IF EDGEPOS(MenuEntry.In.DisableShowLogic) THEN
			DYN_MenuPages.ActivePage.In.DisableShowLogic	:=	TRUE;
		END_IF
		IF EDGEPOS(MenuEntry.In.DisableUserLevel) THEN
			DYN_MenuPages.ActivePage.In.DisableUserLevel	:=	TRUE;
		END_IF	
		IF EDGENEG(MenuEntry.In.DisableShowLogic) THEN
			DYN_MenuPages.ActivePage.In.DisableShowLogic	:=	FALSE;
		END_IF
		IF EDGENEG(MenuEntry.In.DisableUserLevel) THEN
			DYN_MenuPages.ActivePage.In.DisableUserLevel	:=	FALSE;
		END_IF		
		// Copy informations from in of active page to in structre of menu entry
		MenuEntry.In.DisableShowLogic	:= DYN_MenuPages.ActivePage.In.DisableShowLogic;
		MenuEntry.In.DisableUserLevel	:= DYN_MenuPages.ActivePage.In.DisableUserLevel;
		// Copy Page numer informations to out structre of active page
		DYN_MenuPages.ActivePage.Out.ActualPage 		:= 	Page.CurrentPageShow;
		DYN_MenuPages.ActivePage.Out.AvailablePages 	:= 	Page.NumberOfPagesShow;
	ELSE
		// Reset all variables
		LastPage:=0;
		brsmemset(ADR(DYN_MenuPages),0,SIZEOF(DYN_MenuPages));
	END_IF

	
END_PROGRAM
