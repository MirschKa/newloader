
#ifndef  _MNGGLOBAL_HPP_
#define _MNGGLOBAL_HPP_

#include <mng_globalTYP.h>


    typedef struct {

		signed short NumSignals;

        /* REAL Prod_Load_tray_AXIS */
        REAL Prod_Load_tray_AXIS;
        /* BOOL Prod_Load_tray_LBK */
        BOOL Prod_Load_tray_LBK;
        /* BOOL Prod_Load_tray_LFR */
        BOOL Prod_Load_tray_LFR;
        /* REAL Prod_Load_tray_ACT_SPD */
        REAL Prod_Load_tray_ACT_SPD;
        /* BOOL Prod_Load_tray_RCHD */
        BOOL Prod_Load_tray_RCHD;
        /* REAL Prod_Load_tray_DLT_POS */
        REAL Prod_Load_tray_DLT_POS;
        /* DINT Prod_Load_tray_KinState */
        DINT Prod_Load_tray_KinState;
        /* REAL FeedConv_B_ConvActualSpeed */
        REAL FeedConv_B_ConvActualSpeed;
        /* REAL grinder_head_RH_AXIS */
        REAL grinder_head_RH_AXIS;
        /* BOOL grinder_head_RH_LBK */
        BOOL grinder_head_RH_LBK;
        /* BOOL grinder_head_RH_LFR */
        BOOL grinder_head_RH_LFR;
        /* REAL grinder_head_RH_ACT_SPD */
        REAL grinder_head_RH_ACT_SPD;
        /* BOOL grinder_head_RH_RCHD */
        BOOL grinder_head_RH_RCHD;
        /* REAL grinder_head_RH_DLT_POS */
        REAL grinder_head_RH_DLT_POS;
        /* DINT grinder_head_RH_KinState */
        DINT grinder_head_RH_KinState;
        /* REAL StackingDevice_Housing_AXIS */
        REAL StackingDevice_Housing_AXIS;
        /* BOOL StackingDevice_Housing_LBK */
        BOOL StackingDevice_Housing_LBK;
        /* BOOL StackingDevice_Housing_LFR */
        BOOL StackingDevice_Housing_LFR;
        /* REAL StackingDevice_Housing_ACT_SPD */
        REAL StackingDevice_Housing_ACT_SPD;
        /* BOOL StackingDevice_Housing_RCHD */
        BOOL StackingDevice_Housing_RCHD;
        /* REAL StackingDevice_Housing_DLT_POS */
        REAL StackingDevice_Housing_DLT_POS;
        /* DINT StackingDevice_Housing_KinState */
        DINT StackingDevice_Housing_KinState;
        /* REAL InterConv_B_ConvActualSpeed */
        REAL InterConv_B_ConvActualSpeed;
        /* REAL PortionConv_Belt_ConvActualSpeed */
        REAL PortionConv_Belt_ConvActualSpeed;
        /* REAL TransferConv_B_ConvActualSpeed */
        REAL TransferConv_B_ConvActualSpeed;
        /* REAL TransferConv_B_2_ConvActualSpeed */
        REAL TransferConv_B_2_ConvActualSpeed;
        /* REAL Feed_Carr_M_AXIS */
        REAL Feed_Carr_M_AXIS;
        /* BOOL Feed_Carr_M_LBK */
        BOOL Feed_Carr_M_LBK;
        /* BOOL Feed_Carr_M_LFR */
        BOOL Feed_Carr_M_LFR;
        /* REAL Feed_Carr_M_ACT_SPD */
        REAL Feed_Carr_M_ACT_SPD;
        /* BOOL Feed_Carr_M_RCHD */
        BOOL Feed_Carr_M_RCHD;
        /* REAL Feed_Carr_M_DLT_POS */
        REAL Feed_Carr_M_DLT_POS;
        /* DINT Feed_Carr_M_KinState */
        DINT Feed_Carr_M_KinState;
        /* REAL Ejector_Ram_3_AXIS */
        REAL Ejector_Ram_3_AXIS;
        /* BOOL Ejector_Ram_3_LBK */
        BOOL Ejector_Ram_3_LBK;
        /* BOOL Ejector_Ram_3_LFR */
        BOOL Ejector_Ram_3_LFR;
        /* BOOL Claw_Line3_GRIPPPER_lANE3_CLOSED */
        BOOL Claw_Line3_GRIPPPER_lANE3_CLOSED;
        /* REAL L_Tr_Shaft1ConvActualSpeed */
        REAL L_Tr_Shaft1ConvActualSpeed;
        /* REAL InfeedDoor_AXIS */
        REAL InfeedDoor_AXIS;
        /* BOOL InfeedDoor_LBK */
        BOOL InfeedDoor_LBK;
        /* BOOL InfeedDoor_LFR */
        BOOL InfeedDoor_LFR;
        /* REAL Rotor_IdleCut_AXIS */
        REAL Rotor_IdleCut_AXIS;
        /* BOOL Rotor_IdleCut_LBK */
        BOOL Rotor_IdleCut_LBK;
        /* BOOL Rotor_IdleCut_LFR */
        BOOL Rotor_IdleCut_LFR;
        /* REAL Rotor_IdleCut_ACT_SPD */
        REAL Rotor_IdleCut_ACT_SPD;
        /* BOOL Rotor_IdleCut_RCHD */
        BOOL Rotor_IdleCut_RCHD;
        /* REAL Rotor_IdleCut_DLT_POS */
        REAL Rotor_IdleCut_DLT_POS;
        /* DINT Rotor_IdleCut_KinState */
        DINT Rotor_IdleCut_KinState;
        /* REAL Rotor_AXIS */
        REAL Rotor_AXIS;
        /* BOOL Rotor_LBK */
        BOOL Rotor_LBK;
        /* BOOL Rotor_LFR */
        BOOL Rotor_LFR;
        /* REAL PressConv_Up_Tr_3_AXIS */
        REAL PressConv_Up_Tr_3_AXIS;
        /* BOOL PressConv_Up_Tr_3_LBK */
        BOOL PressConv_Up_Tr_3_LBK;
        /* BOOL PressConv_Up_Tr_3_LFR */
        BOOL PressConv_Up_Tr_3_LFR;
        /* REAL Up_Tr_Belt3_ConvActualSpeed */
        REAL Up_Tr_Belt3_ConvActualSpeed;
        /* REAL PressConv_Up_Tr_4_AXIS */
        REAL PressConv_Up_Tr_4_AXIS;
        /* BOOL PressConv_Up_Tr_4_LBK */
        BOOL PressConv_Up_Tr_4_LBK;
        /* BOOL PressConv_Up_Tr_4_LFR */
        BOOL PressConv_Up_Tr_4_LFR;
        /* REAL Up_Tr_Belt4_ConvActualSpeed */
        REAL Up_Tr_Belt4_ConvActualSpeed;
        /* REAL PressConv_Up_Tr_2_AXIS */
        REAL PressConv_Up_Tr_2_AXIS;
        /* BOOL PressConv_Up_Tr_2_LBK */
        BOOL PressConv_Up_Tr_2_LBK;
        /* BOOL PressConv_Up_Tr_2_LFR */
        BOOL PressConv_Up_Tr_2_LFR;
        /* REAL PressConv_Up_Tr_1_AXIS */
        REAL PressConv_Up_Tr_1_AXIS;
        /* BOOL PressConv_Up_Tr_1_LBK */
        BOOL PressConv_Up_Tr_1_LBK;
        /* BOOL PressConv_Up_Tr_1_LFR */
        BOOL PressConv_Up_Tr_1_LFR;
        /* REAL product_stop_AXIS */
        REAL product_stop_AXIS;
        /* BOOL product_stop_LBK */
        BOOL product_stop_LBK;
        /* BOOL product_stop_LFR */
        BOOL product_stop_LFR;
        /* REAL InfeedConveyor_ConvActualSpeed */
        REAL InfeedConveyor_ConvActualSpeed;
        /* BOOL Infeed_Sensor_1_IR */
        BOOL Infeed_Sensor_1_IR;
        /* BOOL Infeed_Sensor_2_IR */
        BOOL Infeed_Sensor_2_IR;

        } ComTCP_INPUTS_32BIT_FWRD;


    typedef struct {

		signed short NumSignals;

        /* REAL Prod_Load_tray_MAX_SPD */
        REAL Prod_Load_tray_MAX_SPD;
        /* REAL Prod_Load_tray_ACC */
        REAL Prod_Load_tray_ACC;
        /* REAL Prod_Load_tray_DEC */
        REAL Prod_Load_tray_DEC;
        /* REAL Prod_Load_tray_TGT_POS */
        REAL Prod_Load_tray_TGT_POS;
        /* REAL FeedConv_B_MAX_VEL */
        REAL FeedConv_B_MAX_VEL;
        /* REAL FeedConv_B_ACC */
        REAL FeedConv_B_ACC;
        /* REAL FeedConv_B_DEC */
        REAL FeedConv_B_DEC;
        /* BOOL FeedConv_B_FWD */
        BOOL FeedConv_B_FWD;
        /* BOOL FeedConv_B_BWD */
        BOOL FeedConv_B_BWD;
        /* REAL grinder_head_RH_MAX_SPD */
        REAL grinder_head_RH_MAX_SPD;
        /* REAL grinder_head_RH_ACC */
        REAL grinder_head_RH_ACC;
        /* REAL grinder_head_RH_DEC */
        REAL grinder_head_RH_DEC;
        /* REAL grinder_head_RH_TGT_POS */
        REAL grinder_head_RH_TGT_POS;
        /* REAL StackingDevice_Housing_MAX_SPD */
        REAL StackingDevice_Housing_MAX_SPD;
        /* REAL StackingDevice_Housing_ACC */
        REAL StackingDevice_Housing_ACC;
        /* REAL StackingDevice_Housing_DEC */
        REAL StackingDevice_Housing_DEC;
        /* REAL StackingDevice_Housing_TGT_POS */
        REAL StackingDevice_Housing_TGT_POS;
        /* REAL InterConv_B_MAX_VEL */
        REAL InterConv_B_MAX_VEL;
        /* REAL InterConv_B_ACC */
        REAL InterConv_B_ACC;
        /* REAL InterConv_B_DEC */
        REAL InterConv_B_DEC;
        /* BOOL InterConv_B_FWD */
        BOOL InterConv_B_FWD;
        /* BOOL InterConv_B_BWD */
        BOOL InterConv_B_BWD;
        /* REAL PortionConv_Belt_MAX_VEL */
        REAL PortionConv_Belt_MAX_VEL;
        /* REAL PortionConv_Belt_ACC */
        REAL PortionConv_Belt_ACC;
        /* REAL PortionConv_Belt_DEC */
        REAL PortionConv_Belt_DEC;
        /* BOOL PortionConv_Belt_FWD */
        BOOL PortionConv_Belt_FWD;
        /* BOOL PortionConv_Belt_BWD */
        BOOL PortionConv_Belt_BWD;
        /* REAL TransferConv_B_MAX_VEL */
        REAL TransferConv_B_MAX_VEL;
        /* REAL TransferConv_B_ACC */
        REAL TransferConv_B_ACC;
        /* REAL TransferConv_B_DEC */
        REAL TransferConv_B_DEC;
        /* BOOL TransferConv_B_FWD */
        BOOL TransferConv_B_FWD;
        /* BOOL TransferConv_B_BWD */
        BOOL TransferConv_B_BWD;
        /* REAL TransferConv_B_2_MAX_VEL */
        REAL TransferConv_B_2_MAX_VEL;
        /* REAL TransferConv_B_2_ACC */
        REAL TransferConv_B_2_ACC;
        /* REAL TransferConv_B_2_DEC */
        REAL TransferConv_B_2_DEC;
        /* BOOL TransferConv_B_2_FWD */
        BOOL TransferConv_B_2_FWD;
        /* BOOL TransferConv_B_2_BWD */
        BOOL TransferConv_B_2_BWD;
        /* REAL Feed_Carr_M_MAX_SPD */
        REAL Feed_Carr_M_MAX_SPD;
        /* REAL Feed_Carr_M_ACC */
        REAL Feed_Carr_M_ACC;
        /* REAL Feed_Carr_M_DEC */
        REAL Feed_Carr_M_DEC;
        /* REAL Feed_Carr_M_TGT_POS */
        REAL Feed_Carr_M_TGT_POS;
        /* REAL Ejector_Ram_3_MAX_SPD */
        REAL Ejector_Ram_3_MAX_SPD;
        /* BOOL Ejector_Ram_3_FWD */
        BOOL Ejector_Ram_3_FWD;
        /* BOOL Ejector_Ram_3_KinInhibit */
        BOOL Ejector_Ram_3_KinInhibit;
        /* BOOL Claw_Line3_GRIPPPER_lANE3_OPEN */
        BOOL Claw_Line3_GRIPPPER_lANE3_OPEN;
        /* BOOL Claw_Line3_GRIPPPER_lANE3_CLOSE */
        BOOL Claw_Line3_GRIPPPER_lANE3_CLOSE;
        /* REAL L_Tr_Shaft1MAX_VEL */
        REAL L_Tr_Shaft1MAX_VEL;
        /* REAL L_Tr_Shaft1ACC */
        REAL L_Tr_Shaft1ACC;
        /* REAL L_Tr_Shaft1DEC */
        REAL L_Tr_Shaft1DEC;
        /* BOOL L_Tr_Shaft1FWD */
        BOOL L_Tr_Shaft1FWD;
        /* BOOL L_Tr_Shaft1BWD */
        BOOL L_Tr_Shaft1BWD;
        /* REAL InfeedDoor_MAX_SPD */
        REAL InfeedDoor_MAX_SPD;
        /* BOOL InfeedDoor_FWD */
        BOOL InfeedDoor_FWD;
        /* BOOL InfeedDoor_KinInhibit */
        BOOL InfeedDoor_KinInhibit;
        /* BOOL InfeedDoor_BWD */
        BOOL InfeedDoor_BWD;
        /* REAL Rotor_IdleCut_MAX_SPD */
        REAL Rotor_IdleCut_MAX_SPD;
        /* REAL Rotor_IdleCut_ACC */
        REAL Rotor_IdleCut_ACC;
        /* REAL Rotor_IdleCut_DEC */
        REAL Rotor_IdleCut_DEC;
        /* REAL Rotor_IdleCut_TGT_POS */
        REAL Rotor_IdleCut_TGT_POS;
        /* REAL Rotor_ACT_POS */
        REAL Rotor_ACT_POS;
        /* REAL PressConv_Up_Tr_3_MAX_SPD */
        REAL PressConv_Up_Tr_3_MAX_SPD;
        /* BOOL PressConv_Up_Tr_3_FWD */
        BOOL PressConv_Up_Tr_3_FWD;
        /* BOOL PressConv_Up_Tr_3_KinInhibit */
        BOOL PressConv_Up_Tr_3_KinInhibit;
        /* REAL Up_Tr_Belt3_MAX_VEL */
        REAL Up_Tr_Belt3_MAX_VEL;
        /* REAL Up_Tr_Belt3_ACC */
        REAL Up_Tr_Belt3_ACC;
        /* REAL Up_Tr_Belt3_DEC */
        REAL Up_Tr_Belt3_DEC;
        /* BOOL Up_Tr_Belt3_FWD */
        BOOL Up_Tr_Belt3_FWD;
        /* BOOL Up_Tr_Belt3_BWD */
        BOOL Up_Tr_Belt3_BWD;
        /* REAL PressConv_Up_Tr_4_MAX_SPD */
        REAL PressConv_Up_Tr_4_MAX_SPD;
        /* BOOL PressConv_Up_Tr_4_FWD */
        BOOL PressConv_Up_Tr_4_FWD;
        /* BOOL PressConv_Up_Tr_4_KinInhibit */
        BOOL PressConv_Up_Tr_4_KinInhibit;
        /* REAL Up_Tr_Belt4_MAX_VEL */
        REAL Up_Tr_Belt4_MAX_VEL;
        /* REAL Up_Tr_Belt4_ACC */
        REAL Up_Tr_Belt4_ACC;
        /* REAL Up_Tr_Belt4_DEC */
        REAL Up_Tr_Belt4_DEC;
        /* BOOL Up_Tr_Belt4_FWD */
        BOOL Up_Tr_Belt4_FWD;
        /* BOOL Up_Tr_Belt4_BWD */
        BOOL Up_Tr_Belt4_BWD;
        /* REAL PressConv_Up_Tr_2_MAX_SPD */
        REAL PressConv_Up_Tr_2_MAX_SPD;
        /* BOOL PressConv_Up_Tr_2_FWD */
        BOOL PressConv_Up_Tr_2_FWD;
        /* BOOL PressConv_Up_Tr_2_KinInhibit */
        BOOL PressConv_Up_Tr_2_KinInhibit;
        /* REAL PressConv_Up_Tr_1_MAX_SPD */
        REAL PressConv_Up_Tr_1_MAX_SPD;
        /* BOOL PressConv_Up_Tr_1_FWD */
        BOOL PressConv_Up_Tr_1_FWD;
        /* BOOL PressConv_Up_Tr_1_KinInhibit */
        BOOL PressConv_Up_Tr_1_KinInhibit;
        /* REAL product_stop_MAX_SPD */
        REAL product_stop_MAX_SPD;
        /* BOOL product_stop_FWD */
        BOOL product_stop_FWD;
        /* BOOL product_stop_KinInhibit */
        BOOL product_stop_KinInhibit;
        /* BOOL product_stop_BWD */
        BOOL product_stop_BWD;
        /* REAL InfeedConveyor_MAX_VEL */
        REAL InfeedConveyor_MAX_VEL;
        /* REAL InfeedConveyor_ACC */
        REAL InfeedConveyor_ACC;
        /* REAL InfeedConveyor_DEC */
        REAL InfeedConveyor_DEC;
        /* BOOL InfeedConveyor_FWD */
        BOOL InfeedConveyor_FWD;
        /* BOOL InfeedConveyor_BWD */
        BOOL InfeedConveyor_BWD;

        } ComTCP_OUTPUTS_32BIT_FWRD;



#endif
