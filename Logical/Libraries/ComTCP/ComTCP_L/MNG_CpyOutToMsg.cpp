
#include "MNG_InternalTyp.hpp"

unsigned long int MNG_CpyOutToMsg(MNG_RawDatagram * pToServer, unsigned long int counter, ComTCP_OUTPUTS_32BIT_FWRD *pSimOutputs ) {

    
        unsigned long int DWordBuffer;
	
        pSimOutputs->NumSignals = 91;
        (pToServer->Header).ByteCount    = H_TO_NUDINT(368);
        (pToServer->Header).DatagramType = H_TO_NUDINT(MNG_Raw32BitImage);
        (pToServer->Header).SentTime = H_TO_NUDINT(0);  // Todo
        (pToServer->Header).Counter = H_TO_NUDINT(counter);
        pToServer->Payload[0] = H_TO_NUDINT(pSimOutputs->NumSignals);

    
    /* Prod_Load_tray_MAX_SPD : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->Prod_Load_tray_MAX_SPD), 4);
    pToServer->Payload[1] = H_TO_NUDINT(DWordBuffer);


    /* Prod_Load_tray_ACC : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->Prod_Load_tray_ACC), 4);
    pToServer->Payload[2] = H_TO_NUDINT(DWordBuffer);


    /* Prod_Load_tray_DEC : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->Prod_Load_tray_DEC), 4);
    pToServer->Payload[3] = H_TO_NUDINT(DWordBuffer);


    /* Prod_Load_tray_TGT_POS : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->Prod_Load_tray_TGT_POS), 4);
    pToServer->Payload[4] = H_TO_NUDINT(DWordBuffer);


    /* FeedConv_B_MAX_VEL : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->FeedConv_B_MAX_VEL), 4);
    pToServer->Payload[5] = H_TO_NUDINT(DWordBuffer);


    /* FeedConv_B_ACC : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->FeedConv_B_ACC), 4);
    pToServer->Payload[6] = H_TO_NUDINT(DWordBuffer);


    /* FeedConv_B_DEC : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->FeedConv_B_DEC), 4);
    pToServer->Payload[7] = H_TO_NUDINT(DWordBuffer);


    /* FeedConv_B_FWD : BOOL */
    if(pSimOutputs->FeedConv_B_FWD == 1) {
        pToServer->Payload[8] = 0xFFFFFFFF;   /*BOOL */
    } else { 
        pToServer->Payload[8] = 0;
    }


    /* FeedConv_B_BWD : BOOL */
    if(pSimOutputs->FeedConv_B_BWD == 1) {
        pToServer->Payload[9] = 0xFFFFFFFF;   /*BOOL */
    } else { 
        pToServer->Payload[9] = 0;
    }


    /* grinder_head_RH_MAX_SPD : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->grinder_head_RH_MAX_SPD), 4);
    pToServer->Payload[10] = H_TO_NUDINT(DWordBuffer);


    /* grinder_head_RH_ACC : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->grinder_head_RH_ACC), 4);
    pToServer->Payload[11] = H_TO_NUDINT(DWordBuffer);


    /* grinder_head_RH_DEC : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->grinder_head_RH_DEC), 4);
    pToServer->Payload[12] = H_TO_NUDINT(DWordBuffer);


    /* grinder_head_RH_TGT_POS : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->grinder_head_RH_TGT_POS), 4);
    pToServer->Payload[13] = H_TO_NUDINT(DWordBuffer);


    /* StackingDevice_Housing_MAX_SPD : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->StackingDevice_Housing_MAX_SPD), 4);
    pToServer->Payload[14] = H_TO_NUDINT(DWordBuffer);


    /* StackingDevice_Housing_ACC : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->StackingDevice_Housing_ACC), 4);
    pToServer->Payload[15] = H_TO_NUDINT(DWordBuffer);


    /* StackingDevice_Housing_DEC : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->StackingDevice_Housing_DEC), 4);
    pToServer->Payload[16] = H_TO_NUDINT(DWordBuffer);


    /* StackingDevice_Housing_TGT_POS : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->StackingDevice_Housing_TGT_POS), 4);
    pToServer->Payload[17] = H_TO_NUDINT(DWordBuffer);


    /* InterConv_B_MAX_VEL : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->InterConv_B_MAX_VEL), 4);
    pToServer->Payload[18] = H_TO_NUDINT(DWordBuffer);


    /* InterConv_B_ACC : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->InterConv_B_ACC), 4);
    pToServer->Payload[19] = H_TO_NUDINT(DWordBuffer);


    /* InterConv_B_DEC : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->InterConv_B_DEC), 4);
    pToServer->Payload[20] = H_TO_NUDINT(DWordBuffer);


    /* InterConv_B_FWD : BOOL */
    if(pSimOutputs->InterConv_B_FWD == 1) {
        pToServer->Payload[21] = 0xFFFFFFFF;   /*BOOL */
    } else { 
        pToServer->Payload[21] = 0;
    }


    /* InterConv_B_BWD : BOOL */
    if(pSimOutputs->InterConv_B_BWD == 1) {
        pToServer->Payload[22] = 0xFFFFFFFF;   /*BOOL */
    } else { 
        pToServer->Payload[22] = 0;
    }


    /* PortionConv_Belt_MAX_VEL : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->PortionConv_Belt_MAX_VEL), 4);
    pToServer->Payload[23] = H_TO_NUDINT(DWordBuffer);


    /* PortionConv_Belt_ACC : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->PortionConv_Belt_ACC), 4);
    pToServer->Payload[24] = H_TO_NUDINT(DWordBuffer);


    /* PortionConv_Belt_DEC : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->PortionConv_Belt_DEC), 4);
    pToServer->Payload[25] = H_TO_NUDINT(DWordBuffer);


    /* PortionConv_Belt_FWD : BOOL */
    if(pSimOutputs->PortionConv_Belt_FWD == 1) {
        pToServer->Payload[26] = 0xFFFFFFFF;   /*BOOL */
    } else { 
        pToServer->Payload[26] = 0;
    }


    /* PortionConv_Belt_BWD : BOOL */
    if(pSimOutputs->PortionConv_Belt_BWD == 1) {
        pToServer->Payload[27] = 0xFFFFFFFF;   /*BOOL */
    } else { 
        pToServer->Payload[27] = 0;
    }


    /* TransferConv_B_MAX_VEL : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->TransferConv_B_MAX_VEL), 4);
    pToServer->Payload[28] = H_TO_NUDINT(DWordBuffer);


    /* TransferConv_B_ACC : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->TransferConv_B_ACC), 4);
    pToServer->Payload[29] = H_TO_NUDINT(DWordBuffer);


    /* TransferConv_B_DEC : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->TransferConv_B_DEC), 4);
    pToServer->Payload[30] = H_TO_NUDINT(DWordBuffer);


    /* TransferConv_B_FWD : BOOL */
    if(pSimOutputs->TransferConv_B_FWD == 1) {
        pToServer->Payload[31] = 0xFFFFFFFF;   /*BOOL */
    } else { 
        pToServer->Payload[31] = 0;
    }


    /* TransferConv_B_BWD : BOOL */
    if(pSimOutputs->TransferConv_B_BWD == 1) {
        pToServer->Payload[32] = 0xFFFFFFFF;   /*BOOL */
    } else { 
        pToServer->Payload[32] = 0;
    }


    /* TransferConv_B_2_MAX_VEL : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->TransferConv_B_2_MAX_VEL), 4);
    pToServer->Payload[33] = H_TO_NUDINT(DWordBuffer);


    /* TransferConv_B_2_ACC : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->TransferConv_B_2_ACC), 4);
    pToServer->Payload[34] = H_TO_NUDINT(DWordBuffer);


    /* TransferConv_B_2_DEC : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->TransferConv_B_2_DEC), 4);
    pToServer->Payload[35] = H_TO_NUDINT(DWordBuffer);


    /* TransferConv_B_2_FWD : BOOL */
    if(pSimOutputs->TransferConv_B_2_FWD == 1) {
        pToServer->Payload[36] = 0xFFFFFFFF;   /*BOOL */
    } else { 
        pToServer->Payload[36] = 0;
    }


    /* TransferConv_B_2_BWD : BOOL */
    if(pSimOutputs->TransferConv_B_2_BWD == 1) {
        pToServer->Payload[37] = 0xFFFFFFFF;   /*BOOL */
    } else { 
        pToServer->Payload[37] = 0;
    }


    /* Feed_Carr_M_MAX_SPD : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->Feed_Carr_M_MAX_SPD), 4);
    pToServer->Payload[38] = H_TO_NUDINT(DWordBuffer);


    /* Feed_Carr_M_ACC : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->Feed_Carr_M_ACC), 4);
    pToServer->Payload[39] = H_TO_NUDINT(DWordBuffer);


    /* Feed_Carr_M_DEC : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->Feed_Carr_M_DEC), 4);
    pToServer->Payload[40] = H_TO_NUDINT(DWordBuffer);


    /* Feed_Carr_M_TGT_POS : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->Feed_Carr_M_TGT_POS), 4);
    pToServer->Payload[41] = H_TO_NUDINT(DWordBuffer);


    /* Ejector_Ram_3_MAX_SPD : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->Ejector_Ram_3_MAX_SPD), 4);
    pToServer->Payload[42] = H_TO_NUDINT(DWordBuffer);


    /* Ejector_Ram_3_FWD : BOOL */
    if(pSimOutputs->Ejector_Ram_3_FWD == 1) {
        pToServer->Payload[43] = 0xFFFFFFFF;   /*BOOL */
    } else { 
        pToServer->Payload[43] = 0;
    }


    /* Ejector_Ram_3_KinInhibit : BOOL */
    if(pSimOutputs->Ejector_Ram_3_KinInhibit == 1) {
        pToServer->Payload[44] = 0xFFFFFFFF;   /*BOOL */
    } else { 
        pToServer->Payload[44] = 0;
    }


    /* Claw_Line3_GRIPPPER_lANE3_OPEN : BOOL */
    if(pSimOutputs->Claw_Line3_GRIPPPER_lANE3_OPEN == 1) {
        pToServer->Payload[45] = 0xFFFFFFFF;   /*BOOL */
    } else { 
        pToServer->Payload[45] = 0;
    }


    /* Claw_Line3_GRIPPPER_lANE3_CLOSE : BOOL */
    if(pSimOutputs->Claw_Line3_GRIPPPER_lANE3_CLOSE == 1) {
        pToServer->Payload[46] = 0xFFFFFFFF;   /*BOOL */
    } else { 
        pToServer->Payload[46] = 0;
    }


    /* L_Tr_Shaft1MAX_VEL : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->L_Tr_Shaft1MAX_VEL), 4);
    pToServer->Payload[47] = H_TO_NUDINT(DWordBuffer);


    /* L_Tr_Shaft1ACC : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->L_Tr_Shaft1ACC), 4);
    pToServer->Payload[48] = H_TO_NUDINT(DWordBuffer);


    /* L_Tr_Shaft1DEC : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->L_Tr_Shaft1DEC), 4);
    pToServer->Payload[49] = H_TO_NUDINT(DWordBuffer);


    /* L_Tr_Shaft1FWD : BOOL */
    if(pSimOutputs->L_Tr_Shaft1FWD == 1) {
        pToServer->Payload[50] = 0xFFFFFFFF;   /*BOOL */
    } else { 
        pToServer->Payload[50] = 0;
    }


    /* L_Tr_Shaft1BWD : BOOL */
    if(pSimOutputs->L_Tr_Shaft1BWD == 1) {
        pToServer->Payload[51] = 0xFFFFFFFF;   /*BOOL */
    } else { 
        pToServer->Payload[51] = 0;
    }


    /* InfeedDoor_MAX_SPD : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->InfeedDoor_MAX_SPD), 4);
    pToServer->Payload[52] = H_TO_NUDINT(DWordBuffer);


    /* InfeedDoor_FWD : BOOL */
    if(pSimOutputs->InfeedDoor_FWD == 1) {
        pToServer->Payload[53] = 0xFFFFFFFF;   /*BOOL */
    } else { 
        pToServer->Payload[53] = 0;
    }


    /* InfeedDoor_KinInhibit : BOOL */
    if(pSimOutputs->InfeedDoor_KinInhibit == 1) {
        pToServer->Payload[54] = 0xFFFFFFFF;   /*BOOL */
    } else { 
        pToServer->Payload[54] = 0;
    }


    /* InfeedDoor_BWD : BOOL */
    if(pSimOutputs->InfeedDoor_BWD == 1) {
        pToServer->Payload[55] = 0xFFFFFFFF;   /*BOOL */
    } else { 
        pToServer->Payload[55] = 0;
    }


    /* Rotor_IdleCut_MAX_SPD : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->Rotor_IdleCut_MAX_SPD), 4);
    pToServer->Payload[56] = H_TO_NUDINT(DWordBuffer);


    /* Rotor_IdleCut_ACC : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->Rotor_IdleCut_ACC), 4);
    pToServer->Payload[57] = H_TO_NUDINT(DWordBuffer);


    /* Rotor_IdleCut_DEC : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->Rotor_IdleCut_DEC), 4);
    pToServer->Payload[58] = H_TO_NUDINT(DWordBuffer);


    /* Rotor_IdleCut_TGT_POS : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->Rotor_IdleCut_TGT_POS), 4);
    pToServer->Payload[59] = H_TO_NUDINT(DWordBuffer);


    /* Rotor_ACT_POS : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->Rotor_ACT_POS), 4);
    pToServer->Payload[60] = H_TO_NUDINT(DWordBuffer);


    /* PressConv_Up_Tr_3_MAX_SPD : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->PressConv_Up_Tr_3_MAX_SPD), 4);
    pToServer->Payload[61] = H_TO_NUDINT(DWordBuffer);


    /* PressConv_Up_Tr_3_FWD : BOOL */
    if(pSimOutputs->PressConv_Up_Tr_3_FWD == 1) {
        pToServer->Payload[62] = 0xFFFFFFFF;   /*BOOL */
    } else { 
        pToServer->Payload[62] = 0;
    }


    /* PressConv_Up_Tr_3_KinInhibit : BOOL */
    if(pSimOutputs->PressConv_Up_Tr_3_KinInhibit == 1) {
        pToServer->Payload[63] = 0xFFFFFFFF;   /*BOOL */
    } else { 
        pToServer->Payload[63] = 0;
    }


    /* Up_Tr_Belt3_MAX_VEL : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->Up_Tr_Belt3_MAX_VEL), 4);
    pToServer->Payload[64] = H_TO_NUDINT(DWordBuffer);


    /* Up_Tr_Belt3_ACC : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->Up_Tr_Belt3_ACC), 4);
    pToServer->Payload[65] = H_TO_NUDINT(DWordBuffer);


    /* Up_Tr_Belt3_DEC : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->Up_Tr_Belt3_DEC), 4);
    pToServer->Payload[66] = H_TO_NUDINT(DWordBuffer);


    /* Up_Tr_Belt3_FWD : BOOL */
    if(pSimOutputs->Up_Tr_Belt3_FWD == 1) {
        pToServer->Payload[67] = 0xFFFFFFFF;   /*BOOL */
    } else { 
        pToServer->Payload[67] = 0;
    }


    /* Up_Tr_Belt3_BWD : BOOL */
    if(pSimOutputs->Up_Tr_Belt3_BWD == 1) {
        pToServer->Payload[68] = 0xFFFFFFFF;   /*BOOL */
    } else { 
        pToServer->Payload[68] = 0;
    }


    /* PressConv_Up_Tr_4_MAX_SPD : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->PressConv_Up_Tr_4_MAX_SPD), 4);
    pToServer->Payload[69] = H_TO_NUDINT(DWordBuffer);


    /* PressConv_Up_Tr_4_FWD : BOOL */
    if(pSimOutputs->PressConv_Up_Tr_4_FWD == 1) {
        pToServer->Payload[70] = 0xFFFFFFFF;   /*BOOL */
    } else { 
        pToServer->Payload[70] = 0;
    }


    /* PressConv_Up_Tr_4_KinInhibit : BOOL */
    if(pSimOutputs->PressConv_Up_Tr_4_KinInhibit == 1) {
        pToServer->Payload[71] = 0xFFFFFFFF;   /*BOOL */
    } else { 
        pToServer->Payload[71] = 0;
    }


    /* Up_Tr_Belt4_MAX_VEL : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->Up_Tr_Belt4_MAX_VEL), 4);
    pToServer->Payload[72] = H_TO_NUDINT(DWordBuffer);


    /* Up_Tr_Belt4_ACC : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->Up_Tr_Belt4_ACC), 4);
    pToServer->Payload[73] = H_TO_NUDINT(DWordBuffer);


    /* Up_Tr_Belt4_DEC : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->Up_Tr_Belt4_DEC), 4);
    pToServer->Payload[74] = H_TO_NUDINT(DWordBuffer);


    /* Up_Tr_Belt4_FWD : BOOL */
    if(pSimOutputs->Up_Tr_Belt4_FWD == 1) {
        pToServer->Payload[75] = 0xFFFFFFFF;   /*BOOL */
    } else { 
        pToServer->Payload[75] = 0;
    }


    /* Up_Tr_Belt4_BWD : BOOL */
    if(pSimOutputs->Up_Tr_Belt4_BWD == 1) {
        pToServer->Payload[76] = 0xFFFFFFFF;   /*BOOL */
    } else { 
        pToServer->Payload[76] = 0;
    }


    /* PressConv_Up_Tr_2_MAX_SPD : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->PressConv_Up_Tr_2_MAX_SPD), 4);
    pToServer->Payload[77] = H_TO_NUDINT(DWordBuffer);


    /* PressConv_Up_Tr_2_FWD : BOOL */
    if(pSimOutputs->PressConv_Up_Tr_2_FWD == 1) {
        pToServer->Payload[78] = 0xFFFFFFFF;   /*BOOL */
    } else { 
        pToServer->Payload[78] = 0;
    }


    /* PressConv_Up_Tr_2_KinInhibit : BOOL */
    if(pSimOutputs->PressConv_Up_Tr_2_KinInhibit == 1) {
        pToServer->Payload[79] = 0xFFFFFFFF;   /*BOOL */
    } else { 
        pToServer->Payload[79] = 0;
    }


    /* PressConv_Up_Tr_1_MAX_SPD : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->PressConv_Up_Tr_1_MAX_SPD), 4);
    pToServer->Payload[80] = H_TO_NUDINT(DWordBuffer);


    /* PressConv_Up_Tr_1_FWD : BOOL */
    if(pSimOutputs->PressConv_Up_Tr_1_FWD == 1) {
        pToServer->Payload[81] = 0xFFFFFFFF;   /*BOOL */
    } else { 
        pToServer->Payload[81] = 0;
    }


    /* PressConv_Up_Tr_1_KinInhibit : BOOL */
    if(pSimOutputs->PressConv_Up_Tr_1_KinInhibit == 1) {
        pToServer->Payload[82] = 0xFFFFFFFF;   /*BOOL */
    } else { 
        pToServer->Payload[82] = 0;
    }


    /* product_stop_MAX_SPD : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->product_stop_MAX_SPD), 4);
    pToServer->Payload[83] = H_TO_NUDINT(DWordBuffer);


    /* product_stop_FWD : BOOL */
    if(pSimOutputs->product_stop_FWD == 1) {
        pToServer->Payload[84] = 0xFFFFFFFF;   /*BOOL */
    } else { 
        pToServer->Payload[84] = 0;
    }


    /* product_stop_KinInhibit : BOOL */
    if(pSimOutputs->product_stop_KinInhibit == 1) {
        pToServer->Payload[85] = 0xFFFFFFFF;   /*BOOL */
    } else { 
        pToServer->Payload[85] = 0;
    }


    /* product_stop_BWD : BOOL */
    if(pSimOutputs->product_stop_BWD == 1) {
        pToServer->Payload[86] = 0xFFFFFFFF;   /*BOOL */
    } else { 
        pToServer->Payload[86] = 0;
    }


    /* InfeedConveyor_MAX_VEL : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->InfeedConveyor_MAX_VEL), 4);
    pToServer->Payload[87] = H_TO_NUDINT(DWordBuffer);


    /* InfeedConveyor_ACC : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->InfeedConveyor_ACC), 4);
    pToServer->Payload[88] = H_TO_NUDINT(DWordBuffer);


    /* InfeedConveyor_DEC : FLOAT32 */
    brsmemcpy((unsigned long int)&(DWordBuffer), (unsigned long int)&(pSimOutputs->InfeedConveyor_DEC), 4);
    pToServer->Payload[89] = H_TO_NUDINT(DWordBuffer);


    /* InfeedConveyor_FWD : BOOL */
    if(pSimOutputs->InfeedConveyor_FWD == 1) {
        pToServer->Payload[90] = 0xFFFFFFFF;   /*BOOL */
    } else { 
        pToServer->Payload[90] = 0;
    }


    /* InfeedConveyor_BWD : BOOL */
    if(pSimOutputs->InfeedConveyor_BWD == 1) {
        pToServer->Payload[91] = 0xFFFFFFFF;   /*BOOL */
    } else { 
        pToServer->Payload[91] = 0;
    }


	return 0;
}
