
#include "MNG_InternalTyp.hpp"

unsigned long int MNG_CpyMsgToInp(MNG_RawDatagram * pFromServer, ComTCP_INPUTS_32BIT_FWRD *pSimInputs) {

    MNG_DatagramType datagramType;
	unsigned long int DWordBuffer;
        unsigned long int byteCount;
	unsigned long int numSignalsInMessage;


	byteCount = N_TO_HUDINT((pFromServer->Header).ByteCount);
	datagramType = static_cast<MNG_DatagramType>(N_TO_HUDINT((pFromServer->Header).DatagramType));
	if (datagramType != MNG_Raw32BitImage) {
		return 0;
	}

    pSimInputs->NumSignals = 71;

    numSignalsInMessage = N_TO_HUDINT(pFromServer->Payload[0]);
        if ((static_cast<long int>(numSignalsInMessage) != pSimInputs->NumSignals) ||
	(byteCount != 4 + numSignalsInMessage * 4)) {
		return 0;
	}

    /* FLOAT32 Prod_Load_tray_AXIS */
    DWordBuffer = N_TO_HUDINT(pFromServer->Payload[1]);
    brsmemcpy((unsigned long int)&(pSimInputs->Prod_Load_tray_AXIS), (unsigned long int)&(DWordBuffer), 4);

    /* BOOL Prod_Load_tray_LBK */
    pSimInputs->Prod_Load_tray_LBK = (pFromServer->Payload[2] != 0);

    /* BOOL Prod_Load_tray_LFR */
    pSimInputs->Prod_Load_tray_LFR = (pFromServer->Payload[3] != 0);

    /* FLOAT32 Prod_Load_tray_ACT_SPD */
    DWordBuffer = N_TO_HUDINT(pFromServer->Payload[4]);
    brsmemcpy((unsigned long int)&(pSimInputs->Prod_Load_tray_ACT_SPD), (unsigned long int)&(DWordBuffer), 4);

    /* BOOL Prod_Load_tray_RCHD */
    pSimInputs->Prod_Load_tray_RCHD = (pFromServer->Payload[5] != 0);

    /* FLOAT32 Prod_Load_tray_DLT_POS */
    DWordBuffer = N_TO_HUDINT(pFromServer->Payload[6]);
    brsmemcpy((unsigned long int)&(pSimInputs->Prod_Load_tray_DLT_POS), (unsigned long int)&(DWordBuffer), 4);

    /* INT32 Prod_Load_tray_KinState */
    DWordBuffer = N_TO_HUDINT(pFromServer->Payload[7]);
    brsmemcpy((unsigned long int)&(pSimInputs->Prod_Load_tray_KinState), (unsigned long int)&(DWordBuffer), 4);

    /* FLOAT32 FeedConv_B_ConvActualSpeed */
    DWordBuffer = N_TO_HUDINT(pFromServer->Payload[8]);
    brsmemcpy((unsigned long int)&(pSimInputs->FeedConv_B_ConvActualSpeed), (unsigned long int)&(DWordBuffer), 4);

    /* FLOAT32 grinder_head_RH_AXIS */
    DWordBuffer = N_TO_HUDINT(pFromServer->Payload[9]);
    brsmemcpy((unsigned long int)&(pSimInputs->grinder_head_RH_AXIS), (unsigned long int)&(DWordBuffer), 4);

    /* BOOL grinder_head_RH_LBK */
    pSimInputs->grinder_head_RH_LBK = (pFromServer->Payload[10] != 0);

    /* BOOL grinder_head_RH_LFR */
    pSimInputs->grinder_head_RH_LFR = (pFromServer->Payload[11] != 0);

    /* FLOAT32 grinder_head_RH_ACT_SPD */
    DWordBuffer = N_TO_HUDINT(pFromServer->Payload[12]);
    brsmemcpy((unsigned long int)&(pSimInputs->grinder_head_RH_ACT_SPD), (unsigned long int)&(DWordBuffer), 4);

    /* BOOL grinder_head_RH_RCHD */
    pSimInputs->grinder_head_RH_RCHD = (pFromServer->Payload[13] != 0);

    /* FLOAT32 grinder_head_RH_DLT_POS */
    DWordBuffer = N_TO_HUDINT(pFromServer->Payload[14]);
    brsmemcpy((unsigned long int)&(pSimInputs->grinder_head_RH_DLT_POS), (unsigned long int)&(DWordBuffer), 4);

    /* INT32 grinder_head_RH_KinState */
    DWordBuffer = N_TO_HUDINT(pFromServer->Payload[15]);
    brsmemcpy((unsigned long int)&(pSimInputs->grinder_head_RH_KinState), (unsigned long int)&(DWordBuffer), 4);

    /* FLOAT32 StackingDevice_Housing_AXIS */
    DWordBuffer = N_TO_HUDINT(pFromServer->Payload[16]);
    brsmemcpy((unsigned long int)&(pSimInputs->StackingDevice_Housing_AXIS), (unsigned long int)&(DWordBuffer), 4);

    /* BOOL StackingDevice_Housing_LBK */
    pSimInputs->StackingDevice_Housing_LBK = (pFromServer->Payload[17] != 0);

    /* BOOL StackingDevice_Housing_LFR */
    pSimInputs->StackingDevice_Housing_LFR = (pFromServer->Payload[18] != 0);

    /* FLOAT32 StackingDevice_Housing_ACT_SPD */
    DWordBuffer = N_TO_HUDINT(pFromServer->Payload[19]);
    brsmemcpy((unsigned long int)&(pSimInputs->StackingDevice_Housing_ACT_SPD), (unsigned long int)&(DWordBuffer), 4);

    /* BOOL StackingDevice_Housing_RCHD */
    pSimInputs->StackingDevice_Housing_RCHD = (pFromServer->Payload[20] != 0);

    /* FLOAT32 StackingDevice_Housing_DLT_POS */
    DWordBuffer = N_TO_HUDINT(pFromServer->Payload[21]);
    brsmemcpy((unsigned long int)&(pSimInputs->StackingDevice_Housing_DLT_POS), (unsigned long int)&(DWordBuffer), 4);

    /* INT32 StackingDevice_Housing_KinState */
    DWordBuffer = N_TO_HUDINT(pFromServer->Payload[22]);
    brsmemcpy((unsigned long int)&(pSimInputs->StackingDevice_Housing_KinState), (unsigned long int)&(DWordBuffer), 4);

    /* FLOAT32 InterConv_B_ConvActualSpeed */
    DWordBuffer = N_TO_HUDINT(pFromServer->Payload[23]);
    brsmemcpy((unsigned long int)&(pSimInputs->InterConv_B_ConvActualSpeed), (unsigned long int)&(DWordBuffer), 4);

    /* FLOAT32 PortionConv_Belt_ConvActualSpeed */
    DWordBuffer = N_TO_HUDINT(pFromServer->Payload[24]);
    brsmemcpy((unsigned long int)&(pSimInputs->PortionConv_Belt_ConvActualSpeed), (unsigned long int)&(DWordBuffer), 4);

    /* FLOAT32 TransferConv_B_ConvActualSpeed */
    DWordBuffer = N_TO_HUDINT(pFromServer->Payload[25]);
    brsmemcpy((unsigned long int)&(pSimInputs->TransferConv_B_ConvActualSpeed), (unsigned long int)&(DWordBuffer), 4);

    /* FLOAT32 TransferConv_B_2_ConvActualSpeed */
    DWordBuffer = N_TO_HUDINT(pFromServer->Payload[26]);
    brsmemcpy((unsigned long int)&(pSimInputs->TransferConv_B_2_ConvActualSpeed), (unsigned long int)&(DWordBuffer), 4);

    /* FLOAT32 Feed_Carr_M_AXIS */
    DWordBuffer = N_TO_HUDINT(pFromServer->Payload[27]);
    brsmemcpy((unsigned long int)&(pSimInputs->Feed_Carr_M_AXIS), (unsigned long int)&(DWordBuffer), 4);

    /* BOOL Feed_Carr_M_LBK */
    pSimInputs->Feed_Carr_M_LBK = (pFromServer->Payload[28] != 0);

    /* BOOL Feed_Carr_M_LFR */
    pSimInputs->Feed_Carr_M_LFR = (pFromServer->Payload[29] != 0);

    /* FLOAT32 Feed_Carr_M_ACT_SPD */
    DWordBuffer = N_TO_HUDINT(pFromServer->Payload[30]);
    brsmemcpy((unsigned long int)&(pSimInputs->Feed_Carr_M_ACT_SPD), (unsigned long int)&(DWordBuffer), 4);

    /* BOOL Feed_Carr_M_RCHD */
    pSimInputs->Feed_Carr_M_RCHD = (pFromServer->Payload[31] != 0);

    /* FLOAT32 Feed_Carr_M_DLT_POS */
    DWordBuffer = N_TO_HUDINT(pFromServer->Payload[32]);
    brsmemcpy((unsigned long int)&(pSimInputs->Feed_Carr_M_DLT_POS), (unsigned long int)&(DWordBuffer), 4);

    /* INT32 Feed_Carr_M_KinState */
    DWordBuffer = N_TO_HUDINT(pFromServer->Payload[33]);
    brsmemcpy((unsigned long int)&(pSimInputs->Feed_Carr_M_KinState), (unsigned long int)&(DWordBuffer), 4);

    /* FLOAT32 Ejector_Ram_3_AXIS */
    DWordBuffer = N_TO_HUDINT(pFromServer->Payload[34]);
    brsmemcpy((unsigned long int)&(pSimInputs->Ejector_Ram_3_AXIS), (unsigned long int)&(DWordBuffer), 4);

    /* BOOL Ejector_Ram_3_LBK */
    pSimInputs->Ejector_Ram_3_LBK = (pFromServer->Payload[35] != 0);

    /* BOOL Ejector_Ram_3_LFR */
    pSimInputs->Ejector_Ram_3_LFR = (pFromServer->Payload[36] != 0);

    /* BOOL Claw_Line3_GRIPPPER_lANE3_CLOSED */
    pSimInputs->Claw_Line3_GRIPPPER_lANE3_CLOSED = (pFromServer->Payload[37] != 0);

    /* FLOAT32 L_Tr_Shaft1ConvActualSpeed */
    DWordBuffer = N_TO_HUDINT(pFromServer->Payload[38]);
    brsmemcpy((unsigned long int)&(pSimInputs->L_Tr_Shaft1ConvActualSpeed), (unsigned long int)&(DWordBuffer), 4);

    /* FLOAT32 InfeedDoor_AXIS */
    DWordBuffer = N_TO_HUDINT(pFromServer->Payload[39]);
    brsmemcpy((unsigned long int)&(pSimInputs->InfeedDoor_AXIS), (unsigned long int)&(DWordBuffer), 4);

    /* BOOL InfeedDoor_LBK */
    pSimInputs->InfeedDoor_LBK = (pFromServer->Payload[40] != 0);

    /* BOOL InfeedDoor_LFR */
    pSimInputs->InfeedDoor_LFR = (pFromServer->Payload[41] != 0);

    /* FLOAT32 Rotor_IdleCut_AXIS */
    DWordBuffer = N_TO_HUDINT(pFromServer->Payload[42]);
    brsmemcpy((unsigned long int)&(pSimInputs->Rotor_IdleCut_AXIS), (unsigned long int)&(DWordBuffer), 4);

    /* BOOL Rotor_IdleCut_LBK */
    pSimInputs->Rotor_IdleCut_LBK = (pFromServer->Payload[43] != 0);

    /* BOOL Rotor_IdleCut_LFR */
    pSimInputs->Rotor_IdleCut_LFR = (pFromServer->Payload[44] != 0);

    /* FLOAT32 Rotor_IdleCut_ACT_SPD */
    DWordBuffer = N_TO_HUDINT(pFromServer->Payload[45]);
    brsmemcpy((unsigned long int)&(pSimInputs->Rotor_IdleCut_ACT_SPD), (unsigned long int)&(DWordBuffer), 4);

    /* BOOL Rotor_IdleCut_RCHD */
    pSimInputs->Rotor_IdleCut_RCHD = (pFromServer->Payload[46] != 0);

    /* FLOAT32 Rotor_IdleCut_DLT_POS */
    DWordBuffer = N_TO_HUDINT(pFromServer->Payload[47]);
    brsmemcpy((unsigned long int)&(pSimInputs->Rotor_IdleCut_DLT_POS), (unsigned long int)&(DWordBuffer), 4);

    /* INT32 Rotor_IdleCut_KinState */
    DWordBuffer = N_TO_HUDINT(pFromServer->Payload[48]);
    brsmemcpy((unsigned long int)&(pSimInputs->Rotor_IdleCut_KinState), (unsigned long int)&(DWordBuffer), 4);

    /* FLOAT32 Rotor_AXIS */
    DWordBuffer = N_TO_HUDINT(pFromServer->Payload[49]);
    brsmemcpy((unsigned long int)&(pSimInputs->Rotor_AXIS), (unsigned long int)&(DWordBuffer), 4);

    /* BOOL Rotor_LBK */
    pSimInputs->Rotor_LBK = (pFromServer->Payload[50] != 0);

    /* BOOL Rotor_LFR */
    pSimInputs->Rotor_LFR = (pFromServer->Payload[51] != 0);

    /* FLOAT32 PressConv_Up_Tr_3_AXIS */
    DWordBuffer = N_TO_HUDINT(pFromServer->Payload[52]);
    brsmemcpy((unsigned long int)&(pSimInputs->PressConv_Up_Tr_3_AXIS), (unsigned long int)&(DWordBuffer), 4);

    /* BOOL PressConv_Up_Tr_3_LBK */
    pSimInputs->PressConv_Up_Tr_3_LBK = (pFromServer->Payload[53] != 0);

    /* BOOL PressConv_Up_Tr_3_LFR */
    pSimInputs->PressConv_Up_Tr_3_LFR = (pFromServer->Payload[54] != 0);

    /* FLOAT32 Up_Tr_Belt3_ConvActualSpeed */
    DWordBuffer = N_TO_HUDINT(pFromServer->Payload[55]);
    brsmemcpy((unsigned long int)&(pSimInputs->Up_Tr_Belt3_ConvActualSpeed), (unsigned long int)&(DWordBuffer), 4);

    /* FLOAT32 PressConv_Up_Tr_4_AXIS */
    DWordBuffer = N_TO_HUDINT(pFromServer->Payload[56]);
    brsmemcpy((unsigned long int)&(pSimInputs->PressConv_Up_Tr_4_AXIS), (unsigned long int)&(DWordBuffer), 4);

    /* BOOL PressConv_Up_Tr_4_LBK */
    pSimInputs->PressConv_Up_Tr_4_LBK = (pFromServer->Payload[57] != 0);

    /* BOOL PressConv_Up_Tr_4_LFR */
    pSimInputs->PressConv_Up_Tr_4_LFR = (pFromServer->Payload[58] != 0);

    /* FLOAT32 Up_Tr_Belt4_ConvActualSpeed */
    DWordBuffer = N_TO_HUDINT(pFromServer->Payload[59]);
    brsmemcpy((unsigned long int)&(pSimInputs->Up_Tr_Belt4_ConvActualSpeed), (unsigned long int)&(DWordBuffer), 4);

    /* FLOAT32 PressConv_Up_Tr_2_AXIS */
    DWordBuffer = N_TO_HUDINT(pFromServer->Payload[60]);
    brsmemcpy((unsigned long int)&(pSimInputs->PressConv_Up_Tr_2_AXIS), (unsigned long int)&(DWordBuffer), 4);

    /* BOOL PressConv_Up_Tr_2_LBK */
    pSimInputs->PressConv_Up_Tr_2_LBK = (pFromServer->Payload[61] != 0);

    /* BOOL PressConv_Up_Tr_2_LFR */
    pSimInputs->PressConv_Up_Tr_2_LFR = (pFromServer->Payload[62] != 0);

    /* FLOAT32 PressConv_Up_Tr_1_AXIS */
    DWordBuffer = N_TO_HUDINT(pFromServer->Payload[63]);
    brsmemcpy((unsigned long int)&(pSimInputs->PressConv_Up_Tr_1_AXIS), (unsigned long int)&(DWordBuffer), 4);

    /* BOOL PressConv_Up_Tr_1_LBK */
    pSimInputs->PressConv_Up_Tr_1_LBK = (pFromServer->Payload[64] != 0);

    /* BOOL PressConv_Up_Tr_1_LFR */
    pSimInputs->PressConv_Up_Tr_1_LFR = (pFromServer->Payload[65] != 0);

    /* FLOAT32 product_stop_AXIS */
    DWordBuffer = N_TO_HUDINT(pFromServer->Payload[66]);
    brsmemcpy((unsigned long int)&(pSimInputs->product_stop_AXIS), (unsigned long int)&(DWordBuffer), 4);

    /* BOOL product_stop_LBK */
    pSimInputs->product_stop_LBK = (pFromServer->Payload[67] != 0);

    /* BOOL product_stop_LFR */
    pSimInputs->product_stop_LFR = (pFromServer->Payload[68] != 0);

    /* FLOAT32 InfeedConveyor_ConvActualSpeed */
    DWordBuffer = N_TO_HUDINT(pFromServer->Payload[69]);
    brsmemcpy((unsigned long int)&(pSimInputs->InfeedConveyor_ConvActualSpeed), (unsigned long int)&(DWordBuffer), 4);

    /* BOOL Infeed_Sensor_1_IR */
    pSimInputs->Infeed_Sensor_1_IR = (pFromServer->Payload[70] != 0);

    /* BOOL Infeed_Sensor_2_IR */
    pSimInputs->Infeed_Sensor_2_IR = (pFromServer->Payload[71] != 0);


return N_TO_HUDINT((pFromServer->Header).Counter); // return counter
}
