
PROGRAM _INIT
//	Cuttinghead.LiftingDevicePosition[0]:= 30.;	// Hubantrieb rechts - Lift Drive Right 
//	Cuttinghead.LiftingDevicePosition[1]:= 30.;	// Hubantrieb links - Lift Drive Left
//	
//	D_Feed.MpLink := ADR(gAxFeed);
//	D_Feed.Parameters := ADR(D_FeedPar);
	 
END_PROGRAM

PROGRAM _CYCLIC

	
   	// ===========================================================
   	//     Datapoints for station Infeed
   	// ===========================================================

	
	// ===== axis =====
	
	// ----- EntryBelt ----- 
	
	PV_xgetadr(ADR('m_entrybelt:M_EntryBelt.Parameter.Velocity'), ADR(pVelocity), ADR(dmyLengh)); 
	rVelocity ACCESS pVelocity;
	InfeedStation.EntryBeltConveyorSpeed 	:= rVelocity;
	ComTCP_SimOutputs.InfeedConveyor_ACC  	:= 3;
	ComTCP_SimOutputs.InfeedConveyor_DEC	:= 3;
	ComTCP_SimOutputs.InfeedConveyor_FWD	:= 1;
	ComTCP_SimOutputs.InfeedConveyor_BWD	:= 0;
	ComTCP_SimOutputs.InfeedConveyor_MAX_VEL := InfeedStation.EntryBeltConveyorSpeed * 0.001; 

	// ===== digital inputs =====
			
	// ----- product detetction ----- Questions
	InfeedStation.ProductDetection[0] := ComTCP_SimInputs.Infeed_Sensor_1_IR ;
	InfeedStation.ProductDetection[1] := ComTCP_SimInputs.Infeed_Sensor_2_IR;
	
	
	//	 ===========================================================
	//	     Datapoints for Loading System 
	//	 ===========================================================
	
	
	//	 ===== axis =====	
	
	// ----- synchronous belt ----- 
	
	//		PV_xgetadr(ADR('m_SynchBelt:D_SynchBelt.Parameter.Position'), ADR(pPosition), ADR(dmyLengh));
	//		rPosition ACCESS pPosition;
	//		LoadingStation.SyncConveyorPosition := rPosition;

	PV_xgetadr(ADR('m_synchconv:D_SynchConv.Status.IsMoving'), ADR(pMoveActive), ADR(dmyLengh));  
	rMoveActive ACCESS pMoveActive;
	LoadingStation.SyncConveyorIsMoving := rMoveActive;
	
	PV_xgetadr(ADR('m_synchconv:M_SynchConv.Parameter.Velocity'), ADR(pVelocity), ADR(dmyLengh));  //change to D_SynchBelt.Parameter.Velocity when commands are passed to driver
	rVelocity ACCESS pVelocity;
	LoadingStation.SyncConveyorSpeed := rVelocity;
	
	ComTCP_SimOutputs.FeedConv_B_ACC := 10;
	ComTCP_SimOutputs.FeedConv_B_DEC := 10;
	ComTCP_SimOutputs.FeedConv_B_FWD := 1;
	ComTCP_SimOutputs.FeedConv_B_BWD := 0;
	ComTCP_SimOutputs.FeedConv_B_MAX_VEL := LoadingStation.SyncConveyorSpeed * 0.001;
				
	//	IF Loading.SynchronConveyorMoving THEN
	//	ComTCP_SimOutputs.FeedConv_B_MAX_VEL := Loading.SynchronConveyorSpeed * 0.001;	
	//	END_IF
	
	// ----- product tray lift -----
	
	PV_xgetadr(ADR('m_traylift:D_TrayLift.Parameter.Position'), ADR(pPosition), ADR(dmyLengh)); //D_TrayLift.Status.Position
	rPosition ACCESS pPosition;
	LoadingStation.TrayLiftPosition := rPosition;
	ComTCP_SimOutputs.Prod_Load_tray_TGT_POS :=  LREAL_TO_REAL(LoadingStation.TrayLiftPosition * 0.05); // Conversion from Linear Travel of Belt to the Angle of the Lifting Device
	ComTCP_SimOutputs.Prod_Load_tray_MAX_SPD := 50;
	ComTCP_SimOutputs.Prod_Load_tray_ACC	 := 150;
	ComTCP_SimOutputs.Prod_Load_tray_DEC	 := 150;
	
	PV_xgetadr(ADR('m_traylift:D_TrayLift.Status.IsMoving'), ADR(pMoveActive), ADR(dmyLengh));  //edited by DS Auto Solutions
	rMoveActive ACCESS pMoveActive;
	LoadingStation.TrayLiftIsMoving:= rMoveActive;

	//	IF LoadingStation.TrayLiftIsMoving THEN
	//	ComTCP_SimOutputs.Prod_Load_tray_TGT_POS :=  LREAL_TO_REAL(LoadingStation.TrayLiftPosition * 0.05);
	//	END_IF


	// ===== digital outputs =====
	
	// ----- entry slider -----
	PV_xgetadr(ADR('m_infeedslider:m_infeedslider.Command.Slider.Close'), ADR(pBool), ADR(dmyLengh));
	rBool ACCESS pBool;
	LoadingStation.EntrySliderClose := rBool;
	
	PV_xgetadr(ADR('m_infeedslider:m_infeedslider.Command.Slider.Open'), ADR(pBool), ADR(dmyLengh));
	rBool ACCESS pBool;
	LoadingStation.EntrySliderOpen := rBool;
		
	ComTCP_SimOutputs.InfeedDoor_FWD := LoadingStation.EntrySliderClose;
	ComTCP_SimOutputs.InfeedDoor_BWD := LoadingStation.EntrySliderOpen;
	ComTCP_SimOutputs.InfeedDoor_MAX_SPD := 0.4;			//speed of travel

	// ===== digital inputs =====	
	
	// ----- entry slider -----
	
	LoadingStation.EntrySliderIsClose := ComTCP_SimInputs.InfeedDoor_LFR;
	
	
	// ===========================================================
	//     Datapoints for Feeding System 
	// ===========================================================
	
	
	// ===== axis =====	

	// ----- feed carriage -----

	PV_xgetadr(ADR('m_feedcarriage:M_FeedCarriage.Parameter.Position'), ADR(pPosition), ADR(dmyLengh));
	rPosition ACCESS pPosition;
	FeedingStation.FeedPosition := rPosition;
	ComTCP_SimOutputs.Feed_Carr_M_MAX_SPD := 2500;
	ComTCP_SimOutputs.Feed_Carr_M_ACC	  := 10000;
	ComTCP_SimOutputs.Feed_Carr_M_DEC	:=  10000;
	
	PV_xgetadr(ADR('m_feedcarriage:M_FeedCarriage.Status.Axis.IsMoving'), ADR(pMoveActive), ADR(dmyLengh));  //edited by DS Auto Solutions
	rMoveActive ACCESS pMoveActive;
	FeedingStation.FeedIsMoving:= rMoveActive;
	
	IF FeedingStation.FeedIsMoving THEN
		ComTCP_SimOutputs.Feed_Carr_M_TGT_POS:= LREAL_TO_REAL(FeedingStation.FeedPosition);
	END_IF
	
	// ----- lower traction -----
		
	PV_xgetadr(ADR('m_lowertractionsystem:M_LowerTractionSystem.Status.Axis.IsMoving'), ADR(pMoveActive), ADR(dmyLengh));  //edited by DS Auto Solutions
	rMoveActive ACCESS pMoveActive;
	FeedingStation.LowerTractionIsMoving:= rMoveActive;

	PV_xgetadr(ADR('m_lowertractionsystem:M_LowerTractionSystem.Parameter.Velocity'), ADR(pVelocity), ADR(dmyLengh));  //edited by DS Auto Solutions
	rVelocity ACCESS pVelocity;
	FeedingStation.LowerTractionSpeed := rVelocity;	
	ComTCP_SimOutputs.L_Tr_Shaft1ACC		:= 10;
	ComTCP_SimOutputs.L_Tr_Shaft1MAX_VEL	:= FeedingStation.LowerTractionSpeed   * 0.001;
	ComTCP_SimOutputs.L_Tr_Shaft1DEC		:= 10;
	ComTCP_SimOutputs.L_Tr_Shaft1FWD		:= 1;
	ComTCP_SimOutputs.L_Tr_Shaft1BWD		:= 0;
	
	//	IF Feeding.LowerTractionMoving THEN													
	//		ComTCP_SimOutputs.L_Tr_Shaft1MAX_VEL	:= Feeding.LowerTractionSpeed  * 0.001;
	//	END_IF
	
	// ----- upper traction -----
	
	PV_xgetadr(ADR('m_uppertractionsystem:M_UpperTractionSystem.Status.Axis.IsMoving'), ADR(pMoveActive), ADR(dmyLengh));  
	rMoveActive ACCESS pMoveActive;
	FeedingStation.UpperTractionIsMoving:= rMoveActive;
	
	PV_xgetadr(ADR('m_uppertractionsystem:M_UpperTractionSystem.Parameter.Velocity'), ADR(pVelocity), ADR(dmyLengh));
	rVelocity ACCESS pVelocity;
	FeedingStation.UpperTractionSpeed := rVelocity;
	
	ComTCP_SimOutputs.Up_Tr_Belt4_MAX_VEL	:= FeedingStation.UpperTractionSpeed * 0.001;
	ComTCP_SimOutputs.Up_Tr_Belt4_ACC		:= 10;
	ComTCP_SimOutputs.Up_Tr_Belt4_DEC		:= 10;
	ComTCP_SimOutputs.Up_Tr_Belt4_FWD		:= 1;
	ComTCP_SimOutputs.Up_Tr_Belt4_BWD   	:= 0;
	
	ComTCP_SimOutputs.Up_Tr_Belt3_MAX_VEL	:= FeedingStation.UpperTractionSpeed * 0.001;
	ComTCP_SimOutputs.Up_Tr_Belt3_ACC		:= 10;
	ComTCP_SimOutputs.Up_Tr_Belt3_DEC		:= 10;
	ComTCP_SimOutputs.Up_Tr_Belt3_FWD		:= 1;
	ComTCP_SimOutputs.Up_Tr_Belt3_BWD   	:= 0;
	
	// ===== digital outputs =====
	
	// ----- upper traction cylinder -----
	PV_xgetadr(ADR('m_uppertractionsystem:M_UpperTractionSystem.Command.PressingDevice.Lift'), ADR(pBool), ADR(dmyLengh));
	rBool ACCESS pBool;
	FeedingStation.UpperTractionLift := rBool;
		
	PV_xgetadr(ADR('m_uppertractionsystem:M_UpperTractionSystem.Command.PressingDevice.Lower'), ADR(pBool), ADR(dmyLengh));
	rBool ACCESS pBool;
	FeedingStation.UpperTractionLower := rBool;
	
	ComTCP_SimOutputs.PressConv_Up_Tr_1_FWD := FeedingStation.UpperTractionLift; // Monostable cylinder
	ComTCP_SimOutputs.PressConv_Up_Tr_1_MAX_SPD := 0.7;
	ComTCP_SimOutputs.PressConv_Up_Tr_2_FWD := FeedingStation.UpperTractionLift; // Monostable cylinder
	ComTCP_SimOutputs.PressConv_Up_Tr_2_MAX_SPD := 0.7;
	
	ComTCP_SimOutputs.PressConv_Up_Tr_3_FWD := FeedingStation.UpperTractionLift; // Monostable cylinder
	ComTCP_SimOutputs.PressConv_Up_Tr_3_MAX_SPD := 0.7;
	ComTCP_SimOutputs.PressConv_Up_Tr_4_FWD := FeedingStation.UpperTractionLift; // Monostable cylinder
	ComTCP_SimOutputs.PressConv_Up_Tr_4_MAX_SPD := 0.7;

	// ----- product gripper -----	
	
	PV_xgetadr(ADR('m_prodgrip:M_ProductGripper.Command.Gripper.Open'), ADR(pBool), ADR(dmyLengh));
	rBool ACCESS pBool;
	FeedingStation.GripperOpen := rBool;
	ComTCP_SimOutputs.Claw_Line3_GRIPPPER_lANE3_OPEN := FeedingStation.GripperOpen;
	
	PV_xgetadr(ADR('m_prodgrip:M_ProductGripper.Command.Gripper.Close'), ADR(pBool), ADR(dmyLengh));
	rBool ACCESS pBool;
	FeedingStation.GripperClose := rBool;	
	ComTCP_SimOutputs.Claw_Line3_GRIPPPER_lANE3_CLOSE := FeedingStation.GripperClose;
			
	PV_xgetadr(ADR('m_prodgrip:M_ProductGripper.Status.Ejector.IsOut'), ADR(pBool), ADR(dmyLengh));
	rBool ACCESS pBool;
	FeedingStation.EjectorIsOut := rBool;
	ComTCP_SimOutputs.Ejector_Ram_3_FWD := FeedingStation.EjectorIsOut ;
	ComTCP_SimOutputs.Ejector_Ram_3_MAX_SPD := 0.25; // Speed in m/s to do the Ejector action

	// ----- product stopper -----	
	
	PV_xgetadr(ADR('m_prodstop:M_ProductStop.Command.Stoper.Up'), ADR(pBool), ADR(dmyLengh));
	rBool ACCESS pBool;
	FeedingStation.ProductStopUp := rBool;
	ComTCP_SimOutputs.Claw_Line3_GRIPPPER_lANE3_OPEN := FeedingStation.GripperOpen;
	
	PV_xgetadr(ADR('m_prodstop:M_ProductStop.Command.Stoper.Down'), ADR(pBool), ADR(dmyLengh));
	rBool ACCESS pBool;
	FeedingStation.ProductStopDown := rBool;	
	ComTCP_SimOutputs.Claw_Line3_GRIPPPER_lANE3_CLOSE := FeedingStation.GripperClose;
			
	PV_xgetadr(ADR('m_prodstop:M_ProductStop.Status.Stoper.IsDown'), ADR(pBool), ADR(dmyLengh));
	rBool ACCESS pBool;
	FeedingStation.ProductStopIsDown := rBool;
	ComTCP_SimOutputs.product_stop_FWD := FeedingStation.ProductStopDown ;	
	ComTCP_SimOutputs.product_stop_BWD := FeedingStation.ProductStopUp ;
	ComTCP_SimOutputs.product_stop_MAX_SPD := 0.25; // Speed in m/s to do the Ejector action

	
	//// ===== Datapoints for Cuttinghead =====
	//	// acopos  
	//	PV_xgetadr(ADR('motion:D_Rotor.Position'), ADR(pPosition), ADR(dmyLengh));
	//	rPosition ACCESS pPosition;
	//	Cuttinghead.RotorPosition := rPosition;
	//		
	//	PV_xgetadr(ADR('motion:D_Rotor.MoveActive'), ADR(pMoveActive), ADR(dmyLengh));  //edited by DS Auto Solutions
	//	rMoveActive ACCESS pMoveActive;
	//	Cuttinghead.RotorMoving := rMoveActive;	
	//	
	//	IF Cuttinghead.RotorMoving THEN
	//	ComTCP_SimOutputs.Rotor_ACT_POS := LREAL_TO_REAL(Cuttinghead.RotorPosition);
	//	END_IF	
	//	
	//	PV_xgetadr(ADR('motion:D_Idlecut.Position'), ADR(pPosition), ADR(dmyLengh));
	//	rPosition ACCESS pPosition;
	//	Cuttinghead.IdlecutSystemPosition := rPosition;
	//	
	//	// pwmposition - im watch setzten
	//	PV_xgetadr(ADR('motion:D_Idlecut.Position'), ADR(pPosition), ADR(dmyLengh));
	//	rPosition ACCESS pPosition;
	//	Cuttinghead.LiftingDevicePosition[0];	// Hubantrieb rechts
	//	Cuttinghead.LiftingDevicePosition[1];	// Hubantrieb links
	//	// digital output
	//	Cuttinghead.RotorLock;
	//	Cuttinghead.RotorUnlock;
	//	//digital input
	//	Cuttinghead.LiftingDeviceTopPosition[0];
	//	Cuttinghead.LiftingDeviceTopPosition[1];
	//	Cuttinghead.LiftingDeviceImpulse[0];
	//	Cuttinghead.LiftingDeviceImpulse[1];
	//	Cuttinghead.RotorIsUnlocked;
	//	
	
	
	// ===========================================================
	//     Datapoints for station Infeed
	// ===========================================================

	
	// ===== axis =====
	
	// ----- Portioning Conveyor ----- 
	
	PV_xgetadr(ADR('m_portconv:M_PortioningConv.Status.Axis.IsMoving'), ADR(pMoveActive), ADR(dmyLengh));  
	rMoveActive ACCESS pMoveActive;
	PortioningStation.PortioningConveyorIsMoving:= rMoveActive;
	
	PV_xgetadr(ADR('m_portconv:M_PortioningConv.Parameter.Velocity'), ADR(pVelocity), ADR(dmyLengh));  //edited by DS Auto Solutions
	rVelocity ACCESS pVelocity;
	PortioningStation.PortioningConveyorVelocity:= rVelocity;
	ComTCP_SimOutputs.PortionConv_Belt_ACC 		:= 100;
	ComTCP_SimOutputs.PortionConv_Belt_DEC			:= 100;
	ComTCP_SimOutputs.PortionConv_Belt_FWD 		:= 1;
	ComTCP_SimOutputs.PortionConv_Belt_BWD  	 	:=0;
	ComTCP_SimOutputs.PortionConv_Belt_MAX_VEL 	:= PortioningStation.PortioningConveyorVelocity* 0.001;
	
	//	IF PortioningStation.PortioningConveyorIsMoving THEN
	//	ComTCP_SimOutputs.PortionConv_Belt_MAX_VEL := PortioningSystem.PortioningConvSpeed * 0.001;
	//	END_IF
					
	PV_xgetadr(ADR('m_intermconv:M_IntermediateConv.Status.Axis.IsMoving'), ADR(pMoveActive), ADR(dmyLengh));  //edited by DS Auto Solutions
	rMoveActive ACCESS pMoveActive;
	PortioningStation.IntermConveyorIsMoving:= rMoveActive;
		
	PV_xgetadr(ADR('m_intermconv:M_IntermediateConv.Parameter.Velocity'), ADR(pVelocity), ADR(dmyLengh));  //edited by DS Auto Solutions
	rVelocity ACCESS pVelocity;
	PortioningStation.IntermConveyorVelocity := rVelocity;
	ComTCP_SimOutputs.InterConv_B_MAX_VEL	:= PortioningStation.IntermConveyorVelocity *0.001;
	ComTCP_SimOutputs.InterConv_B_ACC	:= 100;
	ComTCP_SimOutputs.InterConv_B_DEC	:= 100;
	ComTCP_SimOutputs.InterConv_B_FWD	:= 1;
	ComTCP_SimOutputs.InterConv_B_BWD	:= 0;
	
	//	IF PortioningSystem.IntermediateConvMoving THEN
	//		ComTCP_SimOutputs.InterConv_B_MAX_VEL	:= PortioningSystem.IntermediateConvSpeed *0.001;
	//	END_IF
	
	PV_xgetadr(ADR('m_transfer:M_TransferConv.Status.Axis.IsMoving '), ADR(pMoveActive), ADR(dmyLengh));  //edited by DS Auto Solutions
	rMoveActive ACCESS pMoveActive;
	PortioningStation.TransferConveyorIsMoving:= rMoveActive;
		
	PV_xgetadr(ADR('m_transfer:M_TransferConv.Parameter.Velocity'), ADR(pVelocity), ADR(dmyLengh));  //edited by DS Auto Solutions
	rVelocity ACCESS pVelocity;
	PortioningStation.TransferConveyorVelocity  := rVelocity;
	ComTCP_SimOutputs.TransferConv_B_ACC	:= 100;
	ComTCP_SimOutputs.TransferConv_B_DEC	:= 100;
	ComTCP_SimOutputs.TransferConv_B_FWD	:=1;
	ComTCP_SimOutputs.TransferConv_B_BWD	:= 0;
	ComTCP_SimOutputs.TransferConv_B_MAX_VEL := PortioningStation.TransferConveyorVelocity * 0.001;
	
	ComTCP_SimOutputs.TransferConv_B_2_ACC	:= 100;
	ComTCP_SimOutputs.TransferConv_B_2_DEC	:= 100;
	ComTCP_SimOutputs.TransferConv_B_2_FWD	:=1;
	ComTCP_SimOutputs.TransferConv_B_2_BWD	:= 0;
	ComTCP_SimOutputs.TransferConv_B_2_MAX_VEL := PortioningStation.TransferConveyorVelocity * 0.001;
	
	//	IF PortioningStation.TransferConveyorIsMoving THEN
	//	ComTCP_SimOutputs.TransferConv_B_MAX_VEL := PortioningStation.TransferConveyorVelocity * 0.001;
	//	END_IF
	//
	//	PV_xgetadr(ADR('motion:D_StackDevice.Position'), ADR(pPosition), ADR(dmyLengh));	
	//	rPosition ACCESS pPosition;
	//	PortioningSystem.StackingDevicePosition := rPosition;
	//		
	//	PV_xgetadr(ADR('motion:D_StackDevice.MoveActive'), ADR(pMoveActive), ADR(dmyLengh));  //edited by DS Auto Solutions
	//	rMoveActive ACCESS pMoveActive;
	//	PortioningSystem.StackingDeviceMoving:= rMoveActive;
	//	ComTCP_SimOutputs.StackingDevice_Housing_MAX_SPD := 560;
	//	ComTCP_SimOutputs.StackingDevice_Housing_ACC     := 680;
	//	ComTCP_SimOutputs.StackingDevice_Housing_DEC	 := 680;
	//
	//	IF PortioningSystem.StackingDeviceMoving THEN
	//	ComTCP_SimOutputs.StackingDevice_Housing_TGT_POS := LREAL_TO_REAL (0.22725*PortioningSystem.StackingDevicePosition - 10.22); // Converting the displacement of linear motor to the angular movement of housing
	//	END_IF
	//	
	//	// Datapoints for Interleaver
	//		
	//	// Datapoints for Checkweigher
	//	Checkweigher.ConveyorPosition;
	//	Checkweigher.ProductDetected[0];	// Track 1
	//	Checkweigher.ProductDetected[1];	// Track 2
	//	Checkweigher.ProductDetected[2];	// Track 3
	//	Checkweigher.ProductDetected[3];	// Track 4
	
	 
END_PROGRAM

PROGRAM _EXIT
// IP Import
//	#include <MNG_PackageTyp.h>
//	#include <MNG_PackageVAR.h>
//	#include <TypesTYP.h>
//	#include <VariablesVAR.h>
//	unsigned long bur_heap_size = 0x4FFFF;
END_PROGRAM

