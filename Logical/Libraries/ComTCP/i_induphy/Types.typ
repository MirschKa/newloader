
TYPE
	Infeed_Induphy_Typ : 	STRUCT 
		EntryBeltConveyorPosition : LREAL;
		ProductDetection : ARRAY[0..3]OF BOOL;
		EntryBeltConveyorSpeed : REAL;
		EntryBeltConveyorIsMoving : BOOL;
	END_STRUCT;
	Portioning_Induphy_Typ : 	STRUCT 
		PortioningConveyorPosition : LREAL;
		PortioningConveyorVelocity : REAL;
		PortioningConveyorIsMoving : BOOL;
		IntermConveyorIsMoving : BOOL;
		TransferConveyorIsMoving : BOOL;
		IntermConveyorVelocity : REAL;
		TransferConveyorVelocity : REAL;
	END_STRUCT;
	Loading_Induphy_Typ : 	STRUCT 
		EntrySliderClose : BOOL;
		EntrySliderOpen : BOOL;
		EntrySliderIsClose : BOOL;
		TrayLiftPosition : LREAL;
		TrayLiftIsMoving : BOOL;
		SyncConveyorSpeed : REAL;
		SyncConveyorPosition : LREAL;
		SyncConveyorIsMoving : BOOL;
	END_STRUCT;
	Feeding_Induphy_Typ : 	STRUCT 
		FeedPosition : LREAL;
		FeedIsMoving : BOOL;
		UpperTractionSpeed : REAL;
		UpperTractionIsMoving : BOOL;
		LowerTractionSpeed : REAL;
		LowerTractionIsMoving : BOOL;
		GripperClose : BOOL;
		GripperOpen : BOOL;
		GripperIsClose : BOOL;
		ProductStopUp : BOOL;
		ProductStopDown : BOOL;
		ProductStopIsDown : BOOL;
		EjectorOut : BOOL;
		EjectorIsOut : BOOL;
		UpperTractionLift : BOOL;
		UpperTractionLower : BOOL;
		UpperTractionIsLower : BOOL;
		UpperTractionIsLifted : BOOL;
	END_STRUCT;
END_TYPE
