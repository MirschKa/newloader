(*                                                                            *)
(* File generated by the B&R AS 4 control config generator                    *)
(*                                                                            *)
(*     Provider: machineering                                                 *)
(*  Application: industrialPhysics_2.4.master                                 *)
(* Architecture: Windows 64 Bit                                               *)
(*      Version: 2.4.11382.master                                             *)
(*        Build: 938230eb3                                                    *)
(*         Date: 2020-09-04, Time: 09:42:38                                   *)
(*                                                                            *)

TYPE
	ComTCP_INPUTS_32BIT : 	STRUCT 
		NumSignals : INT := 71;
		Prod_Load_tray_AXIS : REAL := 0.0; (* REAL Prod_Load_tray_AXIS *)
		Prod_Load_tray_LBK : BOOL := 0; (* BOOL Prod_Load_tray_LBK *)
		Prod_Load_tray_LFR : BOOL := 0; (* BOOL Prod_Load_tray_LFR *)
		Prod_Load_tray_ACT_SPD : REAL := 0.0; (* REAL Prod_Load_tray_ACT_SPD *)
		Prod_Load_tray_RCHD : BOOL := 0; (* BOOL Prod_Load_tray_RCHD *)
		Prod_Load_tray_DLT_POS : REAL := 0.0; (* REAL Prod_Load_tray_DLT_POS *)
		Prod_Load_tray_KinState : DINT := 0; (* DINT Prod_Load_tray_KinState *)
		FeedConv_B_ConvActualSpeed : REAL := 0.0; (* REAL FeedConv_B_ConvActualSpeed *)
		grinder_head_RH_AXIS : REAL := 0.0; (* REAL grinder_head_RH_AXIS *)
		grinder_head_RH_LBK : BOOL := 0; (* BOOL grinder_head_RH_LBK *)
		grinder_head_RH_LFR : BOOL := 0; (* BOOL grinder_head_RH_LFR *)
		grinder_head_RH_ACT_SPD : REAL := 0.0; (* REAL grinder_head_RH_ACT_SPD *)
		grinder_head_RH_RCHD : BOOL := 0; (* BOOL grinder_head_RH_RCHD *)
		grinder_head_RH_DLT_POS : REAL := 0.0; (* REAL grinder_head_RH_DLT_POS *)
		grinder_head_RH_KinState : DINT := 0; (* DINT grinder_head_RH_KinState *)
		StackingDevice_Housing_AXIS : REAL := 0.0; (* REAL StackingDevice_Housing_AXIS *)
		StackingDevice_Housing_LBK : BOOL := 0; (* BOOL StackingDevice_Housing_LBK *)
		StackingDevice_Housing_LFR : BOOL := 0; (* BOOL StackingDevice_Housing_LFR *)
		StackingDevice_Housing_ACT_SPD : REAL := 0.0; (* REAL StackingDevice_Housing_ACT_SPD *)
		StackingDevice_Housing_RCHD : BOOL := 0; (* BOOL StackingDevice_Housing_RCHD *)
		StackingDevice_Housing_DLT_POS : REAL := 0.0; (* REAL StackingDevice_Housing_DLT_POS *)
		StackingDevice_Housing_KinState : DINT := 0; (* DINT StackingDevice_Housing_KinState *)
		InterConv_B_ConvActualSpeed : REAL := 0.0; (* REAL InterConv_B_ConvActualSpeed *)
		PortionConv_Belt_ConvActualSpeed : REAL := 0.0; (* REAL PortionConv_Belt_ConvActualSpeed *)
		TransferConv_B_ConvActualSpeed : REAL := 0.0; (* REAL TransferConv_B_ConvActualSpeed *)
		TransferConv_B_2_ConvActualSpeed : REAL := 0.0; (* REAL TransferConv_B_2_ConvActualSpeed *)
		Feed_Carr_M_AXIS : REAL := 0.0; (* REAL Feed_Carr_M_AXIS *)
		Feed_Carr_M_LBK : BOOL := 0; (* BOOL Feed_Carr_M_LBK *)
		Feed_Carr_M_LFR : BOOL := 0; (* BOOL Feed_Carr_M_LFR *)
		Feed_Carr_M_ACT_SPD : REAL := 0.0; (* REAL Feed_Carr_M_ACT_SPD *)
		Feed_Carr_M_RCHD : BOOL := 0; (* BOOL Feed_Carr_M_RCHD *)
		Feed_Carr_M_DLT_POS : REAL := 0.0; (* REAL Feed_Carr_M_DLT_POS *)
		Feed_Carr_M_KinState : DINT := 0; (* DINT Feed_Carr_M_KinState *)
		Ejector_Ram_3_AXIS : REAL := 0.0; (* REAL Ejector_Ram_3_AXIS *)
		Ejector_Ram_3_LBK : BOOL := 0; (* BOOL Ejector_Ram_3_LBK *)
		Ejector_Ram_3_LFR : BOOL := 0; (* BOOL Ejector_Ram_3_LFR *)
		Claw_Line3_GRIPPPER_lANE3_CLOSED : BOOL := 0; (* BOOL Claw_Line3_GRIPPPER_lANE3_CLOSED *)
		L_Tr_Shaft1ConvActualSpeed : REAL := 0.0; (* REAL L_Tr_Shaft1ConvActualSpeed *)
		InfeedDoor_AXIS : REAL := 0.0; (* REAL InfeedDoor_AXIS *)
		InfeedDoor_LBK : BOOL := 0; (* BOOL InfeedDoor_LBK *)
		InfeedDoor_LFR : BOOL := 0; (* BOOL InfeedDoor_LFR *)
		Rotor_IdleCut_AXIS : REAL := 0.0; (* REAL Rotor_IdleCut_AXIS *)
		Rotor_IdleCut_LBK : BOOL := 0; (* BOOL Rotor_IdleCut_LBK *)
		Rotor_IdleCut_LFR : BOOL := 0; (* BOOL Rotor_IdleCut_LFR *)
		Rotor_IdleCut_ACT_SPD : REAL := 0.0; (* REAL Rotor_IdleCut_ACT_SPD *)
		Rotor_IdleCut_RCHD : BOOL := 0; (* BOOL Rotor_IdleCut_RCHD *)
		Rotor_IdleCut_DLT_POS : REAL := 0.0; (* REAL Rotor_IdleCut_DLT_POS *)
		Rotor_IdleCut_KinState : DINT := 0; (* DINT Rotor_IdleCut_KinState *)
		Rotor_AXIS : REAL := 0.0; (* REAL Rotor_AXIS *)
		Rotor_LBK : BOOL := 0; (* BOOL Rotor_LBK *)
		Rotor_LFR : BOOL := 0; (* BOOL Rotor_LFR *)
		PressConv_Up_Tr_3_AXIS : REAL := 0.0; (* REAL PressConv_Up_Tr_3_AXIS *)
		PressConv_Up_Tr_3_LBK : BOOL := 0; (* BOOL PressConv_Up_Tr_3_LBK *)
		PressConv_Up_Tr_3_LFR : BOOL := 0; (* BOOL PressConv_Up_Tr_3_LFR *)
		Up_Tr_Belt3_ConvActualSpeed : REAL := 0.0; (* REAL Up_Tr_Belt3_ConvActualSpeed *)
		PressConv_Up_Tr_4_AXIS : REAL := 0.0; (* REAL PressConv_Up_Tr_4_AXIS *)
		PressConv_Up_Tr_4_LBK : BOOL := 0; (* BOOL PressConv_Up_Tr_4_LBK *)
		PressConv_Up_Tr_4_LFR : BOOL := 0; (* BOOL PressConv_Up_Tr_4_LFR *)
		Up_Tr_Belt4_ConvActualSpeed : REAL := 0.0; (* REAL Up_Tr_Belt4_ConvActualSpeed *)
		PressConv_Up_Tr_2_AXIS : REAL := 0.0; (* REAL PressConv_Up_Tr_2_AXIS *)
		PressConv_Up_Tr_2_LBK : BOOL := 0; (* BOOL PressConv_Up_Tr_2_LBK *)
		PressConv_Up_Tr_2_LFR : BOOL := 0; (* BOOL PressConv_Up_Tr_2_LFR *)
		PressConv_Up_Tr_1_AXIS : REAL := 0.0; (* REAL PressConv_Up_Tr_1_AXIS *)
		PressConv_Up_Tr_1_LBK : BOOL := 0; (* BOOL PressConv_Up_Tr_1_LBK *)
		PressConv_Up_Tr_1_LFR : BOOL := 0; (* BOOL PressConv_Up_Tr_1_LFR *)
		product_stop_AXIS : REAL := 0.0; (* REAL product_stop_AXIS *)
		product_stop_LBK : BOOL := 0; (* BOOL product_stop_LBK *)
		product_stop_LFR : BOOL := 0; (* BOOL product_stop_LFR *)
		InfeedConveyor_ConvActualSpeed : REAL := 0.0; (* REAL InfeedConveyor_ConvActualSpeed *)
		Infeed_Sensor_1_IR : BOOL := 0; (* BOOL Infeed_Sensor_1_IR *)
		Infeed_Sensor_2_IR : BOOL := 0; (* BOOL Infeed_Sensor_2_IR *)
	END_STRUCT;
	ComTCP_OUTPUTS_32BIT : 	STRUCT 
		NumSignals : INT := 91;
		Prod_Load_tray_MAX_SPD : REAL := 0.0; (* REAL Prod_Load_tray_MAX_SPD *)
		Prod_Load_tray_ACC : REAL := 0.0; (* REAL Prod_Load_tray_ACC *)
		Prod_Load_tray_DEC : REAL := 0.0; (* REAL Prod_Load_tray_DEC *)
		Prod_Load_tray_TGT_POS : REAL := 0.0; (* REAL Prod_Load_tray_TGT_POS *)
		FeedConv_B_MAX_VEL : REAL := 0.0; (* REAL FeedConv_B_MAX_VEL *)
		FeedConv_B_ACC : REAL := 0.0; (* REAL FeedConv_B_ACC *)
		FeedConv_B_DEC : REAL := 0.0; (* REAL FeedConv_B_DEC *)
		FeedConv_B_FWD : BOOL := 0; (* BOOL FeedConv_B_FWD *)
		FeedConv_B_BWD : BOOL := 0; (* BOOL FeedConv_B_BWD *)
		grinder_head_RH_MAX_SPD : REAL := 0.0; (* REAL grinder_head_RH_MAX_SPD *)
		grinder_head_RH_ACC : REAL := 0.0; (* REAL grinder_head_RH_ACC *)
		grinder_head_RH_DEC : REAL := 0.0; (* REAL grinder_head_RH_DEC *)
		grinder_head_RH_TGT_POS : REAL := 0.0; (* REAL grinder_head_RH_TGT_POS *)
		StackingDevice_Housing_MAX_SPD : REAL := 0.0; (* REAL StackingDevice_Housing_MAX_SPD *)
		StackingDevice_Housing_ACC : REAL := 0.0; (* REAL StackingDevice_Housing_ACC *)
		StackingDevice_Housing_DEC : REAL := 0.0; (* REAL StackingDevice_Housing_DEC *)
		StackingDevice_Housing_TGT_POS : REAL := 0.0; (* REAL StackingDevice_Housing_TGT_POS *)
		InterConv_B_MAX_VEL : REAL := 0.0; (* REAL InterConv_B_MAX_VEL *)
		InterConv_B_ACC : REAL := 0.0; (* REAL InterConv_B_ACC *)
		InterConv_B_DEC : REAL := 0.0; (* REAL InterConv_B_DEC *)
		InterConv_B_FWD : BOOL := 0; (* BOOL InterConv_B_FWD *)
		InterConv_B_BWD : BOOL := 0; (* BOOL InterConv_B_BWD *)
		PortionConv_Belt_MAX_VEL : REAL := 0.0; (* REAL PortionConv_Belt_MAX_VEL *)
		PortionConv_Belt_ACC : REAL := 0.0; (* REAL PortionConv_Belt_ACC *)
		PortionConv_Belt_DEC : REAL := 0.0; (* REAL PortionConv_Belt_DEC *)
		PortionConv_Belt_FWD : BOOL := 0; (* BOOL PortionConv_Belt_FWD *)
		PortionConv_Belt_BWD : BOOL := 0; (* BOOL PortionConv_Belt_BWD *)
		TransferConv_B_MAX_VEL : REAL := 0.0; (* REAL TransferConv_B_MAX_VEL *)
		TransferConv_B_ACC : REAL := 0.0; (* REAL TransferConv_B_ACC *)
		TransferConv_B_DEC : REAL := 0.0; (* REAL TransferConv_B_DEC *)
		TransferConv_B_FWD : BOOL := 0; (* BOOL TransferConv_B_FWD *)
		TransferConv_B_BWD : BOOL := 0; (* BOOL TransferConv_B_BWD *)
		TransferConv_B_2_MAX_VEL : REAL := 0.0; (* REAL TransferConv_B_2_MAX_VEL *)
		TransferConv_B_2_ACC : REAL := 0.0; (* REAL TransferConv_B_2_ACC *)
		TransferConv_B_2_DEC : REAL := 0.0; (* REAL TransferConv_B_2_DEC *)
		TransferConv_B_2_FWD : BOOL := 0; (* BOOL TransferConv_B_2_FWD *)
		TransferConv_B_2_BWD : BOOL := 0; (* BOOL TransferConv_B_2_BWD *)
		Feed_Carr_M_MAX_SPD : REAL := 0.0; (* REAL Feed_Carr_M_MAX_SPD *)
		Feed_Carr_M_ACC : REAL := 0.0; (* REAL Feed_Carr_M_ACC *)
		Feed_Carr_M_DEC : REAL := 0.0; (* REAL Feed_Carr_M_DEC *)
		Feed_Carr_M_TGT_POS : REAL := 0.0; (* REAL Feed_Carr_M_TGT_POS *)
		Ejector_Ram_3_MAX_SPD : REAL := 0.0; (* REAL Ejector_Ram_3_MAX_SPD *)
		Ejector_Ram_3_FWD : BOOL := 0; (* BOOL Ejector_Ram_3_FWD *)
		Ejector_Ram_3_KinInhibit : BOOL := 0; (* BOOL Ejector_Ram_3_KinInhibit *)
		Claw_Line3_GRIPPPER_lANE3_OPEN : BOOL := 0; (* BOOL Claw_Line3_GRIPPPER_lANE3_OPEN *)
		Claw_Line3_GRIPPPER_lANE3_CLOSE : BOOL := 0; (* BOOL Claw_Line3_GRIPPPER_lANE3_CLOSE *)
		L_Tr_Shaft1MAX_VEL : REAL := 0.0; (* REAL L_Tr_Shaft1MAX_VEL *)
		L_Tr_Shaft1ACC : REAL := 0.0; (* REAL L_Tr_Shaft1ACC *)
		L_Tr_Shaft1DEC : REAL := 0.0; (* REAL L_Tr_Shaft1DEC *)
		L_Tr_Shaft1FWD : BOOL := 0; (* BOOL L_Tr_Shaft1FWD *)
		L_Tr_Shaft1BWD : BOOL := 0; (* BOOL L_Tr_Shaft1BWD *)
		InfeedDoor_MAX_SPD : REAL := 0.0; (* REAL InfeedDoor_MAX_SPD *)
		InfeedDoor_FWD : BOOL := 0; (* BOOL InfeedDoor_FWD *)
		InfeedDoor_KinInhibit : BOOL := 0; (* BOOL InfeedDoor_KinInhibit *)
		InfeedDoor_BWD : BOOL := 0; (* BOOL InfeedDoor_BWD *)
		Rotor_IdleCut_MAX_SPD : REAL := 0.0; (* REAL Rotor_IdleCut_MAX_SPD *)
		Rotor_IdleCut_ACC : REAL := 0.0; (* REAL Rotor_IdleCut_ACC *)
		Rotor_IdleCut_DEC : REAL := 0.0; (* REAL Rotor_IdleCut_DEC *)
		Rotor_IdleCut_TGT_POS : REAL := 0.0; (* REAL Rotor_IdleCut_TGT_POS *)
		Rotor_ACT_POS : REAL := 0.0; (* REAL Rotor_ACT_POS *)
		PressConv_Up_Tr_3_MAX_SPD : REAL := 0.0; (* REAL PressConv_Up_Tr_3_MAX_SPD *)
		PressConv_Up_Tr_3_FWD : BOOL := 0; (* BOOL PressConv_Up_Tr_3_FWD *)
		PressConv_Up_Tr_3_KinInhibit : BOOL := 0; (* BOOL PressConv_Up_Tr_3_KinInhibit *)
		Up_Tr_Belt3_MAX_VEL : REAL := 0.0; (* REAL Up_Tr_Belt3_MAX_VEL *)
		Up_Tr_Belt3_ACC : REAL := 0.0; (* REAL Up_Tr_Belt3_ACC *)
		Up_Tr_Belt3_DEC : REAL := 0.0; (* REAL Up_Tr_Belt3_DEC *)
		Up_Tr_Belt3_FWD : BOOL := 0; (* BOOL Up_Tr_Belt3_FWD *)
		Up_Tr_Belt3_BWD : BOOL := 0; (* BOOL Up_Tr_Belt3_BWD *)
		PressConv_Up_Tr_4_MAX_SPD : REAL := 0.0; (* REAL PressConv_Up_Tr_4_MAX_SPD *)
		PressConv_Up_Tr_4_FWD : BOOL := 0; (* BOOL PressConv_Up_Tr_4_FWD *)
		PressConv_Up_Tr_4_KinInhibit : BOOL := 0; (* BOOL PressConv_Up_Tr_4_KinInhibit *)
		Up_Tr_Belt4_MAX_VEL : REAL := 0.0; (* REAL Up_Tr_Belt4_MAX_VEL *)
		Up_Tr_Belt4_ACC : REAL := 0.0; (* REAL Up_Tr_Belt4_ACC *)
		Up_Tr_Belt4_DEC : REAL := 0.0; (* REAL Up_Tr_Belt4_DEC *)
		Up_Tr_Belt4_FWD : BOOL := 0; (* BOOL Up_Tr_Belt4_FWD *)
		Up_Tr_Belt4_BWD : BOOL := 0; (* BOOL Up_Tr_Belt4_BWD *)
		PressConv_Up_Tr_2_MAX_SPD : REAL := 0.0; (* REAL PressConv_Up_Tr_2_MAX_SPD *)
		PressConv_Up_Tr_2_FWD : BOOL := 0; (* BOOL PressConv_Up_Tr_2_FWD *)
		PressConv_Up_Tr_2_KinInhibit : BOOL := 0; (* BOOL PressConv_Up_Tr_2_KinInhibit *)
		PressConv_Up_Tr_1_MAX_SPD : REAL := 0.0; (* REAL PressConv_Up_Tr_1_MAX_SPD *)
		PressConv_Up_Tr_1_FWD : BOOL := 0; (* BOOL PressConv_Up_Tr_1_FWD *)
		PressConv_Up_Tr_1_KinInhibit : BOOL := 0; (* BOOL PressConv_Up_Tr_1_KinInhibit *)
		product_stop_MAX_SPD : REAL := 0.0; (* REAL product_stop_MAX_SPD *)
		product_stop_FWD : BOOL := 0; (* BOOL product_stop_FWD *)
		product_stop_KinInhibit : BOOL := 0; (* BOOL product_stop_KinInhibit *)
		product_stop_BWD : BOOL := 0; (* BOOL product_stop_BWD *)
		InfeedConveyor_MAX_VEL : REAL := 0.0; (* REAL InfeedConveyor_MAX_VEL *)
		InfeedConveyor_ACC : REAL := 0.0; (* REAL InfeedConveyor_ACC *)
		InfeedConveyor_DEC : REAL := 0.0; (* REAL InfeedConveyor_DEC *)
		InfeedConveyor_FWD : BOOL := 0; (* BOOL InfeedConveyor_FWD *)
		InfeedConveyor_BWD : BOOL := 0; (* BOOL InfeedConveyor_BWD *)
	END_STRUCT;
END_TYPE
