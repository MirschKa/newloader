// ===================================================================
//                        COPYRIGHT -- GEA GROUP 
// ===================================================================
// Program: s_supply\InitExit.st    
// Descrition: 
//
// >>>===================== git information ==========================<<<

PROGRAM _INIT
	// ==========================================
	// Register Station - enter specific data
	// ==========================================
	
	RegisterStation.CtrlName := 'S_ExampleStation1';
	RegisterStation.pMachineLink := ADR(gLoader.ExampleStation1);
END_PROGRAM

PROGRAM _EXIT
	UnregisterStation(pMachineLink := ADR(gLoader.ExampleStation1));
END_PROGRAM
