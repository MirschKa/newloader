// ===================================================================
//                        COPYRIGHT -- GEA GROUP 
// ===================================================================
// Program: s_supply\Cyclic.st    
// Descrition: 
//
// >>>===================== git information ==========================<<<

PROGRAM _CYCLIC
   // Call shared functions for all stations
	Action_StationShared;
   
	// copy data from common function to ctrl struct
	
	S_Supply.Info.ActiveState := ModeStateClient.ActiveState;
	S_Supply.Info.MachineMode := ModeStateClient.MachineMode;
	S_Supply.Info.OperatingMode := ModeStateClient.OperationMode;
	S_Supply.Info.StateComplete := ModeStateClient.StateComplete;
	
	// synchronize module 
	#warning 'TODO: add modules'
	// eg. M_Module(pMe := ADR(Me), Instance := 'M_Module');
	
 
	
	
END_PROGRAM




