﻿<?xml version="1.0" encoding="utf-8"?>
<?AutomationStudio Version=4.8.1.174?>
<Program SubType="IEC" xmlns="http://br-automation.co.at/AS/Program">
  <Files>
    <File Description="Init, cyclic, exit code">Cyclic.st</File>
    <File Description="Init, cyclic, exit code">InitExit.st</File>
    <File>Automatic.st</File>
    <File>Manual.st</File>
    <File>Cleaning.st</File>
    <File Description="Local variables" Private="true">Variables.var</File>
    <File Description="Local variables" Private="true" Reference="true">\Logical\Templates\NewStation\s_newSta\StationShared.var</File>
    <File Description="Action for all stations" Reference="true">\Logical\Templates\NewStation\s_newSta\StationSharedCode.st</File>
  </Files>
</Program>