
{REDUND_ERROR} FUNCTION_BLOCK supply_Status (*TODO: Add your comment here*) (*$GROUP=User,$CAT=User,$GROUPICON=User.png,$CATICON=User.png*)
	VAR_OUTPUT
		Info : StationInfo_Typ;
		Status : SupplyStatus_Typ;
		DataValid : BOOL;
	END_VAR
	VAR_IN_OUT
		StationLink : StationLink_Typ;
	END_VAR
	VAR
		rStation : REFERENCE TO Supply_Typ;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK supply_Master
	VAR_INPUT
		Config : SupplyCfg_Typ;
		Command : SupplyCmd_Typ;
		Recipe : SupplyRec_Typ;
		Parameter : SupplyPar_Typ;
	END_VAR
	VAR_OUTPUT
		Info : StationInfo_Typ;
		Status : SupplyStatus_Typ;
		DataValid : BOOL;
	END_VAR
	VAR_IN_OUT
		StationLink : StationLink_Typ;
	END_VAR
	VAR
		rStation : REFERENCE TO Supply_Typ;
	END_VAR
END_FUNCTION_BLOCK
