
TYPE
	Supply_Typ : 	STRUCT 
		Config : SupplyCfg_Typ;
		Command : SupplyCmd_Typ;
		Status : SupplyStatus_Typ;
		Recipe : SupplyRec_Typ;
		Info : StationInfo_Typ;
		Parameter : SupplyPar_Typ;
	END_STRUCT;
	SupplyCfg_Typ : 	STRUCT 
		New_Member : USINT;
	END_STRUCT;
	SupplyRec_Typ : 	STRUCT 
		New_Member : USINT;
	END_STRUCT;
	SupplyStatus_Typ : 	STRUCT 
		Running : BOOL;
		Stopped : BOOL;
		Paused : BOOL;
	END_STRUCT;
	SupplyCmd_Typ : 	STRUCT 
		Start : BOOL;
		Stop : BOOL;
		Pause : BOOL;
	END_STRUCT;
	SupplyPar_Typ : 	STRUCT 
		New_Member : USINT;
	END_STRUCT;
END_TYPE

