
PROGRAM _INIT
	(* Insert code here *)
	D_AcpPowerSupply.Configuration.Available := TRUE;
	D_AcpPowerSupply.Configuration.AxisName := 'gAxPowerSupply';
	D_AcpPowerSupply.Configuration.PlcAddress := '';
	
	
	D_AcpPowerSupply.Command.Init := TRUE;
	
	// dummy call for compiler
	gAxPowerSupply;
	
END_PROGRAM

PROGRAM _CYCLIC
	
	D_AcpPowerSupply.Command.Init := NOT D_AcpPowerSupply.Info.InitOk;
	
	IF EDGEPOS (D_AcpPowerSupply.Info.InitOk) THEN
		D_AcpPowerSupply.Command.Power := TRUE;
	END_IF
	
	D_AcpPowerSupply();
	
END_PROGRAM

PROGRAM _EXIT
	(* Insert code here *)
	 
END_PROGRAM

