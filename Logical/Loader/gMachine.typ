
TYPE
	MotionPermanent_Typ : 	STRUCT 
		ExampleAxis1 : AxisBasePermanetData_Typ;
		ExampleAxis2 : AxisBasePermanetData_Typ;
		ExampleAxis3 : AxisBasePermanetData_Typ;
		ExampleAxis4 : AxisBasePermanetData_Typ;
		ExampleAxis5 : AxisBasePermanetData_Typ;
	END_STRUCT;
	LoaderStation_Typ : 	STRUCT 
		ExampleStation1 : StationLink_Typ;
		ExampleStation2 : StationLink_Typ;
		ExampleStation3 : StationLink_Typ;
		ExampleStation4 : StationLink_Typ;
		ExampleStation5 : StationLink_Typ;
		ExampleStation6 : StationLink_Typ;
		ExampleStation7 : StationLink_Typ;
		ExampleStation8 : StationLink_Typ;
		ExampleStation9 : StationLink_Typ;
	END_STRUCT;
	Recipe_Data_Type : 	STRUCT 
		Example1 : USINT;
		Example2 : UINT;
		Example3 : UDINT;
		Example4 : STRING[10];
		ExampleNumericInput : INT;
		ExampleNumericOutput : INT;
		ExampleAlphanumInput : STRING[12];
		ExampleAlphanumOutput : STRING[12];
		ExampleMomentary : BOOL;
		ExampleToggle : BOOL;
		ExampleInbox : BOOL;
		ExampleOutbox : BOOL;
	END_STRUCT;
	ProdRecipe_Type : 	STRUCT 
		Number : UDINT;
		Type : UINT;
		Name : STRING[20];
		Comment : STRING[30];
		Data : Recipe_Data_Type;
	END_STRUCT;
END_TYPE
