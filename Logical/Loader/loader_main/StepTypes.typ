
TYPE
	MyStepCtrl_Typ : 	STRUCT 
		Step : MyStep_Enum;
		StepName : STRING[80];
		EnterNewStep : BOOL;
	END_STRUCT;
	MyStep_Enum : 
		(
		stpIDLE := 0,
		stpDO_SOMETING,
		stpFINISHED,
		stpERROR := 99999
		);
END_TYPE
