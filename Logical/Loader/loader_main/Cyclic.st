// ===================================================================
//                        COPYRIGHT -- GEA GROUP 
// ===================================================================
// Program:    
// Descrition: 
// warum schreibst du nix?
// >>>===================== git information ==========================<<<

PROGRAM _CYCLIC
	
	ModeStateManager_0.pStationLink 	  := ADR(gLoader);
	ModeStateManager_0.SizeStationLink := SIZEOF(gLoader);
	ModeStateManager_0();
	
	IF (StartAuto = TRUE) THEN
		ModeStateManager_0.Parameter.NewMachineMode 	 := msPRODUCTION;
		ModeStateManager_0.Parameter.NewOperationMode := msAUTO;
		ModeStateManager_0.Command.ChangeMode 			 := TRUE;
		
		IF (ModeStateManager_0.ModeChanged = TRUE) THEN
			ModeStateManager_0.Command.ChangeMode := FALSE;
			StartAuto 									  := FALSE;
		END_IF
		
	ELSIF (StartManual = TRUE) THEN
		ModeStateManager_0.Parameter.NewMachineMode 	 := msPRODUCTION;
		ModeStateManager_0.Parameter.NewOperationMode := msMANUAL;
		ModeStateManager_0.Command.ChangeMode 			 := TRUE;
		
		IF (ModeStateManager_0.ModeChanged = TRUE) THEN
			ModeStateManager_0.Command.ChangeMode := FALSE;
			StartManual 								  := FALSE;
		END_IF
		
	ELSIF (StartCleaning = TRUE) THEN
		ModeStateManager_0.Parameter.NewMachineMode 	 := msCLEANING;
		ModeStateManager_0.Parameter.NewOperationMode := msMANUAL;
		ModeStateManager_0.Command.ChangeMode 			 := TRUE;
		
		IF (ModeStateManager_0.ModeChanged = TRUE) THEN
			ModeStateManager_0.Command.ChangeMode := FALSE;
			StartCleaning 								  := FALSE;
		END_IF
		
	END_IF
	
END_PROGRAM
