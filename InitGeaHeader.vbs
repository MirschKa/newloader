Option Explicit

' -----------------------------------------------------------------------------
' Check if running under "WScript", and if so, relaunch myself cscript and exit
If InStr(1, WScript.FullName, "WScript.exe", vbTextCompare) <> 0 Then
        Dim objShell
        Set objShell = CreateObject("WScript.Shell")
        objShell.Run "%comspec% /c cscript /nologo """ & WScript.ScriptFullName & """", 1, False
        WScript.Quit(0)
End If

Dim fso, oWS, sFile, hFile, sContent
Set fso = CreateObject("Scripting.FileSystemObject")

' --- test if GitHeader filter is initialized ---
sFile = Trim(Console("echo %userprofile%"))
sFile = sFile & "\GeaTools\git\filter\header.smudge"
WScript.Echo sFile
If Not fso.FileExists( sFile ) Then
	MsgBox "Bitte zuerst ConfigureGeaHeader.vbs starten", VBOKOnly, "InitGeaHeader"
	WScript.Quit
End If

' --- get git working copy top-level ---
Dim workingDir
workingDir = Console ("git rev-parse --show-toplevel")
' call git init 
WScript.Echo Console("git init " & workingDir)

' --- create .gitattributes ---
sFile = workingDir & "\.gitattributes"
sFile = Replace(sFile, "/", "\")
WScript.Echo sFile
If fso.FileExists( sFile ) Then
	WScript.Echo ".gitattributes wird angepasst"
	Set hFile = fso.OpenTextFile( sFile )
	sContent = hFile.ReadAll()
	hFile.Close
	Dim aLines
	aLines = Split(sContent, vbCrLf )
	Set hFile = fso.CreateTextFile(sFile, True)
	Dim s
	For Each s In aLines 
		If InStr(s, "geaheader") = 0 Then
			hFile.WriteLine(s)
		End If	
	Next
	hFile.WriteLine("")
Else
	WScript.Echo ".gitattributes wird angelegt"
	Set hFile = fso.CreateTextFile(sFile, True)
End If
' continue to write file header
hFile.WriteLine("# ==== filestypes for geaheader ====")
hFile.WriteLine("*.st  filter=geaheader")
hFile.WriteLine("*.h   filter=geaheader")
hFile.WriteLine("*.c   filter=geaheader")
hFile.WriteLine("*.txt filter=geaheader")
hFile.Close


' ===== Help function to execute commands in cmd =====
Function Console(strCmd)  
  Dim Wss, Cmd, Return, Output
  Set Wss = CreateObject("WScript.Shell")
  Set Cmd = Wss.Exec("cmd.exe")
  Cmd.StdIn.WriteLine strCmd & " 2>&1"
  Cmd.StdIn.Close
  While InStr(Cmd.StdOut.ReadLine, ">" & strCmd) = 0 : Wend
  Do : Output = Cmd.StdOut.ReadLine
    If Cmd.StdOut.AtEndOfStream Then Exit Do _
    Else Return = Return & Output
  Loop
  Console = Return
End Function